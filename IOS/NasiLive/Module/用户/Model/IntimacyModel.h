//
//  IntimacyModel.h
//  Nasi
//
//  Created by yun on 2019/12/31.
//  Copyright © 2019 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class UserInfoModel;

@interface IntimacyModel : NSObject

@property (nonatomic,assign) long long intimacyid;
@property (nonatomic,assign) long long uid;
@property (nonatomic,assign) long long anchorid;
@property (nonatomic,assign) long long intimacy; //亲密值

@property (strong, nonatomic) UserInfoModel *user;

@end

NS_ASSUME_NONNULL_END
