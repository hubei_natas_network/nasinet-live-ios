//
//  IntimacyTableViewCell.h
//  Nasi
//
//  Created by yun on 2020/1/8.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

#import "IntimacyModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface IntimacyTableViewCell : BaseTableViewCell

@property (strong, nonatomic) IntimacyModel *model;
@property (nonatomic,assign) int index;

@end

NS_ASSUME_NONNULL_END
