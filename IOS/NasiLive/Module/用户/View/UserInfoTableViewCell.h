//
//  UserInfoTableViewCell.h
//  Nasi
//
//  Created by yun on 2019/12/30.
//  Copyright © 2019 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@protocol UserInfoTableViewCellDelegate <NSObject>

@optional

- (void)intimacyViewTap;

@end

@interface UserInfoTableViewCell : BaseTableViewCell

@property (nonatomic,weak) id<UserInfoTableViewCellDelegate> delegate;
@property (strong, nonatomic) UserInfoModel *anchorModel;

@end

NS_ASSUME_NONNULL_END
