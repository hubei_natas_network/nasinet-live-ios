//
//  UserInfoHeaderView.m
//  Nasi
//
//  Created by yun on 2019/12/30.
//  Copyright © 2019 yun7. All rights reserved.
//

#import "UserInfoHeaderView.h"

@interface UserInfoHeaderView()<UIScrollViewDelegate>

@property (weak, nonatomic) UIScrollView *imgScrollView;
@property (weak, nonatomic) IBOutlet UILabel *imgCountLabel;

@property (weak, nonatomic) IBOutlet UIView *scrollContainerView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *attenBtn;
@property (weak, nonatomic) IBOutlet UILabel *idLabel;
@property (weak, nonatomic) IBOutlet UIImageView *genderImgView;
@property (weak, nonatomic) IBOutlet UILabel *signatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *attentCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *fansCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *sendGiftCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *tabInfoBtn;
@property (weak, nonatomic) IBOutlet UIButton *tabMomentsBtn;
@property (weak, nonatomic) IBOutlet UIView *tabUnderline1;
@property (weak, nonatomic) IBOutlet UIView *tabUnderline2;

@end

@implementation UserInfoHeaderView

- (instancetype)init{
    if (self = [super init]) {
        self = [[NSBundle mainBundle]loadNibNamed:@"UserInfoHeaderView" owner:self options:nil].firstObject;
        UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenWidth*82.0/75.f)];
        scrollView.backgroundColor = [UIColor whiteColor];
        scrollView.pagingEnabled = YES;
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.delegate = self;
        self.imgScrollView = scrollView;
        [self.scrollContainerView addSubview:scrollView];
    }
    return self;
}

- (void)setAnchorModel:(UserInfoModel *)anchorModel{
    _anchorModel = anchorModel;
    
    self.imgCountLabel.text = [NSString stringWithFormat:@"1/%lu",anchorModel.profile.photos.count > 0 ?(unsigned long)anchorModel.profile.photos.count:1];
    
    [self.imgScrollView removeAllSubviews];
    self.imgScrollView.contentSize = CGSizeMake(kScreenWidth * anchorModel.profile.photos.count, 0);
    if (anchorModel.profile.photos.count > 0) {
        for (int i=0; i<anchorModel.profile.photos.count; i++) {
            UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(kScreenWidth*i, 0, kScreenWidth, self.imgScrollView.height)];
            imgView.contentMode = UIViewContentModeScaleAspectFill;
            imgView.layer.masksToBounds = YES;
            [imgView sd_setImageWithURL:[NSURL URLWithString:anchorModel.profile.photos[i]] placeholderImage:IMAGE_NAMED(@"cover_loading")];
            [self.imgScrollView addSubview:imgView];
        }
    }else{
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, kScreenWidth, self.imgScrollView.height)];
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        imgView.layer.masksToBounds = YES;
        [imgView sd_setImageWithURL:[NSURL URLWithString:anchorModel.avatar] placeholderImage:IMAGE_NAMED(@"cover_loading")];
        [self.imgScrollView addSubview:imgView];
    }
    
    self.nickNameLabel.text = anchorModel.nick_name;
    NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] initWithString:self.nickNameLabel.text attributes:nil];
    NSAttributedString *speaceString = [[NSAttributedString alloc]initWithString:@" "];
    
    //vip标识
    NSTextAttachment *vipAttchment = [[NSTextAttachment alloc]init];
    vipAttchment.bounds = CGRectMake(0, -4, 16, 19);//设置frame
    vipAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"vip_level_%d",[anchorModel getVipLevel]]];//设置图片
    NSAttributedString *vipString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(vipAttchment)];
    
    //主播等级标识
    NSTextAttachment *anchorLevelAttchment = [[NSTextAttachment alloc]init];
    anchorLevelAttchment.bounds = CGRectMake(0, -3, 34, 15);//设置frame
    anchorLevelAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_anchor_level_%d",anchorModel.anchor_level]];
    NSAttributedString *anchorLevelString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(anchorLevelAttchment)];
    
    //等级标识
    NSTextAttachment *levelAttchment = [[NSTextAttachment alloc]init];
    levelAttchment.bounds = CGRectMake(0, -3, 34, 15);//设置frame
    levelAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_user_level_%d",anchorModel.user_level]];
    NSAttributedString *levelString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(levelAttchment)];
    
    //插入VIP图标
    if ([anchorModel checkVip]) {
        [titleStr appendAttributedString:speaceString];//插入到第几个下标
        [titleStr appendAttributedString:vipString];//插入到第几个下标
    }
    
    if (anchorModel.is_anchor && anchorModel.anchor_level > 0) {
        //插入主播等级图标
        [titleStr appendAttributedString:speaceString];//插入到第几个下标
        [titleStr appendAttributedString:anchorLevelString];//插入到第几个下标
    }
    
    //插入等级图标
    [titleStr appendAttributedString:speaceString];//插入到第几个下标
    [titleStr appendAttributedString:levelString];//插入到第几个下标
    
    [self.nickNameLabel setAttributedText:titleStr];

    self.idLabel.text = [NSString stringWithFormat:@"%lld",anchorModel.userid];

    self.signatureLabel.text = anchorModel.profile.signature;
    self.genderImgView.image = anchorModel.profile.gender ? IMAGE_NAMED(@"ic_boy"):IMAGE_NAMED(@"ic_girl");
    
    if (anchorModel.attent_count >= 10000) {
        double d = anchorModel.attent_count * 1.0 / 10000;
        self.attentCountLabel.text = [NSString stringWithFormat:@"%.1f万", d];
    }else{
        self.attentCountLabel.text = [NSString stringWithFormat:@"%d",anchorModel.attent_count];
    }
    
    if (anchorModel.fans_count >= 10000) {
        double d = anchorModel.fans_count * 1.0 / 10000;
        self.fansCountLabel.text = [NSString stringWithFormat:@"%.1f万", d];
    }else{
        self.fansCountLabel.text = [NSString stringWithFormat:@"%d",anchorModel.fans_count];
    }
    
    if (anchorModel.gift_spend >= 10000) {
        double d = anchorModel.gift_spend * 1.0 / 10000;
        self.sendGiftCountLabel.text = [NSString stringWithFormat:@"%.1f万", d];
    }else{
        self.sendGiftCountLabel.text = [NSString stringWithFormat:@"%lld",anchorModel.gift_spend];
    }
    
    if (anchorModel.userid == userManager.curUserInfo.userid) {
        //查看自己的主页时 隐藏关注按钮
        self.attenBtn.hidden = YES;
    }else{
        self.attenBtn.hidden = NO;
        self.attenBtn.selected = anchorModel.isattent;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat offset = scrollView.mj_offsetX;
    NSInteger index = offset/scrollView.width + 1;
    if (index == 0) {
        index = 1;
    }else if (index == self.anchorModel.profile.photos.count + 1){
        index = self.anchorModel.profile.photos.count;
    }
    self.imgCountLabel.text = [NSString stringWithFormat:@"%ld/%lu",(long)index,(unsigned long)self.anchorModel.profile.photos.count];
}

- (IBAction)attenBtnClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(attentBtnClick:countLabel:)]) {
        [self.delegate attentBtnClick:sender countLabel:self.fansCountLabel];
    }
}

- (IBAction)tabInfoBtnClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(tabInfoBtnClick:)]) {
        self.tabInfoBtn.titleLabel.font = [UIFont boldSystemFontOfSize:20];
        self.tabMomentsBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        self.tabUnderline1.hidden = NO;
        self.tabUnderline2.hidden = YES;
        self.selectedIndex = 0;
        [self.delegate tabInfoBtnClick:sender];
    }
}

- (IBAction)tabMomentBtnClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(tabMomentBtnClick:)]) {
        self.tabMomentsBtn.titleLabel.font = [UIFont boldSystemFontOfSize:20];
        self.tabInfoBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        self.tabUnderline1.hidden = YES;
        self.tabUnderline2.hidden = NO;
        self.selectedIndex = 1;
        [self.delegate tabMomentBtnClick:sender];
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
