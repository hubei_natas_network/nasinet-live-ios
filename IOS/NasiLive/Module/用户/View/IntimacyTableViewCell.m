//
//  IntimacyTableViewCell.m
//  Nasi
//
//  Created by yun on 2020/1/8.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "IntimacyTableViewCell.h"

@interface IntimacyTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *rankImgView;
@property (weak, nonatomic) IBOutlet UILabel *rankLabel;

@property (weak, nonatomic) IBOutlet UIImageView *userIconImgView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *intimacyBtn;
@end

@implementation IntimacyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(IntimacyModel *)model{
    _model = model;
    [self.userIconImgView sd_setImageWithURL:[NSURL URLWithString:model.user.avatar] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    [self.intimacyBtn setTitle:[NSString stringWithFormat:@"%lld",model.intimacy] forState:UIControlStateNormal];
    
    self.nickNameLabel.text = model.user.nick_name;
    
    NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] initWithString:self.nickNameLabel.text attributes:nil];
    NSAttributedString *speaceString = [[NSAttributedString alloc]initWithString:@" "];
    
    //vip标识
    NSTextAttachment *vipAttchment = [[NSTextAttachment alloc]init];
    vipAttchment.bounds = CGRectMake(0, -4, 16, 19);//设置frame
    vipAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"vip_level_%d",[model.user getVipLevel]]];//设置图片
    NSAttributedString *vipString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(vipAttchment)];
    
    //等级标识
    NSTextAttachment *levelAttchment = [[NSTextAttachment alloc]init];
    levelAttchment.bounds = CGRectMake(0, -3, 34, 15);//设置frame
    levelAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_user_level_%d",model.user.user_level]];
    NSAttributedString *levelString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(levelAttchment)];
    
    //插入VIP图标
    if ([model.user checkVip]) {
        [titleStr appendAttributedString:speaceString];//插入到第几个下标
        [titleStr appendAttributedString:vipString];//插入到第几个下标
    }
    //插入等级图标
    [titleStr appendAttributedString:speaceString];//插入到第几个下标
    [titleStr appendAttributedString:levelString];//插入到第几个下标
    
    [self.nickNameLabel setAttributedText:titleStr];
}

- (void)setIndex:(int)index{
    _index = index;
    if (index<3) {
        self.rankImgView.hidden = NO;
        NSString *imgName = [NSString stringWithFormat:@"ic_rank%d",index+1];
        self.rankImgView.image = IMAGE_NAMED(imgName);
        self.rankLabel.hidden = YES;
    }else{
        self.rankImgView.hidden = YES;
        self.rankLabel.text = [NSString stringWithFormat:@"NO.%d",index+1];
        self.rankLabel.hidden = NO;
    }
}

+ (CGFloat)cellHeight{
    return 60;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
