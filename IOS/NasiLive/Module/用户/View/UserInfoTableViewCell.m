//
//  UserInfoTableViewCell.m
//  Nasi
//
//  Created by yun on 2019/12/30.
//  Copyright © 2019 yun7. All rights reserved.
//

#import "UserInfoTableViewCell.h"

@interface UserInfoTableViewCell()<UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lastOnlineLabel;
@property (weak, nonatomic) IBOutlet UILabel *connRateLabel;
@property (weak, nonatomic) IBOutlet UILabel *heightLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *constellationLabel;

@property (weak, nonatomic) IBOutlet UIView *intimacyView;
@property (weak, nonatomic) IBOutlet UIView *anchorTagsView;
@property (weak, nonatomic) IBOutlet UIView *giftShowView;
@end

@implementation UserInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UITapGestureRecognizer *intimacyViewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(intimacyViewTap)];
    [self.intimacyView addGestureRecognizer:intimacyViewTap];
}

- (void)intimacyViewTap{
    if ([self.delegate respondsToSelector:@selector(intimacyViewTap)]) {
        [self.delegate intimacyViewTap];
    }
}

- (void)setAnchorModel:(UserInfoModel *)anchorModel{
    _anchorModel = anchorModel;
    NSDateComponents *comp = [CommonManager calculateComponentsFrom:[CommonManager dateWithString:anchorModel.last_online] to:[NSDate date]];
    NSString *last_online = @"刚刚";
    if (anchorModel.online_status == UserOnlineStatusOnline) {
        last_online = @"在线";
    }else if (comp.year > 0) {
        last_online = [NSString stringWithFormat:@"%ld年前",(long)comp.year];
    }else if (comp.month > 0){
        last_online = [NSString stringWithFormat:@"%ld月前",(long)comp.month];
    }else if (comp.day > 0){
        last_online = [NSString stringWithFormat:@"%ld天前",(long)comp.day];
    }else if (comp.hour > 0){
        last_online = [NSString stringWithFormat:@"%ld小时前",(long)comp.hour];
    }else if (comp.minute > 0){
        last_online = [NSString stringWithFormat:@"%ld分钟前",(long)comp.minute];
    }
    
    UIColor *lightTextColor = [UIColor colorWithHexString:@"888888"];
    self.lastOnlineLabel.text = [NSString stringWithFormat:@"最后登录：%@",last_online];
    NSMutableAttributedString *text1 = [[NSMutableAttributedString alloc] initWithString:self.lastOnlineLabel.text];
    [text1 yy_setColor:lightTextColor range:[[text1 string] rangeOfString:@"最后登录：" options:NSCaseInsensitiveSearch]];
    self.lastOnlineLabel.attributedText = text1;
    
    self.connRateLabel.text = [NSString stringWithFormat:@"行业：%@",anchorModel.profile.career];
    NSMutableAttributedString *text2 = [[NSMutableAttributedString alloc] initWithString:self.connRateLabel.text];
    [text2 yy_setColor:lightTextColor range:[[text2 string] rangeOfString:@"行业：" options:NSCaseInsensitiveSearch]];
    self.connRateLabel.attributedText = text2;
    
    self.heightLabel.text = [NSString stringWithFormat:@"身高：%d",anchorModel.profile.height];
    NSMutableAttributedString *text3 = [[NSMutableAttributedString alloc] initWithString:self.heightLabel.text];
    [text3 yy_setColor:lightTextColor range:[[text3 string] rangeOfString:@"身高：" options:NSCaseInsensitiveSearch]];
    self.heightLabel.attributedText = text3;
    
    self.weightLabel.text = [NSString stringWithFormat:@"体重：%d",anchorModel.profile.weight];
    NSMutableAttributedString *text4 = [[NSMutableAttributedString alloc] initWithString:self.weightLabel.text];
    [text4 yy_setColor:lightTextColor range:[[text4 string] rangeOfString:@"体重：" options:NSCaseInsensitiveSearch]];
    self.weightLabel.attributedText = text4;
    
    self.cityLabel.text = [NSString stringWithFormat:@"城市：%@",anchorModel.profile.city];
    NSMutableAttributedString *text5 = [[NSMutableAttributedString alloc] initWithString:self.cityLabel.text];
    [text5 yy_setColor:lightTextColor range:[[text5 string] rangeOfString:@"城市：" options:NSCaseInsensitiveSearch]];
    self.cityLabel.attributedText = text5;
    
    self.constellationLabel.text = [NSString stringWithFormat:@"星座：%@",anchorModel.profile.constellation];
    NSMutableAttributedString *text6 = [[NSMutableAttributedString alloc] initWithString:self.constellationLabel.text];
    [text6 yy_setColor:lightTextColor range:[[text6 string] rangeOfString:@"星座：" options:NSCaseInsensitiveSearch]];
    self.constellationLabel.attributedText = text6;
    
    NSArray *intimacySubViews = [self.intimacyView subviews];
    for (UIView *view in intimacySubViews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
    }
    for (int i=0; i<anchorModel.intimacys.count; i++) {
        if (i>4) {
            break;
        }
        IntimacyModel *intiModel = anchorModel.intimacys[i];
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake((self.intimacyView.height + 7)*i, 0, self.intimacyView.height, self.intimacyView.height)];
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        [imgView sd_setImageWithURL:[NSURL URLWithString:intiModel.user.avatar] placeholderImage:IMAGE_NAMED(@"cover_loading")];
        imgView.layer.masksToBounds = YES;
        imgView.layer.cornerRadius = self.intimacyView.height/2;
        [self.intimacyView addSubview:imgView];
    }
    NSArray *giftSubViews = [self.giftShowView subviews];
    for (UIView *view in giftSubViews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
    }
    for (int i=0; i<anchorModel.gift_show.count; i++) {
        if (i>4) {
            break;
        }
        GiftModel *giftModel = anchorModel.gift_show[i];
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake((self.giftShowView.height + 7)*i, 0, self.giftShowView.height, self.giftShowView.height)];
        [imgView sd_setImageWithURL:[NSURL URLWithString:giftModel.icon] placeholderImage:IMAGE_NAMED(@"cover_loading")];
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        imgView.layer.masksToBounds = YES;
        [self.giftShowView addSubview:imgView];
    }
    
    [self.anchorTagsView removeAllSubviews];
    for (int i = 0; i<anchorModel.tags.count; i++) {
        UserTagModel *tag = anchorModel.tags[i];
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setBackgroundColor:tag.bg_color];
        btn.frame = CGRectMake(88*i, 0, 80, self.anchorTagsView.height);
        btn.layer.cornerRadius = self.anchorTagsView.height/2;
        btn.layer.masksToBounds = YES;
        [btn setTitle:tag.title forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont boldSystemFontOfSize:13];
        [self.anchorTagsView addSubview:btn];
    }
}

+ (CGFloat)cellHeight{
    return 490.f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
