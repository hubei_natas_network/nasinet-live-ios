//
//  UserInfoHeaderView.h
//  Nasi
//
//  Created by yun on 2019/12/30.
//  Copyright © 2019 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol UserInfoHeaderViewDelegate <NSObject>

@optional

- (void)attentBtnClick:(UIButton *)sender countLabel:(UILabel *)countLabel;
- (void)tabInfoBtnClick:(UIButton *)sender;
- (void)tabMomentBtnClick:(UIButton *)sender;

@end

@interface UserInfoHeaderView : UIView

@property (weak, nonatomic) id<UserInfoHeaderViewDelegate> delegate;

@property (nonatomic,assign) NSInteger selectedIndex;

@property (strong, nonatomic) UserInfoModel *anchorModel;

@end

NS_ASSUME_NONNULL_END
