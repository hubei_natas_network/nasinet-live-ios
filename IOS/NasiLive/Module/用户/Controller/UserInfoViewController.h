//
//  UserInfoViewController.h
//  Meet1V1
//
//  Created by yun on 2019/12/28.
//  Copyright © 2019 yun7. All rights reserved.
//

#import "RootViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserInfoViewController : RootViewController

@property (nonatomic,assign) long long anchorid;

@end

NS_ASSUME_NONNULL_END
