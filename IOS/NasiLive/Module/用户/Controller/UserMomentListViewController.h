//
//  UserMomentListViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/6/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

#import <JXPagerView.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserMomentListViewController : RootViewController<JXPagerViewListViewDelegate>

@property (assign, nonatomic) int userid;
@property (assign, nonatomic) int type;     //0-用户发布的 1-我收藏的

@end

NS_ASSUME_NONNULL_END
