//
//  IntimacyListViewController.m
//  Nasi
//
//  Created by yun on 2020/1/8.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "IntimacyListViewController.h"
#import "IntimacyTableViewCell.h"

#define pagesize 10

@interface IntimacyListViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray *datasource;
}

@end

@implementation IntimacyListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    [self removeTableMJFooter];
    [IntimacyTableViewCell registerWithTableView:self.tableView];
    
    datasource = [NSMutableArray array];
    
    [self.tableView.mj_header beginRefreshing];
}

- (void)headerRereshing{
    [self reqDataAtPage:1];
}

- (void)footerRereshing{
    int page = ceil((datasource.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    [self reqDataAtPage:page];
}

- (void)reqDataAtPage:(int)page{
    NSDictionary *params = @{@"page":@(page),
                             @"size":@(pagesize),
                             @"anchorid":@(self.anchorid)
    };
    NSString *api = @"";
    switch (self.type) {
        case IntimacyListTypeTotal:{
            api = @"Intimacy/getTotalIntimacyRank";
        }
            break;
        case IntimacyListTypeWeek:{
            api = @"Intimacy/getWeekIntimacyRank";
        }
            break;
        default:
            break;
    }
    
    [CommonManager POST:api parameters:params success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            if (page == 1) {
                [self->datasource removeAllObjects];
            }
            NSArray *models = [NSArray yy_modelArrayWithClass:[IntimacyModel class] json:responseObject[@"data"]];
            [self->datasource addObjectsFromArray:models];
            [self.tableView reloadData];
            
            if (models.count < pagesize) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }else{
                [self setupTableViewMJFooter];
            }
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        RESP_FAILURE;
    }];
}

#pragma mark ----------------------  tableviewdelegate + datasource ----------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    IntimacyTableViewCell *cell = [IntimacyTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.model = datasource[indexPath.row];
    cell.index = indexPath.row;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [IntimacyTableViewCell cellHeight];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
