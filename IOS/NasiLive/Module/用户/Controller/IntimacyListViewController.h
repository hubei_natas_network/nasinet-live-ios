//
//  IntimacyListViewController.h
//  Nasi
//
//  Created by yun on 2020/1/8.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, IntimacyListType) {
    IntimacyListTypeTotal = 0,
    IntimacyListTypeWeek,
};

@interface IntimacyListViewController : RootViewController

@property (nonatomic,assign) IntimacyListType type;
@property (nonatomic,assign) long long anchorid;

@end

NS_ASSUME_NONNULL_END
