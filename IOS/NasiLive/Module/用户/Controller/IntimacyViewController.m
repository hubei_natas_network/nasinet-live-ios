//
//  IntimacyViewController.m
//  Nasi
//
//  Created by yun on 2020/1/8.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "IntimacyViewController.h"
#import "IntimacyListViewController.h"
#import "XLPageViewController.h"

@interface IntimacyViewController ()<XLPageViewControllerDelegate,XLPageViewControllerDataSrouce>{
    NSArray *pageTitleArr;
}

@end

@implementation IntimacyViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pageTitleArr = @[@"亲密总榜", @"亲密周榜"];
    
    XLPageViewControllerConfig *config = [XLPageViewControllerConfig defaultConfig];
    config.separatorLineHidden = YES;
    config.titleNormalColor = [UIColor darkGrayColor];
    config.titleSelectedColor = [UIColor blackColor];
    config.titleNormalFont = FFont15;
    config.titleSelectedFont = [UIFont boldSystemFontOfSize:22];
    config.titleSpace = 7.f;
    
    config.titleViewHeight = 44;
    config.titleViewInset = UIEdgeInsetsMake(5, kNavBarHeight + 5, 7, 44);
    
    config.shadowLineColor = MAIN_COLOR;
    config.shadowLineWidth = 15;
    
    XLPageViewController *pageViewController = [[XLPageViewController alloc] initWithConfig:config];
    pageViewController.view.frame = CGRectMake(0, kStatusBarHeight, KScreenWidth, KScreenHeight - kStatusBarHeight - kTabBarHeight);
    pageViewController.delegate = self;
    pageViewController.dataSource = self;
    [self.view addSubview:pageViewController.view];
    [self addChildViewController:pageViewController];
    [pageViewController setSelectedIndex:1];
    
    UIButton *navBackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navBackBtn.frame = CGRectMake(0, kStatusBarHeight, kNavBarHeight, kNavBarHeight);
    [navBackBtn setImage:IMAGE_NAMED(@"ic_back") forState:UIControlStateNormal];
    [navBackBtn addTarget:self action:@selector(backBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:navBackBtn];
}

//根据index创建对应的视图控制器，每个试图控制器只会被创建一次。
- (UIViewController *)pageViewController:(XLPageViewController *)pageViewController viewControllerForIndex:(NSInteger)index{
    IntimacyListViewController *vc = [[IntimacyListViewController alloc]init];
    vc.type = index;
    vc.anchorid = self.anchorid ? self.anchorid : userManager.curUserInfo.userid;
    return vc;
}

//根据index返回对应的标题
- (NSString *)pageViewController:(XLPageViewController *)pageViewController titleForIndex:(NSInteger)index{
    return pageTitleArr[index];
}

//返回分页数
- (NSInteger)pageViewControllerNumberOfPage{
    return pageTitleArr.count;
}

- (void)pageViewController:(XLPageViewController *)pageViewController didSelectedAtIndex:(NSInteger)index{
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
