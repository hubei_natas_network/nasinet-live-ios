//
//  UserInfoViewController.m
//  Meet1V1
//
//  Created by yun on 2019/12/28.
//  Copyright © 2019 yun7. All rights reserved.
//

#import "UserInfoViewController.h"
#import "IntimacyViewController.h"
#import "MessageChatViewController.h"
#import "MomentInfoViewController.h"
#import "RechargeViewController.h"
#import "LandscapeLiveViewController.h"
#import "PortraitLiveViewController.h"
#import "VipCenterViewController.h"

#import "GiftChooseView.h"
#import "UserInfoHeaderView.h"
#import "UserInfoTableViewCell.h"

#import "MomentImagesCell.h"
#import "MomentTextCell.h"
#import "MomentVideoCell.h"
#import "MomentSingleImageCell.h"

#import "MomentModel.h"

#import <SJVideoPlayer.h>
#import <SJBaseVideoPlayer/UIScrollView+ListViewAutoplaySJAdd.h>
#import <YBImageBrowser.h>

#import <ImSDK.h>

#define pagesize 10

@interface UserInfoViewController ()<UITableViewDelegate,UITableViewDataSource,UserInfoHeaderViewDelegate,GiftChooseViewDelegate,UserInfoTableViewCellDelegate,MomentBaseTableViewCellDelegate,SJPlayerAutoplayDelegate>{
    UserInfoHeaderView              *headerView;
    UIView                          *navView;
    UIButton                        *navBackBtn;
    UILabel                         *navNickNameLabel;
    GiftChooseView                  *chooseGiftView;
    
    NSMutableArray                  *momentsArr;
    BOOL                            momentNoMoreData;
    
    BOOL                            reqStatus;
    
    SJVideoPlayer                   *sjPlayer;
}

@property (strong, nonatomic) UserInfoModel *anchorModel;

@end

@implementation UserInfoViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if (reqStatus) {
        //通过此偏移确定statusbar颜色
        [self scrollViewDidScroll:self.tableView];
    }else{
        self.StatusBarStyle = UIStatusBarStyleDefault;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    momentsArr = [NSMutableArray array];
    
    [self createNavUI];
    
    [commonManager showLoadingAnimateInView:self.view];
    [CommonManager POST:@"Anchor/getAnchorInfo" parameters:@{@"anchorid":@(self.anchorid)} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            self.StatusBarStyle = UIStatusBarStyleLightContent;
            self->navBackBtn.selected = YES;
            self.anchorModel = [UserInfoModel yy_modelWithDictionary:responseObject[@"data"]];
            self->navNickNameLabel.text = self.anchorModel.nick_name;
            self->navNickNameLabel.hidden = YES;
            self->reqStatus = YES;
            [self createContentView];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)createNavUI{
    navView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, kTopHeight)];
    [self.view addSubview:navView];
    
    navBackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navBackBtn.frame = CGRectMake(0, kStatusBarHeight, kNavBarHeight, kNavBarHeight);
    [navBackBtn setImage:IMAGE_NAMED(@"ic_back") forState:UIControlStateNormal];
    [navBackBtn setImage:IMAGE_NAMED(@"ic_back_white") forState:UIControlStateSelected];
    [navBackBtn addTarget:self action:@selector(backBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:navBackBtn];
    
    navNickNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, kStatusBarHeight, KScreenWidth, kNavBarHeight)];
    navNickNameLabel.font = [UIFont systemFontOfSize:16];
    navNickNameLabel.textColor = [UIColor whiteColor];
    navNickNameLabel.textAlignment = NSTextAlignmentCenter;
    [navView addSubview:navNickNameLabel];
}

- (void)createContentView{
    CGFloat tableH = self.anchorid == userManager.curUserInfo.userid ? KScreenHeight:KScreenHeight - 69 - kBottomSafeHeight;
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, tableH) style:UITableViewStylePlain];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 100;
    self.tableView.estimatedSectionHeaderHeight = 0;
    self.tableView.estimatedSectionFooterHeight = 0;
    
    //头部刷新
//    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(headerRereshing)];
//    header.automaticallyChangeAlpha = YES;
//    header.lastUpdatedTimeLabel.hidden = YES;
//    header.stateLabel.hidden = YES;
//    self.tableView.mj_header = header;
    
    self.tableView.backgroundColor = CViewBgColor;
    self.tableView.scrollsToTop = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [MomentImagesCell registerWithTableView:self.tableView];
    [MomentSingleImageCell registerWithTableView:self.tableView];
    [MomentVideoCell registerWithTableView:self.tableView];
    [MomentTextCell registerWithTableView:self.tableView];
    
    [UserInfoTableViewCell registerWithTableView:self.tableView];
    
    [self.view insertSubview:self.tableView belowSubview:navView];
    
    SJPlayerAutoplayConfig *config = [SJPlayerAutoplayConfig configWithAutoplayDelegate:self];
    config.autoplayPosition = SJAutoplayPositionMiddle;
    [self.tableView sj_enableAutoplayWithConfig:config];
    
    headerView = [[UserInfoHeaderView alloc]init];
    headerView.anchorModel = self.anchorModel;
    headerView.delegate = self;
    self.tableView.tableHeaderView = headerView;
    self.tableView.tableHeaderView.frame = CGRectMake(0, 0, KScreenWidth, 200 + KScreenWidth*82.0/75.f);
    
    if (self.anchorid != userManager.curUserInfo.userid) {
        //绘制底部工具条
        UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, kScreenHeight - 69 - kBottomSafeHeight, KScreenWidth, 69 + kBottomSafeHeight)];
        bottomView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:bottomView];
        UIView *bottomContentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 69)];
        [bottomView addSubview:bottomContentView];
        
        UIButton *msgBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [msgBtn setImage:IMAGE_NAMED(@"ic_message_send") forState:UIControlStateNormal];
        [msgBtn addTarget:self action:@selector(msgBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [bottomContentView addSubview:msgBtn];
        [msgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(bottomContentView.mas_centerY);
            make.left.mas_equalTo(@13.5);
            make.width.height.mas_equalTo(@45.5);
        }];
        UIButton *giftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [giftBtn setImage:IMAGE_NAMED(@"ic_gift_send") forState:UIControlStateNormal];
        [giftBtn addTarget:self action:@selector(giftBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [bottomContentView addSubview:giftBtn];
        [giftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(bottomContentView.mas_centerY);
            make.left.equalTo(msgBtn.mas_right).offset(9.5);
            make.width.height.mas_equalTo(@45.5);
        }];
        
        UIButton *goliveroomBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [goliveroomBtn setImage:IMAGE_NAMED(@"ic_nogo_liveroom") forState:UIControlStateNormal];
        [goliveroomBtn setImage:IMAGE_NAMED(@"ic_go_liveroom") forState:UIControlStateSelected];
        [goliveroomBtn addTarget:self action:@selector(goliveroomBtnClick) forControlEvents:UIControlEventTouchUpInside];
        goliveroomBtn.selected  = self.anchorModel.is_anchor;
        [bottomContentView addSubview:goliveroomBtn];
        [goliveroomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(bottomContentView.mas_centerY);
            make.right.mas_equalTo(-15);
            make.width.mas_equalTo(156);
            make.height.mas_equalTo(@46.5);
        }];
    }
    //动态预加载
    [self loadMoments];
}

- (void)headerRereshing{
    
}

- (void)footerRereshing{
    if (headerView.selectedIndex == 0) {
        
    }else{
        [self loadMoments];
    }
}


- (void)loadMoments{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"size"] = @(pagesize);
    NSInteger page = ceil((momentsArr.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    params[@"page"] = @(page);
    params[@"authorid"] = @(self.anchorid);
    [CommonManager POST:@"Moment/getListByUser" parameters:params success:^(id responseObject) {
        if (self->headerView.selectedIndex == 1) [self.tableView.mj_footer endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            if (page == 1) {
                [self->momentsArr removeAllObjects];
            }
            NSArray *dataArr = [NSArray yy_modelArrayWithClass:[MomentModel class] json:responseObject[@"data"]];
            if (dataArr.count < pagesize) {
                self->momentNoMoreData = YES;
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
            [self->momentsArr addObjectsFromArray:dataArr];
            if (self->headerView.selectedIndex == 1) [self.tableView reloadData];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        if (self->headerView.selectedIndex == 1) [self.tableView.mj_footer endRefreshing];
        RESP_FAILURE;
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (headerView.selectedIndex == 0) {
        return 1;
    }else{
        return momentsArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (headerView.selectedIndex == 0) {
        UserInfoTableViewCell *cell = [UserInfoTableViewCell cellWithTableView:tableView indexPath:indexPath];
        cell.delegate = self;
        cell.anchorModel = self.anchorModel;
        return cell;
    }else{
        MomentModel *model = momentsArr[indexPath.row];
        if (model.type == MomentTypeImages) {
            
            if (model.image_urls.count == 1) {
                MomentSingleImageCell *cell = [MomentSingleImageCell cellWithTableView:tableView indexPath:indexPath];
                cell.delegate = self;
                cell.model = model;
                return cell;
            }else{
                MomentImagesCell *cell = [MomentImagesCell cellWithTableView:tableView indexPath:indexPath];
                cell.delegate = self;
                cell.model = model;
                return cell;
            }
        }else if (model.type == MomentTypeText){
            MomentTextCell *cell = [MomentTextCell cellWithTableView:tableView indexPath:indexPath];
            cell.delegate = self;
            cell.model = model;
            return cell;
        }else if (model.type == MomentTypeVideo){
            MomentVideoCell *cell = [MomentVideoCell cellWithTableView:tableView indexPath:indexPath];
            cell.delegate = self;
            cell.model = model;
            return cell;
        }
        return [[UITableViewCell alloc]init];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (headerView.selectedIndex == 0) {
        return [UserInfoTableViewCell cellHeight];
    }else{
        return UITableViewAutomaticDimension;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (headerView.selectedIndex == 1) {
        MomentModel *model = momentsArr[indexPath.row];
        MomentInfoViewController *vc = [[MomentInfoViewController alloc]init];
        vc.momentModel = model;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)giftBtnClick{
    if (!chooseGiftView) {
        chooseGiftView = [GiftChooseView showInView:self.view];
        chooseGiftView.delegate = self;
    }else{
        [chooseGiftView show];
    }
}

//进入直播间
- (void)goliveroomBtnClick{
    if (!self.anchorModel.live) {
        [MBProgressHUD showTipMessageInView:@"主播休息中"];
        return;
    }
    if (self.anchorModel.live.orientation == 1) {
        LandscapeLiveViewController *vc = [[LandscapeLiveViewController alloc]init];
        vc.liveModel = self.anchorModel.live;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        PortraitLiveViewController *vc = [[PortraitLiveViewController alloc]init];
        vc.anchorModel = self.anchorModel;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)msgBtnClick{
    TUIConversationCellData *data = [[TUIConversationCellData alloc]init];
    data.userID = [NSString stringWithFormat:@"%lld",self.anchorid];
    data.faceUrl = self.anchorModel.avatar;
    MessageChatViewController *vc = [[MessageChatViewController alloc]initWithConversationCellData:data senderName:self.anchorModel.nick_name];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)intimacyViewTap{
    IntimacyViewController *vc = [[IntimacyViewController alloc]init];
    vc.anchorid = self.anchorModel.userid;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)attentBtnClick:(UIButton *)sender countLabel:(UILabel *)countLabel{
    NSDictionary *param = @{@"anchorid":@(self.anchorModel.userid),
                            @"type":@(!sender.selected)
    };
    [commonManager showLoadingAnimateInView:self.view];
    [CommonManager POST:@"Anchor/attentAnchor" parameters:param success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            int fans_count = [responseObject[@"data"][@"fans_count"] intValue];
            self.anchorModel.fans_count = fans_count;
            if (fans_count >= 10000) {
                double d = fans_count * 1.0 / 10000;
                countLabel.text = [NSString stringWithFormat:@"%.1fw", d];
            }else{
                countLabel.text = [NSString stringWithFormat:@"%d",fans_count];
            }
            sender.selected = !sender.selected;
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

#pragma mark ————————————————————————————————————— ChooseGiftViewDelegate —————————————————————————————————————

- (void)sendGift:(GiftModel *)gift count:(int)count{
    if (userManager.curUserInfo.gold < gift.price*count) {
        //弹出充值窗口
        [QNAlertView showAlertViewWithType:QNAlertTypeWarning title:@"" contentText:@"余额不足" leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"充值" rightClick:^{
            RechargeViewController *vc = [[RechargeViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }];
        return;
    }
    //预更新用户金币余额 防止多次点击
    userManager.curUserInfo.gold -= gift.price;
    [userManager saveUserInfo];
    [self->chooseGiftView refreshUserCoinNum];
    
    NSDictionary *param = @{@"anchorid":@(self.anchorModel.userid),
                            @"giftid":@(gift.giftid),
                            @"count":@(count)
    };
    [CommonManager POST:@"Gift/sendGift" parameters:param success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            userManager.curUserInfo.gold = [responseObject[@"data"][@"gold"] intValue];
            [userManager saveUserInfo];
            [self->chooseGiftView refreshUserCoinNum];
            
            //显示动画
            IMNotificationModel *notificationModel = [[IMNotificationModel alloc]init];
            gift.sender = [userManager.curUserInfo yy_modelCopy];
            gift.count = count;
            notificationModel.gift = gift;
            [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationGiftAnimation object:nil userInfo:@{@"data":notificationModel}];
        }else{
            //恢复本地用户金币余额
            userManager.curUserInfo.gold += gift.price;
            [userManager saveUserInfo];
            [self->chooseGiftView refreshUserCoinNum];
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        //恢复本地用户金币余额
        userManager.curUserInfo.gold += gift.price;
        [userManager saveUserInfo];
        [self->chooseGiftView refreshUserCoinNum];
        RESP_FAILURE;
    }];
}

#pragma mark —————————— SJPlayerAutoplayDelegate ———————————————
- (void)sj_playerNeedPlayNewAssetAtIndexPath:(NSIndexPath *)indexPath {
    if ( indexPath != nil ) {
        MomentModel *model = momentsArr[indexPath.row];
        if ( !sjPlayer ) {
            sjPlayer = [SJVideoPlayer player];
            sjPlayer.muted = YES;
            sjPlayer.playbackObserver.playbackDidFinishExeBlock = ^(__kindof SJBaseVideoPlayer * _Nonnull player) {
                [player replay];
            };
        }
        if (model.unlock_price > 0 && !model.unlocked && model.user.userid != userManager.curUserInfo.userid){
            return;
        }
        sjPlayer.URLAsset = [[SJVideoPlayerURLAsset alloc] initWithURL:[NSURL URLWithString:model.video_url] playModel:[SJPlayModel playModelWithTableView:self.tableView indexPath:indexPath]];
        sjPlayer.URLAsset.title = @"";
        sjPlayer.muted = YES;
        
        //检测播放权限
        [CommonManager POST:@"moment/checkCanPlay" parameters:@{@"momentid":@(model.momentid)} success:^(id responseObject) {
            if (RESP_SUCCESS(responseObject)) {
                self->sjPlayer.muted = NO;
            }else{
                [self->sjPlayer stop];
                [self.tableView sj_removeCurrentPlayerView];
                [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"观影次数不足" contentText:@"开通Vip尽享无限观影" leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"开通" rightClick:^{
                    [self.navigationController pushViewController:[[VipCenterViewController alloc]init] animated:YES];
                }];
            }
        } failure:^(NSError *error) {
            RESP_FAILURE;
            [self->sjPlayer stop];
            [self.tableView sj_removeCurrentPlayerView];
        }];
    }
}

#pragma mark —————————— MomentBaseTableViewCellDelegate ———————————————
- (void)unlockClick:(MomentBaseTableViewCell *)cell{
    [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"解锁动态" contentText:[NSString stringWithFormat:@"解锁此动态需要支付%d金币", cell.model.unlock_price] leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"解锁" rightClick:^{
        [commonManager showLoadingAnimateInWindow];
        [CommonManager POST:@"Moment/unlockMoment" parameters:@{@"momentid":@(cell.model.momentid)} success:^(id responseObject) {
            [commonManager hideAnimateHud];
            if (RESP_SUCCESS(responseObject)) {
                cell.model.unlocked = YES;
                if (cell.model.type == MomentTypeVideo) {
                    //解锁后播放视频
                    if (!self->sjPlayer ) {
                        self->sjPlayer = [SJVideoPlayer player];
                        self->sjPlayer.playbackObserver.playbackDidFinishExeBlock = ^(__kindof SJBaseVideoPlayer * _Nonnull player) {
                            [player replay];
                        };
                    }
                    self->sjPlayer.URLAsset = [[SJVideoPlayerURLAsset alloc] initWithURL:[NSURL URLWithString:cell.model.video_url] playModel:[SJPlayModel playModelWithTableView:self.tableView indexPath:[self.tableView indexPathForCell:cell]]];
                    self->sjPlayer.URLAsset.title = @"";
                }else{
                    [self.tableView reloadData];
                }
            }else{
                RESP_SHOW_ERROR_MSG(responseObject);
            }
        } failure:^(NSError *error) {
            [commonManager hideAnimateHud];
            RESP_FAILURE;
        }];
    }];
}

- (void)likeClick:(MomentModel *)model{
    if (model.liked) {
        return;
    }
    [CommonManager POST:@"Moment/likeMoment" parameters:@{@"momentid":@(model.momentid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            model.liked = YES;
            model.like_count = [responseObject[@"data"][@"like_count"] intValue];
            [self.tableView reloadData];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (void)commentClick:(MomentModel *)model{
    
}

- (void)moreFuncClick:(MomentModel *)model{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"选项" preferredStyle:UIAlertControllerStyleActionSheet];
    NSString *collectStr = model.collected?@"取消收藏":@"收藏";
    UIAlertAction *collectAction = [UIAlertAction actionWithTitle:collectStr style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [commonManager showLoadingAnimateInWindow];
        [CommonManager POST:@"Moment/collectMoment" parameters:@{@"momentid":@(model.momentid), @"type":@(!model.collected)} success:^(id responseObject) {
            [commonManager hideAnimateHud];
            if (RESP_SUCCESS(responseObject)) {
                [commonManager showSuccessAnimateInWindow];
                model.collected = !model.collected;
            }else{
                RESP_SHOW_ERROR_MSG(responseObject);
            }
        } failure:^(NSError *error) {
            [commonManager hideAnimateHud];
            RESP_FAILURE;
        }];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:collectAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)imgViewClick:imageView model:(MomentModel *)model index:(NSInteger)index{
    NSMutableArray *datas = [NSMutableArray array];
    [model.image_urls enumerateObjectsUsingBlock:^(NSString *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        // 网络图片
        YBIBImageData *data = [YBIBImageData new];
        data.imageURL = [NSURL URLWithString:obj];
        data.projectiveView = imageView;
        [datas addObject:data];
    }];
    
    YBImageBrowser *browser = [YBImageBrowser new];
    browser.dataSourceArray = datas;
    browser.currentPage = index;
    // 只有一个保存操作的时候，可以直接右上角显示保存按钮
    browser.defaultToolViewHandler.topView.operationType = YBIBTopViewOperationTypeSave;
    [browser show];
}

- (void)userIconClick:(MomentModel *)model{
    UserInfoViewController *vc = [[UserInfoViewController alloc]init];
    vc.anchorid = model.user.userid;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)tabInfoBtnClick:(UIButton *)sender{
    [self.tableView reloadData];
    self.tableView.mj_footer = nil;
}

- (void)tabMomentBtnClick:(UIButton *)sender{
    [self.tableView reloadData];
    if (momentNoMoreData) {
        [self.tableView.mj_footer removeFromSuperview];
        self.tableView.mj_footer = nil;
    }else if (momentsArr.count > 0){
        MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(footerRereshing)];
        self.tableView.mj_footer = footer;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == self.tableView) {
        CGFloat offsetY = self.tableView.mj_offsetY;
        CGFloat imgH = KScreenWidth*82.0/75.f;
        if (offsetY > imgH - navView.height*2) {
            CGFloat alpha = (offsetY - imgH + navView.height*2) / navView.height;
            if (alpha > 1) {
                alpha = 1;
            }
            if (alpha < 0) {
                alpha = 0;
            }
            if (alpha > 0.7) {
                navBackBtn.selected = NO;
                navNickNameLabel.textColor = [UIColor blackColor];
                self.StatusBarStyle = UIStatusBarStyleDefault;
            }else{
                navBackBtn.selected = YES;
                navNickNameLabel.textColor = [UIColor whiteColor];
                self.StatusBarStyle = UIStatusBarStyleLightContent;
            }
            navNickNameLabel.hidden = NO;
            navView.backgroundColor = [UIColor colorWithWhite:1 alpha:alpha];
        }else{
            navBackBtn.selected = YES;
            navNickNameLabel.hidden = YES;
            navView.backgroundColor = [UIColor clearColor];
            self.StatusBarStyle = UIStatusBarStyleLightContent;
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
