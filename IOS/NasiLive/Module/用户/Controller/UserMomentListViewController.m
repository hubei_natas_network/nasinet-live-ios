//
//  UserMomentListViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/6/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "UserMomentListViewController.h"
#import "MomentInfoViewController.h"
#import "UserInfoViewController.h"

#import "MomentModel.h"
#import "MomentTextCell.h"
#import "MomentVideoCell.h"
#import "MomentSingleImageCell.h"
#import "MomentImagesCell.h"

#import <SJVideoPlayer.h>
#import <SJBaseVideoPlayer/UIScrollView+ListViewAutoplaySJAdd.h>
#import <YBImageBrowser.h>

#define pagesize 20

@interface UserMomentListViewController ()<UITableViewDelegate,UITableViewDataSource,MomentBaseTableViewCellDelegate,SJPlayerAutoplayDelegate>{
    NSMutableArray                  *datasource;
}

@property (strong, nonatomic) SJVideoPlayer *player;

@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@property (copy, nonatomic) NSString *api;

@end

@implementation UserMomentListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    [self removeTableMJFooter];
    
    [MomentImagesCell registerWithTableView:self.tableView];
    [MomentSingleImageCell registerWithTableView:self.tableView];
    [MomentVideoCell registerWithTableView:self.tableView];
    [MomentTextCell registerWithTableView:self.tableView];
    
    SJPlayerAutoplayConfig *config = [SJPlayerAutoplayConfig configWithAutoplayDelegate:self];
    config.autoplayPosition = SJAutoplayPositionMiddle;
    [self.tableView sj_enableAutoplayWithConfig:config];
    
    datasource = [NSMutableArray array];
    
    [self.tableView.mj_header beginRefreshing];
}

- (void)headerRereshing{
    [self reqDataAtPage:1];
}

- (void)footerRereshing{
    int page = ceil((datasource.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    [self reqDataAtPage:page];
}

- (void)reqDataAtPage:(int)page{
    NSDictionary *params = @{@"page":@(page),
                             @"size":@(pagesize),
                             @"authorid":@(self.userid)
    };
    
    [CommonManager POST:self.api parameters:params success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            if (page == 1) {
                [self->datasource removeAllObjects];
            }
            NSArray *models = [NSArray yy_modelArrayWithClass:[MomentModel class] json:responseObject[@"data"]];
            [self->datasource addObjectsFromArray:models];
            [self.tableView reloadData];
            
            if (models.count < pagesize) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }else{
                [self setupTableViewMJFooter];
            }
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        RESP_FAILURE;
    }];
}

#pragma mark ----------------------  tableviewdelegate + datasource ----------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MomentModel *model = datasource[indexPath.row];
    if (model.type == MomentTypeImages) {
        
        if (model.image_urls.count == 1) {
            MomentSingleImageCell *cell = [MomentSingleImageCell cellWithTableView:tableView indexPath:indexPath];
            cell.delegate = self;
            cell.model = model;
            return cell;
        }else{
            MomentImagesCell *cell = [MomentImagesCell cellWithTableView:tableView indexPath:indexPath];
            cell.delegate = self;
            cell.model = model;
            return cell;
        }
    }else if (model.type == MomentTypeText){
        MomentTextCell *cell = [MomentTextCell cellWithTableView:tableView indexPath:indexPath];
        
        cell.delegate = self;
        cell.model = model;
        return cell;
    }else if (model.type == MomentTypeVideo){
        MomentVideoCell *cell = [MomentVideoCell cellWithTableView:tableView indexPath:indexPath];
        
        cell.delegate = self;
        cell.model = model;
        return cell;
    }
    return [[UITableViewCell alloc]init];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MomentModel *model = datasource[indexPath.row];
    MomentInfoViewController *vc = [[MomentInfoViewController alloc]init];
    vc.momentModel = model;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark —————————— self.playerAutoplayDelegate ———————————————
- (void)sj_playerNeedPlayNewAssetAtIndexPath:(NSIndexPath *)indexPath {
    if ( indexPath != nil ) {
        MomentModel *model = datasource[indexPath.row];
        if (model.unlock_price > 0 && !model.unlocked && model.user.userid != userManager.curUserInfo.userid){
            return;
        }
        self.player.URLAsset = [[SJVideoPlayerURLAsset alloc] initWithURL:[NSURL URLWithString:model.video_url] playModel:[SJPlayModel playModelWithTableView:self.tableView indexPath:indexPath]];
        self.player.URLAsset.title = @"";
    }
}

#pragma mark —————————— MomentBaseTableViewCellDelegate ———————————————
- (void)playBtnClick:(MomentBaseTableViewCell *)cell{
    if (cell.model.unlock_price > 0 && !cell.model.unlocked && cell.model.user.userid != userManager.curUserInfo.userid){
        [self unlockClick:cell.model];
        return;
    }
    self.player.URLAsset = [[SJVideoPlayerURLAsset alloc] initWithURL:[NSURL URLWithString:cell.model.video_url] playModel:[SJPlayModel playModelWithTableView:self.tableView indexPath:[self.tableView indexPathForCell:cell]]];
    self.player.URLAsset.title = @"";
}

- (void)unlockClick:(MomentModel *)model{
    [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"解锁动态" contentText:[NSString stringWithFormat:@"解锁此动态需要支付%d金币", model.unlock_price] leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"解锁" rightClick:^{
        [self unlockMoment:model];
    }];
}

- (void)likeClick:(MomentModel *)model{
    if (model.liked) {
        return;
    }
    [CommonManager POST:@"Moment/likeMoment" parameters:@{@"momentid":@(model.momentid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            model.liked = YES;
            model.like_count = [responseObject[@"data"][@"like_count"] intValue];
            [self.tableView reloadData];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (void)commentClick:(MomentModel *)model{
    MomentInfoViewController *vc = [[MomentInfoViewController alloc]init];
    vc.momentModel = model;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)moreFuncClick:(MomentModel *)model{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"选项" preferredStyle:UIAlertControllerStyleActionSheet];
    NSString *collectStr = model.collected?@"取消收藏":@"收藏";
    UIAlertAction *collectAction = [UIAlertAction actionWithTitle:collectStr style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [commonManager showLoadingAnimateInWindow];
        [CommonManager POST:@"Moment/collectMoment" parameters:@{@"momentid":@(model.momentid), @"type":@(!model.collected)} success:^(id responseObject) {
            [commonManager hideAnimateHud];
            if (RESP_SUCCESS(responseObject)) {
                [commonManager showSuccessAnimateInWindow];
                model.collected = !model.collected;
            }else{
                RESP_SHOW_ERROR_MSG(responseObject);
            }
        } failure:^(NSError *error) {
            [commonManager hideAnimateHud];
            RESP_FAILURE;
        }];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:collectAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)imgViewClick:imageView model:(MomentModel *)model index:(NSInteger)index{
    NSMutableArray *datas = [NSMutableArray array];
    [model.image_urls enumerateObjectsUsingBlock:^(NSString *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        // 网络图片
        YBIBImageData *data = [YBIBImageData new];
        data.imageURL = [NSURL URLWithString:obj];
        data.projectiveView = imageView;
        [datas addObject:data];
    }];
    
    YBImageBrowser *browser = [YBImageBrowser new];
    browser.dataSourceArray = datas;
    browser.currentPage = index;
    // 只有一个保存操作的时候，可以直接右上角显示保存按钮
    browser.defaultToolViewHandler.topView.operationType = YBIBTopViewOperationTypeSave;
    [browser show];
}

- (void)userIconClick:(MomentModel *)model{
    UserInfoViewController *vc = [[UserInfoViewController alloc]init];
    vc.anchorid = model.user.userid;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)unlockMoment:(MomentModel *)model{
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"Moment/unlockMoment" parameters:@{@"momentid":@(model.momentid)} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            model.unlocked = YES;
            [self.tableView reloadData];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ? : self.scrollCallback(scrollView);
}

#pragma mark - GKPageListViewDelegate
- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.tableView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}

- (NSString *)api{
    if (!_api) {
        _api = self.type == 1?@"Moment/getCollection":@"Moment/getListByUser";
    }
    return _api;
}

- (SJVideoPlayer *)player{
    if (!_player) {
        _player = [SJVideoPlayer player];
        _player.muted = YES;
        _player.playbackObserver.playbackDidFinishExeBlock = ^(__kindof SJBaseVideoPlayer * _Nonnull player) {
            [player replay];
        };
    }
    return _player;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
