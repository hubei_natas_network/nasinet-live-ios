//
//  SystemMessageModel.m
//  Nasi
//
//  Created by yun on 2020/1/19.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "SystemMessageModel.h"

@implementation SystemMessageModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"msgid" : @"id",
    };
}

@end
