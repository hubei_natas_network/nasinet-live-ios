//
//  SystemMessageModel.h
//  Nasi
//
//  Created by yun on 2020/1/19.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SystemMessageModel : NSObject

@property (nonatomic,assign) int msgid;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *content;
@property (copy, nonatomic) NSString *image_url;
@property (copy, nonatomic) NSString *link;
@property (copy, nonatomic) NSString *create_time;

@end

NS_ASSUME_NONNULL_END
