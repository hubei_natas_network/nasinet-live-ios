//
//  SystemMessageListCell.h
//  Nasi
//
//  Created by yun on 2020/1/19.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface SystemMessageListCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (assign, nonatomic) int unreadCount;

@end

NS_ASSUME_NONNULL_END
