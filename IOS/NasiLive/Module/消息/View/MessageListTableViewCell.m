//
//  MessageListTableViewCell.m
//  NasiLive
//
//  Created by yun11 on 2020/7/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MessageListTableViewCell.h"

#import "TUIConversationCellData.h"

@interface MessageListTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *unreadCountLabel;

@end

@implementation MessageListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setData:(TUIConversationCellData *)data{
    _data = data;

    [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:data.faceUrl] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    self.nickNameLabel.text = data.title;
    self.messageLabel.attributedText = data.subTitle;
    self.timeLabel.text = [CommonManager formateBeautyDate:[CommonManager stringWithDate:data.time formatter:@"yyyy-MM-dd HH:mm:ss"]];
    self.unreadCountLabel.text = [NSString stringWithFormat:@"%d",data.unreadCount];
    self.unreadCountLabel.hidden = data.unreadCount == 0;
}

+ (CGFloat)cellHeight{
    return 72;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
