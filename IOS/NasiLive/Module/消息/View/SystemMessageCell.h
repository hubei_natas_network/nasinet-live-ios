//
//  SystemMessageCell.h
//  Nasi
//
//  Created by yun on 2020/1/19.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@class SystemMessageModel;

@interface SystemMessageCell : BaseTableViewCell

@property (strong, nonatomic) SystemMessageModel *model;

+ (CGFloat)cellHeightWithModel:(SystemMessageModel *)model;

@end

NS_ASSUME_NONNULL_END
