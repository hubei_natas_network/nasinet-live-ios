//
//  MessageListTableViewCell.h
//  NasiLive
//
//  Created by yun11 on 2020/7/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

@class TUIConversationCellData;

NS_ASSUME_NONNULL_BEGIN

@interface MessageListTableViewCell : BaseTableViewCell

@property (strong, nonatomic) TUIConversationCellData *data;

@end

NS_ASSUME_NONNULL_END
