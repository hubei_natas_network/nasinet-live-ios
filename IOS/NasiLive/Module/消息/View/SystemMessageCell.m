//
//  SystemMessageCell.m
//  Nasi
//
//  Created by yun on 2020/1/19.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "SystemMessageCell.h"

#import "SystemMessageModel.h"

@interface SystemMessageCell ()

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *contView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgHeightLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomHeightLC;

@end

@implementation SystemMessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(SystemMessageModel *)model{
    _model = model;
    
    [self resetView];
    
    self.timeLabel.text = [CommonManager stringWithDate:[CommonManager dateWithString:model.create_time] formatter:@"yyyy年MM月dd日 HH:mm:ss"];
    
    if (model.image_url.length > 0) {
        self.imgView.hidden = NO;
        [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.image_url] placeholderImage:IMAGE_NAMED(@"cover_loading")];
        self.imgHeightLC.constant = (KScreenWidth - 15*2)*5/14;
    }
    if (model.link.length > 0) {
        self.bottomView.hidden = NO;
        self.bottomHeightLC.constant = 30;
    }
    
    self.titleLabel.text = model.title;
    
    NSAttributedString *titleAttr = [CommonManager getAttributedStringWithString:model.content lineSpace:7];
    self.infoLabel.attributedText = titleAttr;
}

- (void)resetView{
    self.imgView.hidden = YES;
    self.bottomView.hidden = YES;
    self.imgHeightLC.constant = 0;
    self.bottomHeightLC.constant = 0;
}

+ (CGFloat)cellHeightWithModel:(SystemMessageModel *)model{
    CGFloat bottomH = model.link.length > 0?30:0;
    CGFloat imgH = model.image_url.length > 0?(KScreenWidth - 15*2)*5/14:0;
    NSAttributedString *titleAttr = [CommonManager getAttributedStringWithString:model.content lineSpace:7];
    CGFloat contentLabelH = [self sizeLabelToFit:titleAttr width:KScreenWidth - 15*4 height:0].height;
    if (contentLabelH > 41) {
        contentLabelH = 41;
    }
    return 119 + bottomH + imgH + contentLabelH;
}

+ (CGSize)sizeLabelToFit:(NSAttributedString *)aString width:(CGFloat)width height:(CGFloat)height {
    UILabel *tempLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    tempLabel.font = [UIFont systemFontOfSize:12];
    tempLabel.attributedText = aString;
    tempLabel.numberOfLines = 0;
    [tempLabel sizeToFit];
    CGSize size = tempLabel.frame.size;
    return size;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
