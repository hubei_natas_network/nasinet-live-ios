//
//  SystemMessageListCell.m
//  Nasi
//
//  Created by yun on 2020/1/19.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "SystemMessageListCell.h"

@interface SystemMessageListCell()

@property (weak, nonatomic) IBOutlet UILabel *unreadCountLabel;

@end

@implementation SystemMessageListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setUnreadCount:(int)unreadCount{
    _unreadCount = unreadCount;
    self.unreadCountLabel.text = [NSString stringWithFormat:@"%d",unreadCount];
    self.unreadCountLabel.hidden = unreadCount == 0;
}

+ (CGFloat)cellHeight{
    return 72;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
