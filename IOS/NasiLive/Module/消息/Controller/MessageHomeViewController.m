//
//  MessageHomeViewController.m
//  Nasi
//
//  Created by yun on 2020/1/17.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MessageHomeViewController.h"
#import "SystemMsgViewController.h"
#import "MessageChatViewController.h"

#import "SystemMessageListCell.h"
#import "MessageListTableViewCell.h"

#import "TUIConversationCell.h"
#import "TIMUserProfile+DataProvider.h"
#import "ReactiveObjC/ReactiveObjC.h"
#import "TUIConversationCell.h"
#import "TConversationListViewModel.h"
#import "ImSDK.h"

#import <TUIChatController.h>

@interface MessageHomeViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray  *datasource;
    
    int             sysMsgCount;
}

@property (nonatomic, strong) TConversationListViewModel *viewModel;

@end

@implementation MessageHomeViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationUpdateMessageNum object:nil userInfo:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    datasource = [NSMutableArray array];
    
    self.title = @"消息";
    
    [self addNavigationItemWithImageNames:@[@"ic_friend"] isLeft:NO target:self action:@selector(rightBtnClick) tags:@[@"1"]];
    
    
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 100;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self removeTableMJFooter];
    [self removeTableMJHeader];
    
    [SystemMessageListCell registerWithTableView:self.tableView];
    [MessageListTableViewCell registerWithTableView:self.tableView];
    
    @weakify(self)
    [RACObserve(self.viewModel, dataList) subscribeNext:^(id _Nullable x) {
        @strongify(self)
        [self->datasource removeAllObjects];
        for (TUIConversationCellData *data in self.viewModel.dataList) {
            if (data.userID.length > 0 && ![data.userID isEqualToString:[configManager appConfig].txim_admin] && ![data.userID isEqualToString:[configManager appConfig].txim_broadcast]) {
                [self->datasource addObject:data];
            }else if ([data.userID isEqualToString:[configManager appConfig].txim_admin]){
                self->sysMsgCount = data.unreadCount;
            }
        }
        [self.tableView reloadData];
    }];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateMessageNum:) name:KNotificationUpdateMessageNum object:nil];
}

- (void)rightBtnClick{
    
}

- (TConversationListViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [TConversationListViewModel new];
    }
    return _viewModel;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return datasource.count + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return [SystemMessageListCell cellHeight];
    }
    return [MessageListTableViewCell cellHeight];
}



- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 0;
    }
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [tableView beginUpdates];
        TUIConversationCellData *conv = datasource[indexPath.row-1];
        [self.viewModel removeData:conv];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationNone];
        [tableView endUpdates];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        SystemMessageListCell *cell = [SystemMessageListCell cellWithTableView:tableView indexPath:indexPath];
        cell.imgView.image = IMAGE_NAMED(@"ic_msg_system");
        cell.titleLabel.text = @"系统消息";
        cell.unreadCount = sysMsgCount;
        return cell;
    }else{
        MessageListTableViewCell *cell = [MessageListTableViewCell cellWithTableView:tableView indexPath:indexPath];
        cell.data = datasource[indexPath.row - 1];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        SystemMsgViewController *vc = [[SystemMsgViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        TUIConversationCellData *data = datasource[indexPath.row - 1];
        MessageChatViewController *vc = [[MessageChatViewController alloc]initWithConversationCellData:data senderName:data.title];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
           [cell setSeparatorInset:UIEdgeInsetsMake(0, 75, 0, 0)];
        if (indexPath.row == datasource.count) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
    }

    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }

    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)updateMessageNum:(NSNotification *)notification{
    [self.viewModel loadConversation];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
