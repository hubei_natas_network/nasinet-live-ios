//
//  MessageChatViewController.h
//  Meet1V1
//
//  Created by yun on 2020/1/19.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

#import <ImSDK.h>
#import <TUIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageChatViewController : RootViewController

- (instancetype)initWithConversationCellData:(TUIConversationCellData *)data;
- (instancetype)initWithConversationCellData:(TUIConversationCellData *)data senderName:(NSString *)senderName;

@end

NS_ASSUME_NONNULL_END
