//
//  SystemMsgViewController.m
//  Nasi
//
//  Created by yun on 2020/1/19.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "SystemMsgViewController.h"
#import "H5ViewController.h"

#import "SystemMessageCell.h"
#import "SystemMessageModel.h"

#import <ImSDK.h>

#define pagesize 20

@interface SystemMsgViewController ()<UITableViewDelegate,UITableViewDataSource>{
    
    NSMutableArray          *datasource;
}

@end

@implementation SystemMsgViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    TIMConversation *conv = [[TIMManager sharedInstance] getConversation:TIM_C2C receiver:[configManager appConfig].txim_admin];
    [conv setReadMessage:nil succ:nil fail:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"系统消息";
    self.view.backgroundColor = [UIColor colorWithHexString:@"FAFAFA"];
    
    datasource = [NSMutableArray array];
    
    [self.view addSubview:self.tableView];
    self.tableView.estimatedRowHeight = 0;
    self.tableView.backgroundColor = CLineColor;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 100;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    [self removeTableMJFooter];
    
    [SystemMessageCell registerWithTableView:self.tableView];
    
    [self reqData];
}

- (void)headerRereshing{
    [datasource removeAllObjects];
    [self reqData];
}

- (void)footerRereshing{
    int page = ceil((datasource.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    [self reqData];
}

- (void)reqData{
    NSString *lastid = @"";
    if (datasource.count > 0) {
        SystemMessageModel *model = datasource[datasource.count - 1];
        lastid = [NSString stringWithFormat:@"%d", model.msgid];
    }
    NSDictionary *param = @{
        @"lastid":lastid,
        @"size":@(pagesize)
    };
    [CommonManager POST:@"Message/getSystemMsg" parameters:param success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSArray *models = [NSArray yy_modelArrayWithClass:[SystemMessageModel class] json:responseObject[@"data"]];
                if (models.count < pagesize) {
                    [self removeTableMJFooter];
                }else{
                    [self setupTableViewMJFooter];
                }
                [self->datasource addObjectsFromArray:models];
                [self.tableView reloadData];
                //设置已读
                NSArray *conversations = [[TIMManager sharedInstance] getConversationList];
                for (TIMConversation *conv in conversations) {
                    if ([[conv getReceiver] isEqualToString:[configManager appConfig].txim_admin]) {
                        [conv setReadMessage:[conv getLastMsg] succ:nil fail:nil];
                    }
                }
            });
        }
    } failure:^(NSError *error) {
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SystemMessageCell *cell = [SystemMessageCell cellWithTableView:tableView indexPath:indexPath];
    cell.model = datasource[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SystemMessageModel *model = datasource[indexPath.row];
    if (model.link.length > 0) {
        H5ViewController *vc = [[H5ViewController alloc]init];
        vc.href = model.link;
        vc.title = model.title;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
