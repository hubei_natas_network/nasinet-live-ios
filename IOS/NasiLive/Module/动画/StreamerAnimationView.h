//
//  StreamerAnimationView.h
//  NasiLive
//
//  Created by yun11 on 2020/7/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol StreamerAnimationViewDelegate <NSObject>

- (void)streamerAnimationDelegate:(IMNotificationModel *)notificationModel;

@end

@interface StreamerAnimationView : UIView

@property(nonatomic,assign) id<StreamerAnimationViewDelegate> delegate;
@property(nonatomic,strong) NSMutableArray *streamerModels;
@property(nonatomic,assign) BOOL animating;

-(void)addModels:(IMNotificationModel *)notificationModel;
-(void)enAnimation;
-(void)stopAnimation;

@end

NS_ASSUME_NONNULL_END
