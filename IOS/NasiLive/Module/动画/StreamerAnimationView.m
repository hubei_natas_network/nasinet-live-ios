//
//  StreamerAnimationView.m
//  NasiLive
//
//  Created by yun11 on 2020/7/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "StreamerAnimationView.h"

#import <UIButton+WebCache.h>

@interface StreamerView : UIView

@property (strong, nonatomic) UIImageView *contentBackView;

@property (assign, nonatomic) CGFloat toX;

@end

@implementation StreamerView

- (instancetype)initWithNotification:(IMNotificationModel *)notificationModel{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, kTopHeight + 20, KScreenWidth, 77);
        [self creatUIWithNotification:(IMNotificationModel *)notificationModel];
    }
    return self;
}

- (void)creatUIWithNotification:(IMNotificationModel *)notificationModel{
    if (notificationModel.streamer.type == StreamerTypeVIP) {
        self.toX = 0;
        self.contentBackView = [[UIImageView alloc]initWithFrame:CGRectMake(KScreenWidth, 0, 243, 77)];
        self.contentBackView.image = IMAGE_NAMED(@"bg_streamer_vip");
        [self addSubview:self.contentBackView];
        
        UIButton *iconBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [iconBtn sd_setImageWithURL:[NSURL URLWithString:notificationModel.streamer.user.avatar] forState:UIControlStateNormal placeholderImage:IMAGE_NAMED(@"ic_avatar")];
        iconBtn.layer.cornerRadius = 25.0f/2;
        iconBtn.layer.masksToBounds = YES;
        [self.contentBackView addSubview:iconBtn];
        [iconBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(8);
            make.height.width.mas_equalTo(25);
            make.top.mas_equalTo(32);
        }];
        
        UILabel *nameLabel = [[UILabel alloc]init];
        nameLabel.text = notificationModel.streamer.user.nick_name;
        nameLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
        nameLabel.textColor = [UIColor colorWithHexString:@"#E4FF2E"];
        [self.contentBackView addSubview:nameLabel];
        [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(iconBtn.mas_right).offset(5);
            make.centerY.equalTo(iconBtn.mas_centerY);
        }];
        
        UILabel *label1 = [[UILabel alloc]init];
        label1.text = @"开通贵族";
        label1.font = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
        label1.textColor = [UIColor whiteColor];
        [self.contentBackView addSubview:label1];
        [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(nameLabel.mas_right).offset(3);
            make.centerY.equalTo(iconBtn.mas_centerY);
        }];
        
        UIImageView *vipImgView = [[UIImageView alloc]initWithImage:notificationModel.streamer.vip.icon];
        [self.contentBackView addSubview:vipImgView];
        [vipImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(17);
            make.height.mas_equalTo(21);
            make.left.equalTo(label1.mas_right).offset(3);
            make.centerY.equalTo(iconBtn.mas_centerY);
        }];
        
        UILabel *vipTitleLabel = [[UILabel alloc]init];
        vipTitleLabel.text = notificationModel.streamer.vip.title;
        vipTitleLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
        vipTitleLabel.textColor = [UIColor whiteColor];
        [self.contentBackView addSubview:vipTitleLabel];
        [vipTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(vipImgView.mas_right).offset(3);
            make.centerY.equalTo(iconBtn.mas_centerY);
        }];
    }else if (notificationModel.streamer.type == StreamerTypeAllChannelGift){
        self.toX = 7;
        self.contentBackView = [[UIImageView alloc]initWithFrame:CGRectMake(KScreenWidth, 0, 288, 49)];
        self.contentBackView.image = IMAGE_NAMED(@"bg_streamer_gift");
        [self addSubview:self.contentBackView];
        
        UIButton *iconBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [iconBtn sd_setImageWithURL:[NSURL URLWithString:notificationModel.streamer.user.avatar] forState:UIControlStateNormal placeholderImage:IMAGE_NAMED(@"ic_avatar")];
        iconBtn.layer.cornerRadius = 33.0f/2;
        iconBtn.layer.masksToBounds = YES;
        [self.contentBackView addSubview:iconBtn];
        [iconBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(5.5);
            make.height.width.mas_equalTo(33);
            make.top.mas_equalTo(10.5);
        }];
        
        UILabel *titleLabel = [[UILabel alloc]init];
        titleLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
        titleLabel.textColor = [UIColor whiteColor];
        [self.contentBackView addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(iconBtn.mas_right).offset(5);
            make.top.equalTo(iconBtn.mas_top).offset(2);
        }];
        titleLabel.text = [NSString stringWithFormat:@"%@在%@的直播间",notificationModel.streamer.user.nick_name,notificationModel.streamer.anchor.nick_name];
        NSMutableAttributedString *titleAttributedString = [[NSMutableAttributedString alloc] initWithString:titleLabel.text];
        NSRange titleR1 = [titleLabel.text rangeOfString:notificationModel.streamer.user.nick_name];
        NSRange titleR2 =[titleLabel.text rangeOfString:notificationModel.streamer.anchor.nick_name];
        [titleAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#FFF728"] range:titleR1];
        [titleAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#FFF728"] range:titleR2];
        [titleLabel setAttributedText:titleAttributedString];
        
        UILabel *subTitleLabel = [[UILabel alloc]init];
        subTitleLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
        subTitleLabel.textColor = [UIColor whiteColor];
        [self.contentBackView addSubview:subTitleLabel];
        [subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(iconBtn.mas_right).offset(5);
            make.bottom.equalTo(iconBtn.mas_bottom).offset(0);
        }];
        subTitleLabel.text = [NSString stringWithFormat:@"送出一个%@",notificationModel.streamer.gift.title];
        NSMutableAttributedString *subTitleAttributedString = [[NSMutableAttributedString alloc] initWithString:subTitleLabel.text];
        NSRange subTitleR = [subTitleLabel.text rangeOfString:notificationModel.streamer.gift.title];
        [subTitleAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#FFF728"] range:subTitleR];
        [subTitleLabel setAttributedText:subTitleAttributedString];
        
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        self.contentBackView.alpha = 1;
        self.contentBackView.mj_x = self.toX;
    }];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.7 animations:^{
            self.contentBackView.mj_x = -KScreenWidth;
        } completion:^(BOOL finished) {
            self.contentBackView.alpha = 0;
            [self.contentBackView removeFromSuperview];
        }];
        
    });
}

@end

@interface StreamerAnimationView (){
    NSTimer *animationTimer;//定时器
    StreamerView *streamerView;
}

@end

@implementation StreamerAnimationView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.layer.masksToBounds = NO;
        _streamerModels = [NSMutableArray array];
    }
    return self;
}
- (void)setAnimating:(BOOL)animating{
    _animating = animating;
}
- (void)addModels:(IMNotificationModel *)notificationModel{
    [_streamerModels addObject:notificationModel];
}
- (void)stopAnimation{
    [animationTimer invalidate];
    animationTimer = nil;
    _streamerModels = nil;
}
- (void)enAnimation{
    if (_streamerModels.count == 0 || _streamerModels == nil) {//判断队列中有item且不是满屏
        [animationTimer invalidate];
        animationTimer = nil;
        return;
    }
    IMNotificationModel *notificationModel = [_streamerModels firstObject];
    [_streamerModels removeObjectAtIndex:0];
    [self animationGiftPopView:notificationModel];
}
- (void)animationGiftPopView:(IMNotificationModel *)notificationModel{
    
    CGFloat seconds = 4;
    [self setAnimating:YES];
    
    streamerView = [[StreamerView alloc]initWithNotification:notificationModel];
    [self addSubview:streamerView];
    
    kWeakSelf(self)
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(seconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self->streamerView removeFromSuperview];
        [self setAnimating:NO];
        [weakself enAnimation];
    });
}

@end
