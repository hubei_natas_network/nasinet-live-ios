//
//  VipEnterAnimationView.m
//  NasiLive
//
//  Created by yun11 on 2020/7/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "VipEnterAnimationView.h"

#import <UIButton+WebCache.h>

#define ViewHeight 44

@interface VipStreamerView : UIView

@end

@implementation VipStreamerView

- (instancetype)initWithNotification:(IMNotificationModel *)notificationModel{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, KScreenWidth, ViewHeight);
        [self creatUIWithNotification:(IMNotificationModel *)notificationModel];
    }
    return self;
}

- (void)creatUIWithNotification:(IMNotificationModel *)notificationModel{
    UIImageView *contentBackView = [[UIImageView alloc]initWithFrame:CGRectMake(KScreenWidth, 0, 250, ViewHeight)];
    contentBackView.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_vip_enter_%d",notificationModel.chat.sender.vip_level]];
    [self addSubview:contentBackView];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
    titleLabel.textColor = [UIColor whiteColor];
    [contentBackView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(11);
        make.centerY.equalTo(contentBackView.mas_centerY);
    }];
    
    NSString *imgName;
    if (notificationModel.chat.sender.vip_level > 0) {
        imgName = [NSString stringWithFormat:@"bg_vip_enter_%d",notificationModel.chat.sender.vip_level];
    }else{
        imgName = @"bg_guardian_enter";
    }
    contentBackView.image = IMAGE_NAMED(imgName);
    
    titleLabel.text = [NSString stringWithFormat:@"%@ 进入直播间",notificationModel.chat.sender.nick_name];
    
    NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] initWithString:titleLabel.text attributes:nil];
    NSAttributedString *speaceString = [[NSAttributedString alloc]initWithString:@" "];
    
    //守护标识
    NSTextAttachment *guardAttchment = [[NSTextAttachment alloc]init];
    guardAttchment.bounds = CGRectMake(0, -3, 34, 15);//设置frame
    guardAttchment.image = [UIImage imageNamed:@"ic_guardian"];
    NSAttributedString *guardString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(guardAttchment)];
    
    //vip标识
    NSTextAttachment *vipAttchment = [[NSTextAttachment alloc]init];
    vipAttchment.bounds = CGRectMake(0, -4, 16, 19);//设置frame
    vipAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"vip_level_%d",notificationModel.chat.sender.vip_level]];//设置图片
    NSAttributedString *vipString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(vipAttchment)];
    
    NSRange nameRange = NSMakeRange(0, notificationModel.chat.sender.nick_name.length+1);
    [titleStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"#FFF727"] range:nameRange];
    
    if (notificationModel.chat.is_guardian) {
        //插入守护图标
        [titleStr insertAttributedString:speaceString atIndex:0];//插入到第几个下标
        [titleStr insertAttributedString:guardString atIndex:0];//插入到第几个下标
    }
    
    //插入VIP图标
    if (notificationModel.chat.sender.vip_level > 0) {
        [titleStr insertAttributedString:speaceString atIndex:0];//插入到第几个下标
        [titleStr insertAttributedString:vipString atIndex:0];//插入到第几个下标
    }
    
    [titleLabel setAttributedText:titleStr];
    
    [UIView animateWithDuration:0.3 animations:^{
        contentBackView.alpha = 1;
        contentBackView.mj_x = 15;
    }];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.7 animations:^{
            contentBackView.mj_x = -KScreenWidth;
        } completion:^(BOOL finished) {
            contentBackView.alpha = 0;
            [contentBackView removeFromSuperview];
        }];
        
    });
}

@end

@interface VipEnterAnimationView (){
    NSTimer *animationTimer;//定时器
    VipStreamerView *streamerView;
}

@end

@implementation VipEnterAnimationView

+ (instancetype)createVipEnterAnimationViewWithYPosition:(CGFloat)y{
    return [[VipEnterAnimationView alloc]initWithFrame:CGRectMake(0, y, kScreenWidth, ViewHeight)];
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.masksToBounds = NO;
        _streamerModels = [NSMutableArray array];
    }
    return self;
}

- (void)setAnimating:(BOOL)animating{
    _animating = animating;
}
- (void)addModels:(IMNotificationModel *)notificationModel{
    [_streamerModels addObject:notificationModel];
}
- (void)stopAnimation{
    [animationTimer invalidate];
    animationTimer = nil;
    _streamerModels = nil;
}
- (void)enAnimation{
    if (_streamerModels.count == 0 || _streamerModels == nil) {//判断队列中有item且不是满屏
        [animationTimer invalidate];
        animationTimer = nil;
        return;
    }
    IMNotificationModel *notificationModel = [_streamerModels firstObject];
    [_streamerModels removeObjectAtIndex:0];
    [self animationGiftPopView:notificationModel];
}
- (void)animationGiftPopView:(IMNotificationModel *)notificationModel{
    
    CGFloat seconds = 4;
    [self setAnimating:YES];
    
    streamerView = [[VipStreamerView alloc]initWithNotification:notificationModel];
    [self addSubview:streamerView];
    
    kWeakSelf(self);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(seconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self->streamerView removeFromSuperview];
        [weakself setAnimating:NO];
        if (self->_streamerModels.count > 0) {
            [weakself enAnimation];
        }
    });
}

@end
