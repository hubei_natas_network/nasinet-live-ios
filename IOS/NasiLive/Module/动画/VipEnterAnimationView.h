//
//  VipEnterAnimationView.h
//  NasiLive
//
//  Created by yun11 on 2020/7/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol VipEnterAnimationViewDelegate <NSObject>

- (void)vipEnterAnimationDelegate:(IMNotificationModel *)notificationModel;

@end

@interface VipEnterAnimationView : UIView

@property(nonatomic,assign) id<VipEnterAnimationViewDelegate> delegate;
@property(nonatomic,strong) NSMutableArray *streamerModels;
@property(nonatomic,assign) BOOL animating;

+ (instancetype)createVipEnterAnimationViewWithYPosition:(CGFloat)y;

- (void)addModels:(IMNotificationModel *)notificationModel;
- (void)enAnimation;
- (void)stopAnimation;

@end

NS_ASSUME_NONNULL_END
