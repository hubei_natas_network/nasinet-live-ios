//
//  NormalGiftBackView.m
//
//  Created by yun11 on 2020/5/8.
//  Copyright © 2019 yun11. All rights reserved.
//

#import "NormalGiftBackView.h"

@implementation NormalGiftBackView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
//  去除连送礼物view的点击
- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event{
    UIView *hitView = [super hitTest:point withEvent:event];
    if(hitView == self){
        return nil;
    }
    return hitView;
}

@end
