//
//  GiftShowView.m
//  NASI
//
//  Created by 王敏欣 on 2017/1/9.
//  Copyright © 2017年 cat. All rights reserved.
//
#import "GiftShowView.h"
#import "SDWebImageManager.h"


@implementation GiftShowView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.layer.masksToBounds = NO;
        _giftModels = [NSMutableArray array];
    }
    return self;
}
- (void)setAnimating:(BOOL)animating{
    _animating = animating;
}
- (void)addModels:(IMNotificationModel *)notificationModel{
    [_giftModels addObject:notificationModel];
}
- (void)stopAnimationGift{
    [animationGiftTime invalidate];
    animationGiftTime = nil;
    _giftModels = nil;
}
- (void)enAnimationGift{
    if (_giftModels.count == 0 || _giftModels == nil) {//判断队列中有item且不是满屏
        [animationGiftTime invalidate];
        animationGiftTime = nil;
        return;
    }
    IMNotificationModel *notificationModel = [_giftModels firstObject];
    [_giftModels removeObjectAtIndex:0];
    [self animationGiftPopView:notificationModel];
}
-(void)animationGiftPopView:(IMNotificationModel *)notificationModel{
    
    CGFloat seconds = notificationModel.gift.duration * 1.0 /1000;
    if (seconds == 0) {
        seconds = 2;
    }
    [self setAnimating:YES];

    if (notificationModel.gift.animat_type == GiftAnimatTypeSVGA) {
        SVGAParser *parser = [[SVGAParser alloc] init];
        [parser parseWithURL:[NSURL URLWithString:notificationModel.gift.animation] completionBlock:^(SVGAVideoEntity * _Nullable videoItem) {
            self->animatView = [[AnimationView alloc]initWithGiftData:notificationModel andVideoitem:videoItem];
            [self addSubview:self->animatView];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(seconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self->animatView removeFromSuperview];
                [self setAnimating:NO];
                if (self->_giftModels.count >0) {
                    [self.delegate animationGiftdelegate:nil];
                }
            });
        } failureBlock:nil];
    }else{
        animatView = [[AnimationView alloc]initWithGiftData:notificationModel andVideoitem:nil];
        [self addSubview:animatView];

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(seconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self->animatView removeFromSuperview];
            [self setAnimating:NO];
            if (self->_giftModels.count >0) {
                [self.delegate animationGiftdelegate:nil];
            }
        });
    }
}
@end
