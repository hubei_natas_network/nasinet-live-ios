//
//  GiftShowView.h
//  NASI
//
//  Created by 王敏欣 on 2017/1/9.
//  Copyright © 2017年 cat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnimationView.h"
#import <SVGAPlayer/SVGA.h>

@protocol GiftShowViewDelegate <NSObject>

-(void)animationGiftdelegate:(IMNotificationModel *)notificationModel;

@end

@interface GiftShowView : UIView
{
    NSTimer *animationGiftTime;//豪华礼物定时器
    AnimationView *animatView;
    SVGAPlayer *player;
}
@property(nonatomic,assign)id<GiftShowViewDelegate>delegate;
@property(nonatomic,strong) NSMutableArray *giftModels;
@property(nonatomic,assign) BOOL animating;

-(void)addModels:(IMNotificationModel *)notificationModel;
-(void)enAnimationGift;
-(void)stopAnimationGift;


@end
