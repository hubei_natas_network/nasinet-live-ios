//
//  AnimationView.m
//  NASI
//
//  Created by Boom on 2018/10/16.
//  Copyright © 2018年 cat. All rights reserved.
//

#import "AnimationView.h"
#import <YYWebImage/YYWebImage.h>

@implementation AnimationView{
}

- (instancetype)initWithGiftData:(IMNotificationModel *)notificationModel andVideoitem:(SVGAVideoEntity * _Nullable)videoitem{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, KScreenWidth, KScreenHeight);
        [self creatUIWithGiftData:notificationModel andVideoitem:videoitem];
    }
    return self;
}
- (void)creatUIWithGiftData:(IMNotificationModel *)notificationModel andVideoitem:(SVGAVideoEntity * _Nullable)videoitem{
    if (videoitem) {
        CGRect frame = CGRectMake(0, (KScreenHeight-(KScreenWidth/videoitem.videoSize.width*videoitem.videoSize.height))/2, KScreenWidth, (KScreenWidth/videoitem.videoSize.width*videoitem.videoSize.height));
        SVGAPlayer *player = [[SVGAPlayer alloc] initWithFrame:frame];
        [self addSubview:player];
        player.videoItem = videoitem;
        [player startAnimation];
    }else if (notificationModel.gift.animat_type == GiftAnimatTypeGif){
        UIImageView *imgView = [YYAnimatedImageView new];
        imgView.frame = CGRectMake(0, 0, KScreenWidth, KScreenHeight);
        imgView.yy_imageURL = [NSURL URLWithString:notificationModel.gift.animation];
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:imgView];
    }else{
//        UIImageView *imgView = [YYAnimatedImageView new];
//        imgView.frame = CGRectMake((KScreenWidth - 100)*0.5, (KScreenHeight - 100)*0.5, 100, 100);
//        imgView.yy_imageURL = [NSURL URLWithString:notificationModel.gift.animation];
//        imgView.contentMode = UIViewContentModeScaleAspectFit;
//        [self addSubview:imgView];
    }

    NSString *titleStr = [NSString stringWithFormat:@"%@ 赠送给主播 %@ x%d",notificationModel.gift.sender.nick_name,notificationModel.gift.title,notificationModel.gift.count];
    CGFloat titleWidth = [titleStr boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 30) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]} context:nil].size.width;
    UIImageView *titleBackImgView = [[UIImageView alloc]initWithFrame:CGRectMake(KScreenWidth, 110, 35+titleWidth+20, 30)];
    titleBackImgView.image = [UIImage imageNamed:@"bg_notice_gift"];
    titleBackImgView.alpha = 0.5;
    titleBackImgView.layer.cornerRadius = 15;
    titleBackImgView.layer.masksToBounds = YES;
    [self addSubview:titleBackImgView];
    
    UIImageView *giftIconImgView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 7.5, 15, 15)];
    [giftIconImgView sd_setImageWithURL:[NSURL URLWithString:notificationModel.gift.icon] placeholderImage:[UIImage imageNamed:@"ic_giftbox"]];
    [titleBackImgView addSubview:giftIconImgView];
    UILabel *titLabel = [[UILabel alloc]initWithFrame:CGRectMake(giftIconImgView.right+10, 0, titleWidth+20, 30)];
    titLabel.text = titleStr;
    titLabel.textColor = [UIColor whiteColor];
    titLabel.font = [UIFont systemFontOfSize:14];
    [titleBackImgView addSubview:titLabel];
    [UIView animateWithDuration:0.2 animations:^{
        titleBackImgView.alpha = 1;
        titleBackImgView.mj_x = 10;
    }];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.2 animations:^{
            titleBackImgView.alpha = 0;
            titleBackImgView.mj_x = -KScreenWidth;
        } completion:^(BOOL finished) {
            [titleBackImgView removeFromSuperview];
        }];

    });
}
@end
