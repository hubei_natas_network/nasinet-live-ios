//
//  AnimationView.h
//  NASI
//
//  Created by Boom on 2018/10/16.
//  Copyright © 2018年 cat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVGAPlayer/SVGA.h>

#import "IMNotificationModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AnimationView : UIView

- (instancetype)initWithGiftData:(IMNotificationModel *)notificationModel andVideoitem:(SVGAVideoEntity * _Nullable)videoitem;

@end

NS_ASSUME_NONNULL_END
