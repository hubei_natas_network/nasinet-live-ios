//
//  SharePanelView.h
//  NasiLive
//
//  Created by yun11 on 2020/5/25.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ShareSDK/ShareSDK.h>

NS_ASSUME_NONNULL_BEGIN

@class SharePanelView;

typedef NS_ENUM(NSInteger, PYShareChannel) {
    PYShareChannelWechat,
    PYShareChannelPengyouquan,
    PYShareChannelQQ,
    PYShareChannelWibo,
    PYShareChannelQZone,
    PYShareChannelMore,
    PYShareChannelReport,
    PYShareChannelSave,
    PYShareChannelCollect,
    PYShareChannelLink
};

@protocol SharePanelViewDelegate <NSObject>

@optional
- (void)shareWithChannel:(PYShareChannel)channel panel:(SharePanelView *)panelView;

@end

@interface SharePanelView : UIView

+ (instancetype)showPanelInView:(UIView *)superView;
+ (instancetype)showPanelInView:(UIView *)superView channel1:(NSArray *)channel1Arr channel2:(NSArray *)channel2Arr;

+ (SSDKPlatformType)getSSDKPlatformTypeBy:(PYShareChannel)channel;

- (void)setCollectBtnSelected:(BOOL)selected;

@property (weak, nonatomic) id<SharePanelViewDelegate> delegate;

@end
NS_ASSUME_NONNULL_END
