//
//  SearchViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/4/20.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "SearchViewController.h"

#import "MomentListViewController.h"
#import "SVideoListViewController.h"
#import "LiveListViewController.h"
#import "AnchorListViewController.h"

#import "ReportCategoryViewController.h"

#import "SVScaleVideoView.h"
#import "SVCommentView.h"

#import "JXCategoryTitleView.h"
#import "JXCategoryView.h"

#import "SharePanelView.h"


@interface SearchViewController ()<JXCategoryViewDelegate,JXCategoryListContainerViewDelegate,UITextFieldDelegate,SVScaleVideoViewProtocol,SVVideoViewDelegate>

@property (nonatomic, strong) NSArray <NSString *>          *titles;

@property (nonatomic, strong) JXCategoryTitleView           *categoryView;
@property (strong, nonatomic) JXCategoryListContainerView   *listContainerView;

@property (strong, nonatomic) MomentListViewController      *momentListVc;
@property (strong, nonatomic) SVideoListViewController      *svListVc;
@property (strong, nonatomic) LiveListViewController        *liveListVc;
@property (strong, nonatomic) AnchorListViewController      *anchorListVc;

@property (weak, nonatomic) IBOutlet UITextField        *keywordTextFiled;

@property (weak, nonatomic) IBOutlet UIView             *contentView;

@property (nonatomic, weak) SVScaleVideoView            *scaleView;

@property (strong, nonatomic) SVCommentView                     *commentView;

@end

@implementation SearchViewController
@synthesize currentListVC;

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //处于第一个item的时候，才允许屏幕边缘手势返回
    self.navigationController.interactivePopGestureRecognizer.enabled = (self.categoryView.selectedIndex == 0);
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //离开页面的时候，需要恢复屏幕边缘手势，不能影响其他页面
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.keywordTextFiled.delegate = self;
    [self.keywordTextFiled becomeFirstResponder];
    
    _titles = @[@"动态", @"短视频", @"直播", @"用户"];
    
    _categoryView = [[JXCategoryTitleView alloc] init];
    self.categoryView.titles = self.titles;
    self.categoryView.backgroundColor = [UIColor clearColor];
    self.categoryView.delegate = self;
    self.categoryView.titleFont = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
    self.categoryView.titleSelectedColor = MAIN_COLOR;
    self.categoryView.titleColor = [UIColor colorWithHexString:@"#83796A"];
    self.categoryView.titleColorGradientEnabled = YES;
    self.categoryView.titleLabelZoomEnabled = NO;
    self.categoryView.contentScrollViewClickTransitionAnimationEnabled = NO;
    
    self.listContainerView = [[JXCategoryListContainerView alloc] initWithType:JXCategoryListContainerType_ScrollView delegate:self];
    self.listContainerView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.listContainerView];
    
    self.categoryView.listContainer = self.listContainerView;
    [self.contentView addSubview:self.categoryView];
    
    [self.categoryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        make.height.mas_equalTo(30);
    }];
    
    [self.listContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.categoryView.mas_bottom);
        make.left.right.bottom.mas_equalTo(0);
    }];
    
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorColor = MAIN_COLOR;
    lineView.indicatorWidth = 12;
    lineView.indicatorHeight = 3;
    lineView.indicatorCornerRadius = 1.5;
    self.categoryView.indicators = @[lineView];
}

#pragma mark - JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
    switch (index) {
        case 0:
            [self.momentListVc searchKeyword:self.keywordTextFiled.text];
            break;
        case 1:
            [self.svListVc searchKeyword:self.keywordTextFiled.text];
            break;
        case 2:
            [self.liveListVc searchKeyword:self.keywordTextFiled.text];
            break;
        case 3:
            [self.anchorListVc searchKeyword:self.keywordTextFiled.text];
            break;
        default:
            break;
    }
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didScrollSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

#pragma mark - JXCategoryListContainerViewDelegate

- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index{
    switch (index) {
        case 0:{
            self.momentListVc = [[MomentListViewController alloc]init];
            self.momentListVc.listType = MomentListTypeSearch;
            return self.momentListVc;
        }
            break;
        case 1:{
            SVideoListViewController *listVc = [[SVideoListViewController alloc]init];
            listVc.type = SvideoListTypeSearch;
            kWeakSelf(listVc)
            @weakify(self);
            listVc.itemClickBlock = ^(NSArray * _Nonnull videos, NSInteger index) {
                @strongify(self);
                [self showVideoVCWithVideos:videos datasourceVC:weaklistVc index:index];
            };
            self.svListVc = listVc;
            return self.svListVc;
        }
            break;
        case 2:{
            self.liveListVc = [[LiveListViewController alloc]init];
            self.liveListVc.listType = LiveListTypeSearch;
            return self.liveListVc;
        }
            break;
        case 3:{
            self.anchorListVc = [[AnchorListViewController alloc]init];
            self.anchorListVc.listType = AnchorListTypeSearch;
            return self.anchorListVc;
        }
            break;
        default:
            break;
    }
    return nil;
}

- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.text.length == 0) {
        return NO;
    }
    [self.view endEditing:YES];
    [self.momentListVc searchKeyword:textField.text];
    [self.svListVc searchKeyword:textField.text];
    [self.liveListVc searchKeyword:textField.text];
    [self.anchorListVc searchKeyword:textField.text];
    return YES;
}

- (IBAction)cancelBtnClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)showVideoVCWithVideos:(NSArray *)videos datasourceVC:(SVideoListViewController *)vc index:(NSInteger)index {
    SVScaleVideoView *scaleView = [[SVScaleVideoView alloc] initWithVC:self datasourceVC:vc videos:videos index:index];
    scaleView.videoView.delegate = self;
    [scaleView show];
    
    self.scaleView = scaleView;
}


#pragma mark - SharePanelViewDelegate
- (void)shareWithChannel:(PYShareChannel)channel panel:(nonnull SharePanelView *)panelView{
    if (channel == PYShareChannelReport) {
        ReportCategoryViewController *vc = [[ReportCategoryViewController alloc]init];
        vc.relateid = self.scaleView.videoView.currentPlayView.model.videoid;
        vc.type = ReportTypeShortVideo;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (channel == PYShareChannelCollect){
        if (![CommonManager checkAndLogin]) {
            return;
        }
        [commonManager showLoadingAnimateInWindow];
        [CommonManager POST:@"Shortvideo/collect" parameters:@{@"videoid":@(self.scaleView.videoView.currentPlayView.model.videoid), @"type":@(!self.scaleView.videoView.currentPlayView.model.collected)} success:^(id responseObject) {
            [commonManager hideAnimateHud];
            if (RESP_SUCCESS(responseObject)) {
                [commonManager showSuccessAnimateInWindow];
                self.scaleView.videoView.currentPlayView.model.collected = !self.scaleView.videoView.currentPlayView.model.collected;
                [panelView setCollectBtnSelected:self.scaleView.videoView.currentPlayView.model.collected];
            }else{
                RESP_SHOW_ERROR_MSG(responseObject);
            }
        } failure:^(NSError *error) {
            [commonManager hideAnimateHud];
            RESP_FAILURE;
        }];
    }else if (channel == PYShareChannelLink){
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = [NSString stringWithFormat:@"%@%d",[configManager appConfig].share_shortvideo_url,self.scaleView.videoView.currentPlayView.model.videoid];
        [MBProgressHUD showTipMessageInView:@"链接已复制到剪贴板"];
    }else{
        SSDKPlatformType type = [SharePanelView getSSDKPlatformTypeBy:channel];
        //统一创建分享参数
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:self.scaleView.videoView.currentPlayView.model.title
                                         images:self.scaleView.videoView.currentPlayView.model.thumb_url
                                            url:[NSURL URLWithString:[NSString stringWithFormat:@"%@%d",[configManager appConfig].share_shortvideo_url,self.scaleView.videoView.currentPlayView.model.videoid]]
                                          title:@"推荐你看这个视频"
                                           type:SSDKContentTypeAuto];
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            switch (state) {
                case SSDKResponseStateSuccess:
                    [MBProgressHUD showTipMessageInView:@"分享成功"];
                    break;
                case SSDKResponseStateFail:
                {
                    [MBProgressHUD showTipMessageInView:@"分享失败"];
                    //失败
                    break;
                }
                case SSDKResponseStateCancel:
                    //取消
                    break;
                    
                default:
                    break;
            }
        }];
    }
}

#pragma mark - SVVideoViewDelegate
- (void)videoView:(SVVideoView *)videoView didClickIcon:(ShortVideoModel *)videoModel {
    UserSVPageController *vc = [[UserSVPageController alloc]init];
    vc.author = videoModel.author;
    vc.SvAttentUserBlock = ^(int userid, BOOL attented) {
        [videoView updateUserModel:userid attent:attented];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)videoView:(SVVideoView *)videoView didClickAttent:(nonnull ShortVideoModel *)videoModel {
    ShortVideoModel *model = videoModel;
    
    int type = model.author.isattent ? 0 : 1;
    [CommonManager POST:@"Anchor/attentAnchor" parameters:@{@"anchorid":@(model.author.userid),@"type":@(type)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            model.author.isattent = !model.author.isattent;
            model.author.fans_count = [responseObject[@"data"][@"fans_count"] intValue];
            videoView.currentPlayView.model = videoModel;
            [self.scaleView.videoView updateUserModel:model.author.userid attent:model.author.isattent];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}


- (void)videoView:(SVVideoView *)videoView didClickComment:(ShortVideoModel *)videoModel {
    if (!_commentView) {
        _commentView = [[SVCommentView alloc]initWithFrame:CGRectMake(0, KScreenHeight, KScreenWidth, KScreenHeight)];
        _commentView.CommentCountBlock = ^(int count) {
            videoView.currentPlayView.model = videoModel;
        };
        [kAppWindow addSubview:_commentView];
    }
    _commentView.videoModel = videoModel;
    [UIView animateWithDuration:0.3 animations:^{
        self->_commentView.mj_y = 0;
    }];
}

- (void)videoView:(SVVideoView *)videoView didClickShare:(ShortVideoModel *)videoModel {
    SharePanelView *panelView = [SharePanelView showPanelInView:kAppWindow];
    panelView.delegate = self;
    [panelView setCollectBtnSelected:videoModel.collected];
}

- (void)videoView:(SVVideoView *)videoView didScrollIsCritical:(BOOL)isCritical {
    
}

@end
