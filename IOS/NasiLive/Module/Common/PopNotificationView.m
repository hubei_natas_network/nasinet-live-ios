//
//  PopNotificationView.m
//  NasiLive
//
//  Created by yun11 on 2020/9/7.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "PopNotificationView.h"

@implementation PopNotificationView

- (void)setModel:(AdModel *)model{
    _model = model;
}

- (void)hide{
    if (self.onCloseClick) {
        self.onCloseClick();
    }
}

@end
