//
//  SharePanelView.m
//  NasiLive
//
//  Created by yun11 on 2020/5/25.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "SharePanelView.h"

@interface ShareItemButton : UIButton

@end

@implementation ShareItemButton

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.imageView sizeToFit];
    [self.titleLabel sizeToFit];
    
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    
    CGFloat imgW = self.imageView.frame.size.width;
    CGFloat imgH = self.imageView.frame.size.height;
    
    self.imageView.frame = CGRectMake((width - imgH) / 2, 0, imgW, imgH);
    
    CGFloat titleW = self.titleLabel.frame.size.width;
    CGFloat titleH = self.titleLabel.frame.size.height;
    
    self.titleLabel.frame = CGRectMake((width - titleW) / 2, height - titleH, titleW, titleH);
}

@end

@interface SharePanelView()

@property (weak, nonatomic) ShareItemButton *collectBtn;

@property (strong, nonatomic) NSDictionary *channelDictionary;
@property (strong, nonatomic) UIView *contentView;

@property (strong, nonatomic) NSArray *channelArray1;
@property (strong, nonatomic) NSArray *channelArray2;

@end

@implementation SharePanelView

+ (instancetype)showPanelInView:(UIView *)superView{
    SharePanelView *panelView = [[self alloc]initWithFrame:superView.bounds channel1:nil channel2:nil];
    [superView addSubview:panelView];
    [panelView show];
    
    return panelView;
}

+ (instancetype)showPanelInView:(UIView *)superView channel1:(nonnull NSArray *)channel1Arr channel2:(nonnull NSArray *)channel2Arr{
    SharePanelView *panelView = [[self alloc]initWithFrame:superView.bounds channel1:channel1Arr channel2:channel2Arr];
    [superView addSubview:panelView];
    [panelView show];
    
    return panelView;
}

- (instancetype)initWithFrame:(CGRect)frame channel1:(NSArray *)channel1Arr channel2:(NSArray *)channel2Arr{
    if (self = [super initWithFrame:frame]) {
        UIView *shadowView = [[UIView alloc]initWithFrame:self.bounds];
        shadowView.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
            [self hidden];
        }];
        [shadowView addGestureRecognizer:tap];
        [self addSubview:shadowView];
        
        self.contentView = [[UIView alloc]initWithFrame:CGRectMake(0, self.height, self.width, 0)];
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.contentView];
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.width, 44)];
        titleLabel.text = @"分享到";
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
        [self.contentView addSubview:titleLabel];
        
        CGFloat btnW = 50;
        
        UIScrollView *scrollView1 = [[UIScrollView alloc]initWithFrame:CGRectMake(0, titleLabel.mj_maxY + 15, self.width, 70)];
        scrollView1.showsVerticalScrollIndicator = NO;
        scrollView1.showsHorizontalScrollIndicator = NO;
        [self.contentView addSubview:scrollView1];
        
        NSMutableArray *dataArr1 = [NSMutableArray array];
        if (!channel1Arr) {
            channel1Arr = @[@(PYShareChannelWechat),@(PYShareChannelPengyouquan),@(PYShareChannelQQ),@(PYShareChannelWibo),@(PYShareChannelQZone),@(PYShareChannelMore)];
        }
        self.channelArray1 = channel1Arr;
        for (int i=0; i<channel1Arr.count; i++) {
            [dataArr1 addObject:self.channelDictionary[channel1Arr[i]]];
        }
        
        for (int i = 0; i < dataArr1.count; i++) {
            ShareItemButton *btn = [ShareItemButton new];
            btn.frame = CGRectMake(15 + ((btnW+15)*i), 0, btnW, 70);
            [btn setImage:IMAGE_NAMED(dataArr1[i][@"icon"]) forState:UIControlStateNormal];
            [btn setTitle:dataArr1[i][@"title"] forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont systemFontOfSize:12];
            [btn setTitleColor:CFontColor forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(panelBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            btn.tag = i;
            [scrollView1 addSubview:btn];
            if ([dataArr1[i][@"title"] isEqualToString:@"收藏"]) {
                self.collectBtn = btn;
            }
        }
        [scrollView1 setContentSize:CGSizeMake(15*2 + btnW * dataArr1.count + 15*(dataArr1.count - 1) , 0)];
        
        UIView *breakline1 = [[UIView alloc]initWithFrame:CGRectMake(15, scrollView1.mj_maxY + 15, self.width - 30, 0.5)];
        breakline1.backgroundColor = CLineColor;
        [self.contentView addSubview:breakline1];
        
        UIScrollView *scrollView2 = [[UIScrollView alloc]initWithFrame:CGRectMake(0, breakline1.mj_maxY + 15, self.width, 70)];
        scrollView2.showsVerticalScrollIndicator = NO;
        scrollView2.showsHorizontalScrollIndicator = NO;
        [self.contentView addSubview:scrollView2];
        
        NSMutableArray *dataArr2 = [NSMutableArray array];
        if (!channel2Arr) {
            channel2Arr = @[@(PYShareChannelReport),@(PYShareChannelSave),@(PYShareChannelCollect),@(PYShareChannelLink)];
        }
        self.channelArray2 = channel2Arr;
        for (int i=0; i<channel2Arr.count; i++) {
            [dataArr2 addObject:self.channelDictionary[channel2Arr[i]]];
        }
        for (int i = 0; i < dataArr2.count; i++) {
            ShareItemButton *btn = [ShareItemButton new];
            btn.frame = CGRectMake(15 + ((btnW+15)*i), 0, btnW, 70);
            [btn setImage:IMAGE_NAMED(dataArr2[i][@"icon"]) forState:UIControlStateNormal];
            [btn setTitle:dataArr2[i][@"title"] forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont systemFontOfSize:12];
            [btn setTitleColor:CFontColor forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(panelBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            btn.tag = i + dataArr1.count;
            [scrollView2 addSubview:btn];
            if ([dataArr1[i][@"title"] isEqualToString:@"收藏"]) {
                self.collectBtn = btn;
            }
        }
        [scrollView2 setContentSize:CGSizeMake(15*2 + btnW * dataArr1.count + 15*(dataArr1.count - 1) , 0)];
        
        UIView *breakline2 = [[UIView alloc]initWithFrame:CGRectMake(15, scrollView2.mj_maxY + 15, self.width - 30, 0.5)];
        breakline2.backgroundColor = CLineColor;
        [self.contentView addSubview:breakline2];
        
        UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelBtn setTitleColor:CFontColor forState:UIControlStateNormal];
        [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        cancelBtn.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
        cancelBtn.frame = CGRectMake(0, breakline2.mj_maxY, self.width, 44);
        [cancelBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:cancelBtn];
        
        self.contentView.height = cancelBtn.mj_maxY + kBottomSafeHeight;
        
    }
    return self;
}

- (void)setCollectBtnSelected:(BOOL)selected{
    self.collectBtn.selected = selected;
}

- (void)panelBtnClick:(UIButton *)sender{
    if ([self.delegate respondsToSelector:@selector(shareWithChannel:panel:)]) {
        PYShareChannel channel;
        if (sender.tag < self.channelArray1.count) {
            channel = [self.channelArray1[sender.tag] intValue];
        }else{
            channel = [self.channelArray2[sender.tag - self.channelArray1.count] intValue];
        }
        [self.delegate shareWithChannel:channel panel:self];
        if (channel != PYShareChannelCollect) {
            [self hidden];
        }
    }
}

- (void)cancelBtnClick{
    [self hidden];
}

- (void)show{
    [UIView animateWithDuration:0.25 animations:^{
        self.contentView.transform = CGAffineTransformMakeTranslation(0, -self.contentView.height);
    }];
}

- (void)hidden{
    [UIView animateWithDuration:0.2 animations:^{
        self.contentView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (NSDictionary *)channelDictionary{
    if (!_channelDictionary) {
        _channelDictionary = @{@(PYShareChannelWechat):@{@"title":@"微信",@"icon":@"ic_share_wechat"},
                              @(PYShareChannelPengyouquan):@{@"title":@"朋友圈",@"icon":@"ic_share_pengyouquan"},
                              @(PYShareChannelQQ):@{@"title":@"QQ",@"icon":@"ic_share_qq"},
                              @(PYShareChannelWibo):@{@"title":@"微博",@"icon":@"ic_share_weibo"},
                              @(PYShareChannelQZone):@{@"title":@"QQ空间",@"icon":@"ic_share_qzone"},
                              @(PYShareChannelMore):@{@"title":@"更多",@"icon":@"ic_share_more"},
                              @(PYShareChannelReport):@{@"title":@"举报",@"icon":@"ic_share_report"},
                              @(PYShareChannelSave):@{@"title":@"保存到手机",@"icon":@"ic_share_save"},
                              @(PYShareChannelCollect):@{@"title":@"收藏",@"icon":@"ic_share_collect"},
                              @(PYShareChannelLink):@{@"title":@"复制链接",@"icon":@"ic_share_link"}};
    }
    return _channelDictionary;
}

+ (SSDKPlatformType)getSSDKPlatformTypeBy:(PYShareChannel)channel{
    SSDKPlatformType type;
    switch (channel) {
        case PYShareChannelWechat:
            type = SSDKPlatformSubTypeWechatSession;
            break;
        case PYShareChannelPengyouquan:
            type = SSDKPlatformSubTypeWechatTimeline;
            break;
        case PYShareChannelQQ:
            type = SSDKPlatformSubTypeQQFriend;
            break;
        case PYShareChannelWibo:
            type = SSDKPlatformTypeSinaWeibo;
            break;
        case PYShareChannelQZone:
            type = SSDKPlatformSubTypeQZone;
            break;
        default:
            type = PYShareChannelWechat;
            break;
    }
    return type;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
