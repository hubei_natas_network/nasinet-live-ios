//
//  ReportCategoryViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/7/28.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"
#import "ReportViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReportCategoryViewController : RootViewController

@property (assign, nonatomic) ReportType type;
@property (assign, nonatomic) int relateid;

@end

NS_ASSUME_NONNULL_END
