//
//  SearchVcProtocal.h
//  NasiBBS
//
//  Created by yun11 on 2020/9/26.
//  Copyright © 2020 yun7. All rights reserved.
//

@protocol SearchVcProtocal <NSObject>

- (void)searchKeyword:(NSString *)keyword;

@end

