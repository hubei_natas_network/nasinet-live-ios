//
//  VipOpenPopView.m
//  MiyouBBS
//
//  Created by yun11 on 2021/3/10.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "VipOpenPopView.h"
#import "VipCenterViewController.h"
#import "InviteViewController.h"

@interface VipOpenPopView ()

@property (weak, nonatomic) UIView *superView;

@property (nonatomic, strong) UIView *backImageView;

@end

@implementation VipOpenPopView

- (instancetype)init{
    if (self = [super init]) {
        self = [[NSBundle mainBundle]loadNibNamed:@"VipOpenPopView" owner:self options:nil].firstObject;
    }
    return self;
}

+ (instancetype)showInWindow{
    VipOpenPopView *popView = [[self alloc]init];
    popView.superView = [popView appRootViewController].view;
    [popView show];
    return popView;
}

+ (instancetype)showInView:(UIView *)superView{
    VipOpenPopView *popView = [[self alloc]init];
    popView.superView = superView;
    [popView show];
    return popView;
}

- (void)show{
    self.centerX = self.superView.width/2;
    self.centerY = self.superView.height/2 - 20;
    [self.superView addSubview:self];
}

- (void)hide{
    [self.backImageView removeFromSuperview];
    self.backImageView = nil;
    [self removeFromSuperview];
}

- (void)willMoveToSuperview:(UIView *)newSuperview{
    if (newSuperview == nil) {
        return;
    }
    
    if (!self.backImageView) {
        self.backImageView = [[UIView alloc] initWithFrame:self.superView.bounds];
        self.backImageView.backgroundColor = [UIColor blackColor];
        self.backImageView.alpha = 0.3f;
        self.backImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    }
    [self.superView addSubview:self.backImageView];
    
    self.transform = CGAffineTransformMakeScale(0.4, 0.4);
    [UIView animateWithDuration:0.4f delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:20 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        self.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
    }];
    [super willMoveToSuperview:newSuperview];
}

- (IBAction)buyBtnClick:(id)sender {
    VipCenterViewController *vc = [[VipCenterViewController alloc]init];
    [[kAppDelegate getCurrentUIVC].navigationController pushViewController:vc animated:YES];
}

- (IBAction)inviteBtnClick:(id)sender {
    InviteViewController *vc = [[InviteViewController alloc]init];
    [[kAppDelegate getCurrentUIVC].navigationController pushViewController:vc animated:YES];
}

- (IBAction)closeBtnClick:(UIButton *)sender {
    [self hide];
}

- (UIViewController *)appRootViewController{
    UIViewController *appRootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *topVC = appRootVC;
    while (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    return topVC;
}

@end
