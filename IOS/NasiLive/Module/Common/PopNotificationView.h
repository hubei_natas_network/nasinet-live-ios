//
//  PopNotificationView.h
//  NasiLive
//
//  Created by yun11 on 2020/9/7.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PopNotificationViewDelegate <NSObject>

@optional

- (void)popNotificationViewClick:(AdModel *)model;

@end


typedef void (^OnCloseClickBlock)(void);

@interface PopNotificationView : UIView

+ (instancetype)showInWindowWithAdModel:(AdModel *)model delegate:(id<PopNotificationViewDelegate>)delegate;
+ (instancetype)showInView:(UIView *)superView adModel:(AdModel *)model delegate:(id<PopNotificationViewDelegate>)delegate;

@property (weak, nonatomic) id<PopNotificationViewDelegate> delegate;

@property (strong, nonatomic) AdModel *model;

@property (strong, nonatomic) OnCloseClickBlock onCloseClick;

- (void)show;
- (void)hide;

@end

NS_ASSUME_NONNULL_END
