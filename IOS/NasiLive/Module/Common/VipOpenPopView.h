//
//  VipOpenPopView.h
//  MiyouBBS
//
//  Created by yun11 on 2021/3/10.
//  Copyright © 2021 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^OnCloseClickBlock)(void);

@interface VipOpenPopView : UIView

+ (instancetype)showInWindow;
+ (instancetype)showInView:(UIView *)superView;

- (void)show;
- (void)hide;

@end

NS_ASSUME_NONNULL_END
