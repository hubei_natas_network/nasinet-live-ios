//
//  ReportViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/7/28.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

typedef NS_ENUM(NSInteger, ReportType){
    ReportTypeAnchor,
    ReportTypeShortVideo,
    ReportTypeMoment
};

NS_ASSUME_NONNULL_BEGIN

@interface ReportViewController : RootViewController

@property (copy, nonatomic) NSString *reportTitle;
@property (assign, nonatomic) ReportType type;
@property (assign, nonatomic) int relateid;

@end

NS_ASSUME_NONNULL_END
