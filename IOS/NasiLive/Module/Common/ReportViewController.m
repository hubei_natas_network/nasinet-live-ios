//
//  ReportViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/7/28.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "ReportViewController.h"

#import <TZImagePickerController.h>
#import <QCloudCOSXML.h>

@interface ReportViewController ()<UITextViewDelegate,TZImagePickerControllerDelegate>{
    NSMutableArray *imgUrlArr;
}

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *contentCountLabel;
@property (weak, nonatomic) IBOutlet UITextView *contentTextView;

@property (weak, nonatomic) IBOutlet UIImageView *imgView1;
@property (weak, nonatomic) IBOutlet UIImageView *imgView2;
@property (weak, nonatomic) IBOutlet UIImageView *imgView3;
@property (weak, nonatomic) IBOutlet UIImageView *imgView4;
@property (weak, nonatomic) IBOutlet UILabel *imgCountLabel;

@property (copy, nonatomic) NSString *api;

@end

@implementation ReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"用户举报";
    self.isShowLeftBack = YES;
    
    self.titleLabel.text = self.reportTitle;
    self.contentTextView.delegate = self;
    
    [self.imgView1 addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imgViewTap:)]];
    [self.imgView2 addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imgViewTap:)]];
    [self.imgView3 addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imgViewTap:)]];
    [self.imgView4 addGestureRecognizer: [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imgViewTap:)]];
    
    imgUrlArr = [NSMutableArray arrayWithCapacity:4];
    
    [CommonManager loadCosTempKey];
}

- (void)imgViewTap:(UITapGestureRecognizer *)tap{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
    imagePickerVc.allowTakeVideo = NO;
    imagePickerVc.allowPickingGif = NO;
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.showPhotoCannotSelectLayer = YES;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.naviTitleColor = [UIColor colorWithHexString:@"333333"];
    imagePickerVc.barItemTextColor = [UIColor colorWithHexString:@"333333"];
    imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        if (photos.count > 0) {
            UIImage *selImg = photos[0];
            ((UIImageView *)tap.view).image = selImg;
            NSString *fileName = [CommonManager getNameBaseCurrentTime:@".jpg"];
            NSString *flieUploadPath = [NSString stringWithFormat:@"%@/%@",configManager.appConfig.cos_folder_image,fileName];
            QCloudCOSXMLUploadObjectRequest* put = [QCloudCOSXMLUploadObjectRequest new];
            put.object = flieUploadPath;
            put.bucket = configManager.appConfig.cos_bucket;
            put.body = UIImageJPEGRepresentation(selImg, 1);
            [put setFinishBlock:^(QCloudUploadObjectResult *result, NSError* error) {
                //可以从 result 获取结果
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (result && !error) {
                        switch (tap.view.tag) {
                            case 1:{
                                if (self->imgUrlArr.count > 0) {
                                    [self->imgUrlArr replaceObjectAtIndex:0 withObject:result.location];
                                }else{
                                    [self->imgUrlArr addObject:result.location];
                                }
                                self.imgView2.hidden = NO;
                            }
                                break;
                            case 2:{
                                if (self->imgUrlArr.count > 1) {
                                    [self->imgUrlArr replaceObjectAtIndex:1 withObject:result.location];
                                }else{
                                    [self->imgUrlArr addObject:result.location];
                                }
                                self.imgView3.hidden = NO;
                            }
                                break;
                            case 3:{
                                if (self->imgUrlArr.count > 2) {
                                    [self->imgUrlArr replaceObjectAtIndex:2 withObject:result.location];
                                }else{
                                    [self->imgUrlArr addObject:result.location];
                                }
                                self.imgView4.hidden = NO;
                            }
                                break;
                            case 4:{
                                if (self->imgUrlArr.count > 3) {
                                    [self->imgUrlArr replaceObjectAtIndex:3 withObject:result.location];
                                }else{
                                    [self->imgUrlArr addObject:result.location];
                                }
                            }
                                break;
                            default:
                                break;
                        }
                        self.imgCountLabel.text = [NSString stringWithFormat:@"上传图片%lu/4",(unsigned long)self->imgUrlArr.count];
                    }else{
                        [commonManager hideAnimateHud];
                        [commonManager showErrorAnimateInWindow];
                    }
                });
            }];
            [[QCloudCOSTransferMangerService defaultCOSTransferManager] UploadObject:put];
        }
    }];
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (IBAction)submitBtnClick:(UIButton *)sender {
    if (self.contentTextView.text.length > 200) {
        [MBProgressHUD showTipMessageInView:@"举报内容过长"];
        return;
    }
    NSDictionary *param = @{@"title":self.reportTitle,
                            @"relateid":@(self.relateid),
                            @"content":self.contentTextView.text,
                            @"img_urls":[imgUrlArr componentsJoinedByString:@","]
    };
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:self.api parameters:param success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            [MBProgressHUD showTipMessageInView:@"举报成功"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                NSInteger index = [[self.navigationController viewControllers]indexOfObject:self];
                [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:index-2]animated:YES];
            });
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

#pragma mark ———————————————————————————————— UITextViewDelegate ——————————————————————————————————-
- (void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@"详细描述举报情况"]) {
        textView.text = @"";
    }
    textView.textColor = CFontColor;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length == 0 || [textView.text isEqualToString:@"详细描述举报情况"]) {
        textView.text = @"详细描述举报情况";
        textView.textColor = CFontColorLightGray;
    }
}

- (void)textViewDidChange:(UITextView *)textView{
    self.contentCountLabel.text = [NSString stringWithFormat:@"%lu/200",(unsigned long)self.contentTextView.text.length];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if (self.contentTextView.text.length + text.length > 200) {
        [MBProgressHUD showTipMessageInView:@"文本长度超出上限"];
        return NO;
    }
    return YES;
}

- (NSString *)api{
    if (!_api) {
        switch (self.type) {
            case ReportTypeAnchor:
                _api = @"Anchor/reoprt";
                break;
            case ReportTypeShortVideo:
                _api = @"Shortvideo/reoprt";
                break;
            case ReportTypeMoment:
                _api = @"Moment/reoprt";
                break;
            default:
                break;
        }
    }
    return _api;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
