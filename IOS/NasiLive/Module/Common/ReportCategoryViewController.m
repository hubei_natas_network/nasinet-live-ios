//
//  ReportCategoryViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/7/28.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "ReportCategoryViewController.h"

#import "ReportViewController.h"

@interface ReportCategoryViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSArray         *datasource;
}

@end

@implementation ReportCategoryViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"用户举报";
    self.isShowLeftBack = YES;
    
    datasource = @[@"色情低俗",@"政治敏感",@"违法犯罪",@"发布垃圾广告、售卖假货等",@"非原创内容",@"冒充官方",@"头像、昵称、签名违规",@"侵犯权益",@"涉嫌诈骗",@"疑似自我伤害",@"侮辱谩骂"];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self removeTableMJFooter];
    [self removeTableMJHeader];
    [self.view addSubview:self.tableView];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return datasource.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCellID"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UITableViewCellID"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = datasource[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ReportViewController *vc = [[ReportViewController alloc]init];
    vc.relateid = self.relateid;
    vc.reportTitle = datasource[indexPath.row];
    vc.type = self.type;
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
