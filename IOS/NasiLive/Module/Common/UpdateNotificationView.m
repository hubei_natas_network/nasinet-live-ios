//
//  UpdateNotificationView.m
//  NasiLive
//
//  Created by yun11 on 2020/9/7.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "UpdateNotificationView.h"

@interface UpdateNotificationView ()

@property (weak, nonatomic) UIView *superView;

@property (weak, nonatomic) IBOutlet UITextView *contentTextView;
@property (weak, nonatomic) IBOutlet UIButton *goNextBtn;

@property (nonatomic, strong) UIView *backImageView;


@end

@implementation UpdateNotificationView

- (instancetype)init{
    if (self = [super init]) {
        self = [[NSBundle mainBundle]loadNibNamed:@"UpdateNotificationView" owner:self options:nil].firstObject;
    }
    return self;
}

+ (instancetype)showInWindowWithAdModel:(AdModel *)model delegate:(id<PopNotificationViewDelegate>)delegate{
    UpdateNotificationView *popView = [[self alloc]init];
    popView.superView = [popView appRootViewController].view;
    popView.model = model;
    popView.delegate = delegate;
    [popView show];
    return popView;
}

+ (instancetype)showPopTextNotificationViewInView:(UIView *)superView adModel:(AdModel *)model delegate:(id<PopNotificationViewDelegate>)delegate{
    UpdateNotificationView *popView = [[self alloc]init];
    popView.superView = superView;
    popView.model = model;
    popView.delegate = delegate;
    [popView show];
    return popView;
}

- (void)show{
    self.centerX = self.superView.width/2;
    self.centerY = self.superView.height/2 - 20;
    [self.superView addSubview:self];
}

- (void)hide{
    [super hide];
    [self.backImageView removeFromSuperview];
    self.backImageView = nil;
    [self removeFromSuperview];
}

- (void)willMoveToSuperview:(UIView *)newSuperview{
    if (newSuperview == nil) {
        return;
    }
    
    if (!self.backImageView) {
        self.backImageView = [[UIView alloc] initWithFrame:self.superView.bounds];
        self.backImageView.backgroundColor = [UIColor blackColor];
        self.backImageView.alpha = 0.3f;
        self.backImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    }
    [self.superView addSubview:self.backImageView];
    
    self.transform = CGAffineTransformMakeScale(0.4, 0.4);
    [UIView animateWithDuration:0.4f delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:20 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        self.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
    }];
    [super willMoveToSuperview:newSuperview];
}

- (void)setModel:(AdModel *)model{
    [super setModel:model];
    self.contentTextView.text = model.title;
}

- (IBAction)goBtnClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(popNotificationViewClick:)]) {
        [self.delegate popNotificationViewClick:self.model];
    }
}

- (IBAction)closeBtnClick:(UIButton *)sender {
    [self hide];
}

- (UIViewController *)appRootViewController{
    UIViewController *appRootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *topVC = appRootVC;
    while (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    return topVC;
}


@end
