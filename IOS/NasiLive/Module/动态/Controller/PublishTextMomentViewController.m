//
//  PublishTextMomentViewController.m
//  Meet1V1
//
//  Created by yun on 2020/1/14.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "PublishTextMomentViewController.h"
#import "SelectGroupController.h"

@interface PublishTextMomentViewController ()<UITextViewDelegate>

@property (assign, nonatomic) int chooseGroupId;

@property (weak, nonatomic) IBOutlet UIButton *publishBtn;
@property (weak, nonatomic) IBOutlet UITextView *inputTextView;
@property (weak, nonatomic) IBOutlet UITextView *placeHolderTview;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;

@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
@end

@implementation PublishTextMomentViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.inputTextView.delegate = self;
    
    // textview 改变字体的行间距
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 7;// 字体的行间距
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:14],
                                 NSParagraphStyleAttributeName:paragraphStyle,
                                 };
    // 在有临时字符的情况下赋值属性，不可省略，否则无效
    self.inputTextView.attributedText = [[NSAttributedString alloc] initWithString:@"间隔" attributes:attributes];
    // 删除临时字符
    self.inputTextView.attributedText = [[NSAttributedString alloc] initWithString:@"" attributes:attributes];
}

- (IBAction)publishClick:(UIButton *)sender {
    [self.inputTextView resignFirstResponder];
    
    NSString *titleStr = self.titleTextField.text;
    NSString *content = self.inputTextView.text;
    if (titleStr.length == 0) {
        [self.titleTextField becomeFirstResponder];
        return;
    }else if (titleStr.length > 50){
        [MBProgressHUD showTipMessageInView:@"长度超限"];
        return;
    }
    if (content.length == 0) {
        [self.inputTextView becomeFirstResponder];
        return;
    }
    [commonManager showLoadingAnimateInWindow];
    NSDictionary *param = @{@"title":titleStr,
                            @"content":content,
                            @"type":@(1),
                            @"groupid":@(self.chooseGroupId)
    };
    [CommonManager POST:@"Moment/publish" parameters:param success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            [commonManager showSuccessAnimateInWindow];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)textViewDidChange:(UITextView *)textView{
    if (self.inputTextView.text.length == 0) {
        self.publishBtn.enabled = NO;
    }else{
        self.publishBtn.enabled = YES;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if (![text isEqualToString:@""]){
        self.placeHolderTview.hidden = YES;
    }

    if ([text isEqualToString:@""] && range.location == 0 && range.length == 1){
        self.placeHolderTview.hidden = NO;
    }

    return YES;
}

- (IBAction)chooseGroupBtnClick:(UIButton *)sender {
    kWeakSelf(self)
    SelectGroupController *vc = [[SelectGroupController alloc]init];
    vc.groupSelectblock = ^(GroupModel * _Nonnull model) {
        weakself.chooseGroupId = model.groupid;
        weakself.groupNameLabel.text = model.title;
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)backBtnClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc{
    NSLog(@"PublishMomentVC-dealloc");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
