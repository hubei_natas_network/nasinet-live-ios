//
//  SelectGroupController.h
//  NASI
//
//  Created by yun7_mac on 2019/9/24.
//  Copyright © 2019 cat. All rights reserved.
//

#import "RootViewController.h"

NS_ASSUME_NONNULL_BEGIN

@class GroupModel;

typedef void (^GroupSelectBlock)(GroupModel *model);

@interface SelectGroupController : RootViewController

@property (copy, nonatomic) GroupSelectBlock groupSelectblock;

@end

NS_ASSUME_NONNULL_END
