//
//  GroupInfoViewController.h
//  NasiBBS
//
//  Created by yun11 on 2020/9/23.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

#import "GroupModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface GroupInfoViewController : RootViewController

@property (strong, nonatomic) GroupModel *model;

@end

NS_ASSUME_NONNULL_END
