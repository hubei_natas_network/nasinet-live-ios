//
//  MomentCommentReplyViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/8/28.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

#import "MomentCommentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MomentCommentReplyViewController : RootViewController


@property (strong, nonatomic) MomentCommentModel *commentModel;

@end

NS_ASSUME_NONNULL_END
