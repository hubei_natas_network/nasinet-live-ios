//
//  MomentInfoViewController.m
//  Meet1V1
//
//  Created by yun on 2020/1/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MomentInfoViewController.h"
#import "UserInfoViewController.h"
#import "ReportCategoryViewController.h"
#import "MomentCommentReplyViewController.h"
#import "VipCenterViewController.h"
#import "GroupInfoViewController.h"

#import "MomentInfoTextCell.h"
#import "MomentInfoVideoCell.h"
#import "MomentInfoImagesCell.h"
#import "MomentInfoSingleImageCell.h"
#import "MomentCommentCell.h"

#import "MomentModel.h"
#import "MomentCommentModel.h"

#import "twEmojiView.h"
#import "SharePanelView.h"

#import <SJVideoPlayer.h>
#import <SJBaseVideoPlayer/UIScrollView+ListViewAutoplaySJAdd.h>
#import <YBImageBrowser.h>
#import <IQKeyboardManager.h>

#define pagesize 10
#define headerViewH 50
#define bottomViewH (kBottomSafeHeight > 0 ?50:60)

@interface MomentInfoViewController ()<UITableViewDelegate,UITableViewDataSource,MomentBaseTableViewCellDelegate,MomentCommentCellDelegate,SJPlayerAutoplayDelegate,UIGestureRecognizerDelegate,UITextFieldDelegate,twEmojiViewDelegate,SharePanelViewDelegate>{
    NSMutableArray      *datasource;
    
    UIView              *keyboardToolBarBackShadow;
    UITextField         *inputTextKeyField;
    UIButton            *toolbarEmojiBtn;
    UIView              *keyboardToolBar;
    SJVideoPlayer       *sjPlayer;
    twEmojiView         *emojiView;
}

@end

@implementation MomentInfoViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [sjPlayer vc_viewDidAppear];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [IQKeyboardManager sharedManager].enable = NO;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillDisappear:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [sjPlayer vc_viewWillDisappear];
    
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [sjPlayer vc_viewDidDisappear];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    datasource = [NSMutableArray array];
    
    [self createUI];
    
    [self.view addSubview:self.tableView];
    self.tableView.frame = CGRectMake(0, kStatusBarHeight + headerViewH, KScreenWidth, KScreenHeight - kStatusBarHeight - headerViewH - bottomViewH - kBottomSafeHeight);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 100;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self removeTableMJHeader];
    [self removeTableMJFooter];
    
    SJPlayerAutoplayConfig *config = [SJPlayerAutoplayConfig configWithAutoplayDelegate:self];
    config.autoplayPosition = SJAutoplayPositionMiddle;
    [self.tableView sj_enableAutoplayWithConfig:config];
    
    [MomentInfoTextCell registerWithTableView:self.tableView];
    [MomentInfoImagesCell registerWithTableView:self.tableView];
    [MomentInfoSingleImageCell registerWithTableView:self.tableView];
    [MomentInfoVideoCell registerWithTableView:self.tableView];
    
    [MomentCommentCell registerWithTableView:self.tableView];
    
    [self createKeyboardToolBar];

    [self headerRereshing];
    
    if (self.momentModel.type != MomentTypeVideo) {
        [self addWatchLog];
    }
}

- (void)createUI{
    UIView *navView = [[UIView alloc]init];
    navView.backgroundColor = [UIColor colorWithHexString:@"FCFCFC"];
    [self.view addSubview:navView];
    [navView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.offset(0);
        make.height.mas_equalTo(kStatusBarHeight + headerViewH);
    }];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setImage:IMAGE_NAMED(@"ic_back") forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(@0);
        make.top.mas_equalTo(kStatusBarHeight);
        make.height.width.mas_equalTo(headerViewH);
    }];
    
    UIImageView *iconImageView = [[UIImageView alloc]init];
    [iconImageView sd_setImageWithURL:[NSURL URLWithString:self.momentModel.user.avatar] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    iconImageView.userInteractionEnabled = YES;
    iconImageView.layer.cornerRadius = 20;
    iconImageView.layer.masksToBounds = YES;
    iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    [navView addSubview:iconImageView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userIconClick)];
    [iconImageView addGestureRecognizer:tap];
    [iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(backBtn.mas_right).offset(5);
        make.height.width.mas_equalTo(@40);
        make.centerY.equalTo(backBtn.mas_centerY);
    }];
    
    UILabel *nickNameLabel = [[UILabel alloc]init];
    nickNameLabel.text = self.momentModel.user.nick_name;
    nickNameLabel.textColor = [UIColor colorWithHexString:@"333333"];
    nickNameLabel.font = [UIFont boldSystemFontOfSize:15];
    [navView addSubview:nickNameLabel];
    [nickNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(iconImageView.mas_right).offset(10);
        make.top.equalTo(iconImageView.mas_top);
    }];
    
    NSString *genderImgStr = self.momentModel.user.profile.gender ? @"ic_boy_w_s":@"ic_girl_w_s";
    NSString *genderColorStr = self.momentModel.user.profile.gender ? @"6FCAFF":@"FF9CE4";
    UIButton *ageGenderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [ageGenderBtn setImage:IMAGE_NAMED(genderImgStr) forState:UIControlStateNormal];
    [ageGenderBtn setTitle:[NSString stringWithFormat:@"%d",self.momentModel.user.profile.age] forState: UIControlStateNormal];
    [ageGenderBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    ageGenderBtn.titleLabel.font = [UIFont systemFontOfSize:8];
    [ageGenderBtn setBackgroundColor:[UIColor colorWithHexString:genderColorStr]];
    ageGenderBtn.layer.cornerRadius = 1.5f;
    [ageGenderBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 2, 0, 0)];
    [ageGenderBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 0)];
    [navView addSubview:ageGenderBtn];
    [ageGenderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(nickNameLabel.mas_left);
        make.bottom.equalTo(iconImageView.mas_bottom).offset(-3);
        make.height.mas_equalTo(@13);
        make.width.mas_equalTo(@22);
    }];
    
    UIImageView *levelImgView = [[UIImageView alloc]init];
    levelImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_user_level_%d",self.momentModel.user.user_level]];
    levelImgView.contentMode = UIViewContentModeScaleToFill;
    levelImgView.layer.cornerRadius = 1.5f;
    levelImgView.layer.masksToBounds = YES;
    [navView addSubview:levelImgView];
    [levelImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(ageGenderBtn);
        make.left.equalTo(ageGenderBtn.mas_right).offset(5);
        make.height.mas_equalTo(@13);
        make.width.mas_equalTo(@22);
    }];
    
    UIView *bottomView = [[UIView alloc]init];
    [bottomView setBackgroundColor:[UIColor colorWithHexString:@"F5F5F5"]];
    [self.view addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(@0);
        make.height.mas_equalTo(bottomViewH + kBottomSafeHeight);
    }];
    
    UIButton *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareBtn setImage:IMAGE_NAMED(@"ic_share") forState:UIControlStateNormal];
    [shareBtn addTarget:self action:@selector(shareBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:shareBtn];
    [shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-14);
        make.height.width.mas_equalTo(30);
        make.top.offset(15);
    }];
    
    UIButton *collectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [collectBtn setImage:IMAGE_NAMED(@"ic_collect_b") forState:UIControlStateNormal];
    [collectBtn setImage:IMAGE_NAMED(@"ic_collect_b_sel") forState:UIControlStateSelected];
    [collectBtn addTarget:self action:@selector(collectMomentClick:) forControlEvents:UIControlEventTouchUpInside];
    collectBtn.selected = self.momentModel.collected;
    [bottomView addSubview:collectBtn];
    [collectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(shareBtn.mas_left).offset(-14);
        make.height.width.mas_equalTo(30);
        make.top.offset(15);
    }];
    
    UIButton *likeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [likeBtn setImage:IMAGE_NAMED(@"ic_zan_b") forState:UIControlStateNormal];
    [likeBtn setImage:IMAGE_NAMED(@"ic_zan_b_sel") forState:UIControlStateSelected];
    [likeBtn addTarget:self action:@selector(likeMomentClick:) forControlEvents:UIControlEventTouchUpInside];
    likeBtn.selected = self.momentModel.liked;
    [bottomView addSubview:likeBtn];
    [likeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(collectBtn.mas_left).offset(-14);
        make.height.width.mas_equalTo(30);
        make.top.offset(15);
    }];
    
    UIButton *sendCommnetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [sendCommnetBtn setBackgroundColor:[UIColor whiteColor]];
    [sendCommnetBtn setTitle:@"说点什么吧..." forState:UIControlStateNormal];
    [sendCommnetBtn setTitleColor:[UIColor colorWithHexString:@"CBCBCB"] forState:UIControlStateNormal];
    sendCommnetBtn.titleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightMedium];
    [sendCommnetBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
    [sendCommnetBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    sendCommnetBtn.layer.cornerRadius = 20;
    [sendCommnetBtn addTarget:self action:@selector(showInputView) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:sendCommnetBtn];
    [sendCommnetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(@15);
        make.right.equalTo(likeBtn.mas_left).offset(-15);
        make.top.mas_equalTo(@10);
        make.height.mas_equalTo(@40);
    }];
}

- (void)createKeyboardToolBar{
    
    keyboardToolBarBackShadow = [[UIView alloc]initWithFrame:self.view.bounds];
    keyboardToolBarBackShadow.backgroundColor = [UIColor clearColor];
    keyboardToolBarBackShadow.hidden = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        [self->inputTextKeyField resignFirstResponder];
        [UIView animateWithDuration:0.3 animations:^{
            self->keyboardToolBar.transform = CGAffineTransformIdentity;
            self->emojiView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            self->keyboardToolBarBackShadow.hidden = YES;
        }];
    }];
    [keyboardToolBarBackShadow addGestureRecognizer:tap];
    [self.view addSubview:keyboardToolBarBackShadow];
    
    //输入框
    inputTextKeyField = [[UITextField alloc]initWithFrame:CGRectMake(15,10,KScreenWidth- 15 - 50, 32)];
    inputTextKeyField.returnKeyType = UIReturnKeySend;
    inputTextKeyField.delegate = self;
    inputTextKeyField.textColor = [UIColor colorWithHexString:@"555555"];
    inputTextKeyField.borderStyle = UITextBorderStyleNone;
    inputTextKeyField.placeholder = @"说点什么吧...";
    inputTextKeyField.backgroundColor = [UIColor whiteColor];
    inputTextKeyField.layer.cornerRadius = 16;
    inputTextKeyField.layer.masksToBounds = YES;
    UIView *fieldLeft = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 15, 30)];
    fieldLeft.backgroundColor = [UIColor whiteColor];
    inputTextKeyField.leftView = fieldLeft;
    inputTextKeyField.leftViewMode = UITextFieldViewModeAlways;
    inputTextKeyField.font = [UIFont systemFontOfSize:15];
    
    //emoji按钮
    toolbarEmojiBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [toolbarEmojiBtn setImage:[UIImage imageNamed:@"ic_sv_emoji"] forState:UIControlStateNormal];
    toolbarEmojiBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    toolbarEmojiBtn.layer.masksToBounds = YES;
    toolbarEmojiBtn.layer.cornerRadius = 5;
    toolbarEmojiBtn.selected = NO;
    [toolbarEmojiBtn addTarget:self action:@selector(emojiBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    toolbarEmojiBtn.frame = CGRectMake(KScreenWidth-55,7,50,40);
    
    //tool绑定键盘
    keyboardToolBar = [[UIView alloc]initWithFrame:CGRectMake(0,KScreenHeight, KScreenWidth, 52)];
    keyboardToolBar.backgroundColor = CLineColor;
    UIView *tooBgv = [[UIView alloc]initWithFrame:keyboardToolBar.bounds];
    tooBgv.backgroundColor = CLineColor;
    tooBgv.alpha = 1;
    [keyboardToolBar addSubview:tooBgv];
    
    [keyboardToolBar addSubview:toolbarEmojiBtn];
    [keyboardToolBar addSubview:inputTextKeyField];
    
    [self.view addSubview:keyboardToolBar];
    
    emojiView = [[twEmojiView alloc]initWithFrame:CGRectMake(0, KScreenHeight, KScreenWidth, EmojiHeight)];
    emojiView.delegate = self;
    [self.view addSubview:emojiView];
}

- (void)headerRereshing{
    [self reqData];
}

- (void)footerRereshing{
    [self reqData];
}

- (void)addWatchLog{
    [CommonManager POST:@"Moment/addWatchLog" parameters:@{@"momentid":@(self.momentModel.momentid)} success:nil failure:nil];
}

- (void)reqData{
    long lastid = 9999999999;
    if (datasource.count > 0) {
        MomentCommentModel *lastModel = [datasource lastObject];
        lastid = lastModel.commentid;
    }
    
    NSDictionary *params = @{@"lastid":@(lastid),
                             @"size":@(pagesize),
                             @"momentid":@(self.momentModel.momentid)
    };
    NSString *api = @"Moment/getComments";
    [CommonManager POST:api parameters:params success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            
            NSArray *models = [NSArray yy_modelArrayWithClass:[MomentCommentModel class] json:responseObject[@"data"]];
            [self->datasource addObjectsFromArray:models];
            [self.tableView reloadData];
            
            if (models.count < pagesize) {
                [self removeTableMJFooter];
            }else{
                [self setupTableViewMJFooter];
            }
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        RESP_FAILURE;
    }];
}

- (void)showInputView{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    keyboardToolBarBackShadow.hidden = NO;
    inputTextKeyField.placeholder = @"说点什么吧...";
    [inputTextKeyField becomeFirstResponder];
}

- (void)emojiBtnClick:(UIButton *)sender{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    [inputTextKeyField resignFirstResponder];
    [UIView animateWithDuration:0.3 animations:^{
        self->keyboardToolBar.transform = CGAffineTransformMakeTranslation(0, -EmojiHeight-self->keyboardToolBar.height-kBottomSafeHeight);
        self->emojiView.transform = CGAffineTransformMakeTranslation(0, -EmojiHeight-kBottomSafeHeight);
    } completion:^(BOOL finished) {
        self->keyboardToolBarBackShadow.hidden = NO;
    }];
}

- (void)pushComment{
    NSString *content = inputTextKeyField.text;
    if (content.length == 0) {
        [inputTextKeyField becomeFirstResponder];
        return;
    }
    if (content.length > 50) {
        [MBProgressHUD showTipMessageInView:@"最多输入50个字符"];
    }
    NSDictionary *param = @{@"momentid":@(self.momentModel.momentid),
                            @"content":content
    };
    [inputTextKeyField resignFirstResponder];
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"Moment/publishComment" parameters:param success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            self->inputTextKeyField.text = @"";
            self.momentModel.comment_count += 1;
            MomentCommentModel *model = [MomentCommentModel yy_modelWithDictionary:responseObject[@"data"]];
            [self->datasource insertObject:model atIndex:0];
            [self.tableView insertRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] withRowAnimation:UITableViewRowAnimationFade];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)shareBtnClick{
    SharePanelView *panelView = [SharePanelView showPanelInView:self.view];
    panelView.delegate = self;
}

- (void)userIconClick{
    UserInfoViewController *vc = [[UserInfoViewController alloc]init];
    vc.anchorid = self.momentModel.user.userid;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)likeMomentClick:(UIButton *)sender{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    if (self.momentModel.liked) {
        return;
    }
    [CommonManager POST:@"Moment/likeMoment" parameters:@{@"momentid":@(self.momentModel.momentid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.momentModel.liked = YES;
            self.momentModel.like_count = [responseObject[@"data"][@"like_count"] intValue];
            sender.selected = self.momentModel.liked;
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (void)collectMomentClick:(UIButton *)sender{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    int type = 1;
    if (self.momentModel.collected) {
        type = 0; //取消收藏
    }
    [CommonManager POST:@"Moment/collectMoment" parameters:@{@"momentid":@(self.momentModel.momentid), @"type":@(type)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.momentModel.collected = !self.momentModel.collected;
            self.momentModel.collect_count = [responseObject[@"data"][@"collect_count"] intValue];
            sender.selected = self.momentModel.collected;
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

#pragma mark  ——————————————————————   SharePanelViewDelegate   ——————————————————————————
- (void)shareWithChannel:(PYShareChannel)channel panel:(nonnull SharePanelView *)panelView{
    if (channel == PYShareChannelReport) {
        ReportCategoryViewController *vc = [[ReportCategoryViewController alloc]init];
        vc.relateid = self.momentModel.momentid;
        vc.type = ReportTypeShortVideo;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (channel == PYShareChannelCollect){
        if (![CommonManager checkAndLogin]) {
            return;
        }
        [commonManager showLoadingAnimateInWindow];
        [CommonManager POST:@"moment/collectMoment" parameters:@{@"momentid":@(self.momentModel.momentid), @"type":@(!self.momentModel.collected)} success:^(id responseObject) {
            [commonManager hideAnimateHud];
            if (RESP_SUCCESS(responseObject)) {
                [commonManager showSuccessAnimateInWindow];
                self.momentModel.collected = !self.momentModel.collected;
                [panelView setCollectBtnSelected:self.momentModel.collected];
            }else{
                RESP_SHOW_ERROR_MSG(responseObject);
            }
        } failure:^(NSError *error) {
            [commonManager hideAnimateHud];
            RESP_FAILURE;
        }];
    }else if (channel == PYShareChannelLink){
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = [NSString stringWithFormat:@"%@%d",[configManager appConfig].share_moment_url,self.momentModel.momentid];
        [MBProgressHUD showTipMessageInView:@"链接已复制到剪贴板"];
    }else{
        SSDKPlatformType type = [SharePanelView getSSDKPlatformTypeBy:channel];
        //统一创建分享参数
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:self.momentModel.title
                                         images:self.momentModel.user.avatar
                                            url:[NSURL URLWithString:[NSString stringWithFormat:@"%@%d",[configManager appConfig].share_moment_url,self.momentModel.momentid]]
                                          title:@"推荐你看这个动态"
                                           type:SSDKContentTypeAuto];
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            switch (state) {
                case SSDKResponseStateSuccess:
                    [MBProgressHUD showTipMessageInView:@"分享成功"];
                    break;
                case SSDKResponseStateFail:
                {
                    [MBProgressHUD showTipMessageInView:@"分享失败"];
                    //失败
                    break;
                }
                case SSDKResponseStateCancel:
                    //取消
                    break;
                    
                default:
                    break;
            }
        }];
    }
}

#pragma mark    —————————— tableviewdelegate + datasource ——————————
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }
    return datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (self.momentModel.type == MomentTypeImages) {
            if (self.momentModel.image_urls.count == 1) {
                MomentInfoSingleImageCell *cell = [MomentInfoSingleImageCell cellWithTableView:tableView indexPath:indexPath];
                cell.delegate = self;
                cell.model = self.momentModel;
                return cell;
            }else{
                MomentInfoImagesCell *cell = [MomentInfoImagesCell cellWithTableView:tableView indexPath:indexPath];
                cell.delegate = self;
                cell.model = self.momentModel;
                return cell;
            }
        }else if ( self.momentModel.type == MomentTypeText){
            MomentInfoTextCell *cell = [MomentInfoTextCell cellWithTableView:tableView indexPath:indexPath];
            
            cell.delegate = self;
            cell.model = self.momentModel;
            return cell;
        }else if ( self.momentModel.type == MomentTypeVideo){
            MomentInfoVideoCell *cell = [MomentInfoVideoCell cellWithTableView:tableView indexPath:indexPath];
            
            cell.delegate = self;
            cell.model = self.momentModel;
            return cell;
        }
    }else{
        MomentCommentModel *model = datasource[indexPath.row];
        MomentCommentCell *cell = [MomentCommentCell cellWithTableView:tableView indexPath:indexPath];
        cell.delegate = self;
        cell.model = model;
        return cell;
    }
    return [UITableViewCell new];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 1){
        UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 35)];
        headerView.backgroundColor = [UIColor whiteColor];
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, KScreenWidth - 30, 35)];
        titleLabel.font = [UIFont boldSystemFontOfSize:12];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.text = [NSString stringWithFormat:@"最新评论（%d）", self.momentModel.comment_count];
        [headerView addSubview:titleLabel];
        return headerView;
    }
    return [[UIView alloc]init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        MomentCommentReplyViewController *vc = [[MomentCommentReplyViewController alloc]init];
        vc.commentModel = datasource[indexPath.row];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return 35;
    }
    return 0;
}

#pragma mark —————————— SJPlayerAutoplayDelegate ———————————————
- (void)sj_playerNeedPlayNewAssetAtIndexPath:(NSIndexPath *)indexPath {
    if ( indexPath != nil ) {
        if ( !sjPlayer ) {
            sjPlayer = [SJVideoPlayer player];
            [self _setupFloatSmallViewControllerOfPlayer];
        }
        if (self.momentModel.unlock_price > 0 && !self.momentModel.unlocked){
            return;
        }
        sjPlayer.URLAsset = [[SJVideoPlayerURLAsset alloc] initWithURL:[NSURL URLWithString:self.momentModel.video_url] playModel:[SJPlayModel playModelWithTableView:self.tableView indexPath:indexPath]];
        sjPlayer.URLAsset.title = @"";
        sjPlayer.muted = YES;
        
        //检测播放权限
        [CommonManager POST:@"moment/checkCanPlay" parameters:@{@"momentid":@(self.momentModel.momentid)} success:^(id responseObject) {
            if (RESP_SUCCESS(responseObject)) {
                self->sjPlayer.muted = NO;
            }else{
                [self->sjPlayer stop];
                [self.tableView sj_removeCurrentPlayerView];
                [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"观影次数不足" contentText:@"开通Vip尽享无限观影" leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"开通" rightClick:^{
                    [self.navigationController pushViewController:[[VipCenterViewController alloc]init] animated:YES];
                }];
            }
        } failure:^(NSError *error) {
            RESP_FAILURE;
            [self->sjPlayer stop];
            [self.tableView sj_removeCurrentPlayerView];
        }];
    }
}

- (void)_setupFloatSmallViewControllerOfPlayer {
    __weak typeof(self) _self = self;
    // 开启小浮窗(当播放器视图滑动消失时, 显示小浮窗视图)
    sjPlayer.floatSmallViewController.enabled = YES;
    sjPlayer.pauseWhenScrollDisappeared = NO;
    
    // 单击小浮窗时的回调
    sjPlayer.floatSmallViewController.singleTappedOnTheFloatViewExeBlock = ^(id<SJFloatSmallViewController>  _Nonnull controller) {
        __strong typeof(_self) self = _self;
        if ( !self ) return ;
        [self.tableView scrollToTop];
    };
    
    // 双击小浮窗时的回调
    sjPlayer.floatSmallViewController.doubleTappedOnTheFloatViewExeBlock = ^(id<SJFloatSmallViewController>  _Nonnull controller) {
        __strong typeof(_self) self = _self;
        if ( !self ) return ;
        
        self->sjPlayer.timeControlStatus == SJPlaybackTimeControlStatusPaused ? [self->sjPlayer play] : [self->sjPlayer pause];
    };
    
}

#pragma mark —————————— MomentBaseTableViewCellDelegate ———————————————
- (void)playBtnClick:(MomentBaseTableViewCell *)cell{
    if (!sjPlayer ) {
        sjPlayer = [SJVideoPlayer player];
        sjPlayer.playbackObserver.playbackDidFinishExeBlock = ^(__kindof SJBaseVideoPlayer * _Nonnull player) {
            [player replay];
        };
    }
    sjPlayer.URLAsset = [[SJVideoPlayerURLAsset alloc] initWithURL:[NSURL URLWithString:cell.model.video_url] playModel:[SJPlayModel playModelWithTableView:self.tableView indexPath:[self.tableView indexPathForCell:cell]]];
    sjPlayer.URLAsset.title = @"";
    sjPlayer.muted = YES;
    
    //检测播放权限
    [CommonManager POST:@"moment/checkCanPlay" parameters:@{@"momentid":@(cell.model.momentid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self->sjPlayer.muted = NO;
        }else{
            [self->sjPlayer stop];
            [self.tableView sj_removeCurrentPlayerView];
            [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"观影次数不足" contentText:@"开通Vip尽享无限观影" leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"开通" rightClick:^{
                [self.navigationController pushViewController:[[VipCenterViewController alloc]init] animated:YES];
            }];
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
        [self->sjPlayer stop];
        [self.tableView sj_removeCurrentPlayerView];
    }];
}

- (void)unlockClick:(MomentBaseTableViewCell *)cell{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"解锁动态" contentText:[NSString stringWithFormat:@"解锁此动态需要支付%d金币", cell.model.unlock_price] leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"解锁" rightClick:^{
        [commonManager showLoadingAnimateInWindow];
        [CommonManager POST:@"Moment/unlockMoment" parameters:@{@"momentid":@(cell.model.momentid)} success:^(id responseObject) {
            [commonManager hideAnimateHud];
            if (RESP_SUCCESS(responseObject)) {
                cell.model.unlocked = YES;
                if (cell.model.type == MomentTypeVideo) {
                    //解锁后播放视频
                    if (!self->sjPlayer ) {
                        self->sjPlayer = [SJVideoPlayer player];
                        self->sjPlayer.playbackObserver.playbackDidFinishExeBlock = ^(__kindof SJBaseVideoPlayer * _Nonnull player) {
                            [player replay];
                        };
                    }
                    self->sjPlayer.URLAsset = [[SJVideoPlayerURLAsset alloc] initWithURL:[NSURL URLWithString:cell.model.video_url] playModel:[SJPlayModel playModelWithTableView:self.tableView indexPath:[self.tableView indexPathForCell:cell]]];
                    self->sjPlayer.URLAsset.title = @"";
                }else{
                    [self.tableView reloadSection:0 withRowAnimation:UITableViewRowAnimationNone];
                }
            }else{
                RESP_SHOW_ERROR_MSG(responseObject);
            }
        } failure:^(NSError *error) {
            [commonManager hideAnimateHud];
            RESP_FAILURE;
        }];
    }];
}

- (void)imgViewClick:imageView model:(MomentModel *)model index:(NSInteger)index{
    NSMutableArray *datas = [NSMutableArray array];
    [model.image_urls enumerateObjectsUsingBlock:^(NSString *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        // 网络图片
        YBIBImageData *data = [YBIBImageData new];
        data.imageURL = [NSURL URLWithString:obj];
        data.projectiveView = imageView;
        [datas addObject:data];
    }];
    
    YBImageBrowser *browser = [YBImageBrowser new];
    browser.dataSourceArray = datas;
    browser.currentPage = index;
    // 只有一个保存操作的时候，可以直接右上角显示保存按钮
    browser.defaultToolViewHandler.topView.operationType = YBIBTopViewOperationTypeSave;
    [browser show];
}

- (void)userIconClick:(MomentModel *)model{
    UserInfoViewController *vc = [[UserInfoViewController alloc]init];
    vc.anchorid = model.user.userid;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)groupTagClick:(GroupModel *)model{
    GroupInfoViewController *vc = [[GroupInfoViewController alloc]init];
    vc.model = model;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark ——————————————— MomentCommentCellDelegate ———————————————————

- (void)commentLikeClick:(MomentCommentModel *)model{
    if (model.liked) {
        return;
    }
    [CommonManager POST:@"Moment/likeComment" parameters:@{@"commentid":@(model.commentid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            model.liked = YES;
            model.like_count = [responseObject[@"data"][@"like_count"] intValue];
            [self.tableView reloadSection:1 withRowAnimation:UITableViewRowAnimationNone];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == inputTextKeyField) {
        [inputTextKeyField resignFirstResponder];
        [self pushComment];
    }
    return YES;
}

#pragma mark - Emoji 代理
- (void)sendimage:(NSString *)str {
    if ([str isEqual:@"msg_del"]) {
        [inputTextKeyField deleteBackward];
    }else {
        [inputTextKeyField insertText:str];
    }
}
- (void)clickSendEmojiBtn {
    NSString *content = inputTextKeyField.text;
    if (content.length > 50) {
        [MBProgressHUD showTipMessageInView:@"超出最大字数限制"];
        return;
    }
    [UIView animateWithDuration:0.3 animations:^{
        self->keyboardToolBar.transform = CGAffineTransformIdentity;
        self->emojiView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        self->keyboardToolBarBackShadow.hidden = YES;
    }];
    [self pushComment];
}

- (void)keyboardWillAppear:(NSNotification *)notification{
    
    NSDictionary *info = [notification userInfo];
    
    //取出动画时长
    CGFloat animationDuration = [[info valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //取出键盘位置大小信息
    CGRect keyboardBounds = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    //rect转换
//    CGRect keyboardRect = [self convertRect:keyboardBounds toView:nil];
    
    //记录Y轴变化
    CGFloat keyboardHeight = keyboardBounds.size.height;
    
    //上移动画options
    UIViewAnimationOptions options = (UIViewAnimationOptions)[[info valueForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue] << 16;
    
    keyboardToolBarBackShadow.hidden = NO;
    [UIView animateWithDuration:animationDuration delay:0 options:options animations:^{
        self->keyboardToolBar.transform = CGAffineTransformMakeTranslation(0, -keyboardHeight-self->keyboardToolBar.height);
    } completion:nil];
}


- (void)keyboardWillDisappear:(NSNotification *)notification{
    NSDictionary *info = [notification userInfo];
    
    //取出动画时长
    CGFloat animationDuration = [[info valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //下移动画options
    UIViewAnimationOptions options = (UIViewAnimationOptions)[[info valueForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue] << 16;
    
    keyboardToolBarBackShadow.hidden = YES;
    //回复动画
    [UIView animateWithDuration:animationDuration delay:0 options:options animations:^{
        self->keyboardToolBar.transform = CGAffineTransformIdentity;
        self->emojiView.transform = CGAffineTransformIdentity;
    } completion:nil];
    
}

- (void)didMoveToParentViewController:(UIViewController *)parent{
    if (!parent) {
        [[NSNotificationCenter defaultCenter]removeObserver:self];
        if (sjPlayer) {
            [sjPlayer stop];
            sjPlayer = nil;
        }
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    if (sjPlayer) {
        [sjPlayer stop];
        sjPlayer = nil;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
