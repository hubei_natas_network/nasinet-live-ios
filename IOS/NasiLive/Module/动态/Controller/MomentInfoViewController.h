//
//  MomentInfoViewController.h
//  Meet1V1
//
//  Created by yun on 2020/1/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

@class MomentModel;

NS_ASSUME_NONNULL_BEGIN

@interface MomentInfoViewController : RootViewController

@property (strong, nonatomic) MomentModel *momentModel;

@end

NS_ASSUME_NONNULL_END
