//
//  MomentRankViewController.h
//  NasiBBS
//
//  Created by yun11 on 2020/9/24.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"
#import "MomentListViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MomentRankViewController : RootViewController

@property (assign, nonatomic) MomentListType listType;

@end

NS_ASSUME_NONNULL_END
