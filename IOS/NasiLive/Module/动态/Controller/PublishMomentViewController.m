//
//  PublishMomentViewController.m
//  Meet1V1
//
//  Created by yun on 2020/1/14.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "PublishMomentViewController.h"
#import "SelectGroupController.h"
//#import "ChooseTopicViewController.h"

//#import "TopicModel.h"

#import <YBImageBrowser.h>
#import <QCloudCore.h>
#import <QCloudCOSXML.h>
#import "TXUGCPublish.h"
#import <TZImagePickerController.h>

@interface PublishMomentViewController ()<UITextViewDelegate,UIGestureRecognizerDelegate,TZImagePickerControllerDelegate,TXVideoPublishListener,YBImageBrowserDelegate>{
    NSMutableArray          *image_urls;
    NSMutableArray          *blur_image_urls;
    NSString                *video_url;
    NSInteger               unlock_price;
    MomentDisplayStyle      display_style;
    
    NSString                *chooseVideoPath;
    NSString                *videoCoverPath;
    NSMutableArray          *chooseImages;
    
    NSInteger               browserIndex;
    YBImageBrowser          *imgBrowser;
}
@property (assign, nonatomic) int chooseGroupId;
@property (weak, nonatomic) IBOutlet UILabel *groupNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UIButton *publishBtn;
@property (weak, nonatomic) IBOutlet UITextView *inputTextView;
@property (weak, nonatomic) IBOutlet UITextView *placeHolderTview;

@property (weak, nonatomic) IBOutlet UIView *videoHolderView;
@property (weak, nonatomic) IBOutlet UIImageView *videoImgView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *videoViewHLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *videoViewWLC;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputTViewHLC;

@property (weak, nonatomic) IBOutlet UIView *topicView;
@property (weak, nonatomic) IBOutlet UIButton *topicBtn;
@property (weak, nonatomic) IBOutlet UIView *chosenTopicView;
@property (weak, nonatomic) IBOutlet UILabel *chosenTopicLabel;

@property (weak, nonatomic) IBOutlet UIView *setUnlockPriceView;
@property (weak, nonatomic) IBOutlet UIButton *priceBtn;


@property (weak, nonatomic) IBOutlet UIStackView *skView;
@property (weak, nonatomic) IBOutlet UIStackView *skView1;
@property (weak, nonatomic) IBOutlet UIStackView *skView2;
@property (weak, nonatomic) IBOutlet UIStackView *skView3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topicViewTopToSkLc;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topicViewTopToVideoLc;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *unlockTopToVideoLc;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *videoTopToTextLc;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stackTopToTextLc;

@property (weak, nonatomic) IBOutlet UIView *typeChooseView;

@property (nonatomic,assign) MomentType momentType;

@end

@implementation PublishMomentViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    image_urls = [NSMutableArray arrayWithCapacity:9];
    blur_image_urls = [NSMutableArray arrayWithCapacity:9];
    chooseImages = [NSMutableArray arrayWithCapacity:9];
    video_url = @"";
    
    self.momentType = MomentTypeText;
    
    self.inputTextView.delegate = self;
    
    [self.inputTextView layoutIfNeeded];
    
    // textview 改变字体的行间距
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 7;// 字体的行间距
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:16],
                                 NSParagraphStyleAttributeName:paragraphStyle,
                                 };
    // 在有临时字符的情况下赋值属性，不可省略，否则无效
    self.inputTextView.attributedText = [[NSAttributedString alloc] initWithString:@"间隔" attributes:attributes];
    // 删除临时字符
    self.inputTextView.attributedText = [[NSAttributedString alloc] initWithString:@"" attributes:attributes];
}

- (IBAction)publishClick:(UIButton *)sender {
    [self.inputTextView resignFirstResponder];
    
    switch (self.momentType) {
        case MomentTypeText:{
            NSString *titleStr = self.titleTextField.text;
            NSString *content = self.inputTextView.text;
            if (titleStr.length == 0 && self.momentType == MomentTypeText) {
                [self.titleTextField becomeFirstResponder];
                return;
            }else if (titleStr.length > 50){
                [MBProgressHUD showTipMessageInView:@"长度超限"];
                return;
            }
            if (content.length == 0 && self.momentType == MomentTypeText) {
                [self.inputTextView becomeFirstResponder];
                return;
            }else if (content.length > 120){
                [MBProgressHUD showTipMessageInView:@"长度超限"];
                return;
            }
            [commonManager showLoadingAnimateInWindow];
            [self submitToServer];
        }
            break;
        case MomentTypeVideo:{
            if (chooseVideoPath.length == 0 && videoCoverPath.length == 0) {
                [MBProgressHUD showTipMessageInView:@"请先选择或拍摄一段视频"];
                return;
            }
            [commonManager showLoadingAnimateInWindow];
            [self uploadToVod];
        }
            break;
        case MomentTypeImages:{
            if (chooseImages.count == 0) {
                [MBProgressHUD showTipMessageInView:@"请先选择至少1张图片"];
                return;
            }
            [commonManager showLoadingAnimateInWindow];
            [self uploadImages];
        }
            break;
        default:
            break;
    }
}

- (void)submitToServer{
    if (self.momentType == MomentTypeImages && image_urls.count == 0) {
        [MBProgressHUD showTipMessageInView:@"请先选择至少1张图片"];
        return;
    }
    if (self.momentType == MomentTypeVideo && video_url.length == 0) {
        [MBProgressHUD showTipMessageInView:@"请先选择或拍摄一段视频"];
        return;
    }
    NSDictionary *param = @{@"title":self.titleTextField.text,
                            @"content":self.inputTextView.text,
                            @"unlock_price":@(unlock_price),
                            @"type":@(self.momentType),
                            @"image_url":[image_urls componentsJoinedByString:@","],
                            @"blur_image_url":[blur_image_urls componentsJoinedByString:@","],
                            @"video_url":video_url,
                            @"single_display_type":@(display_style),
                            @"groupid":@(self.chooseGroupId)
                           
    };
    [CommonManager POST:@"Moment/publish" parameters:param success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            [commonManager showSuccessAnimateInWindow];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)uploadToVod{
    [CommonManager POST:@"Config/getSignForVod" parameters:@{} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            NSString *signature = responseObject[@"data"][@"signature"];
            TXUGCPublish *_videoPublish = [[TXUGCPublish alloc] initWithUserID:[NSString stringWithFormat:@"%lld",userManager.curUserInfo.userid]];
            _videoPublish.delegate = self;
            TXPublishParam *publishParam = [[TXPublishParam alloc] init];
            
            publishParam.signature  = signature;
            publishParam.videoPath  = self->chooseVideoPath;
            publishParam.coverPath = self->videoCoverPath;
            [_videoPublish publishVideo:publishParam];
        }else{
            [commonManager hideAnimateHud];
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)onPublishComplete:(TXPublishResult*)result {
    if (result.retCode == 0) {
        //提交到服务器
        image_urls = [NSMutableArray arrayWithArray:@[result.coverURL]];
        video_url = result.videoURL;
        [self submitToServer];
    }else{
        [commonManager hideAnimateHud];
        [commonManager showErrorAnimateInWindow];
    }
}

//- (void)uploadVideoBlurCover{
//    [CommonManager POST:@"Config/getTempKeysForCos" parameters:@{} success:^(id responseObject) {
//            if (RESP_SUCCESS(responseObject)) {
//                configManager.txCosModel = [TxCosModel yy_modelWithDictionary:responseObject[@"data"]];
//                [configManager saveTxCosModel];
//                [self uploadBlurImages];
//            }else{
//                [commonManager hideAnimateHud];
//                RESP_SHOW_ERROR_MSG(responseObject);
//            }
//        } failure:^(NSError *error) {
//            [commonManager hideAnimateHud];
//            RESP_FAILURE;
//        }];
//}

- (void)uploadImages{
    [image_urls removeAllObjects];
    [blur_image_urls removeAllObjects];
    if (chooseImages.count == 1) {
        UIImage *image = chooseImages[0];
        CGFloat fixelW = CGImageGetWidth(image.CGImage);
        CGFloat fixelH = CGImageGetHeight(image.CGImage);
        if (fixelW > fixelH) {
            //横屏
            self->display_style = MomentDisplayStyleHorizontal;
        }else if (fixelW < fixelH){
            //竖屏
            self->display_style = MomentDisplayStyleVertical;
        }else if (fixelW == fixelH){
            //方形
            self->display_style = MomentDisplayStyleSquare;
        }
    }
    [CommonManager POST:@"Config/getTempKeysForCos" parameters:@{} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            configManager.txCosModel = [TxCosModel yy_modelWithDictionary:responseObject[@"data"]];
            [configManager saveTxCosModel];
            for (int i = 0; i < self->chooseImages.count; i++) {
                NSString *fileName = [CommonManager getNameBaseCurrentTime:@".jpg"];
                NSString *flieUploadPath = [NSString stringWithFormat:@"%@/%@",configManager.appConfig.cos_folder_image,fileName];
                NSString *blurflieUploadPath = [NSString stringWithFormat:@"%@/%@",configManager.appConfig.cos_folder_blurimage,fileName];
                QCloudCOSXMLUploadObjectRequest* put = [QCloudCOSXMLUploadObjectRequest new];
                put.object = flieUploadPath;
                put.bucket = configManager.appConfig.cos_bucket;
                put.body = UIImageJPEGRepresentation(self->chooseImages[i], 1);
                if (self->unlock_price > 0) {
                    NSMutableDictionary *headers = [NSMutableDictionary dictionary];
                    headers[@"Pic-Operations"] = [NSString stringWithFormat:@"{\"is_pic_info\":1,\"rules\":[{\"fileid\":\"%@\",\"rule\":\"imageMogr2/blur/50x25\"}]}",[NSString stringWithFormat:@"/%@",blurflieUploadPath]];
                    put.customHeaders = headers;
                }
                [put setFinishBlock:^(QCloudUploadObjectResult *result, NSError* error) {
                    //可以从 result 获取结果
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (result && !error) {
                            NSString *blurUrl = [result.location stringByReplacingOccurrencesOfString:flieUploadPath withString:blurflieUploadPath];
                            if (self->image_urls.count > i) {
                                [self->image_urls insertObject:result.location atIndex:i];
                                if (self->unlock_price > 0) {
                                    [self->blur_image_urls insertObject:blurUrl atIndex:i];
                                }
                            }else{
                                [self->image_urls addObject:result.location];
                                if (self->unlock_price > 0) {
                                    [self->blur_image_urls addObject:blurUrl];
                                }
                            }
                            if (self->image_urls.count == self->chooseImages.count) {
                                [self submitToServer];
                            }
                        }else{
                            [commonManager hideAnimateHud];
                            [commonManager showErrorAnimateInWindow];
                        }
                    });
                }];

                [[QCloudCOSTransferMangerService defaultCOSTransferManager] UploadObject:put];
            }
        }else{
            [commonManager hideAnimateHud];
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}




- (IBAction)chooseVideoClick{
    
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:9 delegate:self];
    imagePickerVc.allowPickingGif = NO;
    imagePickerVc.allowPickingImage = NO;
    imagePickerVc.maxImagesCount = 1;
    imagePickerVc.showPhotoCannotSelectLayer = YES;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.naviTitleColor = [UIColor colorWithHexString:@"333333"];
    imagePickerVc.barItemTextColor = [UIColor colorWithHexString:@"333333"];
    imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
    [imagePickerVc setDidFinishPickingVideoHandle:^(UIImage *coverImage, PHAsset *asset) {
        self.momentType = MomentTypeVideo;
        //重置数据
        [self->image_urls removeAllObjects];
        self->video_url = @"";
        self->videoCoverPath = @"";
        
        self.videoImgView.image = coverImage;
        
        [self->chooseImages removeAllObjects];
        [self->chooseImages addObject:coverImage];
        //存储封面图片
        NSString *coverImageName = [CommonManager getNameBaseCurrentTime:@".jpg"];
        NSString *coverImagePath = [CommonManager getFilePathWithFileName:coverImageName];
        if([UIImagePNGRepresentation(coverImage) writeToFile:coverImagePath atomically:YES]){
            self->videoCoverPath = coverImagePath;
        }
        
        CGFloat fixelW = CGImageGetWidth(coverImage.CGImage);
        CGFloat fixelH = CGImageGetHeight(coverImage.CGImage);
        CGFloat videoViewW = 0;
        CGFloat videoViewH = 0;
        if (fixelW > fixelH) {
            //横屏视频
            self->display_style = MomentDisplayStyleHorizontal;
            videoViewW = kScreenWidth - 15*2;
            videoViewH = videoViewW * 9 / 16;
        }else{
            //竖屏视频
            self->display_style = MomentDisplayStyleVertical;
            videoViewW = (kScreenWidth - 15*2)*0.66;
            videoViewH = videoViewW * 5 / 4;
        }
        self.videoViewHLC.constant = videoViewH;
        self.videoViewWLC.constant = videoViewW;
        
        [MBProgressHUD showTipMessageInWindow:@"视频处理中"];
        
        NSArray * assetResources = [PHAssetResource assetResourcesForAsset: asset];
        PHAssetResource * resource;
        for (PHAssetResource * assetRes in assetResources) {
            if (assetRes.type == PHAssetResourceTypePairedVideo || assetRes.type == PHAssetResourceTypeVideo) {
                resource = assetRes;
            }
        }
        NSString *videoFileName = @"tempAssetVideo.mov";
        if (resource.originalFilename) {
            videoFileName = resource.originalFilename;
        }
        if (asset.mediaType == PHAssetMediaTypeVideo) {
            if ([videoFileName hasSuffix:@".mp4"] || [videoFileName hasSuffix:@".MP4"] || [videoFileName hasSuffix:@".Mp4"]) {
                PHVideoRequestOptions * options = [[PHVideoRequestOptions alloc] init];
                options.version = PHImageRequestOptionsVersionCurrent;
                options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
                NSString * PATH_MOVIE_FILE = [NSTemporaryDirectory() stringByAppendingPathComponent: videoFileName];
                [[NSFileManager defaultManager] removeItemAtPath: PATH_MOVIE_FILE error: nil];
                [[PHAssetResourceManager defaultManager] writeDataForAssetResource: resource toFile: [NSURL fileURLWithPath: PATH_MOVIE_FILE] options: nil completionHandler: ^(NSError * _Nullable error) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUD];
                    });
                    if (!error) {
                        self->chooseVideoPath = PATH_MOVIE_FILE;
                    }
                }];
            }else{
                //转mp4
                [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:[PHVideoRequestOptions new] resultHandler:^(AVAsset * _Nullable avAsset, AVAudioMix * _Nullable audioMix, NSDictionary * _Nullable info) {
                    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]initWithAsset:avAsset presetName:AVAssetExportPreset1280x720];
                    NSString *videoFileName = [CommonManager getNameBaseCurrentTime:@".mp4"];
                    NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent: videoFileName];
                    exportSession.outputURL = [NSURL fileURLWithPath:filePath];
                    exportSession.outputFileType = AVFileTypeMPEG4;
                    [exportSession exportAsynchronouslyWithCompletionHandler:^{
                        int exportStatus = (int)exportSession.status;
                        switch (exportStatus) {
                            case AVAssetExportSessionStatusFailed:
                                NSLog(@"Export failed: %@", [[exportSession error] localizedDescription]);
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [MBProgressHUD hideHUD];
                                    [MBProgressHUD showTipMessageInWindow:@"转码失败,请更换视频"];
                                });
                                break;
                            case AVAssetExportSessionStatusCancelled:
                                NSLog(@"Export canceled");
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [MBProgressHUD hideHUD];
                                    [MBProgressHUD showTipMessageInWindow:@"转码失败,请更换视频"];
                                });
                                break;
                            case AVAssetExportSessionStatusCompleted:
                                NSLog(@"转换成功");
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [MBProgressHUD hideHUD];
                                    self->chooseVideoPath = filePath;
                                });
                                break;
                        }
                    }];
                }];
            }
        }
    }];
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (IBAction)chooseImgClick:(id)sender{
    self.skView.hidden = NO;
    [self resetImageStackViewFirstLoad:YES];
    [self chooseImgTap:nil];
}

- (void)chooseImgTap:(UIGestureRecognizer *)ges{
    UIImageView *imgView;
    NSInteger tag ;
    if (!ges) {
        imgView = [self.view viewWithTag:1000];
        tag = 0;
    }else{
        imgView = (UIImageView *)ges.view;
        tag = imgView.tag - 1000;
    }
    if (chooseImages.count > tag) {
        //查看图片
        NSMutableArray *datas = [NSMutableArray array];
        [chooseImages enumerateObjectsUsingBlock:^(UIImage *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            // 网络图片
            YBIBImageData *data = [YBIBImageData new];
            data.image = ^UIImage * _Nullable{
                return obj;
            };
            data.projectiveView = imgView;
            [datas addObject:data];
        }];
        
        if (!imgBrowser) {
            imgBrowser = [YBImageBrowser new];
            [imgBrowser.defaultToolViewHandler.topView.operationButton setImage:IMAGE_NAMED(@"ic_delete") forState:UIControlStateNormal];
            [imgBrowser.defaultToolViewHandler.topView setClickOperation:^(YBIBTopViewOperationType type) {
                [self imgOperationClick];
            }];
        }
        imgBrowser.dataSourceArray = datas;
        imgBrowser.delegate = self;
        imgBrowser.currentPage = tag;
        [imgBrowser show];
    }else{
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:9 delegate:self];
        imagePickerVc.allowTakeVideo = NO;
        imagePickerVc.allowPickingGif = NO;
        imagePickerVc.allowPickingVideo = NO;
        imagePickerVc.maxImagesCount = 9 - tag;
        imagePickerVc.showSelectedIndex = YES;
        imagePickerVc.showPhotoCannotSelectLayer = YES;
        imagePickerVc.allowPickingOriginalPhoto = NO;
        imagePickerVc.naviTitleColor = [UIColor colorWithHexString:@"333333"];
        imagePickerVc.barItemTextColor = [UIColor colorWithHexString:@"333333"];
        imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
        [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
            self.momentType = MomentTypeImages;
            
            for (int i=0; i<photos.count; i++) {
                UIImage *selImg = photos[i];
                [self->chooseImages addObject:selImg];
            }
            [self resetImageStackViewFirstLoad:NO];
        }];
        imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:imagePickerVc animated:YES completion:nil];
    }
}

- (void)resetImageStackViewFirstLoad:(BOOL)firstLoad{
    NSMutableArray *imgViewArray = [NSMutableArray arrayWithCapacity:9];
    NSArray *subViews1 = [self.skView1 subviews];
    for (UIView *view in subViews1) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imgView = (UIImageView *)view;
            imgView.image = nil;
            [imgView removeAllSubviews];
            [imgViewArray addObject:imgView];
        }
    }
    NSArray *subViews2 = [self.skView2 subviews];
    for (UIView *view in subViews2) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imgView = (UIImageView *)view;
            imgView.image = nil;
            [imgView removeAllSubviews];
            [imgViewArray addObject:imgView];
        }
    }
    NSArray *subViews3 = [self.skView3 subviews];
    for (UIView *view in subViews3) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imgView = (UIImageView *)view;
            imgView.image = nil;
            [imgView removeAllSubviews];
            [imgViewArray addObject:imgView];
        }
    }
    for (int i = 0; i < imgViewArray.count; i++) {
        UIImageView *imgView = imgViewArray[i];
        if (firstLoad) {
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(chooseImgTap:)];
            [imgView addGestureRecognizer:tap];
        }
        if (i < chooseImages.count) {
            [imgView setImage:chooseImages[i]];
        }else if (i == chooseImages.count && i > 0){
            [imgView setImage:IMAGE_NAMED(@"ic_add")];
        }else{
            [imgView setImage:nil];
        }
    }
    self.skView2.hidden = chooseImages.count < 3;
    self.skView3.hidden = chooseImages.count < 6;
}

//删除图片
- (void)imgOperationClick{
    if (chooseImages.count > browserIndex) {
        [chooseImages removeObjectAtIndex:browserIndex];
    }
    [imgBrowser hide];
    [self resetImageStackViewFirstLoad:NO];
    if (chooseImages.count == 0) {
        self.momentType = MomentTypeText;
    }
}

- (IBAction)topicClick:(id)sender {
//    ChooseTopicViewController *vc = [[ChooseTopicViewController alloc]init];
//    kWeakSelf(self);
//    vc.topicSelectblock = ^(TopicModel * _Nonnull model) {
//        weakself.topicBtn.hidden = YES;
//        weakself.chosenTopicView.hidden = NO;
//        weakself.chosenTopicLabel.text = [NSString stringWithFormat:@"#%@#",model.title];
//    };
//    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)delChosenTopicClick:(UIButton *)sender {
    self.topicBtn.hidden = NO;
    self.chosenTopicView.hidden = YES;
    self.chosenTopicLabel.text = @"";
}

- (IBAction)priceBtnClick:(id)sender{
    QNAlertView *alert = [QNAlertView showTextFieldWithTitle:@"设置解锁价格" placeHolder:@"设置为0则为免费" leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"确定" rightClick:^(NSString *text) {
        if (text.length > 0) {
            self->unlock_price = [text integerValue];
            if (self->unlock_price > 0) {
                [self.priceBtn setTitle:[NSString stringWithFormat:@"%ld金币", self->unlock_price] forState:UIControlStateNormal];
            }else{
                [self.priceBtn setTitle:@"设置解锁价格" forState:UIControlStateNormal];
            }
        }
    }];
    alert.textFieldText = [NSString stringWithFormat:@"%ld",(long)unlock_price];
}

- (IBAction)delVideoClick:(id)sender {
    self.videoImgView.image = [UIImage new];
    self.momentType = MomentTypeText;
    video_url = @"";
    videoCoverPath = @"";
    chooseVideoPath = @"";
}

- (IBAction)chooseGroupBtnClick:(UIButton *)sender {
    kWeakSelf(self)
    SelectGroupController *vc = [[SelectGroupController alloc]init];
    vc.groupSelectblock = ^(GroupModel * _Nonnull model) {
        weakself.chooseGroupId = model.groupid;
        weakself.groupNameLabel.text = model.title;
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)textViewDidChange:(UITextView *)textView{
    if (self.inputTextView.text.length == 0 && self.momentType == MomentTypeText) {
        self.publishBtn.enabled = NO;
    }else{
        self.publishBtn.enabled = YES;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if (![text isEqualToString:@""]){
        self.placeHolderTview.hidden = YES;
    }

    if ([text isEqualToString:@""] && range.location == 0 && range.length == 1){
        self.placeHolderTview.hidden = NO;
    }

    return YES;
}

- (IBAction)backBtnClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setMomentType:(MomentType)momentType{
    _momentType = momentType;
    if (momentType == MomentTypeText) {
        self.inputTViewHLC.constant = 249;
        self.videoTopToTextLc.constant = 50 ;
        self.stackTopToTextLc.constant = 50 ;
        self.videoHolderView.hidden = YES;
        self.skView.hidden = YES;
        self.typeChooseView.hidden = NO;
      
        self.unlockTopToVideoLc.active = NO ;
    }else if (momentType == MomentTypeImages){
        self.videoTopToTextLc.constant = 10 ;
        self.stackTopToTextLc.constant = 10 ;
        self.inputTViewHLC.constant = 80;
        self.videoHolderView.hidden = YES;
        self.skView.hidden = NO;
        self.typeChooseView.hidden = YES;
        self.unlockTopToVideoLc.active = NO;
    }else{
        self.videoTopToTextLc.constant = 10 ;
        self.stackTopToTextLc.constant = 10 ;
        self.inputTViewHLC.constant = 80;
        self.videoHolderView.hidden = NO;
        self.skView.hidden = YES;
        self.typeChooseView.hidden = YES;
        self.unlockTopToVideoLc.active = YES;
    }
    BOOL isHidden = momentType != MomentTypeText;
    self.inputTextView.hidden = isHidden ;
    self.placeHolderTview.hidden = isHidden ;
    self.setUnlockPriceView.hidden = momentType == MomentTypeText;
}

- (void)yb_imageBrowser:(YBImageBrowser *)imageBrowser pageChanged:(NSInteger)page data:(id<YBIBDataProtocol>)data{
    browserIndex = page;
}

- (void)dealloc{
    NSLog(@"PublishMomentVC-dealloc");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
