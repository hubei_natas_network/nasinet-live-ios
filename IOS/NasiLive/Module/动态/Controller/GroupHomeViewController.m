//
//  GroupHomeViewController.m
//  NasiBBS
//
//  Created by yun11 on 2020/9/23.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "GroupHomeViewController.h"
#import "GroupInfoViewController.h"
#import "MomentInfoViewController.h"

#import <TYCyclePagerView.h>
#import "TYPageControl.h"
#import "TYCyclePagerViewCell.h"

#import "GroupFindCell.h"

#import "AdModel.h"
#import "GroupModel.h"

@interface GroupHomeViewController ()<UITableViewDelegate,UITableViewDataSource,GroupFindCellDelegate,TYCyclePagerViewDataSource,TYCyclePagerViewDelegate>{
    BOOL                        isLoaded;
    
    TYCyclePagerView            *tyCyclePagerView;
    TYPageControl               *pageControl;
    UIView                      *headerView;
    
    NSMutableArray              *topDatasource;
    NSMutableArray              *datasource;
}

@end

@implementation GroupHomeViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"圈子";
    self.isShowLeftBack = NO;
    
    datasource = [NSMutableArray array];
    topDatasource = [NSMutableArray array];
    
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 100;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self removeTableMJFooter];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        make.bottom.offset(0);
    }];
    
    [GroupFindCell registerWithTableView:self.tableView];
     //隐藏轮播图
//    [self createHeaderView];
    
    [self reqData];
}


- (void)createHeaderView{
    if (headerView) {
        [headerView removeFromSuperview];
        headerView = nil;
    }
    headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, kScreenWidth / 18 * 5 + 5)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    tyCyclePagerView = [[TYCyclePagerView alloc]init];
    tyCyclePagerView.isInfiniteLoop = YES;
    tyCyclePagerView.autoScrollInterval = 3.0;
    tyCyclePagerView.dataSource = self;
    tyCyclePagerView.delegate = self;
    // registerClass or registerNib
    [tyCyclePagerView registerClass:[TYCyclePagerViewCell class] forCellWithReuseIdentifier:@"TYCyclePagerViewCellID"];
    
    tyCyclePagerView.frame = CGRectMake(0, 0, kScreenWidth, kScreenWidth / 18 * 5);
    [headerView addSubview:tyCyclePagerView];
    self.tableView.tableHeaderView = headerView;
}

- (void)headerRereshing{
    [self reqData];
}

- (void)reqData{
    if (!isLoaded) {
        [commonManager showLoadingAnimateInView:self.tableView];
    }
    [CommonManager POST:@"group/getGroupPageList" parameters:nil success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        if (!self->isLoaded) {
            self->isLoaded = YES;
            [commonManager hideAnimateHud];
        }
        if (RESP_SUCCESS(responseObject)) {
            [self->datasource removeAllObjects];
            NSArray *models = [NSArray yy_modelArrayWithClass:[GroupModel class] json:responseObject[@"data"]];
            [self->datasource addObjectsFromArray:models];
            
            [self.tableView reloadData];
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        if (!self->isLoaded) {
            self->isLoaded = YES;
            [commonManager hideAnimateHud];
        }
        [self.collectionView.mj_header endRefreshing];
        RESP_FAILURE;
    }];
    
    [CommonManager POST:@"Ads/getHomeScrollAd" parameters:nil success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            [self->topDatasource removeAllObjects];
            NSArray *models = [NSArray yy_modelArrayWithClass:[AdModel class] json:responseObject[@"data"]];
            [self->topDatasource addObjectsFromArray:models];
            [self->tyCyclePagerView reloadData];
        }
    } failure:^(NSError *error) {
    }];
}


#pragma mark —————————— tableviewdelegate + datasource ——————————
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GroupFindCell *cell = [GroupFindCell cellWithTableView:tableView indexPath:indexPath];
    cell.model = datasource[indexPath.row];
    cell.delegate = self;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    GroupInfoViewController *vc = [[GroupInfoViewController alloc]init];
    vc.model = datasource[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)momentClick:(MomentModel *)model{
    MomentInfoViewController *vc = [[MomentInfoViewController alloc]init];
    vc.momentModel = model;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)attentBtnClick:(GroupModel *)model{
    NSDictionary *param = @{@"groupid":@(model.groupid),
                            @"type":model.isFollowed?@(0):@(1),
    };
    [CommonManager POST:@"group/attentGroup" parameters:param success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            model.isFollowed = !model.isFollowed;
            if (model.isFollowed) {
                model.follow_count ++;
            }else{
                model.follow_count --;
            }
            [self.tableView reloadData];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}


#pragma mark -------------------- TYCyclePagerViewDataSource  --------------------------------

- (NSInteger)numberOfItemsInPagerView:(TYCyclePagerView *)pageView {
    return topDatasource.count;
}

- (UICollectionViewCell *)pagerView:(TYCyclePagerView *)pagerView cellForItemAtIndex:(NSInteger)index {
    AdModel *model = topDatasource[index];
    TYCyclePagerViewCell *cell = [pagerView dequeueReusableCellWithReuseIdentifier:@"TYCyclePagerViewCellID" forIndex:index];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:model.image_url] placeholderImage:IMAGE_NAMED(@"cover_loading")];
    return cell;
}

- (TYCyclePagerViewLayout *)layoutForPagerView:(TYCyclePagerView *)pageView {
    TYCyclePagerViewLayout *layout = [[TYCyclePagerViewLayout alloc]init];
    layout.itemSize = CGSizeMake(pageView.width * 0.9, pageView.height * 0.9);
    layout.layoutType = TYCyclePagerTransformLayoutLinear;
    layout.itemSpacing = 5;
    return layout;
}

- (void)pagerView:(TYCyclePagerView *)pageView didScrollFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    //    pageControl.currentPage = toIndex;
    //[_pageControl setCurrentPage:newIndex animate:YES];
}

- (void)pagerView:(TYCyclePagerView *)pageView didSelectedItemCell:(__kindof UICollectionViewCell *)cell atIndex:(NSInteger)index{
    AdModel *model = topDatasource[index];
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:model.jump_url] options:@{} completionHandler:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
