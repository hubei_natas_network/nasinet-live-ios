//
//  SelectGroupController.m
//  NASI
//
//  Created by yun7_mac on 2019/9/24.
//  Copyright © 2019 cat. All rights reserved.
//

#import "SelectGroupController.h"
#import "GroupSimpleCell.h"
#import "GroupModel.h"

@interface SelectGroupController ()<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) NSArray   *dataArray;
@property (assign, nonatomic) BOOL      loaded;

@end

@implementation SelectGroupController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"选择圈子";
    
    self.navigationController.navigationBar.backgroundColor = RGB(246, 246, 246);
    self.navigationController.interactivePopGestureRecognizer.delegate = (id) self;
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 100;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self removeTableMJFooter];
    
    [GroupSimpleCell registerWithTableView:self.tableView];
    
    [self pullInternet];
}

- (void)headerRereshing{
    [self pullInternet];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GroupSimpleCell *cell = [GroupSimpleCell cellWithTableView:tableView indexPath:indexPath];
    cell.model = _dataArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.groupSelectblock) {
        self.groupSelectblock(self.dataArray[indexPath.row]);
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [GroupSimpleCell cellHeight];
}

//获取网络数据
- (void)pullInternet{
    if (!self.loaded) {
        [commonManager showLoadingAnimateInWindow];
    }
    [CommonManager POST:@"group/getGroupList" parameters:nil success:^(id responseObject) {
        self.loaded = YES;
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            self.dataArray = [NSArray yy_modelArrayWithClass:[GroupModel class] json:responseObject[@"data"]];
            [self.tableView reloadData];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
