//
//  GroupInfoViewController.m
//  NasiBBS
//
//  Created by yun11 on 2020/9/23.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "GroupInfoViewController.h"
#import "UserInfoViewController.h"
#import "MomentInfoViewController.h"
#import "ReportCategoryViewController.h"
#import "VipCenterViewController.h"

#import "MomentImagesCell.h"
#import "MomentTextCell.h"
#import "MomentVideoCell.h"
#import "MomentSingleImageCell.h"
#import "MomentRecTableViewCell.h"

#import "GroupInfoHeaderView.h"

#import "SharePanelView.h"

#import <SJVideoPlayer.h>
#import <SJBaseVideoPlayer/UIScrollView+ListViewAutoplaySJAdd.h>
#import <YBImageBrowser.h>

#import "MomentModel.h"

#define pagesize 10

@interface GroupInfoViewController ()<UITableViewDelegate,UITableViewDataSource,MomentBaseTableViewCellDelegate,SharePanelViewDelegate,GroupInfoHeaderViewDelegate>{
    NSMutableArray  *datasource;
    
    SJVideoPlayer   *sjPlayer;
    
    GroupInfoHeaderView *headerView;
}

@property (strong, nonatomic) UIView        *navigationBar;
@property (strong, nonatomic) UIView        *navigationBackgroundView;
@property (strong, nonatomic) UIButton      *backBtn;
@property (strong, nonatomic) UILabel       *titleLabel;


@property (weak, nonatomic) MomentModel     *currentModel;

@end

@implementation GroupInfoViewController

- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    datasource = [NSMutableArray array];
    
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 100;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self removeTableMJFooter];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    [MomentImagesCell registerWithTableView:self.tableView];
    [MomentSingleImageCell registerWithTableView:self.tableView];
    [MomentVideoCell registerWithTableView:self.tableView];
    [MomentTextCell registerWithTableView:self.tableView];
    [MomentRecTableViewCell registerWithTableView:self.tableView];
    
    [self createNavBar];
    
    [self reqDataAtPage:1];
    
    [self requestGroupInfo];
}


- (void)createNavBar{
    _navigationBar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, kTopHeight)];
    _navigationBar.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_navigationBar];
    
    _navigationBackgroundView = [[UIView alloc]initWithFrame:_navigationBar.bounds];
    _navigationBackgroundView.backgroundColor = [UIColor whiteColor];
    _navigationBackgroundView.alpha = 0;
    [_navigationBar addSubview:_navigationBackgroundView];
    
    _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backBtn setImage:[UIImage imageNamed:@"ic_back_white"] forState:UIControlStateNormal];
    [_backBtn setImage:[UIImage imageNamed:@"ic_back"] forState:UIControlStateSelected];
    _backBtn.frame = CGRectMake(0, kStatusBarHeight, 44, 44);
    [_backBtn addTarget:self action:@selector(backBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [_navigationBar addSubview:_backBtn];
    
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(44, kStatusBarHeight, KScreenWidth - 44*2, 44)];
    _titleLabel.textColor = [UIColor blackColor];
    _titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.text = self.model.title;
    _titleLabel.alpha = 0;
    [_navigationBar addSubview:_titleLabel];

}

- (void)requestGroupInfo{
    [commonManager showLoadingAnimateInView:self.view];
    [CommonManager POST:@"group/getGroupInfo" parameters:@{@"groupid":@(self.model.groupid)} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            self.model = [GroupModel yy_modelWithDictionary:responseObject[@"data"]];
            [self setUpTableHeaderView];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)setUpTableHeaderView{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 0)];
    headerView = [[GroupInfoHeaderView alloc]init];
    headerView.model = self.model;
    headerView.delegate = self;
    [view addSubview: headerView];
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.mas_equalTo(0);
    }];
    
    CGFloat height = KScreenWidth/12*5 + 172;
    view.height = height;
    self.tableView.tableHeaderView = view;
}

- (void)headerRereshing{
    [self reqDataAtPage:1];
}

- (void)footerRereshing{
    int page = ceil((datasource.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    [self reqDataAtPage:page];
}

- (void)reqDataAtPage:(int)page{
    NSDictionary *params = @{@"page":@(page),
                             @"size":@(pagesize),
                             @"groupid":@(self.model.groupid)
    };
    
    [CommonManager POST:@"group/getGroupMoment" parameters:params success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            if (page == 1) {
                [self->datasource removeAllObjects];
            }
            
            [self->sjPlayer stop];
            [self.tableView sj_removeCurrentPlayerView];
            [self.tableView sj_playNextVisibleAsset];
            
            NSDictionary *dataDict = responseObject[@"data"];
            NSArray *models = [NSArray yy_modelArrayWithClass:[MomentModel class] json:dataDict];
            [self->datasource addObjectsFromArray:models];
            [self.tableView reloadData];
            
            if (models.count < pagesize) {
                [self removeTableMJFooter];
            }else{
                [self setupTableViewMJFooter];
            }
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        RESP_FAILURE;
    }];
}

#pragma mark —————————— tableviewdelegate + datasource ——————————
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MomentModel *model = datasource[indexPath.row];
    if (model.type == MomentTypeImages) {
        
        if (model.image_urls.count == 1) {
            MomentSingleImageCell *cell = [MomentSingleImageCell cellWithTableView:tableView indexPath:indexPath];
            cell.delegate = self;
            cell.model = model;
            return cell;
        }else{
            MomentImagesCell *cell = [MomentImagesCell cellWithTableView:tableView indexPath:indexPath];
            cell.delegate = self;
            cell.model = model;
            return cell;
        }
    }else if (model.type == MomentTypeText){
        MomentTextCell *cell = [MomentTextCell cellWithTableView:tableView indexPath:indexPath];
        
        cell.delegate = self;
        cell.model = model;
        return cell;
    }else if (model.type == MomentTypeVideo){
        MomentVideoCell *cell = [MomentVideoCell cellWithTableView:tableView indexPath:indexPath];
        
        cell.delegate = self;
        cell.model = model;
        return cell;
    }
    return [[UITableViewCell alloc]init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MomentInfoViewController *vc = [[MomentInfoViewController alloc]init];
    vc.momentModel = datasource[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

#pragma mark  ——————————————————————   SharePanelViewDelegate   ——————————————————————————
- (void)shareWithChannel:(PYShareChannel)channel panel:(nonnull SharePanelView *)panelView{
    if (channel == PYShareChannelReport) {
        ReportCategoryViewController *vc = [[ReportCategoryViewController alloc]init];
        vc.relateid = self.currentModel.momentid;
        vc.type = ReportTypeShortVideo;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (channel == PYShareChannelCollect){
        if (![CommonManager checkAndLogin]) {
            return;
        }
        [commonManager showLoadingAnimateInWindow];
        [CommonManager POST:@"moment/collectMoment" parameters:@{@"momentid":@(self.currentModel.momentid), @"type":@(!self.currentModel.collected)} success:^(id responseObject) {
            [commonManager hideAnimateHud];
            if (RESP_SUCCESS(responseObject)) {
                [commonManager showSuccessAnimateInWindow];
                self.currentModel.collected = !self.currentModel.collected;
                [panelView setCollectBtnSelected:self.currentModel.collected];
            }else{
                RESP_SHOW_ERROR_MSG(responseObject);
            }
        } failure:^(NSError *error) {
            [commonManager hideAnimateHud];
            RESP_FAILURE;
        }];
    }else if (channel == PYShareChannelLink){
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = [NSString stringWithFormat:@"%@%d",[configManager appConfig].share_moment_url,self.currentModel.momentid];
        [MBProgressHUD showTipMessageInView:@"链接已复制到剪贴板"];
    }else{
        SSDKPlatformType type = [SharePanelView getSSDKPlatformTypeBy:channel];
        //统一创建分享参数
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:self.currentModel.title
                                         images:self.currentModel.user.avatar
                                            url:[NSURL URLWithString:[NSString stringWithFormat:@"%@%d",[configManager appConfig].share_moment_url,self.currentModel.momentid]]
                                          title:@"推荐你看这个动态"
                                           type:SSDKContentTypeAuto];
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            switch (state) {
                case SSDKResponseStateSuccess:
                    [MBProgressHUD showTipMessageInView:@"分享成功"];
                    break;
                case SSDKResponseStateFail:
                {
                    [MBProgressHUD showTipMessageInView:@"分享失败"];
                    //失败
                    break;
                }
                case SSDKResponseStateCancel:
                    //取消
                    break;
                    
                default:
                    break;
            }
        }];
    }
}

#pragma mark —————————— MomentBaseTableViewCellDelegate ———————————————
- (void)playBtnClick:(MomentBaseTableViewCell *)cell{
    if (!sjPlayer ) {
        sjPlayer = [SJVideoPlayer player];
        sjPlayer.playbackObserver.playbackDidFinishExeBlock = ^(__kindof SJBaseVideoPlayer * _Nonnull player) {
            [player replay];
        };
    }
    sjPlayer.URLAsset = [[SJVideoPlayerURLAsset alloc] initWithURL:[NSURL URLWithString:cell.model.video_url] playModel:[SJPlayModel playModelWithTableView:self.tableView indexPath:[self.tableView indexPathForCell:cell]]];
    sjPlayer.URLAsset.title = @"";
    sjPlayer.muted = YES;
    
    //检测播放权限
    [CommonManager POST:@"moment/checkCanPlay" parameters:@{@"momentid":@(cell.model.momentid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self->sjPlayer.muted = NO;
        }else{
            [self->sjPlayer stop];
            [self.tableView sj_removeCurrentPlayerView];
            [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"观影次数不足" contentText:@"开通Vip尽享无限观影" leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"开通" rightClick:^{
                [self.navigationController pushViewController:[[VipCenterViewController alloc]init] animated:YES];
            }];
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
        [self->sjPlayer stop];
        [self.tableView sj_removeCurrentPlayerView];
    }];
}

- (void)unlockClick:(MomentBaseTableViewCell *)cell{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"解锁动态" contentText:[NSString stringWithFormat:@"解锁此动态需要支付%d金币", cell.model.unlock_price] leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"解锁" rightClick:^{
        [commonManager showLoadingAnimateInWindow];
        [CommonManager POST:@"Moment/unlockMoment" parameters:@{@"momentid":@(cell.model.momentid)} success:^(id responseObject) {
            [commonManager hideAnimateHud];
            if (RESP_SUCCESS(responseObject)) {
                cell.model.unlocked = YES;
                if (cell.model.type == MomentTypeVideo) {
                    //解锁后播放视频
                    if (!self->sjPlayer ) {
                        self->sjPlayer = [SJVideoPlayer player];
                        self->sjPlayer.playbackObserver.playbackDidFinishExeBlock = ^(__kindof SJBaseVideoPlayer * _Nonnull player) {
                            [player replay];
                        };
                    }
                    self->sjPlayer.URLAsset = [[SJVideoPlayerURLAsset alloc] initWithURL:[NSURL URLWithString:cell.model.video_url] playModel:[SJPlayModel playModelWithTableView:self.tableView indexPath:[self.tableView indexPathForCell:cell]]];
                    self->sjPlayer.URLAsset.title = @"";
                }else{
                    [self.tableView reloadData];
                }
            }else{
                RESP_SHOW_ERROR_MSG(responseObject);
            }
        } failure:^(NSError *error) {
            [commonManager hideAnimateHud];
            RESP_FAILURE;
        }];
    }];
}

- (void)likeClick:(MomentModel *)model{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    if (model.liked) {
        return;
    }
    [CommonManager POST:@"Moment/likeMoment" parameters:@{@"momentid":@(model.momentid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            model.liked = YES;
            model.like_count = [responseObject[@"data"][@"like_count"] intValue];
            [self.tableView reloadData];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (void)commentClick:(MomentModel *)model{
    MomentInfoViewController *vc = [[MomentInfoViewController alloc]init];
    vc.momentModel = model;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)moreFuncClick:(MomentModel *)model{
    SharePanelView *panelView = [SharePanelView showPanelInView:kAppWindow];
    panelView.delegate = self;
    self.currentModel = model;
}

- (void)imgViewClick:imageView model:(MomentModel *)model index:(NSInteger)index{
    NSMutableArray *datas = [NSMutableArray array];
    [model.image_urls enumerateObjectsUsingBlock:^(NSString *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        // 网络图片
        YBIBImageData *data = [YBIBImageData new];
        data.imageURL = [NSURL URLWithString:obj];
        data.projectiveView = imageView;
        [datas addObject:data];
    }];
    
    YBImageBrowser *browser = [YBImageBrowser new];
    browser.dataSourceArray = datas;
    browser.currentPage = index;
    // 只有一个保存操作的时候，可以直接右上角显示保存按钮
    browser.defaultToolViewHandler.topView.operationType = YBIBTopViewOperationTypeSave;
    [browser show];
}

- (void)userIconClick:(MomentModel *)model{
    UserInfoViewController *vc = [[UserInfoViewController alloc]init];
    vc.anchorid = model.user.userid;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)followBtnClick:(UIButton *)sender model:(GroupModel *)model{
    NSDictionary *param = @{@"groupid":@(model.groupid),
                            @"type":model.isFollowed?@(0):@(1),
    };
    [CommonManager POST:@"group/attentGroup" parameters:param success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            model.isFollowed = !model.isFollowed;
            if (model.isFollowed) {
                model.follow_count ++;
            }else{
                model.follow_count --;
            }
            self->headerView.model = model;
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat offsetY = scrollView.contentOffset.y;
    // 0-200 0
    // 200 - KDYHeaderHeigh - kNavBarheight 渐变从0-1
    // > KDYHeaderHeigh - kNavBarheight 1
    CGFloat alpha = 0;
    if (offsetY < 200) {
        alpha = 0;
    }else if (offsetY > (400 - kTopHeight)) {
        alpha = 1;
    }else {
        alpha = (offsetY - 200) / (400 - kTopHeight - 200);
    }
    self.StatusBarStyle = alpha > 0.2 ? UIStatusBarStyleDefault:UIStatusBarStyleLightContent;
    self.backBtn.selected = alpha > 0.2;
    self.titleLabel.alpha = alpha;
    self.navigationBackgroundView.alpha = alpha;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
