//
//  MomentRootViewController.m
//  Meet1V1
//
//  Created by yun on 2020/1/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MomentRootViewController.h"
#import "MomentListViewController.h"
#import "PublishMomentViewController.h"
#import "PublishTextMomentViewController.h"

#import "SearchViewController.h"

#import "PopTextNotificationView.h"
#import "PopImgNotificationView.h"
#import "UpdateNotificationView.h"

#import "H5ViewController.h"

#import "XLPageViewController.h"

@interface MomentRootViewController ()<XLPageViewControllerDelegate, XLPageViewControllerDataSrouce, PopNotificationViewDelegate>{
    UIView     *selTypeView;
    UIView     *shadowView;
    UIButton   *startMomentBtn;

    NSArray *pageTitleArr;
}

@property (strong, nonatomic) PopNotificationView *popNotificationView;

@end

@implementation MomentRootViewController

- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    pageTitleArr = @[@"关注", @"推荐", @"最新", @"短文", @"图片", @"视频"];
    
    [self createUI];
    
    if ([userManager isLogined]) {
        [userManager refreshFromServer];
    }
    
//    [self checkVersionUpdate];
}

- (void)createUI{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, kStatusBarHeight )];
    headerView.backgroundColor = MAIN_COLOR;
    [self.view addSubview:headerView];
    
    XLPageViewControllerConfig *config = [XLPageViewControllerConfig defaultConfig];
    config.separatorLineHidden = YES;
    config.titleNormalColor   = [UIColor whiteColor];
    config.titleSelectedColor = [UIColor whiteColor];
    config.titleNormalFont = FFont15;
    config.titleViewBackgroundColor = MAIN_COLOR ;
    config.titleSelectedFont = [UIFont boldSystemFontOfSize:22];
    config.titleSpace = 15.f;
    
    config.titleViewHeight = 44;
    config.titleViewInset = UIEdgeInsetsMake(5, 15, 0, 44);
    

    config.shadowLineHidden = YES;
    
    XLPageViewController *pageViewController = [[XLPageViewController alloc] initWithConfig:config];
    pageViewController.view.frame = CGRectMake(0, headerView.height, KScreenWidth, KScreenHeight  - kTabBarHeight);
    pageViewController.delegate = self;
    pageViewController.dataSource = self;
    [self.view addSubview:pageViewController.view];
    [self addChildViewController:pageViewController];
    [pageViewController setSelectedIndex:1];
    
    startMomentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    startMomentBtn.frame = CGRectMake(self.view.width - 60 - 15, self.view.height - kTabBarHeight - 20 - 60, 60, 60);
    [startMomentBtn setImage:IMAGE_NAMED(@"ic_post") forState:UIControlStateNormal];
    [startMomentBtn addTarget:self action:@selector(publishBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:startMomentBtn];
}


- (void)publishBtnClick{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    PublishMomentViewController *vc = [[PublishMomentViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    
}


- (void)publishTextBtnClick{
    PublishTextMomentViewController *vc = [[PublishTextMomentViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}



- (void)searchBtnClick{
    SearchViewController *vc = [[SearchViewController alloc]init];
    [self.navigationController pushViewController:vc animated:NO];
}

- (UIButton *)createPublishBtnWithImageName:(NSString *)imageName title:(NSString *)title frame:(CGRect)frame selector:(nonnull SEL)selector{
    UIButton *publishTextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [publishTextBtn setImage:IMAGE_NAMED(imageName) forState:UIControlStateNormal];
    [publishTextBtn setTitle:title forState:UIControlStateNormal];
    [publishTextBtn setTitleColor:[UIColor colorWithHexString:@"444444"] forState:UIControlStateNormal];
    publishTextBtn.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
    [publishTextBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    [publishTextBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [publishTextBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    publishTextBtn.frame = frame;
    [publishTextBtn addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    return publishTextBtn;
}

//根据index创建对应的视图控制器，每个试图控制器只会被创建一次。
- (UIViewController *)pageViewController:(XLPageViewController *)pageViewController viewControllerForIndex:(NSInteger)index{
    MomentListViewController *vc = [[MomentListViewController alloc]init];
    vc.listType = index - 2;
    return vc;
}

//根据index返回对应的标题
- (NSString *)pageViewController:(XLPageViewController *)pageViewController titleForIndex:(NSInteger)index{
    return pageTitleArr[index];
}

//返回分页数
- (NSInteger)pageViewControllerNumberOfPage{
    return pageTitleArr.count;
}

- (void)pageViewController:(XLPageViewController *)pageViewController didSelectedAtIndex:(NSInteger)index{
    
}

#pragma mark - —————————————————— PopNotificationViewDelegate ——————————————————————————
- (void)popNotificationViewClick:(AdModel *)model{
    [self.popNotificationView hide];
    if (model.jump_type == AdJumpTypeInApp) {
        H5ViewController *vc = [[H5ViewController alloc]init];
        vc.href = model.jump_url;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:model.jump_url] options:@{} completionHandler:nil];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
