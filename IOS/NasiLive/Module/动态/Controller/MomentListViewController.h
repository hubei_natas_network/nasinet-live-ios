//
//  MomentListViewController.h
//  Meet1V1
//
//  Created by yun on 2020/1/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"
#import "JXCategoryListContainerView.h"
#import "SearchVcProtocal.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, MomentListType) {
    MomentListTypeNewest = 0,       //最新
    MomentListTypeText,
    MomentListTypeImage,
    MomentListTypeVideo,
    MomentListTypeRec = -1,         //推荐
    MomentListTypeAttent = -2,      //关注
    MomentListTypeHotRank = -3,     //热度排行
    MomentListTypePaidRank = -4,    //付费排行
    MomentListTypeWatchLog = -5,    //付费排行
    MomentListTypeSearch = -6,      //搜索
};

typedef NS_ENUM(NSUInteger, MomentListRankType) {
    MomentListRankTypeDay = 0,
    MomentListRankTypeWeek,
    MomentListRankTypeMonth
};

@interface MomentListViewController : RootViewController<JXCategoryListContentViewDelegate,SearchVcProtocal>

@property (nonatomic,assign) MomentListType         listType;
@property (assign, nonatomic) MomentListRankType    rankType;

@end

NS_ASSUME_NONNULL_END
