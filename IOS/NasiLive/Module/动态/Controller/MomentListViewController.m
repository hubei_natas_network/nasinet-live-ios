//
//  MomentListViewController.m
//  Meet1V1
//
//  Created by yun on 2020/1/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MomentListViewController.h"
#import "UserInfoViewController.h"
#import "MomentInfoViewController.h"
#import "ReportCategoryViewController.h"
#import "VipCenterViewController.h"
#import "GroupInfoViewController.h"
#import "RankHomeViewController.h"
#import "UserLevelViewController.h"
#import "MomentRankViewController.h"
#import "GroupHomeViewController.h"

#import "MomentImagesCell.h"
#import "MomentTextCell.h"
#import "MomentVideoCell.h"
#import "MomentSingleImageCell.h"
#import "MomentRecTableViewCell.h"

#import "SharePanelView.h"

#import <SJVideoPlayer.h>
#import <SJBaseVideoPlayer/UIScrollView+ListViewAutoplaySJAdd.h>
#import <YBImageBrowser.h>

#import "MomentModel.h"

#define pagesize 10
#define EntranceTagIndex 10000

@interface MomentListViewController ()<UITableViewDelegate,UITableViewDataSource,MomentBaseTableViewCellDelegate,SharePanelViewDelegate>{
    NSMutableArray  *datasource;
    MomentModel     *recModel;
    
    SJVideoPlayer   *sjPlayer;
    
    BOOL            isFirstLoad;
}

@property (copy, nonatomic) NSString                *api;
@property (strong, nonatomic) NSMutableDictionary   *requestParam;
@property (weak, nonatomic) MomentModel             *currentModel;

@property (copy, nonatomic) NSString                *keyword;

@end

@implementation MomentListViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [sjPlayer vc_viewDidAppear];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [sjPlayer vc_viewWillDisappear];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [sjPlayer vc_viewDidDisappear];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    datasource = [NSMutableArray array];
    isFirstLoad = YES;
    
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 100;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self removeTableMJFooter];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    [MomentImagesCell registerWithTableView:self.tableView];
    [MomentSingleImageCell registerWithTableView:self.tableView];
    [MomentVideoCell registerWithTableView:self.tableView];
    [MomentTextCell registerWithTableView:self.tableView];
    [MomentRecTableViewCell registerWithTableView:self.tableView];
    
    if (!(self.listType == MomentListTypeSearch  && self.keyword.length == 0)) {
        [self reqDataAtPage:1];
    }
    
    [self createTableViewHeader];
}

- (void)createTableViewHeader{
    if (self.listType == MomentListTypeRec) {
        CGFloat itemW = (KScreenWidth - 14*2 - 9*3)/4;
        CGFloat itemH = itemW*5/4;
        UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, itemH + 15*2)];
        headerView.backgroundColor = [UIColor whiteColor];
        
        NSArray *btnImgArr = @[@"ic_hot_rank_entrance",@"ic_fee_rank_entrance",@"ic_user_rank_entrance",@"ic_task_entrance"];
        for (int i = 0; i < btnImgArr.count; i++) {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(14+(itemW + 9)*i, 15, itemW, itemH);
            btn.tag = EntranceTagIndex+i;
            [btn setBackgroundImage:[UIImage imageNamed:btnImgArr[i]] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(entranceBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            [headerView addSubview:btn];
        }
        self.tableView.tableHeaderView = headerView;
    }
}

- (void)entranceBtnClick:(UIButton *)sender{
    switch (sender.tag - EntranceTagIndex) {
        case 0:{
            MomentRankViewController *vc = [[MomentRankViewController alloc]init];
            vc.listType = MomentListTypeHotRank;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 1:{
            MomentRankViewController *vc = [[MomentRankViewController alloc]init];
            vc.listType = MomentListTypePaidRank;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2:{
            RankHomeViewController *vc = [[RankHomeViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 3:{
            GroupHomeViewController *vc = [[GroupHomeViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        default:
            break;
    }
}

- (void)headerRereshing{
    [self reqDataAtPage:1];
}

- (void)footerRereshing{
    int page = ceil((datasource.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    [self reqDataAtPage:page];
}

- (void)reqDataAtPage:(int)page{
    self.requestParam[@"page"] = @(page);
    
    if (page == 1 && isFirstLoad) {
        [commonManager showLoadingAnimateInView:self.tableView];
    }
    
    [CommonManager POST:self.api parameters:self.requestParam success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if (page == 1 && self->isFirstLoad) {
            self->isFirstLoad = NO;
            [commonManager hideAnimateHud];
        }
        if (RESP_SUCCESS(responseObject)) {
            if (page == 1) {
                [self->datasource removeAllObjects];
                if (self.listType == MomentListTypeRec) {
                    if (ValidDict(responseObject[@"data"][@"recommend"])) {
                        self->recModel = [MomentModel yy_modelWithDictionary:responseObject[@"data"][@"recommend"]];
                    }
                }
            }
            
            [self->sjPlayer stop];
            [self.tableView sj_removeCurrentPlayerView];
            [self.tableView sj_playNextVisibleAsset];
            
            NSDictionary *dataDict = self.listType == MomentListTypeRec ? responseObject[@"data"][@"list"] : responseObject[@"data"];
            NSArray *models = [NSArray yy_modelArrayWithClass:[MomentModel class] json:dataDict];
            [self->datasource addObjectsFromArray:models];
            [self.tableView reloadData];
            
            if (models.count < pagesize) {
                [self removeTableMJFooter];
            }else{
                [self setupTableViewMJFooter];
            }
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        if (page == 1 && self->isFirstLoad) {
            self->isFirstLoad = NO;
            [commonManager hideAnimateHud];
        }
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        RESP_FAILURE;
    }];
}

#pragma mark —————————— tableviewdelegate + datasource ——————————
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (recModel) {
        return datasource.count + 1;
    }
    return datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0 &&  recModel) {
        MomentRecTableViewCell *cell = [MomentRecTableViewCell cellWithTableView:tableView indexPath:indexPath];
        cell.model = recModel;
        cell.delegate = self;
        return cell;
    }else{
        NSInteger index = recModel ? indexPath.row - 1 : indexPath.row;
        MomentModel *model = datasource[index];
        if (model.type == MomentTypeImages) {
            
            if (model.image_urls.count == 1) {
                MomentSingleImageCell *cell = [MomentSingleImageCell cellWithTableView:tableView indexPath:indexPath];
                cell.delegate = self;
                cell.model = model;
                return cell;
            }else{
                MomentImagesCell *cell = [MomentImagesCell cellWithTableView:tableView indexPath:indexPath];
                cell.delegate = self;
                cell.model = model;
                return cell;
            }
        }else if (model.type == MomentTypeText){
            MomentTextCell *cell = [MomentTextCell cellWithTableView:tableView indexPath:indexPath];
            
            cell.delegate = self;
            cell.model = model;
            return cell;
        }else if (model.type == MomentTypeVideo){
            MomentVideoCell *cell = [MomentVideoCell cellWithTableView:tableView indexPath:indexPath];
            
            cell.delegate = self;
            cell.model = model;
            return cell;
        }
        return [[UITableViewCell alloc]init];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MomentInfoViewController *vc = [[MomentInfoViewController alloc]init];
    
    if (indexPath.row == 0 && recModel) {
        vc.momentModel = recModel;
    }else{
        NSInteger index = recModel ? indexPath.row - 1 : indexPath.row;
        vc.momentModel = datasource[index];
    }
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

#pragma mark  ——————————————————————   SharePanelViewDelegate   ——————————————————————————
- (void)shareWithChannel:(PYShareChannel)channel panel:(nonnull SharePanelView *)panelView{
    if (channel == PYShareChannelReport) {
        ReportCategoryViewController *vc = [[ReportCategoryViewController alloc]init];
        vc.relateid = self.currentModel.momentid;
        vc.type = ReportTypeShortVideo;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (channel == PYShareChannelCollect){
        if (![CommonManager checkAndLogin]) {
            return;
        }
        [commonManager showLoadingAnimateInWindow];
        [CommonManager POST:@"moment/collectMoment" parameters:@{@"momentid":@(self.currentModel.momentid), @"type":@(!self.currentModel.collected)} success:^(id responseObject) {
            [commonManager hideAnimateHud];
            if (RESP_SUCCESS(responseObject)) {
                [commonManager showSuccessAnimateInWindow];
                self.currentModel.collected = !self.currentModel.collected;
                [panelView setCollectBtnSelected:self.currentModel.collected];
            }else{
                RESP_SHOW_ERROR_MSG(responseObject);
            }
        } failure:^(NSError *error) {
            [commonManager hideAnimateHud];
            RESP_FAILURE;
        }];
    }else if (channel == PYShareChannelLink){
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = [NSString stringWithFormat:@"%@%d",[configManager appConfig].share_moment_url,self.currentModel.momentid];
        [MBProgressHUD showTipMessageInView:@"链接已复制到剪贴板"];
    }else{
        SSDKPlatformType type = [SharePanelView getSSDKPlatformTypeBy:channel];
        //统一创建分享参数
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:self.currentModel.title
                                         images:self.currentModel.user.avatar
                                            url:[NSURL URLWithString:[NSString stringWithFormat:@"%@%d",[configManager appConfig].share_moment_url,self.currentModel.momentid]]
                                          title:@"推荐你看这个动态"
                                           type:SSDKContentTypeAuto];
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            switch (state) {
                case SSDKResponseStateSuccess:
                    [MBProgressHUD showTipMessageInView:@"分享成功"];
                    break;
                case SSDKResponseStateFail:
                {
                    [MBProgressHUD showTipMessageInView:@"分享失败"];
                    //失败
                    break;
                }
                case SSDKResponseStateCancel:
                    //取消
                    break;
                    
                default:
                    break;
            }
        }];
    }
}

#pragma mark —————————— MomentBaseTableViewCellDelegate ———————————————
- (void)playBtnClick:(MomentBaseTableViewCell *)cell{
    if (!sjPlayer ) {
        sjPlayer = [SJVideoPlayer player];
        sjPlayer.playbackObserver.playbackDidFinishExeBlock = ^(__kindof SJBaseVideoPlayer * _Nonnull player) {
            [player replay];
        };
    }
    sjPlayer.URLAsset = [[SJVideoPlayerURLAsset alloc] initWithURL:[NSURL URLWithString:cell.model.video_url] playModel:[SJPlayModel playModelWithTableView:self.tableView indexPath:[self.tableView indexPathForCell:cell]]];
    sjPlayer.URLAsset.title = @"";
    sjPlayer.muted = YES;
    
    //检测播放权限
    [CommonManager POST:@"moment/checkCanPlay" parameters:@{@"momentid":@(cell.model.momentid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self->sjPlayer.muted = NO;
        }else{
            [self->sjPlayer stop];
            [self.tableView sj_removeCurrentPlayerView];
            [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"观影次数不足" contentText:@"开通Vip尽享无限观影" leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"开通" rightClick:^{
                [self.navigationController pushViewController:[[VipCenterViewController alloc]init] animated:YES];
            }];
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
        [self->sjPlayer stop];
        [self.tableView sj_removeCurrentPlayerView];
    }];
}

- (void)unlockClick:(MomentBaseTableViewCell *)cell{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"解锁动态" contentText:[NSString stringWithFormat:@"解锁此动态需要支付%d金币", cell.model.unlock_price] leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"解锁" rightClick:^{
        [commonManager showLoadingAnimateInWindow];
        [CommonManager POST:@"Moment/unlockMoment" parameters:@{@"momentid":@(cell.model.momentid)} success:^(id responseObject) {
            [commonManager hideAnimateHud];
            if (RESP_SUCCESS(responseObject)) {
                cell.model.unlocked = YES;
                if (cell.model.type == MomentTypeVideo) {
                    //解锁后播放视频
                    if (!self->sjPlayer ) {
                        self->sjPlayer = [SJVideoPlayer player];
                        self->sjPlayer.playbackObserver.playbackDidFinishExeBlock = ^(__kindof SJBaseVideoPlayer * _Nonnull player) {
                            [player replay];
                        };
                    }
                    self->sjPlayer.URLAsset = [[SJVideoPlayerURLAsset alloc] initWithURL:[NSURL URLWithString:cell.model.video_url] playModel:[SJPlayModel playModelWithTableView:self.tableView indexPath:[self.tableView indexPathForCell:cell]]];
                    self->sjPlayer.URLAsset.title = @"";
                }else{
                    [self.tableView reloadData];
                }
            }else{
                RESP_SHOW_ERROR_MSG(responseObject);
            }
        } failure:^(NSError *error) {
            [commonManager hideAnimateHud];
            RESP_FAILURE;
        }];
    }];
}

- (void)likeClick:(MomentModel *)model{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    if (model.liked) {
        return;
    }
    [CommonManager POST:@"Moment/likeMoment" parameters:@{@"momentid":@(model.momentid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            model.liked = YES;
            model.like_count = [responseObject[@"data"][@"like_count"] intValue];
            [self.tableView reloadData];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (void)commentClick:(MomentModel *)model{
    MomentInfoViewController *vc = [[MomentInfoViewController alloc]init];
    vc.momentModel = model;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)moreFuncClick:(MomentModel *)model{
    SharePanelView *panelView = [SharePanelView showPanelInView:kAppWindow];
    panelView.delegate = self;
    self.currentModel = model;
}

- (void)imgViewClick:imageView model:(MomentModel *)model index:(NSInteger)index{
    NSMutableArray *datas = [NSMutableArray array];
    [model.image_urls enumerateObjectsUsingBlock:^(NSString *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        // 网络图片
        YBIBImageData *data = [YBIBImageData new];
        data.imageURL = [NSURL URLWithString:obj];
        data.projectiveView = imageView;
        [datas addObject:data];
    }];
    
    YBImageBrowser *browser = [YBImageBrowser new];
    browser.dataSourceArray = datas;
    browser.currentPage = index;
    // 只有一个保存操作的时候，可以直接右上角显示保存按钮
    browser.defaultToolViewHandler.topView.operationType = YBIBTopViewOperationTypeSave;
    [browser show];
}

- (void)userIconClick:(MomentModel *)model{
    UserInfoViewController *vc = [[UserInfoViewController alloc]init];
    vc.anchorid = model.user.userid;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)groupTagClick:(GroupModel *)model{
    GroupInfoViewController *vc = [[GroupInfoViewController alloc]init];
    vc.model = model;
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSString *)api{
    if (!_api) {
        switch (self.listType) {
            case MomentListTypeRec:{
                _api = @"Moment/recommendList";
            }
                break;
            case MomentListTypeAttent:{
                _api = @"Moment/getAttentList";
            }
                break;
            case MomentListTypeHotRank:{
                _api = @"Moment/getHotRankList";
            }
                break;
            case MomentListTypePaidRank:{
                _api = @"Moment/getPaidRankList";
            }
                break;
            case MomentListTypeWatchLog:{
                _api = @"Moment/getUserWatchLog";
            }
                break;
            case MomentListTypeSearch:{
                _api = @"Moment/search";
            }
                break;
            default:
                _api = @"Moment/getList";
                break;
        }
    }
    return _api;
}

- (NSMutableDictionary *)requestParam{
    if (!_requestParam) {
        _requestParam = [NSMutableDictionary dictionary];
        _requestParam[@"size"] = @(pagesize);
        switch (self.listType) {
            case MomentListTypeRec:{
            }
                break;
            case MomentListTypeAttent:{
            }
                break;
            case MomentListTypeHotRank:
            case MomentListTypePaidRank:{
                _requestParam[@"type"] = @(self.rankType);
            }
                break;
            default:{
                _requestParam[@"type"] = @(self.listType);
            }
                break;
        }
    }
    return _requestParam;
}

- (void)setKeyword:(NSString *)keyword{
    if (keyword.length == 0 || [keyword isEqualToString:_keyword]) {
        return;
    }
    _keyword = keyword;
    self.requestParam[@"keyword"] = keyword;
    isFirstLoad = YES;
    [self reqDataAtPage:1];
}


#pragma mark - JXCategoryListContentViewDelegate

- (UIView *)listView {
    return self.view;
}

#pragma mark - SearchVcProtocal
- (void)searchKeyword:(NSString *)keyword{
    self.keyword = keyword;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
