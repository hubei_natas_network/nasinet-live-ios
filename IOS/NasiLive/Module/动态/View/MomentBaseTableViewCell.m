//
//  MomentBaseTableViewCell.m
//  Meet1V1
//
//  Created by yun on 2020/1/10.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MomentBaseTableViewCell.h"
#import "MomentModel.h"

@implementation MomentBaseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(MomentModel *)model{
    _model = model;
}

- (void)unlockClick{
    if ([self.delegate respondsToSelector:@selector(unlockClick:)]) {
        [self.delegate unlockClick:self];
    }
}

- (void)likeBtnClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(likeClick:)]) {
        [self.delegate likeClick:self.model];
    }
}

- (void)commentBtnClick:(UIButton *)sender{
    if ([self.delegate respondsToSelector:@selector(commentClick:)]) {
        [self.delegate commentClick:self.model];
    }
}

- (void)userIconClick{
    if ([self.delegate respondsToSelector:@selector(userIconClick:)]) {
        [self.delegate userIconClick:self.model];
    }
}

- (void)collectBtnClick:(UIButton *)sender{
    if ([self.delegate respondsToSelector:@selector(collectBtnClick:)]) {
        [self.delegate collectBtnClick:self.model];
    }
}

- (void)moreFuncClick:(UIButton *)sender{
    if ([self.delegate respondsToSelector:@selector(moreFuncClick:)]) {
        [self.delegate moreFuncClick:self.model];
    }
}

- (void)groupTap{
    if ([self.delegate respondsToSelector:@selector(groupTagClick:)]) {
        [self.delegate groupTagClick:self.model.group];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
