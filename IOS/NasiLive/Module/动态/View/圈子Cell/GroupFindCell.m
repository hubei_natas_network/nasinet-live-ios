//
//  GroupFindCell.m
//  NASI
//
//  Created by yun7_mac on 2019/9/22.
//  Copyright © 2019 cat. All rights reserved.
//

#import "GroupFindCell.h"

#import "GroupModel.h"
#import "MomentModel.h"

@interface GroupFindCell ()<UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *groupImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *attentBtn;

@end

@implementation GroupFindCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    for (int i = 0; i < 6; i++) {
        UIImageView *imgView = (UIImageView *)[self viewWithTag:1000+i];
        if (imgView) {
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(momentTap:)];
            tap.delegate = self;
            [imgView addGestureRecognizer:tap];
        }
    }
}

- (void)setModel:(GroupModel *)model{
    _model = model;
    
    [self resetSubView];
    
    [self.groupImgView sd_setImageWithURL:[NSURL URLWithString:model.img_url]];
    self.titleLabel.text = model.title;
    self.subTitleLabel.text = model.sub_title;
    [self.attentBtn setSelected:model.isFollowed];
    
    for (int i = 0; i < model.moments.count; i++) {
        MomentModel *momentModel = model.moments[i];
        UIImageView *imgView = (UIImageView *)[self viewWithTag:1000+i];
        if (imgView) {
            [imgView sd_setImageWithURL:[NSURL URLWithString:momentModel.cover_image_url] placeholderImage:IMAGE_NAMED(@"cover_loading")];
        }
        UIImageView *playImgView = (UIImageView *)[self viewWithTag:2000+i];
        if (playImgView) {
            if (momentModel.type == MomentTypeImages) {
                playImgView.image = IMAGE_NAMED(@"ic_moment_flag_img");
            }else{
                playImgView.image = IMAGE_NAMED(@"ic_moment_flag_video");
            }
        }
    }
}

- (void)resetSubView{
    for (int i = 0; i < 6; i++) {
        UIImageView *imgView = (UIImageView *)[self viewWithTag:1000+i];
        if (imgView) {
            imgView.image = IMAGE_NAMED(@"bg_moment_empty");
        }
        UIImageView *playImgView = (UIImageView *)[self viewWithTag:2000+i];
        if (playImgView) {
            playImgView.image = nil;
        }
    }
}

- (IBAction)attentBtnClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(attentBtnClick:)]) {
        [self.delegate attentBtnClick:self.model];
    }
}

- (void)momentTap:(UITapGestureRecognizer *)tapGes{
    NSInteger index = tapGes.view.tag - 1000;
    if ([self.delegate respondsToSelector:@selector(momentClick:)]) {
        if (self.model.moments.count > index) {
            [self.delegate momentClick:self.model.moments[index]];
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
