//
//  GroupSimpleCell.m
//  NASI
//
//  Created by yun7_mac on 2019/9/24.
//  Copyright © 2019 cat. All rights reserved.
//

#import "GroupSimpleCell.h"

#import "GroupModel.h"

@interface GroupSimpleCell ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;


@end

@implementation GroupSimpleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(GroupModel *)model{
    _model = model;
    [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:model.img_url] placeholderImage:IMAGE_NAMED(@"cover_loading")];
    self.titleLabel.text = model.title;
    self.subTitleLabel.text = model.sub_title;
}

+ (CGFloat)cellHeight{
    return 80.f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
