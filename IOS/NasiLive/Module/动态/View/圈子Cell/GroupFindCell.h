//
//  GroupFindCell.h
//  NASI
//
//  Created by yun7_mac on 2019/9/22.
//  Copyright © 2019 cat. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@class GroupModel;
@class MomentModel;

@protocol GroupFindCellDelegate <NSObject>

@optional
- (void)momentClick:(MomentModel *)model;
- (void)attentBtnClick:(GroupModel *)model;

@end

@interface GroupFindCell : BaseTableViewCell

@property (strong, nonatomic) GroupModel *model;

@property (weak, nonatomic) id<GroupFindCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
