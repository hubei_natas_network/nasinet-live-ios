//
//  GroupInfoHeaderView.h
//  NASI
//
//  Created by yun7_mac on 2019/9/29.
//  Copyright © 2019 cat. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class GroupModel;
@class MomentModel;

@protocol GroupInfoHeaderViewDelegate <NSObject>

@optional
- (void)noticeClick:(NSString *)notice;
- (void)recPostsClick:(MomentModel *)model;
- (void)followBtnClick:(UIButton *)sender model:(GroupModel *)model;

@end

@interface GroupInfoHeaderView : UIView

@property (weak, nonatomic) id<GroupInfoHeaderViewDelegate> delegate;
@property (strong, nonatomic) GroupModel *model;

@end

NS_ASSUME_NONNULL_END
