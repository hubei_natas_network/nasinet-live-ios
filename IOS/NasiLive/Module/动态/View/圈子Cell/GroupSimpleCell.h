//
//  GroupSimpleCell.h
//  NASI
//
//  Created by yun7_mac on 2019/9/24.
//  Copyright © 2019 cat. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@class GroupModel;

@interface GroupSimpleCell : BaseTableViewCell

@property (strong, nonatomic) GroupModel *model;

+ (CGFloat)cellHeight;

@end

NS_ASSUME_NONNULL_END
