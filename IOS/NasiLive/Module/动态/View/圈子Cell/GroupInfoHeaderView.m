//
//  GroupInfoHeaderView.m
//  NASI
//
//  Created by yun7_mac on 2019/9/29.
//  Copyright © 2019 cat. All rights reserved.
//

#import "GroupInfoHeaderView.h"

#import "GroupModel.h"
#import "MomentModel.h"

@interface GroupInfoHeaderView ()<UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *followerCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *postsCountLabel;

@property (weak, nonatomic) IBOutlet UIButton *followBtn;
@property (weak, nonatomic) IBOutlet UILabel *gonggaoLabel;
@property (weak, nonatomic) IBOutlet UILabel *recLabel;

@end

@implementation GroupInfoHeaderView

- (instancetype)init{
    if (self = [super init]) {
        self = [[NSBundle mainBundle]loadNibNamed:@"GroupInfoHeaderView" owner:self options:nil].firstObject;
    }
    return self;
}

- (void)setModel:(GroupModel *)model{
    _model = model;
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:model.img_url] placeholderImage:IMAGE_NAMED(@"cover_loading")];
    self.titleLabel.text = model.title;
    self.subTitleLabel.text = model.sub_title;
    self.followerCountLabel.text = [NSString stringWithFormat:@"%d 粉丝",model.follow_count];
    self.postsCountLabel.text = [NSString stringWithFormat:@"%d 贴子",model.moment_count];
    [self.followBtn setSelected:model.isFollowed];
    self.gonggaoLabel.text = model.notice;
}

- (IBAction)followBtnClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(followBtnClick:model:)]) {
        [self.delegate followBtnClick:sender model:self.model];
    }
}

@end
