//
//  MomentVideoCell.m
//  Meet1V1
//
//  Created by yun on 2020/1/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MomentVideoCell.h"
#import "SJPlayerSuperview.h"

#import "MomentModel.h"

@interface MomentVideoCell ()

@property (weak, nonatomic) IBOutlet UIImageView *userIconImgView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *ageGenderBtn;
@property (weak, nonatomic) IBOutlet UIImageView *levelImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeCountBtn;
@property (weak, nonatomic) IBOutlet UIButton *commentCountBtn;
@property (weak, nonatomic) IBOutlet UIButton *moreFuncBtn;
@property (weak, nonatomic) IBOutlet UIImageView *coverImgView;
@property (weak, nonatomic) IBOutlet SJPlayerSuperview *playerView;
@property (weak, nonatomic) IBOutlet UIView *unlockPriceView;
@property (weak, nonatomic) IBOutlet UILabel *unlockPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *groupLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coverImgHeightLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *coverImgWidthLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *skViewTopLC;

@end

@implementation MomentVideoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userIconClick)];
    [self.userIconImgView addGestureRecognizer:tap];
    UITapGestureRecognizer *groupTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(groupTap)];
    [self.groupLabel addGestureRecognizer:groupTap];
}

- (void)setModel:(MomentModel *)model{
    [super setModel:model];
    [self.userIconImgView sd_setImageWithURL:[NSURL URLWithString:model.user.avatar] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    self.userNameLabel.text = model.user.nick_name;
    [self.ageGenderBtn setBackgroundColor:model.user.profile.gender?[UIColor colorWithHexString:@"6FCAFF"]:[UIColor colorWithHexString:@"FF9CE4"]];
    self.ageGenderBtn.selected = model.user.profile.gender;
    [self.ageGenderBtn setTitle:[NSString stringWithFormat:@"%d",model.user.profile.age] forState:UIControlStateNormal];
    [self.levelImgView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"ic_user_level_%d",model.user.user_level]]];
    
    if (model.title.length > 0) {
        NSAttributedString *titleAttr = [CommonManager getAttributedStringWithString:model.title lineSpace:5];
        self.titleLabel.attributedText = titleAttr;
        self.skViewTopLC.constant = 14.f;
    }else{
        NSAttributedString *titleAttr = [CommonManager getAttributedStringWithString:@"" lineSpace:5];
        self.titleLabel.attributedText = titleAttr;
        self.skViewTopLC.constant = 0.f;
    }
    
    self.timeLabel.text = [CommonManager formateBeautyDate:model.create_time];
    self.likeCountBtn.selected = model.liked;
    [self.likeCountBtn setTitle:[NSString stringWithFormat:@"%d",model.like_count] forState:UIControlStateNormal];
    [self.commentCountBtn setTitle:[NSString stringWithFormat:@"%d",model.comment_count] forState:UIControlStateNormal];
    
    self.moreFuncBtn.hidden = model.user.userid == userManager.curUserInfo.userid;
    
    CGFloat imgW = (KScreenWidth - 14*2);
    CGFloat imgH = imgW*9/16;
    self.coverImgWidthLC.constant = imgW;
    self.coverImgHeightLC.constant = imgH;
    
    [self.coverImgView sd_setImageWithURL:[NSURL URLWithString:model.cover_image_url] placeholderImage:IMAGE_NAMED(@"cover_loading")];
    if (model.unlock_price > 0 && !model.unlocked && model.user.userid != userManager.curUserInfo.userid) {
        self.unlockPriceView.hidden = NO;
        self.unlockPriceLabel.text = [NSString stringWithFormat:@"%d金币",model.unlock_price];
    }else{
        self.unlockPriceView.hidden = YES;
    }
    self.groupLabel.text = model.group.title;
}

- (IBAction)playBtnClick:(UIButton *)sender {
    if (self.model.unlock_price > 0 && !self.model.unlocked && self.model.user.userid != userManager.curUserInfo.userid) {
        [self unlockClick];
    }else{
        if ([self.delegate respondsToSelector:@selector(playBtnClick:)]) {
            [self.delegate playBtnClick:self];
        }
    }
}

- (IBAction)likeBtnClick:(UIButton *)sender {
    [super likeBtnClick:sender];
}

- (IBAction)commentBtnClick:(UIButton *)sender {
    [super commentBtnClick:sender];
}

- (IBAction)moreFuncClick:(UIButton *)sender {
    [super moreFuncClick:sender];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
