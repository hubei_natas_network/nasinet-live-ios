//
//  MomentRecTableViewCell.m
//  NASI
//
//  Created by yun11 on 2020/9/22.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MomentRecTableViewCell.h"

#import "MomentModel.h"

@interface MomentRecTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *thumbImgView;
@property (weak, nonatomic) IBOutlet UILabel *groupLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (weak, nonatomic) IBOutlet UIButton *commentBtn;


@end

@implementation MomentRecTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UITapGestureRecognizer *groupTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(groupTap)];
    [self.groupLabel addGestureRecognizer:groupTap];
}

- (void)setModel:(MomentModel *)model{
    [super setModel:model];
    self.titleLabel.text = model.title;
    [self.thumbImgView sd_setImageWithURL:[NSURL URLWithString:model.cover_image_url] placeholderImage:IMAGE_NAMED(@"cover_loading")];
    self.likeBtn.selected = model.liked;
    [self.likeBtn setTitle:[NSString stringWithFormat:@"%d",model.like_count] forState:UIControlStateNormal];
    [self.commentBtn setTitle:[NSString stringWithFormat:@"%d",model.comment_count] forState:UIControlStateNormal];
    self.groupLabel.text = model.group.title;
}

- (IBAction)likeBtnClick:(UIButton *)sender {
    [super likeBtnClick:sender];
}

- (IBAction)commentBtnClick:(UIButton *)sender {
    [super commentBtnClick:sender];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
