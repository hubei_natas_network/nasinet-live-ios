//
//  MomentSingleImageCell.h
//  Meet1V1
//
//  Created by yun on 2020/1/16.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MomentBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MomentSingleImageCell : MomentBaseTableViewCell

@end

NS_ASSUME_NONNULL_END
