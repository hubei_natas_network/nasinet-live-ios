//
//  MomentImagesCell.h
//  Meet1V1
//
//  Created by yun on 2020/1/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "MomentBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MomentImagesCell : MomentBaseTableViewCell

@end

NS_ASSUME_NONNULL_END
