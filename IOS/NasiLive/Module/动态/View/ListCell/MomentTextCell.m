//
//  MomentTextCell.m
//  Meet1V1
//
//  Created by yun on 2020/1/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MomentTextCell.h"

#import "MomentModel.h"

@interface MomentTextCell ()

@property (weak, nonatomic) IBOutlet UIImageView *userIconImgView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *ageGenderBtn;
@property (weak, nonatomic) IBOutlet UIImageView *levelImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeCountBtn;
@property (weak, nonatomic) IBOutlet UIButton *commentCountBtn;
@property (weak, nonatomic) IBOutlet UIButton *moreFuncBtn;
@property (weak, nonatomic) IBOutlet UILabel *groupLabel;

@end

@implementation MomentTextCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userIconClick)];
    [self.userIconImgView addGestureRecognizer:tap];
    UITapGestureRecognizer *groupTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(groupTap)];
    [self.groupLabel addGestureRecognizer:groupTap];
}

- (void)setModel:(MomentModel *)model{
    [super setModel:model];
    [self.userIconImgView sd_setImageWithURL:[NSURL URLWithString:model.user.avatar] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    self.userNameLabel.text = model.user.nick_name;
    [self.ageGenderBtn setBackgroundColor:model.user.profile.gender?[UIColor colorWithHexString:@"6FCAFF"]:[UIColor colorWithHexString:@"FF9CE4"]];
    self.ageGenderBtn.selected = model.user.profile.gender;
    [self.ageGenderBtn setTitle:[NSString stringWithFormat:@"%d",model.user.profile.age] forState:UIControlStateNormal];
    [self.levelImgView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"ic_user_level_%d",model.user.user_level]]];
    
    self.titleLabel.text = model.title;
    self.contentLabel.text = model.content;
    
    self.timeLabel.text = [CommonManager formateBeautyDate:model.create_time];
    self.likeCountBtn.selected = model.liked;
    [self.likeCountBtn setTitle:[NSString stringWithFormat:@"%d",model.like_count] forState:UIControlStateNormal];
    [self.commentCountBtn setTitle:[NSString stringWithFormat:@"%d",model.comment_count] forState:UIControlStateNormal];
    
    if (model.user.userid == userManager.curUserInfo.userid) {
        self.moreFuncBtn.hidden = YES;
    }else{
        self.moreFuncBtn.hidden = NO;
    }
    self.groupLabel.text = model.group.title;
}

- (IBAction)likeBtnClick:(UIButton *)sender {
    [super likeBtnClick:sender];
}

- (IBAction)commentBtnClick:(UIButton *)sender {
    [super commentBtnClick:sender];
}

- (IBAction)moreFuncClick:(UIButton *)sender {
    [super moreFuncClick:sender];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
