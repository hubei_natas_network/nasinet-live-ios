//
//  MomentInfoVideoCell.m
//  Meet1V1
//
//  Created by yun on 2020/1/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MomentInfoVideoCell.h"
#import "SJPlayerSuperview.h"

#import "MomentModel.h"

@interface MomentInfoVideoCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UIImageView *coverImgView;
@property (weak, nonatomic) IBOutlet SJPlayerSuperview *playerView;

@property (weak, nonatomic) IBOutlet UIView *unlockPriceView;
@property (weak, nonatomic) IBOutlet UILabel *unlockPriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *groupLabel;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *skViewTopLC;

@end

@implementation MomentInfoVideoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UITapGestureRecognizer *groupTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(groupTap)];
    [self.groupLabel addGestureRecognizer:groupTap];
}

- (void)setModel:(MomentModel *)model{
    [super setModel:model];
    
    if (model.title.length > 0) {
        self.skViewTopLC.constant = 14.f;
        NSAttributedString *titleAttr = [CommonManager getAttributedStringWithString:model.title lineSpace:5];
        self.titleLabel.attributedText = titleAttr;
    }else{
        NSAttributedString *titleAttr = [CommonManager getAttributedStringWithString:@"" lineSpace:5];
        self.titleLabel.attributedText = titleAttr;
        self.skViewTopLC.constant = 0.f;
    }
    
    [self.coverImgView sd_setImageWithURL:[NSURL URLWithString:model.cover_image_url] placeholderImage:IMAGE_NAMED(@"cover_loading")];
    if (model.unlock_price > 0 && !model.unlocked && model.user.userid != userManager.curUserInfo.userid) {
        self.unlockPriceView.hidden = NO;
        self.unlockPriceLabel.text = [NSString stringWithFormat:@"%d金币",model.unlock_price];
    }else{
        self.unlockPriceView.hidden = YES;
    }
    self.groupLabel.text = model.group.title;
}

- (IBAction)playBtnClick:(UIButton *)sender {
    if (self.model.unlock_price > 0 && !self.model.unlocked && self.model.user.userid != userManager.curUserInfo.userid) {
        [self unlockClick];
    }else{
        if ([self.delegate respondsToSelector:@selector(playBtnClick:)]) {
            [self.delegate playBtnClick:self];
        }
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
