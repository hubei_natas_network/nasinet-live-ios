//
//  SVCommentCell.h
//  Meet1V1
//
//  Created by yun on 2020/1/13.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

@class MomentCommentModel;

NS_ASSUME_NONNULL_BEGIN

@protocol MomentCommentCellDelegate <NSObject>

@optional
- (void)commentLikeClick:(MomentCommentModel *)model;
- (void)replyClick:(MomentCommentModel *)model;

@end

@interface MomentCommentCell : BaseTableViewCell

@property (strong, nonatomic) MomentCommentModel *model;

@property (weak, nonatomic) id<MomentCommentCellDelegate> delegate;

@property (assign, nonatomic) BOOL isShowReplyCount;

@end

NS_ASSUME_NONNULL_END
