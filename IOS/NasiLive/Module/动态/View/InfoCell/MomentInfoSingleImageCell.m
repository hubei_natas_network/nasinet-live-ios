//
//  MomentInfoSingleImageCell.m
//  Meet1V1
//
//  Created by yun on 2020/1/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MomentInfoSingleImageCell.h"

#import <SDWebImage.h>

#import "MomentModel.h"

@interface MomentInfoSingleImageCell()<UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *singleImgView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UILabel *groupLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewHeightLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *skViewTopLC;

@end

@implementation MomentInfoSingleImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UITapGestureRecognizer *tapImage = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imgClick:)];
    [self.singleImgView addGestureRecognizer:tapImage];
    UITapGestureRecognizer *groupTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(groupTap)];
    [self.groupLabel addGestureRecognizer:groupTap];
    
}

- (void)setModel:(MomentModel *)model{
    [super setModel:model];
    //重置图片
    [self.singleImgView removeAllSubviews];
    
    if (model.title.length > 0) {
        self.skViewTopLC.constant = 14.f;
        NSAttributedString *titleAttr = [CommonManager getAttributedStringWithString:model.title lineSpace:5];
        self.titleLabel.attributedText = titleAttr;
    }else{
        NSAttributedString *titleAttr = [CommonManager getAttributedStringWithString:@"" lineSpace:5];
        self.titleLabel.attributedText = titleAttr;
        self.skViewTopLC.constant = 0.f;
    }
    
    //单图模式
    NSString *urlStr = (model.unlock_price > 0 && !model.unlocked && model.user.userid != userManager.curUserInfo.userid)?model.blur_image_urls[0]:model.image_urls[0];
    CGFloat imgWidth = (KScreenWidth - 28)*0.66;
    if (model.display_style == MomentDisplayStyleHorizontal) {
        self.imgViewHeightLC.constant = imgWidth / 16 * 9;
    }else if (model.display_style == MomentDisplayStyleVertical) {
        self.imgViewHeightLC.constant = imgWidth / 4 * 5;
    }else{
        self.imgViewHeightLC.constant = imgWidth;
    }
    [self.singleImgView sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:IMAGE_NAMED(@"cover_loading")];
    if (model.unlock_price > 0 && !model.unlocked && model.user.userid != userManager.curUserInfo.userid) {
        [self createUnlockBtnToView:self.singleImgView imageSizeType:1];
        [self createUnlockPriceBtnToView:self.singleImgView price:model.unlock_price];
    }
    self.groupLabel.text = model.group.title;
    
    [self updateConstraints];
    [self layoutIfNeeded];
}

- (void)createUnlockBtnToView:(UIView *)view imageSizeType:(int)type{
    NSString *imgStr = type == 1?@"ic_lock":@"ic_lock_lt";
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:IMAGE_NAMED(imgStr) forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(unlockClick) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.mas_equalTo(@0);
    }];
}

- (void)createUnlockPriceBtnToView:(UIView *)view price:(int)price{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:IMAGE_NAMED(@"ic_bg_blue") forState:UIControlStateNormal];
    btn.layer.cornerRadius = 9;
    btn.layer.masksToBounds = YES;
    [btn setTitle:[NSString stringWithFormat:@"%d金币",price] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:11];
    [view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(@18);
        make.width.mas_equalTo(@50);
        make.right.offset(-5);
        make.top.offset(5);
    }];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)imgClick:(UIGestureRecognizer *)ges{
    if ([self.delegate respondsToSelector:@selector(imgViewClick:model:index:)]) {
        [self.delegate imgViewClick:self.singleImgView model:self.model index:0];
    }
}

@end
