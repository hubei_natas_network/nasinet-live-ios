//
//  MomentInfoImagesCell.m
//  Meet1V1
//
//  Created by yun on 2020/1/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MomentInfoImagesCell.h"

#import <SDWebImage.h>

#import "MomentModel.h"

@interface MomentInfoImagesCell()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIStackView *skView1;
@property (weak, nonatomic) IBOutlet UIStackView *skView2;
@property (weak, nonatomic) IBOutlet UIStackView *skView3;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *groupLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *skViewTopLC;

@end

@implementation MomentInfoImagesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UITapGestureRecognizer *groupTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(groupTap)];
    [self.groupLabel addGestureRecognizer:groupTap];
    
    NSArray *views1 = [self.skView1 subviews];
    for (UIView *view in views1) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imgView = (UIImageView *)view;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imgClick:)];
            [imgView addGestureRecognizer:tap];
        }
    }
    NSArray *views2 = [self.skView2 subviews];
    for (UIView *view in views2) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imgView = (UIImageView *)view;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imgClick:)];
            [imgView addGestureRecognizer:tap];
        }
    }
    NSArray *views3 = [self.skView3 subviews];
    for (UIView *view in views3) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imgView = (UIImageView *)view;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imgClick:)];
            [imgView addGestureRecognizer:tap];
        }
    }
}

- (void)setModel:(MomentModel *)model{
    [super setModel:model];
    //重置图片
    [self resetImageViews];
    
    if (model.image_urls.count <= 6) {
        self.skView3.hidden = YES;
    }
    if (model.image_urls.count <= 3) {
        self.skView2.hidden = YES;
    }
    
    if (model.title.length > 0) {
        NSAttributedString *titleAttr = [CommonManager getAttributedStringWithString:model.title lineSpace:5];
        self.titleLabel.attributedText = titleAttr;
    }else{
        NSAttributedString *titleAttr = [CommonManager getAttributedStringWithString:@"" lineSpace:5];
        self.titleLabel.attributedText = titleAttr;
        self.skViewTopLC.constant = 0.f;
    }
    self.timeLabel.text = [CommonManager formateBeautyDate:model.create_time];
    
    //多图模式
    NSArray *imgViews1 = [self.skView1 subviews];
    NSArray *imgViews2 = [self.skView2 subviews];
    NSArray *imgViews3 = [self.skView3 subviews];
    if (model.image_urls.count == 4) {
        NSArray *imgViewArr = @[(UIImageView *)imgViews1[0],(UIImageView *)imgViews1[1],(UIImageView *)imgViews2[0],(UIImageView *)imgViews2[1]];
        for (int i = 0; i < 4; i++) {
            UIImageView *imgView = imgViewArr[i];
            NSString *urlStr = (model.unlock_price > 0 && !model.unlocked && model.user.userid != userManager.curUserInfo.userid)?model.blur_image_urls[i]:model.image_urls[i];
            [imgView sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:IMAGE_NAMED(@"cover_loading")];
            if (model.unlock_price > 0 && !model.unlocked && model.user.userid != userManager.curUserInfo.userid) {
                [self createUnlockBtnToView:imgView imageSizeType:2];
                if (i == 0) {
                    [self createUnlockPriceBtnToView:imgView price:model.unlock_price];
                }
            }
        }
    }else{
        NSMutableArray *imgViewArr = [NSMutableArray arrayWithArray:imgViews1];
        [imgViewArr addObjectsFromArray:imgViews2];
        [imgViewArr addObjectsFromArray:imgViews3];
        for (int i = 0; i < model.image_urls.count; i++) {
            UIImageView *imgView = imgViewArr[i];
            NSString *urlStr = (model.unlock_price > 0 && !model.unlocked && model.user.userid != userManager.curUserInfo.userid)?model.blur_image_urls[i]:model.image_urls[i];
            [imgView sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:IMAGE_NAMED(@"cover_loading")];
            if (model.unlock_price > 0 && !model.unlocked && model.user.userid != userManager.curUserInfo.userid) {
                [self createUnlockBtnToView:imgView imageSizeType:2];
                if (i == 0) {
                    [self createUnlockPriceBtnToView:imgView price:model.unlock_price];
                }
            }
        }
    }
    self.groupLabel.text = model.group.title;
}

- (void)resetImageViews{

    self.skViewTopLC.constant = 14.f;
    
    self.skView1.hidden = NO;
    self.skView2.hidden = NO;
    self.skView3.hidden = NO;

    NSArray *views1 = [self.skView1 subviews];
    for (UIView *view in views1) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imgView = (UIImageView *)view;
            imgView.image = nil;
            [imgView removeAllSubviews];
        }
    }
    NSArray *views2 = [self.skView2 subviews];
    for (UIView *view in views2) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imgView = (UIImageView *)view;
            imgView.image = nil;
            [imgView removeAllSubviews];
        }
    }
    NSArray *views3 = [self.skView3 subviews];
    for (UIView *view in views3) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imgView = (UIImageView *)view;
            imgView.image = nil;
            [imgView removeAllSubviews];
        }
    }
}

- (void)createUnlockBtnToView:(UIView *)view imageSizeType:(int)type{
    NSString *imgStr = type == 1?@"ic_lock":@"ic_lock_lt";
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:IMAGE_NAMED(imgStr) forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(unlockClick) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.mas_equalTo(@0);
    }];
}

- (void)createUnlockPriceBtnToView:(UIView *)view price:(int)price{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:IMAGE_NAMED(@"ic_bg_blue") forState:UIControlStateNormal];
    btn.layer.cornerRadius = 9;
    btn.layer.masksToBounds = YES;
    [btn setTitle:[NSString stringWithFormat:@"%d金币",price] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:11];
    [view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(@18);
        make.width.mas_equalTo(@50);
        make.right.offset(-5);
        make.top.offset(5);
    }];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)likeBtnClick:(UIButton *)sender {
    [super likeBtnClick:sender];
}

- (IBAction)commentBtnClick:(UIButton *)sender {
    [super commentBtnClick:sender];
}

- (void)imgClick:(UIGestureRecognizer *)ges{
    UIImageView *imgView = (UIImageView *)ges.view;
    NSInteger tag = imgView.tag;
    if (self.model.image_urls.count == 4) {
        if (tag == 2) {
            return;
        }else if (tag == 3){
            tag = 2;
        }else if (tag == 4){
            tag = 3;
        }else if (tag == 5){
            return;
        }
    }
    if ([self.delegate respondsToSelector:@selector(imgViewClick:model:index:)]) {
        [self.delegate imgViewClick:imgView model:self.model index:tag];
    }
}

- (IBAction)collectBtnClick:(UIButton *)sender {
    [super collectBtnClick:sender];
}

@end
