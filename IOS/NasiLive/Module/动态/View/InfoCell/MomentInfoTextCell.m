//
//  MomentInfoTextCell.m
//  Meet1V1
//
//  Created by yun on 2020/1/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MomentInfoTextCell.h"

#import "MomentModel.h"

@interface MomentInfoTextCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UILabel *groupLabel;

@end

@implementation MomentInfoTextCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UITapGestureRecognizer *groupTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(groupTap)];
    [self.groupLabel addGestureRecognizer:groupTap];
}

- (void)setModel:(MomentModel *)model{
    [super setModel:model];
    
    self.titleLabel.text = model.title;
    self.contentLabel.text = model.content;
    self.timeLabel.text = [CommonManager formateBeautyDate:model.create_time];

    self.groupLabel.text = model.group.title;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
