//
//  MomentBaseTableViewCell.h
//  Meet1V1
//
//  Created by yun on 2020/1/10.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@class MomentModel;
@class GroupModel;
@class MomentBaseTableViewCell;

@protocol MomentBaseTableViewCellDelegate <NSObject>

@optional
- (void)unlockClick:(MomentBaseTableViewCell *)cell;
- (void)likeClick:(MomentModel *)model;
- (void)commentClick:(MomentModel *)model;
- (void)userIconClick:(MomentModel *)model;
- (void)moreFuncClick:(MomentModel *)model;
- (void)collectBtnClick:(MomentModel *)model;
- (void)playBtnClick:(MomentBaseTableViewCell *)cell;
- (void)imgViewClick:(UIImageView *)imageView model:(MomentModel *)model index:(NSInteger)index;
- (void)groupTagClick:(GroupModel *)model;

@end

@interface MomentBaseTableViewCell : BaseTableViewCell<UIGestureRecognizerDelegate>

@property (strong, nonatomic) MomentModel *model;

@property (weak, nonatomic) id<MomentBaseTableViewCellDelegate> delegate;

- (void)likeBtnClick:(UIButton *)sender;
- (void)commentBtnClick:(UIButton *)sender;
- (void)moreFuncClick:(UIButton *)sender;
- (void)collectBtnClick:(UIButton *)sender;
- (void)unlockClick;
- (void)groupTap;

@end

NS_ASSUME_NONNULL_END
