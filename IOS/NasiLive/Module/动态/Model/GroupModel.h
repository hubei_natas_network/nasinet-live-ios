//
//  GroupModel.h
//  NASI
//
//  Created by yun11 on 2020/9/22.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GroupModel : NSObject

@property (nonatomic,assign) int groupid;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *sub_title;
@property (copy, nonatomic) NSString *img_url;
@property (nonatomic,assign) int follow_count;
@property (nonatomic,assign) int moment_count;
@property (copy, nonatomic) NSString *notice;

@property (assign, nonatomic) BOOL isFollowed;

@property (strong, nonatomic) NSArray *moments;

@end

NS_ASSUME_NONNULL_END
