//
//  MomentModel.m
//  Meet1V1
//
//  Created by yun on 2020/1/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MomentModel.h"

@implementation MomentModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"momentid" : @"id",
             @"display_style": @"single_display_type"
    };
}

// 当 JSON 转为 Model 完成后，该方法会被调用。
// 你可以在这里对数据进行校验，如果校验不通过，可以返回 NO，则该 Model 会被忽略。
// 你也可以在这里做一些自动转换不能完成的工作。
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    if (StrValid(dic[@"title"])) {
        NSString *title = dic[@"title"];
        _title = [title stringByRemovingPercentEncoding];
    }
    if (StrValid(dic[@"image_url"])) {
        NSString *image_urls = dic[@"image_url"];
        _image_urls = [image_urls componentsSeparatedByString:@","];
        if (_image_urls.count > 0) {
            _cover_image_url = _image_urls[0];
        }
    }
    if (StrValid(dic[@"blur_image_url"])) {
        NSString *image_urls = dic[@"blur_image_url"];
        _blur_image_urls = [image_urls componentsSeparatedByString:@","];
    }
    return YES;
}

@end
