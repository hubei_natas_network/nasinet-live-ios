//
//  MomentModel.h
//  Meet1V1
//
//  Created by yun on 2020/1/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GroupModel.h"

NS_ASSUME_NONNULL_BEGIN

@class UserInfoModel;

typedef NS_ENUM(NSUInteger, MomentType) {
    MomentTypeText = 1,
    MomentTypeImages,
    MomentTypeVideo
};

typedef NS_ENUM(NSUInteger, MomentDisplayStyle) {
    MomentDisplayStyleNone,
    MomentDisplayStyleHorizontal,
    MomentDisplayStyleVertical,
    MomentDisplayStyleSquare
};


@interface MomentModel : NSObject

@property (nonatomic,assign) int momentid;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *content;
@property (strong, nonatomic) NSArray *image_urls;
@property (strong, nonatomic) NSArray *blur_image_urls;
@property (copy, nonatomic) NSString *cover_image_url;
@property (copy, nonatomic) NSString *video_url;
@property (nonatomic,assign) MomentType type;
@property (nonatomic,assign) MomentDisplayStyle display_style;
@property (copy, nonatomic) NSString *create_time;
@property (nonatomic,assign) int unlock_price;
@property (nonatomic,assign) BOOL unlocked;
@property (nonatomic,assign) BOOL liked; //是否已点赞
@property (nonatomic,assign) BOOL collected; //是否已收藏

@property (nonatomic,assign) int collect_count;
@property (nonatomic,assign) int watch_count;
@property (nonatomic,assign) int like_count;
@property (nonatomic,assign) int comment_count;

@property (strong, nonatomic) UserInfoModel *user;
@property (strong, nonatomic) GroupModel *group;

@end

NS_ASSUME_NONNULL_END
