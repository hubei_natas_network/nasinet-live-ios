//
//  GroupModel.m
//  NASI
//
//  Created by yun11 on 2020/9/22.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "GroupModel.h"
#import "MomentModel.h"

@implementation GroupModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"groupid" : @"id",
    };
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"moments":[MomentModel class]
    };
}

@end
