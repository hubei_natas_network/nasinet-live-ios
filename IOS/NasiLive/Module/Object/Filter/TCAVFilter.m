// Copyright (c) 2019 Tencent. All rights reserved.

#import "TCAVFilter.h"

TCAVFilterIdentifier const TCAVFilterIdentifierNone      = @"";
TCAVFilterIdentifier const TCAVFilterIdentifierNormal    = @"normal";
TCAVFilterIdentifier const TCAVFilterIdentifierYinghong  = @"yinghong";
TCAVFilterIdentifier const TCAVFilterIdentifierYunshang  = @"yunshang";
TCAVFilterIdentifier const TCAVFilterIdentifierChunzhen  = @"chunzhen";
TCAVFilterIdentifier const TCAVFilterIdentifierBailan    = @"bailan";
TCAVFilterIdentifier const TCAVFilterIdentifierYuanqi    = @"yuanqi";
TCAVFilterIdentifier const TCAVFilterIdentifierChaotuo   = @"chaotuo";
TCAVFilterIdentifier const TCAVFilterIdentifierXiangfen  = @"xiangfen";
TCAVFilterIdentifier const TCAVFilterIdentifierWhite     = @"white";
TCAVFilterIdentifier const TCAVFilterIdentifierLangman   = @"langman";
TCAVFilterIdentifier const TCAVFilterIdentifierQingxin   = @"qingxin";
TCAVFilterIdentifier const TCAVFilterIdentifierWeimei    = @"weimei";
TCAVFilterIdentifier const TCAVFilterIdentifierFennen    = @"fennen";
TCAVFilterIdentifier const TCAVFilterIdentifierHuaijiu   = @"huaijiu";
TCAVFilterIdentifier const TCAVFilterIdentifierLandiao   = @"landiao";
TCAVFilterIdentifier const TCAVFilterIdentifierQingliang = @"qingliang";
TCAVFilterIdentifier const TCAVFilterIdentifierRixi      = @"rixi";

@implementation TCAVFilter

- (instancetype)initWithIdentifier:(TCAVFilterIdentifier)identifier
                   lookupImagePath:(NSString *)lookupImagePath
{
    if (self = [super init]) {
        _identifier = identifier;
        _lookupImagePath = lookupImagePath;
    }
    return self;
}
@end

@implementation TCAVFilterManager
{
    NSDictionary<TCAVFilterIdentifier, TCAVFilter*> *_filterDictionary;
}
+ (instancetype)defaultManager
{
    static TCAVFilterManager *defaultManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        defaultManager = [[TCAVFilterManager alloc] init];
    });
    return defaultManager;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSBundle *bundle = [NSBundle mainBundle];
        NSString *path = [bundle pathForResource:@"FilterResource" ofType:@"bundle"];
        NSFileManager *manager = [[NSFileManager alloc] init];
        if ([manager fileExistsAtPath:path]) {
            NSArray<TCAVFilterIdentifier> *availableFilters = @[
                TCAVFilterIdentifierNormal,
                TCAVFilterIdentifierYinghong,
                TCAVFilterIdentifierYunshang,
                TCAVFilterIdentifierChunzhen,
                TCAVFilterIdentifierBailan,
                TCAVFilterIdentifierYuanqi,
                TCAVFilterIdentifierChaotuo,
                TCAVFilterIdentifierXiangfen,
                TCAVFilterIdentifierWhite,
                TCAVFilterIdentifierLangman,
                TCAVFilterIdentifierQingxin,
                TCAVFilterIdentifierWeimei,
                TCAVFilterIdentifierFennen,
                TCAVFilterIdentifierHuaijiu,
                TCAVFilterIdentifierLandiao,
                TCAVFilterIdentifierQingliang,
                TCAVFilterIdentifierRixi];
            NSMutableArray<TCAVFilter *> *filters = [[NSMutableArray alloc] initWithCapacity:availableFilters.count];
            NSMutableDictionary<TCAVFilterIdentifier, TCAVFilter*> *filterMap = [[NSMutableDictionary alloc] initWithCapacity:availableFilters.count];
            for (TCAVFilterIdentifier identifier in availableFilters) {
                NSString * itemPath = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", identifier]];
                if ([manager fileExistsAtPath:path]) {
                    TCAVFilter *filter = [[TCAVFilter alloc] initWithIdentifier:identifier lookupImagePath:itemPath];
                    [filters addObject:filter];
                    filterMap[identifier] = filter;
                }
            }
            _allFilters = filters;

        }
    }
    return self;
}

- (TCAVFilter *)filterWithIdentifier:(TCAVFilterIdentifier)identifier;
{
    return _filterDictionary[identifier];
}
@end
