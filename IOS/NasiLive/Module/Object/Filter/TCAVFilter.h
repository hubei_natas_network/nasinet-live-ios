// Copyright (c) 2019 Tencent. All rights reserved.

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NSString * TCAVFilterIdentifier NS_STRING_ENUM;

extern TCAVFilterIdentifier const TCAVFilterIdentifierNone;
extern TCAVFilterIdentifier const TCAVFilterIdentifierNormal;
extern TCAVFilterIdentifier const TCAVFilterIdentifierYinghong;
extern TCAVFilterIdentifier const TCAVFilterIdentifierYunshang;
extern TCAVFilterIdentifier const TCAVFilterIdentifierChunzhen;
extern TCAVFilterIdentifier const TCAVFilterIdentifierBailan;
extern TCAVFilterIdentifier const TCAVFilterIdentifierYuanqi;
extern TCAVFilterIdentifier const TCAVFilterIdentifierChaotuo;
extern TCAVFilterIdentifier const TCAVFilterIdentifierXiangfen;
extern TCAVFilterIdentifier const TCAVFilterIdentifierWhite;
extern TCAVFilterIdentifier const TCAVFilterIdentifierLangman;
extern TCAVFilterIdentifier const TCAVFilterIdentifierQingxin;
extern TCAVFilterIdentifier const TCAVFilterIdentifierWeimei;
extern TCAVFilterIdentifier const TCAVFilterIdentifierFennen;
extern TCAVFilterIdentifier const TCAVFilterIdentifierHuaijiu;
extern TCAVFilterIdentifier const TCAVFilterIdentifierLandiao;
extern TCAVFilterIdentifier const TCAVFilterIdentifierQingliang;
extern TCAVFilterIdentifier const TCAVFilterIdentifierRixi;

@interface TCAVFilter : NSObject
@property (readonly, nonatomic) TCAVFilterIdentifier identifier;
@property (readonly, nonatomic) NSString *lookupImagePath;
@end

@interface TCAVFilterManager : NSObject
+ (instancetype)defaultManager;
@property (readonly, nonatomic) NSArray<TCAVFilter *> *allFilters;
- (TCAVFilter *)filterWithIdentifier:(TCAVFilterIdentifier)identifier;
@end

NS_ASSUME_NONNULL_END
