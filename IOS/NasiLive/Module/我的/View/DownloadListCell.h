//
//  DownloadListCell.h
//  Avideo
//
//  Created by yun7_mac on 2019/3/12.
//  Copyright © 2019 XKX. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN


@class HJDownloadModel;
typedef void(^CustomSelectBlock)( HJDownloadModel * model);
typedef void(^DownSucessBlock)(void);

@interface DownloadListCell : BaseTableViewCell

@property (nonatomic, strong) HJDownloadModel *downloadModel;
@property (nonatomic, copy) CustomSelectBlock customSelectedBlock;//点击通知
@property (nonatomic, copy) DownSucessBlock   sucessBlock;//下载完成通知

+ (CGFloat)cellHeight;

@end

NS_ASSUME_NONNULL_END
