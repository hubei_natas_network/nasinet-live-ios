//
//  WithdrawTableViewCell.h
//  NasiLive
//
//  Created by yun11 on 2020/4/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

#import "WithdrawModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface WithdrawTableViewCell : BaseTableViewCell

@property (strong, nonatomic) WithdrawModel *model;

@end

NS_ASSUME_NONNULL_END
