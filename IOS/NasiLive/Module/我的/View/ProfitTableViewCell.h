//
//  ProfitTableViewCell.h
//  NasiLive
//
//  Created by yun11 on 2020/4/1.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

#import "ProfitModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProfitTableViewCell : BaseTableViewCell

@property (strong, nonatomic) ProfitModel *model;

@end

NS_ASSUME_NONNULL_END
