//
//  RechargeCollectionViewCell.h
//  NasiLive
//
//  Created by yun11 on 2020/4/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RechargeCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) NSDictionary *dict;

@property (assign, nonatomic) BOOL cellSelected;

@end

NS_ASSUME_NONNULL_END
