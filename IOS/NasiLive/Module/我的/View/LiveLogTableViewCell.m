//
//  LiveLogTableViewCell.m
//  NasiLive
//
//  Created by yun11 on 2020/4/8.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "LiveLogTableViewCell.h"

@interface LiveLogTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *profitLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;

@end

@implementation LiveLogTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(LiveModel *)model{
    _model = model;
    self.timeLabel.text = model.start_time;
    self.titleLabel.text = model.title;
    self.profitLabel.text = [NSString stringWithFormat:@"%ld",(long)model.profit];
    self.durationLabel.text = [CommonManager formateSecond:model.end_stamp - model.start_stamp withHour:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
