//
//  MineHomeHeaderView.m
//  Meet1V1
//
//  Created by yun on 2020/1/20.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MineHomeHeaderView.h"
#import "VipCenterViewController.h"
#import "InviteViewController.h"
#import "MomentWatchLogViewController.h"
#import "RechargeViewController.h"
#import "MessageHomeViewController.h"
#import "EditMineInfoViewController.h"
#import "AttentViewController.h"
#import "WelfareCenterController.h"

#import <ImSDK.h>

@interface MineHomeHeaderView()

@property (weak, nonatomic) IBOutlet UIView *infoContainerView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *watchCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *attentCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *fansCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *visitorCountLabel;
@property (weak, nonatomic) IBOutlet UIView *attentCountView;
@property (weak, nonatomic) IBOutlet UIView *fansCountView;
@property (weak, nonatomic) IBOutlet UIView *visitorCountView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImgView;
@property (weak, nonatomic) IBOutlet UILabel *vipDateLabel;

@property (weak, nonatomic) IBOutlet UIButton *msgBtn;
@property (weak, nonatomic) IBOutlet UILabel *msgCountLabel;

@property (weak, nonatomic) IBOutlet UIView *loginBtnView;

@end

@implementation MineHomeHeaderView

- (instancetype)init{
    self = [[NSBundle mainBundle]loadNibNamed:@"MineHomeHeaderView" owner:self options:nil].firstObject;
    [self refresh];
    
    UITapGestureRecognizer *tapAttent = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAttent)];
    [self.attentCountView addGestureRecognizer:tapAttent];
    UITapGestureRecognizer *tapFans = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapFans)];
    [self.fansCountView addGestureRecognizer:tapFans];
    UITapGestureRecognizer *tapHistory = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapHistory)];
    [self.visitorCountView addGestureRecognizer:tapHistory];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateMessageNum:) name:KNotificationUpdateMessageNum object:nil];
    
    return self;
}

- (void)refresh{
    if (![userManager isLogined]) {
        self.infoContainerView.hidden = YES;
        self.loginBtnView.hidden = NO;
        self.attentCountLabel.text = @"0";
        self.fansCountLabel.text = @"0";
        self.visitorCountLabel.text = @"0";
        return;
    }
    self.infoContainerView.hidden = NO;
    self.loginBtnView.hidden = YES;
    
    UserInfoModel *model = userManager.curUserInfo;
    
    self.nickNameLabel.text = model.nick_name;
    NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] initWithString:self.nickNameLabel.text attributes:nil];
    NSAttributedString *speaceString = [[NSAttributedString alloc]initWithString:@" "];
    
    //vip标识
    NSTextAttachment *vipAttchment = [[NSTextAttachment alloc]init];
    vipAttchment.bounds = CGRectMake(0, -4, 16, 19);//设置frame
    vipAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"vip_level_%d",[model getVipLevel]]];//设置图片
    NSAttributedString *vipString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(vipAttchment)];
    
    //主播等级标识
    NSTextAttachment *anchorLevelAttchment = [[NSTextAttachment alloc]init];
    anchorLevelAttchment.bounds = CGRectMake(0, -3, 34, 15);//设置frame
    anchorLevelAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_anchor_level_%d",model.anchor_level]];
    NSAttributedString *anchorLevelString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(anchorLevelAttchment)];
    
    //等级标识
    NSTextAttachment *levelAttchment = [[NSTextAttachment alloc]init];
    levelAttchment.bounds = CGRectMake(0, -3, 34, 15);//设置frame
    levelAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_user_level_%d",model.user_level]];
    NSAttributedString *levelString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(levelAttchment)];
    
    //插入VIP图标
    if ([model checkVip]) {
        [titleStr appendAttributedString:speaceString];//插入到第几个下标
        [titleStr appendAttributedString:vipString];//插入到第几个下标
    }
    
    if (model.is_anchor && model.anchor_level > 0) {
        //插入主播等级图标
        [titleStr appendAttributedString:speaceString];//插入到第几个下标
        [titleStr appendAttributedString:anchorLevelString];//插入到第几个下标
    }
    
    //插入等级图标
    [titleStr appendAttributedString:speaceString];//插入到第几个下标
    [titleStr appendAttributedString:levelString];//插入到第几个下标
    
    [self.nickNameLabel setAttributedText:titleStr];
    
    [self.avatarImgView sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    self.attentCountLabel.text = [NSString stringWithFormat:@"%d",model.attent_count];
    self.fansCountLabel.text = [NSString stringWithFormat:@"%d",model.fans_count];
    self.visitorCountLabel.text = [NSString stringWithFormat:@"%d",model.click_count];
    
    if ([model checkVip]){
        self.vipDateLabel.text = [NSString stringWithFormat:@"%@ 到期", model.vip_date];
        self.watchCountLabel.text = @"无限观影";
    }else{
        self.vipDateLabel.text = @"立即开通";
        self.watchCountLabel.text = model.left_watch_count;
    }
}

- (IBAction)editBtnClick:(UIButton *)sender {
    if (!userManager.isLogined) {
        [kAppDelegate showLoginViewController];
        return;
    }
    [self.viewController.navigationController pushViewController:[[EditMineInfoViewController alloc]init] animated:YES];
}

- (IBAction)chargeBtnClick:(UIButton *)sender {
    if (!userManager.isLogined) {
        [kAppDelegate showLoginViewController];
        return;
    }
    [self.viewController.navigationController pushViewController:[[RechargeViewController alloc]init] animated:YES];
}

- (IBAction)msgBtnClick:(UIButton *)sender {
    if (!userManager.isLogined) {
        [kAppDelegate showLoginViewController];
        return;
    }
    [self.viewController.navigationController pushViewController:[[MessageHomeViewController alloc]init] animated:YES];
}

- (IBAction)loginBtnClick:(UIButton *)sender {
    [kAppDelegate showLoginViewController];
}

- (IBAction)vipBtnClick:(UIButton *)sender {
    [self.viewController.navigationController pushViewController:[[VipCenterViewController alloc]init] animated:YES];
}

- (IBAction)inviteBtnClick:(UIButton *)sender {
    [self.viewController.navigationController pushViewController:[[InviteViewController alloc]init] animated:YES];
}

- (IBAction)boonBtnClick:(UIButton *)sender {
    [self.viewController.navigationController pushViewController:[[WelfareCenterController alloc]init] animated:YES];
}

- (void)tapAttent{
    if (!userManager.isLogined) {
        [kAppDelegate showLoginViewController];
        return;
    }
    AttentViewController *vc = [[AttentViewController alloc]init];
    vc.isAttent = YES;
    [self.viewController.navigationController pushViewController:vc animated:YES];
}

- (void)tapFans{
    if (!userManager.isLogined) {
        [kAppDelegate showLoginViewController];
        return;
    }
    AttentViewController *vc = [[AttentViewController alloc]init];
    vc.isAttent = NO;
    [self.viewController.navigationController pushViewController:vc animated:YES];
}

- (void)tapHistory{
    [self.viewController.navigationController pushViewController:[[MomentWatchLogViewController alloc]init] animated:YES];
}

- (void)updateMessageNum:(NSNotification *)notification{
    int num = 0;
    NSArray *conversations = [[TIMManager sharedInstance] getConversationList];
    for (TIMConversation *conv in conversations) {
        if (conv.getType == TIM_C2C && ![conv.getReceiver isEqualToString:[configManager appConfig].txim_broadcast]) {
            num += [conv getUnReadMessageNum];
        }
    }
    if (num > 0) {
        self.msgCountLabel.hidden = NO;
        self.msgCountLabel.text = [NSString stringWithFormat:@"%d",num];
    }else{
        self.msgCountLabel.hidden = YES;
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
