//
//  DownloadListCell.m
//  Avideo
//
//  Created by yun7_mac on 2019/3/12.
//  Copyright © 2019 XKX. All rights reserved.
//

#import "DownloadListCell.h"

#import "HJDownloadModel.h"
#import "HJDownloadManager.h"

@interface DownloadListCell()

/** <#describe#> */
@property (nonatomic, strong) UILabel * titleLabel;

/** <#describe#> */
@property (nonatomic, strong) UIImageView * imgView;

@property (strong, nonatomic) UIImageView * statusImgView;

/** <#describe#> */
@property (nonatomic, strong) UIProgressView * progressView;

/** <#describe#> */
@property (nonatomic, strong) UILabel *fileSizeLabel;

/** <#describe#> */
@property (nonatomic, strong) UILabel *speedLabel;

@property (nonatomic, strong) UIButton *btnSelect;


@end

@implementation DownloadListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self createUI];
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;

//        self.selectedBackgroundView = [[UIView alloc]init];
       
    }
    return self;
}

- (void)selectAction:(UIButton*)btn{
    
    
    self.btnSelect.selected = !self.btnSelect.selected ;
    _downloadModel.isSelect = ! _downloadModel.isSelect;
    !_customSelectedBlock ?: _customSelectedBlock( _downloadModel);
}

- (void)createUI{
    UIView *backgroundView = [[UIView alloc] initWithFrame:self.contentView.bounds];
    backgroundView.backgroundColor = [UIColor clearColor];
    self.backgroundView = backgroundView;
    
    self.btnSelect = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnSelect.frame = CGRectMake( 15, 28, 40, 40);
   
    [backgroundView addSubview:self.btnSelect];

    [self.btnSelect setImage:[UIImage imageNamed:@"unchoose"] forState:UIControlStateNormal];
    [self.btnSelect setImage:[UIImage imageNamed:@"choose"] forState:UIControlStateSelected];
    [self.btnSelect addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.imgView];
    [self.contentView addSubview:self.statusImgView];
    [self.contentView addSubview:self.progressView];
    [self.contentView addSubview:self.fileSizeLabel];
    [self.contentView addSubview:self.speedLabel];
    
    [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(10);
        make.left.offset(15);
        make.height.mas_equalTo(@80);
        make.width.mas_equalTo(80.0f*14/9);
    }];
    
    [self.statusImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(@40);
        make.width.mas_equalTo(@40);
        make.center.equalTo(self.imgView);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.imgView.mas_right).offset(10);
        make.top.equalTo(self.imgView);
        make.right.offset(-15);
    }];
    
    [self.progressView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.bottom.mas_equalTo(self.imgView.mas_bottom).offset(-5);
        make.centerY.equalTo(self.imgView.mas_centerY);
        make.height.mas_equalTo(2);
        make.left.right.equalTo(self.titleLabel);
    }];
    
    [self.speedLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel);
        make.width.equalTo(self.titleLabel).multipliedBy(0.5);
//        make.bottom.mas_equalTo(self.progressView.mas_top).offset(-5);
        make.bottom.mas_equalTo(self.imgView.mas_bottom).offset(-5);

        make.height.mas_equalTo(15);
    }];
    
    [self.fileSizeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.titleLabel);
        make.width.equalTo(self.titleLabel).multipliedBy(0.6);
        make.bottom.mas_equalTo(self.imgView.mas_bottom).offset(-5);;
        make.height.mas_equalTo(15);
    }];
    
}

- (void)setDownloadModel:(HJDownloadModel *)downloadModel{
    _downloadModel = downloadModel;
    self.btnSelect.selected = downloadModel.isSelect ;
    self.titleLabel.text = downloadModel.fileName;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:downloadModel.imgUrl] placeholderImage:IMAGE_NAMED(@"bg_placeholder")];
    
    if (downloadModel.status == kHJDownloadStatus_Completed) {
        self.statusImgView.image = IMAGE_NAMED(@"ic_play");
    }else if (downloadModel.status == kHJDownloadStatus_Failed){
        self.statusImgView.image = IMAGE_NAMED(@"ic_refresh");
    }else if (downloadModel.status == kHJDownloadStatus_Suspended){
        self.statusImgView.image = IMAGE_NAMED(@"ic_pause");
    }
    
    CGFloat totalSize = downloadModel.fileTotalSize/1024/1024.f;
    self.fileSizeLabel.text = [NSString stringWithFormat:@"%.2fM",totalSize];
    
    __weak typeof(self) weakSelf = self;
    _downloadModel.statusChanged = ^(HJDownloadModel *downloadModel) {
        if (downloadModel.status == kHJDownloadStatus_Completed) {
            if (weakSelf.sucessBlock){
                weakSelf.sucessBlock();
            }
        }
        [weakSelf refreshUIWithDownloadModel:downloadModel];
    };
    
    _downloadModel.progressChanged = ^(HJDownloadModel *downloadModel) {

        [weakSelf refreshUIWithDownloadModel:downloadModel];
    };
    
    [self refreshUIWithDownloadModel:self.downloadModel];
}

- (void)refreshUIWithDownloadModel:(HJDownloadModel *)downloadModel{
    
    HJDownloadStatus downloadStatus = downloadModel.status;
    CGFloat progress = downloadModel.progress;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.progressView setProgress:progress];
        CGFloat totalSize = downloadModel.fileTotalSize/1024/1024.f;
        self.fileSizeLabel.text = [NSString stringWithFormat:@"%.1fM/%.1fM",totalSize *progress,totalSize];
        
        if(downloadStatus == kHJDownloadStatus_None){
            self.speedLabel.text = @"";
            self.statusImgView.image = nil;
        }else if(downloadStatus == kHJDownloadStatus_Running){
            if (ValidStr(downloadModel.speed)) {
                self.speedLabel.text = downloadModel.speed;
            }else{
                self.speedLabel.text = @"正在下载";
            }
            self.statusImgView.image = nil;
        }else if(downloadStatus == kHJDownloadStatus_Suspended){
            self.speedLabel.text = @"已暂停";
            self.statusImgView.image = IMAGE_NAMED(@"ic_pause");
        }else if(downloadStatus == kHJDownloadStatus_Completed){
            self.speedLabel.text = @"已完成";
            self.statusImgView.image = IMAGE_NAMED(@"ic_play");
            self.progressView.hidden = YES ;
            self.fileSizeLabel.text = [NSString stringWithFormat:@"%.1fM",totalSize];
            
        }else if(downloadStatus == kHJDownloadStatus_Failed){
            self.speedLabel.text = @"下载失败";
            self.speedLabel.textColor = [UIColor colorWithHexString:@"#FF3346"];
            self.statusImgView.image = IMAGE_NAMED(@"ic_refresh");
        }else if(downloadStatus == kHJDownloadStatus_Waiting){
            self.speedLabel.text = @"等待下载";
            self.statusImgView.image = nil;
        }else{
            self.speedLabel.text = @"";
            self.statusImgView.image = nil;
        }
    });
}

//-(void)layoutSubviews
//{
//    for (UIControl *control in self.subviews){
//        if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
//            for (UIView *v in control.subviews)
//            {
//                if ([v isKindOfClass: [UIImageView class]]) {
//                    UIImageView *img=(UIImageView *)v;
//                    if (self.selected) {
//                        img.image=[UIImage imageNamed:@"choose"];
//                    }else
//                    {
//                        img.image=[UIImage imageNamed:@"unchoose"];
//                    }
//                }
//            }
//        }
//    }
//    [super layoutSubviews];
//}
//
//
////适配第一次图片为空的情况
//- (void)setEditing:(BOOL)editing animated:(BOOL)animated
//{
//    [super setEditing:editing animated:animated];
//    for (UIControl *control in self.subviews){
//        if ([control isMemberOfClass:NSClassFromString(@"UITableViewCellEditControl")]){
//            for (UIView *v in control.subviews)
//            {
//                if ([v isKindOfClass: [UIImageView class]]) {
//                    UIImageView *img=(UIImageView *)v;
//                    if (!self.selected) {
//                        img.image=[UIImage imageNamed:@"unchoose"];
//                    }
//                }
//            }
//        }
//    }
//    
//}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:14.f];
        _titleLabel.textColor = CFontColor;
        _titleLabel.numberOfLines = 3;
    }
    return _titleLabel;
}

- (UIImageView *)imgView{
    if (!_imgView) {
        _imgView = [[UIImageView alloc] init];
        _imgView.contentMode = UIViewContentModeScaleAspectFill;
        _imgView.layer.masksToBounds = YES;
    }
    return _imgView;
}

- (UIImageView *)statusImgView{
    if (!_statusImgView) {
        _statusImgView = [[UIImageView alloc] init];
        _statusImgView.contentMode = UIViewContentModeScaleAspectFill;
        _statusImgView.layer.masksToBounds = YES;
    }
    return _statusImgView;
}

- (UIProgressView *)progressView{
    if (!_progressView) {
        _progressView = [[UIProgressView alloc] init];
        _progressView.trackTintColor = [UIColor lightGrayColor];
        _progressView.progressTintColor = MAIN_COLOR;
    }
    return _progressView;
}

- (UILabel *)fileSizeLabel{
    if (!_fileSizeLabel) {
        _fileSizeLabel = [[UILabel alloc] init];
        _fileSizeLabel.font = [UIFont systemFontOfSize:12.f];
        _fileSizeLabel.textColor = CFontColor2;
        _fileSizeLabel.textAlignment = NSTextAlignmentRight;
    }
    return _fileSizeLabel;
}

- (UILabel *)speedLabel{
    if (!_speedLabel) {
        _speedLabel = [[UILabel alloc] init];
        _speedLabel.font = [UIFont systemFontOfSize:12.f];
        _speedLabel.textColor = MAIN_COLOR;
    }
    return _speedLabel;
}

+ (CGFloat)cellHeight{
    return 100;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
