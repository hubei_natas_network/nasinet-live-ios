//
//  UserTableViewCell.h
//  NasiLive
//
//  Created by yun11 on 2020/3/27.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

#import "UserInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol UserTableViewCellDelegate <NSObject>

@optional

- (void)optBtnClick:(UIButton *)sender model:(UserInfoModel *)model;

@end

@interface UserTableViewCell : BaseTableViewCell

@property (strong, nonatomic) UserInfoModel *model;
@property (strong, nonatomic) UIImage *optBtnImage;

@property (weak, nonatomic) id<UserTableViewCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
