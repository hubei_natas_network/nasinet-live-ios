//
//  VipFuncCollectionViewCell.m
//  NasiLive
//
//  Created by yun11 on 2020/7/10.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "VipFuncCollectionViewCell.h"

@interface VipFuncCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;

@end

@implementation VipFuncCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setDict:(NSDictionary *)dict{
    _dict = dict;
    
    self.iconImgView.image = [UIImage imageNamed:dict[@"icon"]];
    self.titleLabel.text = dict[@"title"];
    self.subTitleLabel.text = dict[@"subtitle"];
}

- (void)setIsGot:(BOOL)isGot{
    _isGot = isGot;
    if (isGot) {
        self.iconImgView.image = [UIImage imageNamed:self.dict[@"icon_sel"]];
    }else{
        self.iconImgView.image = [UIImage imageNamed:self.dict[@"icon"]];
    }
}

@end
