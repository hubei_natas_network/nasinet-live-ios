//
//  UserTableViewCell.m
//  NasiLive
//
//  Created by yun11 on 2020/3/27.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "UserTableViewCell.h"

#import <UIButton+WebCache.h>

@interface UserTableViewCell()

@property (weak, nonatomic) IBOutlet UIButton *iconBtn;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *signatureLabel;
@property (weak, nonatomic) IBOutlet UIButton *optBtn;


@end

@implementation UserTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(UserInfoModel *)model{
    _model = model;
    [self.iconBtn sd_setImageWithURL:[NSURL URLWithString:model.avatar] forState:UIControlStateNormal placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    self.signatureLabel.text = model.profile.signature;
    
    self.nameLabel.text = model.nick_name;
    NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] initWithString:self.nameLabel.text attributes:nil];
    NSAttributedString *speaceString = [[NSAttributedString alloc]initWithString:@" "];
    
    //vip标识
    NSTextAttachment *vipAttchment = [[NSTextAttachment alloc]init];
    vipAttchment.bounds = CGRectMake(0, -4, 16, 19);//设置frame
    vipAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"vip_level_%d",[model getVipLevel]]];//设置图片
    NSAttributedString *vipString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(vipAttchment)];
    
    //主播等级标识
    NSTextAttachment *anchorLevelAttchment = [[NSTextAttachment alloc]init];
    anchorLevelAttchment.bounds = CGRectMake(0, -3, 34, 15);//设置frame
    anchorLevelAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_anchor_level_%d",model.anchor_level]];
    NSAttributedString *anchorLevelString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(anchorLevelAttchment)];
    
    //等级标识
    NSTextAttachment *levelAttchment = [[NSTextAttachment alloc]init];
    levelAttchment.bounds = CGRectMake(0, -3, 34, 15);//设置frame
    levelAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_user_level_%d",model.user_level]];
    NSAttributedString *levelString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(levelAttchment)];
    
    //插入VIP图标
    if ([model checkVip]) {
        [titleStr appendAttributedString:speaceString];//插入到第几个下标
        [titleStr appendAttributedString:vipString];//插入到第几个下标
    }
    
    if (model.is_anchor && model.anchor_level > 0) {
        //插入主播等级图标
        [titleStr appendAttributedString:speaceString];//插入到第几个下标
        [titleStr appendAttributedString:anchorLevelString];//插入到第几个下标
    }
    
    //插入等级图标
    [titleStr appendAttributedString:speaceString];//插入到第几个下标
    [titleStr appendAttributedString:levelString];//插入到第几个下标
    
    [self.nameLabel setAttributedText:titleStr];
}

- (void)setOptBtnImage:(UIImage *)optBtnImage{
    _optBtnImage = optBtnImage;
    [self.optBtn setImage:optBtnImage forState:UIControlStateNormal];
}

- (IBAction)iconBtnClick:(UIButton *)sender {
}

- (IBAction)optBtnClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(optBtnClick:model:)]) {
        [self.delegate optBtnClick:sender model:self.model];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
