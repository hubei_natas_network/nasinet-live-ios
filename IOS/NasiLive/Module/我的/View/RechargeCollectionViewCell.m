//
//  RechargeCollectionViewCell.m
//  NasiLive
//
//  Created by yun11 on 2020/4/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RechargeCollectionViewCell.h"

@interface RechargeCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *bgImgView;
@property (weak, nonatomic) IBOutlet UILabel *goldCountLabel;
@property (weak, nonatomic) IBOutlet UIView *breakline;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end

@implementation RechargeCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setDict:(NSDictionary *)dict{
    _dict = dict;
    if (configManager.appConfig.switch_iap) {
        self.goldCountLabel.text = [NSString stringWithFormat:@"%d 金币",[dict[@"gold_ios"] intValue] + [dict[@"gold_added"] intValue]];
    }else{
        self.goldCountLabel.text = [NSString stringWithFormat:@"%d 金币",[dict[@"gold"] intValue] + [dict[@"gold_added"] intValue]];
    }
    self.priceLabel.text = [NSString stringWithFormat:@"￥ %.2f",[dict[@"price"] floatValue]];
}

- (void)setCellSelected:(BOOL)cellSelected{
    _cellSelected = cellSelected;
    if (cellSelected) {
        self.bgImgView.hidden = NO;
        self.layer.borderColor = [UIColor clearColor].CGColor;
        self.breakline.hidden = YES;
        self.goldCountLabel.textColor = [UIColor whiteColor];
        self.priceLabel.textColor = [UIColor whiteColor];
    }else{
        self.bgImgView.hidden = YES;
        self.layer.borderColor = CLineColor.CGColor;
        self.breakline.hidden = NO;
        self.goldCountLabel.textColor = [UIColor blackColor];
        self.priceLabel.textColor = [UIColor colorWithHexString:@"9F9F9F"];
    }
}

@end
