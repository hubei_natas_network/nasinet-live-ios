//
//  AnchorTableViewCell.h
//  NasiLive
//
//  Created by yun11 on 2020/3/27.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

#import "UserInfoModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol AnchorTableViewCellDelegate <NSObject>

@optional

- (void)attentBtnClick:(UIButton *)sender model:(UserInfoModel *)model;

@end

@interface AnchorTableViewCell : BaseTableViewCell

@property (strong, nonatomic) UserInfoModel *model;

@property (weak, nonatomic) id<AnchorTableViewCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
