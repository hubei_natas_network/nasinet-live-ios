//
//  WelfareViewCell.h
//  NasiBBS
//
//  Created by kevin on 2021/3/23.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN
@class WelfareModel  ;
@interface WelfareViewCell : BaseTableViewCell
@property (nonatomic , strong) WelfareModel *model ;
@end

NS_ASSUME_NONNULL_END
