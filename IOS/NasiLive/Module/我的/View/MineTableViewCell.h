//
//  MineTableViewCell.h
//  NasiLive
//
//  Created by yun11 on 2020/3/25.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MineTableViewCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *titLabel;
@property (weak, nonatomic) IBOutlet UIImageView *rightImgView;


@end

NS_ASSUME_NONNULL_END
