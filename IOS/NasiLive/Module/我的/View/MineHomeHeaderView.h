//
//  MineHomeHeaderView.h
//  Meet1V1
//
//  Created by yun on 2020/1/20.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol MineHomeHeaderViewDelegate <NSObject>

@optional

@end

@interface MineHomeHeaderView : UIView

@property (weak, nonatomic) id<MineHomeHeaderViewDelegate> delegate;

- (void)refresh;

@end

NS_ASSUME_NONNULL_END
