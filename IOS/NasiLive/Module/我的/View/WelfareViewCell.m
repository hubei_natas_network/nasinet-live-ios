//
//  WelfareViewCell.m
//  NasiBBS
//
//  Created by kevin on 2021/3/23.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "WelfareViewCell.h"
#import "WelfareModel.h"

@interface WelfareViewCell  ()
@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *numLabe;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *starView;

@property (weak, nonatomic) IBOutlet UIView *bgView;

@end

@implementation WelfareViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _bgView.backgroundColor = [UIColor whiteColor];
    _bgView.layer.shadowColor = [UIColor blackColor].CGColor;
    _bgView.layer.shadowOffset = CGSizeMake(0, 0);
    _bgView.layer.shadowOpacity = 0.1;
    _bgView.layer.masksToBounds = NO;
    _bgView.clipsToBounds = NO;
    self.selectionStyle = UITableViewCellSelectionStyleNone;

    
    CGFloat starX = 12 ;
    for (int i = 0; i < 5; i ++) {
        UIImageView *starImage = [[UIImageView alloc]initWithImage:IMAGE_NAMED(@"ic_star")];
        starImage.mj_x = starX *i ;
        [_starView addSubview:starImage];
        
        
    }
}

- (void)setModel:(WelfareModel *)model
{
    _model  = model ;
    [_imageIcon sd_setImageWithURL:[NSURL URLWithString:model.icon] placeholderImage:nil];
    _titleLabel.text    = model.name ;
    _subTitleLabel.text = model.category ;
    _numLabe .text      = [NSString stringWithFormat:@"%d次下载",model.dl_count];
    
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
