//
//  ProfitTableViewCell.m
//  NasiLive
//
//  Created by yun11 on 2020/4/1.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "ProfitTableViewCell.h"

@interface ProfitTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;


@end

@implementation ProfitTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(ProfitModel *)model{
    _model = model;
    self.titleLabel.text = model.consume_type == 1?model.gift.title:(model.moment.title.length > 0?model.moment.title:@"动态被解锁");
    self.timeLabel.text = model.create_time;
    self.countLabel.text = [NSString stringWithFormat:@"%d",model.coin_count];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
