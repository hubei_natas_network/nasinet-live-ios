//
//  VipPageViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/7/10.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "VipPageViewController.h"

#import "VipFuncCollectionViewCell.h"

@interface VipPageViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIImageView *logoImgView;
@property (weak, nonatomic) IBOutlet UILabel *vipStatusLabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *goldCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyBtn;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@property (strong, nonatomic) NSArray *funcArray;
@property (strong, nonatomic) NSArray *gotArray;

@end

@implementation VipPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    self.logoImgView.image = self.vipModel.logo;
    
    self.goldCountLabel.text = [NSString stringWithFormat:@"%d",self.vipModel.gold];
    self.priceLabel.text = [NSString stringWithFormat:@"%.2f",self.vipModel.price];
    
    CGFloat itemW = (KScreenWidth - 60 - 30)/3;
    CGFloat itemH = 110;
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc]init];
    flow.scrollDirection = UICollectionViewScrollDirectionVertical;
    flow.itemSize = CGSizeMake(itemW, itemH);
    flow.minimumLineSpacing = 30;
    flow.minimumInteritemSpacing = 30;
    flow.sectionInset = UIEdgeInsetsMake(0, 15, 0, 15);
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flow];
    self.collectionView.delegate   = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self.containerView addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"VipFuncCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"VipFuncCollectionViewCell"];
    [self.collectionView reloadData];
    
    [self refreshUI];
    
}

- (void)refreshUI{
    self.vipStatusLabel.hidden = [userManager.curUserInfo getVipLevel] != self.vipModel.level;
    self.vipStatusLabel.text = [NSString stringWithFormat:@"%@ 到期",userManager.curUserInfo.vip_date];
    self.bottomView.hidden = [userManager.curUserInfo getVipLevel] > self.vipModel.level || configManager.appConfig.switch_iap;
    self.buyBtn.selected = [userManager.curUserInfo getVipLevel] == self.vipModel.level;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.funcArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    VipFuncCollectionViewCell *cell = (VipFuncCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"VipFuncCollectionViewCell" forIndexPath:indexPath];
    cell.dict = self.funcArray[indexPath.row];
    cell.isGot = [self.gotArray containsObject:[NSString stringWithFormat:@"%ld",indexPath.row+1]];
    return cell;
}

- (IBAction)buyBtnClick:(UIButton *)sender {
    if (self.VipPageBuyClickBlock) {
        self.VipPageBuyClickBlock(self.vipModel.level);
    }
}

- (NSArray *)gotArray{
    if (!_gotArray) {
        switch (self.vipModel.level) {
            case 1:
                _gotArray = @[@"1",@"2",@"5",@"7",@"8"];
                break;
            case 2:
                _gotArray = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8"];
                break;
            case 3:
                _gotArray = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8"];
                break;
            case 4:
                _gotArray = @[@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9"];
                break;
            default:
                break;
        }
    }
    return _gotArray;
}

- (NSArray *)funcArray{
    if (!_funcArray) {
        _funcArray = @[@{@"icon":@"ic_vip_func_texiao_sel",@"icon_sel":@"ic_vip_func_texiao_sel",@"title":@"开通特效",@"subtitle":@"全频道广播"}
                        ,@{@"icon":@"ic_vip_func_card_sel",@"icon_sel":@"ic_vip_func_card_sel",@"title":@"贵族名片",@"subtitle":@"尊贵身份象征"}
                        ,@{@"icon":@"ic_vip_func_lianmai",@"icon_sel":@"ic_vip_func_lianmai_sel",@"title":@"连麦互动",@"subtitle":@"与主播零距离亲密互动"}
                        ,@{@"icon":@"ic_vip_func_gift",@"icon_sel":@"ic_vip_func_gift_sel",@"title":@"贵族礼物",@"subtitle":@"贵族专属礼物"}
                        ,@{@"icon":@"ic_vip_func_xunzhang_sel",@"icon_sel":@"ic_vip_func_xunzhang_sel",@"title":@"贵族勋章",@"subtitle":@"贵族专属勋章"}
                        ,@{@"icon":@"ic_vip_func_welcome",@"icon_sel":@"ic_vip_func_welcome_sel",@"title":@"进场欢迎",@"subtitle":@"进场信息提示"}
                        ,@{@"icon":@"ic_vip_func_moment",@"icon_sel":@"ic_vip_func_moment_sel",@"title":@"无限观影",@"subtitle":@"观影数量无上限"}
                        ,@{@"icon":@"ic_vip_func_msg_sel",@"icon_sel":@"ic_vip_func_msg_sel",@"title":@"无限畅聊",@"subtitle":@"与主播无限畅聊"}
                        ,@{@"icon":@"ic_vip_func_jinyan",@"icon_sel":@"ic_vip_func_jinyan_sel",@"title":@"防禁言",@"subtitle":@"无法被房管禁言"}];
    }
    return _funcArray;
}

#pragma mark - JXCategoryListContentViewDelegate

- (UIView *)listView {
    return self.view;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
