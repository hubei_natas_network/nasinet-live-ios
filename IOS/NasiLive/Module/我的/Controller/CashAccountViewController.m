//
//  CashAccountViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/4/2.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "CashAccountViewController.h"

@interface CashAccountViewController (){
    NSTimer *timer;
    NSInteger timerCount;
}

@property (weak, nonatomic) IBOutlet UITextField *accountTextField;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendCodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@end

@implementation CashAccountViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"管理提现账户";
    [self setIsShowLeftBack:YES];
    
    self.phoneLabel.text = userManager.curUserInfo.mobile;
    self.accountTextField.text = self.accountModel.alipay_account;
    self.nameTextField.text = self.accountModel.alipay_name;
}

- (IBAction)submitBtnClick:(UIButton *)sender {
    NSString *alipay_account = self.accountTextField.text;
    NSString *alipay_name = self.nameTextField.text;
    NSString *code = self.codeTextField.text;
    if ([alipay_name isEqualToString:self.accountModel.alipay_name] && [alipay_account isEqualToString:self.accountModel.alipay_account]) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    if (alipay_account.length == 0) {
        [MBProgressHUD showTipMessageInView:@"请输入支付宝账号"];
        [self.accountTextField becomeFirstResponder];
        return;
    }
    if (alipay_name.length == 0) {
        [MBProgressHUD showTipMessageInView:@"请输入支付宝姓名"];
        [self.nameTextField becomeFirstResponder];
        return;
    }
    if (code.length < 4) {
        [MBProgressHUD showTipMessageInView:@"验证码有误"];
        return;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"alipay_account":alipay_account,
                                                                                  @"alipay_name":alipay_name,
                                                                                  @"smscode":code
    }];
    if (self.accountModel) {
        params[@"accountid"] = @(self.accountModel.accountid);
    }
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"withdraw/editCashAccount" parameters:params success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            self.accountModel.alipay_name = alipay_name;
            self.accountModel.alipay_account = alipay_account;
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
    
}

- (IBAction)sendCodeBtnClick:(UIButton *)sender {
    NSString *mobile = self.phoneLabel.text;
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"user/sendVerifyCode" parameters:@{@"mobile":mobile} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            sender.enabled = NO;
            [sender setTitle:@"重新发送(60)" forState:UIControlStateNormal];
            self->timerCount = 60;
            if (!self->timer) {
                self->timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(timerGo) userInfo:nil repeats:YES];
            }
            [self->timer fire];
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)timerGo{
    timerCount --;
    if (timerCount == 0) {
        [timer invalidate];
        self.sendCodeBtn.enabled = YES;
        [self.sendCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    }else{
        [self.sendCodeBtn setTitle:[NSString stringWithFormat:@"重新发送(%ld)",(long)timerCount] forState:UIControlStateNormal];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
