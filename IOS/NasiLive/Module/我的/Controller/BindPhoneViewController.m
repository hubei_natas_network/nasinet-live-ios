//
//  BindPhoneViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/3/31.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BindPhoneViewController.h"

@interface BindPhoneViewController (){
    NSTimer *timer;
    NSInteger timerCount;
}

@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendCodeBtn;
@property (weak, nonatomic) IBOutlet UIButton *bindBtn;

@end

@implementation BindPhoneViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"绑定手机号";
    [self setIsShowLeftBack:YES];
    
}

- (IBAction)sendCodeBtnClick:(UIButton *)sender {
    NSString *mobile = self.phoneTextField.text;
    if (![CommonManager checkPhone:mobile]) {
        [self.phoneTextField becomeFirstResponder];
        [MBProgressHUD showTipMessageInView:@"请填写正确的手机号"];
        return;
    }
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"auth/sendBindCode" parameters:@{@"mobile":mobile} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            sender.enabled = NO;
            [sender setTitle:@"重新发送(60)" forState:UIControlStateNormal];
            self->timerCount = 60;
            if (!self->timer) {
                self->timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(timerGo) userInfo:nil repeats:YES];
            }
            [self->timer fire];
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (IBAction)bindBtnClick:(UIButton *)sender {
    NSString *mobile = self.phoneTextField.text;
    NSString *pwd = self.pwdTextField.text;
    NSString *code = self.codeTextField.text;
    
    if (![CommonManager checkPhone:mobile]) {
        [self.phoneTextField becomeFirstResponder];
        [MBProgressHUD showTipMessageInView:@"请填写正确的手机号"];
        return;
    }
    
    if (pwd.length < 6 || pwd.length > 18) {
        [self.pwdTextField becomeFirstResponder];
        [MBProgressHUD showTipMessageInView:@"密码长度为6-18位"];
        return;
    }
    
    if (code.length < 4) {
        [MBProgressHUD showTipMessageInView:@"验证码有误"];
        return;
    }
    
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"User/bindPhone" parameters:@{@"mobile":mobile,@"pwd":pwd,@"smscode":code} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            userManager.curUserInfo.mobile = mobile;
            [userManager saveUserInfo];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)timerGo{
    timerCount --;
    if (timerCount == 0) {
        [timer invalidate];
        self.sendCodeBtn.enabled = YES;
        [self.sendCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    }else{
        [self.sendCodeBtn setTitle:[NSString stringWithFormat:@"重新发送(%ld)",(long)timerCount] forState:UIControlStateNormal];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
