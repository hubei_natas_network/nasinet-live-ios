//
//  PhotoWallViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/9/15.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "PhotoWallViewController.h"

#import <YBImageBrowser.h>
#import <QCloudCore.h>
#import <QCloudCOSXML.h>
#import <TZImagePickerController.h>

@interface PhotoWallViewController ()<TZImagePickerControllerDelegate,YBImageBrowserDelegate>{
    NSMutableArray          *chooseImages;
    
    NSInteger               browserIndex;
    YBImageBrowser          *imgBrowser;
}

@property (weak, nonatomic) IBOutlet UIStackView *skView;
@property (weak, nonatomic) IBOutlet UIStackView *skView1;
@property (weak, nonatomic) IBOutlet UIStackView *skView2;
@property (weak, nonatomic) IBOutlet UIStackView *skView3;

@property (strong, nonatomic) NSMutableArray *imageUrlsArray;
@property (strong, nonatomic) NSMutableArray *uploadedUrls;

@end

@implementation PhotoWallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"照片墙";
    
    self.imageUrlsArray = [NSMutableArray arrayWithArray:userManager.curUserInfo.profile.photos];
    chooseImages = [NSMutableArray arrayWithCapacity:9];
    
    [self resetImageStackViewFirstLoad:YES];
}

- (void)resetImageStackViewFirstLoad:(BOOL)firstLoad{
    NSMutableArray *imgViewArray = [NSMutableArray arrayWithCapacity:9];
    NSArray *subViews1 = [self.skView1 subviews];
    for (UIView *view in subViews1) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imgView = (UIImageView *)view;
            imgView.image = nil;
            [imgView removeAllSubviews];
            [imgViewArray addObject:imgView];
        }
    }
    NSArray *subViews2 = [self.skView2 subviews];
    for (UIView *view in subViews2) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imgView = (UIImageView *)view;
            imgView.image = nil;
            [imgView removeAllSubviews];
            [imgViewArray addObject:imgView];
        }
    }
    NSArray *subViews3 = [self.skView3 subviews];
    for (UIView *view in subViews3) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imgView = (UIImageView *)view;
            imgView.image = nil;
            [imgView removeAllSubviews];
            [imgViewArray addObject:imgView];
        }
    }
    for (int i = 0; i < imgViewArray.count; i++) {
        UIImageView *imgView = imgViewArray[i];
        if (firstLoad) {
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(chooseImgTap:)];
            [imgView addGestureRecognizer:tap];
        }
        if (i < self.imageUrlsArray.count) {
            [imgView sd_setImageWithURL:[NSURL URLWithString:self.imageUrlsArray[i]] placeholderImage:IMAGE_NAMED(@"cover_loading")];
        }else if (i < chooseImages.count + self.imageUrlsArray.count) {
            [imgView setImage:chooseImages[i - self.imageUrlsArray.count]];
        }else if (i == chooseImages.count + self.imageUrlsArray.count){
            [imgView setImage:IMAGE_NAMED(@"ic_add")];
        }else{
            [imgView setImage:nil];
        }
    }
    self.skView2.hidden = (self.imageUrlsArray.count + chooseImages.count) < 3;
    self.skView3.hidden = (self.imageUrlsArray.count + chooseImages.count) < 6;
}

- (void)chooseImgTap:(UIGestureRecognizer *)ges{
    UIImageView *imgView = (UIImageView *)ges.view;
    NSInteger tag = imgView.tag;
    if (chooseImages.count + self.imageUrlsArray.count > tag) {
        //查看图片
        NSMutableArray *datas = [NSMutableArray array];
        [self.imageUrlsArray enumerateObjectsUsingBlock:^(NSString *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            // 网络图片
            YBIBImageData *data = [YBIBImageData new];
            data.imageURL = [NSURL URLWithString:obj];
            data.projectiveView = imgView;
            [datas addObject:data];
        }];
        [chooseImages enumerateObjectsUsingBlock:^(UIImage *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            // 本地图片
            YBIBImageData *data = [YBIBImageData new];
            data.image = ^UIImage * _Nullable{
                return obj;
            };
            data.projectiveView = imgView;
            [datas addObject:data];
        }];
        
        if (!imgBrowser) {
            imgBrowser = [YBImageBrowser new];
            [imgBrowser.defaultToolViewHandler.topView.operationButton setImage:IMAGE_NAMED(@"ic_delete") forState:UIControlStateNormal];
            [imgBrowser.defaultToolViewHandler.topView setClickOperation:^(YBIBTopViewOperationType type) {
                [self imgOperationClick];
            }];
        }
        imgBrowser.dataSourceArray = datas;
        imgBrowser.delegate = self;
        imgBrowser.currentPage = tag;
        [imgBrowser show];
    }else{
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:9 delegate:self];
        imagePickerVc.allowTakeVideo = NO;
        imagePickerVc.allowPickingGif = NO;
        imagePickerVc.allowPickingVideo = NO;
        imagePickerVc.maxImagesCount = 9 - tag;
        imagePickerVc.showSelectedIndex = YES;
        imagePickerVc.showPhotoCannotSelectLayer = YES;
        imagePickerVc.allowPickingOriginalPhoto = NO;
        imagePickerVc.naviTitleColor = [UIColor colorWithHexString:@"333333"];
        imagePickerVc.barItemTextColor = [UIColor colorWithHexString:@"333333"];
        imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
        [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
            for (int i=0; i<photos.count; i++) {
                UIImage *selImg = photos[i];
                [self->chooseImages addObject:selImg];
            }
            [self resetImageStackViewFirstLoad:NO];
        }];
        imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:imagePickerVc animated:YES completion:nil];
    }
}

- (void)imgOperationClick{
    if (self.imageUrlsArray.count > browserIndex){
        [self.imageUrlsArray removeObjectAtIndex:browserIndex];
    }else if (chooseImages.count + self.imageUrlsArray.count > browserIndex) {
        [chooseImages removeObjectAtIndex:browserIndex - self.imageUrlsArray.count];
    }
    [imgBrowser hide];
    [self resetImageStackViewFirstLoad:NO];
}

- (IBAction)saveBtnClick:(UIButton *)sender {
    if (chooseImages.count + self.imageUrlsArray.count == 0) {
        [MBProgressHUD showTipMessageInView:@"请先选择至少1张图片"];
        return;
    }
    [commonManager showLoadingAnimateInWindow];
    if (chooseImages.count > 0) {
        [self uploadImages];
    }else{
        [self submitToServer];
    }
}

- (void)uploadImages{
    [self.uploadedUrls removeAllObjects];
    [CommonManager POST:@"Config/getTempKeysForCos" parameters:@{} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            configManager.txCosModel = [TxCosModel yy_modelWithDictionary:responseObject[@"data"]];
            [configManager saveTxCosModel];
            for (int i = 0; i < self->chooseImages.count; i++) {
                NSString *fileName = [CommonManager getNameBaseCurrentTime:@".jpg"];
                NSString *flieUploadPath = [NSString stringWithFormat:@"%@/%@",configManager.appConfig.cos_folder_image,fileName];
                QCloudCOSXMLUploadObjectRequest* put = [QCloudCOSXMLUploadObjectRequest new];
                put.object = flieUploadPath;
                put.bucket = configManager.appConfig.cos_bucket;
                put.body = UIImageJPEGRepresentation(self->chooseImages[i], 1);
                [put setFinishBlock:^(QCloudUploadObjectResult *result, NSError* error) {
                    //可以从 result 获取结果
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (result && !error) {
                            if (self.uploadedUrls.count > i) {
                                [self.uploadedUrls insertObject:result.location atIndex:i];
                            }else{
                                [self.uploadedUrls addObject:result.location];
                            }
                            if (self.uploadedUrls.count == self->chooseImages.count) {
                                [self submitToServer];
                            }
                        }else{
                            [commonManager hideAnimateHud];
                            [commonManager showErrorAnimateInWindow];
                        }
                    });
                }];
                
                [[QCloudCOSTransferMangerService defaultCOSTransferManager] UploadObject:put];
            }
        }else{
            [commonManager hideAnimateHud];
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)submitToServer{
    if (self.uploadedUrls.count + self.imageUrlsArray.count == 0) {
        [MBProgressHUD showTipMessageInView:@"请先选择至少1张图片"];
        return;
    }
    NSMutableArray *urlsArray = [NSMutableArray array];
    for (NSString *url in self.imageUrlsArray) {
        [urlsArray addObject:url];
    }
    for (NSString *url in self.uploadedUrls) {
        [urlsArray addObject:url];
    }
    NSString *urlStr = [urlsArray componentsJoinedByString:@","];
    [CommonManager POST:@"user/editUserInfo" parameters:@{@"photos":urlStr} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            [commonManager showSuccessAnimateInWindow];
            userManager.curUserInfo.profile.photos = urlsArray;
            [userManager saveUserInfo];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)yb_imageBrowser:(YBImageBrowser *)imageBrowser pageChanged:(NSInteger)page data:(id<YBIBDataProtocol>)data{
    browserIndex = page;
}

- (NSMutableArray *)uploadedUrls{
    if (!_uploadedUrls) {
        _uploadedUrls = [NSMutableArray arrayWithCapacity:9];
    }
    return _uploadedUrls;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
