//
//  SettingViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/4/17.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "SettingViewController.h"
#import <SDImageCache.h>

#import "QNH5ViewController.h"

@interface SettingViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSArray *funcArray;
}

@end

@implementation SettingViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"设置";
    [self setIsShowLeftBack:YES];
    
    funcArray = @[@[@{@"title":@"关于我们",@"href":HREF_About},
                    @{@"title":@"使用条款和隐私政策",@"href":HREF_UserProtocol}],
                  @[@{@"title":@"清理缓存",@"href":@""}],
                  @[@{@"title":@"当前版本",@"href":@""}]];
    
    [self.view addSubview:self.tableView];
    self.tableView.backgroundColor = CLineColor;
    self.tableView.bounces = NO;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    self.tableView.separatorColor = CLineColor;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self removeTableMJFooter];
    [self removeTableMJHeader];
    
    UIButton *logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [logoutBtn setTitle:@"退出登录" forState:UIControlStateNormal];
    logoutBtn.titleLabel.font = FFont16;
    logoutBtn.backgroundColor = [UIColor whiteColor];
    [logoutBtn setTitleColor:CFontColor forState:UIControlStateNormal];
    logoutBtn.frame = CGRectMake(0, 320, KScreenWidth, 44);
    [logoutBtn addTarget:self action:@selector(logoutBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutBtn];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *array = funcArray[section];
    return array.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return funcArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"UITableViewCell"];
        cell.backgroundColor = [UIColor whiteColor];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.textColor = CFontColor;
        cell.detailTextLabel.textColor = CFontColor2;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = funcArray[indexPath.section][indexPath.row][@"title"];
    if ([funcArray[indexPath.section][indexPath.row][@"title"] isEqualToString:@"清理缓存"]){
        CGFloat fileSize = [SDImageCache sharedImageCache].totalDiskSize/1024.0/1024.0;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%0.2fM",fileSize];
    }else if ([funcArray[indexPath.section][indexPath.row][@"title"] isEqualToString:@"当前版本"]){
        cell.detailTextLabel.text = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([funcArray[indexPath.section][indexPath.row][@"href"] isEqualToString:@""]) {
        if ([funcArray[indexPath.section][indexPath.row][@"title"] isEqualToString:@"清理缓存"]){
            [[SDImageCache sharedImageCache].diskCache removeAllData];
            [self.tableView reloadData];
        }
    }else{
        QNH5ViewController *vc = [[QNH5ViewController alloc]init];
        vc.rootHref = funcArray[indexPath.section][indexPath.row][@"href"];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [[UIView alloc]init];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

- (void)logoutBtnClick{
    [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"提醒" contentText:@"确定要注销登录吗？" leftButtonTitle:@"确定" leftClick:^{
        [userManager logout:^(BOOL success, NSString *des) {
            if (!AppConfig_NeedLogin) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }
        }];
    } rightButtonTitle:@"取消" rightClick:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
