//
//  MyMomentViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/8/28.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"
#import "JXCategoryListContainerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyMomentViewController : RootViewController

@end

@interface MyMomentListViewController : RootViewController<JXCategoryListContentViewDelegate>

@property (assign, nonatomic) int status;

@end

NS_ASSUME_NONNULL_END
