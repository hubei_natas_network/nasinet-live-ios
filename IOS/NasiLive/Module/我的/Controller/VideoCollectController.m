//
//  VideoCollectController.m
//  MiyouBBS
//
//  Created by kevin on 2021/3/15.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "VideoCollectController.h"
#import "VideoInfoViewController.h"
#import "VideoListViewCell.h"

#define pagesize 20
@interface VideoCollectController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (strong, nonatomic) NSMutableArray        *dataAry;

@end

@implementation VideoCollectController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataAry = [NSMutableArray array];
    [self setUpUI];
   
}


- (void)requestDataAtPage:(int)page{
    
  
    [CommonManager POST:@"video/getUserCollect" parameters:@{@"page":@(page),@"size":@(pagesize)} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        [self.collectionView.mj_header endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            if (page == 1) {
                [self.dataAry removeAllObjects];
            }
            NSArray *array = [NSArray yy_modelArrayWithClass:[VideoModel class] json:responseObject[@"data"]];
            [self.dataAry addObjectsFromArray:array];
            [self.collectionView reloadData];

            if (array.count < pagesize) {
                [self removeCollectionViewMJFooter];
            }else{
                [self setupCollectionViewMJFooter];
            }
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        [self.collectionView.mj_header endRefreshing];
        RESP_FAILURE;
    }];
  
}
- (void)setUpUI{
    
    CGFloat itemW = (kScreenWidth - 30 - 9)/2;
    CGFloat itemH = itemW * 94/168 + 7 + 14;
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc]init];
    flow.scrollDirection = UICollectionViewScrollDirectionVertical;
    flow.itemSize = CGSizeMake(itemW, itemH);
    flow.minimumLineSpacing = 14;
    flow.minimumInteritemSpacing = 9;
    flow.sectionInset = UIEdgeInsetsMake(15,15,15,15);
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0,0, KScreenWidth, KScreenHeight ) collectionViewLayout:flow];
    self.collectionView.delegate   = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, 200, 0);
    [self.view addSubview:self.collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:@"VideoListViewCell" bundle:nil] forCellWithReuseIdentifier:@"VideoListViewCell"];
    [self removeCollectionViewMJFooter];
    [self setupCollectionViewMJHeader ];
    
    [self.collectionView.mj_header beginRefreshing];

}
- (void)footerRereshing{
    int page = ceil((self.dataAry.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    [self requestDataAtPage:page];
}

- (void)headerRereshing{
    [self requestDataAtPage:1];
}

#pragma mark -------------------- UICollectionViewDelegate  --------------------------------

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataAry.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
 
    VideoListViewCell   *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"VideoListViewCell" forIndexPath:indexPath];
    VideoModel *subModel     =   _dataAry[indexPath.row];
    cell.model = subModel ;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    VideoInfoViewController *vc = [[VideoInfoViewController alloc]init];
    vc.model = self.dataAry[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
