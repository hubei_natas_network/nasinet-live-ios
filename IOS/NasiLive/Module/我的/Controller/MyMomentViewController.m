//
//  MyMomentViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/8/28.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MyMomentViewController.h"
#import "MomentListViewController.h"

#import "MomentModel.h"

#import "JXCategoryTitleView.h"
#import "JXCategoryView.h"

@interface MyMomentViewController ()<JXCategoryViewDelegate,JXCategoryListContainerViewDelegate>

@property (nonatomic, strong) NSArray <NSString *>          *titles;

@property (nonatomic, strong) JXCategoryTitleView           *categoryView;
@property (strong, nonatomic) JXCategoryListContainerView   *listContainerView;

@end

@implementation MyMomentViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    //处于第一个item的时候，才允许屏幕边缘手势返回
    self.navigationController.interactivePopGestureRecognizer.enabled = (self.categoryView.selectedIndex == 0);
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //离开页面的时候，需要恢复屏幕边缘手势，不能影响其他页面
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的动态";
    
    _titles = @[@"已审核", @"待审核", @"未通过"];
    
    _categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
    self.categoryView.titles = self.titles;
    self.categoryView.backgroundColor = [UIColor clearColor];
    self.categoryView.delegate = self;
    self.categoryView.titleFont = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
    self.categoryView.titleSelectedColor = MAIN_COLOR;
    self.categoryView.titleColor = [UIColor colorWithHexString:@"#83796A"];
    self.categoryView.titleColorGradientEnabled = YES;
    self.categoryView.titleLabelZoomEnabled = NO;
    self.categoryView.contentScrollViewClickTransitionAnimationEnabled = NO;
    
    self.listContainerView = [[JXCategoryListContainerView alloc] initWithType:JXCategoryListContainerType_ScrollView delegate:self];
    self.listContainerView.frame = CGRectMake(0, self.categoryView.mj_maxY, KScreenWidth, KScreenHeight - self.categoryView.mj_maxY);
    self.listContainerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.listContainerView];
    
    self.categoryView.listContainer = self.listContainerView;
    [self.view addSubview:self.categoryView];
    
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorColor = MAIN_COLOR;
    lineView.indicatorWidth = 12;
    lineView.indicatorHeight = 3;
    lineView.indicatorCornerRadius = 1.5;
    self.categoryView.indicators = @[lineView];
}

#pragma mark - JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didScrollSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

#pragma mark - JXCategoryListContainerViewDelegate

- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index{
    MyMomentListViewController *vc = [[MyMomentListViewController alloc]init];
    switch (index) {
        case 0:
            vc.status = 1;
            break;
        case 1:
            vc.status =0;
            break;
        case 2:
            vc.status = 2;
            break;
        default:
            break;
    }
    return vc;
}

- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count;
}


@end

#import "UserInfoViewController.h"
#import "MomentInfoViewController.h"
#import "ReportCategoryViewController.h"

#import "MomentImagesCell.h"
#import "MomentTextCell.h"
#import "MomentVideoCell.h"
#import "MomentSingleImageCell.h"

#import "SharePanelView.h"

#import <SJVideoPlayer.h>
#import <SJBaseVideoPlayer/UIScrollView+ListViewAutoplaySJAdd.h>
#import <YBImageBrowser.h>

#define pagesize 20

@interface MyMomentListViewController ()<UITableViewDelegate,UITableViewDataSource,SJPlayerAutoplayDelegate,MomentBaseTableViewCellDelegate,SharePanelViewDelegate>{
    NSMutableArray *datasource;
    
    SJVideoPlayer *sjPlayer;
}

@property (weak, nonatomic) MomentModel *currentModel;

@end

@implementation MyMomentListViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [sjPlayer vc_viewDidAppear];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [sjPlayer vc_viewWillDisappear];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [sjPlayer vc_viewDidDisappear];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    datasource = [NSMutableArray array];
    
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 100;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self removeTableMJFooter];
    
    SJPlayerAutoplayConfig *config = [SJPlayerAutoplayConfig configWithAutoplayDelegate:self];
    config.autoplayPosition = SJAutoplayPositionMiddle;
    [self.tableView sj_enableAutoplayWithConfig:config];
    
    [MomentImagesCell registerWithTableView:self.tableView];
    [MomentSingleImageCell registerWithTableView:self.tableView];
    [MomentVideoCell registerWithTableView:self.tableView];
    [MomentTextCell registerWithTableView:self.tableView];
    
    [self.tableView.mj_header beginRefreshing];
    
}

- (void)headerRereshing{
    [self reqDataAtPage:1];
}

- (void)footerRereshing{
    int page = ceil((datasource.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    [self reqDataAtPage:page];
}

- (void)reqDataAtPage:(int)page{
    NSDictionary *params = @{@"page":@(page),
                             @"size":@(pagesize),
                             @"status":@(self.status)
    };
    
    [CommonManager POST:@"user/mymoment" parameters:params success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            if (page == 1) {
                [self->datasource removeAllObjects];
            }
            
            [self->sjPlayer stop];
            [self.tableView sj_removeCurrentPlayerView];
            [self.tableView sj_playNextVisibleAsset];
            
            NSArray *models = [NSArray yy_modelArrayWithClass:[MomentModel class] json:responseObject[@"data"]];
            [self->datasource addObjectsFromArray:models];
            [self.tableView reloadData];
            
            if (models.count < pagesize) {
                [self removeTableMJFooter];
            }else{
                [self setupTableViewMJFooter];
            }
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        RESP_FAILURE;
    }];
}

#pragma mark —————————— tableviewdelegate + datasource ——————————
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MomentModel *model = datasource[indexPath.row];
    if (model.type == MomentTypeImages) {
        if (model.image_urls.count == 1) {
            MomentSingleImageCell *cell = [MomentSingleImageCell cellWithTableView:tableView indexPath:indexPath];
            cell.delegate = self;
            cell.model = model;
            return cell;
        }else{
            MomentImagesCell *cell = [MomentImagesCell cellWithTableView:tableView indexPath:indexPath];
            cell.delegate = self;
            cell.model = model;
            return cell;
        }
    }else if (model.type == MomentTypeText){
        MomentTextCell *cell = [MomentTextCell cellWithTableView:tableView indexPath:indexPath];
        
        cell.delegate = self;
        cell.model = model;
        return cell;
    }else if (model.type == MomentTypeVideo){
        MomentVideoCell *cell = [MomentVideoCell cellWithTableView:tableView indexPath:indexPath];
        
        cell.delegate = self;
        cell.model = model;
        return cell;
    }
    return [[UITableViewCell alloc]init];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MomentInfoViewController *vc = [[MomentInfoViewController alloc]init];
    vc.momentModel = datasource[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

#pragma mark  ——————————————————————   SharePanelViewDelegate   ——————————————————————————
- (void)shareWithChannel:(PYShareChannel)channel panel:(nonnull SharePanelView *)panelView{
    if (channel == PYShareChannelReport) {
        ReportCategoryViewController *vc = [[ReportCategoryViewController alloc]init];
        vc.relateid = self.currentModel.momentid;
        vc.type = ReportTypeShortVideo;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (channel == PYShareChannelCollect){
        if (![CommonManager checkAndLogin]) {
            return;
        }
        [commonManager showLoadingAnimateInWindow];
        [CommonManager POST:@"moment/collectMoment" parameters:@{@"momentid":@(self.currentModel.momentid), @"type":@(!self.currentModel.collected)} success:^(id responseObject) {
            [commonManager hideAnimateHud];
            if (RESP_SUCCESS(responseObject)) {
                [commonManager showSuccessAnimateInWindow];
                self.currentModel.collected = !self.currentModel.collected;
                [panelView setCollectBtnSelected:self.currentModel.collected];
            }else{
                RESP_SHOW_ERROR_MSG(responseObject);
            }
        } failure:^(NSError *error) {
            [commonManager hideAnimateHud];
            RESP_FAILURE;
        }];
    }else if (channel == PYShareChannelLink){
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = [NSString stringWithFormat:@"%@%d",[configManager appConfig].share_moment_url,self.currentModel.momentid];
        [MBProgressHUD showTipMessageInView:@"链接已复制到剪贴板"];
    }else{
        SSDKPlatformType type = [SharePanelView getSSDKPlatformTypeBy:channel];
        //统一创建分享参数
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:self.currentModel.title
                                         images:self.currentModel.user.avatar
                                            url:[NSURL URLWithString:[NSString stringWithFormat:@"%@%d",[configManager appConfig].share_moment_url,self.currentModel.momentid]]
                                          title:@"推荐你看这个动态"
                                           type:SSDKContentTypeAuto];
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            switch (state) {
                case SSDKResponseStateSuccess:
                    [MBProgressHUD showTipMessageInView:@"分享成功"];
                    break;
                case SSDKResponseStateFail:
                {
                    [MBProgressHUD showTipMessageInView:@"分享失败"];
                    //失败
                    break;
                }
                case SSDKResponseStateCancel:
                    //取消
                    break;
                    
                default:
                    break;
            }
        }];
    }
}

#pragma mark —————————— SJPlayerAutoplayDelegate ———————————————
- (void)sj_playerNeedPlayNewAssetAtIndexPath:(NSIndexPath *)indexPath {
    if ( indexPath != nil ) {
        MomentModel *model = datasource[indexPath.row];
        if ( !sjPlayer ) {
            sjPlayer = [SJVideoPlayer player];
            sjPlayer.muted = YES;
            sjPlayer.playbackObserver.playbackDidFinishExeBlock = ^(__kindof SJBaseVideoPlayer * _Nonnull player) {
                [player replay];
            };
        }
        if (model.unlock_price > 0 && !model.unlocked && model.user.userid != userManager.curUserInfo.userid){
            return;
        }
        sjPlayer.URLAsset = [[SJVideoPlayerURLAsset alloc] initWithURL:[NSURL URLWithString:model.video_url] playModel:[SJPlayModel playModelWithTableView:self.tableView indexPath:indexPath]];
        sjPlayer.URLAsset.title = @"";
    }
}

#pragma mark —————————— MomentBaseTableViewCellDelegate ———————————————

- (void)likeClick:(MomentModel *)model{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    if (model.liked) {
        return;
    }
    [CommonManager POST:@"Moment/likeMoment" parameters:@{@"momentid":@(model.momentid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            model.liked = YES;
            model.like_count = [responseObject[@"data"][@"like_count"] intValue];
            [self.tableView reloadData];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (void)commentClick:(MomentModel *)model{
    
}

- (void)moreFuncClick:(MomentModel *)model{
    SharePanelView *panelView = [SharePanelView showPanelInView:kAppWindow];
    panelView.delegate = self;
    self.currentModel = model;
}

- (void)imgViewClick:imageView model:(MomentModel *)model index:(NSInteger)index{
    NSMutableArray *datas = [NSMutableArray array];
    [model.image_urls enumerateObjectsUsingBlock:^(NSString *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        // 网络图片
        YBIBImageData *data = [YBIBImageData new];
        data.imageURL = [NSURL URLWithString:obj];
        data.projectiveView = imageView;
        [datas addObject:data];
    }];
    
    YBImageBrowser *browser = [YBImageBrowser new];
    browser.dataSourceArray = datas;
    browser.currentPage = index;
    // 只有一个保存操作的时候，可以直接右上角显示保存按钮
    browser.defaultToolViewHandler.topView.operationType = YBIBTopViewOperationTypeSave;
    [browser show];
}


#pragma mark - JXCategoryListContentViewDelegate

- (UIView *)listView {
    return self.view;
}

@end
