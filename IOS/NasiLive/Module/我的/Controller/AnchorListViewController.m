//
//  AnchorListViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/6/8.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "AnchorListViewController.h"
#import "UserInfoViewController.h"

#import "AnchorTableViewCell.h"

@interface AnchorListViewController ()<UITableViewDelegate,UITableViewDataSource,AnchorTableViewCellDelegate>{
    NSMutableArray *datasource;
}

@property (copy, nonatomic) NSString                *api;
@property (strong, nonatomic) NSMutableDictionary   *param;

@property (copy, nonatomic) NSString                *keyword;

@end

#define pagesize 20

@implementation AnchorListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    [self removeTableMJFooter];
    [AnchorTableViewCell registerWithTableView:self.tableView];
    
    datasource = [NSMutableArray array];
    
    if (!(self.listType == AnchorListTypeSearch && self.keyword.length == 0)) {
        [self.tableView.mj_header beginRefreshing];
    }
}

- (void)headerRereshing{
    [self reqDataAtPage:1];
}

- (void)footerRereshing{
    int page = ceil((datasource.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    [self reqDataAtPage:page];
}

- (void)reqDataAtPage:(int)page{
    self.param[@"page"] = @(page);
    
    [CommonManager POST:self.api parameters:self.param success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            if (page == 1) {
                [self->datasource removeAllObjects];
            }
            NSArray *models = [NSArray yy_modelArrayWithClass:[UserInfoModel class] json:responseObject[@"data"]];
            [self->datasource addObjectsFromArray:models];
            [self.tableView reloadData];
            
            if (models.count < pagesize) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }else{
                [self setupTableViewMJFooter];
            }
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        RESP_FAILURE;
    }];
}

#pragma mark ----------------------  tableviewdelegate + datasource ----------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AnchorTableViewCell *cell = [AnchorTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.model = datasource[indexPath.row];
    cell.delegate = self;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UserInfoModel *model = datasource[indexPath.row];
    UserInfoViewController *vc = [[UserInfoViewController alloc]init];
    vc.anchorid = model.userid;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)attentBtnClick:(UIButton *)sender model:(UserInfoModel *)model{
    [CommonManager POST:@"Anchor/attentAnchor" parameters:@{@"anchorid":@(model.userid),@"type":sender.selected?@"0":@"1"} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            sender.selected = !sender.selected;
            model.isattent = sender.selected;
            model.fans_count = [responseObject[@"data"][@"fans_count"] intValue];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (NSString *)api{
    if (!_api) {
        switch (self.listType) {
            case AnchorListTypeFans:{
                _api = @"User/getFans";
            }
                break;
            case AnchorListTypeAttent:{
                _api = @"anchor/getAttentAnchors";
            }
                break;
            case AnchorListTypeSearch:{
                _api = @"anchor/search";
            }
                break;
            default:
                _api = @"";
                break;
        }
    }
    return _api;
}

- (NSMutableDictionary *)param{
    if (!_param) {
        _param = [NSMutableDictionary dictionary];
        _param[@"size"] = @(pagesize);
    }
    return _param;
}

- (void)setKeyword:(NSString *)keyword{
    if (keyword.length == 0 || [keyword isEqualToString:_keyword]) {
        return;
    }
    _keyword = keyword;
    self.param[@"keyword"] = keyword;
    [self.tableView.mj_header beginRefreshing];
}


#pragma mark - JXCategoryListContentViewDelegate

- (UIView *)listView {
    return self.view;
}

#pragma mark - SearchVcProtocal
- (void)searchKeyword:(NSString *)keyword{
    self.keyword = keyword;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
