//
//  LiveRoomMgrListViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/10/23.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "LiveRoomMgrListViewController.h"
#import "UserInfoViewController.h"

#import "UserTableViewCell.h"

#import "LiveRoomMgrModel.h"

@interface LiveRoomMgrListViewController ()<UITableViewDelegate,UITableViewDataSource,UserTableViewCellDelegate>{
    NSMutableArray *datasource;
}

@end

#define pagesize 20

@implementation LiveRoomMgrListViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
//    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.mas_equalTo(UIEdgeInsetsZero);
//    }];
    [self removeTableMJFooter];
    [UserTableViewCell registerWithTableView:self.tableView];
    
    datasource = [NSMutableArray array];
    
    [self reqData];
}

- (void)viewWillLayoutSubviews{
    self.tableView.frame = self.view.bounds;
}

- (void)refreshData{
    [self reqData];
}

- (void)headerRereshing{
    [self reqData];
}

- (void)reqData{
    [commonManager showLoadingAnimateInView:self.view];
    [CommonManager POST:@"live/getMgrList" parameters:nil success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            [self->datasource removeAllObjects];
            NSArray *models = [NSArray yy_modelArrayWithClass:[LiveRoomMgrModel class] json:responseObject[@"data"]];
            [self->datasource addObjectsFromArray:models];
            [self.tableView reloadData];
            
            if (self->datasource.count == 0) {
                [self showNoDataImage];
            }else{
                [self removeNoDataImage];
            }
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

#pragma mark ----------------------  tableviewdelegate + datasource ----------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    LiveRoomMgrModel *model = datasource[indexPath.row];
    UserTableViewCell *cell = [UserTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.model = model.user;
    cell.optBtnImage = IMAGE_NAMED(@"btn_cancel_mgr");
    cell.delegate = self;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UserInfoModel *model = datasource[indexPath.row];
    UserInfoViewController *vc = [[UserInfoViewController alloc]init];
    vc.anchorid = model.userid;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)optBtnClick:(UIButton *)sender model:(UserInfoModel *)model{
    [CommonManager POST:@"live/setRoomMgr" parameters:@{@"mgrid":@(model.userid),@"type":@(0)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            [self reqData];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}


#pragma mark - JXCategoryListContentViewDelegate

- (UIView *)listView {
    return self.view;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
