//
//  EditMineInfoViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/3/26.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "EditMineInfoViewController.h"
#import "BindPhoneViewController.h"
#import "PhotoWallViewController.h"

#import <TZImagePickerController.h>
#import "BRStringPickerView.h"

#import <QCloudCOSXML.h>

#import <UIButton+WebCache.h>

@interface EditMineInfoViewController ()<TZImagePickerControllerDelegate,UITextFieldDelegate>{
    NSString            *_avatarUrl;
}

@property (weak, nonatomic) IBOutlet UIButton *iconBtn;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UIButton *phoneBtn;
@property (weak, nonatomic) IBOutlet UITextField *signatureTextField;
@property (weak, nonatomic) IBOutlet UITextField *ageTextField;
@property (weak, nonatomic) IBOutlet UITextField *careerTextField;
@property (weak, nonatomic) IBOutlet UIButton *constellationBtn;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *heightTextField;
@property (weak, nonatomic) IBOutlet UITextField *weightTextField;
@property (weak, nonatomic) IBOutlet UIButton *genderBtn;

@end

@implementation EditMineInfoViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    UserInfoModel *model = [userManager curUserInfo];
    if (model.mobile.length > 0) {
        [self.phoneBtn setTitle:model.mobile forState:UIControlStateNormal];
    }else{
        [self.phoneBtn setTitle:@"绑定手机号" forState:UIControlStateNormal];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"编辑资料";
    [self setIsShowLeftBack:YES];
    
    [self addNavigationItemWithTitles:@[@"保存"] isLeft:NO target:self action:@selector(saveBtnClick:) tags:@[@(1)]];
    
    UserInfoModel *model = [userManager curUserInfo];
    
    [self.iconBtn sd_setImageWithURL:[NSURL URLWithString:model.avatar] forState:UIControlStateNormal];
    self.nameTextField.text = model.nick_name;
    
    self.signatureTextField.text = model.profile.signature;
    self.ageTextField.text = model.profile.age > 0 ? intstr(model.profile.age) : @"";;
    self.careerTextField.text = model.profile.career;
    self.cityTextField.text = model.profile.city;
    [self.constellationBtn setTitle:model.profile.constellation forState:UIControlStateNormal];
    self.heightTextField.text = model.profile.height > 0 ? intstr(model.profile.height) : @"";
    self.weightTextField.text = model.profile.weight > 0 ? intstr(model.profile.weight) : @"";
    if (model.profile.gender) {
        [self.genderBtn setTitle:@"男" forState:UIControlStateNormal];
    }else{
        [self.genderBtn setTitle:@"女" forState:UIControlStateNormal];
    }
    
}

- (IBAction)iconBtnClick:(UIButton *)sender {
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
    imagePickerVc.allowTakeVideo = NO;
    imagePickerVc.allowPickingGif = NO;
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.showPhotoCannotSelectLayer = YES;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.naviTitleColor = [UIColor colorWithHexString:@"333333"];
    imagePickerVc.barItemTextColor = [UIColor colorWithHexString:@"333333"];
    imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
    imagePickerVc.allowCrop = YES;
    imagePickerVc.cropRect = CGRectMake(30, (KScreenHeight - KScreenWidth + 60)/2, KScreenWidth - 60, KScreenWidth - 60);
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        if (photos.count > 0) {
            UIImage *selImg = photos[0];
            [commonManager showLoadingAnimateInWindow];
            [CommonManager POST:@"Config/getTempKeysForCos" parameters:@{} success:^(id responseObject) {
                [commonManager hideAnimateHud];
                if (RESP_SUCCESS(responseObject)) {
                    configManager.txCosModel = [TxCosModel yy_modelWithDictionary:responseObject[@"data"]];
                    [configManager saveTxCosModel];
                    NSString *fileName = [CommonManager getNameBaseCurrentTime:@".jpg"];
                    NSString *flieUploadPath = [NSString stringWithFormat:@"%@/%@",configManager.appConfig.cos_folder_image,fileName];
                    QCloudCOSXMLUploadObjectRequest* put = [QCloudCOSXMLUploadObjectRequest new];
                    put.object = flieUploadPath;
                    put.bucket = configManager.appConfig.cos_bucket;
                    put.body = UIImageJPEGRepresentation(selImg, 1);
                    [put setFinishBlock:^(QCloudUploadObjectResult *result, NSError* error) {
                        //可以从 result 获取结果
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (result && !error) {
                                self->_avatarUrl = result.location;
                                [self submitUserInfo];
                            }else{
                                [commonManager showErrorAnimateInWindow];
                            }
                        });
                    }];
                    [[QCloudCOSTransferMangerService defaultCOSTransferManager] UploadObject:put];
                }else{
                    RESP_SHOW_ERROR_MSG(responseObject);
                }
            } failure:^(NSError *error) {
                [commonManager hideAnimateHud];
                RESP_FAILURE;
            }];
        }
    }];
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (void)submitUserInfo{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (![_avatarUrl isEqualToString:userManager.curUserInfo.avatar]) {
        params[@"avatar"] = _avatarUrl;
    }
    if (![self.nameTextField.text isEqualToString:userManager.curUserInfo.nick_name]) {
        params[@"nick_name"] = self.nameTextField.text;
    }
    if (![self.signatureTextField.text isEqualToString:userManager.curUserInfo.profile.signature]) {
        params[@"signature"] = self.signatureTextField.text;
    }
    if (![self.ageTextField.text isEqualToString:intstr(userManager.curUserInfo.profile.age)]) {
        params[@"age"] = self.ageTextField.text;
    }
    NSString *userGender = userManager.curUserInfo.profile.gender ? @"男":@"女";
    if (![self.genderBtn.titleLabel.text isEqualToString:userGender]) {
        params[@"gender"] = @(!userManager.curUserInfo.profile.gender);
    }
    if (![self.careerTextField.text isEqualToString:userManager.curUserInfo.profile.career]) {
        params[@"career"] = self.careerTextField.text;
    }
    if (![self.constellationBtn.titleLabel.text isEqualToString:userManager.curUserInfo.profile.constellation]) {
        params[@"constellation"] = self.constellationBtn.titleLabel.text;
    }
    if (![self.cityTextField.text isEqualToString:userManager.curUserInfo.profile.city]) {
        params[@"city"] = self.cityTextField.text;
    }
    if (![self.heightTextField.text isEqualToString:intstr(userManager.curUserInfo.profile.height)]) {
        params[@"height"] = self.heightTextField.text;
    }
    if (![self.weightTextField.text isEqualToString:intstr(userManager.curUserInfo.profile.weight)]) {
        params[@"weight"] = self.weightTextField.text;
    }
    if ([params allKeys].count == 0) {
        return;
    }
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"User/editUserInfo" parameters:params success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            [commonManager showSuccessAnimateInWindow];
            if (self->_avatarUrl.length > 0) {
                userManager.curUserInfo.avatar = self->_avatarUrl;
                [self.iconBtn sd_setImageWithURL:[NSURL URLWithString:self->_avatarUrl] forState:UIControlStateNormal];
            }
            userManager.curUserInfo.nick_name = self.nameTextField.text;
            userManager.curUserInfo.profile.signature = self.signatureTextField.text;
            [userManager saveUserInfo];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (IBAction)genderBtnClick:(UIButton *)sender {
    BRResultModel *model1 = [[BRResultModel alloc]init];
    model1.index = 0;
    model1.value = @"女";
    model1.key = @"0";
    BRResultModel *model2 = [[BRResultModel alloc]init];
    model2.index = 1;
    model2.value = @"男";
    model2.key = @"1";
    int selectedIndex = [sender.titleLabel.text isEqualToString:@"男"] ? 1 : 0;
    [BRStringPickerView showPickerWithTitle:@"请选择性别" dataSourceArr:@[model1,model2] selectIndex:selectedIndex resultBlock:^(BRResultModel * _Nullable resultModel) {
        [sender setTitle:resultModel.value forState:UIControlStateNormal];
    }];
}

- (IBAction)constellationBtnClick:(UIButton *)sender {
    NSArray *constellations = @[@"白羊座",@"金牛座",@"双子座",@"巨蟹座",@"狮子座",@"处女座",@"天秤座",@"天蝎座",@"射手座",@"摩羯座",@"水瓶座",@"双鱼座"];
    NSMutableArray *modelArray = [[NSMutableArray alloc]init];
    int selectedIndex = 0;
    for (int i = 0; i < constellations.count; i++) {
        BRResultModel *model = [[BRResultModel alloc]init];
        model.index = i;
        model.value = constellations[i];
        [modelArray addObject:model];
        if ([sender.titleLabel.text isEqualToString:constellations[i]]) {
            selectedIndex = i;
        }
    }
    [BRStringPickerView showPickerWithTitle:@"请选择性别" dataSourceArr:modelArray selectIndex:selectedIndex resultBlock:^(BRResultModel * _Nullable resultModel) {
        [sender setTitle:resultModel.value forState:UIControlStateNormal];
    }];
}

- (IBAction)phoneBtnClick:(UIButton *)sender {
    if (userManager.curUserInfo.mobile.length > 0) {
        return;
    }
    BindPhoneViewController *vc = [[BindPhoneViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)thumbBtnClick:(UIButton *)sender {
    PhotoWallViewController *vc = [[PhotoWallViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)saveBtnClick:(UIButton *)sender {
    [self submitUserInfo];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
