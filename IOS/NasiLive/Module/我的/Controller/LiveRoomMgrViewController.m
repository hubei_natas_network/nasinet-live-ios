//
//  LiveRoomMgrViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/10/23.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "LiveRoomMgrViewController.h"
#import "UserInfoViewController.h"

#import "UserTableViewCell.h"

@interface LiveRoomMgrViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray          *_datasource;
}

@end

#define pagesize 20

@implementation LiveRoomMgrViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"房间管理";
    
    [self setIsShowLeftBack:YES];
    
    _datasource = [NSMutableArray array];
    
    [self.view addSubview:self.tableView];
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self removeTableMJFooter];
    
    [UserTableViewCell registerWithTableView:self.tableView];
    
    [self.tableView.mj_header beginRefreshing];
}



- (void)headerRereshing{
    [self reqDataAtPage:1];
}

- (void)footerRereshing{
    int page = ceil((_datasource.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    [self reqDataAtPage:page];
}

- (void)reqDataAtPage:(int)page{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"size"] = @(pagesize);
    params[@"page"] = @(page);
    
    [CommonManager POST:@"User/getManagedRooms" parameters:params success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            if (page == 1) {
                [self->_datasource removeAllObjects];
            }
            NSArray *models = [NSArray yy_modelArrayWithClass:[UserInfoModel class] json:responseObject[@"data"]];
            [self->_datasource addObjectsFromArray:models];
            
            [self.tableView reloadData];
            if (models.count < pagesize) {
                [self removeTableMJFooter];
            }else{
                [self setupTableViewMJFooter];
            }
            if (self->_datasource.count == 0) {
                [self showNoDataImage];
            }else{
                [self removeNoDataImage];
            }
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        RESP_FAILURE;
    }];
}

#pragma mark ——————————————————————————————————————  tableviewdelegate + datasource ——————————————————————————————————————
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserTableViewCell *cell = [UserTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.model = _datasource[indexPath.row];
    cell.optBtnImage = [UIImage new];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UserInfoModel *model = _datasource[indexPath.row];
    UserInfoViewController *vc = [[UserInfoViewController alloc]init];
    vc.anchorid = model.userid;
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
