//
//  ProfitViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/4/1.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "ProfitViewController.h"
#import "XLPageViewController.h"

#import "ProfitModel.h"
#import "ProfitTableViewCell.h"

@interface ProfitViewController ()<XLPageViewControllerDelegate,XLPageViewControllerDataSrouce>{
    XLPageViewController                *xlPageVc;
    NSArray                             *pageTitleArr;
}

@end

@implementation ProfitViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"收益记录";
    [self setIsShowLeftBack:YES];
    
    pageTitleArr = @[@"礼物收益",@"动态收益"];
    
    XLPageViewControllerConfig *config = [XLPageViewControllerConfig defaultConfig];
    config.separatorLineColor = [UIColor colorWithHexString:@"F4F4F4"];
    config.titleNormalColor = [UIColor colorWithHexString:@"888888"];
    config.titleSelectedColor = MAIN_COLOR;
    config.titleNormalFont = FFont15;
    config.titleSelectedFont = FFont15;
    config.titleSpace = 0;
    config.titleWidth = (self.view.width-30) / pageTitleArr.count;
    
    config.titleViewHeight = 44;
    config.titleViewInset = UIEdgeInsetsMake(0, 15, 0, 15);
    
    config.shadowLineColor = MAIN_COLOR;
    config.shadowLineWidth = 20;
    
    xlPageVc = [[XLPageViewController alloc] initWithConfig:config];
    xlPageVc.view.frame = self.view.bounds;
    xlPageVc.delegate = self;
    xlPageVc.dataSource = self;
    [self.view addSubview:xlPageVc.view];
    [self addChildViewController:xlPageVc];
}

//根据index创建对应的视图控制器，每个试图控制器只会被创建一次。
- (UIViewController *)pageViewController:(XLPageViewController *)pageViewController viewControllerForIndex:(NSInteger)index{
    ProfitListViewController *vc = [[ProfitListViewController alloc]init];
    vc.type = index;
    return vc;
}

//根据index返回对应的标题
- (NSString *)pageViewController:(XLPageViewController *)pageViewController titleForIndex:(NSInteger)index{
    return pageTitleArr[index];
}

//返回分页数
- (NSInteger)pageViewControllerNumberOfPage{
    return pageTitleArr.count;
}

- (void)pageViewController:(XLPageViewController *)pageViewController didSelectedAtIndex:(NSInteger)index{
    
}

@end

#pragma mark —————————————————————————————————————— ProfitListViewController ——————————————————————————————————————

#define pagesize 20

@interface ProfitListViewController()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray          *_datasource;
}

@end

@implementation ProfitListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _datasource = [NSMutableArray array];
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self removeTableMJFooter];
    [ProfitTableViewCell registerWithTableView:self.tableView];
    
    [self.tableView.mj_header beginRefreshing];
}

- (void)headerRereshing{
    [self reqDataAtPage:1];
}

- (void)footerRereshing{
    int page = ceil((_datasource.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    [self reqDataAtPage:page];
}

- (void)reqDataAtPage:(int)page{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"size"] = @(pagesize);
    params[@"page"] = @(page);
    
    NSString *api = self.type == 0? @"User/giftProfit":@"User/momentProfit";
    
    [CommonManager POST:api parameters:params success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            if (page == 1) {
                [self->_datasource removeAllObjects];
            }
            NSArray *models = [NSArray yy_modelArrayWithClass:[ProfitModel class] json:responseObject[@"data"]];
            [self->_datasource addObjectsFromArray:models];
            
            [self.tableView reloadData];
            if (models.count < pagesize) {
                [self removeTableMJFooter];
            }else{
                [self setupTableViewMJFooter];
            }
            if (self->_datasource.count == 0) {
                [self showNoDataImage];
            }else{
                [self removeNoDataImage];
            }
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        RESP_FAILURE;
    }];
}

#pragma mark ——————————————————————————————————————  tableviewdelegate + datasource ——————————————————————————————————————
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ProfitTableViewCell *cell = [ProfitTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.model = _datasource[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

@end
