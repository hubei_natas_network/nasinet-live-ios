//
//  VipPageViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/7/10.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"
#import "JXCategoryListContainerView.h"

#import "VipModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface VipPageViewController : RootViewController<JXCategoryListContentViewDelegate>

@property (strong, nonatomic) VipModel *vipModel;

@property (nonatomic,copy) void (^VipPageBuyClickBlock)(NSInteger level);

- (void)refreshUI;

@end

NS_ASSUME_NONNULL_END
