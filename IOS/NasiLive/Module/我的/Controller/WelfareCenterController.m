//
//  WelfareCenterController.m
//  NasiBBS
//
//  Created by kevin on 2021/3/23.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "WelfareCenterController.h"
#import "WelfareViewCell.h"
#import "WelfareModel.h"

@interface WelfareCenterController ()<UITableViewDelegate,UITableViewDataSource>
{
    
    NSMutableArray        *datasource;
}

@end

@implementation WelfareCenterController
- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleDefault;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //离开页面的时候，需要恢复屏幕边缘手势，不能影响其他页面
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"福利中心";
    
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    [self removeTableMJFooter];
    [self removeTableMJHeader];
    
    [WelfareViewCell registerWithTableView:self.tableView];
    
    datasource = [NSMutableArray array];
    [self reqDate] ;
  
}



- (void)reqDate{
  
    
    [CommonManager POST:@"Promotion/getAppslist" parameters:@{} success:^(id responseObject) {
        
        if (RESP_SUCCESS(responseObject)) {

            NSArray *models = [NSArray yy_modelArrayWithClass:[WelfareModel class] json:responseObject[@"data"]];
            [self->datasource addObjectsFromArray:models];
            [self.tableView reloadData];

        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
    
        RESP_FAILURE;
    }];
}

#pragma mark ----------------------  tableviewdelegate + datasource ----------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self->datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    
    WelfareViewCell *cell = [WelfareViewCell cellWithTableView:tableView indexPath:indexPath];
    WelfareModel *model   =  self->datasource[indexPath.row];
    cell.model = model ;
    return cell;
  

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    WelfareModel *model   =  self->datasource[indexPath.row];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:model.dl_url] options:@{} completionHandler:nil];
    
}

@end
