//
//  MomentWatchLogViewController.m
//  NasiBBS
//
//  Created by yun11 on 2020/9/24.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MomentWatchLogViewController.h"
#import "MomentListViewController.h"

@interface MomentWatchLogViewController ()

@end

@implementation MomentWatchLogViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"浏览历史";
    
    MomentListViewController *vc = [[MomentListViewController alloc]init];
    vc.listType = MomentListTypeWatchLog;
    [self addChildViewController:vc];
    [self.view addSubview:vc.view];
    vc.view.frame = CGRectMake(0, 0, KScreenWidth, KScreenHeight);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
