//
//  CollectionViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/6/23.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "CollectionViewController.h"
#import "XLPageViewController.h"
#import "SVideoListViewController.h"
#import "UserMomentListViewController.h"

#import "SVScaleVideoView.h"
#import "SharePanelView.h"
#import "SVCommentView.h"
#import "VideoCollectController.h"

@interface CollectionViewController ()<XLPageViewControllerDelegate,XLPageViewControllerDataSrouce,SVVideoViewDelegate, SharePanelViewDelegate,SVScaleVideoViewProtocol>{
    NSArray *pageTitleArr;
}

@property (nonatomic, weak) SVScaleVideoView      *scaleView;

@property (strong, nonatomic) SVCommentView       *commentView;

@end

@implementation CollectionViewController
@synthesize currentListVC;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pageTitleArr = @[@"影片",@"动态", @"短视频"];
    
    XLPageViewControllerConfig *config = [XLPageViewControllerConfig defaultConfig];
    config.separatorLineHidden = YES;
    config.titleNormalColor = [UIColor darkGrayColor];
    config.titleSelectedColor = [UIColor blackColor];
    config.titleNormalFont = FFont15;
    config.titleSelectedFont = [UIFont boldSystemFontOfSize:22];
    config.titleSpace = 7.f;
    
    config.titleViewHeight = 44;
    config.titleViewInset = UIEdgeInsetsMake(5, kNavBarHeight + 5, 7, 44);
    
    config.shadowLineColor = MAIN_COLOR;
    config.shadowLineWidth = 15;
    
    XLPageViewController *pageViewController = [[XLPageViewController alloc] initWithConfig:config];
    pageViewController.view.frame = CGRectMake(0, kStatusBarHeight, KScreenWidth, KScreenHeight - kStatusBarHeight);
    pageViewController.delegate = self;
    pageViewController.dataSource = self;
    [self.view addSubview:pageViewController.view];
    [self addChildViewController:pageViewController];
    
    UIButton *navBackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    navBackBtn.frame = CGRectMake(0, kStatusBarHeight, kNavBarHeight, kNavBarHeight);
    [navBackBtn setImage:IMAGE_NAMED(@"ic_back") forState:UIControlStateNormal];
    [navBackBtn addTarget:self action:@selector(backBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:navBackBtn];
}

//根据index创建对应的视图控制器，每个试图控制器只会被创建一次。
- (UIViewController *)pageViewController:(XLPageViewController *)pageViewController viewControllerForIndex:(NSInteger)index{
    switch (index) {
        case 0:{
            
            VideoCollectController *vc =  [[VideoCollectController alloc]init];
            return vc;
        }
            break;
        case 1:{
            
            UserMomentListViewController *listVc = [[UserMomentListViewController alloc] init];
            listVc.type = 1;
            return listVc;
        }
            break;
        case 2:{
            SVideoListViewController *listVc = [[SVideoListViewController alloc] init];
            listVc.type = 2;
            kWeakSelf(listVc)
            @weakify(self);
            listVc.itemClickBlock = ^(NSArray * _Nonnull videos, NSInteger index) {
                @strongify(self);
                [self showVideoVCWithVideos:videos datasourceVC:weaklistVc index:index];
            };
            self.currentListVC = listVc;
            return listVc;
        }
            
        default:
            break;
    }
    return nil;
}

//根据index返回对应的标题
- (NSString *)pageViewController:(XLPageViewController *)pageViewController titleForIndex:(NSInteger)index{
    return pageTitleArr[index];
}

//返回分页数
- (NSInteger)pageViewControllerNumberOfPage{
    return pageTitleArr.count;
}

- (void)pageViewController:(XLPageViewController *)pageViewController didSelectedAtIndex:(NSInteger)index{
    
}

- (void)showVideoVCWithVideos:(NSArray *)videos datasourceVC:(SVideoListViewController *)vc index:(NSInteger)index {
    SVScaleVideoView *scaleView = [[SVScaleVideoView alloc] initWithVC:self datasourceVC:vc videos:videos index:index];
    scaleView.videoView.delegate = self;
    [scaleView show];
    
    self.scaleView = scaleView;
}

#pragma mark - SharePanelViewDelegate
- (void)shareWithChannel:(PYShareChannel)channel panel:(nonnull SharePanelView *)panelView{
    
}

#pragma mark - SVVideoViewDelegate
- (void)videoView:(SVVideoView *)videoView didClickIcon:(ShortVideoModel *)videoModel {
    UserSVPageController *vc = [[UserSVPageController alloc]init];
    vc.author = videoModel.author;
    vc.SvAttentUserBlock = ^(int userid, BOOL attented) {
        [videoView updateUserModel:userid attent:attented];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)videoView:(SVVideoView *)videoView didClickAttent:(nonnull ShortVideoModel *)videoModel {
    ShortVideoModel *model = videoModel;
    
    int type = model.author.isattent ? 0 : 1;
    [CommonManager POST:@"Anchor/attentAnchor" parameters:@{@"anchorid":@(model.author.userid),@"type":@(type)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            model.author.isattent = !model.author.isattent;
            model.author.fans_count = [responseObject[@"data"][@"fans_count"] intValue];
            videoView.currentPlayView.model = videoModel;
            [self.scaleView.videoView updateUserModel:model.author.userid attent:model.author.isattent];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}


- (void)videoView:(SVVideoView *)videoView didClickComment:(ShortVideoModel *)videoModel {
    if (!_commentView) {
        _commentView = [[SVCommentView alloc]initWithFrame:CGRectMake(0, KScreenHeight, KScreenWidth, KScreenHeight)];
        _commentView.CommentCountBlock = ^(int count) {
            videoView.currentPlayView.model = videoModel;
        };
        [kAppWindow addSubview:_commentView];
    }
    _commentView.videoModel = videoModel;
    [UIView animateWithDuration:0.3 animations:^{
        self->_commentView.mj_y = 0;
    }];
}

- (void)videoView:(SVVideoView *)videoView didClickShare:(ShortVideoModel *)videoModel {
    SharePanelView *panelView = [SharePanelView showPanelInView:kAppWindow];
    panelView.delegate = self;
}

- (void)videoView:(SVVideoView *)videoView didScrollIsCritical:(BOOL)isCritical {
    
}

@end
