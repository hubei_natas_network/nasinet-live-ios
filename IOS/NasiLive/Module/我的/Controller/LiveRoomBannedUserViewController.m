//
//  LiveRoomBannedUserViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/10/23.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "LiveRoomBannedUserViewController.h"
#import "UserInfoViewController.h"

#import "UserTableViewCell.h"

#import "LiveRoomMgrModel.h"

@interface LiveRoomBannedUserViewController ()<UITableViewDelegate,UITableViewDataSource,UserTableViewCellDelegate>{
    NSMutableArray *datasource;
}

@end

#define pagesize 20

@implementation LiveRoomBannedUserViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    //    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.edges.mas_equalTo(UIEdgeInsetsZero);
    //    }];
    [self removeTableMJFooter];
    
    [UserTableViewCell registerWithTableView:self.tableView];
    
    datasource = [NSMutableArray array];
    
    [self reqDataAtPage:1];
    
}

- (void)viewWillLayoutSubviews{
    self.tableView.frame = self.view.bounds;
}

- (void)refreshData{
    [self reqDataAtPage:1];
}

- (void)headerRereshing{
    [self reqDataAtPage:1];
}

- (void)footerRereshing{
    int page = ceil((datasource.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    [self reqDataAtPage:page];
}

- (void)reqDataAtPage:(int)page{
    NSDictionary *params = @{@"page":@(page),
                             @"size":@(pagesize),
                             @"anchorid":@(self.anchorid)
    };
    if (page == 1) {
        [commonManager showLoadingAnimateInView:self.view];
    }
    [CommonManager POST:@"live/getBannedUserList" parameters:params success:^(id responseObject) {
        [commonManager hideAnimateHud];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView.mj_header endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            if (page == 1) {
                [self->datasource removeAllObjects];
            }
            NSArray *models = [NSArray yy_modelArrayWithClass:[UserInfoModel class] json:responseObject[@"data"]];
            [self->datasource addObjectsFromArray:models];
            [self.tableView reloadData];
            
            if (self->datasource.count == 0) {
                [self showNoDataImage];
            }else{
                [self removeNoDataImage];
            }
            if (models.count < pagesize) {
                [self removeTableMJFooter];
            }else{
                [self setupTableViewMJFooter];
            }
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        [self.tableView.mj_footer endRefreshing];
        [self.tableView.mj_header endRefreshing];
        RESP_FAILURE;
    }];
}

#pragma mark ----------------------  tableviewdelegate + datasource ----------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UserTableViewCell *cell = [UserTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.model = datasource[indexPath.row];
    cell.optBtnImage = IMAGE_NAMED(@"btn_cancel_ban");
    cell.delegate = self;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UserInfoModel *model = datasource[indexPath.row];
    UserInfoViewController *vc = [[UserInfoViewController alloc]init];
    vc.anchorid = model.userid;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)optBtnClick:(UIButton *)sender model:(UserInfoModel *)model{
    [CommonManager POST:@"live/banUser" parameters:@{@"anchorid":@(self.anchorid),@"userid":@(model.userid),@"type":@(0)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            [self reqDataAtPage:1];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}


#pragma mark - JXCategoryListContentViewDelegate

- (UIView *)listView {
    return self.view;
}

@end
