//
//  AttentViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/6/8.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AttentViewController : RootViewController

@property (assign, nonatomic) BOOL isAttent;

@end

NS_ASSUME_NONNULL_END
