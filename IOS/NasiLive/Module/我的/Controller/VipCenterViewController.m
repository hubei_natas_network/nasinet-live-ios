//
//  VipCenterViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/7/10.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "VipCenterViewController.h"

#import "VipPageViewController.h"

#import "JXCategoryTitleView.h"
#import "JXCategoryView.h"

#import "VipModel.h"

#import <AlipaySDK/AlipaySDK.h>

@interface VipCenterViewController ()<JXCategoryViewDelegate,JXCategoryListContainerViewDelegate>

@property (nonatomic, strong) NSArray <NSString *>          *titles;
@property (strong, nonatomic) NSArray                       *vipDataArray;

@property (nonatomic, strong) JXCategoryTitleView           *categoryView;
@property (strong, nonatomic) JXCategoryListContainerView   *listContainerView;

@property (weak, nonatomic) IBOutlet UIView     *payChannelView;
@property (weak, nonatomic) IBOutlet UIView     *shadowView;

@property (weak, nonatomic) IBOutlet UIView     *alipayView;
@property (weak, nonatomic) IBOutlet UIView     *wxpayView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *channelViewTopLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *channelViewHeightLC;

@property (assign, nonatomic) NSInteger buyingIndex;

@end

@implementation VipCenterViewController

- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // 默认点击当前已开通等级
    int level = [userManager.curUserInfo getVipLevel];
    if (level > 0) {
        [self categoryView:self.categoryView didSelectedItemAtIndex:level-1];
    }
    
    //处于第一个item的时候，才允许屏幕边缘手势返回
    self.navigationController.interactivePopGestureRecognizer.enabled = (self.categoryView.selectedIndex == 0);
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //离开页面的时候，需要恢复屏幕边缘手势，不能影响其他页面
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _titles = @[@"游侠", @"骑士", @"公爵", @"国王"];
    
    [self loadVipList];
    
    self.channelViewHeightLC.constant = 200 + kBottomSafeHeight;
    
    UITapGestureRecognizer *tapAlipay = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(alipayClick)];
    [self.alipayView addGestureRecognizer:tapAlipay];
    
    UITapGestureRecognizer *tapWxpay = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(wxPayClick)];
    [self.wxpayView addGestureRecognizer:tapWxpay];
    
    UITapGestureRecognizer *tapShadow = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shadowTap)];
    [self.shadowView addGestureRecognizer:tapShadow];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(paySuccessNotification) name:KNotificationPaySuccess object:nil];
}

- (void)createUI{
    NSInteger index = [userManager.curUserInfo getVipLevel] - 1;
    if (index < 0) {
        index = 0;
    }
    
    _categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, kTopHeight + 12, [UIScreen mainScreen].bounds.size.width, 30)];
    self.categoryView.titles = self.titles;
    self.categoryView.backgroundColor = [UIColor clearColor];
    self.categoryView.delegate = self;
    self.categoryView.titleFont = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
    self.categoryView.titleSelectedColor = [UIColor colorWithHexString:@"#E8CF8B"];
    self.categoryView.titleColor = [UIColor colorWithHexString:@"#83796A"];
    self.categoryView.titleColorGradientEnabled = YES;
    self.categoryView.titleLabelZoomEnabled = NO;
    self.categoryView.contentScrollViewClickTransitionAnimationEnabled = NO;
    self.categoryView.defaultSelectedIndex = index;
    
    self.listContainerView = [[JXCategoryListContainerView alloc] initWithType:JXCategoryListContainerType_ScrollView delegate:self];
    self.listContainerView.frame = CGRectMake(0, self.categoryView.mj_maxY, KScreenWidth, KScreenHeight - self.categoryView.mj_maxY);
    self.listContainerView.backgroundColor = [UIColor clearColor];
    [self.listContainerView setDefaultSelectedIndex:index];
    [self.view insertSubview:self.listContainerView belowSubview:self.shadowView];
    
    self.categoryView.listContainer = self.listContainerView;
    [self.view insertSubview:self.categoryView belowSubview:self.shadowView];
    
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorColor = [UIColor colorWithHexString:@"#E8CF8B"];
    lineView.indicatorWidth = 12;
    lineView.indicatorHeight = 3;
    lineView.indicatorCornerRadius = 1.5;
    self.categoryView.indicators = @[lineView];
}

- (void)loadVipList{
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"vip/getVipPriceList" parameters:nil success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            self.vipDataArray = [NSArray yy_modelArrayWithClass:[VipModel class] json:responseObject[@"data"]];
            [self createUI];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)alipayClick{
    [self shadowTap];
    VipModel *model = self.vipDataArray[self.buyingIndex];
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"vip/getAliPayOrder" parameters:@{@"level":@(model.level)} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            [[AlipaySDK defaultService] payOrder:responseObject[@"data"][@"paystr"] fromScheme:@"nasilive" callback:^(NSDictionary *resultDic) {
                NSLog(@"1");
            }];
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)wxPayClick{
    [self shadowTap];
}

- (void)shadowTap{
    self.channelViewTopLC.constant = 0;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.shadowView.hidden = YES;
    }];
}

- (void)paySuccessNotification{
    [CommonManager POST:@"User/getUserInfo" parameters:nil success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            userManager.curUserInfo = [UserInfoModel yy_modelWithDictionary:responseObject[@"data"]];
            [userManager saveUserInfo];
            for (int i = 0; i<self.titles.count; i++) {
                VipPageViewController *vc = (VipPageViewController *)self.listContainerView.validListDict[[NSNumber numberWithInt:i]];
                [vc refreshUI];
            }
        }
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark - JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
    VipPageViewController *vc = (VipPageViewController *)self.listContainerView.validListDict[[NSNumber numberWithInteger:index]];
    [vc refreshUI];
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didScrollSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
    VipPageViewController *vc = (VipPageViewController *)self.listContainerView.validListDict[[NSNumber numberWithInteger:index]];
    [vc refreshUI];
}

#pragma mark - JXCategoryListContainerViewDelegate

- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index{
    VipPageViewController *vc = [[VipPageViewController alloc]init];
    vc.vipModel = self.vipDataArray[index];
    kWeakSelf(self);
    vc.VipPageBuyClickBlock = ^(NSInteger index) {
        weakself.buyingIndex = index - 1;
        weakself.shadowView.hidden = NO;
        weakself.channelViewTopLC.constant = -(200 + kBottomSafeHeight);
        [UIView animateWithDuration:0.2f animations:^{
            [weakself.view layoutIfNeeded];
        }];
    };
    return vc;
}

- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count;
}

- (IBAction)backBtnClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
