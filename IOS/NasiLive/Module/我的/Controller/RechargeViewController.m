//
//  RechargeViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/4/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RechargeViewController.h"

#import "RechargeCollectionViewCell.h"

#import <AlipaySDK/AlipaySDK.h>
#import <WXApi.h>
#import <StoreKit/StoreKit.h>

@interface RechargeViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,SKProductsRequestDelegate,SKPaymentTransactionObserver>{
    NSArray                 *datasource;
    
    NSString                *currentGoodsid;
}

@property (weak, nonatomic) IBOutlet UILabel    *goldCountLabel;
@property (weak, nonatomic) IBOutlet UIView     *containerView;

@property (weak, nonatomic) IBOutlet UIView     *payChannelView;
@property (weak, nonatomic) IBOutlet UIView     *shadowView;

@property (weak, nonatomic) IBOutlet UIView     *alipayView;
@property (weak, nonatomic) IBOutlet UIView     *wxpayView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *channelViewTopLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *channelViewHeightLC;

@property (weak, nonatomic) RechargeCollectionViewCell *selectedCell;
@end

@implementation RechargeViewController

- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadUserInfo];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.channelViewHeightLC.constant = 200 + kBottomSafeHeight;
    
    UITapGestureRecognizer *tapAlipay = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(alipayClick)];
    [self.alipayView addGestureRecognizer:tapAlipay];
    
    UITapGestureRecognizer *tapWxpay = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(wxPayClick)];
    [self.wxpayView addGestureRecognizer:tapWxpay];
    
    UITapGestureRecognizer *tapShadow = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shadowTap)];
    [self.shadowView addGestureRecognizer:tapShadow];
    
    self.goldCountLabel.text = [NSString stringWithFormat:@"%d",userManager.curUserInfo.gold];
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc]init];
    flow.scrollDirection = UICollectionViewScrollDirectionVertical;
    flow.itemSize = CGSizeMake((KScreenWidth - 15*2 - 18*2)/3, 72);
    flow.minimumLineSpacing = 13;
    flow.minimumInteritemSpacing = 18;
    flow.sectionInset = UIEdgeInsetsMake(0, 15, 15, 15);
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flow];
    self.collectionView.delegate   = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.containerView addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    [self.collectionView registerNib:[UINib nibWithNibName:@"RechargeCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"RechargeCollectionViewCell"];
    
    [self removeCollectionViewMJFooter];
    [self removeCollectionViewMJHeader];
    
    [self reqData];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(paySuccessNotification) name:KNotificationPaySuccess object:nil];
}

- (void)loadUserInfo{
    [CommonManager POST:@"User/getUserInfo" parameters:nil success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            userManager.curUserInfo = [UserInfoModel yy_modelWithDictionary:responseObject[@"data"]];
            [userManager saveUserInfo];
            self.goldCountLabel.text = [NSString stringWithFormat:@"%d",userManager.curUserInfo.gold];
        }
    } failure:^(NSError *error) {
        
    }];
}

- (void)reqData{
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"recharge/getPriceList" parameters:nil success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            self->datasource = responseObject[@"data"];
            [self.collectionView reloadData];
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)alipayClick{
    [self shadowTap];
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"recharge/getAliPayOrder" parameters:@{@"itemid":self.selectedCell.dict[@"id"]} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            [[AlipaySDK defaultService] payOrder:responseObject[@"data"][@"paystr"] fromScheme:@"nasilive" callback:^(NSDictionary *resultDic) {
                NSLog(@"1");
            }];
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)wxPayClick{
    [self shadowTap];
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"recharge/getWxPayOrder" parameters:@{@"itemid":self.selectedCell.dict[@"id"]} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            PayReq *request = [[PayReq alloc] init];
            request.partnerId = responseObject[@"data"][@"partnerid"];
            request.prepayId = responseObject[@"data"][@"prepayid"];
            request.package = @"Sign=WXPay";
            request.nonceStr = responseObject[@"data"][@"noncestr"];
            request.timeStamp = [responseObject[@"data"][@"timestamp"] intValue];
            request.sign = responseObject[@"data"][@"sign"];
            [WXApi sendReq: request completion:nil];
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)shadowTap{
    self.channelViewTopLC.constant = 0;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.shadowView.hidden = YES;
    }];
}

- (void)paySuccessNotification{
    //刷新用户信息
    [commonManager showSuccessAnimateInWindow];
    [self loadUserInfo];
}

- (IBAction)rechargeBtnClick:(UIButton *)sender {
    if (configManager.appConfig.switch_iap) {
        //监听购买结果
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        if([SKPaymentQueue canMakePayments]){
            [self requestProductData:currentGoodsid];
        }else{
            NSLog(@"不允许程序内付费");
        }
    }else{
        self.shadowView.hidden = NO;
        self.channelViewTopLC.constant = -(200 + kBottomSafeHeight);
        [UIView animateWithDuration:0.2f animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

#pragma mark - ———————————————————————— iOS内购开始 ————————————————————————————

//去苹果服务器请求商品
- (void)requestProductData:(NSString *)goodsid{
    [MBProgressHUD showActivityMessageInView:@""];
    NSArray *product = [[NSArray alloc] initWithObjects:goodsid,nil];
    NSSet *nsset = [NSSet setWithArray:product];
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:nsset];
    request.delegate = self;
    [request start];
}

//收到产品返回信息
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    dispatch_async(dispatch_get_main_queue(), ^{
    [MBProgressHUD hideHUD];
    });
    NSLog(@"--------------收到产品反馈消息---------------------");
    NSArray *product = response.products;
    NSLog(@"productID:%@", response.invalidProductIdentifiers);
    if (product.count == 0){
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showTipMessageInWindow:@"查找不到商品信息"];
        });
        return;
    }
    SKProduct *p = nil;
    for (SKProduct *pro in product) {
        NSLog(@"%@", [pro description]);
        NSLog(@"%@", [pro localizedTitle]);
        NSLog(@"%@", [pro localizedDescription]);
        NSLog(@"%@", [pro price]);
        NSLog(@"%@", [pro productIdentifier]);
        
        if([pro.productIdentifier isEqualToString: currentGoodsid]){
            p = pro;
        }
    }
    SKPayment *payment = [SKPayment paymentWithProduct:p];
    NSLog(@"发送购买请求");
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

//请求失败
- (void)request:(SKRequest *)request didFailWithError:(NSError *)error{
    
    [MBProgressHUD showTipMessageInView:@"支付失败"];
}

- (void)requestDidFinish:(SKRequest *)request{
    
}

//监听购买结果
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transaction{
    
    for(SKPaymentTransaction *tran in transaction){
        switch(tran.transactionState) {
            case SKPaymentTransactionStatePurchased:{
                [self verifyPurchaseWithPaymentTransaction:[NSNumber numberWithInt:tran.transactionState]];
                [[SKPaymentQueue defaultQueue] finishTransaction:tran];
            }
                break;
            case SKPaymentTransactionStatePurchasing:
                break;
            case SKPaymentTransactionStateRestored:{
                [[SKPaymentQueue defaultQueue] finishTransaction:tran];
            }
                break;
            case SKPaymentTransactionStateFailed:{
                [[SKPaymentQueue defaultQueue] finishTransaction:tran];
                
                [MBProgressHUD showTipMessageInView:@"购买失败"];
            }
                break;
            default:
                break;
        }
    }
}

//交易结束
- (void)completeTransaction:(SKPaymentTransaction *)transaction{
    NSLog(@"交易结束");
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
}

-(void)verifyPurchaseWithPaymentTransaction:(NSNumber*)resultState{
    //从沙盒中获取交易凭证并且拼接成请求体数据
//    NSURL *receiptUrl=[[NSBundle mainBundle] appStoreReceiptURL];
//    NSData *receiptData=[NSData dataWithContentsOfURL:receiptUrl];
}

// ———————————————— ↑↑↑↑↑↑↑ iOS内购结束 ↑↑↑↑↑↑↑ ——————————————————————————

- (IBAction)backBtnClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancelBtnClick:(UIButton *)sender {
}

#pragma mark ————————————————————————  delegate + datasource ————————————————————————
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return datasource.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RechargeCollectionViewCell *cell = (RechargeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"RechargeCollectionViewCell" forIndexPath:indexPath];
    cell.dict = datasource[indexPath.row];
    cell.cellSelected = indexPath.row == 0;
    if (indexPath.row == 0) {
        self.selectedCell = cell;
        currentGoodsid = [datasource[indexPath.row][@"price"] stringValue];
    }
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    RechargeCollectionViewCell *cell = (RechargeCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if (self.selectedCell) {
        self.selectedCell.cellSelected = NO;
    }
    cell.cellSelected = YES;
    self.selectedCell = cell;
    currentGoodsid = [datasource[indexPath.row][@"price"] stringValue];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
