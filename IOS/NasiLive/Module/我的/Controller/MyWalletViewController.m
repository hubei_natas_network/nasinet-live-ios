//
//  MyWalletViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/4/2.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MyWalletViewController.h"
#import "CashAccountViewController.h"
#import "WithdrawLogViewController.h"

#import "AuthStep1ViewController.h"

#import "UserWithdrawAccountModel.h"

@interface MyWalletViewController ()<UITextFieldDelegate>{
    int                                 _diamondCount;
    NSString                            *_money;
    
    UserWithdrawAccountModel            *_userAccountModel;
}

@property (weak, nonatomic) IBOutlet UILabel *totalCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UITextField *drawCountTextField;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *alipayAccountLabel;
@property (weak, nonatomic) IBOutlet UILabel *alipayNameLabel;

@property (weak, nonatomic) IBOutlet UIView *accountView;
@property (weak, nonatomic) IBOutlet UIView *unAuthView;
@property (weak, nonatomic) IBOutlet UIButton *manageAccountBtn;


@end

@implementation MyWalletViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    if (_userAccountModel) {
        self.alipayAccountLabel.text = self->_userAccountModel.alipay_account;
        self.alipayNameLabel.text = self->_userAccountModel.alipay_name;
    }else{
        [self loadUserAuthInfo];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的钱包";
    [self setIsShowLeftBack:YES];
    
    self.drawCountTextField.delegate = self;
    [self.drawCountTextField addTarget:self action:@selector(changedTextField) forControlEvents:UIControlEventEditingChanged];
    
    [self addNavigationItemWithTitles:@[@"提现记录"] isLeft:NO target:self action:@selector(rightClick:) tags:@[@1]];
    
    self.totalCountLabel.text = [NSString stringWithFormat:@"%d",userManager.curUserInfo.diamond_total];
    self.countLabel.text = [NSString stringWithFormat:@"%d",userManager.curUserInfo.diamond];
    
    if ([userManager curUserInfo].is_anchor) {
        self.unAuthView.hidden = YES;
        self.accountView.hidden = NO;
        self.manageAccountBtn.hidden = NO;
    }else{
        self.unAuthView.hidden = NO;
        self.accountView.hidden = YES;
        self.manageAccountBtn.hidden = YES;
    }
}

- (void)loadUserAuthInfo{
    [commonManager showLoadingAnimateInView:self.view];
    [CommonManager POST:@"withdraw/getAccount" parameters:nil success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            self->_userAccountModel = [UserWithdrawAccountModel yy_modelWithDictionary:responseObject[@"data"]];
            self.alipayAccountLabel.text = self->_userAccountModel.alipay_account;
            self.alipayNameLabel.text = self->_userAccountModel.alipay_name;
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *regex = @"[0-9]*";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    if ([pred evaluateWithObject:string]) {
        return YES;
    }
    return NO;
}

- (void)changedTextField{
    NSString *valueStr = self.drawCountTextField.text;
    if (valueStr.length == 0) {
        return;
    }
    _diamondCount = [valueStr intValue];
    if (_diamondCount > userManager.curUserInfo.diamond) {
        _diamondCount = userManager.curUserInfo.diamond;
        self.drawCountTextField.text = [NSString stringWithFormat:@"%d",_diamondCount];
    }
    
    _money = [NSString stringWithFormat:@"%.2f",_diamondCount * 1.f/configManager.appConfig.exchange_rate];
    self.moneyLabel.text = [NSString stringWithFormat:@"￥ %@",_money];
}

- (IBAction)withdrawBtnClick:(UIButton *)sender {
    if (!_userAccountModel) {
        [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"提醒" contentText:@"完成身份认证后才可提现" singleButton:@"立即认证" buttonClick:^{
            [self authBtnClick:nil];
        }];
        return;
    }
    if (_diamondCount < configManager.appConfig.withdraw_min) {
        [MBProgressHUD showTipMessageInView:[NSString stringWithFormat:@"单次提现最低%d元",configManager.appConfig.withdraw_min]];
        return;
    }
    NSDictionary *param = @{@"diamond":@(_diamondCount),
                                   @"cash":_money,
                                   @"alipay_account":_userAccountModel.alipay_account,
                                   @"alipay_name":_userAccountModel.alipay_name
    };
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"withdraw/withdraw" parameters:param success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            userManager.curUserInfo.diamond -= self->_diamondCount;
            [userManager saveUserInfo];
            [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"提现成功" contentText:responseObject[@"msg"] singleButton:@"知道了" buttonClick:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
        
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)rightClick:(UIButton *)sender{
    WithdrawLogViewController *vc = [[WithdrawLogViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)editAccountBtnClick:(UIButton *)sender {
    CashAccountViewController *vc = [[CashAccountViewController alloc]init];
    vc.accountModel = _userAccountModel;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)authBtnClick:(UIButton *)sender {
    AuthStep1ViewController *vc = [[AuthStep1ViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
