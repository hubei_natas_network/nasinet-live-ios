//
//  AnchorListViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/6/8.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"
#import "SearchVcProtocal.h"
#import <JXCategoryListContainerView.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, AnchorListType) {
    AnchorListTypeFans = 0,       //粉丝
    AnchorListTypeAttent,         //关注
    AnchorListTypeSearch
};

@interface AnchorListViewController : RootViewController<JXCategoryListContentViewDelegate,SearchVcProtocal>

@property (assign, nonatomic) AnchorListType listType;

@end

NS_ASSUME_NONNULL_END
