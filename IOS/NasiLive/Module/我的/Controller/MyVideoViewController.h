//
//  MyVideoViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/9/7.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"
#import "JXCategoryListContainerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyVideoViewController : RootViewController

@end

@interface MyVideoListViewController : RootViewController<JXCategoryListContentViewDelegate>

@property (assign, nonatomic) int status;

@property (nonatomic, copy) void(^itemClickBlock)(NSArray *videos, NSInteger index);
@property (nonatomic, copy) void (^SvAttentUserBlock)(int userid, BOOL attented);

@property (nonatomic, assign) NSInteger         selectedIndex;

- (void)refreshData;
- (void)updateUserModel:(int)userid attent:(BOOL)isattent;

@end

NS_ASSUME_NONNULL_END
