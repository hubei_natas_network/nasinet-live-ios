//
//  MineHomeViewController.m
//  Nasi
//
//  Created by yun on 2020/1/20.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MineHomeViewController.h"
#import "MessageHomeViewController.h"
#import "EditMineInfoViewController.h"
#import "AttentViewController.h"
#import "AuthStep1ViewController.h"
#import "H5ViewController.h"
#import "QNH5ViewController.h"

#import "ProfitViewController.h"
#import "MyWalletViewController.h"
#import "RechargeViewController.h"

#import "MineHomeHeaderView.h"
#import "MineTableViewCell.h"

@interface MineHomeViewController ()<UITableViewDelegate,UITableViewDataSource,MineHomeHeaderViewDelegate>{
    NSMutableArray *datasource;
    
    MineHomeHeaderView *headerView;
}

@end

@implementation MineHomeViewController

- (instancetype)init{
    if (self = [super init]) {
//        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    if (userManager.isLogined) {
        [self loadUserInfo];
        self.StatusBarStyle = UIStatusBarStyleDefault;
    }else{
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    
    [headerView refresh];
    
    [self createDataSource];
    
    
    [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationUpdateMessageNum object:nil userInfo:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 100;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.height = self.view.height - kTabBarHeight;
    [self removeTableMJFooter];
    [self removeTableMJHeader];
    
    headerView = [[MineHomeHeaderView alloc]init];
    headerView.delegate = self;
    self.tableView.tableHeaderView = headerView;
    self.tableView.tableHeaderView.frame = CGRectMake(0, 0, KScreenWidth, 400);
    
    [MineTableViewCell registerWithTableView:self.tableView];
}

- (void)createDataSource{
    
    if (configManager.appConfig.switch_iap) {
        datasource = [NSMutableArray arrayWithArray:@[
            @{@"icon":@"ic_mine_userlevel",@"title":@"用户等级",@"vc":@"UserLevelViewController"}
            ,@{@"icon":@"ic_mine_shop",@"title":@"我的小店",@"vc":@"",@"href":[NSString stringWithFormat:@"%@/h5/personal",[configManager appConfig].site_domain]}
            ,@{@"icon":@"ic_mine_moment",@"title":@"我的动态",@"vc":@"MyMomentViewController"}
            ,@{@"icon":@"ic_mine_video",@"title":@"我的短视频",@"vc":@"MyVideoViewController"}
            ,@{@"icon":@"ic_mine_download",@"title":@"影片下载",@"vc":@"DownloadViewController"}
            ,@{@"icon":@"ic_mine_collect",@"title":@"我的收藏",@"vc":@"CollectionViewController"}
            ,@{@"icon":@"ic_intinify",@"title":@"亲密榜",@"vc":@"IntimacyViewController"}
            ,@{@"icon":@"ic_mine_guild",@"title":@"主播公会",@"vc":@"",@"href":[NSString stringWithFormat:@"%@/h5/guild",[configManager appConfig].site_domain]}
            ,@{@"icon":@"ic_mine_coom",@"title":@"商务合作",@"vc":@""}
            ,@{@"icon":@"ic_setting",@"title":@"设置",@"vc":@"SettingViewController"}
        ]];
    }else{
        datasource = [NSMutableArray arrayWithArray:@[
            @{@"icon":@"ic_wallet",@"title":@"我的钱包",@"vc":@"MyWalletViewController"}
            ,@{@"icon":@"ic_mine_userlevel",@"title":@"用户等级",@"vc":@"UserLevelViewController"}
            ,@{@"icon":@"ic_mine_shop",@"title":@"我的小店",@"vc":@"",@"href":[NSString stringWithFormat:@"%@/h5/personal",[configManager appConfig].site_domain]}
            ,@{@"icon":@"ic_mine_moment",@"title":@"我的动态",@"vc":@"MyMomentViewController"}
            ,@{@"icon":@"ic_mine_video",@"title":@"我的短视频",@"vc":@"MyVideoViewController"}
            ,@{@"icon":@"ic_mine_download",@"title":@"影片下载",@"vc":@"DownloadViewController"}
            ,@{@"icon":@"ic_mine_collect",@"title":@"我的收藏",@"vc":@"CollectionViewController"}
            ,@{@"icon":@"ic_intinify",@"title":@"亲密榜",@"vc":@"IntimacyViewController"}
            ,@{@"icon":@"ic_profit",@"title":@"收益记录",@"vc":@"ProfitViewController"}
            ,@{@"icon":@"ic_mine_room",@"title":@"房间管理",@"vc":@"LiveRoomMgrViewController"}
            ,@{@"icon":@"ic_mine_guild",@"title":@"主播公会",@"vc":@"",@"href":[NSString stringWithFormat:@"%@/h5/guild",[configManager appConfig].site_domain]}
            ,@{@"icon":@"ic_mine_coom",@"title":@"商务合作",@"vc":@""}
            ,@{@"icon":@"ic_setting",@"title":@"设置",@"vc":@"SettingViewController"}
        ]];
    }
    
    if (userManager.curUserInfo.is_anchor) {
        [datasource insertObject:
         @{@"icon":@"ic_auth",@"title":@"直播记录",@"vc":@"LiveLogViewController"} atIndex:6];
    }else{
        [datasource insertObject:
         @{@"icon":@"ic_auth",@"title":@"认证主播",@"vc":@"AuthStep1ViewController"} atIndex:6];
    }
    [self.tableView reloadData];
}

- (void)loadUserInfo{
    [CommonManager POST:@"User/getUserInfo" parameters:nil success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            UserInfoModel *model = [UserInfoModel yy_modelWithDictionary:responseObject[@"data"]];
            userManager.curUserInfo = model;
            [userManager saveUserInfo];
            [self->headerView refresh];
        }
    } failure:^(NSError *error) {
        
    }];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    MineTableViewCell *cell = [MineTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.iconImgView.image = IMAGE_NAMED(datasource[indexPath.row][@"icon"]);
    cell.titLabel.text = datasource[indexPath.row][@"title"];
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return datasource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 54.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (!userManager.isLogined) {
        [kAppDelegate showLoginViewController];
        return;
    }
    NSString *vcNameStr = datasource[indexPath.row][@"vc"];
    NSString *href = datasource[indexPath.row][@"href"];
    if ([datasource[indexPath.row][@"title"] isEqualToString:@"申请成为主播"]) {
        AuthStep1ViewController *vc = [[AuthStep1ViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (vcNameStr.length > 0){
        UIViewController *vc = [[NSClassFromString(datasource[indexPath.row][@"vc"]) alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (href.length > 0){
        QNH5ViewController *vc = [[QNH5ViewController alloc]init];
        vc.rootHref = datasource[indexPath.row][@"href"];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

@end
