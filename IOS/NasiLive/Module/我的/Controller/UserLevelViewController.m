//
//  UserLevelViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/9/14.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "UserLevelViewController.h"

@interface UserLevelViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *userIconImgView;
@property (weak, nonatomic) IBOutlet UIImageView *userLevelImgView;
@property (weak, nonatomic) IBOutlet UILabel *userLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *nextLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointLabel;
@property (weak, nonatomic) IBOutlet UILabel *nextLevelPointLabel;
@property (weak, nonatomic) IBOutlet UIImageView *processBackImgView;
@property (weak, nonatomic) IBOutlet UIImageView *processFrontImgView;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *processWidthLC;

@end

@implementation UserLevelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.nextLevelPointLabel.hidden = YES;
    self.processBackImgView.hidden = YES;
    self.processFrontImgView.hidden = YES;
    
    UserInfoModel *model = [userManager curUserInfo];
    [self.userIconImgView sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    self.userLevelImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_user_level_%d",model.user_level]];
    self.nickNameLabel.text = model.nick_name;
    self.userLevelLabel.text = [NSString stringWithFormat:@"LV %d",model.user_level];
    self.pointLabel.text = [NSString stringWithFormat:@"经验值%d",model.point];
    
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"user/getuserlevelinfo" parameters:nil success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            NSDictionary *next = responseObject[@"data"][@"leve"];
            if (next) {
                self.nextLevelPointLabel.hidden = NO;
                self.processBackImgView.hidden = NO;
                self.processFrontImgView.hidden = NO;
                self.nextLevelLabel.text = [NSString stringWithFormat:@"LV %d",[next[@"level"] intValue]];
                self.nextLevelPointLabel.text = [NSString stringWithFormat:@"距离升级还差%d",[next[@"point"] intValue] - model.point];
                
                NSDictionary *level = responseObject[@"data"][@"level"];
                int levelPoint = [next[@"point"] intValue] - [level[@"point"] intValue];
                int point = model.point - [level[@"point"] intValue];
                self.processWidthLC.constant = 160 * (point*1.0/levelPoint);
            }
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
