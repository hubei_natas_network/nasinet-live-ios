//
//  AttentViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/6/8.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "AttentViewController.h"

#import "AnchorListViewController.h"

#import <JXCategoryView.h>

@interface AttentViewController ()<JXCategoryViewDelegate,JXCategoryListContainerViewDelegate>

@property (nonatomic, strong) NSArray *titles;

@property (nonatomic, strong) JXCategoryTitleView *categoryView;

@property (nonatomic, strong) JXCategoryListContainerView *listContainerView;

@end

@implementation AttentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titles = @[@"关注", @"粉丝"];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, kStatusBarHeight, 44, 44);
    [backBtn setImage:[UIImage imageNamed:@"ic_back"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    self.categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(44, kStatusBarHeight, KScreenWidth - 44, 40)];
    self.categoryView.delegate = self;
    self.categoryView.averageCellSpacingEnabled = NO;
    self.categoryView.titleLabelZoomEnabled = YES;
    self.categoryView.titleLabelZoomScale = 1.4;
    self.categoryView.cellWidthZoomEnabled = YES;
    self.categoryView.cellWidthZoomScale = 1.4;
    self.categoryView.titleLabelAnchorPointStyle = JXCategoryTitleLabelAnchorPointStyleBottom;
    self.categoryView.selectedAnimationEnabled = YES;
    self.categoryView.titleLabelZoomSelectedVerticalOffset = 1;
    self.categoryView.titleSelectedColor = [UIColor blackColor];
    self.categoryView.titleFont = [UIFont systemFontOfSize:14];
    self.categoryView.defaultSelectedIndex = self.isAttent ? 0:1;
    [self.view addSubview:self.categoryView];
    
    self.categoryView.titles = self.titles;
    self.categoryView.titleColorGradientEnabled = YES;
    
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorColor = MAIN_COLOR;
    lineView.indicatorWidth = 15;
    self.categoryView.indicators = @[lineView];
    
    self.listContainerView = [[JXCategoryListContainerView alloc] initWithType:JXCategoryListContainerType_ScrollView delegate:self];
    self.listContainerView.frame = CGRectMake(0, kTopHeight, KScreenWidth, KScreenHeight - kTopHeight);
    [self.view addSubview:self.listContainerView];
    //关联到categoryView
    self.categoryView.listContainer = self.listContainerView;
}

#pragma mark - JXCategoryViewDelegate

//点击选中或者滚动选中都会调用该方法。适用于只关心选中事件，不关心具体是点击还是滚动选中的。
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index{}

//点击选中的情况才会调用该方法
- (void)categoryView:(JXCategoryBaseView *)categoryView didClickSelectedItemAtIndex:(NSInteger)index{}

//滚动选中的情况才会调用该方法
- (void)categoryView:(JXCategoryBaseView *)categoryView didScrollSelectedItemAtIndex:(NSInteger)index{}

//正在滚动中的回调
- (void)categoryView:(JXCategoryBaseView *)categoryView scrollingFromLeftIndex:(NSInteger)leftIndex toRightIndex:(NSInteger)rightIndex ratio:(CGFloat)ratio{}

#pragma mark - JXCategoryListContainerViewDelegate

//返回列表的数量
- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count;
}
//根据下标index返回对应遵从`JXCategoryListContentViewDelegate`协议的列表实例
- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    AnchorListViewController *vc = [[AnchorListViewController alloc] init];
    vc.listType = index == 0 ? AnchorListTypeAttent:AnchorListTypeFans;
    return vc;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
