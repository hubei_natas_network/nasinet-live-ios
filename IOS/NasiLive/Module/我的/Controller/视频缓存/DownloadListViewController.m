//
//  DownloadListViewController.m
//  MiyouBBS
//
//  Created by yun11 on 2021/3/9.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "DownloadListViewController.h"
#import "VideoPlayViewController.h"
#import "DownloadListCell.h"

#import "HJDownloadManager.h"

@interface DownloadListViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray *_dataArray;
    NSMutableArray *_changeArray;
}
@property (strong, nonatomic) UIView            *editingView;
@property (strong, nonatomic) UIButton          *delBtn;
@property (strong, nonatomic) UIButton          *allBtn;

@end

@implementation DownloadListViewController

static NSString * const reuseIdentifier = @"DownloadListCell";

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self refreshList];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = CViewBgColor;
    _changeArray = [NSMutableArray array];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshList1) name:KNotificationDownloadComplate object:nil];
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.editingView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.bottom.equalTo(self.editingView.mas_top);
    }];
    
    [self.editingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.height.equalTo(@45);
        make.bottom.equalTo(self.view).offset(0);
    }];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.estimatedRowHeight = 100;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self removeTableMJFooter];
    [self removeTableMJHeader];
    
//    [DownloadListCell registerWithTableView:self.tableView];
    [self.tableView registerClass:[DownloadListCell class] forCellReuseIdentifier:@"DownloadListCell"];
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DownloadListCell *cell = [DownloadListCell cellWithTableView:self.tableView indexPath:indexPath];
    kWeakSelf(self);
    cell.sucessBlock = ^{
        [weakself refreshList];
    };
    cell.customSelectedBlock = ^ ( HJDownloadModel *model)
    {
        kStrongSelf(self)
        if (model.isSelect) {
            [self->_changeArray addObject:model];
            NSString *title = [NSString stringWithFormat:@"删除(%d)",self->_changeArray.count];
            [self.delBtn setTitle:title forState:UIControlStateNormal];
        }else
        {
            [self->_changeArray removeObject:model];
            if (self->_changeArray.count <= 0) {
                [self.delBtn setTitle:@"删除" forState:UIControlStateNormal];
            }
        }

    };
    cell.downloadModel = _dataArray[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [DownloadListCell cellHeight];
}

- (void)refreshList{
    _dataArray = [NSMutableArray array];
    NSMutableArray *modelsArray = [kHJDownloadManager downloadModels];
    if (modelsArray) {
        [modelsArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HJDownloadModel *model = obj;
            if (self.status == DownloadStatusComplete) {
                if (model.status == kHJDownloadStatus_Completed) {
                    [self->_dataArray addObject:model];
                }
            }else{
                if (model.status != kHJDownloadStatus_Completed) {
                    [self->_dataArray addObject:model];
                }
            }
        }];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self->_dataArray.count<= 0) {
            [self showNoDataImage];
        }else{
            [self removeNoDataImage];
            
        }
        [self.tableView reloadData];
        
       
    });
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.isEditing) {
       
        return;
    }
    HJDownloadModel *model = _dataArray[indexPath.row];
    if (model.status == kHJDownloadStatus_Failed || model.status == kHJDownloadStatus_Suspended || model.status == kHJDownloadStatus_Waiting) {
        [kHJDownloadManager resumeWithDownloadModel:model];
    }else if(model.status == kHJDownloadStatus_Completed){
        VideoPlayViewController *vc = [[VideoPlayViewController alloc]init];
        vc.model = model;
        vc.modalPresentationStyle = UIModalPresentationFullScreen;
        [self presentViewController:vc animated:NO completion:nil];
    }else{
        [kHJDownloadManager suspendWithDownloadModel:model];
    }
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return UITableViewCellEditingStyleDelete;
    return  UITableViewCellEditingStyleDelete & UITableViewCellEditingStyleInsert;
 ;

}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    __weak DownloadListCell *weakCell = [tableView cellForRowAtIndexPath:indexPath];
    
    UITableViewRowAction *action = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [tableView beginUpdates];
        
        HJDownloadModel *downloadModel = weakCell.downloadModel;
        [kHJDownloadManager stopWithDownloadModel:self->_dataArray[indexPath.row]];
        [self->_dataArray removeObject:downloadModel];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationBottom];
        
        [tableView endUpdates];
    }];
    
    return @[action];
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)refreshList1{
    NSLog(@"1");
    [self refreshList];
}
- (void)setIsEditing:(BOOL)isEditing{
    _isEditing = isEditing;
    
    if (isEditing) {
        if (self->_dataArray.count <= 0) return;
        self->_changeArray = [NSMutableArray array];
        [self.tableView setEditing:YES animated:YES];
        [self showEditingView:YES];
    }else{
       
        [self closeEditing];
    }
    
   
}

-(void)closeEditing
{
    [self.tableView setEditing:NO animated:YES];
    [self showEditingView:NO];
    //推出编辑模式 所有模型为选中状态
    self.allBtn.selected = YES ;
    [self selectAllbuttonClick:self.allBtn];
    if (_endEditing) {
        _endEditing(NO);
    }
    
}
- (void)showEditingView:(BOOL)isShow{
    [self.editingView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(isShow?-(45+kStatusBarHeight):0);
    }];
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}


-(void)deleteBtn{
    if ([self.tableView isEditing]) {
        [_dataArray removeObjectsInArray:_changeArray];
        [_changeArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            HJDownloadModel *  downloadModel =   _changeArray[idx];
            [kHJDownloadManager stopWithDownloadModel:downloadModel];
        }];
        
        [self.tableView reloadData];
    }
    [self closeEditing];
    
}
//全选按钮
-(void)selectAllbuttonClick:(UIButton*)btn
{
    btn.selected = !btn.selected ;
    [_dataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
      HJDownloadModel *  downloadModel =   _dataArray[idx];
        downloadModel.isSelect         =   btn.selected  ;
        
    }];
    //取消全选
    if (!btn.selected) {
        _changeArray = [NSMutableArray array];
        [self.delBtn setTitle:@"删除" forState:UIControlStateNormal];
    }else{
       
        NSString *title = [NSString stringWithFormat:@"删除(%lu)",(unsigned long)_dataArray.count];
        _changeArray = [_dataArray mutableCopy] ;
        [self.delBtn setTitle:title forState:UIControlStateNormal];
    }
    [self.tableView reloadData];
    

    
}


#pragma mark - JXCategoryListContentViewDelegate

- (UIView *)listView {
    return self.view;
}

- (UIView *)editingView{
    if (!_editingView) {
        _editingView = [[UIView alloc] init];
        _editingView.frame = CGRectMake(0, 0, kScreenWidth, 45);
        _editingView.backgroundColor = [UIColor whiteColor];
        UIView *line = [[UIView alloc]init];
        line.backgroundColor = [UIColor colorWithHexString:@"#E0E0E0"];
        [_editingView addSubview:line];
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_editingView);
            make.height.equalTo(@(0.5));
            make.width.equalTo(@(kScreenWidth));
        }];
        UIView *line1 = [[UIView alloc]init];
        line1.backgroundColor = [UIColor colorWithHexString:@"#E0E0E0"];
        [_editingView addSubview:line1];
        [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(_editingView);
            make.height.equalTo(@(28.5));
            make.width.equalTo(@(0.5));
        }];
        UIButton *delBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.delBtn = delBtn ;
        [delBtn setTitle:@"删除" forState:UIControlStateNormal];
        [delBtn setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
        [delBtn addTarget:self action:@selector(deleteBtn) forControlEvents:UIControlEventTouchUpInside];
        [_editingView addSubview:delBtn];
        [delBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.bottom.equalTo(_editingView);
            make.top.equalTo(_editingView).offset(1);
            make.width.equalTo(_editingView).multipliedBy(0.5).offset(-1);
        }];
//        
        UIButton *selBtn = [UIButton buttonWithType:UIButtonTypeCustom];
     
        [selBtn setTitle:@"全选"    forState:UIControlStateNormal];
        [selBtn setTitle:@"取消全选" forState:UIControlStateSelected];
        [selBtn setTitleColor: [UIColor blackColor] forState:UIControlStateNormal];
        [selBtn addTarget:self action:@selector(selectAllbuttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [_editingView addSubview:selBtn];
        self.allBtn = selBtn ;
        [selBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.equalTo(_editingView);
            make.top.equalTo(_editingView).offset(1);
            make.width.equalTo(_editingView).multipliedBy(0.5);
        }];
    }
    return _editingView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
