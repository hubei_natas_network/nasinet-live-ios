//
//  VideoPlayViewController.h
//  Avideo
//
//  Created by yun7_mac on 2019/3/13.
//  Copyright © 2019 XKX. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class HJDownloadModel;

@interface VideoPlayViewController : UIViewController

@property (strong, nonatomic) HJDownloadModel *model;

@end

NS_ASSUME_NONNULL_END
