//
//  VideoPlayViewController.m
//  Avideo
//
//  Created by yun7_mac on 2019/3/13.
//  Copyright © 2019 XKX. All rights reserved.
//

#import "VideoPlayViewController.h"

#import "SJVideoPlayer.h"

#import "HJDownloadModel.h"

@interface VideoPlayViewController (){
    SJVideoPlayer *_videoPlayer;
}

@end

@implementation VideoPlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor blackColor];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:backBtn];
    [backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(kStatusBarHeight);
        make.width.height.mas_equalTo(@44);
        make.left.offset(0);
    }];
    [backBtn setImage:[UIImage imageNamed:@"ic_back_white"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    _videoPlayer = [SJVideoPlayer lightweightPlayer];
    [self.view addSubview:_videoPlayer.view];
    [_videoPlayer.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.view);
        make.leading.trailing.offset(0);
        make.height.equalTo(self->_videoPlayer.view.mas_width).multipliedBy(9 / 16.0f);
    }];
    _videoPlayer.assetURL = [NSURL fileURLWithPath:self.model.destinationPath];
    _videoPlayer.URLAsset.title = self.model.fileName;
}

- (void)backClick{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
