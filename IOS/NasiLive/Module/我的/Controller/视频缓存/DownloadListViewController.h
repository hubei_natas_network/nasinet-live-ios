//
//  DownloadListViewController.h
//  MiyouBBS
//
//  Created by yun11 on 2021/3/9.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "RootViewController.h"
#import "JXCategoryListContainerView.h"

NS_ASSUME_NONNULL_BEGIN


typedef void(^endEditingBlock)( BOOL isEditing);

typedef NS_ENUM(NSInteger, DownloadStatus){
    DownloadStatusComplete = 0,         // 已完成
    DownloadStatusUnComplete            //未完成
};

@interface DownloadListViewController : RootViewController<JXCategoryListContentViewDelegate>

@property (nonatomic,assign) DownloadStatus status;

@property (nonatomic,assign) BOOL isEditing;

@property (nonatomic,copy)endEditingBlock endEditing ;

@end

NS_ASSUME_NONNULL_END
