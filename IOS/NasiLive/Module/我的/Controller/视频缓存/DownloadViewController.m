//
//  DownloadViewController.m
//  MiyouBBS
//
//  Created by yun11 on 2021/3/8.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "DownloadViewController.h"
#import "JXCategoryTitleView.h"
#import "JXCategoryView.h"

#import "DownloadListViewController.h"

@interface DownloadViewController ()<JXCategoryViewDelegate,JXCategoryListContainerViewDelegate>{
    
    DownloadListViewController      *_complateVc;
    DownloadListViewController      *_unComplateVc;
    
    BOOL                            _isInEditing;
}

@property (nonatomic, strong) NSArray <NSString *>          *titles;

@property (nonatomic, strong) JXCategoryTitleView           *categoryView;
@property (strong, nonatomic) JXCategoryListContainerView   *listContainerView;

@end

@implementation DownloadViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //处于第一个item的时候，才允许屏幕边缘手势返回
    self.navigationController.interactivePopGestureRecognizer.enabled = (self.categoryView.selectedIndex == 0);
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //离开页面的时候，需要恢复屏幕边缘手势，不能影响其他页面
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我的缓存";
    
    [self addNavigationItemWithTitles:@[@"编辑"] isLeft:NO target:self action:@selector(editBtnClick:) tags:@[@(111)]];
    
    _titles = @[@"已完成", @"缓存中"];
    
    _categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30)];
    self.categoryView.titles = self.titles;
    self.categoryView.backgroundColor = [UIColor clearColor];
    self.categoryView.delegate = self;
    self.categoryView.titleFont = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
    self.categoryView.titleSelectedColor = MAIN_COLOR;
    self.categoryView.titleColor = [UIColor colorWithHexString:@"#83796A"];
    self.categoryView.titleColorGradientEnabled = YES;
    self.categoryView.titleLabelZoomEnabled = NO;
    self.categoryView.contentScrollViewClickTransitionAnimationEnabled = NO;
    
    self.listContainerView = [[JXCategoryListContainerView alloc] initWithType:JXCategoryListContainerType_ScrollView delegate:self];
    self.listContainerView.frame = CGRectMake(0, self.categoryView.mj_maxY, KScreenWidth, KScreenHeight - self.categoryView.mj_maxY);
    self.listContainerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.listContainerView];
    
    self.categoryView.listContainer = self.listContainerView;
    [self.view addSubview:self.categoryView];
    
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorColor = MAIN_COLOR;
    lineView.indicatorWidth = 12;
    lineView.indicatorHeight = 3;
    lineView.indicatorCornerRadius = 1.5;
    self.categoryView.indicators = @[lineView];
}

- (void)editBtnClick:(UIButton *)sender{
    _isInEditing = !_isInEditing;
    [sender setTitle:_isInEditing ? @"完成":@"编辑" forState:UIControlStateNormal];
    _complateVc.isEditing = _isInEditing;
    _complateVc.endEditing = ^(BOOL isEditing) {
        [sender setTitle:isEditing ? @"完成":@"编辑" forState:UIControlStateNormal];
    };
    _unComplateVc.isEditing = _isInEditing;
    _unComplateVc.endEditing = ^(BOOL isEditing) {
        [sender setTitle:isEditing ? @"完成":@"编辑" forState:UIControlStateNormal];
    };
}


#pragma mark - JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didScrollSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

#pragma mark - JXCategoryListContainerViewDelegate

- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index{
    DownloadListViewController *vc = [[DownloadListViewController alloc]init];
    vc.status = index;
    if (index == 0) {
        _complateVc = vc;
    }else{
        _unComplateVc = vc;
    }
    return vc;
}

- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
