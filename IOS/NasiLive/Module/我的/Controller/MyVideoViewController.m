//
//  MyVideoViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/9/7.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MyVideoViewController.h"

#import "SVScaleVideoView.h"

#import "JXCategoryTitleView.h"
#import "JXCategoryView.h"
#import <IQKeyboardManager.h>

@interface MyVideoViewController ()<JXCategoryViewDelegate,JXCategoryListContainerViewDelegate,SVVideoViewDelegate,SVScaleVideoViewProtocol>

@property (nonatomic, strong) NSArray <NSString *>          *titles;

@property (nonatomic, strong) JXCategoryTitleView           *categoryView;
@property (strong, nonatomic) JXCategoryListContainerView   *listContainerView;
@property (nonatomic, weak) SVScaleVideoView                *scaleView;

@end

@implementation MyVideoViewController
@synthesize currentListVC;

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [IQKeyboardManager sharedManager].enable = NO;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    
    if (self.scaleView) {
        [self.view bringSubviewToFront:self.scaleView];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.scaleView.videoView resume];
    
    //处于第一个item的时候，才允许屏幕边缘手势返回
    self.navigationController.interactivePopGestureRecognizer.enabled = (self.categoryView.selectedIndex == 0);
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.scaleView.videoView pause];
    
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    
    //离开页面的时候，需要恢复屏幕边缘手势，不能影响其他页面
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

- (void)dealloc {
    [self.scaleView.videoView destoryPlayer];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的短视频";
    
    _titles = @[@"已审核", @"待审核", @"未通过"];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, kStatusBarHeight, 44, 44);
    [backBtn setImage:IMAGE_NAMED(@"ic_back") forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    _categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(44, kStatusBarHeight, [UIScreen mainScreen].bounds.size.width - 88, 44)];
    self.categoryView.titles = self.titles;
    self.categoryView.backgroundColor = [UIColor clearColor];
    self.categoryView.delegate = self;
    self.categoryView.titleFont = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
    self.categoryView.titleSelectedColor = MAIN_COLOR;
    self.categoryView.titleColor = [UIColor colorWithHexString:@"#83796A"];
    self.categoryView.titleColorGradientEnabled = YES;
    self.categoryView.titleLabelZoomEnabled = NO;
    self.categoryView.contentScrollViewClickTransitionAnimationEnabled = NO;
    
    self.listContainerView = [[JXCategoryListContainerView alloc] initWithType:JXCategoryListContainerType_ScrollView delegate:self];
    self.listContainerView.frame = CGRectMake(0, self.categoryView.mj_maxY, KScreenWidth, KScreenHeight - self.categoryView.mj_maxY);
    self.listContainerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.listContainerView];
    
    self.categoryView.listContainer = self.listContainerView;
    [self.view addSubview:self.categoryView];
    
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorColor = MAIN_COLOR;
    lineView.indicatorWidth = 12;
    lineView.indicatorHeight = 3;
    lineView.indicatorCornerRadius = 1.5;
    self.categoryView.indicators = @[lineView];
}

#pragma mark - JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
    
    self.currentListVC = (SVideoListViewController *)self.listContainerView.validListDict[@(index)];
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didScrollSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

#pragma mark - JXCategoryListContainerViewDelegate

- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index{
    MyVideoListViewController *vc = [[MyVideoListViewController alloc]init];
    kWeakSelf(vc)
    @weakify(self);
    vc.itemClickBlock = ^(NSArray * _Nonnull videos, NSInteger index) {
        @strongify(self);
        [self showVideoVCWithVideos:videos datasourceVC:weakvc index:index];
    };
    switch (index) {
        case 0:
            vc.status = 1;
            break;
        case 1:
            vc.status =0;
            break;
        case 2:
            vc.status = 2;
            break;
        default:
            break;
    }
    return vc;
}

- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count;
}

- (void)showVideoVCWithVideos:(NSArray *)videos datasourceVC:(SVideoListViewController *)vc index:(NSInteger)index {
    SVScaleVideoView *scaleView = [[SVScaleVideoView alloc] initWithVC:self datasourceVC:vc videos:videos index:index];
    scaleView.videoView.delegate = self;
    [scaleView show];
    
    self.scaleView = scaleView;
}

@end

#import "SVideoListViewController.h"
#import "GKBallLoadingView.h"

#import "SVideoListCollectionViewCell.h"
#import "ShortVideoModel.h"

#define pagesize 20

@interface MyVideoListViewController ()<UIViewControllerTransitioningDelegate,UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UIView            *loadingBgView;

@property (nonatomic, strong) NSMutableArray    *videos;

@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@property (nonatomic, assign) BOOL              isRefresh;

//用户索引
@property (strong, nonatomic) NSMutableDictionary       *userIndexes;

@end

@implementation MyVideoListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGFloat width = (KScreenWidth - 2) / 3;
    CGFloat height = width * 167 / 124;
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    layout.itemSize = CGSizeMake(width, height);
    layout.minimumLineSpacing = 1.0f;
    layout.minimumInteritemSpacing = 1.0f;
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.alwaysBounceVertical = YES;
    [self.collectionView registerNib:[UINib nibWithNibName:@"SVideoListCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"SVideoListCollectionViewCell"];
    [self.view addSubview:self.collectionView];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [self.view addSubview:self.loadingBgView];
    self.loadingBgView.frame = CGRectMake(0, 0, KScreenWidth, 200.f);
    
    [self refreshData];
    
}

- (void)viewDidLayoutSubviews{
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

- (void)refreshData {
    if (self.isRefresh) return;
    
    // 数据加载
    GKBallLoadingView *loadingView = [GKBallLoadingView loadingViewInView:self.loadingBgView];
    [loadingView startLoading];
    
    [CommonManager POST:@"user/myvideo" parameters:@{@"status":@(self.status),@"page":@(1),@"size":@(pagesize)} success:^(id responseObject) {
        [loadingView stopLoading];
        [loadingView removeFromSuperview];
        [self.loadingBgView removeFromSuperview];
        if (RESP_SUCCESS(responseObject)) {
            NSArray *dataArr = [NSArray yy_modelArrayWithClass:[ShortVideoModel class] json:responseObject[@"data"]];
            if (dataArr.count < pagesize) {
                [self.collectionView.mj_footer removeFromSuperview];
                self.collectionView.mj_footer = nil;
            }else{
                self.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
            }
            [self.videos removeAllObjects];
            [self.videos addObjectsFromArray:dataArr];
            
            //为视频创建用户索引
            [self.userIndexes removeAllObjects];
            for (int i = 0; i < dataArr.count; i++) {
                ShortVideoModel *model = dataArr[i];
                if ([[self.userIndexes allKeys] containsObject:@(model.author.userid)]) {
                    NSMutableArray *array = self.userIndexes[@(model.author.userid)];
                    [array addObject:@(i)];
                }else{
                    NSMutableArray *array = [NSMutableArray arrayWithObject:@(i)];
                    self.userIndexes[@(model.author.userid)] = array;
                }
            }
            
            [self.collectionView reloadData];
            self.isRefresh = YES;
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
        
    } failure:^(NSError *error) {
        [loadingView stopLoading];
        [loadingView removeFromSuperview];
        RESP_FAILURE;
    }];
}

- (void)loadMore{
    int page = ceil((self.videos.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    [CommonManager POST:@"user/myvideo" parameters:@{@"status":@(self.status),@"page":@(page),@"size":@(pagesize)} success:^(id responseObject) {
        [self.collectionView.mj_footer endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            NSArray *dataArr = [NSArray yy_modelArrayWithClass:[ShortVideoModel class] json:responseObject[@"data"]];
            if (dataArr.count < pagesize) {
                [self.collectionView.mj_footer removeFromSuperview];
                self.collectionView.mj_footer = nil;
            }else{
                self.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
            }
            [self.videos addObjectsFromArray:dataArr];
            //为视频创建用户索引
            [self.userIndexes removeAllObjects];
            for (int i = 0; i < dataArr.count; i++) {
                ShortVideoModel *model = dataArr[i];
                if ([[self.userIndexes allKeys] containsObject:@(model.author.userid)]) {
                    NSMutableArray *array = self.userIndexes[@(model.author.userid)];
                    [array addObject:@(i)];
                }else{
                    NSMutableArray *array = [NSMutableArray arrayWithObject:@(i)];
                    self.userIndexes[@(model.author.userid)] = array;
                }
            }
            
            [self.collectionView reloadData];
            self.isRefresh = YES;
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
        
    } failure:^(NSError *error) {
        [self.collectionView.mj_footer endRefreshing];
        RESP_FAILURE;
    }];
}

- (void)refreshMoreListWithSuccess:(void (^)(NSArray * _Nonnull))success failure:(void (^)(NSError * _Nonnull))failure {
    if (self.videos.count < pagesize) {
        success(nil);
        return;
    }
    int page = ceil((self.videos.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    [CommonManager POST:@"user/myvideo" parameters:@{@"status":@(self.status),@"page":@(page),@"size":@(pagesize)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            NSArray *dataArr = [NSArray yy_modelArrayWithClass:[ShortVideoModel class] json:responseObject[@"data"]];
            if (dataArr.count < pagesize) {
                [self.collectionView.mj_footer removeFromSuperview];
                self.collectionView.mj_footer = nil;
            }else{
                self.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
            }
            [self.videos addObjectsFromArray:dataArr];
            
            //为视频创建用户索引
            [self.userIndexes removeAllObjects];
            for (int i = 0; i < dataArr.count; i++) {
                ShortVideoModel *model = dataArr[i];
                if ([[self.userIndexes allKeys] containsObject:@(model.author.userid)]) {
                    NSMutableArray *array = self.userIndexes[@(model.author.userid)];
                    [array addObject:@(i)];
                }else{
                    NSMutableArray *array = [NSMutableArray arrayWithObject:@(i)];
                    self.userIndexes[@(model.author.userid)] = array;
                }
            }
            
            [self.collectionView reloadData];
            success(dataArr);
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
        
    } failure:^(NSError *error) {
        RESP_FAILURE;
        failure(error);
    }];
}

- (void)updateUserModel:(int)userid attent:(BOOL)isattent{
    if (![self.userIndexes containsObjectForKey:@(userid)]) {
        return;
    }
    NSMutableArray *array = self.userIndexes[@(userid)];
    for (NSNumber *index in array) {
        ShortVideoModel *model = self.videos[[index intValue]];
        model.author.isattent = isattent;
    }
}

#pragma mark - <UICollectionViewDataSource, UICollectionViewDelegate>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.videos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SVideoListCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SVideoListCollectionViewCell" forIndexPath:indexPath];
    cell.model = self.videos[indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedIndex = indexPath.item;
    !self.itemClickBlock ? : self.itemClickBlock(self.videos, indexPath.item);
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ? : self.scrollCallback(scrollView);
}

#pragma mark - GKPageListViewDelegate
- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.collectionView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}

#pragma mark - 懒加载
- (UIView *)loadingBgView {
    if (!_loadingBgView) {
        _loadingBgView = [UIView new];
    }
    return _loadingBgView;
}

- (NSMutableArray *)videos {
    if (!_videos) {
        _videos = [NSMutableArray new];
    }
    return _videos;
}

- (NSMutableDictionary *)userIndexes{
    if (!_userIndexes) {
        _userIndexes = [NSMutableDictionary dictionary];
    }
    return _userIndexes;
}




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
