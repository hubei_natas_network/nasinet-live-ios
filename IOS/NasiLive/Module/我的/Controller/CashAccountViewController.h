//
//  CashAccountViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/4/2.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

#import "UserWithdrawAccountModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface CashAccountViewController : RootViewController

@property (strong, nonatomic) UserWithdrawAccountModel *accountModel;

@end

NS_ASSUME_NONNULL_END
