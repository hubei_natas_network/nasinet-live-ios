//
//  AgentModel.h
//  NasiLive
//
//  Created by yun11 on 2020/7/2.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AgentModel : NSObject

@property (assign, nonatomic) int uid;
@property (assign, nonatomic) CGFloat profit;
@property (assign, nonatomic) CGFloat total_profit;
@property (assign, nonatomic) int total_invite;
@property (assign, nonatomic) int today_invite;
@property (copy, nonatomic) NSString *invite_code;
@property (copy, nonatomic) NSString *invite_url;

@end

NS_ASSUME_NONNULL_END
