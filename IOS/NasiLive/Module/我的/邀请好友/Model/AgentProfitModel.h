//
//  AgentProfitModel.h
//  NasiLive
//
//  Created by yun11 on 2020/7/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AgentProfitModel : NSObject

@property (assign, nonatomic) int profitid;
@property (assign, nonatomic) CGFloat profit;
@property (copy, nonatomic) NSString *content;
@property (copy, nonatomic) NSString *create_time;

@end

NS_ASSUME_NONNULL_END
