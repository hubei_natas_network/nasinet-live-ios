//
//  AgentWithdrawViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/7/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "AgentWithdrawViewController.h"

#import "CashAccountViewController.h"
#import "AgentWithdrawLogViewController.h"

#import "UserWithdrawAccountModel.h"
#import "AgentModel.h"

@interface AgentWithdrawViewController ()<UITextFieldDelegate>{
    UserWithdrawAccountModel            *_userAccountModel;
}

@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UITextField *drawTextField;
@property (weak, nonatomic) IBOutlet UILabel *alipayAccountLabel;
@property (weak, nonatomic) IBOutlet UILabel *alipayNameLabel;

@end

@implementation AgentWithdrawViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    if (_userAccountModel) {
        self.alipayAccountLabel.text = self->_userAccountModel.alipay_account;
        self.alipayNameLabel.text = self->_userAccountModel.alipay_name;
    }else{
        [self loadUserAuthInfo];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的钱包";
    [self setIsShowLeftBack:YES];
    
    self.drawTextField.delegate = self;
    
    self.totalLabel.text = [NSString stringWithFormat:@"%.2f",self.agentModel.profit];
}

- (void)loadUserAuthInfo{
    [commonManager showLoadingAnimateInView:self.view];
    [CommonManager POST:@"withdraw/getAccount" parameters:nil success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            self->_userAccountModel = [UserWithdrawAccountModel yy_modelWithDictionary:responseObject[@"data"]];
            self.alipayAccountLabel.text = self->_userAccountModel.alipay_account;
            self.alipayNameLabel.text = self->_userAccountModel.alipay_name;
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString *regex = @"[0-9]*";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    if ([pred evaluateWithObject:string]) {
        return YES;
    }
    return NO;
}

- (IBAction)withdrawClick:(UIButton *)sender {
    if (self.drawTextField.text.length == 0) {
        return;
    }
    [self.view endEditing:YES];
    
    CGFloat cash = [self.drawTextField.text floatValue];
    if (cash < configManager.appConfig.withdraw_min) {
        [MBProgressHUD showTipMessageInView:[NSString stringWithFormat:@"单次提现最低%d元",configManager.appConfig.withdraw_min]];
        return;
    }
    NSDictionary *param = @{@"cash":[NSString stringWithFormat:@"%.2f",cash],
                            @"alipay_account":_userAccountModel.alipay_account,
                            @"alipay_name":_userAccountModel.alipay_name
    };
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"agent/withdraw" parameters:param success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            self.agentModel.profit = [responseObject[@"data"][@"profit"] floatValue];
            [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"提现成功" contentText:responseObject[@"msg"] singleButton:@"知道了" buttonClick:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
        
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (IBAction)manageAccountBtnClick:(UIButton *)sender {
    CashAccountViewController *vc = [[CashAccountViewController alloc]init];
    vc.accountModel = _userAccountModel;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)withdrawLogBtnClick:(UIButton *)sender {
    [self.navigationController pushViewController:[[AgentWithdrawLogViewController alloc]init] animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
