//
//  InviteInfoViewController.m
//  MiyouBBS
//
//  Created by yun11 on 2021/3/12.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "InviteInfoViewController.h"

#import "InviteProfitViewController.h"
#import "InviteLogViewController.h"
#import "InviteAwardViewController.h"

#import "JXCategoryTitleView.h"
#import "JXCategoryView.h"

@interface InviteInfoViewController ()<JXCategoryViewDelegate,JXCategoryListContainerViewDelegate>

@property (nonatomic, strong) NSArray <NSString *>          *titles;

@property (nonatomic, strong) JXCategoryTitleView           *categoryView;
@property (strong, nonatomic) JXCategoryListContainerView   *listContainerView;

@property (strong, nonatomic) UILabel *totalProfitLabel;
@property (strong, nonatomic) UILabel *totalCountLabel;
@property (strong, nonatomic) UILabel *todayCountLabel;

@end

@implementation InviteInfoViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:NO animated:YES];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //处于第一个item的时候，才允许屏幕边缘手势返回
    self.navigationController.interactivePopGestureRecognizer.enabled = (self.categoryView.selectedIndex == 0);
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //离开页面的时候，需要恢复屏幕边缘手势，不能影响其他页面
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的邀请";
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(15, 0, KScreenWidth - 30, 120)];
    [self.view addSubview:headerView];
    UIImageView *headerBackImgView = [[UIImageView alloc]initWithFrame:headerView.bounds];
    headerBackImgView.image =IMAGE_NAMED(@"bg_invite_log");
    headerBackImgView.contentMode = UIViewContentModeScaleToFill;
    [headerView addSubview:headerBackImgView];
    
    CGFloat headerItemW = headerView.width/3;
    self.totalProfitLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 36, headerItemW, 20)];
    self.totalProfitLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
    self.totalProfitLabel.textAlignment = NSTextAlignmentCenter;
    self.totalProfitLabel.textColor = [UIColor whiteColor];
    self.totalProfitLabel.text = @(self.agentModel.total_profit).stringValue;
    [headerView addSubview:self.totalProfitLabel];
    
    UILabel *totalProfitTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.totalProfitLabel.mj_maxY + 15, headerItemW, 20)];
    totalProfitTitleLabel.font = [UIFont systemFontOfSize:14];
    totalProfitTitleLabel.textAlignment = NSTextAlignmentCenter;
    totalProfitTitleLabel.textColor = [UIColor whiteColor];
    totalProfitTitleLabel.text = @"累计收益";
    [headerView addSubview:totalProfitTitleLabel];
    
    self.totalCountLabel = [[UILabel alloc]initWithFrame:CGRectMake(headerItemW, 36, headerItemW, 20)];
    self.totalCountLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
    self.totalCountLabel.textAlignment = NSTextAlignmentCenter;
    self.totalCountLabel.textColor = [UIColor whiteColor];
    self.totalCountLabel.text = @(self.agentModel.total_invite).stringValue;
    [headerView addSubview:self.totalCountLabel];
    
    UILabel *totalTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(headerItemW, self.totalCountLabel.mj_maxY + 15, headerItemW, 20)];
    totalTitleLabel.font = [UIFont systemFontOfSize:14];
    totalTitleLabel.textAlignment = NSTextAlignmentCenter;
    totalTitleLabel.textColor = [UIColor whiteColor];
    totalTitleLabel.text = @"累计邀请";
    [headerView addSubview:totalTitleLabel];
    
    self.todayCountLabel = [[UILabel alloc]initWithFrame:CGRectMake(headerItemW*2, 36, headerItemW, 20)];
    self.todayCountLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
    self.todayCountLabel.textAlignment = NSTextAlignmentCenter;
    self.todayCountLabel.textColor = [UIColor whiteColor];
    self.todayCountLabel.text = @(self.agentModel.today_invite).stringValue;
    [headerView addSubview:self.todayCountLabel];
    
    UILabel *todayTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(headerItemW*2, self.totalCountLabel.mj_maxY + 15, headerItemW, 20)];
    todayTitleLabel.font = [UIFont systemFontOfSize:14];
    todayTitleLabel.textAlignment = NSTextAlignmentCenter;
    todayTitleLabel.textColor = [UIColor whiteColor];
    todayTitleLabel.text = @"今日邀请";
    [headerView addSubview:todayTitleLabel];
    
    
    _titles = @[@"邀请奖励", @"充值奖励", @"邀请记录"];
    
    _categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, headerView.height + 5, [UIScreen mainScreen].bounds.size.width, 40)];
    self.categoryView.titles = self.titles;
    self.categoryView.backgroundColor = [UIColor clearColor];
    self.categoryView.delegate = self;
    self.categoryView.titleFont = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
    self.categoryView.titleSelectedColor = MAIN_COLOR;
    self.categoryView.titleColor = [UIColor colorWithHexString:@"#83796A"];
    self.categoryView.titleColorGradientEnabled = YES;
    self.categoryView.titleLabelZoomEnabled = NO;
    self.categoryView.contentScrollViewClickTransitionAnimationEnabled = NO;
    
    self.listContainerView = [[JXCategoryListContainerView alloc] initWithType:JXCategoryListContainerType_ScrollView delegate:self];
    self.listContainerView.frame = CGRectMake(0, self.categoryView.mj_maxY, KScreenWidth, KScreenHeight - self.categoryView.mj_maxY);
    self.listContainerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.listContainerView];
    
    self.categoryView.listContainer = self.listContainerView;
    [self.view addSubview:self.categoryView];
}

#pragma mark - JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didScrollSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

#pragma mark - JXCategoryListContainerViewDelegate

- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index{
    switch (index) {
        case 0:{
            InviteAwardViewController *vc = [[InviteAwardViewController alloc]init];
            return vc;
        }
            break;
        case 1:{
            InviteProfitViewController *vc = [[InviteProfitViewController alloc]init];
            return vc;
        }
            break;
        case 2:{
            InviteLogViewController *vc = [[InviteLogViewController alloc]init];
            return vc;
        }
            break;
        default:
            return nil;
            break;
    }
}

- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count;
}

@end
