//
//  InviteViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/7/2.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "InviteViewController.h"

#import "InviteInfoViewController.h"
#import "AgentWithdrawViewController.h"

#import "AgentModel.h"

#import <ShareSDK/ShareSDK.h>

@interface InviteViewController ()

@property (weak, nonatomic) IBOutlet UILabel *titleTipLabel;
@property (weak, nonatomic) IBOutlet UILabel *profitLabel;
@property (weak, nonatomic) IBOutlet UILabel *rateLabel;
@property (weak, nonatomic) IBOutlet UILabel *inviteCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *totleProfitLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalDurationLabel;
@property (weak, nonatomic) IBOutlet UILabel *inviteCodeLabel;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sharePannelTopLC;

@property (strong, nonatomic) AgentModel *agentModel;


@end

@implementation InviteViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    if (self.agentModel) {
        self.profitLabel.text = [NSString stringWithFormat:@"%.2f",self.agentModel.profit];
    }
}
- (IBAction)backClicl:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"邀请好友";
    [self setIsShowLeftBack:YES];
    
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"agent/getAgentInfo" parameters:nil success:^(id responseObject) {
        [commonManager hideAnimateHud];
        self.agentModel = [AgentModel yy_modelWithDictionary:responseObject[@"data"]];
        self.inviteCodeLabel.text = self.agentModel.invite_code;
        self.titleTipLabel.text = [NSString stringWithFormat:@"每邀请一位新用户注册即可获得3天VIP，好友充值还可返%d%%",configManager.appConfig.agent_ratio];
        self.profitLabel.text = [NSString stringWithFormat:@"%.2f",self.agentModel.profit];
        self.rateLabel.text = [NSString stringWithFormat:@"%d%%",configManager.appConfig.agent_ratio];
        self.inviteCountLabel.text = @(self.agentModel.total_invite).stringValue;
        self.totleProfitLabel.text = @(self.agentModel.total_profit).stringValue;
        self.totalDurationLabel.text = @(self.agentModel.total_invite * 3).stringValue;
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
    
    [self.shadowView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shadowTap)]];
}

- (void)shadowTap{
    self.sharePannelTopLC.constant = 0;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.shadowView.hidden = YES;
    }];
}

- (IBAction)inviteClick:(UIButton *)sender {
    self.shadowView.hidden = NO;
    self.sharePannelTopLC.constant = - 140 - kBottomSafeHeight;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)withdrawClick:(UIButton *)sender {
    if (configManager.appConfig.switch_iap) {
        return;
    }
    AgentWithdrawViewController *vc = [[AgentWithdrawViewController alloc]init];
    vc.agentModel = self.agentModel;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)profitLogClick:(UIButton *)sender {
    InviteInfoViewController *vc = [[InviteInfoViewController alloc]init];
    vc.agentModel = self.agentModel;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)copyCodeBtnClick:(UIButton *)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.agentModel.invite_code;
    [MBProgressHUD showTipMessageInView:@"邀请码已复制到剪贴板"];
}


- (IBAction)ShareBtnClick:(UIButton *)sender {
    NSDictionary *channel = @{@(1):@(SSDKPlatformSubTypeWechatSession)
                              ,@(2):@(SSDKPlatformSubTypeWechatTimeline)
                              ,@(3):@(SSDKPlatformSubTypeQQFriend)
                              ,@(4):@(SSDKPlatformTypeSinaWeibo)
    };
    if (sender.tag == 5) {
        NSString *shareText = @"邀您下载超好看的直播APP，颜值主播等你来撩";
        UIImage *shareImage = [UIImage imageNamed:@"appicon"];
        NSURL *shareURL = [NSURL URLWithString:self.agentModel.invite_url];
        NSArray *activityItems = [[NSArray alloc] initWithObjects:shareText, shareImage, shareURL, nil];
        
        UIActivityViewController *vc = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        
        UIActivityViewControllerCompletionWithItemsHandler myBlock = ^(UIActivityType activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
            NSLog(@"%@",activityType);
            if (completed) {
                [MBProgressHUD showTipMessageInView:@"分享成功"];
            } else {
                [MBProgressHUD showTipMessageInView:@"分享失败"];
            }
            [vc dismissViewControllerAnimated:YES completion:nil];
        };
        
        vc.completionWithItemsHandler = myBlock;
        
        [self presentViewController:vc animated:YES completion:nil];
    }else if (sender.tag == 6){
        [self shadowTap];
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = self.agentModel.invite_url;
        [MBProgressHUD showTipMessageInView:@"邀请链接已复制到剪贴板"];
    }else{
        SSDKPlatformType type = [channel[@(sender.tag)] intValue];
        //统一创建分享参数
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:@"邀您下载超好看的直播APP，颜值主播等你来撩"
                                         images:[UIImage imageNamed:@"appicon"]
                                            url:[NSURL URLWithString:self.agentModel.invite_url]
                                          title:@"香播播-颜值主播等你来撩"
                                           type:SSDKContentTypeAuto];
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            [self shadowTap];
            switch (state) {
                case SSDKResponseStateSuccess:
                    [MBProgressHUD showTipMessageInView:@"分享成功"];
                    break;
                case SSDKResponseStateFail:
                {
                    [MBProgressHUD showTipMessageInView:@"分享失败"];
                    //失败
                    break;
                }
                case SSDKResponseStateCancel:
                    //取消
                    break;
                    
                default:
                    break;
            }
        }];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
