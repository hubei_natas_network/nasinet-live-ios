//
//  AgentWithdrawViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/7/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

@class AgentModel;

NS_ASSUME_NONNULL_BEGIN

@interface AgentWithdrawViewController : RootViewController

@property (strong, nonatomic) AgentModel *agentModel;

@end

NS_ASSUME_NONNULL_END
