//
//  InviteAwardViewController.h
//  MiyouBBS
//
//  Created by yun11 on 2021/3/12.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "RootViewController.h"
#import "JXCategoryListContainerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface InviteAwardViewController : RootViewController<JXCategoryListContentViewDelegate>

@end

NS_ASSUME_NONNULL_END
