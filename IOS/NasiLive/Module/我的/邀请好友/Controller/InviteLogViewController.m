//
//  InviteLogViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/7/2.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "InviteLogViewController.h"

#import "InviteUserTableViewCell.h"

#define pagesize 20

@interface InviteLogViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray                      *_datasource;
}

@end

@implementation InviteLogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self removeTableMJHeader];
    [self removeTableMJFooter];
    
    [InviteUserTableViewCell registerWithTableView:self.tableView];
    
    [self reqDataAtPage:1];
}

- (void)footerRereshing{
    int page = ceil((_datasource.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    [self reqDataAtPage:page];
}

- (void)reqDataAtPage:(int)page{
    NSDictionary *params = @{@"page":@(page),
                             @"size":@(pagesize)
    };
    
    [CommonManager POST:@"agent/getInviteList" parameters:params success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            if (page == 1) {
                self->_datasource = [NSMutableArray array];
            }
            NSArray *dataArr = responseObject[@"data"][@"list"];
            for (NSDictionary *dict in dataArr) {
                UserInfoModel *model = [UserInfoModel yy_modelWithDictionary:dict];
                [self->_datasource addObject:model];
            }
            [self.tableView reloadData];
            
            if (dataArr.count < pagesize) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }else{
                [self setupTableViewMJFooter];
            }
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        RESP_FAILURE;
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    InviteUserTableViewCell *cell = [InviteUserTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.model = _datasource[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (UIView *)listView{
    return self.view;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
