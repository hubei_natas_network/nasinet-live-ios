//
//  InviteProfitTableViewCell.m
//  NasiLive
//
//  Created by yun11 on 2020/7/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "InviteProfitTableViewCell.h"

@interface InviteProfitTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *profitLabel;

@end

@implementation InviteProfitTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(AgentProfitModel *)model{
    _model = model;
    self.titleLabel.text = model.content;
    self.timeLabel.text = model.create_time;
    self.profitLabel.text = [NSString stringWithFormat:@"+%.2f",model.profit];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
