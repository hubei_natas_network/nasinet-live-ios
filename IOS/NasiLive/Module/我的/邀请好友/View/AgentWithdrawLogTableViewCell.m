//
//  AgentWithdrawLogTableViewCell.m
//  NasiLive
//
//  Created by yun11 on 2020/7/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "AgentWithdrawLogTableViewCell.h"

@interface AgentWithdrawLogTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *statusImgView;
@property (weak, nonatomic) IBOutlet UILabel *accountLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

@end

@implementation AgentWithdrawLogTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(WithdrawModel *)model{
    _model = model;
    self.timeLabel.text = model.create_time;
    self.accountLabel.text = model.alipay_account;
    self.orderNoLabel.text = model.trade_no;
    self.moneyLabel.text = [NSString stringWithFormat:@"￥ %@",model.cash];
    
    switch (model.status) {
        case 0:
            self.statusImgView.image = IMAGE_NAMED(@"ic_process_wating");
            break;
        case 1:
            self.statusImgView.image = IMAGE_NAMED(@"ic_process_success");
            break;
        case 2:
            self.statusImgView.image = IMAGE_NAMED(@"ic_process_refused");
            break;
        case 3:
            self.statusImgView.image = IMAGE_NAMED(@"ic_process_error");
            break;
        default:
            self.statusImgView.image = IMAGE_NAMED(@"ic_process_wating");
            break;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
