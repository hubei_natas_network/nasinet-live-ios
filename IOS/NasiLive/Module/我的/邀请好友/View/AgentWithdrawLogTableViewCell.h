//
//  AgentWithdrawLogTableViewCell.h
//  NasiLive
//
//  Created by yun11 on 2020/7/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

#import "WithdrawModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface AgentWithdrawLogTableViewCell : BaseTableViewCell

@property (strong, nonatomic) WithdrawModel *model;

@end

NS_ASSUME_NONNULL_END
