//
//  InviteAwardTableViewCell.h
//  MiyouBBS
//
//  Created by yun11 on 2021/3/12.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface InviteAwardTableViewCell : BaseTableViewCell

@property (strong, nonatomic) UserInfoModel *model;

@end

NS_ASSUME_NONNULL_END
