//
//  InviteUserTableViewCell.m
//  NasiLive
//
//  Created by yun11 on 2020/7/2.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "InviteUserTableViewCell.h"

@interface InviteUserTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end

@implementation InviteUserTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(UserInfoModel *)model{
    _model = model;
    [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    self.nameLabel.text = model.nick_name;
    self.timeLabel.text = model.regist_time;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
