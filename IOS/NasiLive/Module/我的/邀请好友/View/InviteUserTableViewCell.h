//
//  InviteUserTableViewCell.h
//  NasiLive
//
//  Created by yun11 on 2020/7/2.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface InviteUserTableViewCell : BaseTableViewCell

@property (strong, nonatomic) UserInfoModel *model;

@end

NS_ASSUME_NONNULL_END
