//
//  InviteProfitTableViewCell.h
//  NasiLive
//
//  Created by yun11 on 2020/7/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

#import "AgentProfitModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface InviteProfitTableViewCell : BaseTableViewCell

@property (strong, nonatomic) AgentProfitModel *model;

@end

NS_ASSUME_NONNULL_END
