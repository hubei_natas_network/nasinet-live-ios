//
//  UserTagModel.m
//  Nasi
//
//  Created by yun on 2020/1/6.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "UserTagModel.h"

@implementation UserTagModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"tagid" : @"id"
    };
}

// 当 JSON 转为 Model 完成后，该方法会被调用。
// 你可以在这里对数据进行校验，如果校验不通过，可以返回 NO，则该 Model 会被忽略。
// 你也可以在这里做一些自动转换不能完成的工作。
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    if (StrValid(dic[@"color"])) {
        NSString *colorStr = dic[@"color"];
        if ([colorStr hasPrefix:@"#"]) {
            _bg_color = [UIColor colorWithHexString:[colorStr substringFromIndex:1]];
        }
    }
    return YES;
}

@end
