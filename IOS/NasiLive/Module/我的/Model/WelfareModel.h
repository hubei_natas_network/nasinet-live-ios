//
//  WelfareModel.h
//  NasiBBS
//
//  Created by kevin on 2021/3/24.
//  Copyright © 2021 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WelfareModel : NSObject
@property (assign, nonatomic) int dl_count;
@property (copy, nonatomic) NSString *category;
@property (copy, nonatomic) NSString *dl_url;
@property (copy, nonatomic) NSString *icon;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *sub_title;
@end

NS_ASSUME_NONNULL_END
