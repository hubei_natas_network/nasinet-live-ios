//
//  UserTagModel.h
//  Nasi
//
//  Created by yun on 2020/1/6.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, UserTagType) {
    UserTagTypeDebuff,
    UserTagTypeBuff
};

@interface UserTagModel : NSObject

@property (nonatomic,assign) int tagid;
@property (copy, nonatomic) NSString *title;
@property (strong, nonatomic) UIColor *bg_color;
@property (nonatomic,assign) int point;
@property (nonatomic,assign) UserTagType type;

@end

NS_ASSUME_NONNULL_END
