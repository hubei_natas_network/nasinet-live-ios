//
//  ProfitModel.h
//  NasiLive
//
//  Created by yun11 on 2020/4/1.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GiftModel.h"
#import "MomentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProfitModel : NSObject

@property (assign, nonatomic) int profitid;
@property (assign, nonatomic) int coin_count;
@property (copy, nonatomic) NSString *content;
@property (assign, nonatomic) int type; //1-收入 0-支出
@property (assign, nonatomic) int consume_type; //1-礼物 2-有料
@property (assign, nonatomic) int resid; //关联商品id
@property (copy, nonatomic) NSString *create_time;

@property (strong, nonatomic) GiftModel *gift;
@property (strong, nonatomic) MomentModel *moment;

@end

NS_ASSUME_NONNULL_END
