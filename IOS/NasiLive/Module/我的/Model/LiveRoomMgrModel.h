//
//  LiveRoomMgrModel.h
//  NasiLive
//
//  Created by yun11 on 2020/10/23.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LiveRoomMgrModel : NSObject

@property (copy, nonatomic) NSString *anchorid;
@property (copy, nonatomic) NSString *mgrid;
@property (copy, nonatomic) NSString *create_time;

@property (strong, nonatomic)  UserInfoModel *user;

@end

NS_ASSUME_NONNULL_END
