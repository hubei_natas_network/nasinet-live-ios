//
//  WithdrawModel.m
//  NasiLive
//
//  Created by yun11 on 2020/4/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "WithdrawModel.h"

@implementation WithdrawModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"withdrawid" : @"id"
    };
}

@end
