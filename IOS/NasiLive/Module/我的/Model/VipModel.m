//
//  VipModel.m
//  NasiLive
//
//  Created by yun11 on 2020/7/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "VipModel.h"

@implementation VipModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
    };
}

// 当 JSON 转为 Model 完成后，该方法会被调用。
// 你可以在这里对数据进行校验，如果校验不通过，可以返回 NO，则该 Model 会被忽略。
// 你也可以在这里做一些自动转换不能完成的工作。
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    switch (_level) {
        case 1:{
            _title = @"游侠";
        }
            break;
        case 2:{
            _title = @"骑士";
        }
            break;
        case 3:{
            _title = @"公爵";
        }
            break;
        case 4:{
            _title = @"国王";
        }
            break;
        default:
            break;
    }
    _icon = [UIImage imageNamed:[NSString stringWithFormat:@"vip_level_%d",_level]];
    _logo = [UIImage imageNamed:[NSString stringWithFormat:@"vip_logo_big_%d",_level]];
    return YES;
}

@end
