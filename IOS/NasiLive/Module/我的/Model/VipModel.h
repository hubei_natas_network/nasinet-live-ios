//
//  VipModel.h
//  NasiLive
//
//  Created by yun11 on 2020/7/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface VipModel : NSObject

@property (assign, nonatomic) int level;
@property (assign, nonatomic) CGFloat price;
@property (assign, nonatomic) int gold;
@property (copy, nonatomic) NSString *title;
@property (strong, nonatomic) UIImage *icon;
@property (strong, nonatomic) UIImage *logo;

@end

NS_ASSUME_NONNULL_END
