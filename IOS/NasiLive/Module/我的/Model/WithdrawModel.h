//
//  WithdrawModel.h
//  NasiLive
//
//  Created by yun11 on 2020/4/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface WithdrawModel : NSObject

@property (assign, nonatomic) int withdrawid;
@property (assign, nonatomic) int diamond;
@property (copy, nonatomic) NSString *cash;
@property (copy, nonatomic) NSString *alipay_account;
@property (copy, nonatomic) NSString *alipay_name;
@property (copy, nonatomic) NSString *bankcard_account;
@property (copy, nonatomic) NSString *bankcard_name;
@property (copy, nonatomic) NSString *trade_no;
@property (copy, nonatomic) NSString *create_time;
@property (assign, nonatomic) int status;

@end

NS_ASSUME_NONNULL_END

/*
 "id": 8,#注释 <number>
 "uid": 63621147,用户id <number>
 "diamond": 100,扣除钻石 <number>
 "cash": "10.00",提现金额 <string>
 "alipay_account": "asd",#注释 <string>
 "alipay_name": "asd",#注释 <string>
 "create_time": "2020-04-02 20:54:11",#注释 <string>
 "operate_time": null,#注释 <string>
 "status": 0,0-未处理 1-已提现 2-已拒绝(钻石返还) 3-异常单(扣除金币) <number>
 "trade_no": null,转账单号（支付宝订单号） <string>
 "type": 1#注释 <number>
 */
