//
//  AuthStep3ViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/4/4.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "AuthStep3ViewController.h"
#import "BRPickerView.h"

@interface AuthStep3ViewController (){
    int             _gender;
    int             _constellationIndex;
    NSString        *_constellation;
}

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *genderBtn;
@property (weak, nonatomic) IBOutlet UIButton *constellationBtn;
@property (weak, nonatomic) IBOutlet UITextField *ageTextField;
@property (weak, nonatomic) IBOutlet UITextField *heightTextField;
@property (weak, nonatomic) IBOutlet UITextField *weightTextField;
@property (weak, nonatomic) IBOutlet UITextField *carrerTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *signatureTextField;

@end

@implementation AuthStep3ViewController

- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"申请成为主播";

}

- (IBAction)submitBtnClick:(UIButton *)sender {
    
    if (_constellation.length == 0) {
        [MBProgressHUD showTipMessageInView:@"请选择星座"];
        return;
    }
    NSString *age = self.ageTextField.text;
    if (age.length == 0) {
        [MBProgressHUD showTipMessageInView:@"请填写年龄"];
        [self.ageTextField becomeFirstResponder];
        return;
    }
    NSString *height = self.heightTextField.text;
    if (height.length == 0) {
        [MBProgressHUD showTipMessageInView:@"请填写身高"];
        [self.heightTextField becomeFirstResponder];
        return;
    }
    NSString *weight = self.weightTextField.text;
    if (weight.length == 0) {
        [MBProgressHUD showTipMessageInView:@"请填写体重"];
        [self.weightTextField becomeFirstResponder];
        return;
    }
    NSString *career = self.carrerTextField.text;
    if (career.length == 0) {
        [MBProgressHUD showTipMessageInView:@"请填写职业"];
        [self.carrerTextField becomeFirstResponder];
        return;
    }
    NSString *city = self.cityTextField.text;
    if (city.length == 0) {
        [MBProgressHUD showTipMessageInView:@"请填写所在城市"];
        [self.cityTextField becomeFirstResponder];
        return;
    }
    NSString *signature = self.signatureTextField.text;
    if (signature.length == 0) {
        [MBProgressHUD showTipMessageInView:@"请输入个性签名"];
        [self.signatureTextField becomeFirstResponder];
        return;
    }
    NSDictionary *params = @{@"age":age,
               @"constellation":_constellation,
               @"gender":@(_gender),
               @"height":height,
               @"weight":weight,
               @"career":career,
               @"city":city,
               @"signature":signature
    };
    
    if (self.authDict) {
        [commonManager showLoadingAnimateInWindow];
        [CommonManager POST:@"auth/identityAuth" parameters:self.authDict success:^(id responseObject) {
            if (RESP_SUCCESS(responseObject)) {
                [self submitUserInfo:params];
            }else{
                [commonManager hideAnimateHud];
                RESP_SHOW_ERROR_MSG(responseObject);
            }
        } failure:^(NSError *error) {
            [commonManager hideAnimateHud];
            RESP_FAILURE;
        }];
    }else{
        [commonManager showLoadingAnimateInWindow];
        [self submitUserInfo:params];
    }
}

- (void)submitUserInfo:(NSDictionary *)param{
    [CommonManager POST:@"User/editUserInfo" parameters:param success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            [MBProgressHUD showTipMessageInWindow:@"提交成功，请等待管理员审核"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popToRootViewControllerAnimated:YES];
            });
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (IBAction)genderBtnClick:(UIButton *)sender {
    BRStringPickerView *genderPickerView = [[BRStringPickerView alloc]initWithPickerMode:BRStringPickerComponentSingle];
    genderPickerView.dataSourceArr = @[@"女",@"男"];
    genderPickerView.selectIndex = _gender;
    genderPickerView.resultModelBlock = ^(BRResultModel *resultModel) {
        self->_gender = resultModel.index;
        [sender setTitle:resultModel.value forState:UIControlStateNormal];
    };
    [genderPickerView show];
}

- (IBAction)constellationBtnClick:(UIButton *)sender {
    NSArray *infoArr = @[@{@"key": @"白羊座", @"value": @"白羊座： 3月21日～4月20日", @"remark": @""},
                         @{@"key": @"金牛座", @"value": @"金牛座： 4月21日～5月21日", @"remark": @""},
                         @{@"key": @"双子座", @"value": @"双子座： 5月22日～6月21日", @"remark": @""},
                         @{@"key": @"巨蟹座", @"value": @"巨蟹座： 6月22日～7月22日", @"remark": @""},
                         @{@"key": @"狮子座", @"value": @"狮子座： 7月23日～8月23日", @"remark": @""},
                         @{@"key": @"处女座", @"value": @"处女座： 8月24日～9月23日", @"remark": @""},
                         @{@"key": @"天秤座", @"value": @"天秤座： 9月24日～10月23日", @"remark": @""},
                         @{@"key": @"天蝎座", @"value": @"天蝎座： 10月24日～11月22日", @"remark": @""},
                         @{@"key": @"射手座", @"value": @"射手座： 11月23日～12月21日", @"remark": @""},
                         @{@"key": @"摩羯座", @"value": @"摩羯座： 12月22日～1月20日", @"remark": @""},
                         @{@"key": @"水瓶座", @"value": @"水瓶座： 1月21日～2月19日", @"remark": @""},
                         @{@"key": @"双鱼座", @"value": @"双鱼座： 2月20日～3月20日", @"remark": @""}];
    NSMutableArray *modelArr = [[NSMutableArray alloc]init];
    for (NSDictionary *dic in infoArr) {
        BRResultModel *model = [[BRResultModel alloc]init];
        model.key = dic[@"key"];
        model.value = dic[@"value"];
        model.remark = dic[@"remark"];
        [modelArr addObject:model];
    }
    
    [BRStringPickerView showPickerWithTitle:@"请选择星座" dataSourceArr:[modelArr copy] selectIndex:_constellationIndex resultBlock:^(BRResultModel *resultModel) {
        self->_constellationIndex = resultModel.index;
        self->_constellation = resultModel.key;
        [sender setTitle:resultModel.key forState:UIControlStateNormal];
    }];
}

- (IBAction)backClick:(UIButton *)sender {
    [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"提醒" contentText:@"确定要取消申请吗？" leftButtonTitle:@"确定" leftClick:^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    } rightButtonTitle:@"点错了" rightClick:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
