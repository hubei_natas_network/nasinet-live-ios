//
//  AuthViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/4/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "AuthStep1ViewController.h"
#import "AuthStep2ViewController.h"

@interface AuthStep1ViewController (){
    NSTimer *timer;
    NSInteger timerCount;
}
 
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIView *bindPhoneView;
@property (weak, nonatomic) IBOutlet UIView *phoneBindedView;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendCodeBtn;

@end

@implementation AuthStep1ViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = @"申请成为主播";
    
    if (userManager.curUserInfo.mobile.length > 0) {
        self.bindPhoneView.hidden = YES;
        self.phoneBindedView.hidden = NO;
    }else{
        self.bindPhoneView.hidden = NO;
        self.phoneBindedView.hidden = YES;
    }
}

- (IBAction)sendCodeBtnClick:(UIButton *)sender {
    NSString *mobile = self.phoneTextField.text;
    if (![CommonManager checkPhone:mobile]) {
        [self.phoneTextField becomeFirstResponder];
        [MBProgressHUD showTipMessageInView:@"请填写正确的手机号"];
        return;
    }
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"auth/sendBindCode" parameters:@{@"mobile":mobile} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            sender.enabled = NO;
            [sender setTitle:@"重新发送(60)" forState:UIControlStateNormal];
            self->timerCount = 60;
            if (!self->timer) {
                self->timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(timerGo) userInfo:nil repeats:YES];
            }
            [self->timer fire];
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (IBAction)nextBtnClick:(UIButton *)sender {
    
    if (userManager.curUserInfo.mobile.length > 0) {
        AuthStep2ViewController *vc = [[AuthStep2ViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    
    NSString *mobile = self.phoneTextField.text;
    NSString *pwd = self.pwdTextField.text;
    NSString *code = self.codeTextField.text;
    
    if (![CommonManager checkPhone:mobile]) {
        [self.phoneTextField becomeFirstResponder];
        [MBProgressHUD showTipMessageInView:@"请填写正确的手机号"];
        return;
    }
    
    if (pwd.length < 6 || pwd.length > 18) {
        [self.pwdTextField becomeFirstResponder];
        [MBProgressHUD showTipMessageInView:@"密码长度为6-18位"];
        return;
    }
    
    if (code.length < 4) {
        [MBProgressHUD showTipMessageInView:@"验证码有误"];
        return;
    }
    
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"User/bindPhone" parameters:@{@"mobile":mobile,@"pwd":pwd,@"smscode":code} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            userManager.curUserInfo.mobile = mobile;
            [userManager saveUserInfo];
            AuthStep2ViewController *vc = [[AuthStep2ViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (IBAction)protocolBtnClick:(UIButton *)sender {
}

- (IBAction)backClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)timerGo{
    timerCount --;
    if (timerCount == 0) {
        [timer invalidate];
        self.sendCodeBtn.enabled = YES;
        [self.sendCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    }else{
        [self.sendCodeBtn setTitle:[NSString stringWithFormat:@"重新发送(%ld)",(long)timerCount] forState:UIControlStateNormal];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
