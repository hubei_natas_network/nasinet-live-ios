//
//  AuthStep2ViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/4/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "AuthStep2ViewController.h"
#import "AuthStep3ViewController.h"

#import <TZImagePickerController.h>
#import <QCloudCOSXML.h>

@interface AuthStep2ViewController ()<TZImagePickerControllerDelegate>{
    NSString                    *_idcardImageUrl1;
    NSString                    *_idcardImageUrl2;
    NSString                    *_idcardImageUrl3;
}

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UITextField *realNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *cardNoTextField;

@property (weak, nonatomic) IBOutlet UIImageView *cardImgView1;
@property (weak, nonatomic) IBOutlet UIImageView *cardImgView2;
@property (weak, nonatomic) IBOutlet UIImageView *cardImgView3;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *authedView;


@end

@implementation AuthStep2ViewController

- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cardImgViewTap:)];
    [self.cardImgView1 addGestureRecognizer:tap1];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cardImgViewTap:)];
    [self.cardImgView2 addGestureRecognizer:tap2];
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cardImgViewTap:)];
    [self.cardImgView3 addGestureRecognizer:tap3];
    
    if (userManager.curUserInfo.is_anchor) {
        self.authedView.hidden = NO;
        self.scrollView.hidden = YES;
    }else{
        self.authedView.hidden = YES;
        self.scrollView.hidden = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabel.text = @"申请成为主播";
    
    [CommonManager loadCosTempKey];
}

- (void)cardImgViewTap:(UIGestureRecognizer *)sender {
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
    imagePickerVc.allowTakeVideo = NO;
    imagePickerVc.allowPickingGif = NO;
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.showPhotoCannotSelectLayer = YES;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.naviTitleColor = [UIColor colorWithHexString:@"333333"];
    imagePickerVc.barItemTextColor = [UIColor colorWithHexString:@"333333"];
    imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        if (photos.count > 0) {
            UIImage *selImg = photos[0];
            ((UIImageView *)sender.view).image = selImg;
            NSString *fileName = [CommonManager getNameBaseCurrentTime:@".jpg"];
            NSString *flieUploadPath = [NSString stringWithFormat:@"%@/%@",configManager.appConfig.cos_folder_image,fileName];
            QCloudCOSXMLUploadObjectRequest* put = [QCloudCOSXMLUploadObjectRequest new];
            put.object = flieUploadPath;
            put.bucket = configManager.appConfig.cos_bucket;
            put.body = UIImageJPEGRepresentation(selImg, 1);
            [put setFinishBlock:^(QCloudUploadObjectResult *result, NSError* error) {
                //可以从 result 获取结果
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (result && !error) {
                        switch (sender.view.tag) {
                            case 1:
                                self->_idcardImageUrl1 = result.location;
                                break;
                            case 2:
                                self->_idcardImageUrl2 = result.location;
                                break;
                            case 3:
                                self->_idcardImageUrl3 = result.location;
                                break;
                            default:
                                break;
                        }
                    }else{
                        [commonManager hideAnimateHud];
                        [commonManager showErrorAnimateInWindow];
                    }
                });
            }];
            [[QCloudCOSTransferMangerService defaultCOSTransferManager] UploadObject:put];
        }
    }];
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (IBAction)nextBtnClick:(UIButton *)sender {
    if (userManager.curUserInfo.is_anchor) {
        AuthStep3ViewController *vc = [[AuthStep3ViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    NSString *realName = self.realNameTextField.text;
    NSString *cardNo = self.cardNoTextField.text;
    if (realName.length < 2) {
        [self.realNameTextField becomeFirstResponder];
        [MBProgressHUD showTipMessageInView:@"请填写您的真实姓名"];
        return;
    }
    if (![CommonManager checksimpleVerifyIdentityCardNum:cardNo]) {
        [self.cardNoTextField becomeFirstResponder];
        [MBProgressHUD showTipMessageInView:@"身份证号校验失败"];
        return;
    }
    if (_idcardImageUrl1.length == 0 || _idcardImageUrl2.length == 0 || _idcardImageUrl3.length == 0) {
        [MBProgressHUD showTipMessageInView:@"请先上传身份证照片"];
        return;
    }
    
    NSDictionary *params = @{@"real_name":realName,
                             @"id_num":cardNo,
                             @"id_card_url":[@[_idcardImageUrl1,_idcardImageUrl2,_idcardImageUrl3] componentsJoinedByString:@","]
    };
    AuthStep3ViewController *vc = [[AuthStep3ViewController alloc]init];
    vc.authDict = params;
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)backClick:(UIButton *)sender {
    [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"提醒" contentText:@"确定要取消申请吗？" leftButtonTitle:@"确定" leftClick:^{
        [self.navigationController popToRootViewControllerAnimated:YES];
    } rightButtonTitle:@"点错了" rightClick:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
