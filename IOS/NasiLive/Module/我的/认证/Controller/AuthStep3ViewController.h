//
//  AuthStep3ViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/4/4.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AuthStep3ViewController : RootViewController

@property (strong, nonatomic) NSDictionary *authDict;

@end

NS_ASSUME_NONNULL_END
