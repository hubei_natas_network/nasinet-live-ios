//
//  MLVBLiveManager.h
//  NasiLive
//
//  Created by yun11 on 2020/11/7.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LiveModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef void (^MLVBLoginCompletionCallback)(int errCode, NSString *errMsg);

@interface MLVBLiveManager : NSObject

- (void)loginWithUserID:(NSString *)userID
               userSign:(NSString *)userSign
             completion:(MLVBLoginCompletionCallback)completion
                failure:(QNHttpRequestFailed)failure;

- (void)createMlvbLiveParameters:(NSDictionary *)param success:(QNHttpRequestSuccess)success failure:(QNHttpRequestFailed)failure;

- (void)turnLinkOnOff:(int)onoff success:(QNHttpRequestSuccess)success failure:(QNHttpRequestFailed)failure;

- (void)requestLinkWithAnchor:(NSInteger)anchorid success:(QNHttpRequestSuccess)success failure:(QNHttpRequestFailed)failure;

- (void)acceptLinkWithUser:(NSInteger)userid success:(QNHttpRequestSuccess)success failure:(QNHttpRequestFailed)failure;

- (void)refuseLinkWithUser:(NSInteger)userid success:(QNHttpRequestSuccess)success failure:(QNHttpRequestFailed)failure;

- (void)stopLinkWithUser:(NSInteger)userid anchor:(NSInteger)anchorid success:(QNHttpRequestSuccess)success failure:(QNHttpRequestFailed)failure;

- (void)mergeStreamWithUser:(NSInteger)userid success:(QNHttpRequestSuccess)success failure:(QNHttpRequestFailed)failure;

- (void)requestEnterPkModeSuccess:(QNHttpRequestSuccess)success failure:(QNHttpRequestFailed)failure;

- (void)requestEndPkModeSuccess:(QNHttpRequestSuccess)success failure:(QNHttpRequestFailed)failure;

@end

NS_ASSUME_NONNULL_END
