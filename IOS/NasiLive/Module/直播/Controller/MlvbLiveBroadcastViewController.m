//
//  MlvbLiveBroadcastViewController.m
//  NasiLive
//
//  Created by yun on 2020/3/4.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MlvbLiveBroadcastViewController.h"
#import "ContributeRankViewController.h"
#import "MessagePopListViewController.h"
#import "LiveGoodsViewController.h"
#import "LiveManageViewController.h"
#import "UserInfoViewController.h"
#import "VipCenterViewController.h"
#import "MessageChatViewController.h"
#import "GuardianViewController.h"

#import "ShopGoodsModel.h"

#import "MLVBLiveManager.h"

#import "ImSDK.h"
#import <QCloudCOSXML.h>
#import <TZImagePickerController.h>
#import "BRPickerView.h"
#import "SharePanelView.h"
#import "UserCardView.h"

#import "TCAVFilter.h"

#import "RoomChatTableViewCell.h"
#import "GiftShowView.h"
#import "ContinueGiftView.h"
#import "NormalGiftBackView.h"
#import "VipEnterAnimationView.h"

#import <TiSDK/TiSDKInterface.h> // TiSDK接口头文件
#import "TiUIManager.h"

#import "SuperPlayer.h"

@import TXLiteAVSDK_Professional;

@interface MlvbLiveBroadcastViewController ()<TXLivePushListener,TZImagePickerControllerDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,GiftShowViewDelegate,TXVideoCustomProcessDelegate,TiUIManagerDelegate,SharePanelViewDelegate,UserCardViewDelegate,SuperPlayerDelegate>{
    TXLivePush              *_pusher;
    MLVBLiveManager         *_liveManager;
    BOOL                    _inLive;
    
    UIView                  *_shadowView;
    
    GiftShowView            *_giftShowView;
    ContinueGiftView        *_continueGiftView;
    NormalGiftBackView      *_normalGiftBackView;
    VipEnterAnimationView   *_vipEnterAnimationView;
    
    UIView                  *_beautyPanel;
    UIButton                *_beautyBtn;
    UIButton                *_filterBtn;
    UIView                  *_underline;
    UIView                  *_beautyView;
    UIView                  *_filterView;
    UIButton                *_selectedFilterBtn;
    
    NSInteger               _selectedCategoryId;
    
    NSArray<TCAVFilter *>   *_filters;
    
    CGFloat                 _filterValue;
    CGFloat                 _beautyLevel;
    CGFloat                 _whiteLevel;
    CGFloat                 _ruddyLevel;
    NSUInteger              _selectedFilterIndex;
        
    NSString                *_thumbUrl;
    UIImage                 *_thumbImage;
        
    NSMutableArray          *_chatArray;
    
    ContributeRankViewController                *_contributeRankVc;
    LiveManageViewController                    *_liveManageVc;
    MessagePopListViewController                *_messageVc;
    LiveGoodsViewController                     *_goodsVc;
    GuardianViewController                      *_guardVc;
    
    int                 liveManagerRetryTimes;
    int                 mergeRetryTimes;
    int                 linkerid;
}

@property (weak, nonatomic) IBOutlet UIView *playerContainerView;
@property (weak, nonatomic) IBOutlet UIView *linkPlayerContainerView;
@property (weak, nonatomic) IBOutlet UIView *linkPlayerView;
@property (weak, nonatomic) IBOutlet UIView *prepareView;
@property (weak, nonatomic) IBOutlet UIView *controlView;

@property (weak, nonatomic) IBOutlet UIButton *chooseCategoryBtn;
@property (weak, nonatomic) IBOutlet UIImageView *selectThumbImgView;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITableView *chatTableView;
@property (weak, nonatomic) IBOutlet UILabel *liveTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *audienceCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *profitLabel;

@property (weak, nonatomic) IBOutlet UIView *showGoodsBtnView;

@property (weak, nonatomic) IBOutlet UIView *roomTypeSelView;
@property (weak, nonatomic) IBOutlet UIButton *roomTypeBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *typeViewBottomLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *startBtnBottomLC;
@property (weak, nonatomic) IBOutlet UIButton *normalRoomTypeBtn;
@property (weak, nonatomic) IBOutlet UIButton *feeRoomTypeBtn;
@property (weak, nonatomic) IBOutlet UIButton *pwdRoomTypeBtn;

@property (weak, nonatomic) IBOutlet UIButton *linkBtn;

@property (weak, nonatomic) IBOutlet UIView *roomOptView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *roomOptViewBottomLC;
@property (weak, nonatomic) IBOutlet UILabel *guardCountLabel;

@property (weak, nonatomic) IBOutlet UIView *pkView;
@property (weak, nonatomic) IBOutlet UIView *pkPlayerView1;
@property (weak, nonatomic) IBOutlet UIView *pkPlayerView2;
@property (weak, nonatomic) IBOutlet UIView *scoreView1;
@property (weak, nonatomic) IBOutlet UIView *scoreView2;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel1;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel2;
@property (weak, nonatomic) IBOutlet UILabel *pkTimerLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scoreView1WidthLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scoreView2WidthLC;
@property (weak, nonatomic) IBOutlet UIImageView *pkResultImgView1;
@property (weak, nonatomic) IBOutlet UIImageView *pkResultImgView2;
@property (weak, nonatomic) IBOutlet UIImageView *pkResultImgView3;
@property (weak, nonatomic) IBOutlet UIView *pkAnchorView;
@property (weak, nonatomic) IBOutlet UILabel *pkAnchorNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *pkAnchorIconView;
@property (weak, nonatomic) IBOutlet UIButton *pkAnchorAttentBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pkAnchorViewLeadingLC;

@property (weak, nonatomic) IBOutlet UIView *pkActionView;
@property (weak, nonatomic) IBOutlet UIButton *pkActionBtn;


@property (strong, nonatomic) NSTimer *timer;
@property (nonatomic,assign) NSInteger liveDuration;
@property (nonatomic,assign) NSInteger audienceCount;

@property (strong, nonatomic) LiveModel *liveModel;

@property (strong, nonatomic) NSMutableArray *shareChannelArray1;
@property (strong, nonatomic) NSMutableArray *shareChannelArray2;

@property (assign, nonatomic) int selectedRoomTypeValue;
@property (assign, nonatomic) int price;
@property (copy, nonatomic) NSString *pwd;

@property (strong, nonatomic) NSArray *goodsArray;
@property (assign, nonatomic) int explainingGoodsid;

@property (strong, nonatomic) SVGAPlayer *svgaPkPlayer;
@property (strong, nonatomic) SVGAPlayer *svgaPkWinPlayer;
@property (strong, nonatomic) YYAnimatedImageView *pkFindingAnimation;

@property (strong, nonatomic) SuperPlayerView         *superPlayer;

@property (assign, nonatomic) int pkStatus; //0-未pk 1-pk中 2-惩罚中

@end

@implementation MlvbLiveBroadcastViewController

- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        }
    });
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initLiveManager];
    
    self.controlView.hidden = YES;
    self.prepareView.backgroundColor = [UIColor clearColor];
    self.linkBtn.selected = NO; //默认关闭连麦功能
    
    UITapGestureRecognizer *chooseThumbTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(chooseThumbTap:)];
    [self.selectThumbImgView addGestureRecognizer:chooseThumbTap];
    
    self.titleTextField.delegate = self;
    self.chatTableView.delegate = self;
    self.chatTableView.dataSource = self;
    
    [RoomChatTableViewCell registerWithTableView:self.chatTableView];
    
    [self initPusher];
    
    _chatArray = [NSMutableArray array];
    [self displaySystemMessage:configManager.appConfig.room_notice];
    
    _normalGiftBackView = [[NormalGiftBackView alloc]initWithFrame:CGRectMake(0, kStatusBarHeight + 200, 300, 140)];
    [self.view insertSubview:_normalGiftBackView belowSubview:self.controlView];
    
    _shadowView = [[UIView alloc]initWithFrame:self.view.bounds];
    _shadowView.backgroundColor = [UIColor clearColor];
    _shadowView.hidden = YES;
    [self.view insertSubview:_shadowView belowSubview:self.roomTypeSelView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shadowViewTap)];
    [_shadowView addGestureRecognizer:tap];
    
    switch (configManager.appConfig.beauty_channel) {
        case 0:{
            //腾讯云自带美颜 --------- start ------------
            [self createBeautyPanel];
            //腾讯云自带美颜 --------- end ------------
        }
            break;
        case 1:{
            // TiSDK
            [TiSDK init:configManager.appConfig.tisdk_key CallBack:^(InitStatus initStatus) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"TiSDKInitStatusNotification" object:nil];
            }];
            
            [[TiUIManager shareManager] loadToView:self.view forDelegate:self];
        }
            break;
        default:
            break;
    }
    
    [self getGoodsList];
    [self getGuardianCount];
    
    [[[SVGAParser alloc]init] parseWithNamed:@"animation_pk_start" inBundle:nil completionBlock:^(SVGAVideoEntity * _Nonnull videoItem) {
        self.svgaPkPlayer = [[SVGAPlayer alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth - 40, kScreenWidth - 40)];
        self.svgaPkPlayer.centerY = self.view.centerY - 30;
        self.svgaPkPlayer.centerX = self.view.centerX;
        self.svgaPkPlayer.videoItem = videoItem;
    } failureBlock:^(NSError * _Nonnull error) {
        NSLog(@"1");
    }];
    
    [[[SVGAParser alloc]init] parseWithNamed:@"animation_pk_win" inBundle:nil completionBlock:^(SVGAVideoEntity * _Nonnull videoItem) {
        self.svgaPkWinPlayer = [[SVGAPlayer alloc] initWithFrame:CGRectMake(0, 0, 89, 89)];
        self.svgaPkWinPlayer.videoItem = videoItem;
    } failureBlock:^(NSError * _Nonnull error) {
        NSLog(@"1");
    }];
    
    self.pkFindingAnimation = [[YYAnimatedImageView alloc]initWithFrame:CGRectMake(0, 0, 74, 34)];
    self.pkFindingAnimation.yy_imageURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"animation_pk_finding" ofType:@"gif"]];
    self.pkFindingAnimation.contentMode = UIViewContentModeScaleAspectFit;
}

- (void)viewDidLayoutSubviews{
    CGPoint chatTablePoint = [self.chatTableView convertPoint:self.chatTableView.bounds.origin toView:self.view];
    _normalGiftBackView.mj_y = chatTablePoint.y - _normalGiftBackView.height;
}

- (void)timerGo{
    self.liveDuration ++;
    self.liveTimeLabel.text = [CommonManager formateSecond:self.liveDuration withHour:YES];
    
    [self caculatePkTime];
}

- (void)resetPlayer{
    if (self.liveModel.pk_status == PkStatusInPk && self.liveModel.pkinfo && self.liveModel.pklive) {
        [self getPKAnchorBasicInfo];
        
        [self.pkActionBtn setTitle:@"结束PK" forState:UIControlStateNormal];
        
        self.pkView.hidden = NO;
        self.pkAnchorView.hidden = YES;
        
        [self refreshPkScoreWithAnimation:NO];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self showPkStartAnimation];
        });
        
        BOOL isHome = self.liveModel.pkinfo.home_anchorid == userManager.curUserInfo.userid;
        self.pkAnchorViewLeadingLC.constant = isHome ? kScreenWidth/2 + 5 : 5;
        
        [_pusher startPreview:isHome ? self.pkPlayerView1 : self.pkPlayerView2];
        
        // superPlayer 永远播放对方主播的画面 此处播放低延时流
        [self setupPlayer:self.superPlayer videoUrl:self.liveModel.pklive.acc_pull_url];
        self.superPlayer.fatherView = !isHome ? self.pkPlayerView1 : self.pkPlayerView2;
    }else{
        self.pkView.hidden = YES;
        self.pkAnchorView.hidden = YES;
        [_pusher startPreview:self.playerContainerView];
        if (_superPlayer) {
            [_superPlayer resetPlayer];
            [_superPlayer removeFromSuperview];
            _superPlayer = nil;
        }
    }
}

- (void)resetPkAnchorView{
    self.pkAnchorView.hidden = NO;
    [self.pkAnchorIconView sd_setImageWithURL:[NSURL URLWithString:self.liveModel.pklive.anchor.avatar] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    self.pkAnchorNameLabel.text = self.liveModel.pklive.anchor.nick_name;
    self.pkAnchorAttentBtn.hidden = self.liveModel.pklive.anchor.isattent;
}

- (void)initLiveManager{
    _liveManager = [[MLVBLiveManager alloc]init];
    kWeakSelf(self);
    [_liveManager loginWithUserID:@([userManager curUserInfo].userid).stringValue userSign:userManager.curUserInfo.txim_sign completion:^(int errCode, NSString * _Nonnull errMsg) {
        if (errCode != 0) {
            self->liveManagerRetryTimes ++;
            if (self->liveManagerRetryTimes > 5) {
                QNAlertView *alert = [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"错误" contentText:@"房间服务初始化失败" singleButton:@"退出" buttonClick:^{
                    [weakself.navigationController popViewControllerAnimated:YES];
                }];
                alert.cancelBackTap = YES;
            }else{
                [weakself initLiveManager];
            }
        }
    } failure:^(NSError *error) {
        self->liveManagerRetryTimes ++;
        if (self->liveManagerRetryTimes > 5) {
            QNAlertView *alert = [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"错误" contentText:@"房间服务初始化失败" singleButton:@"退出" buttonClick:^{
                [weakself.navigationController popViewControllerAnimated:YES];
            }];
            alert.cancelBackTap = YES;
        }else{
            [weakself initLiveManager];
        }
    }];
}

- (void)endPk{
    self.pkActionView.hidden = YES;
    self.liveModel.pk_status = PkStatusNO;
    self.liveModel.pkinfo = nil;
    self.liveModel.pklive = nil;
    [self caculatePkTime];
    [self resetPlayer];
}

- (void)initPusher{
    TXLivePushConfig *_config = [[TXLivePushConfig alloc] init];
    _pusher = [[TXLivePush alloc] initWithConfig: _config];
    _pusher.delegate = self;
    _pusher.videoProcessDelegate = self;
    [_pusher setVideoQuality:VIDEO_QUALITY_HIGH_DEFINITION adjustBitrate:YES adjustResolution:NO];
    
    if (configManager.appConfig.beauty_channel == 0) {
        //腾讯云自带美颜 --------- start ------------
        
        //默认美颜设置
        _beautyLevel = 8.f;
        _whiteLevel = 5.f;
        _ruddyLevel = 3.f;
        [[_pusher getBeautyManager] setBeautyStyle:TXBeautyStyleSmooth];
        [[_pusher getBeautyManager] setBeautyLevel:_beautyLevel];
        [[_pusher getBeautyManager] setWhitenessLevel:_whiteLevel];
        [[_pusher getBeautyManager] setRuddyLevel:_ruddyLevel];
    
        _filters = [TCAVFilterManager defaultManager].allFilters;
        // 初始化滤镜选项
        _filterValue = 5.f;
        const TCAVFilterIdentifier defaultFilterIdentifier = TCAVFilterIdentifierNormal;
        // index = 0 为关闭
        _selectedFilterIndex = [_filters indexOfObjectPassingTest:
                                         ^BOOL(TCAVFilter * _Nonnull obj,
                                               NSUInteger idx,
                                               BOOL * _Nonnull stop) {
            return [obj.identifier isEqualToString:defaultFilterIdentifier];
        }];
        UIImage *lutImage = [self filterImageByMenuOptionIndex:_selectedFilterIndex+1];
        [[_pusher getBeautyManager] setFilter:lutImage];
        [[_pusher getBeautyManager] setFilterStrength:_filterValue/10.0];
        
        //腾讯云自带美颜 --------- end ------------
    }
    
    //启动本地摄像头预览
    [_pusher startPreview:self.playerContainerView];
    
    //IM通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(imNotification:) name:KNotificationInRoomIM object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(imGroupEventNotification:) name:KNotificationGroupEvent object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(giftAnimationNotification:) name:KNotificationGiftAnimation object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidEnterBackGround:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)createBeautyPanel{
    
    _beautyPanel = [[UIView alloc]initWithFrame:CGRectMake(0, KScreenHeight, KScreenWidth, 240 + kBottomSafeHeight)];
    _beautyPanel.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    _beautyPanel.hidden = YES;
    [self.view addSubview:_beautyPanel];
    
    UIView *switchView = [[UIView alloc]initWithFrame:CGRectMake(0, _beautyPanel.height - 50 - kBottomSafeHeight, _beautyPanel.width, 40)];
    [_beautyPanel addSubview:switchView];
    
    _beautyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _beautyBtn.frame = CGRectMake(_beautyPanel.width / 2 - 30 - 40, 0, 40, 36);
    _beautyBtn.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
    [_beautyBtn setTitle:@"美颜" forState:UIControlStateNormal];
    [_beautyBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [_beautyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _beautyBtn.selected = YES;
    [_beautyBtn addTarget:self action:@selector(switchBeautyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [switchView addSubview:_beautyBtn];
    
    _filterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _filterBtn.frame = CGRectMake(_beautyPanel.width / 2 + 30, 0, 40, 36);
    _filterBtn.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
    [_filterBtn setTitle:@"滤镜" forState:UIControlStateNormal];
    [_filterBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [_filterBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_filterBtn addTarget:self action:@selector(switchFilterBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [switchView addSubview:_filterBtn];
    
    _underline = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_beautyBtn.frame), 12, 4)];
    _underline.centerX = _beautyBtn.centerX;
    _underline.layer.cornerRadius = 2;
    [_underline setBackgroundColor:MAIN_COLOR];
    [switchView addSubview:_underline];
    
    
    // 美颜  ————————————————————————————————————————————————————————————————————————————
    _beautyView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _beautyPanel.width, switchView.mj_y)];
    [_beautyPanel addSubview:_beautyView];
    
    UILabel *beautyTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 40, 30, 15)];
    beautyTitleLabel.text = @"磨皮";
    beautyTitleLabel.font = [UIFont systemFontOfSize:14];
    beautyTitleLabel.textColor = [UIColor whiteColor];
    [_beautyView addSubview:beautyTitleLabel];
    
    UISlider *beautySlider = [[UISlider alloc]initWithFrame:CGRectMake(CGRectGetMaxX(beautyTitleLabel.frame) + 8, 0, _beautyView.width - 15 - (CGRectGetMaxX(beautyTitleLabel.frame) + 8), 30)];
    beautySlider.centerY = beautyTitleLabel.centerY;
    beautySlider.maximumValue = 9.f;
    beautySlider.value = _beautyLevel;
    beautySlider.thumbTintColor = MAIN_COLOR;
    beautySlider.minimumTrackTintColor = MAIN_COLOR;
    beautySlider.maximumTrackTintColor = [UIColor colorWithHexString:@"CCCCCC"];
    [beautySlider addTarget:self action:@selector(beautySliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_beautyView addSubview:beautySlider];
    
    UILabel *whiteTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(beautyTitleLabel.frame) + 40, 30, 15)];
    whiteTitleLabel.text = @"美白";
    whiteTitleLabel.font = [UIFont systemFontOfSize:14];
    whiteTitleLabel.textColor = [UIColor whiteColor];
    [_beautyView addSubview:whiteTitleLabel];
    
    UISlider *whiteySlider = [[UISlider alloc]initWithFrame:CGRectMake(CGRectGetMaxX(whiteTitleLabel.frame) + 8, 0, _beautyView.width - 15 - (CGRectGetMaxX(beautyTitleLabel.frame) + 8), 30)];
    whiteySlider.centerY = whiteTitleLabel.centerY;
    whiteySlider.maximumValue = 9.f;
    whiteySlider.value = _whiteLevel;
    whiteySlider.thumbTintColor = MAIN_COLOR;
    whiteySlider.minimumTrackTintColor = MAIN_COLOR;
    whiteySlider.maximumTrackTintColor = [UIColor colorWithHexString:@"CCCCCC"];
    [whiteySlider addTarget:self action:@selector(whiteySliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_beautyView addSubview:whiteySlider];
    
    UILabel *ruddyTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(whiteTitleLabel.frame) + 40, 30, 15)];
    ruddyTitleLabel.text = @"红润";
    ruddyTitleLabel.font = [UIFont systemFontOfSize:14];
    ruddyTitleLabel.textColor = [UIColor whiteColor];
    [_beautyView addSubview:ruddyTitleLabel];
    
    UISlider *ruddySlider = [[UISlider alloc]initWithFrame:CGRectMake(CGRectGetMaxX(ruddyTitleLabel.frame) + 8, 0, _beautyView.width - 15 - (CGRectGetMaxX(beautyTitleLabel.frame) + 8), 30)];
    ruddySlider.centerY = ruddyTitleLabel.centerY;
    ruddySlider.maximumValue = 9.f;
    ruddySlider.value = _ruddyLevel;
    ruddySlider.thumbTintColor = MAIN_COLOR;
    ruddySlider.minimumTrackTintColor = MAIN_COLOR;
    ruddySlider.maximumTrackTintColor = [UIColor colorWithHexString:@"CCCCCC"];
    [ruddySlider addTarget:self action:@selector(ruddySliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_beautyView addSubview:ruddySlider];
    
    
    
    //滤镜  ————————————————————————————————————————————————————————————————————————————
    _filterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _beautyPanel.width, switchView.mj_y)];
    _filterView.hidden = YES;
    [_beautyPanel addSubview:_filterView];
    
    UISlider *filterSlider = [[UISlider alloc]initWithFrame:CGRectMake(45, 30, _filterView.width - 45*2, 30)];
    filterSlider.centerY = beautyTitleLabel.centerY;
    filterSlider.maximumValue = 10.f;
    filterSlider.value = _filterValue;
    filterSlider.thumbTintColor = MAIN_COLOR;
    filterSlider.minimumTrackTintColor = MAIN_COLOR;
    filterSlider.maximumTrackTintColor = [UIColor colorWithHexString:@"CCCCCC"];
    [filterSlider addTarget:self action:@selector(filterSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_filterView addSubview:filterSlider];
    
    UIScrollView *filterScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(filterSlider.frame)+30, _filterView.width, _filterView.height - (CGRectGetMaxY(filterSlider.frame)+30) - 15)];
    filterScrollView.showsHorizontalScrollIndicator = NO;
    [_filterView addSubview:filterScrollView];
    CGFloat filterM = 16;
    CGFloat filterH = filterScrollView.height;
    CGFloat filterW = filterH * 121 / 176;
    for (int i = 0; i<_filters.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *image = [self iconForFilter:_filters[i].identifier selected:NO];
        [button setImage:image forState:UIControlStateNormal];
        UIImage *selectImage = [self iconForFilter:_filters[i].identifier selected:YES];
        [button setImage:selectImage forState:UIControlStateSelected];
        button.frame = CGRectMake(filterM + (filterM + filterW)*i, 0, filterW, filterH);
        button.tag = i;
        [button addTarget:self action:@selector(filterBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [filterScrollView addSubview:button];
        if (_selectedFilterIndex == i) {
            button.selected = YES;
            _selectedFilterBtn = button;
        }
    }
    [filterScrollView setContentSize:CGSizeMake((filterM+filterW)*_filters.count + filterM, 0)];
}

- (void)getGoodsList{
    [CommonManager POST:@"live/getGoodsList" parameters:@{@"anchorid":@([userManager curUserInfo].userid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            NSDictionary *dataDict = responseObject[@"data"];
            self.goodsArray = [NSArray yy_modelArrayWithClass:[ShopGoodsModel class] json:dataDict];
            if (self.goodsArray.count > 0) {
                self.showGoodsBtnView.hidden = NO;
                ShopGoodsModel *model = self.goodsArray[0];
                if (model.live_explaining) {
                    self.explainingGoodsid = model.goodsid;
                }
            }
        }
    } failure:^(NSError *error) {
    }];
}

- (void)getPKAnchorBasicInfo{
    [CommonManager POST:@"anchor/getAnchorBasicInfo" parameters:@{@"anchorid":@(self.liveModel.pklive.anchorid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.liveModel.pklive.anchor = [UserInfoModel yy_modelWithDictionary:responseObject[@"data"]];
            [self resetPkAnchorView];
        }
    } failure:^(NSError *error) {
    }];
}

- (void)getGuardianCount{
    [CommonManager POST:@"anchor/getGuardianCount" parameters:@{@"anchorid":@(userManager.curUserInfo.userid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.guardCountLabel.text = [NSString stringWithFormat:@"守护 %@人",responseObject[@"data"][@"count"]];
        }else{
        }
    } failure:^(NSError *error) {
    }];
}

- (void)getLiveBasicInfo{
    [CommonManager POST:@"live/getLiveBasicInfo" parameters:@{@"liveid":@(self.liveModel.liveid).stringValue} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            LiveModel *liveModel = [LiveModel yy_modelWithDictionary:responseObject[@"data"][@"live"]];
            self.liveModel.profit = liveModel.profit;
            self.liveModel.hot = liveModel.hot;
            self.profitLabel.text = [NSString stringWithFormat:@"%d",self.liveModel.profit];
        }
    } failure:^(NSError *error) {
    }];
}

#pragma mark ————————————————————— Action ————————————————————————
- (IBAction)switchCamera:(UIButton *)sender {
    [self.view endEditing:YES];
    [_pusher switchCamera];
}

- (IBAction)shareBtnClick:(UIButton *)sender {
    SharePanelView *sharePannelView = [SharePanelView showPanelInView:self.view channel1:self.shareChannelArray1 channel2:self.shareChannelArray2];
    sharePannelView.delegate = self;
}

- (IBAction)switchBeautyPanel:(UIButton *)sender {
    [self.view endEditing:YES];
    self.controlView.hidden = YES;
    self.prepareView.hidden = YES;
    _beautyPanel.hidden = NO;
    _shadowView.hidden = NO;
    switch (configManager.appConfig.beauty_channel) {
        case 0:{
            [UIView animateWithDuration:0.2 animations:^{
                self->_beautyPanel.mj_y = KScreenHeight - self->_beautyPanel.height;
            }];
        }
            break;
        case 1:{
            [[TiUIManager shareManager]showMainMenuView];
        }
            break;
        default:
            break;
    }
    
}

- (IBAction)msgBtnClick:(UIButton *)sender {
    [self shadowViewTap];
    if (!_messageVc) {
        _messageVc = [[MessagePopListViewController alloc]init];
        [self addChildViewController:_messageVc];
        [self.view addSubview:_messageVc.view];
        _messageVc.view.frame = CGRectMake(0, KScreenHeight, KScreenWidth, 550);
    }
    _messageVc.view.hidden = NO;
    _shadowView.hidden = NO;
    [UIView animateWithDuration:0.2 animations:^{
        self->_messageVc.view.mj_y = KScreenHeight - 550;
    }];
}

- (IBAction)closeClick:(UIButton *)sender {
    if (_inLive) {
        kWeakSelf(self);
        [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"提示" contentText:@"确定结束直播?" leftButtonTitle:@"点错了" leftClick:nil rightButtonTitle:@"结束" rightClick:^{
            [commonManager showLoadingAnimateInWindow];
            [CommonManager POST:@"Live/endLive" parameters:@{@"liveid":@(self.liveModel.liveid)} success:^(id responseObject) {
                [commonManager hideAnimateHud];
                if (RESP_SUCCESS(responseObject)) {
                    [weakself removeTimer];
                    weakself.linkPlayerContainerView.hidden = YES;
                    //结束推流
                    self->_inLive = NO;
                    [self->_pusher stopPreview]; //如果已经启动了摄像头预览，请在结束推流时将其关闭。
                    [self->_pusher stopPush];
                    //发送通知
                    [weakself sendStopMsg];
                    weakself.liveModel = [LiveModel yy_modelWithDictionary:responseObject[@"data"]];
                    QNAlertView *alertView = [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"直播结束" contentText:[NSString stringWithFormat:@"本次直播获得收益：%ld钻石",(long)self.liveModel.profit] singleButton:@"返回" buttonClick:^{
                        [weakself.navigationController popViewControllerAnimated:YES];
                    }];
                    alertView.cancelBackTap = YES;
                }else{
                    RESP_SHOW_ERROR_MSG(responseObject);
                }
            } failure:^(NSError *error) {
                [commonManager hideAnimateHud];
                RESP_FAILURE;
            }];
        }];
    }else{
        [self removeTimer];
        [_pusher stopPreview];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)guardBtnClick:(id)sender {
    if (!_guardVc) {
        _guardVc = [[GuardianViewController alloc]init];
        _guardVc.anchorid = (int)userManager.curUserInfo.userid;
        _guardVc.isAnchor = YES;
        [self addChildViewController:_guardVc];
        [self.view addSubview:_guardVc.view];
        _guardVc.view.frame = CGRectMake(0, KScreenHeight, KScreenWidth, kScreenHeight - 200);
    }
    [_guardVc refreshData];
    [UIView animateWithDuration:0.2 animations:^{
        self->_shadowView.hidden = NO;
        self->_shadowView.alpha = 1;
        self->_guardVc.view.mj_y = 200;
    }];
}

- (IBAction)rankBtnClick:(UIButton *)sender {
    if (!_contributeRankVc) {
        _contributeRankVc = [[ContributeRankViewController alloc]init];
        _contributeRankVc.liveid = self.liveModel.liveid;
        [self addChildViewController:_contributeRankVc];
        [self.view addSubview:_contributeRankVc.view];
        _contributeRankVc.view.frame = CGRectMake(0, KScreenHeight, KScreenWidth, 350);
    }
    _contributeRankVc.view.hidden = NO;
    _shadowView.hidden = NO;
    [UIView animateWithDuration:0.2 animations:^{
        self->_contributeRankVc.view.mj_y = KScreenHeight - 350;
    }];
    [_contributeRankVc reqData];
}

- (IBAction)chooseCategoryClick:(UIButton *)sender{
    [self.view endEditing:YES];
    NSMutableArray *modelArray = [[NSMutableArray alloc]init];
    int selectedIndex = 0;
    for (int i = 0; i < configManager.liveCategorys.count; i++) {
        NSDictionary *dic = configManager.liveCategorys[i];
        BRResultModel *model = [[BRResultModel alloc]init];
        model.index = i;
        model.value = dic[@"title"];
        model.key = [NSString stringWithFormat:@"%d",[dic[@"id"] intValue]];
        [modelArray addObject:model];
        if ([dic[@"id"] integerValue] == _selectedCategoryId) {
            selectedIndex = i;
        }
    }
    BRStringPickerView *categoryPickerView = [[BRStringPickerView alloc]initWithPickerMode:BRStringPickerComponentSingle];
    categoryPickerView.dataSourceArr = [modelArray copy];
    categoryPickerView.selectIndex = selectedIndex;
    categoryPickerView.resultModelBlock = ^(BRResultModel *resultModel) {
        self->_selectedCategoryId = [resultModel.key integerValue];
        [sender setTitle:resultModel.value forState:UIControlStateNormal];
    };
    [categoryPickerView show];
}

- (IBAction)startLiveClick:(UIButton *)sender {
    NSString *title = self.titleTextField.text;
    if (title.length == 0 || [title isEqualToString:@"给直播写个标题吧"]) {
        [self.titleTextField becomeFirstResponder];
        return;
    }
    if (_selectedCategoryId == 0) {
        [self chooseCategoryClick:self.chooseCategoryBtn];
        return;
    }
    if (_thumbUrl.length == 0) {
        [self uploadImage];
    }else{
        [self startLive];
    }
    
}

- (IBAction)showGoodsBtnClick:(UIButton *)sender {
    [self shadowViewTap];
    if (!_goodsVc) {
        _goodsVc = [[LiveGoodsViewController alloc]init];
        _goodsVc.goodsList = self.goodsArray;
        _goodsVc.isAnchor = YES;
        _goodsVc.chatroomid = self.liveModel.chatroomid;
        _goodsVc.explainingID = self.explainingGoodsid;
        [self addChildViewController:_goodsVc];
        [self.view addSubview:_goodsVc.view];
        _goodsVc.view.frame = CGRectMake(0, KScreenHeight, KScreenWidth, 450);
    }
    [UIView animateWithDuration:0.2 animations:^{
        self->_shadowView.hidden = NO;
        self->_shadowView.alpha = 1;
        self->_goodsVc.view.mj_y = KScreenHeight - 450;
    }];
}

- (IBAction)linkBtnClick:(UIButton *)sender {
    int onoff = self.liveModel.link_on ? 0 : 1;
    [_liveManager turnLinkOnOff:onoff success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.liveModel.link_on = !self.liveModel.link_on;
            self.linkBtn.selected = self.liveModel.link_on;
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (IBAction)linkCloseBtnClick:(UIButton *)sender {
    [commonManager showLoadingAnimateInWindow];
    [_liveManager stopLinkWithUser:linkerid anchor:self.liveModel.anchorid success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            [self stopLink];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (IBAction)startPkBtnClick:(id)sender {
    [self shadowViewTap];
    if (self.liveModel.link_status) {
        [MBProgressHUD showTipMessageInView:@"当前正在连麦中"];
        return;
    }
    if (self.liveModel.room_type == LiveRoomTypeFee) {
        [MBProgressHUD showTipMessageInView:@"付费房间无法进行PK"];
        return;
    }
    if (self.liveModel.room_type == LiveRoomTypePwd) {
        [MBProgressHUD showTipMessageInView:@"私密房间无法进行PK"];
        return;
    }
    self.liveModel.pk_status = PkStatusFinding;
    self.pkActionView.hidden = NO;
    [self.pkActionBtn setTitle:@"匹配中" forState:UIControlStateNormal];
    [self.pkActionView insertSubview:self.pkFindingAnimation belowSubview:self.pkActionBtn];
    
    [_liveManager requestEnterPkModeSuccess:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            
        }else{
            self.liveModel.pk_status = PkStatusNO;
            self.pkActionView.hidden = YES;
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        self.liveModel.pk_status = PkStatusNO;
        self.pkActionView.hidden = YES;
        RESP_FAILURE;
    }];
}

- (IBAction)settingBtnClick:(UIButton *)sender{
    [self shadowViewTap];
    if (!_liveManageVc) {
        _liveManageVc = [[LiveManageViewController alloc]init];
        _liveManageVc.anchorid = self.liveModel.anchorid;
        [self addChildViewController:_liveManageVc];
        [self.view addSubview:_liveManageVc.view];
        _liveManageVc.view.frame = CGRectMake(0, KScreenHeight, KScreenWidth, 500);
    }
    _liveManageVc.view.hidden = NO;
    _shadowView.hidden = NO;
    [UIView animateWithDuration:0.2 animations:^{
        self->_liveManageVc.view.mj_y = KScreenHeight - 500;
    }];
    [_liveManageVc refreshData];
}

- (IBAction)pkAnchorAttentBtnClick:(UIButton *)sender {
    if (!userManager.isLogined) {
        [kAppDelegate showLoginViewController];
        return;
    }
    [CommonManager POST:@"Anchor/attentAnchor" parameters:@{@"anchorid":@(self.liveModel.pklive.anchorid),@"type":@"1"} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            sender.hidden = YES;
            self.liveModel.pklive.anchor.isattent = YES;
            self.liveModel.pklive.anchor.fans_count = [responseObject[@"data"][@"fans_count"] intValue];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (IBAction)pkActionBtnClick:(UIButton *)sender {
    if (self.liveModel == PkStatusNO) {
        [self endPk];
        return;
    }
    [_liveManager requestEndPkModeSuccess:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            [self endPk];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (void)shadowViewTap{
    self.controlView.hidden = !_inLive;
    self.prepareView.hidden = _inLive;
    
    _shadowView.hidden = YES;
    if (!_beautyPanel.hidden) {
        [UIView animateWithDuration:0.2 animations:^{
            self->_beautyPanel.mj_y = KScreenHeight;
        } completion:^(BOOL finished) {
            self->_beautyPanel.hidden = YES;
        }];
    }
    if (!_contributeRankVc.view.hidden) {
        [UIView animateWithDuration:0.2 animations:^{
            self->_contributeRankVc.view.mj_y = KScreenHeight;
        } completion:^(BOOL finished) {
            self->_contributeRankVc.view.hidden = YES;
        }];
    }
    if (!_messageVc.view.hidden) {
        [UIView animateWithDuration:0.2 animations:^{
            self->_messageVc.view.mj_y = KScreenHeight;
        } completion:^(BOOL finished) {
            self->_messageVc.view.hidden = YES;
        }];
    }
    if (!_goodsVc.view.hidden) {
        [UIView animateWithDuration:0.2 animations:^{
            self->_goodsVc.view.mj_y = KScreenHeight;
        } completion:^(BOOL finished) {
            self->_goodsVc.view.hidden = YES;
        }];
    }
    if (!_liveManageVc.view.hidden) {
        [UIView animateWithDuration:0.2 animations:^{
            self->_liveManageVc.view.mj_y = KScreenHeight;
        } completion:^(BOOL finished) {
            self->_liveManageVc.view.hidden = YES;
        }];
    }
    if (!_guardVc.view.hidden) {
        [UIView animateWithDuration:0.2 animations:^{
            self->_guardVc.view.mj_y = KScreenHeight;
        } completion:^(BOOL finished) {
            self->_guardVc.view.hidden = YES;
        }];
    }
    
    if (self.typeViewBottomLC.constant == 0) {
        self.typeViewBottomLC.constant = -100;
        self.startBtnBottomLC.constant = 35;
        [UIView animateWithDuration:0.2 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
    if (self.roomOptViewBottomLC.constant == 0) {
        self.roomOptViewBottomLC.constant = -100;
        [UIView animateWithDuration:0.2 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

- (void)switchBeautyBtnClick:(UIButton *)senderBtn{
    if (senderBtn.selected) {
        return;
    }
    senderBtn.selected = YES;
    _filterBtn.selected = NO;
    
    _beautyView.hidden = NO;
    _filterView.hidden = YES;
    
    [UIView animateWithDuration:0.2 animations:^{
        self->_underline.centerX = senderBtn.centerX;
    }];
}

- (void)switchFilterBtnClick:(UIButton *)senderBtn{
    if (senderBtn.selected) {
        return;
    }
    senderBtn.selected = YES;
    _beautyBtn.selected = NO;
    
    _beautyView.hidden = YES;
    _filterView.hidden = NO;
    
    [UIView animateWithDuration:0.2 animations:^{
        self->_underline.centerX = senderBtn.centerX;
    }];
}

- (void)beautySliderValueChanged:(UISlider *)slider{
    _beautyLevel = slider.value;
    [[_pusher getBeautyManager] setBeautyLevel:_beautyLevel];
}

- (void)whiteySliderValueChanged:(UISlider *)slider{
    _whiteLevel = slider.value;
    [[_pusher getBeautyManager] setWhitenessLevel:_whiteLevel];
}

- (void)ruddySliderValueChanged:(UISlider *)slider{
    _ruddyLevel = slider.value;
    [[_pusher getBeautyManager] setRuddyLevel:_ruddyLevel];
}

- (void)filterSliderValueChanged:(UISlider *)slider{
    _filterValue = slider.value;
    [[_pusher getBeautyManager]setFilterStrength:_filterValue/10.0];
}

- (void)filterBtnClick:(UIButton *)senderBtn{
    if (senderBtn.selected) {
        return;
    }
    _selectedFilterBtn.selected = NO;
    senderBtn.selected = YES;
    _selectedFilterBtn = senderBtn;
    _selectedFilterIndex = senderBtn.tag;
    
    UIImage *lutImage = [self filterImageByMenuOptionIndex:_selectedFilterIndex+1];
    [[_pusher getBeautyManager]setFilter:lutImage];
    [[_pusher getBeautyManager]setFilterStrength:_filterValue/10.0];
}

- (void)chooseThumbTap:(UITapGestureRecognizer *)tapGes{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
    imagePickerVc.allowTakeVideo = NO;
    imagePickerVc.allowPickingGif = NO;
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.showPhotoCannotSelectLayer = YES;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.naviTitleColor = [UIColor colorWithHexString:@"333333"];
    imagePickerVc.barItemTextColor = [UIColor colorWithHexString:@"333333"];
    imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
    imagePickerVc.allowCrop = YES;
    imagePickerVc.cropRect = CGRectMake(20, (KScreenHeight - KScreenWidth + 40)/2, KScreenWidth - 40, KScreenWidth - 40);
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        if (photos.count > 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self->_thumbUrl = @"";
                UIImage *selImg = photos[0];
                self.selectThumbImgView.image = selImg;
                self->_thumbImage = selImg;
            });
        }
    }];
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (UIImage*)filterImageByMenuOptionIndex:(NSInteger)index
{
    if (index == 0) {
        return nil;
    }
    TCAVFilter *filter = _filters[index-1];
    return [UIImage imageWithContentsOfFile:filter.lookupImagePath];
}

- (UIImage *)iconForFilter:(nonnull NSString *)filter selected:(BOOL)selected{
    if (nil == filter) {
        return [UIImage imageNamed:[NSString stringWithFormat:@"original%@",selected?@"_pre":@""]];
    } else if ([filter isEqualToString:@"white"]) {
        return [UIImage imageNamed:[NSString stringWithFormat:@"fwhite%@",selected?@"_pre":@""]];
    } else {
        return [UIImage imageNamed:[NSString stringWithFormat:@"%@%@",filter,selected?@"_pre":@""]];
    }
}

- (void)uploadImage{
    if (!_thumbImage) {
        [MBProgressHUD showTipMessageInWindow:@"请先选择一张封面"];
        return;
    }
    [commonManager showLoadingAnimateInWindow];
    kWeakSelf(self);
    [CommonManager POST:@"Config/getTempKeysForCos" parameters:@{} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            configManager.txCosModel = [TxCosModel yy_modelWithDictionary:responseObject[@"data"]];
            [configManager saveTxCosModel];
            NSString *fileName = [CommonManager getNameBaseCurrentTime:@".jpg"];
            NSString *flieUploadPath = [NSString stringWithFormat:@"%@/%@",configManager.appConfig.cos_folder_image,fileName];
            QCloudCOSXMLUploadObjectRequest* put = [QCloudCOSXMLUploadObjectRequest new];
            put.object = flieUploadPath;
            put.bucket = configManager.appConfig.cos_bucket;
            put.body = UIImageJPEGRepresentation(weakself.selectThumbImgView.image, 1);
            [put setFinishBlock:^(QCloudUploadObjectResult *result, NSError* error) {
                //可以从 result 获取结果
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (result && !error) {
                        self->_thumbUrl = result.location;
                        [weakself startLive];
                    }else{
                        [commonManager hideAnimateHud];
                        [commonManager showErrorAnimateInWindow];
                    }
                });
            }];
            [[QCloudCOSTransferMangerService defaultCOSTransferManager] UploadObject:put];
        }else{
            [commonManager hideAnimateHud];
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)startLive{
    [commonManager showLoadingAnimateInWindow];
    NSDictionary *params = @{@"cateid":@(_selectedCategoryId)
                             ,@"thumb":_thumbUrl
                             ,@"title":self.titleTextField.text
                             ,@"orientation":@(2)
                             ,@"room_type":@(self.selectedRoomTypeValue)
                             ,@"price":@(self.price)
                             ,@"pwd":self.pwd
    };
    
    kWeakSelf(self);
    [_liveManager createMlvbLiveParameters:params success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            weakself.prepareView.hidden = YES;
            weakself.controlView.hidden = NO;
            self->_inLive = YES;
            weakself.liveModel = [LiveModel yy_modelWithDictionary:responseObject[@"data"]];
            [self->_pusher startPush:self.liveModel.push_url];

            [weakself.timer fire];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)sendStopMsg{
    NSDictionary *dict = @{@"Action":@"LiveFinished"};
    
    [[V2TIMManager sharedInstance]sendGroupCustomMessage:[NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil] to:self.liveModel.chatroomid priority:V2TIM_PRIORITY_NORMAL succ:nil fail:nil];
}

- (IBAction)roomTypeBtnClick:(UIButton *)sender {
    _shadowView.hidden = NO;
    self.typeViewBottomLC.constant = 0;
    self.startBtnBottomLC.constant = 135;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)roomTypeNormalBtnClick:(UIButton *)sender {
    [self.roomTypeBtn setTitle:@"普通房间" forState:UIControlStateNormal];
    self.selectedRoomTypeValue = 0;
    self.pwd = @"";
    self.price = 0;
    sender.selected = YES;
    self.feeRoomTypeBtn.selected = NO;
    self.pwdRoomTypeBtn.selected = NO;
    [self shadowViewTap];
}

- (IBAction)roomTypeFeeBtnClick:(UIButton *)sender {
    [self.roomTypeBtn setTitle:@"付费房间" forState:UIControlStateNormal];
    self.selectedRoomTypeValue = 2;
    self.pwd = @"";
    sender.selected = YES;
    self.normalRoomTypeBtn.selected = NO;
    self.pwdRoomTypeBtn.selected = NO;
    [self shadowViewTap];
    QNAlertView *alertView = [QNAlertView showQNTextFieldWithTitle:@"付费房间" placeHolder:@"请设置房间价格（金币/分钟）" textFieldType:QNTextFieldTypeNumber leftButtonTitle:nil leftClick:nil rightButtonTitle:@"确定" rightClick:^(NSString *value){
        self.price = [value intValue];
    }];
    alertView.cancelBackTap = YES;
    alertView.requireInput = YES;
}

- (IBAction)roomTypePwdBtnClick:(UIButton *)sender {
    [self.roomTypeBtn setTitle:@"私密房间" forState:UIControlStateNormal];
    self.selectedRoomTypeValue = 1;
    self.price = 0;
    sender.selected = YES;
    self.normalRoomTypeBtn.selected = NO;
    self.feeRoomTypeBtn.selected = NO;
    [self shadowViewTap];
    
    QNAlertView *alertView = [QNAlertView showQNTextFieldWithTitle:@"私密房间" placeHolder:@"请设置房间密码" textFieldType:QNTextFieldTypeNoSpecialWord leftButtonTitle:nil leftClick:nil rightButtonTitle:@"确定" rightClick:^(NSString *value){
        self.pwd = value;
    }];
    alertView.cancelBackTap = YES;
    alertView.requireInput = YES;
}

- (IBAction)moreFunClick:(UIButton *)sender {
    _shadowView.hidden = NO;
    self.roomOptViewBottomLC.constant = 0;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
}


- (void)displaySystemMessage:(NSString *)message{
    RoomChatModel *sysChatModel = [[RoomChatModel alloc]init];
    sysChatModel.chatType = RoomChatTypeSysMsg;
    sysChatModel.message = message;
    [_chatArray addObject:sysChatModel];
    [self.chatTableView reloadData];
    //滚动到最底部
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:_chatArray.count-1];
    [self.chatTableView scrollToRowAtIndexPath:indexpath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

- (void)mergeStreamStart{
    //调用混流接口开始混流
    [_liveManager mergeStreamWithUser:linkerid success:^(id responseObject) {
        if (!(RESP_SUCCESS(responseObject))) {
            self->mergeRetryTimes ++;
            if (self->mergeRetryTimes<5) {
                [self mergeStreamStart];
            }
        }
    } failure:^(NSError *error) {
        self->mergeRetryTimes ++;
        if (self->mergeRetryTimes<5) {
            [self mergeStreamStart];
        }
    }];
}

- (void)setupPlayer:(SuperPlayerView *)player videoUrl:(NSString *)videoUrl{
    SuperPlayerModel *playerModel = [[SuperPlayerModel alloc] init];
    // 设置播放地址，直播、点播都可以
    playerModel.videoURL = videoUrl;
    // 开始播放
    [player playWithModel:playerModel];
}

- (void)showPkStartAnimation{
    [self.view addSubview:self.svgaPkPlayer];
    [self.svgaPkPlayer startAnimation];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.svgaPkPlayer stopAnimation];
        [self.svgaPkPlayer removeFromSuperview];
    });
}

- (void)refreshPkScoreWithAnimation:(BOOL)animation{
    int home_score = self.liveModel.pkinfo.home_score;
    int away_score = self.liveModel.pkinfo.away_score;
    
    BOOL score1Changed = [self.scoreLabel1.text intValue] != home_score;
    BOOL score2Changed = [self.scoreLabel2.text intValue] != away_score;
    
    self.scoreLabel1.text = @(home_score).stringValue;
    self.scoreLabel2.text = @(away_score).stringValue;
    
    self.scoreView1.layer.cornerRadius = home_score > away_score ? 8.5 : 0;
    self.scoreView2.layer.cornerRadius = home_score < away_score ? 8.5 : 0;
    
    CGFloat totleWidth = kScreenWidth + 8.5*2;
    if (home_score + away_score == 0) {
        self.scoreView1WidthLC.constant = totleWidth / 2;
        self.scoreView2WidthLC.constant = totleWidth / 2;
    }else{
        CGFloat homeWidth = totleWidth * (home_score*1.0 / (home_score+away_score)) + 8.5;
        CGFloat awayWidth = totleWidth * (away_score*1.0 / (home_score+away_score)) + 8.5;
        if (homeWidth < 50) {
            homeWidth = 50;
            awayWidth = totleWidth - 50 + 8.5;
        }else if(awayWidth < 50){
            awayWidth = 50;
            homeWidth = totleWidth - 50 + 8.5;
        }
        self.scoreView1WidthLC.constant = homeWidth;
        self.scoreView2WidthLC.constant = awayWidth;
        [self.pkView bringSubviewToFront:home_score>away_score ? self.scoreView1 : self.scoreView2];
        if (animation) {
            [UIView animateWithDuration:0.2 animations:^{
                [self.scoreLabel1 layoutIfNeeded];
                [self.scoreLabel2 layoutIfNeeded];
            }];
            if (score1Changed) {
                CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
                //速度控制函数，控制动画运行的节奏
                animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                animation.duration = 0.2;       //执行时间
                animation.repeatCount = 1;      //执行次数
                animation.autoreverses = YES;    //完成动画后会回到执行动画之前的状态
                animation.fromValue = [NSNumber numberWithFloat:1.0];   //初始伸缩倍数
                animation.toValue = [NSNumber numberWithFloat:1.5];     //结束伸缩倍数
                [[self.scoreLabel1 layer]addAnimation:animation forKey:nil];
            }
            if (score2Changed) {
                CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
                //速度控制函数，控制动画运行的节奏
                animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                animation.duration = 0.2;       //执行时间
                animation.repeatCount = 1;      //执行次数
                animation.autoreverses = YES;    //完成动画后会回到执行动画之前的状态
                animation.fromValue = [NSNumber numberWithFloat:1.0];   //初始伸缩倍数
                animation.toValue = [NSNumber numberWithFloat:1.5];     //结束伸缩倍数
                [[self.scoreLabel2 layer]addAnimation:animation forKey:nil];
            }
        }
    }
}

//PK计时
- (void)caculatePkTime{
    if (self.liveModel.pk_status == PkStatusInPk && self.liveModel.pkinfo) {
        int timeDura = (int)[[NSDate date] timeIntervalSinceDate:[CommonManager dateWithString:self.liveModel.pkinfo.create_time]];
        if (timeDura >= 8 * 60) {
            //pk 已结束
            self.pkStatus = 0;
        }else if (timeDura >= 6 * 60) {
            //pk 惩罚中
            self.pkStatus = 2;
            int leftSecond = 8*60 - timeDura;
            NSString *minute = @(leftSecond / 60).stringValue;
            NSString *second = leftSecond % 60 >= 10 ? @(leftSecond % 60).stringValue:[NSString stringWithFormat:@"0%d",leftSecond % 60];
            self.pkTimerLabel.text = [NSString stringWithFormat:@"惩罚中 0%@:%@",minute,second];
        }else{
            self.pkStatus = 1;
            int leftSecond = 6*60 - timeDura;
            NSString *minute = @(leftSecond / 60).stringValue;
            NSString *second = leftSecond % 60 >= 10 ? @(leftSecond % 60).stringValue:[NSString stringWithFormat:@"0%d",leftSecond % 60];
            self.pkTimerLabel.text = [NSString stringWithFormat:@"倒计时 0%@:%@",minute,second];
        }
    }else{
        self.pkStatus = 0;
    }
}

//该字段只允许通过倒计时方法修改值
- (void)setPkStatus:(int)pkStatus{
    if (_pkStatus == pkStatus) {
        return;
    }
    _pkStatus = pkStatus;
    
    int home_score = self.liveModel.pkinfo.home_score;
    int away_score = self.liveModel.pkinfo.away_score;
    
    self.pkResultImgView1.hidden = (_pkStatus != 2) || (home_score == away_score);
    self.pkResultImgView2.hidden = (_pkStatus != 2) || (home_score == away_score);
    self.pkResultImgView3.hidden = (_pkStatus != 2) || (home_score != away_score);
    
    if (_pkStatus == 2) {
        //进入惩罚状态 显示动画
        if (home_score != away_score) {
            self.pkResultImgView1.image = nil;
            self.pkResultImgView2.image = nil;
            [self showPkWinAnimationToView:home_score > away_score ? self.pkResultImgView1:self.pkResultImgView2];
            home_score > away_score ? (self.pkResultImgView2.image = IMAGE_NAMED(@"ic_pk_lose@3x.png")) : (self.pkResultImgView1.image = IMAGE_NAMED(@"ic_pk_lose@3x.png"));
        }
    }else if (_pkStatus == 1){
        
    }else{
        if (self.liveModel.pk_status != PkStatusNO) {
            //倒计时结束 自动调用结束pk接口
            [self pkActionBtnClick:self.pkActionBtn];
        }
        [self endPk];
    }
}

- (void)showPkWinAnimationToView:(UIView *)view{
    [view addSubview:self.svgaPkWinPlayer];
    [self.svgaPkWinPlayer startAnimation];
}

- (void)hidePkWinAnimation{
    [self.svgaPkWinPlayer stopAnimation];
    [self.svgaPkWinPlayer removeFromSuperview];
}

#pragma mark ————————————————————————— NSNotification ————————————————————————
-(void)imGroupEventNotification:(NSNotification *)notification{
    IMNotificationModel *notificationModel = [notification userInfo][@"data"];
    if ([notificationModel.roomid isEqualToString:self.liveModel.chatroomid]) {
        switch (notificationModel.action) {
            case IMNotificationActionLiveGroupMemberJoinExit:{
                [[TIMGroupManager sharedInstance] getGroupInfo:@[self.liveModel.chatroomid] succ:^(NSArray *groupList) {
                    if (groupList.count > 0) {
                        TIMGroupInfo *info = groupList[0];
                        self.audienceCountLabel.text = [NSString stringWithFormat:@"%d",info.memberNum];
                    }
                } fail:^(int code, NSString *msg) {
                    
                }];
            }
                break;
            default:
                break;
        }
    }
}
    
- (void)imNotification:(NSNotification *)notification{
    IMNotificationModel *notificationModel = [notification userInfo][@"data"];
    if ([notificationModel.roomid isEqualToString:self.liveModel.chatroomid]) {
        switch (notificationModel.action) {
            case IMNotificationActionRoomMessage:
                [self reciveRoomMsg:notificationModel];
                break;
            case IMNotificationActionRoomNotification:
                [self roomNotification:notificationModel];
                break;
            default:
                break;
        }
    }
}

- (void)reciveRoomMsg:(IMNotificationModel *)notificationModel{
    if ([notificationModel.roomid isEqualToString: self.liveModel.chatroomid]) {
        RoomChatModel *chatModel = [notificationModel.chat yy_modelCopy];
        chatModel.textColor = @"#FFFFFF";
        chatModel.nameColor = @"40ACFF";
        [_chatArray addObject:chatModel];
        [self.chatTableView reloadData];
        //滚动到最底部
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:_chatArray.count-1];
        [self.chatTableView scrollToRowAtIndexPath:indexpath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        
        if (([chatModel.sender getVipLevel] > 0 || chatModel.is_guardian) && [chatModel.message isEqualToString:KEnterRoomMessage]) {
            //VIP入场提示
            if (!_vipEnterAnimationView) {
                CGPoint msgTablePoint = [self.chatTableView convertPoint:self.chatTableView.bounds.origin toView:self.view];
                _vipEnterAnimationView = [VipEnterAnimationView createVipEnterAnimationViewWithYPosition:msgTablePoint.y - 44 - 7];
                [self.view insertSubview:_vipEnterAnimationView belowSubview:self.controlView];
            }
            [_vipEnterAnimationView addModels:notificationModel];
            if(!_vipEnterAnimationView.animating){
                [_vipEnterAnimationView enAnimation];
            }
        }
    }
}

- (void)roomNotification:(IMNotificationModel *)notificationModel{
    switch (notificationModel.notify.type) {
        case RoomNotifyTypeSetManager:{
            [self displaySystemMessage:[NSString stringWithFormat:@"%@ 被主播设置为房管",notificationModel.notify.user.nick_name]];
        }
            break;
        case RoomNotifyTypeCancelManager:{
            [self displaySystemMessage:[NSString stringWithFormat:@"%@ 房管权限已被取消",notificationModel.notify.user.nick_name]];
        }
            break;
        case RoomNotifyTypeGuardAnchor:{
            [self getGuardianCount];
            [self displaySystemMessage:[NSString stringWithFormat:@"%@ 守护了主播",notificationModel.notify.user.nick_name]];
        }
            break;
        case RoomNotificationReciveLinkRequest:{
            //收到连麦请求
            kWeakSelf(self);
            [QNAlertView showAlertViewWithImageUrl:notificationModel.notify.user.avatar title:@"连麦" contentText:[NSString stringWithFormat:@"收到用户%@（ID:%lld）的连麦请求",notificationModel.notify.user.nick_name,notificationModel.notify.user.userid] leftButtonTitle:@"拒绝" leftClick:^(void) {
                [weakself refuseLinkRequest:(int)notificationModel.notify.user.userid];
            } rightButtonTitle:@"接受" rightClick:^(void) {
                [weakself acceptLinkRequest:(int)notificationModel.notify.user.userid linkAccUrl:notificationModel.notify.link_acc_url];
            }];
        }
            break;
        case RoomNotificationStopLink:{
            [self stopLink];
        }
            break;
        case RoomNotifyTypePkStart:{
            //开始pk
            self.liveModel.pk_status = PkStatusInPk;
            self.liveModel.pkinfo = notificationModel.notify.pkinfo;
            self.liveModel.pklive = notificationModel.notify.pklive;
            [self caculatePkTime];
            [self resetPlayer];
        }
            break;
        case RoomNotifyTypePkEnd:{
            //pk结束
            if (self.liveModel.pk_status == PkStatusNO) {
                return;
            }
            [self endPk];
        }
            break;
        case RoomNotifyTypePkScoreChange:{
            //PK比分变化
            self.liveModel.pkinfo.home_score = notificationModel.notify.pkinfo.home_score;
            self.liveModel.pkinfo.away_score = notificationModel.notify.pkinfo.away_score;
            [self refreshPkScoreWithAnimation:YES];
        }
            break;
        default:
            break;
    }
}

- (void)acceptLinkRequest:(int)userid linkAccUrl:(NSString *)linkAccUrl{
    [commonManager showLoadingAnimateInWindow];
    [_liveManager acceptLinkWithUser:userid success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            self.liveModel.link_status = YES;
            // 设置视频质量为大主播(连麦模式)
            [self->_pusher setVideoQuality:VIDEO_QUALITY_LINKMIC_MAIN_PUBLISHER adjustBitrate:NO adjustResolution:NO];
            self->linkerid = userid;
            self.linkPlayerContainerView.hidden = NO;
            SuperPlayerModel *playerModel = [[SuperPlayerModel alloc] init];
            // 设置播放地址，直播、点播都可以
            playerModel.videoURL = linkAccUrl;
            // 开始播放
            [self.superPlayer playWithModel:playerModel];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)refuseLinkRequest:(int)userid{
    [commonManager showLoadingAnimateInWindow];
    [_liveManager refuseLinkWithUser:userid success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)stopLink{
    // 设置视频质量高清
    [self->_pusher setVideoQuality:VIDEO_QUALITY_HIGH_DEFINITION adjustBitrate:NO adjustResolution:NO];
    self.linkPlayerContainerView.hidden = YES;
    [self.superPlayer resetPlayer];
    
    self.liveModel.link_status = NO;
}

#pragma mark ———————————————————  礼物弹出及队列显示开始 ——————————————————
- (void)giftAnimationNotification:(NSNotification *)notification{
    //刷新直播信息 收益
    [self getLiveBasicInfo];
    
    IMNotificationModel *notificationModel = [notification userInfo][@"data"];
    if (notificationModel.gift.type == GiftTypeNormal) {
        if (!_continueGiftView) {
            _continueGiftView = [[ContinueGiftView alloc]initWithFrame:CGRectMake(0, 0, _normalGiftBackView.width, _normalGiftBackView.height)];
            [_normalGiftBackView addSubview:_continueGiftView];
            //初始化礼物空位
            [_continueGiftView initGift];
        }
        [_continueGiftView popGiftWithNotification:notificationModel];
    }else if (notificationModel.gift.type == GiftTypeSplendid){
        if (!_giftShowView) {
            _giftShowView = [[GiftShowView alloc]init];
            _giftShowView.delegate = self;
            [self.view addSubview:_giftShowView];
        }
        if (notificationModel.gift != nil) {
            [_giftShowView addModels:notificationModel];
        }
        if(!_giftShowView.animating){
            [_giftShowView enAnimationGift];
        }
    }
    
    [self displaySystemMessage:[NSString stringWithFormat:@"%@ 送给主播 %@x%d", notificationModel.gift.sender.nick_name, notificationModel.gift.title, notificationModel.gift.count]];
}

- (void)onAppDidEnterBackGround:(NSNotification *)notification {
    if (_inLive) {
        [_pusher pausePush];
    }
}

- (void)onAppWillEnterForeground:(NSNotification *)notification {
    if (_inLive) {
        [_pusher resumePush];
    }
}

#pragma mark ————————————————————— SharePanelViewDelegate ————————————————————————
- (void)shareWithChannel:(PYShareChannel)channel panel:(nonnull SharePanelView *)panelView{
    if (channel == PYShareChannelLink){
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = [NSString stringWithFormat:@"%@%lld",[configManager appConfig].share_live_url,self.liveModel.anchor.userid];
        [MBProgressHUD showTipMessageInView:@"链接已复制到剪贴板"];
    }else{
        SSDKPlatformType type = [SharePanelView getSSDKPlatformTypeBy:channel];
        //统一创建分享参数
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:@"想和我的距离再近一点，那我的直播你必须到场，与你不期而遇"
                                         images:self.liveModel.thumb
                                            url:[NSURL URLWithString:[NSString stringWithFormat:@"%@%lld",[configManager appConfig].share_live_url,self.liveModel.anchor.userid]]
                                          title:[NSString stringWithFormat:@"我已开播，快来围观吧~"]
                                           type:SSDKContentTypeAuto];
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            switch (state) {
                case SSDKResponseStateSuccess:
                    [MBProgressHUD showTipMessageInView:@"分享成功"];
                    break;
                case SSDKResponseStateFail:
                {
                    [MBProgressHUD showTipMessageInView:@"分享失败"];
                    //失败
                    break;
                }
                case SSDKResponseStateCancel:
                    //取消
                    break;
                    
                default:
                    break;
            }
        }];
    }
}

#pragma mark —————————————————— TableViewDelegate Datasource ———————————————————————
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _chatArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RoomChatTableViewCell *cell = [RoomChatTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.chatModel = _chatArray[indexPath.section];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 5;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 5)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RoomChatModel *chatModel = _chatArray[indexPath.section];
    if (chatModel.sender.userid == 0) {
        return;
    }
    [UserCardView showSettingUserCardInView:self.view userModel:chatModel.sender delegate:self];
}

#pragma mark ————————————————————————————————————— UserCardViewDelegate —————————————————————————————————————
- (void)UserCardViewMsgBtnClick:(UserCardView *)view{
    TUIConversationCellData *data = [[TUIConversationCellData alloc]init];
    data.userID = [NSString stringWithFormat:@"%lld",view.userModel.userid];
    data.faceUrl = view.userModel.avatar;
    MessageChatViewController *vc = [[MessageChatViewController alloc]initWithConversationCellData:data senderName:view.userModel.nick_name];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)UserCardViewPageBtnClick:(UserCardView *)view{
    UserInfoViewController *vc = [[UserInfoViewController alloc]init];
    vc.anchorid = view.userModel.userid;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)UserCardViewOptBtnClick:(UserCardView *)view{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"设置" message:[NSString stringWithFormat:@"用户：%@",view.userModel.nick_name] preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"禁言" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [CommonManager POST:@"live/banUser" parameters:@{@"anchorid":@(self.liveModel.anchorid),@"userid":@(view.userModel.userid),@"type":@(1)} success:^(id responseObject) {
            if (RESP_SUCCESS(responseObject)) {
                [MBProgressHUD showTipMessageInView:@"禁言成功"];
            }else{
                RESP_SHOW_ERROR_MSG(responseObject);
            }
        } failure:^(NSError *error) {
            RESP_FAILURE;
        }];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"设为管理员" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [CommonManager POST:@"live/setRoomMgr" parameters:@{@"mgrid":@(view.userModel.userid),@"type":@(1)} success:^(id responseObject) {
            if (RESP_SUCCESS(responseObject)) {
                [MBProgressHUD showTipMessageInView:@"设置成功"];
            }else{
                RESP_SHOW_ERROR_MSG(responseObject);
            }
        } failure:^(NSError *error) {
            RESP_FAILURE;
        }];
    }];
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:action1];
    [alertVC addAction:action2];
    [alertVC addAction:action3];
    [self presentViewController:alertVC animated:YES completion:nil];
}

- (void)UserCardViewVipChargeBtnClick:(UserCardView *)view{
    VipCenterViewController *vc = [[VipCenterViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark ———————————————————————— UITextFieldDelegate ————————————————————————
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == self.titleTextField) {
        if ([self.titleTextField.text isEqualToString:@"给直播写个标题吧"]) {
            self.titleTextField.text = @"";
        }
    }
}

#pragma mark —————————————————————————— TXLivePushListener —————————————————————
- (void)onNetStatus:(NSDictionary *)param{
    
}

- (void)onPushEvent:(int)EvtID withParam:(NSDictionary *)param{
    NSLog(@"%d",EvtID);
}

#pragma mark ———————————————————————— TXVideoCustomProcessDelegate ——————————————————————

- (GLuint)onPreProcessTexture:(GLuint)texture width:(CGFloat)width height:(CGFloat)height{
    if(configManager.appConfig.beauty_channel == 0)
        return texture;
    return [[TiSDKManager shareManager] renderTexture2D:texture Width:width Height:height Rotation:CLOCKWISE_0 Mirror:_pusher.frontCamera];
}

/**
 * 在OpenGL线程中回调，可以在这里释放创建的OpenGL资源
 */
- (void)onTextureDestoryed{
    NSLog(@"onTextureDestoryed");
    if(configManager.appConfig.beauty_channel == 1){
        dispatch_async(dispatch_get_main_queue(), ^{
            [[TiSDKManager shareManager] destroy];
            [[TiUIManager shareManager] destroy]; // TiSDK开源UI窗口对象资源释放
        });
    }
}

#pragma mark ———————————————————————— TiUIManagerDelegate ————————————————————————————
- (void)didClickOnExitTap{
    [self shadowViewTap];
}

#pragma mark ———————————————————————— SuperPlayerDelegate ————————————————————————————
/// 播放开始通知
- (void)superPlayerDidStart:(SuperPlayerView *)player{
    [self mergeStreamStart];
}

- (void)superPlayerError:(SuperPlayerView *)player errCode:(int)code errMessage:(NSString *)why{
    //小主播流播放错误 停止连麦
    [self stopLink];
    NSLog(@"superPlayerError");
}

- (void)superPlayerDidEnd:(SuperPlayerView *)player{
    NSLog(@"superPlayerDidEnd");
}

#pragma mark ——————————————————————— 懒加载 ———————————————————————————

- (NSTimer *)timer{
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerGo) userInfo:nil repeats:YES];
    }
    return _timer;
}

- (SuperPlayerView *)superPlayer{
    if (!_superPlayer) {
        _superPlayer = [[SuperPlayerView alloc] init];
        // 设置代理，用于接受事件
        _superPlayer.delegate = self;
        _superPlayer.loop = YES;
        _superPlayer.isLockScreen = YES;
        _superPlayer.playerConfig.renderMode = 0;
        _superPlayer.layoutStyle = SuperPlayerLayoutStyleFullScreen;
        _superPlayer.controlView.hidden = YES;
    }
    return _superPlayer;
}

- (NSMutableArray *)shareChannelArray1{
    if (!_shareChannelArray1) {
        _shareChannelArray1 = [NSMutableArray arrayWithArray:@[@(PYShareChannelWechat),@(PYShareChannelPengyouquan),@(PYShareChannelQQ),@(PYShareChannelWibo),@(PYShareChannelQZone),@(PYShareChannelMore)]];
    }
    return _shareChannelArray1;
}

- (NSMutableArray *)shareChannelArray2{
    if (!_shareChannelArray2) {
        _shareChannelArray2 = [NSMutableArray arrayWithArray:@[@(PYShareChannelLink)]];
    }
    return _shareChannelArray2;
}

- (NSString *)pwd{
    if (!_pwd) {
        _pwd = @"";
    }
    return _pwd;
}

- (void)removeTimer{
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    if (_pusher) {
        [_pusher stopPreview];
        [_pusher stopPush];
    }
    if (_superPlayer) {
        [_superPlayer resetPlayer];
        [_superPlayer removeFromSuperview];
        _superPlayer = nil;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
