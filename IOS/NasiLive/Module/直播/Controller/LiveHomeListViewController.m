//
//  LiveHomeListViewController.m
//  Nasi
//
//  Created by yun on 2019/12/24.
//  Copyright © 2019 yun7. All rights reserved.
//

#import "LiveHomeListViewController.h"
#import "PortraitLiveViewController.h"
#import "LandscapeLiveViewController.h"
#import "RechargeViewController.h"
#import "H5ViewController.h"

#import <TYCyclePagerView.h>
#import "TYPageControl.h"
#import "TYCyclePagerViewCell.h"

#import "LiveCollectionViewCell.h"

#import "AdModel.h"
#import "LiveModel.h"

#define pagesize 20

@interface LiveHomeListViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,TYCyclePagerViewDataSource,TYCyclePagerViewDelegate>{
    TYCyclePagerView            *tyCyclePagerView;
    TYPageControl               *pageControl;
    UIView                      *headerView;
    
    NSMutableArray              *topDatasource;
    NSMutableArray              *datasource;
    
    BOOL                        isfirstLoad;
}

@end

@implementation LiveHomeListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isfirstLoad = YES;
    topDatasource = [NSMutableArray array];
    datasource = [NSMutableArray array];
    
    [self createHeaderView];
    
    CGFloat itemW = (KScreenWidth - 5*3)/2;
    CGFloat itemH = itemW;
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc]init];
    flow.scrollDirection = UICollectionViewScrollDirectionVertical;
    flow.itemSize = CGSizeMake(itemW, itemH);
    flow.minimumLineSpacing = 5;
    flow.minimumInteritemSpacing = 5;
    flow.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5);
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0,0, KScreenWidth, KScreenHeight - kTopHeight - kTabBarHeight) collectionViewLayout:flow];
    self.collectionView.delegate   = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:@"LiveCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"LiveCollectionViewCell"];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"RecCollectionViewHeader"];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CollectionViewSectionHeader"];
    [self removeCollectionViewMJFooter];
    [self setupCollectionViewMJHeader];
    [self reqDataAtPage:1];
}

- (void)createHeaderView{
    if (headerView) {
        [headerView removeFromSuperview];
        headerView = nil;
    }
    headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, kScreenWidth / 18 * 5 + 5)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    tyCyclePagerView = [[TYCyclePagerView alloc]init];
    tyCyclePagerView.isInfiniteLoop = YES;
    tyCyclePagerView.autoScrollInterval = 3.0;
    tyCyclePagerView.dataSource = self;
    tyCyclePagerView.delegate = self;
    // registerClass or registerNib
    [tyCyclePagerView registerClass:[TYCyclePagerViewCell class] forCellWithReuseIdentifier:@"TYCyclePagerViewCellID"];
    
    tyCyclePagerView.frame = CGRectMake(0, 0, kScreenWidth, kScreenWidth / 18 * 5);
    [headerView addSubview:tyCyclePagerView];
    
}

- (void)headerRereshing{
    [self reqDataAtPage:1];
}

- (void)footerRereshing{
    int page = ceil((datasource.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    [self reqDataAtPage:page];
}

- (void)reqDataAtPage:(int)page{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    params[@"size"] = @(pagesize);
    params[@"page"] = @(page);
    
    if (page == 1 && isfirstLoad) {
        [commonManager showLoadingAnimateInView:self.collectionView];
    }
    [CommonManager POST:@"live/getHotLives" parameters:params success:^(id responseObject) {
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
        if (page == 1 && self->isfirstLoad) {
            self->isfirstLoad = NO;
            [commonManager hideAnimateHud];
        }
        if (RESP_SUCCESS(responseObject)) {
            if (page == 1) {
                [self->datasource removeAllObjects];
            }
            NSArray *models = [NSArray yy_modelArrayWithClass:[LiveModel class] json:responseObject[@"data"]];
            [self->datasource addObjectsFromArray:models];
            
            [self.collectionView reloadData];
            if (models.count < pagesize) {
                [self removeCollectionViewMJFooter];
            }else{
                [self setupCollectionViewMJFooter];
            }
            if (self->datasource.count == 0) {
                [self showNoDataImage];
            }else{
                [self removeNoDataImage];
            }
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        if (page == 1 && self->isfirstLoad) {
            self->isfirstLoad = NO;
            [commonManager hideAnimateHud];
        }
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
        RESP_FAILURE;
    }];
    
    if (page == 1) {
        [CommonManager POST:@"Ads/getHomeScrollAd" parameters:nil success:^(id responseObject) {
            if (RESP_SUCCESS(responseObject)) {
                if (page == 1) {
                    [self->topDatasource removeAllObjects];
                }
                NSArray *models = [NSArray yy_modelArrayWithClass:[AdModel class] json:responseObject[@"data"]];
                [self->topDatasource addObjectsFromArray:models];
                [self->tyCyclePagerView reloadData];
            }
        } failure:^(NSError *error) {
        }];
    }
}

#pragma mark ----------------------  delegate + datasource ----------------------------
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return datasource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    LiveCollectionViewCell *cell = (LiveCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"LiveCollectionViewCell" forIndexPath:indexPath];
    cell.model = datasource[indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    LiveModel *model = datasource[indexPath.row];
    if (model.room_type == LiveRoomTypeFee) {
        if (![CommonManager checkAndLogin]) {
            return;
        }
        if (userManager.curUserInfo.gold < model.price) {
            [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"付费房间" contentText:@"您的金币余额不足" leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"立即充值" rightClick:^{
                RechargeViewController *vc = [[RechargeViewController alloc]init];
                [self.navigationController pushViewController:vc animated:YES];
            }];
        }else{
            [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"付费房间" contentText:[NSString stringWithFormat:@"该房间价格：%d金币/分钟",model.price] leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"进入" rightClick:^{
                [self jumpIntoRoomWithModel:model];
            }];
        }
    }else if(model.room_type == LiveRoomTypePwd){
        [QNAlertView showQNTextFieldWithTitle:@"私密房间" placeHolder:@"请输入房间密码" textFieldType:QNTextFieldTypeNormal leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"确定" rightClick:^(NSString *value) {
            if ([[[value md5String] uppercaseString] isEqualToString:[model.password uppercaseString]]) {
                [self jumpIntoRoomWithModel:model];
            }else{
                [MBProgressHUD showTipMessageInWindow:@"密码错误"];
            }
        }];
    }else{
        [self jumpIntoRoomWithModel:model];
    }
}

- (void)jumpIntoRoomWithModel:(LiveModel *)model{
    if (model.orientation == 1) {
        LandscapeLiveViewController *vc = [[LandscapeLiveViewController alloc]init];
        vc.liveModel = model;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        PortraitLiveViewController *vc = [[PortraitLiveViewController alloc]init];
        vc.anchorModel = model.anchor;

        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"RecCollectionViewHeader" forIndexPath:indexPath];
        header.backgroundColor = [UIColor whiteColor];
        [header addSubview:headerView];
        return header;
    }else{
        return nil;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(KScreenWidth, headerView.height);
}

#pragma mark -------------------- TYCyclePagerViewDataSource  --------------------------------

- (NSInteger)numberOfItemsInPagerView:(TYCyclePagerView *)pageView {
    return topDatasource.count;
}

- (UICollectionViewCell *)pagerView:(TYCyclePagerView *)pagerView cellForItemAtIndex:(NSInteger)index {
    AdModel *model = topDatasource[index];
    TYCyclePagerViewCell *cell = [pagerView dequeueReusableCellWithReuseIdentifier:@"TYCyclePagerViewCellID" forIndex:index];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:model.image_url] placeholderImage:IMAGE_NAMED(@"cover_loading")];
    return cell;
}

- (TYCyclePagerViewLayout *)layoutForPagerView:(TYCyclePagerView *)pageView {
    TYCyclePagerViewLayout *layout = [[TYCyclePagerViewLayout alloc]init];
    layout.itemSize = CGSizeMake(pageView.width * 0.9, pageView.height * 0.9);
    layout.layoutType = TYCyclePagerTransformLayoutLinear;
    layout.itemSpacing = 5;
    return layout;
}

- (void)pagerView:(TYCyclePagerView *)pageView didScrollFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
//    pageControl.currentPage = toIndex;
    //[_pageControl setCurrentPage:newIndex animate:YES];
}

- (void)pagerView:(TYCyclePagerView *)pageView didSelectedItemCell:(__kindof UICollectionViewCell *)cell atIndex:(NSInteger)index{
    AdModel *model = topDatasource[index];
    if (model.jump_type == AdJumpTypeInApp) {
        H5ViewController *vc = [[H5ViewController alloc]init];
        vc.href = model.jump_url;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:model.jump_url] options:@{} completionHandler:nil];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
