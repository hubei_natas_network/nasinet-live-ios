//
//  LiveHomeViewController.m
//  Nasi
//
//  Created by yun on 2019/12/24.
//  Copyright © 2019 yun7. All rights reserved.
//

#import "LiveHomeViewController.h"
#import "LiveHomeListViewController.h"
#import "LiveListViewController.h"
#import "LiveBroadcastViewController.h"
#import "SearchViewController.h"
#import "MlvbLiveBroadcastViewController.h"

#import "H5ViewController.h"

#import "XLPageViewController.h"

@interface LiveHomeViewController ()<XLPageViewControllerDelegate,XLPageViewControllerDataSrouce>{
    NSMutableArray          *pageTitleArr;
    
    UIButton                *startLiveBtn;
}

@end

@implementation LiveHomeViewController

- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    if (userManager.curUserInfo.is_anchor) {
        startLiveBtn.hidden = NO;
    }else{
        startLiveBtn.hidden = YES;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadData];
    self.view.backgroundColor = MAIN_COLOR ;
    XLPageViewControllerConfig *config = [XLPageViewControllerConfig defaultConfig];
    config.separatorLineHidden = YES;
    config.titleNormalColor   = [UIColor whiteColor];
    config.titleSelectedColor = [UIColor whiteColor];
    config.titleNormalFont = FFont15;
    config.titleSelectedFont = [UIFont boldSystemFontOfSize:22];
    config.titleSpace = 5.f;
    config.titleViewBackgroundColor = MAIN_COLOR ;
    config.titleViewHeight = 44;
    config.titleViewInset = UIEdgeInsetsMake(0, 15, 0, 44);

    config.shadowLineColor = MAIN_COLOR;
    config.shadowLineWidth = 20;

    XLPageViewController *pageViewController = [[XLPageViewController alloc] initWithConfig:config];
    pageViewController.view.frame = CGRectMake(0, kStatusBarHeight, KScreenWidth, KScreenHeight - kStatusBarHeight - kTabBarHeight);
    pageViewController.delegate = self;
    pageViewController.dataSource = self;
    [self.view addSubview:pageViewController.view];
    [self addChildViewController:pageViewController];
    
   
    
    startLiveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    startLiveBtn.frame = CGRectMake(self.view.width - 60 - 15, self.view.height - kTabBarHeight - 20 - 60, 60, 60);
    [startLiveBtn setImage:IMAGE_NAMED(@"button_zhibo") forState:UIControlStateNormal];
    [startLiveBtn addTarget:self action:@selector(startLiveBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:startLiveBtn];
}

- (void)loadData {
    pageTitleArr = [NSMutableArray array];
    [pageTitleArr addObject:@"热门"];
    for (NSDictionary *dict in configManager.liveCategorys) {
        [pageTitleArr addObject:dict[@"title"]];
    }
}

- (void)startLiveBtnClick{
    if (!userManager.curUserInfo.is_anchor) {
        [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"" contentText:@"完成认证，开始直播吧！" leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"前往认证" rightClick:^{
            
        }];
        return;
    }
    MlvbLiveBroadcastViewController *vc = [[MlvbLiveBroadcastViewController alloc]init];
//    LiveBroadcastViewController *vc =  [[LiveBroadcastViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)searchBtnClick{
    SearchViewController *vc = [[SearchViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

//根据index创建对应的视图控制器，每个试图控制器只会被创建一次。
- (UIViewController *)pageViewController:(XLPageViewController *)pageViewController viewControllerForIndex:(NSInteger)index{
    if (index == 0) {
        LiveHomeListViewController *vc = [[LiveHomeListViewController alloc]init];
        return vc;
    }else{
        LiveListViewController *vc = [[LiveListViewController alloc]init];
        vc.categoryid = [configManager.liveCategorys[index-1][@"id"] intValue];
        return vc;
    }
}

//根据index返回对应的标题
- (NSString *)pageViewController:(XLPageViewController *)pageViewController titleForIndex:(NSInteger)index{
    return pageTitleArr[index];
}

//返回分页数
- (NSInteger)pageViewControllerNumberOfPage{
    return pageTitleArr.count;
}

- (void)pageViewController:(XLPageViewController *)pageViewController didSelectedAtIndex:(NSInteger)index{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
