//
//  LiveListViewController.m
//  NasiLive
//
//  Created by yun on 2020/2/27.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "LiveListViewController.h"
#import "PortraitLiveViewController.h"
#import "LandscapeLiveViewController.h"
#import "RechargeViewController.h"

#import "LiveCollectionViewCell.h"

#import "AdModel.h"
#import "LiveModel.h"

#define pagesize 20

@interface LiveListViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>{
    
    NSMutableArray *datasource;
    
    BOOL isfirstLoad;
}

@property (copy, nonatomic) NSString                *api;
@property (strong, nonatomic) NSMutableDictionary   *param;
@property (copy, nonatomic) NSString                *keyword;

@end

@implementation LiveListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isfirstLoad = YES;
    datasource = [NSMutableArray array];
    
    CGFloat itemW = (KScreenWidth - 5*3)/2;
    CGFloat itemH = itemW;
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc]init];
    flow.scrollDirection = UICollectionViewScrollDirectionVertical;
    flow.itemSize = CGSizeMake(itemW, itemH);
    flow.minimumLineSpacing = 5;
    flow.minimumInteritemSpacing = 5;
    flow.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5);
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0,0, KScreenWidth, KScreenHeight - kTopHeight - kTabBarHeight) collectionViewLayout:flow];
    self.collectionView.delegate   = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:@"LiveCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"LiveCollectionViewCell"];
    
    [self removeCollectionViewMJFooter];
    [self setupCollectionViewMJHeader];
    
    if (!(self.listType == LiveListTypeSearch && self.keyword.length == 0)) {
        [self reqDataAtPage:1];
    }
}

- (void)headerRereshing{
    [self reqDataAtPage:1];
}

- (void)footerRereshing{
    int page = ceil((datasource.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    [self reqDataAtPage:page];
}

- (void)reqDataAtPage:(int)page{
    self.param[@"page"] = @(page);
    
    if (page == 1 && isfirstLoad) {
        [commonManager showLoadingAnimateInView:self.collectionView];
    }
    [CommonManager POST:self.api parameters:self.param success:^(id responseObject) {
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
        if (page == 1 && self->isfirstLoad) {
            self->isfirstLoad = NO;
            [commonManager hideAnimateHud];
        }
        if (RESP_SUCCESS(responseObject)) {
            if (page == 1) {
                [self->datasource removeAllObjects];
            }
            NSArray *models = [NSArray yy_modelArrayWithClass:[LiveModel class] json:responseObject[@"data"]];
            [self->datasource addObjectsFromArray:models];
            
            [self.collectionView reloadData];
            if (models.count < pagesize) {
                [self removeCollectionViewMJFooter];
            }else{
                [self setupCollectionViewMJFooter];
            }
            if (self->datasource.count == 0) {
                [self showNoDataImage];
            }else{
                [self removeNoDataImage];
            }
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        if (page == 1 && self->isfirstLoad) {
            self->isfirstLoad = NO;
            [commonManager hideAnimateHud];
        }
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
        RESP_FAILURE;
    }];
}

#pragma mark ————————————————————————  delegate + datasource ————————————————————————
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return datasource.count;

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    LiveCollectionViewCell *cell = (LiveCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"LiveCollectionViewCell" forIndexPath:indexPath];
    cell.model = datasource[indexPath.row];
    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    LiveModel *model = datasource[indexPath.row];
    if (model.room_type == LiveRoomTypeFee) {
        if (![CommonManager checkAndLogin]) {
            return;
        }
        if (userManager.curUserInfo.gold < model.price) {
            [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"付费房间" contentText:@"您的金币余额不足" leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"立即充值" rightClick:^{
                RechargeViewController *vc = [[RechargeViewController alloc]init];
                [self.navigationController pushViewController:vc animated:YES];
            }];
        }else{
            [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"付费房间" contentText:[NSString stringWithFormat:@"该房间价格：%d金币/分钟",model.price] leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"进入" rightClick:^{
                [self jumpIntoRoomWithModel:model];
            }];
        }
    }else if(model.room_type == LiveRoomTypePwd){
        [QNAlertView showQNTextFieldWithTitle:@"私密房间" placeHolder:@"请输入房间密码" textFieldType:QNTextFieldTypeNormal leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"确定" rightClick:^(NSString *value) {
            if ([[[value md5String] uppercaseString] isEqualToString:[model.password uppercaseString]]) {
                [self jumpIntoRoomWithModel:model];
            }else{
                [MBProgressHUD showTipMessageInWindow:@"密码错误"];
            }
        }];
    }else{
        [self jumpIntoRoomWithModel:model];
    }
}

- (void)jumpIntoRoomWithModel:(LiveModel *)model{
    if (model.orientation == 1) {
        LandscapeLiveViewController *vc = [[LandscapeLiveViewController alloc]init];
        vc.liveModel = model;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        PortraitLiveViewController *vc = [[PortraitLiveViewController alloc]init];
        vc.anchorModel = model.anchor;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


- (NSString *)api{
    if (!_api) {
        switch (self.listType) {
            case LiveListTypeCategory:{
                _api = @"live/getLivesByCategory";
            }
                break;
            case LiveListTypeSearch:{
                _api = @"live/search";
            }
                break;
            default:
                _api = @"live/getLivesByCategory";
                break;
        }
    }
    return _api;
}

- (NSMutableDictionary *)param{
    if (!_param) {
        _param = [NSMutableDictionary dictionary];
        _param[@"size"] = @(pagesize);
        switch (self.listType) {
            case LiveListTypeCategory:{
                _param[@"categoryid"] = @(self.categoryid);
            }
                break;
            case LiveListTypeSearch:{
                _param[@"keyword"] = self.keyword;
            }
                break;
            default:{
                _param[@"categoryid"] = @(self.categoryid);
            }
                break;
        }
    }
    return _param;
}

- (void)setKeyword:(NSString *)keyword{
    if (keyword.length == 0 || [keyword isEqualToString:_keyword]) {
        return;
    }
    _keyword = keyword;
    self.param[@"keyword"] = keyword;
    isfirstLoad = YES;
    [self reqDataAtPage:1];
}


#pragma mark - JXCategoryListContentViewDelegate

- (UIView *)listView {
    return self.view;
}

#pragma mark - SearchVcProtocal
- (void)searchKeyword:(NSString *)keyword{
    self.keyword = keyword;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
