//
//  LiveHomeListViewController.h
//  Nasi
//
//  Created by yun on 2019/12/24.
//  Copyright © 2019 yun7. All rights reserved.
//

#import "RootViewController.h"
#import "JXCategoryListContainerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveHomeListViewController : RootViewController<JXCategoryListContentViewDelegate>

@end

NS_ASSUME_NONNULL_END
