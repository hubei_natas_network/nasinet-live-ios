//
//  LiveListViewController.h
//  NasiLive
//
//  Created by yun on 2020/2/27.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"
#import "SearchVcProtocal.h"
#import "JXCategoryListContainerView.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, LiveListType) {
    LiveListTypeCategory = 0,       //分类列表
    LiveListTypeSearch,
};

@interface LiveListViewController : RootViewController<SearchVcProtocal,JXCategoryListContentViewDelegate>

@property (assign, nonatomic) LiveListType  listType;
@property (nonatomic,assign) int            categoryid;

@end

NS_ASSUME_NONNULL_END
