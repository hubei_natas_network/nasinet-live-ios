//
//  MLVBLiveManager.m
//  NasiLive
//
//  Created by yun11 on 2020/11/7.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "MLVBLiveManager.h"

#define ServerAddr                      @"https://liveroom.qcloud.com/weapp/live_room"

static NSString * const RespCodeKey = @"code";

@interface MLVBLiveManager (){
    NSString            *_token;
}

@end

@implementation MLVBLiveManager

- (instancetype)init{
    if (self = [super init]) {
    }
    return self;
}

- (void)loginWithUserID:(NSString *)userID userSign:(NSString *)userSign completion:(nonnull MLVBLoginCompletionCallback)completion failure:(nonnull QNHttpRequestFailed)failure{
    // Room登录
    NSDictionary *params = @{@"sdkAppID": [configManager appConfig].im_sdkappid,
                             @"userID": userID,
                             @"userSig": userSign,
                             @"platform": @"iOS"
    };
    NSMutableString *query = [[NSMutableString alloc] init];
    [params enumerateKeysAndObjectsUsingBlock:^(NSString *_Nonnull key, NSString *_Nonnull obj, BOOL * _Nonnull stop) {
        [query appendFormat:@"%@=%@&", key, [obj stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"`#%^{}\"[]|\\<> "].invertedSet]];
    }];
    [query deleteCharactersInRange:NSMakeRange(query.length-1, 1)];
    NSString *cgiUrl = [NSString stringWithFormat:@"%@/login?%@", ServerAddr, query];
    
    [QNNetworkHelper POST:cgiUrl parameters:nil success:^(id responseObject) {
        int errCode = [responseObject[RespCodeKey] intValue];
        NSString *errMsg = responseObject[@"message"];
        NSString *token  = responseObject[@"token"];
        
        self->_token = token;
        
        completion(errCode,errMsg);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)createMlvbLiveParameters:(NSDictionary *)param success:(QNHttpRequestSuccess)success failure:(QNHttpRequestFailed)failure{
    NSMutableDictionary *m_param = [NSMutableDictionary dictionaryWithDictionary:param];
    m_param[@"mlvb_token"] = _token;
    [CommonManager POST:@"Live/mlvbStartLive" parameters:m_param success:^(id responseObject) {
        success(responseObject);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)turnLinkOnOff:(int)onoff success:(QNHttpRequestSuccess)success failure:(QNHttpRequestFailed)failure{
    [CommonManager POST:@"live/setLinkOnOff" parameters:@{@"type":@(onoff)} success:^(id responseObject) {
        success(responseObject);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)requestLinkWithAnchor:(NSInteger)anchorid success:(nonnull QNHttpRequestSuccess)success failure:(nonnull QNHttpRequestFailed)failure{
    [CommonManager POST:@"live/requestMlvbLink" parameters:@{@"anchorid":@(anchorid),@"mlvb_token":_token} success:^(id responseObject) {
        success(responseObject);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)acceptLinkWithUser:(NSInteger)userid success:(QNHttpRequestSuccess)success failure:(QNHttpRequestFailed)failure{
    [CommonManager POST:@"live/acceptMlvbLink" parameters:@{@"userid":@(userid)} success:^(id responseObject) {
        success(responseObject);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)refuseLinkWithUser:(NSInteger)userid success:(QNHttpRequestSuccess)success failure:(QNHttpRequestFailed)failure{
    [CommonManager POST:@"live/refuseMlvbLink" parameters:@{@"userid":@(userid)} success:^(id responseObject) {
        success(responseObject);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)stopLinkWithUser:(NSInteger)userid anchor:(NSInteger)anchorid success:(QNHttpRequestSuccess)success failure:(QNHttpRequestFailed)failure{
    [CommonManager POST:@"live/stopMlvbLink" parameters:@{@"linkerid":@(userid),@"anchorid":@(anchorid)} success:^(id responseObject) {
        success(responseObject);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)mergeStreamWithUser:(NSInteger)userid success:(QNHttpRequestSuccess)success failure:(QNHttpRequestFailed)failure{
    [CommonManager POST:@"live/mergeStream" parameters:@{@"linkerid":@(userid)} success:^(id responseObject) {
        success(responseObject);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)requestEnterPkModeSuccess:(QNHttpRequestSuccess)success failure:(QNHttpRequestFailed)failure{
    [CommonManager POST:@"live/enterPkMode" parameters:@{} success:^(id responseObject) {
        success(responseObject);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

- (void)requestEndPkModeSuccess:(QNHttpRequestSuccess)success failure:(QNHttpRequestFailed)failure{
    [CommonManager POST:@"live/endPk" parameters:@{} success:^(id responseObject) {
        success(responseObject);
    } failure:^(NSError *error) {
        failure(error);
    }];
}


@end
