//
//  LiveBroadcastViewController.m
//  NasiLive
//
//  Created by yun on 2020/3/4.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "LiveBroadcastViewController.h"
#import "ContributeRankViewController.h"
#import "MessagePopListViewController.h"
#import "LiveGoodsViewController.h"

#import "ShopGoodsModel.h"

#import "ImSDK.h"
#import <QCloudCOSXML.h>
#import <TZImagePickerController.h>
#import "BRPickerView.h"
#import "SharePanelView.h"

#import "TCAVFilter.h"

#import "RoomChatTableViewCell.h"
#import "GiftShowView.h"

#import <TiSDK/TiSDKInterface.h> // TiSDK接口头文件
#import "TiUIManager.h"

@import TXLiteAVSDK_Professional;

@interface LiveBroadcastViewController ()<TXLivePushListener,TZImagePickerControllerDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,GiftShowViewDelegate,TXVideoCustomProcessDelegate,TiUIManagerDelegate,SharePanelViewDelegate>{
    TXLivePush          *_pusher;
    BOOL                _inLive;
    
    UIView              *_shadowView;
    
    GiftShowView        *_giftShowView;
    
    UIView              *_beautyPanel;
    UIButton            *_beautyBtn;
    UIButton            *_filterBtn;
    UIView              *_underline;
    UIView              *_beautyView;
    UIView              *_filterView;
    UIButton            *_selectedFilterBtn;
    
    BRStringPickerView  *_categoryPickerView;
    NSInteger           _selectedCategoryId;
    
    NSArray<TCAVFilter *> *_filters;
    
    CGFloat             _filterValue;
    CGFloat             _beautyLevel;
    CGFloat             _whiteLevel;
    CGFloat             _ruddyLevel;
    NSUInteger          _selectedFilterIndex;
    
    NSString            *_thumbUrl;
    UIImage             *_thumbImage;
    
    NSMutableArray      *_chatArray;
    
    ContributeRankViewController                *_contributeRankVc;
    MessagePopListViewController                *_messageVc;
    LiveGoodsViewController                     *_goodsVc;
}

@property (weak, nonatomic) IBOutlet UIView *playerContainerView;
@property (weak, nonatomic) IBOutlet UIView *prepareView;
@property (weak, nonatomic) IBOutlet UIView *controlView;

@property (weak, nonatomic) IBOutlet UIButton *chooseCategoryBtn;
@property (weak, nonatomic) IBOutlet UIImageView *selectThumbImgView;
@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UITableView *chatTableView;
@property (weak, nonatomic) IBOutlet UILabel *liveTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *audienceCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *profitLabel;

@property (weak, nonatomic) IBOutlet UIButton *showGoodsBtn;

@property (weak, nonatomic) IBOutlet UIButton *roomTypeBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *typeViewBottomLC;
@property (weak, nonatomic) IBOutlet UIButton *normalRoomTypeBtn;
@property (weak, nonatomic) IBOutlet UIButton *feeRoomTypeBtn;
@property (weak, nonatomic) IBOutlet UIButton *pwdRoomTypeBtn;

@property (strong, nonatomic) NSTimer *timer;
@property (nonatomic,assign) NSInteger liveDuration;
@property (nonatomic,assign) NSInteger audienceCount;

@property (strong, nonatomic) LiveModel *liveModel;

@property (strong, nonatomic) NSMutableArray *shareChannelArray1;
@property (strong, nonatomic) NSMutableArray *shareChannelArray2;

@property (assign, nonatomic) int selectedRoomTypeValue;
@property (assign, nonatomic) int price;
@property (copy, nonatomic) NSString *pwd;

@property (strong, nonatomic) NSArray *goodsArray;
@property (assign, nonatomic) int explainingGoodsid;

@end

@implementation LiveBroadcastViewController

- (void)viewDidAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
            self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        }
    });
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.controlView.hidden = YES;
    self.prepareView.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *chooseThumbTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(chooseThumbTap:)];
    [self.selectThumbImgView addGestureRecognizer:chooseThumbTap];
    
    self.titleTextField.delegate = self;
    self.chatTableView.delegate = self;
    self.chatTableView.dataSource = self;
    
    [RoomChatTableViewCell registerWithTableView:self.chatTableView];
    
    [self initPusher];
    
    RoomChatModel *sysChatModel = [[RoomChatModel alloc]init];
    sysChatModel.chatType = UIButtonTypeSystem;
    sysChatModel.message = configManager.appConfig.room_notice;
    _chatArray = [NSMutableArray arrayWithObject:sysChatModel];
    
    _shadowView = [[UIView alloc]initWithFrame:self.view.bounds];
    _shadowView.backgroundColor = [UIColor clearColor];
    _shadowView.hidden = YES;
    [self.view addSubview:_shadowView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shadowViewTap)];
    [_shadowView addGestureRecognizer:tap];
    
    switch (configManager.appConfig.beauty_channel) {
        case 0:{
            //腾讯云自带美颜 --------- start ------------
            [self createBeautyPanel];
            //腾讯云自带美颜 --------- end ------------
        }
            break;
        case 1:{
            // TiSDK
            [TiSDK init:configManager.appConfig.tisdk_key CallBack:^(InitStatus initStatus) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"TiSDKInitStatusNotification" object:nil];
            }];
            
            [[TiUIManager shareManager] loadToView:self.view forDelegate:self];
        }
            break;
        default:
            break;
    }
    
    [self getGoodsList];
    
}

- (void)getGoodsList{
    [CommonManager POST:@"live/getGoodsList" parameters:@{@"anchorid":@([userManager curUserInfo].userid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            NSDictionary *dataDict = responseObject[@"data"];
            self.goodsArray = [NSArray yy_modelArrayWithClass:[ShopGoodsModel class] json:dataDict];
            if (self.goodsArray.count > 0) {
                self.showGoodsBtn.hidden = NO;
                ShopGoodsModel *model = self.goodsArray[0];
                if (model.live_explaining) {
                    self.explainingGoodsid = model.goodsid;
                }
            }
        }
    } failure:^(NSError *error) {
    }];
}

- (void)initPusher{
    TXLivePushConfig *_config = [[TXLivePushConfig alloc] init];
    _pusher = [[TXLivePush alloc] initWithConfig: _config];
    _pusher.delegate = self;
    _pusher.videoProcessDelegate = self;
    [_pusher setVideoQuality:VIDEO_QUALITY_HIGH_DEFINITION adjustBitrate:NO adjustResolution:NO];
    
    if (configManager.appConfig.beauty_channel == 0) {
        //腾讯云自带美颜 --------- start ------------
        
        //默认美颜设置
        _beautyLevel = 8.f;
        _whiteLevel = 5.f;
        _ruddyLevel = 3.f;
        [[_pusher getBeautyManager] setBeautyStyle:TXBeautyStyleSmooth];
        [[_pusher getBeautyManager] setBeautyLevel:_beautyLevel];
        [[_pusher getBeautyManager] setWhitenessLevel:_whiteLevel];
        [[_pusher getBeautyManager] setRuddyLevel:_ruddyLevel];
    
        _filters = [TCAVFilterManager defaultManager].allFilters;
        // 初始化滤镜选项
        _filterValue = 5.f;
        const TCAVFilterIdentifier defaultFilterIdentifier = TCAVFilterIdentifierNormal;
        // index = 0 为关闭
        _selectedFilterIndex = [_filters indexOfObjectPassingTest:
                                         ^BOOL(TCAVFilter * _Nonnull obj,
                                               NSUInteger idx,
                                               BOOL * _Nonnull stop) {
            return [obj.identifier isEqualToString:defaultFilterIdentifier];
        }];
        UIImage *lutImage = [self filterImageByMenuOptionIndex:_selectedFilterIndex+1];
        [[_pusher getBeautyManager] setFilter:lutImage];
        [[_pusher getBeautyManager] setFilterStrength:_filterValue/10.0];
        
        //腾讯云自带美颜 --------- end ------------
    }
    
    //启动本地摄像头预览
    [_pusher startPreview:self.playerContainerView];
    
    //IM通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(imNotification:) name:KNotificationInRoomIM object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(imGroupEventNotification:) name:KNotificationGroupEvent object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(giftAnimationNotification:) name:KNotificationGiftAnimation object:nil];
}

- (void)createBeautyPanel{
    
    _beautyPanel = [[UIView alloc]initWithFrame:CGRectMake(0, KScreenHeight, KScreenWidth, 240 + kBottomSafeHeight)];
    _beautyPanel.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
    _beautyPanel.hidden = YES;
    [self.view addSubview:_beautyPanel];
    
    UIView *switchView = [[UIView alloc]initWithFrame:CGRectMake(0, _beautyPanel.height - 50 - kBottomSafeHeight, _beautyPanel.width, 40)];
    [_beautyPanel addSubview:switchView];
    
    _beautyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _beautyBtn.frame = CGRectMake(_beautyPanel.width / 2 - 30 - 40, 0, 40, 36);
    _beautyBtn.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
    [_beautyBtn setTitle:@"美颜" forState:UIControlStateNormal];
    [_beautyBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [_beautyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _beautyBtn.selected = YES;
    [_beautyBtn addTarget:self action:@selector(switchBeautyBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [switchView addSubview:_beautyBtn];
    
    _filterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _filterBtn.frame = CGRectMake(_beautyPanel.width / 2 + 30, 0, 40, 36);
    _filterBtn.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
    [_filterBtn setTitle:@"滤镜" forState:UIControlStateNormal];
    [_filterBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [_filterBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_filterBtn addTarget:self action:@selector(switchFilterBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [switchView addSubview:_filterBtn];
    
    _underline = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(_beautyBtn.frame), 12, 4)];
    _underline.centerX = _beautyBtn.centerX;
    _underline.layer.cornerRadius = 2;
    [_underline setBackgroundColor:MAIN_COLOR];
    [switchView addSubview:_underline];
    
    
    // 美颜  ————————————————————————————————————————————————————————————————————————————
    _beautyView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _beautyPanel.width, switchView.mj_y)];
    [_beautyPanel addSubview:_beautyView];
    
    UILabel *beautyTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 40, 30, 15)];
    beautyTitleLabel.text = @"磨皮";
    beautyTitleLabel.font = [UIFont systemFontOfSize:14];
    beautyTitleLabel.textColor = [UIColor whiteColor];
    [_beautyView addSubview:beautyTitleLabel];
    
    UISlider *beautySlider = [[UISlider alloc]initWithFrame:CGRectMake(CGRectGetMaxX(beautyTitleLabel.frame) + 8, 0, _beautyView.width - 15 - (CGRectGetMaxX(beautyTitleLabel.frame) + 8), 30)];
    beautySlider.centerY = beautyTitleLabel.centerY;
    beautySlider.maximumValue = 9.f;
    beautySlider.value = _beautyLevel;
    beautySlider.thumbTintColor = MAIN_COLOR;
    beautySlider.minimumTrackTintColor = MAIN_COLOR;
    beautySlider.maximumTrackTintColor = [UIColor colorWithHexString:@"CCCCCC"];
    [beautySlider addTarget:self action:@selector(beautySliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_beautyView addSubview:beautySlider];
    
    UILabel *whiteTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(beautyTitleLabel.frame) + 40, 30, 15)];
    whiteTitleLabel.text = @"美白";
    whiteTitleLabel.font = [UIFont systemFontOfSize:14];
    whiteTitleLabel.textColor = [UIColor whiteColor];
    [_beautyView addSubview:whiteTitleLabel];
    
    UISlider *whiteySlider = [[UISlider alloc]initWithFrame:CGRectMake(CGRectGetMaxX(whiteTitleLabel.frame) + 8, 0, _beautyView.width - 15 - (CGRectGetMaxX(beautyTitleLabel.frame) + 8), 30)];
    whiteySlider.centerY = whiteTitleLabel.centerY;
    whiteySlider.maximumValue = 9.f;
    whiteySlider.value = _whiteLevel;
    whiteySlider.thumbTintColor = MAIN_COLOR;
    whiteySlider.minimumTrackTintColor = MAIN_COLOR;
    whiteySlider.maximumTrackTintColor = [UIColor colorWithHexString:@"CCCCCC"];
    [whiteySlider addTarget:self action:@selector(whiteySliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_beautyView addSubview:whiteySlider];
    
    UILabel *ruddyTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(whiteTitleLabel.frame) + 40, 30, 15)];
    ruddyTitleLabel.text = @"红润";
    ruddyTitleLabel.font = [UIFont systemFontOfSize:14];
    ruddyTitleLabel.textColor = [UIColor whiteColor];
    [_beautyView addSubview:ruddyTitleLabel];
    
    UISlider *ruddySlider = [[UISlider alloc]initWithFrame:CGRectMake(CGRectGetMaxX(ruddyTitleLabel.frame) + 8, 0, _beautyView.width - 15 - (CGRectGetMaxX(beautyTitleLabel.frame) + 8), 30)];
    ruddySlider.centerY = ruddyTitleLabel.centerY;
    ruddySlider.maximumValue = 9.f;
    ruddySlider.value = _ruddyLevel;
    ruddySlider.thumbTintColor = MAIN_COLOR;
    ruddySlider.minimumTrackTintColor = MAIN_COLOR;
    ruddySlider.maximumTrackTintColor = [UIColor colorWithHexString:@"CCCCCC"];
    [ruddySlider addTarget:self action:@selector(ruddySliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_beautyView addSubview:ruddySlider];
    
    
    
    //滤镜  ————————————————————————————————————————————————————————————————————————————
    _filterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _beautyPanel.width, switchView.mj_y)];
    _filterView.hidden = YES;
    [_beautyPanel addSubview:_filterView];
    
    UISlider *filterSlider = [[UISlider alloc]initWithFrame:CGRectMake(45, 30, _filterView.width - 45*2, 30)];
    filterSlider.centerY = beautyTitleLabel.centerY;
    filterSlider.maximumValue = 10.f;
    filterSlider.value = _filterValue;
    filterSlider.thumbTintColor = MAIN_COLOR;
    filterSlider.minimumTrackTintColor = MAIN_COLOR;
    filterSlider.maximumTrackTintColor = [UIColor colorWithHexString:@"CCCCCC"];
    [filterSlider addTarget:self action:@selector(filterSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_filterView addSubview:filterSlider];
    
    UIScrollView *filterScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(filterSlider.frame)+30, _filterView.width, _filterView.height - (CGRectGetMaxY(filterSlider.frame)+30) - 15)];
    filterScrollView.showsHorizontalScrollIndicator = NO;
    [_filterView addSubview:filterScrollView];
    CGFloat filterM = 16;
    CGFloat filterH = filterScrollView.height;
    CGFloat filterW = filterH * 121 / 176;
    for (int i = 0; i<_filters.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *image = [self iconForFilter:_filters[i].identifier selected:NO];
        [button setImage:image forState:UIControlStateNormal];
        UIImage *selectImage = [self iconForFilter:_filters[i].identifier selected:YES];
        [button setImage:selectImage forState:UIControlStateSelected];
        button.frame = CGRectMake(filterM + (filterM + filterW)*i, 0, filterW, filterH);
        button.tag = i;
        [button addTarget:self action:@selector(filterBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [filterScrollView addSubview:button];
        if (_selectedFilterIndex == i) {
            button.selected = YES;
            _selectedFilterBtn = button;
        }
    }
    [filterScrollView setContentSize:CGSizeMake((filterM+filterW)*_filters.count + filterM, 0)];
}

#pragma mark ————————————————————— Action ————————————————————————
- (IBAction)switchCamera:(UIButton *)sender {
    [self.view endEditing:YES];
    [_pusher switchCamera];
}

- (IBAction)shareBtnClick:(UIButton *)sender {
    SharePanelView *sharePannelView = [SharePanelView showPanelInView:self.view channel1:self.shareChannelArray1 channel2:self.shareChannelArray2];
    sharePannelView.delegate = self;
}

- (IBAction)switchBeautyPanel:(UIButton *)sender {
    [self.view endEditing:YES];
    self.controlView.hidden = YES;
    self.prepareView.hidden = YES;
    _beautyPanel.hidden = NO;
    _shadowView.hidden = NO;
    switch (configManager.appConfig.beauty_channel) {
        case 0:{
            [UIView animateWithDuration:0.2 animations:^{
                self->_beautyPanel.mj_y = KScreenHeight - self->_beautyPanel.height;
            }];
        }
            break;
        case 1:{
            [[TiUIManager shareManager]showMainMenuView];
        }
            break;
        default:
            break;
    }
    
}

- (IBAction)msgBtnClick:(UIButton *)sender {
    if (!_messageVc) {
        _messageVc = [[MessagePopListViewController alloc]init];
        [self addChildViewController:_messageVc];
        [self.view addSubview:_messageVc.view];
        _messageVc.view.frame = CGRectMake(0, KScreenHeight, KScreenWidth, 550);
    }
    _messageVc.view.hidden = NO;
    _shadowView.hidden = NO;
    [UIView animateWithDuration:0.2 animations:^{
        self->_messageVc.view.mj_y = KScreenHeight - 550;
    }];
}

- (IBAction)closeClick:(UIButton *)sender {
    if (_inLive) {
        [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"提示" contentText:@"确定结束直播?" leftButtonTitle:@"点错了" leftClick:nil rightButtonTitle:@"结束" rightClick:^{
            [commonManager showLoadingAnimateInWindow];
            [CommonManager POST:@"Live/endLive" parameters:@{@"liveid":@(self.liveModel.liveid)} success:^(id responseObject) {
                [commonManager hideAnimateHud];
                if (RESP_SUCCESS(responseObject)) {
                    if (self->_timer) {
                        [self->_timer invalidate];
                        self->_timer = nil;
                    }
                    //结束推流
                    self->_inLive = NO;
                    [self->_pusher stopPreview]; //如果已经启动了摄像头预览，请在结束推流时将其关闭。
                    [self->_pusher stopPush];
                    //发送通知
                    [self sendStopMsg];
                    self.liveModel = [LiveModel yy_modelWithDictionary:responseObject[@"data"]];
//                    QNAlertView *alertView = [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"直播结束" contentText:[NSString stringWithFormat:@"本次直播获得收益：%ld钻石",(long)self.liveModel.gift_profit] singleButton:@"返回" buttonClick:^{
//                        [self.navigationController popViewControllerAnimated:YES];
//                    }];
//                    alertView.cancelBackTap = YES;
                }else{
                    RESP_SHOW_ERROR_MSG(responseObject);
                }
            } failure:^(NSError *error) {
                [commonManager hideAnimateHud];
                RESP_FAILURE;
            }];
        }];
    }else{
        if (_timer) {
            [_timer invalidate];
            _timer = nil;
        }
        [_pusher stopPreview];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)rankBtnClick:(UIButton *)sender {
    if (!_contributeRankVc) {
        _contributeRankVc = [[ContributeRankViewController alloc]init];
        _contributeRankVc.liveid = self.liveModel.liveid;
        [self addChildViewController:_contributeRankVc];
        [self.view addSubview:_contributeRankVc.view];
        _contributeRankVc.view.frame = CGRectMake(0, KScreenHeight, KScreenWidth, 350);
    }
    _contributeRankVc.view.hidden = NO;
    _shadowView.hidden = NO;
    [UIView animateWithDuration:0.2 animations:^{
        self->_contributeRankVc.view.mj_y = KScreenHeight - 350;
    }];
}

- (IBAction)chooseCategoryClick:(UIButton *)sender {
    [self.view endEditing:YES];
    if (!_categoryPickerView) {
        NSMutableArray *modelArray = [[NSMutableArray alloc]init];
        int selectedIndex = 0;
        for (int i = 0; i < configManager.liveCategorys.count; i++) {
            NSDictionary *dic = configManager.liveCategorys[i];
            BRResultModel *model = [[BRResultModel alloc]init];
            model.index = i;
            model.value = dic[@"title"];
            model.key = [NSString stringWithFormat:@"%d",[dic[@"id"] intValue]];
            [modelArray addObject:model];
            if ([dic[@"id"] integerValue] == _selectedCategoryId) {
                selectedIndex = i;
            }
        }
        _categoryPickerView = [[BRStringPickerView alloc]initWithPickerMode:BRStringPickerComponentSingle];
        _categoryPickerView.dataSourceArr = [modelArray copy];
        _categoryPickerView.selectIndex = selectedIndex;
        _categoryPickerView.resultModelBlock = ^(BRResultModel *resultModel) {
            self->_selectedCategoryId = [resultModel.key integerValue];
            [sender setTitle:resultModel.value forState:UIControlStateNormal];
        };
    }
    [_categoryPickerView show];
}

- (IBAction)startLiveClick:(UIButton *)sender {
    NSString *title = self.titleTextField.text;
    if (title.length == 0 || [title isEqualToString:@"给直播写个标题吧"]) {
        [self.titleTextField becomeFirstResponder];
        return;
    }
    if (_selectedCategoryId == 0) {
        [self chooseCategoryClick:self.chooseCategoryBtn];
        return;
    }
    if (_thumbUrl.length == 0) {
        [self uploadImage];
    }else{
        [self startLive];
    }
    
}

- (IBAction)showGoodsBtnClick:(UIButton *)sender {
    if (!_goodsVc) {
        _goodsVc = [[LiveGoodsViewController alloc]init];
        _goodsVc.goodsList = self.goodsArray;
        _goodsVc.isAnchor = YES;
        _goodsVc.chatroomid = self.liveModel.chatroomid;
        _goodsVc.explainingID = self.explainingGoodsid;
        [self addChildViewController:_goodsVc];
        [self.view addSubview:_goodsVc.view];
        _goodsVc.view.frame = CGRectMake(0, KScreenHeight, KScreenWidth, 450);
    }
    [UIView animateWithDuration:0.2 animations:^{
        self->_shadowView.hidden = NO;
        self->_shadowView.alpha = 1;
        self->_goodsVc.view.mj_y = KScreenHeight - 450;
    }];
}

- (void)shadowViewTap{
    self.controlView.hidden = !_inLive;
    self.prepareView.hidden = _inLive;
    
    _shadowView.hidden = YES;
    if (!_beautyPanel.hidden) {
        [UIView animateWithDuration:0.2 animations:^{
            self->_beautyPanel.mj_y = KScreenHeight;
        } completion:^(BOOL finished) {
            self->_beautyPanel.hidden = YES;
        }];
    }
    if (!_contributeRankVc.view.hidden) {
        [UIView animateWithDuration:0.2 animations:^{
            self->_contributeRankVc.view.mj_y = KScreenHeight;
        } completion:^(BOOL finished) {
            self->_contributeRankVc.view.hidden = YES;
        }];
    }
    if (!_messageVc.view.hidden) {
        [UIView animateWithDuration:0.2 animations:^{
            self->_messageVc.view.mj_y = KScreenHeight;
        } completion:^(BOOL finished) {
            self->_messageVc.view.hidden = YES;
        }];
    }
    if (!_goodsVc.view.hidden) {
        [UIView animateWithDuration:0.2 animations:^{
            self->_goodsVc.view.mj_y = KScreenHeight;
        } completion:^(BOOL finished) {
        }];
    }
    if (self.typeViewBottomLC.constant == 0) {
        self.typeViewBottomLC.constant = -100;
        [UIView animateWithDuration:0.2 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

- (void)switchBeautyBtnClick:(UIButton *)senderBtn{
    if (senderBtn.selected) {
        return;
    }
    senderBtn.selected = YES;
    _filterBtn.selected = NO;
    
    _beautyView.hidden = NO;
    _filterView.hidden = YES;
    
    [UIView animateWithDuration:0.2 animations:^{
        self->_underline.centerX = senderBtn.centerX;
    }];
}

- (void)switchFilterBtnClick:(UIButton *)senderBtn{
    if (senderBtn.selected) {
        return;
    }
    senderBtn.selected = YES;
    _beautyBtn.selected = NO;
    
    _beautyView.hidden = YES;
    _filterView.hidden = NO;
    
    [UIView animateWithDuration:0.2 animations:^{
        self->_underline.centerX = senderBtn.centerX;
    }];
}

- (void)beautySliderValueChanged:(UISlider *)slider{
    _beautyLevel = slider.value;
    [[_pusher getBeautyManager] setBeautyLevel:_beautyLevel];
}

- (void)whiteySliderValueChanged:(UISlider *)slider{
    _whiteLevel = slider.value;
    [[_pusher getBeautyManager] setWhitenessLevel:_whiteLevel];
}

- (void)ruddySliderValueChanged:(UISlider *)slider{
    _ruddyLevel = slider.value;
    [[_pusher getBeautyManager] setRuddyLevel:_ruddyLevel];
}

- (void)filterSliderValueChanged:(UISlider *)slider{
    _filterValue = slider.value;
    [[_pusher getBeautyManager]setFilterStrength:_filterValue/10.0];
}

- (void)filterBtnClick:(UIButton *)senderBtn{
    if (senderBtn.selected) {
        return;
    }
    _selectedFilterBtn.selected = NO;
    senderBtn.selected = YES;
    _selectedFilterBtn = senderBtn;
    _selectedFilterIndex = senderBtn.tag;
    
    UIImage *lutImage = [self filterImageByMenuOptionIndex:_selectedFilterIndex+1];
    [[_pusher getBeautyManager]setFilter:lutImage];
    [[_pusher getBeautyManager]setFilterStrength:_filterValue/10.0];
}

- (void)chooseThumbTap:(UITapGestureRecognizer *)tapGes{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
    imagePickerVc.allowTakeVideo = NO;
    imagePickerVc.allowPickingGif = NO;
    imagePickerVc.allowPickingVideo = NO;
    imagePickerVc.showPhotoCannotSelectLayer = YES;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.naviTitleColor = [UIColor colorWithHexString:@"333333"];
    imagePickerVc.barItemTextColor = [UIColor colorWithHexString:@"333333"];
    imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
    imagePickerVc.allowCrop = YES;
    imagePickerVc.cropRect = CGRectMake(20, (KScreenHeight - KScreenWidth + 40)/2, KScreenWidth - 40, KScreenWidth - 40);
    [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        if (photos.count > 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self->_thumbUrl = @"";
                UIImage *selImg = photos[0];
                self.selectThumbImgView.image = selImg;
                self->_thumbImage = selImg;
            });
        }
    }];
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (UIImage*)filterImageByMenuOptionIndex:(NSInteger)index
{
    if (index == 0) {
        return nil;
    }
    TCAVFilter *filter = _filters[index-1];
    return [UIImage imageWithContentsOfFile:filter.lookupImagePath];
}

- (UIImage *)iconForFilter:(nonnull NSString *)filter selected:(BOOL)selected{
    if (nil == filter) {
        return [UIImage imageNamed:[NSString stringWithFormat:@"original%@",selected?@"_pre":@""]];
    } else if ([filter isEqualToString:@"white"]) {
        return [UIImage imageNamed:[NSString stringWithFormat:@"fwhite%@",selected?@"_pre":@""]];
    } else {
        return [UIImage imageNamed:[NSString stringWithFormat:@"%@%@",filter,selected?@"_pre":@""]];
    }
}

- (void)uploadImage{
    if (!_thumbImage) {
        [MBProgressHUD showTipMessageInWindow:@"请先选择一张封面"];
        return;
    }
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"Config/getTempKeysForCos" parameters:@{} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            configManager.txCosModel = [TxCosModel yy_modelWithDictionary:responseObject[@"data"]];
            [configManager saveTxCosModel];
            NSString *fileName = [CommonManager getNameBaseCurrentTime:@".jpg"];
            NSString *flieUploadPath = [NSString stringWithFormat:@"%@/%@",configManager.appConfig.cos_folder_image,fileName];
            QCloudCOSXMLUploadObjectRequest* put = [QCloudCOSXMLUploadObjectRequest new];
            put.object = flieUploadPath;
            put.bucket = configManager.appConfig.cos_bucket;
            put.body = UIImageJPEGRepresentation(self.selectThumbImgView.image, 1);
            [put setFinishBlock:^(QCloudUploadObjectResult *result, NSError* error) {
                //可以从 result 获取结果
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (result && !error) {
                        self->_thumbUrl = result.location;
                        [self startLive];
                    }else{
                        [commonManager hideAnimateHud];
                        [commonManager showErrorAnimateInWindow];
                    }
                });
            }];
            [[QCloudCOSTransferMangerService defaultCOSTransferManager] UploadObject:put];
        }else{
            [commonManager hideAnimateHud];
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)startLive{
    [commonManager showLoadingAnimateInWindow];
    NSDictionary *params = @{@"cateid":@(_selectedCategoryId)
                             ,@"thumb":_thumbUrl
                             ,@"title":self.titleTextField.text
                             ,@"orientation":@(2)
                             ,@"room_type":@(self.selectedRoomTypeValue)
                             ,@"price":@(self.price)
                             ,@"pwd":self.pwd
    };
    [CommonManager POST:@"Live/startLive" parameters:params success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            self.prepareView.hidden = YES;
            self.controlView.hidden = NO;
            self->_inLive = YES;
            self.liveModel = [LiveModel yy_modelWithDictionary:responseObject[@"data"]];
            [self->_pusher startPush:self.liveModel.push_url];
            if (!self.timer) {
                self.timer = [NSTimer scheduledTimerWithTimeInterval:1.f block:^(NSTimer * _Nonnull timer) {
                    self.liveDuration ++;
                    self.liveTimeLabel.text = [CommonManager formateSecond:self.liveDuration withHour:YES];
                } repeats:YES];
                [self.timer fire];
            }
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)getLiveInfo{
    [CommonManager POST:@"live/getliveinfo" parameters:@{@"liveid":@(self.liveModel.liveid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.liveModel = [LiveModel yy_modelWithDictionary:responseObject[@"data"]];
//            self.profitLabel.text = [NSString stringWithFormat:@"%d",self.liveModel.gift_profit];
        }
    } failure:^(NSError *error) {
    }];
}

- (void)sendStopMsg{
    NSDictionary *dict = @{@"Action":@"LiveFinished"};
    TIMCustomElem *timEle = [[TIMCustomElem alloc]init];
    timEle.data = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    TIMMessage *timMsg = [[TIMMessage alloc]init];
    [timMsg addElem:timEle];
    TIMConversation *conversation = [[TIMManager sharedInstance] getConversation:TIM_GROUP receiver:self.liveModel.chatroomid];
    [conversation sendMessage:timMsg succ:^{
    } fail:^(int code, NSString *msg) {
    }];
}

- (IBAction)roomTypeBtnClick:(UIButton *)sender {
    _shadowView.hidden = NO;
    self.typeViewBottomLC.constant = 0;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)roomTypeNormalBtnClick:(UIButton *)sender {
    [self.roomTypeBtn setTitle:@"普通房间" forState:UIControlStateNormal];
    self.selectedRoomTypeValue = 0;
    self.pwd = @"";
    self.price = 0;
    sender.selected = YES;
    self.feeRoomTypeBtn.selected = NO;
    self.pwdRoomTypeBtn.selected = NO;
    [self shadowViewTap];
}

- (IBAction)roomTypeFeeBtnClick:(UIButton *)sender {
    [self.roomTypeBtn setTitle:@"付费房间" forState:UIControlStateNormal];
    self.selectedRoomTypeValue = 2;
    self.pwd = @"";
    sender.selected = YES;
    self.normalRoomTypeBtn.selected = NO;
    self.pwdRoomTypeBtn.selected = NO;
    [self shadowViewTap];
    QNAlertView *alertView = [QNAlertView showQNTextFieldWithTitle:@"付费房间" placeHolder:@"请设置房间价格（金币/分钟）" textFieldType:QNTextFieldTypeNumber leftButtonTitle:nil leftClick:nil rightButtonTitle:@"确定" rightClick:^(NSString *value){
        self.price = [value intValue];
    }];
    alertView.cancelBackTap = YES;
    alertView.requireInput = YES;
}

- (IBAction)roomTypePwdBtnClick:(UIButton *)sender {
    [self.roomTypeBtn setTitle:@"私密房间" forState:UIControlStateNormal];
    self.selectedRoomTypeValue = 1;
    self.price = 0;
    sender.selected = YES;
    self.normalRoomTypeBtn.selected = NO;
    self.feeRoomTypeBtn.selected = NO;
    [self shadowViewTap];
    
    QNAlertView *alertView = [QNAlertView showQNTextFieldWithTitle:@"私密房间" placeHolder:@"请设置房间密码" textFieldType:QNTextFieldTypeNoSpecialWord leftButtonTitle:nil leftClick:nil rightButtonTitle:@"确定" rightClick:^(NSString *value){
        self.pwd = value;
    }];
    alertView.cancelBackTap = YES;
    alertView.requireInput = YES;
}


#pragma mark ————————————————————————— NSNotification ————————————————————————
-(void)imGroupEventNotification:(NSNotification *)notification{
    IMNotificationModel *notificationModel = [notification userInfo][@"data"];
    if ([notificationModel.roomid isEqualToString:self.liveModel.chatroomid]) {
        switch (notificationModel.action) {
            case IMNotificationActionLiveGroupMemberJoinExit:{
                [[TIMGroupManager sharedInstance] getGroupInfo:@[self.liveModel.chatroomid] succ:^(NSArray *groupList) {
                    if (groupList.count > 0) {
                        TIMGroupInfo *info = groupList[0];
                        self.audienceCountLabel.text = [NSString stringWithFormat:@"%d",info.memberNum];
                    }
                } fail:^(int code, NSString *msg) {
                    
                }];
            }
                break;
            default:
                break;
        }
    }
}
    
- (void)imNotification:(NSNotification *)notification{
    IMNotificationModel *notificationModel = [notification userInfo][@"data"];
    if ([notificationModel.roomid isEqualToString:self.liveModel.chatroomid]) {
        switch (notificationModel.action) {
            case IMNotificationActionRoomMessage:
                [self reciveRoomMsg:notificationModel];
                break;
            default:
                break;
        }
    }
}

- (void)reciveRoomMsg:(IMNotificationModel *)notificationModel{
    if ([notificationModel.roomid isEqualToString: self.liveModel.chatroomid]) {
        RoomChatModel *chatModel = [notificationModel.chat yy_modelCopy];
        chatModel.textColor = @"#FFFFFF";
        chatModel.nameColor = @"40ACFF";
        [_chatArray addObject:chatModel];
        [self.chatTableView reloadData];
        //滚动到最底部
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:_chatArray.count-1];
        [self.chatTableView scrollToRowAtIndexPath:indexpath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
}

#pragma mark ———————————————————  礼物弹出及队列显示开始 ——————————————————
- (void)giftAnimationNotification:(NSNotification *)notification{
    [self getLiveInfo];
    IMNotificationModel *notificationModel = [notification userInfo][@"data"];
    if (!_giftShowView) {
        _giftShowView = [[GiftShowView alloc]init];
        _giftShowView.delegate = self;
        [self.view addSubview:_giftShowView];
    }
    if (notificationModel.gift != nil) {
        [_giftShowView addModels:notificationModel];
    }
    if(!_giftShowView.animating){
        [_giftShowView enAnimationGift];
    }
}

- (void)animationGiftdelegate:(IMNotificationModel *)notificationModel{
    if (!_giftShowView) {
        _giftShowView = [[GiftShowView alloc]init];
        _giftShowView.delegate = self;
        [self.view insertSubview:_giftShowView atIndex:100];
    }
    if (notificationModel.gift != nil) {
        [_giftShowView addModels:notificationModel];
    }
    if (!_giftShowView.animating){
        [_giftShowView enAnimationGift];
    }
}

#pragma mark ————————————————————— SharePanelViewDelegate ————————————————————————
- (void)shareWithChannel:(PYShareChannel)channel panel:(nonnull SharePanelView *)panelView{
    if (channel == PYShareChannelLink){
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = [NSString stringWithFormat:@"%@%lld",[configManager appConfig].share_live_url,self.liveModel.anchor.userid];
        [MBProgressHUD showTipMessageInView:@"链接已复制到剪贴板"];
    }else{
        SSDKPlatformType type = [SharePanelView getSSDKPlatformTypeBy:channel];
        //统一创建分享参数
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:@"想和我的距离再近一点，那我的直播你必须到场，与你不期而遇"
                                         images:self.liveModel.thumb
                                            url:[NSURL URLWithString:[NSString stringWithFormat:@"%@%lld",[configManager appConfig].share_live_url,self.liveModel.anchor.userid]]
                                          title:[NSString stringWithFormat:@"我已开播，快来围观吧~"]
                                           type:SSDKContentTypeAuto];
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            switch (state) {
                case SSDKResponseStateSuccess:
                    [MBProgressHUD showTipMessageInView:@"分享成功"];
                    break;
                case SSDKResponseStateFail:
                {
                    [MBProgressHUD showTipMessageInView:@"分享失败"];
                    //失败
                    break;
                }
                case SSDKResponseStateCancel:
                    //取消
                    break;
                    
                default:
                    break;
            }
        }];
    }
}

#pragma mark —————————————————— TableViewDelegate Datasource ———————————————————————
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _chatArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RoomChatTableViewCell *cell = [RoomChatTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.chatModel = _chatArray[indexPath.section];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 5;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 5)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

#pragma mark ———————————————————————— UITextFieldDelegate ————————————————————————
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == self.titleTextField) {
        if ([self.titleTextField.text isEqualToString:@"给直播写个标题吧"]) {
            self.titleTextField.text = @"";
        }
    }
}

#pragma mark —————————————————————————— TXLivePushListener —————————————————————
- (void)onNetStatus:(NSDictionary *)param{
    
}

- (void)onPushEvent:(int)EvtID withParam:(NSDictionary *)param{
    NSLog(@"%d",EvtID);
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    if (_pusher) {
        [_pusher stopPreview];
        [_pusher stopPush];
    }
}

#pragma mark ———————————————————————— TXVideoCustomProcessDelegate ——————————————————————

- (GLuint)onPreProcessTexture:(GLuint)texture width:(CGFloat)width height:(CGFloat)height{
    if(configManager.appConfig.beauty_channel == 0)
        return texture;
    return [[TiSDKManager shareManager] renderTexture2D:texture Width:width Height:height Rotation:CLOCKWISE_0 Mirror:_pusher.frontCamera];
}

/**
 * 在OpenGL线程中回调，可以在这里释放创建的OpenGL资源
 */
- (void)onTextureDestoryed{
    NSLog(@"onTextureDestoryed");
    if(configManager.appConfig.beauty_channel == 1){
        dispatch_async(dispatch_get_main_queue(), ^{
            [[TiSDKManager shareManager] destroy];
            [[TiUIManager shareManager] destroy]; // TiSDK开源UI窗口对象资源释放
        });
    }
}

#pragma mark ———————————————————————— TiUIManagerDelegate ————————————————————————————
- (void)didClickOnExitTap{
    [self shadowViewTap];
}

#pragma mark ——————————————————————— 懒加载 ———————————————————————————

- (NSMutableArray *)shareChannelArray1{
    if (!_shareChannelArray1) {
        _shareChannelArray1 = [NSMutableArray arrayWithArray:@[@(PYShareChannelWechat),@(PYShareChannelPengyouquan),@(PYShareChannelQQ),@(PYShareChannelWibo),@(PYShareChannelQZone),@(PYShareChannelMore)]];
    }
    return _shareChannelArray1;
}

- (NSMutableArray *)shareChannelArray2{
    if (!_shareChannelArray2) {
        _shareChannelArray2 = [NSMutableArray arrayWithArray:@[@(PYShareChannelLink)]];
    }
    return _shareChannelArray2;
}

- (NSString *)pwd{
    if (!_pwd) {
        _pwd = @"";
    }
    return _pwd;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
