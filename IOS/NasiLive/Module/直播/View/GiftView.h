//
//  GiftView.h
//  Nasi
//
//  Created by yun on 2020/1/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GiftView : UIView

- (instancetype)initWithModel:(GiftModel *)model;

@property (strong, nonatomic) GiftModel *giftModel;
@property (nonatomic,assign) BOOL selected;

@end

NS_ASSUME_NONNULL_END
