//
//  RoomChatTableViewCell.h
//  Nasi
//
//  Created by yun on 2019/12/26.
//  Copyright © 2019 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "RoomChatModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RoomChatTableViewCell : BaseTableViewCell

@property (strong, nonatomic) RoomChatModel *chatModel;

@property (nonatomic,assign) BOOL backViewHidden;

@end

NS_ASSUME_NONNULL_END
