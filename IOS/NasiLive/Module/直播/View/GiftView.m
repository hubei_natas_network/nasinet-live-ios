//
//  GiftView.m
//  Nasi
//
//  Created by yun on 2020/1/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "GiftView.h"

@interface GiftView()

@property (strong, nonatomic) UIImageView *giftImgView;

@end

@implementation GiftView

- (instancetype)init{
    if (self = [super init]) {
        self.layer.cornerRadius = 3.f;
        self.layer.masksToBounds = YES;
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (instancetype)initWithModel:(GiftModel *)model{
    if (self = [super init]) {
        self.giftModel = model;
        UILabel *priceLabel = [[UILabel alloc]init];
        priceLabel.text = [NSString stringWithFormat:@"%d", model.price];
        priceLabel.textColor = [UIColor colorWithHexString:@"FFE241"];
        priceLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
        [self addSubview:priceLabel];
        [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX).offset(-6);
            make.height.mas_equalTo(12);
            make.bottom.offset(-15);
        }];
        UIImageView *coinView = [[UIImageView alloc]initWithImage:IMAGE_NAMED(@"ic_coin")];
        [self addSubview:coinView];
        [coinView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(priceLabel.mas_right).offset(2);
            make.centerY.equalTo(priceLabel.mas_centerY);
            make.width.height.mas_equalTo(10);
        }];
        
        UILabel *titleLabel = [[UILabel alloc]init];
        titleLabel.text = model.title;
        titleLabel.textColor = [UIColor colorWithHexString:@"C9C9C9"];
        titleLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.offset(0);
            make.height.mas_equalTo(12);
            make.bottom.equalTo(priceLabel.mas_top).offset(-5);
        }];
        
        self.giftImgView = [[UIImageView alloc]init];
        self.giftImgView.contentMode = UIViewContentModeScaleAspectFit;
        [self.giftImgView sd_setImageWithURL:[NSURL URLWithString:model.icon]];
        [self addSubview:self.giftImgView];
        [self.giftImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(15);
            make.bottom.equalTo(titleLabel.mas_top).offset(-5);
            make.centerX.equalTo(self.mas_centerX);
            make.width.mas_equalTo(self.giftImgView.mas_height);
        }];
        
        if (model.use_type != GiftSepcialTypeNone) {
            UIImageView *flagImgView = [[UIImageView alloc]init];
            flagImgView.contentMode = UIViewContentModeScaleAspectFit;
            flagImgView.image = [UIImage imageNamed:model.use_type == GiftSepcialTypeGuard ?@"gift_special_type_guard":@"gift_special_type_all"];
            [self addSubview:flagImgView];
            [flagImgView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.offset(5);
                make.right.offset(-5);
                make.width.mas_equalTo(model.use_type == GiftSepcialTypeGuard ? 24 : 14);
                make.height.mas_equalTo(14);
            }];
        }
        
    }
    return self;
}

- (void)setGiftModel:(GiftModel *)giftModel{
    _giftModel = giftModel;
}

- (void)setSelected:(BOOL)selected{
    _selected = selected;
    if (selected) {
        self.backgroundColor = [UIColor colorWithHexString:@"#313343"];
        //添加动画
        //缩小效果，并回到原位
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        //速度控制函数，控制动画运行的节奏
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        animation.duration = 0.8;       //执行时间
        animation.repeatCount = INT_MAX;      //执行次数
        animation.autoreverses = YES;    //完成动画后会回到执行动画之前的状态
        animation.fromValue = [NSNumber numberWithFloat:1.0];   //初始伸缩倍数
        animation.toValue = [NSNumber numberWithFloat:0.8];     //结束伸缩倍数
        [[self.giftImgView layer]addAnimation:animation forKey:nil];
    }else{
        self.backgroundColor = [UIColor clearColor];
        [[self.giftImgView layer]removeAllAnimations];
    }
}

@end
