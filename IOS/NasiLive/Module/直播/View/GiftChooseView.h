//
//  GiftChooseView.h
//  NasiLive
//
//  Created by yun on 2020/3/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol GiftChooseViewDelegate <NSObject>

@optional
- (void)sendGift:(GiftModel *)gift count:(int)count;

@end

@interface GiftChooseView : UIView

+ (instancetype)showInView:(UIView *)superView;
- (instancetype)initWithSafeAreaInsets:(UIEdgeInsets)safeAreaInsets;

- (void)show;
- (void)hide;

@property (weak, nonatomic) id<GiftChooseViewDelegate> delegate;

@property (assign, nonatomic, readonly) int listRows;                                   //默认2行
@property (assign, nonatomic, readonly) UIEdgeInsets safeAreaInsets;                    //内容缩进

@property (assign, nonatomic) UIInterfaceOrientation interfaceOrientation;              //屏幕方向

- (void)refreshUserCoinNum;

@end

NS_ASSUME_NONNULL_END
