//
//  LiveCollectionViewCell.h
//  NasiLive
//
//  Created by yun on 2020/2/24.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LiveModel;

NS_ASSUME_NONNULL_BEGIN

@interface LiveCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *anchorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *hotLabel;
@property (weak, nonatomic) IBOutlet UILabel *liveTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *anchorLevelImgView;
@property (weak, nonatomic) IBOutlet UIImageView *roomTypeImgView;
@property (weak, nonatomic) IBOutlet UIImageView *pkFlagImgView;


@property (strong, nonatomic) LiveModel *model;

@end

NS_ASSUME_NONNULL_END
