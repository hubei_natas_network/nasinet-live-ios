//
//  RoomChatTableViewCell.m
//  Nasi
//
//  Created by yun on 2019/12/26.
//  Copyright © 2019 yun7. All rights reserved.
//

#import "RoomChatTableViewCell.h"

@interface RoomChatTableViewCell()

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation RoomChatTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setChatModel:(RoomChatModel *)chatModel{
    _chatModel = chatModel;
    if (chatModel.chatType == RoomChatTypeSysMsg) {
        _titleLabel.textColor = [UIColor colorWithHexString:@"8be8ff"];
        _titleLabel.text = chatModel.message;
    }else{
        _titleLabel.textColor = [UIColor colorWithHexString:chatModel.textColor];
        _titleLabel.text = [NSString stringWithFormat:@"%@：%@",chatModel.sender.nick_name,chatModel.message];
        
        NSMutableAttributedString *msgStr = [[NSMutableAttributedString alloc] initWithString:_titleLabel.text attributes:nil];
        NSAttributedString *speaceString = [[NSAttributedString alloc]initWithString:@" "];
        
        //等级标识
        NSTextAttachment *levelAttchment = [[NSTextAttachment alloc]init];
        levelAttchment.bounds = CGRectMake(0, -3, 34, 15);//设置frame
        levelAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_user_level_%d",chatModel.sender.user_level]];
        NSAttributedString *levelString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(levelAttchment)];
        
        //守护标识
        NSTextAttachment *guardAttchment = [[NSTextAttachment alloc]init];
        guardAttchment.bounds = CGRectMake(0, -3, 34, 15);//设置frame
        guardAttchment.image = [UIImage imageNamed:@"ic_guardian"];
        NSAttributedString *guardString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(guardAttchment)];
        
        //房管标识
        NSTextAttachment *mgrAttchment = [[NSTextAttachment alloc]init];
        mgrAttchment.bounds = CGRectMake(0, -3, 34, 15);//设置frame
        mgrAttchment.image = [UIImage imageNamed:@"ic_room_manager"];
        NSAttributedString *mgrString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(mgrAttchment)];
        
        //vip标识
        NSTextAttachment *vipAttchment = [[NSTextAttachment alloc]init];
        vipAttchment.bounds = CGRectMake(0, -4, 16, 19);//设置frame
        vipAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"vip_level_%d",[chatModel.sender getVipLevel]]];//设置图片
        NSAttributedString *vipString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(vipAttchment)];
        
        NSRange nameRange = NSMakeRange(0, chatModel.sender.nick_name.length+1);
        [msgStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:chatModel.nameColor] range:nameRange];
        
        if (chatModel.is_guardian) {
            //插入守护图标
            [msgStr insertAttributedString:speaceString atIndex:0];//插入到第几个下标
            [msgStr insertAttributedString:guardString atIndex:0];//插入到第几个下标
        }
        
        //插入等级图标
        [msgStr insertAttributedString:speaceString atIndex:0];//插入到第几个下标
        [msgStr insertAttributedString:levelString atIndex:0];//插入到第几个下标
        
        if (chatModel.is_manager) {
            //插入房管图标
            [msgStr insertAttributedString:speaceString atIndex:0];//插入到第几个下标
            [msgStr insertAttributedString:mgrString atIndex:0];//插入到第几个下标
        }
        
        //插入VIP图标
        if ([chatModel.sender checkVip]) {
            [msgStr insertAttributedString:speaceString atIndex:0];//插入到第几个下标
            [msgStr insertAttributedString:vipString atIndex:0];//插入到第几个下标
        }
        
        [_titleLabel setAttributedText:msgStr];
    }
}

- (void)setBackViewHidden:(BOOL)backViewHidden{
    _backViewHidden = backViewHidden;
    self.backView.hidden = backViewHidden;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
