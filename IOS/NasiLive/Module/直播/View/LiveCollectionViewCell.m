//
//  LiveCollectionViewCell.m
//  NasiLive
//
//  Created by yun on 2020/2/24.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "LiveCollectionViewCell.h"

#import "LiveModel.h"

@implementation LiveCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(LiveModel *)model{
    _model = model;
    
    self.pkFlagImgView.hidden = model.pk_status != PkStatusInPk;
    self.roomTypeImgView.hidden = model.pk_status == PkStatusInPk;
    
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:model.thumb] placeholderImage:IMAGE_NAMED(@"cover_loading")];
    self.anchorNameLabel.text = model.anchor.nick_name;
    self.hotLabel.text = [CommonManager formateCount:model.hot];
    self.liveTitleLabel.text = model.title;
    self.anchorLevelImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_anchor_level_%d",model.anchor.anchor_level]];
    switch (model.room_type) {
        case LiveRoomTypeNormal:
            self.roomTypeImgView.image = [UIImage imageNamed:@"ic_room_normal"];
            break;
        case LiveRoomTypeFee:
            self.roomTypeImgView.image = [UIImage imageNamed:@"ic_room_fee"];
            break;
        case LiveRoomTypePwd:
            self.roomTypeImgView.image = [UIImage imageNamed:@"ic_room_pwd"];
            break;
        default:
            self.roomTypeImgView.image = nil;
            break;
    }
    if (configManager.appConfig.switch_iap) {
        self.roomTypeImgView.image = nil;
    }
}

@end
