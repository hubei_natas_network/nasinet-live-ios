//
//  GiftChooseView.m
//  NasiLive
//
//  Created by yun on 2020/3/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "GiftChooseView.h"
#import "GiftView.h"

@interface GiftChooseView()<UIScrollViewDelegate>{
    NSArray *giftArray;
    
    CADisplayLink *_displayLink;
    CGFloat _transAngle;
}

@property (strong, nonatomic) UIView *shadowView;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) UIView *containerView;

@property (weak, nonatomic) UIScrollView *scrollView;
@property (weak, nonatomic) UIPageControl *pageControl;

@property (weak, nonatomic) UIButton *sendBtn;

@property (weak, nonatomic) UIImageView *continueSendBackImgView;
@property (weak, nonatomic) UIButton *continueSendBtn;
@property (weak, nonatomic) UIButton *countSelBtn;
@property (strong, nonatomic) UIImageView *countSelImgView;


@property (weak, nonatomic) UILabel *coinNumLabel;

@property (weak, nonatomic) UIButton *activityIndicator;

@property (weak, nonatomic) GiftView *selectedGiftView;

@end

@implementation GiftChooseView

+ (instancetype)showInView:(UIView *)superView{
    GiftChooseView *chooseView = [[self alloc]initWithSafeAreaInsets:superView.safeAreaInsets];
    [superView addSubview:chooseView];
    [chooseView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    [chooseView show];
    return chooseView;
}

- (instancetype)initWithSafeAreaInsets:(UIEdgeInsets)safeAreaInsets{
    if (self = [super init]) {
        
        self.hidden = YES;
        _interfaceOrientation = UIInterfaceOrientationPortrait;
        _safeAreaInsets = safeAreaInsets;
        _listRows = 2;
        
        self.shadowView = [[UIView alloc]init];
        self.shadowView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.01];
        self.shadowView.userInteractionEnabled = YES;
        self.shadowView.hidden = YES;
        self.shadowView.alpha = 0;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hide)];
        [self.shadowView addGestureRecognizer:tap];
        [self addSubview:self.shadowView];
        [self.shadowView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsZero);
        }];
        
        self.contentView = [[UIView alloc]initWithFrame:CGRectMake(0, KScreenHeight, KScreenWidth, 320 + kBottomSafeHeight)];
        self.contentView.backgroundColor = [UIColor colorWithHexString:@"#151723"];
        [self addSubview:self.contentView];
        
        self.containerView = [[UIView alloc]init];
        [self.contentView addSubview:self.containerView];
        if (@available(iOS 11,*)) {
            [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.mas_equalTo(0);
                make.left.mas_equalTo(safeAreaInsets.left);
                make.right.mas_equalTo(-safeAreaInsets.right);
                make.bottom.mas_equalTo(-safeAreaInsets.bottom);
            }];
        }else {
           [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
               make.edges.mas_equalTo(UIEdgeInsetsZero);
           }];
        }
        
        UILabel *titleLabel = [[UILabel alloc]init];
        titleLabel.text = @"礼物";
        titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
        titleLabel.textColor = [UIColor whiteColor];
        [self.containerView addSubview:titleLabel];
        [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(13);
            make.leading.mas_equalTo(15);
            make.width.mas_equalTo(100);
            make.height.mas_equalTo(15);
        }];
        
        UIView *bottomView = [[UIView alloc]init];
        [self.containerView addSubview:bottomView];
        [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.mas_equalTo(0);
            make.height.mas_equalTo(28);
            make.bottom.mas_equalTo(-12);
        }];
        
        UILabel *goldLabel = [[UILabel alloc]init];
        goldLabel.textColor = [UIColor whiteColor];
        goldLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
        goldLabel.text = [NSString stringWithFormat:@"%d",userManager.curUserInfo.gold];
        [bottomView addSubview:goldLabel];
        self.coinNumLabel = goldLabel;
        [goldLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15);
            make.top.bottom.mas_equalTo(0);
        }];
        UIImageView *coinView = [[UIImageView alloc]initWithImage:IMAGE_NAMED(@"ic_coin")];
        coinView.contentMode = UIViewContentModeScaleAspectFill;
        [bottomView addSubview: coinView];
        [coinView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(goldLabel.mas_right).offset(3);
            make.centerY.equalTo(goldLabel.mas_centerY);
            make.width.height.mas_equalTo(10);
        }];
        
        UIPageControl *pageControl = [[UIPageControl alloc]init];
        pageControl.pageIndicatorTintColor = [UIColor grayColor];
        pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
        pageControl.hidesForSinglePage = YES;
        [bottomView addSubview:pageControl];
        self.pageControl = pageControl;
        [pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(goldLabel.mas_centerY);
            make.centerX.equalTo(bottomView.mas_centerX);
        }];
        
        UIButton *sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [sendBtn setImage:IMAGE_NAMED(@"btn_gift_send") forState:UIControlStateNormal];
        [sendBtn addTarget:self action:@selector(sendBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [bottomView addSubview:sendBtn];
        self.sendBtn = sendBtn;
        [sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(62);
            make.height.mas_equalTo(28);
            make.right.offset(-15);
            make.centerY.equalTo(bottomView.mas_centerY);
        }];
        
        UIImageView *continueSendBackIv = [[UIImageView alloc]initWithImage:IMAGE_NAMED(@"btn_gift_continue_send")];
        self.continueSendBackImgView = continueSendBackIv;
        continueSendBackIv.hidden = YES;
        continueSendBackIv.userInteractionEnabled = YES;
        [bottomView addSubview:continueSendBackIv];
        [self.continueSendBackImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(118);
            make.height.mas_equalTo(28);
            make.right.offset(-15);
            make.centerY.equalTo(bottomView.mas_centerY);
        }];
        
        UIButton *continueSendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.continueSendBtn = continueSendBtn;
        [self.continueSendBtn setTitle:@"" forState:UIControlStateNormal];
        [self.continueSendBackImgView addSubview:self.continueSendBtn];
        [continueSendBtn addTarget:self action:@selector(continueSendBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.continueSendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(118/2);
            make.right.top.bottom.offset(0);
        }];
        
        UIButton *countSelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [countSelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [countSelBtn setImage:IMAGE_NAMED(@"ic_gift_count_up") forState:UIControlStateNormal];
        [countSelBtn setImage:IMAGE_NAMED(@"ic_gift_count_down") forState:UIControlStateSelected];
        [countSelBtn setTitle:@"1" forState:UIControlStateNormal];
        countSelBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        countSelBtn.titleEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 5);
        countSelBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 48, 0, 5);
        [countSelBtn addTarget:self action:@selector(countSelBtnClick) forControlEvents:UIControlEventTouchUpInside];
        self.countSelBtn = countSelBtn;
        [self.continueSendBackImgView addSubview:self.countSelBtn];
        [self.countSelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-118/2);
            make.left.top.bottom.offset(0);
        }];
        
        
        UIScrollView *scrollView = [[UIScrollView alloc]init];
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.bounces = NO;
        scrollView.pagingEnabled = YES;
        scrollView.delegate = self;
        [self.containerView addSubview:scrollView];
        self.scrollView = scrollView;
        [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.mas_equalTo(0);
            make.top.equalTo(titleLabel.mas_bottom).offset(15);
            make.bottom.equalTo(bottomView.mas_top).offset(-15);
        }];
        
        UIButton *activityIndicator = [UIButton buttonWithType:UIButtonTypeCustom];
        [activityIndicator setImage:IMAGE_NAMED(@"ic_refresh") forState:UIControlStateNormal];
        [activityIndicator addTarget:self action:@selector(refresh) forControlEvents:UIControlEventTouchUpInside];
        [self.containerView addSubview:activityIndicator];
        self.activityIndicator = activityIndicator;
        [activityIndicator mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(scrollView.mas_centerX);
            make.centerY.equalTo(scrollView.mas_centerY);
            make.height.width.mas_equalTo(30);
        }];
        
        self.countSelImgView = [[UIImageView alloc]initWithImage:IMAGE_NAMED(@"bg_gift_count_sel")];
        [self.containerView addSubview:self.countSelImgView];
        self.countSelImgView.hidden = YES;
        self.countSelImgView.userInteractionEnabled = YES;
        [self.countSelImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-54);
            make.bottom.equalTo(bottomView.mas_top).offset(-5);
            make.height.mas_equalTo(225);
            make.width.mas_equalTo(82);
        }];
        
        NSArray *countArr = @[@(1314),@(520),@(100),@(88),@(66),@(10),@(1)];
        for (int i = 0; i < countArr.count; i++) {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn setTitle:[NSString stringWithFormat:@"%@",countArr[i]] forState:UIControlStateNormal];
            [btn setTitleColor:CFontColor forState:UIControlStateNormal];
            btn.titleLabel.font = FFont12;
            btn.tag = [countArr[i] intValue];
            [btn addTarget:self action:@selector(countBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            [self.countSelImgView addSubview:btn];
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.offset(0);
                make.top.mas_equalTo(5+30*i);
                make.height.mas_equalTo(30);
            }];
        }

        _displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(rotate)];
        [_displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
        _displayLink.paused = true;
        [_displayLink setPaused:NO];
        
        [self loadGifts];
    }
    return self;
}

- (void)show{
    self.hidden = NO;
    [UIView animateWithDuration:0.2 animations:^{
        self.shadowView.hidden = NO;
        self.shadowView.alpha = 1;
        self.contentView.hidden = NO;
        self.contentView.alpha = 1;
        self.contentView.mj_y = KScreenHeight - self.contentView.height;
        [self refreshUserCoinNum];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hide{
    [UIView animateWithDuration:0.2 animations:^{
        self.shadowView.alpha = 0;
        self.contentView.alpha = 0;
        self.contentView.mj_y = KScreenHeight;
    } completion:^(BOOL finished) {
        self.hidden = YES;
        self.shadowView.hidden = YES;
        self.contentView.hidden = YES;
        self.countSelBtn.selected = NO;
        self.countSelImgView.hidden = YES;
    }];
}

- (void)refresh{
    [_displayLink setPaused:NO];
    [self loadGifts];
}

- (void)loadGifts{
    [CommonManager POST:@"Gift/getGiftList" parameters:nil success:^(id responseObject) {
        [self->_displayLink setPaused:YES];
        if (RESP_SUCCESS(responseObject)) {
            self.activityIndicator.hidden = YES;
            self->giftArray = [NSMutableArray yy_modelArrayWithClass:[GiftModel class] json:responseObject[@"data"]];
            [self createGiftsView];
        }
    } failure:^(NSError *error) {
        [self->_displayLink setPaused:YES];
    }];
}

- (void)createGiftsView{
    [self.scrollView removeAllSubviews];
    CGFloat scrollViewWidth = KScreenWidth - self.safeAreaInsets.left - self.safeAreaInsets.right;
    int perLineCount =  (self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft || self.interfaceOrientation == UIInterfaceOrientationLandscapeRight) ? 8 : 4; //每行数量
    CGFloat width = scrollViewWidth / perLineCount;
    for (int i = 0; i < giftArray.count; i++) {
        CGFloat x = (i%perLineCount)*width + (i/(self.listRows*perLineCount))*scrollViewWidth;
        GiftView *view = [[GiftView alloc] initWithModel:giftArray[i]];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(giftTap:)];
        [view addGestureRecognizer:tap];
        [self.scrollView addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(x);
            make.width.mas_equalTo(width);
            make.height.equalTo(self.scrollView.mas_height).multipliedBy(1.f/self.listRows);
            if (self.listRows == 1) {
                make.centerY.equalTo(self.scrollView.mas_centerY);
            }else{
                make.centerY.equalTo(self.scrollView.mas_centerY).multipliedBy(0.5+(i/perLineCount)%self.listRows);
            }
        }];
    }
    self.scrollView.contentSize = CGSizeMake(scrollViewWidth*ceil((giftArray.count+0.1)/(self.listRows*perLineCount)), 0);
    self.pageControl.numberOfPages = ceil((giftArray.count+0.1)/(self.listRows*perLineCount));
}

- (void)giftTap:(UIGestureRecognizer *)ges{
    GiftView *giftView = (GiftView *)ges.view;
    self.selectedGiftView.selected = NO;
    giftView.selected = YES;
    self.selectedGiftView = giftView;
    
    if (self.selectedGiftView.giftModel.type == GiftTypeNormal) {
        self.continueSendBackImgView.hidden = NO;
        self.sendBtn.hidden = YES;
    }else{
        self.continueSendBackImgView.hidden = YES;
        self.sendBtn.hidden = NO;
    }
}

- (void)sendBtnClick{
    if (!self.selectedGiftView) {
        [MBProgressHUD showTipMessageInWindow:@"请先选择礼物"];
        return;
    }
    if ([self.delegate respondsToSelector:@selector(sendGift:count:)]) {
        [self.delegate sendGift:self.selectedGiftView.giftModel count:1];
    }
}

- (void)continueSendBtnClick{
    if (!self.selectedGiftView) {
        [MBProgressHUD showTipMessageInWindow:@"请先选择礼物"];
        return;
    }
    if ([self.delegate respondsToSelector:@selector(sendGift:count:)]) {
        [self.delegate sendGift:self.selectedGiftView.giftModel count:[self.countSelBtn.titleLabel.text intValue]];
    }
}

- (void)countBtnClick:(UIButton *)sender{
    self.countSelImgView.hidden = YES;
    self.countSelBtn.selected = NO;
    [self.countSelBtn setTitle:[NSString stringWithFormat:@"%ld",(long)sender.tag] forState:UIControlStateNormal];
}

- (void)countSelBtnClick{
    self.countSelBtn.selected = !self.countSelBtn.selected;
    self.countSelImgView.hidden = !self.countSelBtn.selected;
}

- (void)refreshUserCoinNum{
    self.coinNumLabel.text = [NSString stringWithFormat:@"%d",userManager.curUserInfo.gold];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    long index = scrollView.mj_offsetX / scrollView.width;
    if (index < 0) {
        index = 0;
    }
    if (index > self.pageControl.numberOfPages - 1) {
        index = self.pageControl.numberOfPages - 1;
    }
    self.pageControl.currentPage = index;
}

- (void)rotate{
    _transAngle -= M_PI/80;
    self.activityIndicator.transform = CGAffineTransformMakeRotation(_transAngle);
}

#pragma mark  ———————————— 屏幕旋转相关 ——————————————
- (void)setInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    if (_interfaceOrientation == interfaceOrientation) {
        return;
    }
    [self hide];
    _interfaceOrientation = interfaceOrientation;
    switch (interfaceOrientation) {
        case UIInterfaceOrientationUnknown:
        case UIInterfaceOrientationPortraitUpsideDown:
        case UIInterfaceOrientationPortrait:{
            _safeAreaInsets = UIEdgeInsetsMake(0, 0, 34, 0);
            if (_listRows != 2) {
                _listRows = 2;
                [self createGiftsView];
            }
            self.contentView.frame = CGRectMake(0, KScreenHeight, KScreenWidth, 100 + KScreenWidth/2 + 100 + kBottomSafeHeight);
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:{
            _safeAreaInsets = UIEdgeInsetsMake(0, 44, 21, 44);
            if (_listRows != 1) {
                _listRows = 1;
                [self createGiftsView];
            }
            self.contentView.frame = CGRectMake(0, KScreenHeight, KScreenWidth, 220);
        }
            break;
        default:
            break;
    }
    [self.scrollView scrollToLeft];
    if (@available(iOS 11,*)) {
        [self.containerView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.safeAreaInsets);
        }];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
