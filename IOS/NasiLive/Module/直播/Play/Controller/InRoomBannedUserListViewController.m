//
//  InRoomBannedUserListViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/10/23.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "InRoomBannedUserListViewController.h"

#import "LiveRoomBannedUserViewController.h"

@interface InRoomBannedUserListViewController (){
    UILabel                                 *_titleLabel;
    
    LiveRoomBannedUserViewController        *_bannedVc;
}

@end

@implementation InRoomBannedUserListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    UIView *cornerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 44+14)];
    cornerView.backgroundColor = [UIColor whiteColor];
    cornerView.layer.cornerRadius = 7.f;
    [self.view addSubview:cornerView];
    
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 44)];
    _titleLabel.text = @"禁言列表";
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    _titleLabel.textColor = CFontColor;
    [self.view addSubview:_titleLabel];
    
    _bannedVc = [[LiveRoomBannedUserViewController alloc]init];
    _bannedVc.anchorid = self.anchorid;
    [self addChildViewController:_bannedVc];
    [self.view addSubview:_bannedVc.view];
    [_bannedVc.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(44);
        make.height.mas_equalTo(500);
    }];
}

- (void)refreshData{
    [_bannedVc refreshData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
