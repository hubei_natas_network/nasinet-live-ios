//
//  OpenGuardianViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/12/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "OpenGuardianViewController.h"
#import "RechargeViewController.h"

#import "GuardPriceModel.h"
#import "AnchorGuardianModel.h"

@interface OpenGuardianViewController (){
    NSInteger               selectedIndex;
    int                     price;
}

@property (weak, nonatomic) IBOutlet UIView *yearView;
@property (weak, nonatomic) IBOutlet UIImageView *yearBgImgView;
@property (weak, nonatomic) IBOutlet UILabel *yearTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearPriceLabel;

@property (weak, nonatomic) IBOutlet UIView *monthView;
@property (weak, nonatomic) IBOutlet UIImageView *monthBgImgView;
@property (weak, nonatomic) IBOutlet UILabel *monthTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthPriceLabel;

@property (weak, nonatomic) IBOutlet UIView *weekView;
@property (weak, nonatomic) IBOutlet UIImageView *weekBgImgView;
@property (weak, nonatomic) IBOutlet UILabel *weekTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *weekPriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *goldCountLabel;

@property (weak, nonatomic) IBOutlet UILabel *expireTimeLabel;

@property (weak, nonatomic) IBOutlet UIView *tequan3View;
@property (weak, nonatomic) IBOutlet UIView *tequan4View;

@property (weak, nonatomic) IBOutlet UIButton *buyBtn;
@end

@implementation OpenGuardianViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *tapYear = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapYearView)];
    [self.yearView addGestureRecognizer:tapYear];
    
    UITapGestureRecognizer *tapMonth = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapMonthView)];
    [self.monthView addGestureRecognizer:tapMonth];
    
    UITapGestureRecognizer *tapWeek = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapWeekView)];
    [self.weekView addGestureRecognizer:tapWeek];
    
    self.weekPriceLabel.text = [self formatePrice:configManager.guardPriceModel.week_price];
    self.monthPriceLabel.text = [self formatePrice:configManager.guardPriceModel.month_price];
    self.yearPriceLabel.text = [self formatePrice:configManager.guardPriceModel.year_price];
    
    [self refreshData];
}

- (void)refreshData{
    self.goldCountLabel.text = @(userManager.curUserInfo.gold).stringValue;
    if (self.guardModel && [self.guardModel checkGuard]){
        [self.buyBtn setTitle:@"续费（9折）" forState:UIControlStateNormal];
        self.expireTimeLabel.text = [NSString stringWithFormat:@"我的守护：%@",self.guardModel.expire_time];
    }else{
        self.expireTimeLabel.hidden = YES;
    }
    self.goldCountLabel.text = @(userManager.curUserInfo.gold).stringValue;
}

- (IBAction)buyBtnClick:(id)sender {
    if (userManager.curUserInfo.gold < price) {
        //弹出充值窗口
        [QNAlertView showAlertViewWithType:QNAlertTypeWarning title:@"" contentText:@"余额不足" leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"充值" rightClick:^{
            RechargeViewController *vc = [[RechargeViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }];
        return;
    }
    int isRenew = 0;
    if (self.guardModel && [self.guardModel checkGuard]) {
        isRenew = 1;
    }
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"Anchor/guard" parameters:@{@"anchorid":@(self.anchorid),@"renew":@(isRenew),@"type":@(selectedIndex)} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            [commonManager showSuccessAnimateInWindow];
            self.guardModel.expire_time = responseObject[@"data"][@"guard"][@"expire_time"];
            self.guardModel.type = selectedIndex;
            userManager.curUserInfo.gold = [responseObject[@"data"][@"gold"] intValue];
            [userManager saveUserInfo];
            self.expireTimeLabel.hidden = NO;
            self.expireTimeLabel.text = [NSString stringWithFormat:@"我的守护：%@",self.guardModel.expire_time];
            self.goldCountLabel.text = @(userManager.curUserInfo.gold).stringValue;
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
    }];
}

- (IBAction)rechargeBtnClick:(id)sender {
    RechargeViewController *vc = [[RechargeViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)tapYearView{
    selectedIndex = 0;
    price = configManager.guardPriceModel.week_price;
    [self setGuardSelected:YES imageView:self.yearBgImgView titleLabel:self.yearTitleLabel priceLabel:self.yearPriceLabel];
    [self setGuardSelected:NO imageView:self.monthBgImgView titleLabel:self.monthTitleLabel priceLabel:self.monthPriceLabel];
    [self setGuardSelected:NO imageView:self.weekBgImgView titleLabel:self.weekTitleLabel priceLabel:self.weekPriceLabel];
    self.tequan3View.alpha = 1;
    self.tequan4View.alpha = 1;
}

- (void)tapMonthView{
    selectedIndex = 1;
    price = configManager.guardPriceModel.month_price;
    [self setGuardSelected:NO imageView:self.yearBgImgView titleLabel:self.yearTitleLabel priceLabel:self.yearPriceLabel];
    [self setGuardSelected:YES imageView:self.monthBgImgView titleLabel:self.monthTitleLabel priceLabel:self.monthPriceLabel];
    [self setGuardSelected:NO imageView:self.weekBgImgView titleLabel:self.weekTitleLabel priceLabel:self.weekPriceLabel];
    self.tequan3View.alpha = 1;
    self.tequan4View.alpha = 0.5;
}

- (void)tapWeekView{
    selectedIndex = 2;
    price = configManager.guardPriceModel.year_price;
    [self setGuardSelected:NO imageView:self.yearBgImgView titleLabel:self.yearTitleLabel priceLabel:self.yearPriceLabel];
    [self setGuardSelected:NO imageView:self.monthBgImgView titleLabel:self.monthTitleLabel priceLabel:self.monthPriceLabel];
    [self setGuardSelected:YES imageView:self.weekBgImgView titleLabel:self.weekTitleLabel priceLabel:self.weekPriceLabel];
    self.tequan3View.alpha = 0.5;
    self.tequan4View.alpha = 0.5;
}

- (void)setGuardSelected:(BOOL)selected imageView:(UIImageView *)imgView titleLabel:(UILabel *)titleLabel priceLabel:(UILabel *)priceLabel{
    imgView.image = selected ? IMAGE_NAMED(@"bg_guard_red") : IMAGE_NAMED(@"bg_guard_white");
    titleLabel.textColor = selected ? [UIColor whiteColor] : [UIColor blackColor];
    priceLabel.textColor = selected ? [UIColor whiteColor] : [UIColor colorWithHexString:@"#FF8B02"];
}

- (NSString *)formatePrice:(NSInteger)price{
    if (price > 10000) {
        return [NSString stringWithFormat:@"%.0f万金币",price * 1.0/10000];
    }else{
        return [NSString stringWithFormat:@"%ld金币",(long)price];
    }
}

- (UIView *)listView{
    return self.view;
}


@end
