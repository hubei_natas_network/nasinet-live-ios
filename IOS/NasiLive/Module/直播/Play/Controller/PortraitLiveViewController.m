//
//  PortraitLiveViewController.m
//  NasiLive
//
//  Created by yun on 2020/2/26.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "PortraitLiveViewController.h"
#import "ContributeRankViewController.h"
#import "RechargeViewController.h"
#import "MessagePopListViewController.h"
#import "UserInfoViewController.h"
#import "MessageChatViewController.h"
#import "ReportCategoryViewController.h"
#import "VipCenterViewController.h"
#import "QNH5ViewController.h"
#import "LiveGoodsViewController.h"
#import "InRoomBannedUserListViewController.h"
#import "GuardianViewController.h"

#import "IQKeyboardManager.h"

#import "MLVBLiveManager.h"

#import "ImSDK.h"

#import "ShopGoodsModel.h"
#import "AnchorGuardianModel.h"

#import "SharePanelView.h"
#import "RoomChatTableViewCell.h"
#import "RoomChatModel.h"
#import "GiftChooseView.h"
#import "GiftShowView.h"
#import "ContinueGiftView.h"
#import "NormalGiftBackView.h"
#import "VipEnterAnimationView.h"

#import "UserCardView.h"

#import "ContributeRankCollectionViewCell.h"

#import <TiSDK/TiSDKInterface.h> // TiSDK接口头文件

#import "SuperPlayer.h"

@interface PortraitLiveViewController ()<SuperPlayerDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,GiftChooseViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UserCardViewDelegate,SharePanelViewDelegate,TXLivePushListener,TXVideoCustomProcessDelegate>{
    
    TXLivePush          *_pusher;
    
    MLVBLiveManager         *_liveManager;
    
    NSMutableArray          *_chatArray;
    
    UITextField             *_keyField;
    UIButton                *_msgSendBtn;
    UIView                  *_keyboardToolBar;
    
    GiftShowView            *_giftShowView;
    GiftChooseView          *_chooseGiftView;
    UIView                  *_shadowView;
    
    ContinueGiftView        *_continueGiftView;
    NormalGiftBackView      *_normalGiftBackView;
    VipEnterAnimationView   *_vipEnterAnimationView;
    
    TIMConversation         *_conversation;
    
    BOOL                    _joinGroupFlag;
    BOOL                    _isMgr;
    
    ContributeRankViewController                *_contributeVC;
    InRoomBannedUserListViewController          *_bannedUserListVc;
    MessagePopListViewController                *_messageVc;
    LiveGoodsViewController                     *_goodsVc;
    GuardianViewController                      *_guardVc;
    
    CGFloat                 _filterValue;
    CGFloat                 _beautyLevel;
    CGFloat                 _whiteLevel;
    CGFloat                 _ruddyLevel;
    
    int                     liveManagerRetryTimes;
    NSString                *_linkPushUrl;
}

@property (strong, nonatomic) LiveModel *liveModel;

@property (weak, nonatomic) IBOutlet UIView *controlView;
@property (weak, nonatomic) IBOutlet UIImageView *bgHolderImgView;
@property (weak, nonatomic) IBOutlet UIView *playerContainerView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *anchorIconView;
@property (weak, nonatomic) IBOutlet UILabel *anchorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *liveHotLabel;
@property (weak, nonatomic) IBOutlet UIButton *attentBtn;
@property (weak, nonatomic) IBOutlet UIButton *showGoodsBtn;
@property (weak, nonatomic) IBOutlet UITableView *msgTableView;
@property (weak, nonatomic) IBOutlet UIButton *sendMsgBtn;
@property (weak, nonatomic) IBOutlet UIView *rankContainerView;
@property (weak, nonatomic) IBOutlet UILabel *audienceCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *bannedUserListBtn;
@property (weak, nonatomic) IBOutlet UILabel *guardCountLabel;

@property (weak, nonatomic) IBOutlet UIView *pkView;
@property (weak, nonatomic) IBOutlet UIView *pkPlayerView1;
@property (weak, nonatomic) IBOutlet UIView *pkPlayerView2;
@property (weak, nonatomic) IBOutlet UIView *scoreView1;
@property (weak, nonatomic) IBOutlet UIView *scoreView2;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel1;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel2;
@property (weak, nonatomic) IBOutlet UILabel *pkTimerLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scoreView1WidthLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scoreView2WidthLC;
@property (weak, nonatomic) IBOutlet UIImageView *pkResultImgView1;
@property (weak, nonatomic) IBOutlet UIImageView *pkResultImgView2;
@property (weak, nonatomic) IBOutlet UIImageView *pkResultImgView3;

@property (weak, nonatomic) IBOutlet UIView *pkHolderView;
@property (weak, nonatomic) IBOutlet UIView *pkAnchorView;
@property (weak, nonatomic) IBOutlet UILabel *pkAnchorNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *pkAnchorIconView;
@property (weak, nonatomic) IBOutlet UIButton *pkAnchorAttentBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pkAnchorViewLeadingLC;


@property (weak, nonatomic) IBOutlet UIView *explainingGoodsView;
@property (weak, nonatomic) IBOutlet UIImageView *explainingGoodsImgView;
@property (weak, nonatomic) IBOutlet UILabel *explainingGoodsPriceLabel;

@property (strong, nonatomic) UICollectionView *rankUserCollectionView;

@property (weak, nonatomic) IBOutlet UIView *localContainerView;
@property (weak, nonatomic) IBOutlet UIView *localPreviewView;

@property (strong, nonatomic) SVGAPlayer *svgaPkPlayer;
@property (strong, nonatomic) SVGAPlayer *svgaPkWinPlayer;

@property (strong, nonatomic) NSTimer *timer;
@property (nonatomic,assign) NSInteger timerCount;

@property (strong, nonatomic) AnchorGuardianModel *guardModel;

@property (strong, nonatomic) NSMutableArray *topUserArray;
@property (strong, nonatomic) NSArray *goodsArray;

@property (assign, nonatomic) int explainingGoodsid;

@property (strong, nonatomic) NSMutableArray *shareChannelArray1;
@property (strong, nonatomic) NSMutableArray *shareChannelArray2;

@property (strong, nonatomic) SuperPlayerView         *superPlayer;
@property (strong, nonatomic) SuperPlayerView         *superPlayer2;

@property (assign, nonatomic) int pkStatus; //0-未pk 1-pk中 2-惩罚中

@end

@implementation PortraitLiveViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [IQKeyboardManager sharedManager].enable = NO;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    // 禁用锁屏
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.scrollView scrollToRightAnimated:NO];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initLiveManager];
    
    [self addNotifationListen];
    [self createKeyboardToolBar];
    
    self.msgTableView.delegate = self;
    self.msgTableView.dataSource = self;
    
    [RoomChatTableViewCell registerWithTableView:self.msgTableView];
    RoomChatModel *sysChatModel = [[RoomChatModel alloc]init];
    sysChatModel.chatType = UIButtonTypeSystem;
    sysChatModel.message = configManager.appConfig.room_notice;
    _chatArray = [NSMutableArray arrayWithObject:sysChatModel];
    
    UITapGestureRecognizer *iconTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userIconTap)];
    [self.anchorIconView addGestureRecognizer:iconTap];
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    flow.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flow.itemSize = CGSizeMake(32, 33.5);
    flow.minimumLineSpacing = 3;
    self.rankUserCollectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:flow];
    self.rankUserCollectionView.backgroundColor = [UIColor clearColor];
    self.rankUserCollectionView.alwaysBounceHorizontal = YES;
    self.rankUserCollectionView.showsHorizontalScrollIndicator = NO;
    self.rankUserCollectionView.delegate = self;
    self.rankUserCollectionView.dataSource = self;
    [self.rankUserCollectionView registerNib:[UINib nibWithNibName:@"ContributeRankCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"ContributeRankCollectionViewCell"];
    [self.rankContainerView addSubview:self.rankUserCollectionView];
    [self.rankUserCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    _normalGiftBackView = [[NormalGiftBackView alloc]initWithFrame:CGRectMake(0, kStatusBarHeight + 200, 300, 140)];
    [self.view insertSubview:_normalGiftBackView belowSubview:self.controlView];
    
    _shadowView = [[UIView alloc]initWithFrame:self.view.bounds];
    _shadowView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.01];
    _shadowView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shadowTap)];
    [_shadowView addGestureRecognizer:tap];
    _shadowView.hidden = YES;
    [self.view addSubview:_shadowView];
    
    UITapGestureRecognizer *explaningGoodsTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(explaningGoodsTap)];
    [self.explainingGoodsView addGestureRecognizer:explaningGoodsTap];
    
    [[[SVGAParser alloc]init] parseWithNamed:@"animation_pk_start" inBundle:nil completionBlock:^(SVGAVideoEntity * _Nonnull videoItem) {
        self.svgaPkPlayer = [[SVGAPlayer alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth - 40, kScreenWidth - 40)];
        self.svgaPkPlayer.centerY = self.view.centerY - 30;
        self.svgaPkPlayer.centerX = self.view.centerX;
        self.svgaPkPlayer.videoItem = videoItem;
    } failureBlock:^(NSError * _Nonnull error) {
        NSLog(@"1");
    }];
    
    [[[SVGAParser alloc]init] parseWithNamed:@"animation_pk_win" inBundle:nil completionBlock:^(SVGAVideoEntity * _Nonnull videoItem) {
        self.svgaPkWinPlayer = [[SVGAPlayer alloc] initWithFrame:CGRectMake(0, 0, 89, 89)];
        self.svgaPkWinPlayer.videoItem = videoItem;
    } failureBlock:^(NSError * _Nonnull error) {
        NSLog(@"1");
    }];
    
    [self loadData];
    [self fillViewAnchorData];
}

- (void)viewDidLayoutSubviews{
    CGPoint chatTablePoint = [self.msgTableView convertPoint:self.msgTableView.bounds.origin toView:self.view];
    _normalGiftBackView.mj_y = chatTablePoint.y - _normalGiftBackView.height;
}

- (void)timerGo{
    if (self.timerCount % 5 == 0 && !self->_joinGroupFlag) {
        [self joinGroup];
    }
    if (self.timerCount == 60) {
        self.timerCount = 0;
        [self refreshLiveData];
        if (self.liveModel.room_type == LiveRoomTypeFee) {
            //执行扣费
            [self doDeduct];
        }
    }
    self.timerCount ++;
    
    [self caculatePkTime];
}

- (void)loadData{
    [self getLiveInfo];
    [self getGoodsList];
    [self checkIsMgr];
    [self getGuardInfo];
    [self getGuardianCount];
}

- (void)fillViewAnchorData{
    [self.anchorIconView sd_setImageWithURL:[NSURL URLWithString:self.anchorModel.avatar] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    self.anchorNameLabel.text = self.anchorModel.nick_name;
    self.liveHotLabel.text = @"";
    self.attentBtn.selected = self.anchorModel.isattent;
}

- (void)fillViewLiveData{
    
    self.timerCount = 0;
    [self.timer setFireDate:[NSDate date]];
    
    self.liveHotLabel.text = [NSString stringWithFormat:@"热度：%@",[CommonManager formateCount:self.liveModel.hot]];
    
    // superPlayer 永远播放当前房间主播的画面
    [self setupPlayer:self.superPlayer videoUrl:self.liveModel.pull_url];
    [self resetPlayer];
    
    if (self.liveModel.room_type == LiveRoomTypeFee) {
        [self doDeduct];
    }
    
    [self getGoodsList];
    
    [self checkIsMgr];
}

- (void)resetPlayer{
    if (self.liveModel.pk_status == PkStatusInPk && self.liveModel.pkinfo && self.liveModel.pklive) {
        [self getPKAnchorBasicInfo];
        
        self.pkView.hidden = NO;
        self.pkHolderView.hidden = YES;
        
        self.bgHolderImgView.hidden = YES;
        [self refreshPkScoreWithAnimation:NO];
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self showPkStartAnimation];
        });
        
        BOOL isHome = self.liveModel.pkinfo.home_anchorid == self.anchorModel.userid;
        self.pkAnchorViewLeadingLC.constant = isHome ? kScreenWidth/2 + 5 : 5;
        
        self.superPlayer.fatherView = isHome ? self.pkPlayerView1 : self.pkPlayerView2;
        
        // superPlayer2 永远播放对方主播的画面
        [self setupPlayer:self.superPlayer2 videoUrl:self.liveModel.pklive.pull_url];
        self.superPlayer2.fatherView = !isHome ? self.pkPlayerView1 : self.pkPlayerView2;
    }else{
        self.pkView.hidden = YES;
        self.pkHolderView.hidden = YES;
        self.superPlayer.fatherView = self.playerContainerView;
        if (_superPlayer2) {
            [_superPlayer2 resetPlayer];
            [_superPlayer2 removeFromSuperview];
            _superPlayer2 = nil;
        }
    }
}

- (void)resetPkAnchorView{
    self.pkHolderView.hidden = NO;
    [self.pkAnchorIconView sd_setImageWithURL:[NSURL URLWithString:self.liveModel.pklive.anchor.avatar] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    self.pkAnchorNameLabel.text = self.liveModel.pklive.anchor.nick_name;
    self.pkAnchorAttentBtn.hidden = self.liveModel.pklive.anchor.isattent;
}

- (void)endPk{
    [self hidePkWinAnimation];
    self.liveModel.pklive = nil;
    self.liveModel.pkinfo = nil;
    self.liveModel.pk_status = PkStatusNO;
    [self resetPlayer];
}

- (void)initLiveManager{
    _liveManager = [[MLVBLiveManager alloc]init];
    [_liveManager loginWithUserID:@([userManager curUserInfo].userid).stringValue userSign:userManager.curUserInfo.txim_sign completion:^(int errCode, NSString * _Nonnull errMsg) {
        if (errCode != 0) {
            self->liveManagerRetryTimes ++;
            if (self->liveManagerRetryTimes < 5) {
                [self initLiveManager];
            }
        }
    } failure:^(NSError *error) {
        self->liveManagerRetryTimes ++;
        if (self->liveManagerRetryTimes < 5) {
            [self initLiveManager];
        }
    }];
}

- (void)getLiveInfo{
    [CommonManager POST:@"live/getAnchorLiveInfo" parameters:@{@"anchorid":@(self.anchorModel.userid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            NSDictionary *dataDict = responseObject[@"data"];
            self.liveModel = [LiveModel yy_modelWithDictionary:dataDict[@"live"]];
            self.topUserArray = [NSMutableArray arrayWithArray:[NSArray yy_modelArrayWithClass:[IntimacyModel class] json:dataDict[@"contribute"]]];
            [self.rankUserCollectionView reloadData];
            [self fillViewLiveData];
        }else{
            QNAlertView *alertView = [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"" contentText:@"本场直播已结束" singleButton:@"确定" buttonClick:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
            alertView.cancelBackTap = YES;
        }
    } failure:^(NSError *error) {
    }];
}

- (void)refreshLiveData{
    [CommonManager POST:@"live/getLiveBasicInfo" parameters:@{@"liveid":@(self.liveModel.liveid).stringValue} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            LiveModel *liveModel = [LiveModel yy_modelWithDictionary:responseObject[@"data"][@"live"]];
            self.liveModel.hot = liveModel.hot;
            self.liveHotLabel.text = [NSString stringWithFormat:@"热度：%@",[CommonManager formateCount:self.liveModel.hot]];
            self.anchorModel.isattent = responseObject[@"data"][@"isattent"];
            self.attentBtn.selected = self.anchorModel.isattent;
            self.topUserArray = [NSMutableArray arrayWithArray:[NSArray yy_modelArrayWithClass:[IntimacyModel class] json:responseObject[@"data"][@"contribute"]]];
            [self.rankUserCollectionView reloadData];
        }
    } failure:^(NSError *error) {
    }];
}

- (void)getPKAnchorBasicInfo{
    [CommonManager POST:@"anchor/getAnchorBasicInfo" parameters:@{@"anchorid":@(self.liveModel.pklive.anchorid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.liveModel.pklive.anchor = [UserInfoModel yy_modelWithDictionary:responseObject[@"data"]];
            [self resetPkAnchorView];
        }
    } failure:^(NSError *error) {
    }];
}

- (void)getGuardInfo{
    [CommonManager POST:@"anchor/getGuardInfo" parameters:@{@"anchorid":@(self.anchorModel.userid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.guardModel = [AnchorGuardianModel yy_modelWithDictionary:responseObject[@"data"]];
        }else{
            self.guardModel = [[AnchorGuardianModel alloc]init];
            self.guardModel.anchorid = (int)self.anchorModel.userid;
            self.guardModel.uid = (int)userManager.curUserInfo.userid;
        }
    } failure:^(NSError *error) {
        self.guardModel = [[AnchorGuardianModel alloc]init];
        self.guardModel.anchorid = (int)self.anchorModel.userid;
        self.guardModel.uid = (int)userManager.curUserInfo.userid;
    }];
}

- (void)getGuardianCount{
    [CommonManager POST:@"anchor/getGuardianCount" parameters:@{@"anchorid":@(self.anchorModel.userid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.guardCountLabel.text = [NSString stringWithFormat:@"守护 %@人",responseObject[@"data"][@"count"]];
        }else{
        }
    } failure:^(NSError *error) {
    }];
}

- (void)getGoodsList{
    [CommonManager POST:@"live/getGoodsList" parameters:@{@"anchorid":@(self.anchorModel.userid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            NSDictionary *dataDict = responseObject[@"data"];
            self.goodsArray = [NSArray yy_modelArrayWithClass:[ShopGoodsModel class] json:dataDict];
            if (self.goodsArray.count > 0) {
                self.showGoodsBtn.hidden = NO;
                ShopGoodsModel *model = self.goodsArray[0];
                if (model.live_explaining) {
                    self.explainingGoodsid = model.goodsid;
                    self.explainingGoodsView.hidden = NO;
                    [self.explainingGoodsImgView sd_setImageWithURL:[NSURL URLWithString:model.thumb_url]];
                    self.explainingGoodsPriceLabel.text = model.price;
                }else{
                    self.explainingGoodsView.hidden = YES;
                }
            }else{
                self.showGoodsBtn.hidden = YES;
                self.explainingGoodsView.hidden = YES;
            }
        }
    } failure:^(NSError *error) {
    }];
}

- (void)checkIsMgr{
    if (![userManager isLogined]) {
        return;
    }
    [CommonManager POST:@"live/checkIsMgr" parameters:@{@"anchorid":@(self.anchorModel.userid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self->_isMgr = YES;
            self.bannedUserListBtn.hidden = NO;
        }
    } failure:^(NSError *error) {
    }];
}

- (void)joinGroup{
    [[V2TIMManager sharedInstance] joinGroup:self.liveModel.chatroomid msg:@"" succ:^(){
        [self sendMessage:KEnterRoomMessage];
        self->_joinGroupFlag = YES;
    }fail:^(int code, NSString * err) {
        NSLog(@"code=%d, err=%@", code, err);
        if (code == 10013) {
            //已经是群成员了
            self->_joinGroupFlag = YES;
        }
    }];
}

- (void)addNotifationListen{
    //IM通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(imNotification:) name:KNotificationInRoomIM object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(giftAnimationNotification:) name:KNotificationGiftAnimation object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(imGroupEventNotification:) name:KNotificationGroupEvent object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showUserCardNotification:) name:KNotificationShowUserCard object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(notificationLogout:) name:KNotificationLoginOut object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillDisappear:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textFieldTextChange) name:UITextFieldTextDidChangeNotification object:nil];
}

- (void)createKeyboardToolBar{
    //输入框
    _keyField = [[UITextField alloc]initWithFrame:CGRectMake(10,7,KScreenWidth- 20, 30)];
    _keyField.returnKeyType = UIReturnKeySend;
    _keyField.delegate = self;
    _keyField.textColor = [UIColor blackColor];
    _keyField.borderStyle = UITextBorderStyleNone;
    _keyField.placeholder = @"";
    _keyField.backgroundColor = [UIColor whiteColor];
    _keyField.layer.cornerRadius = 15.0;
    _keyField.layer.masksToBounds = YES;
    UIView *fieldLeft = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 15, 30)];
    fieldLeft.backgroundColor = [UIColor whiteColor];
    _keyField.leftView = fieldLeft;
    _keyField.leftViewMode = UITextFieldViewModeAlways;
    _keyField.font = [UIFont systemFontOfSize:15];
    
    //tool绑定键盘
    _keyboardToolBar = [[UIView alloc]initWithFrame:CGRectMake(0,KScreenHeight, KScreenWidth, 44)];
    _keyboardToolBar.backgroundColor = [UIColor clearColor];
    UIView *tooBgv = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 44)];
    tooBgv.backgroundColor = [UIColor whiteColor];
    tooBgv.alpha = 0.7;
    [_keyboardToolBar addSubview:tooBgv];
    
    [_keyboardToolBar addSubview:_msgSendBtn];
    [_keyboardToolBar addSubview:_keyField];
    
    [self.view addSubview:_keyboardToolBar];
}

#pragma mark ————————————————————————————————————— Action —————————————————————————————————————
- (IBAction)sendGiftBtnClick:(UIButton *)sender {
    if (!userManager.isLogined) {
        [kAppDelegate showLoginViewController];
        return;
    }
    if (!_chooseGiftView) {
        _chooseGiftView = [GiftChooseView showInView:self.view];
        _chooseGiftView.delegate = self;
    }else{
        [_chooseGiftView show];
    }
}

- (IBAction)msgBtnClick:(UIButton *)sender {
    if (!_messageVc) {
        _messageVc = [[MessagePopListViewController alloc]init];
        [self addChildViewController:_messageVc];
        [self.view addSubview:_messageVc.view];
        _messageVc.view.frame = CGRectMake(0, KScreenHeight, KScreenWidth, 550);
    }
    [UIView animateWithDuration:0.2 animations:^{
        self->_shadowView.hidden = NO;
        self->_shadowView.alpha = 1;
        self->_messageVc.view.mj_y = KScreenHeight - 550;
    }];
}

- (IBAction)guardBtnClick:(id)sender {
    if (!_guardVc) {
        _guardVc = [[GuardianViewController alloc]init];
        _guardVc.anchorid = self.anchorModel.userid;
        _guardVc.guardModel = self.guardModel;
        [self addChildViewController:_guardVc];
        [self.view addSubview:_guardVc.view];
        _guardVc.view.frame = CGRectMake(0, KScreenHeight, KScreenWidth, kScreenHeight - 200);
    }
    [_guardVc refreshData];
    [UIView animateWithDuration:0.2 animations:^{
        self->_shadowView.hidden = NO;
        self->_shadowView.alpha = 1;
        self->_guardVc.view.mj_y = 200;
    }];
}


- (void)shadowTap{
    [self.view endEditing:YES];
    [UIView animateWithDuration:0.2 animations:^{
        self->_shadowView.alpha = 0;
        self->_contributeVC.view.mj_y = KScreenHeight;
        self->_messageVc.view.mj_y = KScreenHeight;
        self->_goodsVc.view.mj_y = KScreenHeight;
        self->_bannedUserListVc.view.mj_y = KScreenHeight;
        self->_guardVc.view.mj_y = KScreenHeight;
    } completion:^(BOOL finished) {
        self->_shadowView.hidden = YES;
    }];
}


- (IBAction)sendMsgBtnClick:(UIButton *)sender {
    if (!userManager.isLogined) {
        [kAppDelegate showLoginViewController];
        return;
    }
    self->_shadowView.hidden = NO;
    self->_shadowView.alpha = 1;
    [_keyField becomeFirstResponder];
}

- (IBAction)attentBtnClick:(UIButton *)sender {
    if (!userManager.isLogined) {
        [kAppDelegate showLoginViewController];
        return;
    }
    [CommonManager POST:@"Anchor/attentAnchor" parameters:@{@"anchorid":@(self.anchorModel.userid),@"type":self.attentBtn.selected?@"0":@"1"} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.attentBtn.selected = !self.attentBtn.selected;
            self.liveModel.anchor.isattent = self.attentBtn.selected;
            self.liveModel.anchor.fans_count = [responseObject[@"data"][@"fans_count"] intValue];
            if (self.attentBtn.selected) {
                [self sendMessage:@"关注了主播"];
            }
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (IBAction)rankBtnClick:(UIButton *)sender {
    if (!_contributeVC) {
        _contributeVC = [[ContributeRankViewController alloc]init];
        _contributeVC.liveid = self.liveModel.liveid;
        [self addChildViewController:_contributeVC];
        [self.view addSubview:_contributeVC.view];
        _contributeVC.view.frame = CGRectMake(0, KScreenHeight, KScreenWidth, 350);
    }
    [UIView animateWithDuration:0.2 animations:^{
        self->_shadowView.hidden = NO;
        self->_shadowView.alpha = 1;
        self->_contributeVC.view.mj_y = KScreenHeight - 350;
    }];
    
    [self->_contributeVC reqData];
}

- (IBAction)bannedUserListBtnClick:(UIButton *)sender {
    if (!_bannedUserListVc) {
        _bannedUserListVc = [[InRoomBannedUserListViewController alloc]init];
        _bannedUserListVc.anchorid = self.anchorModel.userid;
        [self addChildViewController:_bannedUserListVc];
        [self.view addSubview:_bannedUserListVc.view];
        _bannedUserListVc.view.frame = CGRectMake(0, KScreenHeight, KScreenWidth, 500);
    }
    [UIView animateWithDuration:0.2 animations:^{
        self->_shadowView.hidden = NO;
        self->_shadowView.alpha = 1;
        self->_bannedUserListVc.view.mj_y = KScreenHeight - 500;
    }];
    [_bannedUserListVc refreshData];
}

- (IBAction)shareBtnClick:(UIButton *)sender {
    SharePanelView *sharePannelView = [SharePanelView showPanelInView:self.view channel1:self.shareChannelArray1 channel2:self.shareChannelArray2];
    sharePannelView.delegate = self;
}

- (IBAction)linkBtnClick:(UIButton *)sender {
    if (!self.liveModel.link_on) {
        [MBProgressHUD showTipMessageInView:@"主播未开启连麦"];
        return;
    }
    if (self.liveModel.link_status) {
        [MBProgressHUD showTipMessageInView:@"主播正在连麦中"];
        return;
    }
    [commonManager showLoadingAnimateInWindow];
    [_liveManager requestLinkWithAnchor:self.anchorModel.userid success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            [MBProgressHUD showTipMessageInWindow:@"申请连麦中，请等待主播回应"];
            self->_linkPushUrl = responseObject[@"data"][@"push_url"];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (IBAction)linkStopBtnClick:(UIButton *)sender {
    [commonManager showLoadingAnimateInWindow];
    [_liveManager stopLinkWithUser:[userManager curUserInfo].userid anchor:self.anchorModel.userid success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            [self stopLink];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (IBAction)showGoodsBtnClick:(UIButton *)sender {
    if (!_goodsVc) {
        _goodsVc = [[LiveGoodsViewController alloc]init];
        _goodsVc.goodsList = self.goodsArray;
        _goodsVc.isAnchor = NO;
        _goodsVc.explainingID = self.explainingGoodsid;
        [self addChildViewController:_goodsVc];
        [self.view addSubview:_goodsVc.view];
        _goodsVc.view.frame = CGRectMake(0, KScreenHeight, KScreenWidth, 450);
    }
    [UIView animateWithDuration:0.2 animations:^{
        self->_shadowView.hidden = NO;
        self->_shadowView.alpha = 1;
        self->_goodsVc.view.mj_y = KScreenHeight - 450;
    }];
}

- (IBAction)closeExplainViewClick:(UIButton *)sender {
    [UIView animateWithDuration:0.3 animations:^{
        self.explainingGoodsView.alpha = 0;
    } completion:^(BOOL finished) {
        self.explainingGoodsImgView.hidden = YES;
        self.explainingGoodsImgView.alpha = 1;
    }];
}

- (IBAction)closeBtnClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)pkAnchorAttentBtnClick:(UIButton *)sender {
    if (!userManager.isLogined) {
        [kAppDelegate showLoginViewController];
        return;
    }
    [CommonManager POST:@"Anchor/attentAnchor" parameters:@{@"anchorid":@(self.liveModel.pklive.anchorid),@"type":@"1"} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            sender.hidden = YES;
            self.liveModel.pklive.anchor.isattent = YES;
            self.liveModel.pklive.anchor.fans_count = [responseObject[@"data"][@"fans_count"] intValue];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (IBAction)pkHomeAnchorClick:(UIButton *)sender {
    if (!self.liveModel.pkinfo) {
        return;
    }
    UserInfoModel *anchor = self.anchorModel.userid == self.liveModel.pkinfo.home_anchorid ? self.anchorModel:self.liveModel.pklive.anchor;
    [UserCardView showUserCardInView:self.view userModel:anchor delegate:self];
}

- (IBAction)pkAwayAnchorClick:(UIButton *)sender {
    if (!self.liveModel.pkinfo) {
        return;
    }
    UserInfoModel *anchor = self.anchorModel.userid == self.liveModel.pkinfo.away_anchorid ? self.anchorModel:self.liveModel.pklive.anchor;
    [UserCardView showUserCardInView:self.view userModel:anchor delegate:self];
}

#pragma mark ————————————————————————————————————— Method —————————————————————————————————————
- (void)sendMessage:(NSString *)message{
    NSDictionary *chatDict;
    if ([userManager isLogined]) {
        chatDict = @{@"sender":[[UserInfoModel copyUserModelFromModel:userManager.curUserInfo] yy_modelToJSONObject],
                     @"is_manager":_isMgr?@(1):@(0),
                     @"is_guardian":[userManager.curUserInfo checkGuardAnchor:(int)self.anchorModel.userid]?@(1):@(0),
                     @"message":message
        };
    }else{
        chatDict = @{@"sender":@{@"id":@(0),
                                 @"nick_name":[[NSUserDefaults standardUserDefaults]objectForKey:@"TempUid"],
                                 @"user_level":@(1),
        },
                     @"message":message
        };
    }
    NSDictionary *dict = @{@"Action":@"RoomMessage",@"Data":@{@"chat":chatDict}};
    [[V2TIMManager sharedInstance]sendGroupCustomMessage:[NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil] to:self.liveModel.chatroomid priority:V2TIM_PRIORITY_NORMAL succ:^{
        //发送成功 刷新tableview
        RoomChatModel *chatModel = [RoomChatModel yy_modelWithDictionary:chatDict];
        chatModel.textColor = @"#FFFFFF";
        chatModel.nameColor = @"8be8ff";
        IMNotificationModel *imModel = [[IMNotificationModel alloc]init];
        imModel.chat = chatModel;
        imModel.roomid = self.liveModel.chatroomid;
        [self reciveRoomMsg:imModel];
    } fail:^(int code, NSString *desc) {
        [MBProgressHUD showTipMessageInView:@"发送失败"];
    }];
}

- (void)explaningGoodsTap{
    if (self.explainingGoodsid > 0) {
        QNH5ViewController *vc = [[QNH5ViewController alloc]init];
        vc.rootHref = [NSString stringWithFormat:@"%@/h5/goods/%d",[configManager appConfig].site_domain,self.explainingGoodsid];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)updateAudienceCount{
    [[TIMGroupManager sharedInstance] getGroupInfo:@[self.liveModel.chatroomid] succ:^(NSArray *groupList) {
        if (groupList.count > 0) {
            TIMGroupInfo *info = groupList[0];
            self.audienceCountLabel.text = [NSString stringWithFormat:@"%d",info.memberNum];
        }
    } fail:^(int code, NSString *msg) {
        
    }];
}

- (void)displaySystemMessage:(NSString *)message{
    RoomChatModel *sysChatModel = [[RoomChatModel alloc]init];
    sysChatModel.chatType = RoomChatTypeSysMsg;
    sysChatModel.message = message;
    [_chatArray addObject:sysChatModel];
    [self.msgTableView reloadData];
    //滚动到最底部
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:_chatArray.count-1];
    [self.msgTableView scrollToRowAtIndexPath:indexpath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

//连麦请求被接受
- (void)linkRequestAccepted{
    self.localContainerView.hidden = NO;
    //启动本地预览画面
    if(!_pusher){
        TXLivePushConfig *_config = [[TXLivePushConfig alloc] init];
        _pusher = [[TXLivePush alloc] initWithConfig: _config];
        _pusher.delegate = self;
        _pusher.videoProcessDelegate = self;
        [_pusher setVideoQuality:VIDEO_QUALITY_LINKMIC_SUB_PUBLISHER adjustBitrate:YES adjustResolution:NO];
        
        if (configManager.appConfig.beauty_channel == 0) {
            //腾讯云自带美颜 --------- start ------------
            
            //默认美颜设置
            _beautyLevel = 8.f;
            _whiteLevel = 5.f;
            _ruddyLevel = 3.f;
            [[_pusher getBeautyManager] setBeautyStyle:TXBeautyStyleSmooth];
            [[_pusher getBeautyManager] setBeautyLevel:_beautyLevel];
            [[_pusher getBeautyManager] setWhitenessLevel:_whiteLevel];
            [[_pusher getBeautyManager] setRuddyLevel:_ruddyLevel];
            
            //腾讯云自带美颜 --------- end ------------
        }else{
            [TiSDK init:configManager.appConfig.tisdk_key CallBack:^(InitStatus initStatus) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"TiSDKInitStatusNotification" object:nil];
            }];
        }
    }
    
    //启动本地摄像头预览
    [_pusher startPreview:self.localPreviewView];
    [_pusher startPush:_linkPushUrl];
    
    //切换至主播ACC播流
    SuperPlayerModel *playerModel = [[SuperPlayerModel alloc] init];
    // 设置播放地址，直播、点播都可以
    playerModel.videoURL = self.liveModel.acc_pull_url;
    // 开始播放
    [_superPlayer playWithModel:playerModel];
}

- (void)stopLink{
    self.localContainerView.hidden = YES;
    [_pusher stopPreview];
    [_pusher stopPush];
    //切回普通播流
    SuperPlayerModel *playerModel = [[SuperPlayerModel alloc] init];
    // 设置播放地址，直播、点播都可以
    playerModel.videoURL = self.liveModel.pull_url;
    // 开始播放
    [_superPlayer playWithModel:playerModel];
}

- (void)doDeduct{
    if (![CommonManager checkAndLogin]) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    if (userManager.curUserInfo.gold < self.liveModel.price) {
        [_superPlayer resetPlayer];
        [_superPlayer removeFromSuperview];
        _superPlayer = nil;
        QNAlertView *alert = [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"余额不足" contentText:@"请充值后重新进入房间" singleButton:@"确定" buttonClick:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
        alert.cancelBackTap = YES;
    }else{
        [CommonManager POST:@"live/timeBilling" parameters:@{@"liveid":@(self.liveModel.liveid)} success:^(id responseObject) {
            if (RESP_SUCCESS(responseObject)) {
                userManager.curUserInfo.gold = [responseObject[@"data"][@"gold"] intValue];
                [userManager saveUserInfo];
                [self->_chooseGiftView refreshUserCoinNum];
                if (userManager.curUserInfo.gold < self.liveModel.price) {
                    [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"提醒" contentText:@"您的金币即将用尽" leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"立即充值" rightClick:^{
                        RechargeViewController *vc = [[RechargeViewController alloc]init];
                        [self.navigationController pushViewController:vc animated:YES];
                    }];
                }
            }else{
                [self->_superPlayer resetPlayer];
                [self->_superPlayer removeFromSuperview];
                self->_superPlayer = nil;
                QNAlertView *alert = [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"扣费失败" contentText:@"请重新进入房间" singleButton:@"确定" buttonClick:^{
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                alert.cancelBackTap = YES;
            }
        } failure:^(NSError *error) {
            [self->_superPlayer resetPlayer];
            [self->_superPlayer removeFromSuperview];
            self->_superPlayer = nil;
            QNAlertView *alert = [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"扣费失败" contentText:@"请重新进入房间" singleButton:@"确定" buttonClick:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
            alert.cancelBackTap = YES;
        }];
    }
    
}

- (void)userIconTap{
    [UserCardView showUserCardInView:self.view userModel:self.anchorModel delegate:self];
}

- (void)setupPlayer:(SuperPlayerView *)player videoUrl:(NSString *)videoUrl{
    SuperPlayerModel *playerModel = [[SuperPlayerModel alloc] init];
    // 设置播放地址，直播、点播都可以
    playerModel.videoURL = videoUrl;
    // 开始播放
    [player playWithModel:playerModel];
}

- (void)showPkStartAnimation{
    [self.view addSubview:self.svgaPkPlayer];
    [self.svgaPkPlayer startAnimation];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.svgaPkPlayer stopAnimation];
        [self.svgaPkPlayer removeFromSuperview];
    });
}

- (void)refreshPkScoreWithAnimation:(BOOL)animation{
    int home_score = self.liveModel.pkinfo.home_score;
    int away_score = self.liveModel.pkinfo.away_score;
    
    BOOL score1Changed = [self.scoreLabel1.text intValue] != home_score;
    BOOL score2Changed = [self.scoreLabel2.text intValue] != away_score;
    
    self.scoreLabel1.text = @(home_score).stringValue;
    self.scoreLabel2.text = @(away_score).stringValue;
    
    self.scoreView1.layer.cornerRadius = home_score > away_score ? 8.5 : 0;
    self.scoreView2.layer.cornerRadius = home_score < away_score ? 8.5 : 0;
    
    CGFloat totleWidth = kScreenWidth + 8.5*2;
    if (home_score + away_score == 0) {
        self.scoreView1WidthLC.constant = totleWidth / 2;
        self.scoreView2WidthLC.constant = totleWidth / 2;
    }else{
        CGFloat homeWidth = totleWidth * (home_score*1.0 / (home_score+away_score)) + 8.5;
        CGFloat awayWidth = totleWidth * (away_score*1.0 / (home_score+away_score)) + 8.5;
        if (homeWidth < 50) {
            homeWidth = 50;
            awayWidth = totleWidth - 50 + 8.5;
        }else if(awayWidth < 50){
            awayWidth = 50;
            homeWidth = totleWidth - 50 + 8.5;
        }
        self.scoreView1WidthLC.constant = homeWidth;
        self.scoreView2WidthLC.constant = awayWidth;
        [self.pkView bringSubviewToFront:home_score>away_score ? self.scoreView1 : self.scoreView2];
        if (animation) {
            [UIView animateWithDuration:0.2 animations:^{
                [self.scoreLabel1 layoutIfNeeded];
                [self.scoreLabel2 layoutIfNeeded];
            }];
            if (score1Changed) {
                CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
                //速度控制函数，控制动画运行的节奏
                animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                animation.duration = 0.2;       //执行时间
                animation.repeatCount = 1;      //执行次数
                animation.autoreverses = YES;    //完成动画后会回到执行动画之前的状态
                animation.fromValue = [NSNumber numberWithFloat:1.0];   //初始伸缩倍数
                animation.toValue = [NSNumber numberWithFloat:1.5];     //结束伸缩倍数
                [[self.scoreLabel1 layer]addAnimation:animation forKey:nil];
            }
            if (score2Changed) {
                CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
                //速度控制函数，控制动画运行的节奏
                animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                animation.duration = 0.2;       //执行时间
                animation.repeatCount = 1;      //执行次数
                animation.autoreverses = YES;    //完成动画后会回到执行动画之前的状态
                animation.fromValue = [NSNumber numberWithFloat:1.0];   //初始伸缩倍数
                animation.toValue = [NSNumber numberWithFloat:1.5];     //结束伸缩倍数
                [[self.scoreLabel2 layer]addAnimation:animation forKey:nil];
            }
        }
    }
}

//PK计时
- (void)caculatePkTime{
    if (self.liveModel.pk_status == PkStatusInPk && self.liveModel.pkinfo) {
        int timeDura = (int)[[NSDate date] timeIntervalSinceDate:[CommonManager dateWithString:self.liveModel.pkinfo.create_time]];
        if (timeDura >= 8 * 60) {
            //pk 已结束
            self.pkStatus = 0;
        }else if (timeDura >= 6 * 60) {
            //pk 惩罚中
            self.pkStatus = 2;
            int leftSecond = 8*60 - timeDura;
            NSString *minute = @(leftSecond / 60).stringValue;
            NSString *second = leftSecond % 60 >= 10 ? @(leftSecond % 60).stringValue:[NSString stringWithFormat:@"0%d",leftSecond % 60];
            self.pkTimerLabel.text = [NSString stringWithFormat:@"惩罚中 0%@:%@",minute,second];
        }else{
            self.pkStatus = 1;
            int leftSecond = 6*60 - timeDura;
            NSString *minute = @(leftSecond / 60).stringValue;
            NSString *second = leftSecond % 60 >= 10 ? @(leftSecond % 60).stringValue:[NSString stringWithFormat:@"0%d",leftSecond % 60];
            self.pkTimerLabel.text = [NSString stringWithFormat:@"倒计时 0%@:%@",minute,second];
        }
    }else{
        self.pkStatus = 0;
    }
}

- (void)setPkStatus:(int)pkStatus{
    if (_pkStatus == pkStatus) {
        return;
    }
    _pkStatus = pkStatus;
    
    int home_score = self.liveModel.pkinfo.home_score;
    int away_score = self.liveModel.pkinfo.away_score;
    
    self.pkResultImgView1.hidden = (_pkStatus != 2) || (home_score == away_score);
    self.pkResultImgView2.hidden = (_pkStatus != 2) || (home_score == away_score);
    self.pkResultImgView3.hidden = (_pkStatus != 2) || (home_score != away_score);
    
    if (_pkStatus == 2) {
        //进入惩罚状态 显示动画
        if (home_score != away_score) {
            self.pkResultImgView1.image = nil;
            self.pkResultImgView2.image = nil;
            [self showPkWinAnimationToView:home_score > away_score ? self.pkResultImgView1:self.pkResultImgView2];
            home_score > away_score ? (self.pkResultImgView2.image = IMAGE_NAMED(@"ic_pk_lose@3x.png")) : (self.pkResultImgView1.image = IMAGE_NAMED(@"ic_pk_lose@3x.png"));
        }
    }else if (_pkStatus == 1){
        
    }else{
        [self endPk];
    }
}

- (void)showPkWinAnimationToView:(UIView *)view{
    [view addSubview:self.svgaPkWinPlayer];
    [self.svgaPkWinPlayer startAnimation];
}

- (void)hidePkWinAnimation{
    [self.svgaPkWinPlayer stopAnimation];
    [self.svgaPkWinPlayer removeFromSuperview];
}

#pragma mark ————————————————————————————————————— SharePanelViewDelegate —————————————————————————————————————
- (void)shareWithChannel:(PYShareChannel)channel panel:(nonnull SharePanelView *)panelView{
    if (channel == PYShareChannelReport) {
        ReportCategoryViewController *vc = [[ReportCategoryViewController alloc]init];
        vc.relateid = self.liveModel.anchor.userid;
        vc.type = ReportTypeAnchor;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (channel == PYShareChannelLink){
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = [NSString stringWithFormat:@"%@%lld",[configManager appConfig].share_live_url,self.liveModel.anchor.userid];
        [MBProgressHUD showTipMessageInView:@"链接已复制到剪贴板"];
    }else{
        SSDKPlatformType type = [SharePanelView getSSDKPlatformTypeBy:channel];
        //统一创建分享参数
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:@"想和我的距离再近一点，那我的直播你必须到场，与你不期而遇"
                                         images:self.liveModel.thumb
                                            url:[NSURL URLWithString:[NSString stringWithFormat:@"%@%lld",[configManager appConfig].share_live_url,self.liveModel.anchor.userid]]
                                          title:[NSString stringWithFormat:@"我正在%@的直播间",self.liveModel.anchor.nick_name]
                                           type:SSDKContentTypeAuto];
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            switch (state) {
                case SSDKResponseStateSuccess:
                    [MBProgressHUD showTipMessageInView:@"分享成功"];
                    break;
                case SSDKResponseStateFail:
                {
                    [MBProgressHUD showTipMessageInView:@"分享失败"];
                    //失败
                    break;
                }
                case SSDKResponseStateCancel:
                    //取消
                    break;
                    
                default:
                    break;
            }
        }];
    }
}

#pragma mark ————————————————————————————————————— TableViewDelegate Datasource —————————————————————————————————————
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _chatArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RoomChatTableViewCell *cell = [RoomChatTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.chatModel = _chatArray[indexPath.section];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 5;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 5)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RoomChatModel *chatModel = _chatArray[indexPath.section];
    if (chatModel.sender.userid == 0) {
        return;
    }
    if (_isMgr) {
        [UserCardView showSettingUserCardInView:self.view userModel:chatModel.sender delegate:self];
    }else{
        [UserCardView showUserCardInView:self.view userModel:chatModel.sender delegate:self];
    }
}

#pragma mark —————————————————————————————————————————— CollectionView ————————————————————————————————————————
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.topUserArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ContributeRankCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ContributeRankCollectionViewCell" forIndexPath:indexPath];
    cell.model = self.topUserArray[indexPath.row];
    cell.rankIndex = indexPath.row + 1;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    IntimacyModel *model = self.topUserArray[indexPath.row];
    [UserCardView showUserCardInView:self.view userModel:model.user delegate:self];
}

#pragma mark —————————————————————————————————————————— NSNotification ————————————————————————————————————————
-(void)imGroupEventNotification:(NSNotification *)notification{
    IMNotificationModel *notificationModel = [notification userInfo][@"data"];
    if ([notificationModel.roomid isEqualToString:self.liveModel.chatroomid]) {
        switch (notificationModel.action) {
            case IMNotificationActionLiveGroupMemberJoinExit:{
                [self updateAudienceCount];
            }
                break;
            default:
                break;
        }
    }
}

- (void)imNotification:(NSNotification *)notification{
    IMNotificationModel *notificationModel = [notification userInfo][@"data"];
    if ([notificationModel.roomid isEqualToString:self.liveModel.chatroomid]) {
        switch (notificationModel.action) {
            case IMNotificationActionRoomMessage:
                [self reciveRoomMsg:notificationModel];
                break;
            case IMNotificationActionLiveFinished:{
                [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"提示" contentText:@"本场直播结束，谢谢观看" singleButton:@"确定" buttonClick:^{
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }
                break;
            case IMNotificationActionExplainingGoods:{
                if (notificationModel.goods) {
                    self.explainingGoodsid = notificationModel.goods.goodsid;
                    self.explainingGoodsView.hidden = NO;
                    [self.explainingGoodsImgView sd_setImageWithURL:[NSURL URLWithString:notificationModel.goods.thumb_url]];
                    self.explainingGoodsPriceLabel.text = notificationModel.goods.price;
                }else{
                    [UIView animateWithDuration:0.3 animations:^{
                        self.explainingGoodsView.alpha = 0;
                    } completion:^(BOOL finished) {
                        self.explainingGoodsView.hidden = YES;
                        self.explainingGoodsView.alpha = 1;
                    }];
                }
            }
                break;
            case IMNotificationActionRoomNotification:
                [self roomNotification:notificationModel];
                break;
            default:
                break;
        }
    }
}

- (void)roomNotification:(IMNotificationModel *)notificationModel{
    switch (notificationModel.notify.type) {
        case RoomNotifyTypeSetManager:{
            if (notificationModel.notify.user.userid == userManager.curUserInfo.userid) {
                self->_isMgr = YES;
                self.bannedUserListBtn.hidden = NO;
            }
            [self displaySystemMessage:[NSString stringWithFormat:@"%@ 被主播设置为房管",notificationModel.notify.user.nick_name]];
        }
            break;
        case RoomNotifyTypeCancelManager:{
            if (notificationModel.notify.user.userid == userManager.curUserInfo.userid) {
                self->_isMgr = NO;
                self.bannedUserListBtn.hidden = YES;
            }
            [self displaySystemMessage:[NSString stringWithFormat:@"%@ 房管权限已被取消",notificationModel.notify.user.nick_name]];
        }
            break;
        case RoomNotifyTypeGuardAnchor:{
            [self getGuardianCount];
            [self displaySystemMessage:[NSString stringWithFormat:@"%@ 守护了主播",notificationModel.notify.user.nick_name]];
        }
            break;
        case RoomNotifyTypeLinkOn:{
            self.liveModel.link_on = YES;
            [self displaySystemMessage:@"主播已开启连麦功能"];
        }
            break;
        case RoomNotifyTypeLinkOff:{
            self.liveModel.link_on = NO;
            [self displaySystemMessage:@"主播已关闭连麦功能"];
        }
            break;
        case RoomNotificationAcceptLinkRequest:{
            //连麦请求被接受 先判断是不是自己
            if (notificationModel.notify.touid != [userManager curUserInfo].userid) {
                return;
            }
            [self linkRequestAccepted];
        }
            break;
        case RoomNotificationRefuseLinkRequest:{
            //连麦请求被拒绝 先判断是不是自己
            if (notificationModel.notify.touid != [userManager curUserInfo].userid) {
                return;
            }
            [MBProgressHUD showTipMessageInWindow:@"主播拒绝了你的连麦请求"];
        }
            break;
        case RoomNotificationStopLink:{
            //连麦结束
            [self stopLink];
        }
            break;
        case RoomNotifyTypePkStart:{
            //开始pk
            self.liveModel.pk_status = PkStatusInPk;
            self.liveModel.pkinfo = notificationModel.notify.pkinfo;
            self.liveModel.pklive = notificationModel.notify.pklive;
            [self caculatePkTime];
            [self resetPlayer];
        }
            break;
        case RoomNotifyTypePkEnd:{
            //pk结束
            if (self.liveModel.pk_status == PkStatusNO) {
                return;
            }
            [self endPk];
        }
            break;
        case RoomNotifyTypePkScoreChange:{
            //PK比分变化
            self.liveModel.pkinfo.home_score = notificationModel.notify.pkinfo.home_score;
            self.liveModel.pkinfo.away_score = notificationModel.notify.pkinfo.away_score;
            [self refreshPkScoreWithAnimation:YES];
        }
            break;
        default:
            break;
    }
}

- (void)reciveRoomMsg:(IMNotificationModel *)notificationModel{
    if ([notificationModel.roomid isEqualToString: self.liveModel.chatroomid]) {
        RoomChatModel *chatModel = [notificationModel.chat yy_modelCopy];
        chatModel.textColor = @"#FFFFFF";
        chatModel.nameColor = @"8be8ff";
        [_chatArray addObject:chatModel];
        [self.msgTableView reloadData];
        //滚动到最底部
        NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:_chatArray.count-1];
        [self.msgTableView scrollToRowAtIndexPath:indexpath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        
        if (([chatModel.sender getVipLevel] > 0 || chatModel.is_guardian) && [chatModel.message isEqualToString:KEnterRoomMessage]) {
            //VIP入场提示
            if (!_vipEnterAnimationView) {
                CGPoint msgTablePoint = [self.msgTableView convertPoint:self.msgTableView.bounds.origin toView:self.view];
                _vipEnterAnimationView = [VipEnterAnimationView createVipEnterAnimationViewWithYPosition:msgTablePoint.y - 44 - 7];
                [self.view insertSubview:_vipEnterAnimationView belowSubview:self.controlView];
            }
            [_vipEnterAnimationView addModels:notificationModel];
            if(!_vipEnterAnimationView.animating){
                [_vipEnterAnimationView enAnimation];
            }
        }
    }
}

- (void)giftAnimationNotification:(NSNotification *)notification{
    IMNotificationModel *notificationModel = [notification userInfo][@"data"];
    if (notificationModel.gift.type == GiftTypeNormal) {
        if (!_continueGiftView) {
            _continueGiftView = [[ContinueGiftView alloc]initWithFrame:CGRectMake(0, 0, _normalGiftBackView.width, _normalGiftBackView.height)];
            [_normalGiftBackView addSubview:_continueGiftView];
            //初始化礼物空位
            [_continueGiftView initGift];
        }
        [_continueGiftView popGiftWithNotification:notificationModel];
    }else if (notificationModel.gift.type == GiftTypeSplendid){
        if (!_giftShowView) {
            _giftShowView = [[GiftShowView alloc]init];
            _giftShowView.delegate = self;
            [self.view addSubview:_giftShowView];
        }
        if (notificationModel.gift != nil) {
            [_giftShowView addModels:notificationModel];
        }
        if(!_giftShowView.animating){
            [_giftShowView enAnimationGift];
        }
    }
    [self displaySystemMessage:[NSString stringWithFormat:@"%@ 送给主播 %@x%d", notificationModel.gift.sender.nick_name, notificationModel.gift.title, notificationModel.gift.count]];
}

- (void)showUserCardNotification:(NSNotification *)notification{
    UserInfoModel *model = [notification userInfo][@"model"];
    [UserCardView showUserCardInView:self.view userModel:model delegate:self];
}

- (void)notificationLogout:(NSNotification *)notification{
    [self removeTimer];
}

#pragma mark ———————————————————————————— ChooseGiftViewDelegate ————————————————————————————

- (void)sendGift:(GiftModel *)gift count:(int)count{
    if (gift.use_type == GiftSepcialTypeGuard && (![self.guardModel checkGuard] || self.guardModel.type == 2)) {
        [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"提示" contentText:@"该礼物为月守护以上专属" leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"守护主播" rightClick:^{
            [self guardBtnClick:nil];
        }];
        return;
    }
    if (userManager.curUserInfo.gold < gift.price*count) {
        //弹出充值窗口
        [QNAlertView showAlertViewWithType:QNAlertTypeWarning title:@"" contentText:@"余额不足" leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"充值" rightClick:^{
            RechargeViewController *vc = [[RechargeViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }];
        return;
    }
    //预更新用户金币余额 防止多次点击
    userManager.curUserInfo.gold -= gift.price;
    [userManager saveUserInfo];
    [self->_chooseGiftView refreshUserCoinNum];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:@{@"anchorid":@(self.liveModel.anchorid),
                                                                                 @"liveid":@(self.liveModel.liveid),
                                                                                 @"giftid":@(gift.giftid),
                                                                                 @"count":@(count)
    }];
    if (self.liveModel.pkinfo && self.liveModel.pk_status == PkStatusInPk) {
        [param setObject:@(self.liveModel.pkinfo.pkid) forKey:@"pkid"];
        [param setObject:@(self.liveModel.pkinfo.home_anchorid == self.anchorModel.userid ? 1 : 2) forKey:@"pkto"]; //1主场 2-客场
    }
    [CommonManager POST:@"Gift/sendGift" parameters:param success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            userManager.curUserInfo.gold = [responseObject[@"data"][@"gold"] intValue];
            [userManager saveUserInfo];
            [self->_chooseGiftView refreshUserCoinNum];
            
            //显示动画
            IMNotificationModel *notificationModel = [[IMNotificationModel alloc]init];
            gift.sender = [userManager.curUserInfo yy_modelCopy];
            gift.count = count;
            notificationModel.gift = gift;
            [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationGiftAnimation object:nil userInfo:@{@"data":notificationModel}];
        }else{
            //恢复本地用户金币余额
            userManager.curUserInfo.gold += gift.price;
            [userManager saveUserInfo];
            [self->_chooseGiftView refreshUserCoinNum];
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        //恢复本地用户金币余额
        userManager.curUserInfo.gold += gift.price;
        [userManager saveUserInfo];
        [self->_chooseGiftView refreshUserCoinNum];
        RESP_FAILURE;
    }];
}

#pragma mark ————————————————————————————————————— UserCardViewDelegate —————————————————————————————————————
- (void)UserCardViewMsgBtnClick:(UserCardView *)view{
    TUIConversationCellData *data = [[TUIConversationCellData alloc]init];
    data.userID = [NSString stringWithFormat:@"%lld",view.userModel.userid];
    data.faceUrl = view.userModel.avatar;
    MessageChatViewController *vc = [[MessageChatViewController alloc]initWithConversationCellData:data senderName:view.userModel.nick_name];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)UserCardViewPageBtnClick:(UserCardView *)view{
    UserInfoViewController *vc = [[UserInfoViewController alloc]init];
    vc.anchorid = view.userModel.userid;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)UserCardViewOptBtnClick:(UserCardView *)view{
    if (view.optType == UserCardViewOptTypeMgr) {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"设置" message:[NSString stringWithFormat:@"用户：%@",view.userModel.nick_name] preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"禁言" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [CommonManager POST:@"live/banUser" parameters:@{@"anchorid":@(self.anchorModel.userid),@"userid":@(view.userModel.userid),@"type":@(1)} success:^(id responseObject) {
                if (RESP_SUCCESS(responseObject)) {
                    [MBProgressHUD showTipMessageInView:@"禁言成功"];
                }else{
                    RESP_SHOW_ERROR_MSG(responseObject);
                }
            } failure:^(NSError *error) {
                RESP_FAILURE;
            }];
        }];
        UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"设为管理员" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [CommonManager POST:@"live/setRoomMgr" parameters:@{@"mgrid":@(view.userModel.userid),@"type":@(1)} success:^(id responseObject) {
                if (RESP_SUCCESS(responseObject)) {
                    [MBProgressHUD showTipMessageInView:@"设置成功"];
                }else{
                    RESP_SHOW_ERROR_MSG(responseObject);
                }
            } failure:^(NSError *error) {
                RESP_FAILURE;
            }];
        }];
        UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        [alertVC addAction:action1];
        [alertVC addAction:action2];
        [alertVC addAction:action3];
        [self presentViewController:alertVC animated:YES completion:nil];
    }else{
        ReportCategoryViewController *vc = [[ReportCategoryViewController alloc]init];
        vc.relateid = self.liveModel.anchor.userid;
        vc.type = ReportTypeAnchor;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)UserCardViewVipChargeBtnClick:(UserCardView *)view{
    VipCenterViewController *vc = [[VipCenterViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark ————————————————————————————————————— TextFieldDelegate —————————————————————————————————————
- (void)textFieldTextChange{
    if (_keyField.text.length > 0) {
        _msgSendBtn.selected = YES;
    }else{
        _msgSendBtn.selected = NO;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == _keyField) {
        [_keyField resignFirstResponder];
        if (_keyField.text.length > 50) {
            [MBProgressHUD showTipMessageInWindow:@"字数最多50字"];
            return NO;
        }
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        NSString *trimedString = [_keyField.text stringByTrimmingCharactersInSet:set];
        if ([trimedString length] == 0) {
            return NO;
        }
        [_keyField resignFirstResponder];
        _keyField.text = nil;
        [self sendMessage:trimedString];
    }
    return YES;
}

- (void)keyboardWillAppear:(NSNotification *)notification{
    
    NSDictionary *info = [notification userInfo];
    
    //取出动画时长
    CGFloat animationDuration = [[info valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //取出键盘位置大小信息
    CGRect keyboardBounds = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    //rect转换
    //    CGRect keyboardRect = [self convertRect:keyboardBounds toView:nil];
    
    //记录Y轴变化
    CGFloat keyboardHeight = keyboardBounds.size.height;
    
    //上移动画options
    UIViewAnimationOptions options = (UIViewAnimationOptions)[[info valueForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue] << 16;
    
    [UIView animateWithDuration:animationDuration delay:0 options:options animations:^{
        self->_keyboardToolBar.transform = CGAffineTransformMakeTranslation(0, -keyboardHeight-self->_keyboardToolBar.height);
    } completion:nil];
}


- (void)keyboardWillDisappear:(NSNotification *)notification{
    NSDictionary *info = [notification userInfo];
    
    //取出动画时长
    CGFloat animationDuration = [[info valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //下移动画options
    UIViewAnimationOptions options = (UIViewAnimationOptions)[[info valueForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue] << 16;
    
    //回复动画
    [UIView animateWithDuration:animationDuration delay:0 options:options animations:^{
        self->_keyboardToolBar.transform = CGAffineTransformIdentity;
    } completion:nil];
    
}

#pragma mark ————————————————————————————————————— 懒加载 —————————————————————————————————————
- (NSMutableArray *)topUserArray{
    if (!_topUserArray) {
        _topUserArray = [NSMutableArray arrayWithCapacity:10];
    }
    return _topUserArray;
}

- (NSMutableArray *)shareChannelArray1{
    if (!_shareChannelArray1) {
        _shareChannelArray1 = [NSMutableArray arrayWithArray:@[@(PYShareChannelWechat),@(PYShareChannelPengyouquan),@(PYShareChannelQQ),@(PYShareChannelWibo),@(PYShareChannelQZone),@(PYShareChannelMore)]];
    }
    return _shareChannelArray1;
}

- (NSMutableArray *)shareChannelArray2{
    if (!_shareChannelArray2) {
        _shareChannelArray2 = [NSMutableArray arrayWithArray:@[@(PYShareChannelReport),@(PYShareChannelLink)]];
    }
    return _shareChannelArray2;
}

- (NSTimer *)timer{
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(timerGo) userInfo:nil repeats:YES];
    }
    return _timer;
}

- (SuperPlayerView *)superPlayer{
    if (!_superPlayer) {
        _superPlayer = [[SuperPlayerView alloc] init];
        // 设置代理，用于接受事件
        _superPlayer.delegate = self;
        _superPlayer.loop = YES;
        _superPlayer.isLockScreen = YES;
        _superPlayer.playerConfig.renderMode = 0;
        _superPlayer.layoutStyle = SuperPlayerLayoutStyleFullScreen;
        _superPlayer.controlView.hidden = YES;
    }
    return _superPlayer;
}

- (SuperPlayerView *)superPlayer2{
    if (!_superPlayer2) {
        _superPlayer2 = [[SuperPlayerView alloc] init];
        // 设置代理，用于接受事件
        _superPlayer2.delegate = self;
        _superPlayer2.loop = YES;
        _superPlayer2.isLockScreen = YES;
        _superPlayer2.playerConfig.renderMode = 0;
        _superPlayer2.layoutStyle = SuperPlayerLayoutStyleFullScreen;
        _superPlayer2.controlView.hidden = YES;
    }
    return _superPlayer2;
}

- (void)removeTimer{
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)didMoveToParentViewController:(UIViewController *)parent{
    if (!parent) {
        [self removeTimer];
    }
}

#pragma mark —————————————————————————— SuperPlayerViewDelegate —————————————————————
- (void)superPlayerDidStart:(SuperPlayerView *)player{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.bgHolderImgView.hidden = YES;
    });
}

#pragma mark —————————————————————————— TXLivePushListener —————————————————————
- (void)onNetStatus:(NSDictionary *)param{
    
}

- (void)onPushEvent:(int)EvtID withParam:(NSDictionary *)param{
    NSLog(@"%d",EvtID);
}

#pragma mark ———————————————————————— TXVideoCustomProcessDelegate ——————————————————————

- (GLuint)onPreProcessTexture:(GLuint)texture width:(CGFloat)width height:(CGFloat)height{
    if(configManager.appConfig.beauty_channel == 0)
        return texture;
    return [[TiSDKManager shareManager] renderTexture2D:texture Width:width Height:height Rotation:CLOCKWISE_0 Mirror:_pusher.frontCamera];
}

/**
 * 在OpenGL线程中回调，可以在这里释放创建的OpenGL资源
 */
- (void)onTextureDestoryed{
    NSLog(@"onTextureDestoryed");
    if(configManager.appConfig.beauty_channel == 1){
        dispatch_async(dispatch_get_main_queue(), ^{
            [[TiSDKManager shareManager] destroy];
        });
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    if (_pusher) {
        [_pusher stopPreview];
        [_pusher stopPush];
    }
    if (_superPlayer) {
        [_superPlayer resetPlayer];
        [_superPlayer removeFromSuperview];
        _superPlayer = nil;
    }
    if (_superPlayer2) {
        [_superPlayer2 resetPlayer];
        [_superPlayer2 removeFromSuperview];
        _superPlayer2 = nil;
    }
    //退出聊天室
    [[TIMGroupManager sharedInstance] quitGroup:self.liveModel.chatroomid succ:^() {
        NSLog(@"succ");
    } fail:^(int code, NSString* err) {
        NSLog(@"failed code: %d %@", code, err);
    }];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
