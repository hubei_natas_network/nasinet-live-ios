//
//  GuardianViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/12/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "GuardianViewController.h"

#import "OpenGuardianViewController.h"
#import "GuardianListViewController.h"

#import "JXCategoryTitleView.h"
#import "JXCategoryView.h"


@interface GuardianViewController ()<JXCategoryViewDelegate,JXCategoryListContainerViewDelegate>{
    
}

@property (nonatomic, strong) NSArray <NSString *>          *titles;

@property (nonatomic, strong) JXCategoryTitleView           *categoryView;
@property (strong, nonatomic) JXCategoryListContainerView   *listContainerView;

@property (weak, nonatomic) OpenGuardianViewController      *openGuardVc;
@property (weak, nonatomic) GuardianListViewController      *guardListVc;

@end

@implementation GuardianViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    UIImageView *backView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height + 5)];
    backView.image = [UIImage imageNamed:@"bg_guardian"];
    backView.layer.cornerRadius = 5.f;
    [self.view addSubview:backView];
    
    _titles = self.isAnchor ? @[@"守护成员"] : @[@"守护成员", @"开通守护"];
    
    _categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, 7, [UIScreen mainScreen].bounds.size.width, 50)];
    self.categoryView.titles = self.titles;
    self.categoryView.backgroundColor = [UIColor clearColor];
    self.categoryView.delegate = self;
    self.categoryView.titleFont = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
    self.categoryView.titleSelectedColor = [UIColor whiteColor];
    self.categoryView.titleColor = [UIColor colorWithHexString:@"#E89EFF"];
    self.categoryView.titleColorGradientEnabled = YES;
    self.categoryView.titleLabelZoomEnabled = NO;
    self.categoryView.contentScrollViewClickTransitionAnimationEnabled = NO;
    
    self.listContainerView = [[JXCategoryListContainerView alloc] initWithType:JXCategoryListContainerType_ScrollView delegate:self];
    self.listContainerView.frame = CGRectMake(0, self.categoryView.mj_maxY, KScreenWidth, kScreenHeight - 200 - 57);
    self.listContainerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.listContainerView];
    
    self.categoryView.listContainer = self.listContainerView;
    [self.view addSubview:self.categoryView];
}

- (void)refreshData{
    [self.guardListVc refreshData];
    [self.openGuardVc refreshData];
}

#pragma mark - JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didScrollSelectedItemAtIndex:(NSInteger)index {
    //侧滑手势处理
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
}

#pragma mark - JXCategoryListContainerViewDelegate

- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index{
    if (index == 0) {
        GuardianListViewController *vc = [[GuardianListViewController alloc]init];
        vc.anchorid = self.anchorid;
        self.guardListVc = vc;
        return vc;
    }else{
        OpenGuardianViewController *vc = [[OpenGuardianViewController alloc]init];
        vc.anchorid = self.anchorid;
        vc.guardModel = self.guardModel;
        self.openGuardVc = vc;
        return vc;
    }
}

- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count;
}

@end
