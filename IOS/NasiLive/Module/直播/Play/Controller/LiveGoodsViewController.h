//
//  LiveGoodsViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/8/25.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"


NS_ASSUME_NONNULL_BEGIN

@interface LiveGoodsViewController : RootViewController

@property (strong, nonatomic) NSArray *goodsList;
@property (assign, nonatomic) BOOL isAnchor;
@property (assign, nonatomic) int explainingID;
@property (copy, nonatomic) NSString *chatroomid;

@end

NS_ASSUME_NONNULL_END
