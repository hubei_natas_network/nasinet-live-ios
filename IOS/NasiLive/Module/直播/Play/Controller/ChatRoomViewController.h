//
//  ChatRoomViewController.h
//  NasiLive
//
//  Created by yun on 2020/2/27.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

@class RoomChatModel;

NS_ASSUME_NONNULL_BEGIN

@interface ChatRoomViewController : RootViewController

@property (strong, nonatomic) LiveModel *liveModel;

- (void)addChatModel:(RoomChatModel *)chatModel;

@end

NS_ASSUME_NONNULL_END
