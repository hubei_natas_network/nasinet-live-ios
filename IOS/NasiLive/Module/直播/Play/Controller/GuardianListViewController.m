//
//  GuardianListViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/12/4.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "GuardianListViewController.h"

#import "GuardianTableViewCell.h"
#import "AnchorGuardianModel.h"

#define pagesize 15

@interface GuardianListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) NSMutableArray *datasource;

@end

@implementation GuardianListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
   
    self.tableView = [self createTableViewWithFrame:CGRectMake(15, 0, kScreenWidth - 30, kScreenHeight - 300)];
    [self.view addSubview:self.tableView];
    self.tableView.layer.cornerRadius = 10;
    self.tableView.layer.masksToBounds = YES;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [GuardianTableViewCell registerWithTableView:self.tableView];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.tableView.height = self.view.height;
}

- (void)refreshData{
    [self reqDataAtPage:1];
}

- (void)reqDataAtPage:(int)page{
    NSDictionary *params = @{
        @"anchorid":@(self.anchorid),
        @"page":@(page),
        @"size":@(pagesize)
    };
    
    [CommonManager POST:@"Anchor/getGuardianList" parameters:params success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            if (page == 1) {
                [self.datasource removeAllObjects];
            }
            NSArray *models = [NSArray yy_modelArrayWithClass:[AnchorGuardianModel class] json:responseObject[@"data"]];
            [self.datasource addObjectsFromArray:models];
            [self.tableView reloadData];
            if (models.count == pagesize) {
                [self setupTableViewMJFooter];
            }else{
                [self removeTableMJFooter];
            }
            if (self.datasource.count == 0) {
                [self showNoDataImage];
            }else{
                [self removeNoDataImage];
            }
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}



#pragma mark ----------------------  tableviewdelegate + datasource ----------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.datasource.count == 0) {
        return 0;
    }
    if (self.datasource.count > 3) {
        return 2;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return self.datasource.count >=3 ? 3 : self.datasource.count;
    }else{
        return self.datasource.count - 3;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GuardianTableViewCell *cell = [GuardianTableViewCell cellWithTableView:tableView indexPath:indexPath];
    if (indexPath.section == 0) {
        cell.model = self.datasource[indexPath.row];
        cell.index = (int)indexPath.row;
    }else{
        cell.model = self.datasource[indexPath.row + 3];
        cell.index = (int)indexPath.row + 3;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    AnchorGuardianModel *model = indexPath.section == 0 ? self.datasource[indexPath.row] : self.datasource[indexPath.row + 3];
    [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationShowUserCard object:nil userInfo:@{@"model":model.user}];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.width, 35)];
    headerView.backgroundColor = [UIColor clearColor];
    headerView.layer.masksToBounds = YES;
    UIImageView *backImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, headerView.width, headerView.height + 10)];
    backImgView.backgroundColor = [UIColor whiteColor];
    backImgView.layer.cornerRadius = 10;
    backImgView.layer.masksToBounds = YES;
    [headerView addSubview:backImgView];
    UIImageView *iconImgView = [[UIImageView alloc]initWithFrame:CGRectMake(11, 13, 78, 13)];
    iconImgView.image = section == 0 ? [UIImage imageNamed:@"ic_guardlist_header1"]:[UIImage imageNamed:@"ic_guardlist_header2"];
    [headerView addSubview:iconImgView];
    
   
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 35;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.width, 17)];
    footerView.backgroundColor = [UIColor clearColor];
    footerView.layer.masksToBounds = YES;
    UIImageView *backImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, -10, footerView.width, footerView.height + 10 - 7)];
    backImgView.backgroundColor = [UIColor whiteColor];
    backImgView.layer.cornerRadius = 10;
    backImgView.layer.masksToBounds = YES;
    [footerView addSubview:backImgView];
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return section == 0  ? 17 : 0;
}

- (NSMutableArray *)datasource{
    if (!_datasource) {
        _datasource = [NSMutableArray array];
    }
    return _datasource;
}


- (UIView *)listView{
    return self.view;
}

@end
