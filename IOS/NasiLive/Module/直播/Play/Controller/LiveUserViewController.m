//
//  LiveUserViewController.m
//  NasiLive
//
//  Created by yun on 2020/3/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "LiveUserViewController.h"
#import "MomentInfoViewController.h"
#import "UserInfoViewController.h"

#import "MomentModel.h"
#import "MomentTextCell.h"
#import "MomentVideoCell.h"
#import "MomentSingleImageCell.h"
#import "MomentImagesCell.h"
#import "LiveAnchorTableViewCell.h"

#import <YBImageBrowser.h>

#define pagesize 20

@interface LiveUserViewController ()<UITableViewDelegate,UITableViewDataSource,MomentBaseTableViewCellDelegate>{
    NSMutableArray  *datasource;
}

@end

@implementation LiveUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    datasource = [NSMutableArray array];
    
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    [self removeTableMJFooter];
    
    [LiveAnchorTableViewCell registerWithTableView:self.tableView];
    
    [MomentImagesCell registerWithTableView:self.tableView];
    [MomentSingleImageCell registerWithTableView:self.tableView];
    [MomentVideoCell registerWithTableView:self.tableView];
    [MomentTextCell registerWithTableView:self.tableView];
    
    [self reqDataAtPage:1];
    
}

- (void)headerRereshing{
    [self reqDataAtPage:1];
}

- (void)footerRereshing{
    int page = ceil((datasource.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    [self reqDataAtPage:page];
}

- (void)reqDataAtPage:(int)page{
    NSDictionary *params = @{@"page":@(page),
                             @"size":@(pagesize),
                             @"authorid":@(self.liveModel.anchorid)
    };
    
    [CommonManager POST:@"Moment/getListByUser" parameters:params success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            if (page == 1) {
                [self->datasource removeAllObjects];
            }
            NSArray *models = [NSArray yy_modelArrayWithClass:[MomentModel class] json:responseObject[@"data"]];
            [self->datasource addObjectsFromArray:models];
            [self.tableView reloadData];
            
            if (models.count < pagesize) {
                [self.tableView.mj_footer endRefreshingWithNoMoreData];
            }else{
                [self setupTableViewMJFooter];
            }
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        RESP_FAILURE;
    }];
}

#pragma mark ----------------------  tableviewdelegate + datasource ----------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }
    return datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        LiveAnchorTableViewCell *cell  = [LiveAnchorTableViewCell cellWithTableView:tableView indexPath:indexPath];
        cell.model = self.liveModel.anchor;
        return cell;
    }else{
        MomentModel *model = datasource[indexPath.row];
        if (model.type == MomentTypeImages) {
            if (model.image_urls.count == 1) {
                MomentSingleImageCell *cell = [MomentSingleImageCell cellWithTableView:tableView indexPath:indexPath];
                cell.delegate = self;
                cell.model = model;
                return cell;
            }else{
                MomentImagesCell *cell = [MomentImagesCell cellWithTableView:tableView indexPath:indexPath];
                cell.delegate = self;
                cell.model = model;
                return cell;
            }
        }else if (model.type == MomentTypeText){
            MomentTextCell *cell = [MomentTextCell cellWithTableView:tableView indexPath:indexPath];
            
            cell.delegate = self;
            cell.model = model;
            return cell;
        }else if (model.type == MomentTypeVideo){
            MomentVideoCell *cell = [MomentVideoCell cellWithTableView:tableView indexPath:indexPath];
            
            cell.delegate = self;
            cell.model = model;
            return cell;
        }
    }
    return [[UITableViewCell alloc]init];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        MomentModel *model = datasource[indexPath.row];
        MomentInfoViewController *vc = [[MomentInfoViewController alloc]init];
        vc.momentModel = model;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark —————————— MomentBaseTableViewCellDelegate ———————————————
- (void)playBtnClick:(MomentBaseTableViewCell *)cell{
    MomentInfoViewController *vc = [[MomentInfoViewController alloc]init];
    vc.momentModel = cell.model;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)unlockClick:(MomentModel *)model{
    MomentInfoViewController *vc = [[MomentInfoViewController alloc]init];
    vc.momentModel = model;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)likeClick:(MomentModel *)model{
    if (model.liked) {
        return;
    }
    [CommonManager POST:@"Moment/likeMoment" parameters:@{@"momentid":@(model.momentid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            model.liked = YES;
            model.like_count = [responseObject[@"data"][@"like_count"] intValue];
            [self.tableView reloadData];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (void)commentClick:(MomentModel *)model{
    MomentInfoViewController *vc = [[MomentInfoViewController alloc]init];
    vc.momentModel = model;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)moreFuncClick:(MomentModel *)model{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"选项" preferredStyle:UIAlertControllerStyleActionSheet];
    NSString *collectStr = model.collected?@"取消收藏":@"收藏";
    UIAlertAction *collectAction = [UIAlertAction actionWithTitle:collectStr style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [commonManager showLoadingAnimateInWindow];
        [CommonManager POST:@"Moment/collectMoment" parameters:@{@"momentid":@(model.momentid), @"type":@(!model.collected)} success:^(id responseObject) {
            [commonManager hideAnimateHud];
            if (RESP_SUCCESS(responseObject)) {
                [commonManager showSuccessAnimateInWindow];
                model.collected = !model.collected;
            }else{
                RESP_SHOW_ERROR_MSG(responseObject);
            }
        } failure:^(NSError *error) {
            [commonManager hideAnimateHud];
            RESP_FAILURE;
        }];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:collectAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)imgViewClick:imageView model:(MomentModel *)model index:(NSInteger)index{
    NSMutableArray *datas = [NSMutableArray array];
    [model.image_urls enumerateObjectsUsingBlock:^(NSString *_Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        // 网络图片
        YBIBImageData *data = [YBIBImageData new];
        data.imageURL = [NSURL URLWithString:obj];
        data.projectiveView = imageView;
        [datas addObject:data];
    }];
    
    YBImageBrowser *browser = [YBImageBrowser new];
    browser.dataSourceArray = datas;
    browser.currentPage = index;
    // 只有一个保存操作的时候，可以直接右上角显示保存按钮
    browser.defaultToolViewHandler.topView.operationType = YBIBTopViewOperationTypeSave;
    [browser show];
}

- (void)userIconClick:(MomentModel *)model{
    UserInfoViewController *vc = [[UserInfoViewController alloc]init];
    vc.anchorid = model.user.userid;
    [self.navigationController pushViewController:vc animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
