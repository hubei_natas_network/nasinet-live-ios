//
//  OpenGuardianViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/12/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"
#import "JXCategoryListContainerView.h"

@class AnchorGuardianModel;

NS_ASSUME_NONNULL_BEGIN

@interface OpenGuardianViewController : RootViewController<JXCategoryListContentViewDelegate>

@property (assign, nonatomic) int anchorid;
@property (strong, nonatomic) AnchorGuardianModel *guardModel;

- (void)refreshData;

@end

NS_ASSUME_NONNULL_END
