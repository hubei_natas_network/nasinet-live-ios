//
//  LandscapeLiveViewController.m
//  NasiLive
//
//  Created by yun on 2020/2/25.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "LandscapeLiveViewController.h"
#import "XLPageViewController.h"
#import "IQKeyboardManager.h"

#import "ChatRoomViewController.h"
#import "LiveUserViewController.h"
#import "ContributeRankViewController.h"
#import "InRoomBannedUserListViewController.h"

#import "ReportCategoryViewController.h"
#import "RechargeViewController.h"
#import "UserInfoViewController.h"
#import "MessageChatViewController.h"
#import "VipCenterViewController.h"

#import "ImSDK.h"

#import "SharePanelView.h"
#import "RoomChatModel.h"
#import "GiftShowView.h"
#import "GiftChooseView.h"
#import "ContinueGiftView.h"
#import "NormalGiftBackView.h"
#import "UserCardView.h"
#import "VipEnterAnimationView.h"

#import "SuperPlayer.h"
#import <BarrageRenderer.h>

#import "AnchorGuardianModel.h"
#import "GuardianListViewController.h"
#import "OpenGuardianViewController.h"
#import "GuardianViewController.h"


#define VIEWSAFEAREAINSETS(view) ({UIEdgeInsets i; if(@available(iOS 11.0, *)) {i = view.safeAreaInsets;} else {i = UIEdgeInsetsZero;} i;})


@interface LandscapeLiveViewController ()<SuperPlayerDelegate,XLPageViewControllerDelegate, XLPageViewControllerDataSrouce,UITextFieldDelegate,GiftShowViewDelegate,GiftChooseViewDelegate,BarrageRendererDelegate,UserCardViewDelegate,SharePanelViewDelegate,VipEnterAnimationViewDelegate>{
    
    InRoomBannedUserListViewController      *_bannedUserListVc;
    OpenGuardianViewController              *_openGuardVc ;
    GuardianViewController                      *_guardVc;


    
    SuperPlayerView         *superPlayer;
    LiveControlView         *liveControlView;
    
    UIButton                *attentBtn;
    UIButton                *bannedListBtn;
    UIButton                *guardBtn;
    UIView                  *containerBackView;
    UIView                  *containerView;
    
    NSArray                 *pageTitleArray;
    
    UITextField             *keyField;
    UIView                  *keyboardToolBar;
    
    GiftShowView            *giftShowView;
    GiftChooseView          *giftChooseView;
    UIView                  *shadowView;
    ContinueGiftView        *continueGiftView;
    NormalGiftBackView      *normalGiftBackView;
    VipEnterAnimationView   *_vipEnterAnimationView;
    
    BarrageRenderer         *danmakuView;
    
    CGFloat                 sideSafaWidth;
    CGFloat                 statusBarHeight;
    
    BOOL                    joinGroupFlag;
    BOOL                    _isMgr;
}

@property (weak, nonatomic) ChatRoomViewController *chatVC;

@property (strong, nonatomic) NSTimer *timer;
@property (nonatomic,assign) NSInteger timerCount;

@property (weak, nonatomic) ContributeRankViewController *contriVc;

@property (strong, nonatomic) UserCardView *currentCardView;

@property (strong, nonatomic) NSMutableArray *shareChannelArray1;
@property (strong, nonatomic) NSMutableArray *shareChannelArray2;

@property (strong, nonatomic) AnchorGuardianModel *guardModel;


@end

@implementation LandscapeLiveViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [IQKeyboardManager sharedManager].enable = NO;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    
    if (!self.timer) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.f block:^(NSTimer * _Nonnull timer) {
            if (self.timerCount % 5 == 0 && !self->joinGroupFlag) {
                [self joinGroup];
            }
            if (self.timerCount == 60) {
                self.timerCount = 0;
                [self getLiveInfo];
                if (self.liveModel.room_type == LiveRoomTypeFee) {
                    //执行扣费
                    [self doDeduct];
                }
            }
            self.timerCount ++;
        } repeats:YES];
        [self.timer fire];
    }
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addNotifationListen];
    [self createKeyboardToolBar];
    
    [self checkIsMgr];
    
    sideSafaWidth = kTopSafeHeight;
    statusBarHeight = kStatusBarHeight;
    
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, kStatusBarHeight)];
    topView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:topView];
    
    containerBackView = [[UIView alloc]initWithFrame:CGRectMake(0, kStatusBarHeight, KScreenWidth, KScreenWidth *9/16)];
    containerBackView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:containerBackView];
    containerView = [[UIView alloc]initWithFrame:CGRectMake(0, kStatusBarHeight, KScreenWidth, KScreenWidth *9/16)];
    containerView.layer.masksToBounds = YES;
    [self.view addSubview:containerView];
    
    superPlayer = [[SuperPlayerView alloc] init];
    // 设置代理，用于接受事件
    superPlayer.delegate = self;
    
    liveControlView = [[LiveControlView alloc]initWithFrame:CGRectZero];
    superPlayer.controlView = liveControlView;
    superPlayer.loop = YES;
    superPlayer.isFullScreen = NO;
    // 设置父 View，_playerView 会被自动添加到 holderView 下面
    superPlayer.fatherView = containerView;
    
    SuperPlayerModel *playerModel = [[SuperPlayerModel alloc] init];
    // 设置播放地址，直播、点播都可以
    playerModel.videoURL = self.liveModel.pull_url;
    // 开始播放
    [superPlayer playWithModel:playerModel];
    
    liveControlView.title = self.liveModel.title;
    liveControlView.hotValue = [CommonManager formateCount:self.liveModel.hot];
    [liveControlView.danmakuBtn addTarget:self action:@selector(danmakuShow:) forControlEvents:UIControlEventTouchUpInside];
    [liveControlView.giftBtn addTarget:self action:@selector(giftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    danmakuView = [[BarrageRenderer alloc] init];
    danmakuView.smoothness = .2f;
    danmakuView.canvasMargin = UIEdgeInsetsMake(20, 0, 0, 0);
    danmakuView.delegate = self;
    [self.view addSubview:danmakuView.view];
    [danmakuView start];
    
    //底部按钮
    UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, KScreenHeight - kTabBarHeight, KScreenWidth, kTabBarHeight)];
    bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottomView];
    
    UIButton *giftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    giftBtn.frame = CGRectMake(bottomView.width - 45 - 15, 2, 45, 45);
    [giftBtn setImage:IMAGE_NAMED(@"ic_gift_send") forState:UIControlStateNormal];
    [giftBtn addTarget:self action:@selector(giftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:giftBtn];
    
    UIButton *sendMsgBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sendMsgBtn.frame = CGRectMake(15, 5, CGRectGetMinX(giftBtn.frame) - 15*2, 39);
    sendMsgBtn.layer.cornerRadius = 39.f/2;
    sendMsgBtn.backgroundColor = [UIColor colorWithHexString:@"F5F5F5"];
    [sendMsgBtn setTitle:@"快来调侃一下主播吧" forState:UIControlStateNormal];
    [sendMsgBtn setTitleColor:[UIColor colorWithHexString:@"D4D3D3"] forState:UIControlStateNormal];
    sendMsgBtn.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
    [sendMsgBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [sendMsgBtn setTitleEdgeInsets: UIEdgeInsetsMake(0, 15, 0, 0)];
    [sendMsgBtn addTarget:self action:@selector(sendMsgBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:sendMsgBtn];
    
    bannedListBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    bannedListBtn.frame = CGRectMake(KScreenWidth - 15 - 45, KScreenHeight - kTabBarHeight - 45 - 30, 45, 45);
    [bannedListBtn setImage:IMAGE_NAMED(@"btn_room_ban_cycle") forState:UIControlStateNormal];
    bannedListBtn.hidden = YES ;
    [bannedListBtn addTarget:self action:@selector(bannedListBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:bannedListBtn];
    
    guardBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    guardBtn.frame = CGRectMake(KScreenWidth - 15 - 45, KScreenHeight - kTabBarHeight - 45 - 30 , 45, 45);
    [guardBtn setImage:IMAGE_NAMED(@"ic_guard") forState:UIControlStateNormal];
//    guardBtn.hidden = YES ;
    [guardBtn addTarget:self action:@selector(guardClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:guardBtn];
    
    //分栏子控制器
    pageTitleArray = @[@"聊天",@"主播",@"贡献榜"];
    
    XLPageViewControllerConfig *config = [XLPageViewControllerConfig defaultConfig];
    config.separatorLineHidden = YES;
    config.titleNormalColor = [UIColor darkGrayColor];
    config.titleSelectedColor = [UIColor blackColor];
    config.titleNormalFont = FFont15;
    config.titleSelectedFont = [UIFont boldSystemFontOfSize:22];
    config.titleSpace = 8.f;
    
    config.titleViewHeight = 44;
    config.titleViewInset = UIEdgeInsetsMake(0, 15, 0, 73);
    
    config.shadowLineColor = MAIN_COLOR;
    config.shadowLineWidth = 15;
    
    XLPageViewController *pageViewController = [[XLPageViewController alloc] initWithConfig:config];
    pageViewController.view.frame = CGRectMake(0, CGRectGetMaxY(containerView.frame), KScreenWidth, KScreenHeight - CGRectGetMaxY(containerView.frame) - kTabBarHeight);
    pageViewController.delegate = self;
    pageViewController.dataSource = self;
    [self.view insertSubview:pageViewController.view atIndex:0];
    [self addChildViewController:pageViewController];
    
    //关注按钮
    attentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    attentBtn.frame = CGRectMake(KScreenWidth - 73, CGRectGetMaxY(containerView.frame), 73, 44);
    attentBtn.titleLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightMedium];
    [attentBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    [attentBtn setImage:IMAGE_NAMED(@"ic_attent_heart") forState:UIControlStateNormal];
    [attentBtn setTitle:@"关注" forState:UIControlStateNormal];
    [attentBtn setTitle:@"已关注" forState:UIControlStateSelected];
    [attentBtn setBackgroundImage:[UIImage imageWithColor:MAIN_COLOR] forState:UIControlStateNormal];
    [attentBtn setBackgroundImage:[UIImage imageWithColor:[UIColor colorWithHexString:@"#66F6EF"]] forState:UIControlStateSelected];
    [attentBtn addTarget:self action:@selector(attentBtnClick) forControlEvents:UIControlEventTouchUpInside];
    attentBtn.selected = self.liveModel.anchor.isattent;
    [self.view insertSubview:attentBtn belowSubview:containerBackView];
    
    //礼物选择界面
    shadowView = [[UIView alloc]init];
    shadowView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.01];
    shadowView.userInteractionEnabled = YES;
    shadowView.hidden = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shadowTap)];
    [shadowView addGestureRecognizer:tap];
    [self.view addSubview:shadowView];
    [shadowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    giftChooseView = [[GiftChooseView alloc]initWithSafeAreaInsets:UIEdgeInsetsMake(0, 0, kBottomSafeHeight, 0)];
    giftChooseView.delegate = self;
    [self.view addSubview:giftChooseView];
    [giftChooseView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    normalGiftBackView = [[NormalGiftBackView alloc]initWithFrame:CGRectMake(0, kStatusBarHeight + 200, 300, 140)];
    [self.view addSubview:normalGiftBackView];
    [normalGiftBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0);
        make.height.mas_equalTo(140);
        make.width.mas_equalTo(300);
        make.centerY.equalTo(self.view.mas_centerY).multipliedBy(0.5);
    }];
    
    [self getLiveInfo];
    [self getGuardInfo];
    
    if (self.liveModel.room_type == LiveRoomTypeFee) {
        [self doDeduct];
    }
}

- (void)getLiveInfo{
    [CommonManager POST:@"live/getliveinfo" parameters:@{@"liveid":@(self.liveModel.liveid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            NSDictionary *dataDict = responseObject[@"data"];
            LiveModel *liveModel = [LiveModel yy_modelWithDictionary:dataDict[@"live"]];
            self.liveModel.anchor.fans_count = liveModel.anchor.fans_count;
            self.liveModel.anchor.isattent = liveModel.anchor.isattent;
            self.liveModel.profit = liveModel.profit;
            self.liveModel.hot = liveModel.hot;
            self->liveControlView.hotValue = [CommonManager formateCount:self.liveModel.hot];
            self->attentBtn.selected = self.liveModel.anchor.isattent;
        }else{
            QNAlertView *alertView = [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"" contentText:@"本场直播已结束" singleButton:@"确定" buttonClick:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
            alertView.cancelBackTap = YES;
        }
    } failure:^(NSError *error) {
    }];
}
- (void)getGuardInfo{
    
    [CommonManager POST:@"anchor/getGuardInfo" parameters:@{@"anchorid":@(self.liveModel.anchor.userid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.guardModel = [AnchorGuardianModel yy_modelWithDictionary:responseObject[@"data"]];
        }else{
            self.guardModel = [[AnchorGuardianModel alloc]init];
            self.guardModel.anchorid = (int)self.liveModel.anchor.userid;
            self.guardModel.uid = (int)userManager.curUserInfo.userid;
        }
    } failure:^(NSError *error) {
        self.guardModel = [[AnchorGuardianModel alloc]init];
        self.guardModel.anchorid = (int)self.liveModel.anchor.userid;
        self.guardModel.uid = (int)userManager.curUserInfo.userid;
    }];
}
- (void)checkIsMgr{
    if (![userManager isLogined]) {
        return;
    }
    [CommonManager POST:@"live/checkIsMgr" parameters:@{@"anchorid":@(self.liveModel.anchorid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self->_isMgr = YES;
            self->bannedListBtn.hidden = NO;
            self->guardBtn.mj_y =  KScreenHeight - kTabBarHeight - 45 - 30 - 65 ;
          
        }
    } failure:^(NSError *error) {
    }];
}

- (void)doDeduct{
    if (![CommonManager checkAndLogin]) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    if (userManager.curUserInfo.gold < self.liveModel.price) {
        [superPlayer resetPlayer];
        [superPlayer removeFromSuperview];
        superPlayer = nil;
        QNAlertView *alert = [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"余额不足" contentText:@"请充值后重新进入房间" singleButton:@"确定" buttonClick:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
        alert.cancelBackTap = YES;
    }else{
        [CommonManager POST:@"live/timeBilling" parameters:@{@"liveid":@(self.liveModel.liveid)} success:^(id responseObject) {
            if (RESP_SUCCESS(responseObject)) {
                userManager.curUserInfo.gold = [responseObject[@"data"][@"gold"] intValue];
                [userManager saveUserInfo];
                [self->giftChooseView refreshUserCoinNum];
                if (userManager.curUserInfo.gold < self.liveModel.price) {
                    [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"余额不足" contentText:@"请充值后重新进入房间" leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"立即充值" rightClick:^{
                        RechargeViewController *vc = [[RechargeViewController alloc]init];
                        [self.navigationController pushViewController:vc animated:YES];
                    }];
                }
            }else{
                [self->superPlayer resetPlayer];
                [self->superPlayer removeFromSuperview];
                self->superPlayer = nil;
                QNAlertView *alert = [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"扣费失败" contentText:@"请重新进入房间" singleButton:@"确定" buttonClick:^{
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                alert.cancelBackTap = YES;
            }
        } failure:^(NSError *error) {
            [self->superPlayer resetPlayer];
            [self->superPlayer removeFromSuperview];
            self->superPlayer = nil;
            QNAlertView *alert = [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"扣费失败" contentText:@"请重新进入房间" singleButton:@"确定" buttonClick:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
            alert.cancelBackTap = YES;
        }];
    }
    
}

- (void)joinGroup{
    [[V2TIMManager sharedInstance] joinGroup:self.liveModel.chatroomid msg:@"" succ:^(){
        [self sendMessage:KEnterRoomMessage];
        self->joinGroupFlag = YES;
    }fail:^(int code, NSString * err) {
        NSLog(@"code=%d, err=%@", code, err);
        if (code == 10013) {
            //已经是群成员了
            self->joinGroupFlag = YES;
        }
    }];
}

- (void)giftBtnClick{
    if (!userManager.isLogined) {
        [kAppDelegate showLoginViewController];
        return;
    }
    [giftChooseView show];
}

- (void)bannedListBtnClick{
    if (!_bannedUserListVc) {
        _bannedUserListVc = [[InRoomBannedUserListViewController alloc]init];
        _bannedUserListVc.anchorid = self.liveModel.anchorid;
        [self addChildViewController:_bannedUserListVc];
        [self.view addSubview:_bannedUserListVc.view];
        _bannedUserListVc.view.frame = CGRectMake(0, KScreenHeight, KScreenWidth, 500);
    }
    [UIView animateWithDuration:0.2 animations:^{
        self->shadowView.hidden = NO;
        self->shadowView.alpha = 1;
        self->_bannedUserListVc.view.mj_y = KScreenHeight - 500;
    }];
    [_bannedUserListVc refreshData];
}
- (void)guardClick{
    
    if (!_guardVc) {
        _guardVc = [[GuardianViewController alloc]init];
        _guardVc.anchorid = self.liveModel.anchor.userid;
        _guardVc.guardModel = self.guardModel;
        [self addChildViewController:_guardVc];
        [self.view addSubview:_guardVc.view];
        _guardVc.view.frame = CGRectMake(0, KScreenHeight, KScreenWidth, kScreenHeight - 200);
    }
    [_guardVc refreshData];
    [UIView animateWithDuration:0.2 animations:^{
        self->shadowView.hidden = NO;
        self->shadowView.alpha = 1;
        self->_guardVc.view.mj_y = 200;
    }];
    
    
}
- (void)shadowTap{
    [UIView animateWithDuration:0.2 animations:^{
        self->shadowView.alpha = 0;
        self->giftChooseView.mj_y = KScreenHeight;
        self->_bannedUserListVc.view.mj_y = KScreenHeight;
        self->_guardVc.view.mj_y = KScreenHeight;

    } completion:^(BOOL finished) {
        self->shadowView.hidden = YES;
    }];
}


- (void)sendMsgBtnClick{
    if (!userManager.isLogined) {
        [kAppDelegate showLoginViewController];
        return;
    }
    [keyField becomeFirstResponder];
}

- (void)addNotifationListen{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChanged) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    //IM通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(imNotification:) name:KNotificationInRoomIM object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(giftAnimationNotification:) name:KNotificationGiftAnimation object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showUserCardNotification:) name:KNotificationShowUserCard object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(notificationLogout:) name:KNotificationLoginOut object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillDisappear:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textFieldTextChange) name:UITextFieldTextDidChangeNotification object:nil];
}

- (void)createKeyboardToolBar{
    //输入框
    keyField = [[UITextField alloc]initWithFrame:CGRectMake(10,7,KScreenWidth- 20, 30)];
    keyField.returnKeyType = UIReturnKeySend;
    keyField.delegate = self;
    keyField.textColor = [UIColor blackColor];
    keyField.borderStyle = UITextBorderStyleNone;
    keyField.placeholder = @"";
    keyField.backgroundColor = [UIColor whiteColor];
    keyField.layer.cornerRadius = 15.0;
    keyField.layer.masksToBounds = YES;
    UIView *fieldLeft = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 15, 30)];
    fieldLeft.backgroundColor = [UIColor whiteColor];
    keyField.leftView = fieldLeft;
    keyField.leftViewMode = UITextFieldViewModeAlways;
    keyField.font = [UIFont systemFontOfSize:15];
    
    //tool绑定键盘
    keyboardToolBar = [[UIView alloc]initWithFrame:CGRectMake(0,KScreenHeight, KScreenWidth, 44)];
    keyboardToolBar.backgroundColor = [UIColor clearColor];
    UIView *tooBgv = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 44)];
    tooBgv.backgroundColor = [UIColor whiteColor];
    tooBgv.alpha = 0.7;
    [keyboardToolBar addSubview:tooBgv];
    
    [keyboardToolBar addSubview:keyField];
    
    [self.view addSubview:keyboardToolBar];
}

- (void)attentBtnClick {
    if (!userManager.isLogined) {
        [kAppDelegate showLoginViewController];
        return;
    }
    [CommonManager POST:@"Anchor/attentAnchor" parameters:@{@"anchorid":@(self.liveModel.anchor.userid),@"type":attentBtn.selected?@"0":@"1"} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self->attentBtn.selected = !self->attentBtn.selected;
            self.liveModel.anchor.isattent = self->attentBtn.selected;
            self.liveModel.anchor.fans_count = [responseObject[@"data"][@"fans_count"] intValue];
            if (self->attentBtn.selected) {
                [self sendMessage:@"关注了主播"];
            }
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (void)sendMessage:(NSString *)message{
    NSDictionary *chatDict;
    if ([userManager isLogined]) {
        chatDict = @{@"sender":[[UserInfoModel copyUserModelFromModel:userManager.curUserInfo] yy_modelToJSONObject],
                     @"message":message
        };
    }else{
        chatDict = @{@"sender":@{@"id":@(0),
                                 @"nick_name":[[NSUserDefaults standardUserDefaults]objectForKey:@"TempUid"],
                                 @"user_level":@(1),
        },
                     @"message":message
        };
    }
    NSDictionary *dict = @{@"Action":@"RoomMessage",@"Data":@{@"chat":chatDict}};
    [[V2TIMManager sharedInstance]sendGroupCustomMessage:[NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil] to:self.liveModel.chatroomid priority:V2TIM_PRIORITY_NORMAL succ:^{
        //发送成功 刷新tableview
        RoomChatModel *chatModel = [RoomChatModel yy_modelWithDictionary:chatDict];
        chatModel.textColor = @"#666666";
        chatModel.nameColor = @"8be8ff";
        IMNotificationModel *imModel = [[IMNotificationModel alloc]init];
        imModel.chat = chatModel;
        imModel.roomid = self.liveModel.chatroomid;
        [self reciveRoomMsg:imModel];
    } fail:^(int code, NSString *desc) {
        [MBProgressHUD showTipMessageInView:@"发送失败"];
    }];
}

- (void)displaySystemMessage:(NSString *)message{
    RoomChatModel *sysChatModel = [[RoomChatModel alloc]init];
    sysChatModel.chatType = RoomChatTypeSysMsg;
    sysChatModel.message = message;
    [self.chatVC addChatModel:sysChatModel];
    [self sendDanmaku:sysChatModel.message];
}

#pragma mark —————————————————— superplayer delegate ——————————————————————
- (void)sendMsg:(NSString *)message{
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimedString = [message stringByTrimmingCharactersInSet:set];
    if ([trimedString length] == 0) {
        return;
    }
    
    NSDictionary *chatDict = @{@"sender":[[UserInfoModel copyUserModelFromModel:userManager.curUserInfo] yy_modelToJSONObject],
                               @"message":trimedString
    };
    NSDictionary *dict = @{@"Action":@"RoomMessage",@"Data":@{@"chat":chatDict}};
    
    [[V2TIMManager sharedInstance]sendGroupCustomMessage:[NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil] to:self.liveModel.chatroomid priority:V2TIM_PRIORITY_NORMAL succ:^{
        //发送成功 刷新tableview
        RoomChatModel *chatModel = [RoomChatModel yy_modelWithDictionary:chatDict];
        chatModel.textColor = @"#666666";
        chatModel.nameColor = @"8be8ff";
        [self.chatVC addChatModel:chatModel];
        [self sendDanmaku:message];
    } fail:^(int code, NSString *desc) {
        [MBProgressHUD showTipMessageInView:@"发送失败"];
    }];
}

- (void)superPlayerOnShareBtnClick:(SuperPlayerView *)player{
    SharePanelView *sharePannelView = [SharePanelView showPanelInView:self.view channel1:self.shareChannelArray1 channel2:self.shareChannelArray2];
    sharePannelView.delegate = self;
}

#pragma mark ———————————————————————— SharePanelViewDelegate —————————————————————————————
- (void)shareWithChannel:(PYShareChannel)channel panel:(nonnull SharePanelView *)panelView{
    if (channel == PYShareChannelReport) {
        ReportCategoryViewController *vc = [[ReportCategoryViewController alloc]init];
        vc.relateid = self.liveModel.anchor.userid;
        vc.type = ReportTypeAnchor;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (channel == PYShareChannelLink){
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = [NSString stringWithFormat:@"%@%lld",[configManager appConfig].share_live_url,self.liveModel.anchor.userid];
        [MBProgressHUD showTipMessageInView:@"链接已复制到剪贴板"];
    }else{
        SSDKPlatformType type = [SharePanelView getSSDKPlatformTypeBy:channel];
        //统一创建分享参数
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:@"想和我的距离再近一点，那我的直播你必须到场，与你不期而遇"
                                         images:self.liveModel.thumb
                                            url:[NSURL URLWithString:[NSString stringWithFormat:@"%@%lld",[configManager appConfig].share_live_url,self.liveModel.anchor.userid]]
                                          title:[NSString stringWithFormat:@"我正在%@的直播间",self.liveModel.anchor.nick_name]
                                           type:SSDKContentTypeAuto];
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            switch (state) {
                case SSDKResponseStateSuccess:
                    [MBProgressHUD showTipMessageInView:@"分享成功"];
                    break;
                case SSDKResponseStateFail:
                {
                    [MBProgressHUD showTipMessageInView:@"分享失败"];
                    //失败
                    break;
                }
                case SSDKResponseStateCancel:
                    //取消
                    break;
                    
                default:
                    break;
            }
        }];
    }
}

#pragma mark - 弹幕

- (void)sendDanmaku:(NSString *)string{
    BarrageDescriptor * descriptor = [[BarrageDescriptor alloc]init];
    descriptor.spriteName = NSStringFromClass([BarrageWalkTextSprite class]);
    descriptor.params[@"text"] = string;
    descriptor.params[@"textColor"] = [UIColor whiteColor];
    descriptor.params[@"direction"] = @(BarrageWalkDirectionR2L);
    descriptor.params[@"speed"] = @(100);
    [danmakuView receive:descriptor];
}

- (void)danmakuShow:(UIButton *)btn {
    if (btn.selected) {
        [danmakuView.view setHidden:NO];
    } else {
        [danmakuView.view setHidden:YES];
    }
}

#pragma mark ————————————————————— ChooseGiftViewDelegate —————————————————————
- (void)sendGift:(GiftModel *)gift count:(int)count{
    if (userManager.curUserInfo.gold < gift.price*count) {
        //弹出充值窗口
        [QNAlertView showAlertViewWithType:QNAlertTypeWarning title:@"" contentText:@"余额不足" leftButtonTitle:@"取消" leftClick:nil rightButtonTitle:@"充值" rightClick:^{
            RechargeViewController *vc = [[RechargeViewController alloc]init];
            [self.navigationController pushViewController:vc animated:YES];
        }];
        return;
    }
    //预更新用户金币余额 防止多次点击
    userManager.curUserInfo.gold -= gift.price;
    [userManager saveUserInfo];
    [self->giftChooseView refreshUserCoinNum];
    
    NSDictionary *param = @{@"anchorid":@(self.liveModel.anchor.userid),
                            @"liveid":@(self.liveModel.liveid),
                            @"giftid":@(gift.giftid),
                            @"count":@(count)
    };
    [CommonManager POST:@"Gift/sendGift" parameters:param success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            userManager.curUserInfo.gold = [responseObject[@"data"][@"gold"] intValue];
            [userManager saveUserInfo];
            [self->giftChooseView refreshUserCoinNum];
            
            //显示动画
            IMNotificationModel *notificationModel = [[IMNotificationModel alloc]init];
            gift.sender = [userManager.curUserInfo yy_modelCopy];
            gift.count = count;
            notificationModel.gift = gift;
            [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationGiftAnimation object:nil userInfo:@{@"data":notificationModel}];
        }else{
            //恢复本地用户金币余额
            userManager.curUserInfo.gold += gift.price;
            [userManager saveUserInfo];
            [self->giftChooseView refreshUserCoinNum];
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        //恢复本地用户金币余额
        userManager.curUserInfo.gold += gift.price;
        [userManager saveUserInfo];
        [self->giftChooseView refreshUserCoinNum];
        RESP_FAILURE;
    }];
}

#pragma mark ———————————————————————— Notification ————————————————————————————--

- (void)imNotification:(NSNotification *)notification{
    IMNotificationModel *notificationModel = [notification userInfo][@"data"];
    if (![notificationModel.roomid isEqualToString:self.liveModel.chatroomid]) {
        return;
    }
    switch (notificationModel.action) {
        case IMNotificationActionRoomMessage:
            [self reciveRoomMsg:notificationModel];
            break;
        case IMNotificationActionLiveFinished:{
            [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"提示" contentText:@"本场直播结束，谢谢观看" singleButton:@"确定" buttonClick:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
            break;
        case IMNotificationActionRoomNotification:
            [self roomNotification:notificationModel];
            break;
        default:
            break;
    }
}

- (void)reciveRoomMsg:(IMNotificationModel *)notificationModel{
    RoomChatModel *chatModel = [notificationModel.chat yy_modelCopy];
    chatModel.textColor = @"#666666";
    chatModel.nameColor = @"8be8ff";
    [self.chatVC addChatModel:chatModel];
    [self sendDanmaku:notificationModel.chat.message];
    if ([chatModel.sender getVipLevel] > 0 && [chatModel.message isEqualToString:KEnterRoomMessage]) {
        //VIP入场提示
        if (!_vipEnterAnimationView) {
            _vipEnterAnimationView = [[VipEnterAnimationView alloc]init];
            _vipEnterAnimationView.delegate = self;
            [self.view addSubview:_vipEnterAnimationView];
        }
        [_vipEnterAnimationView addModels:notificationModel];
        if(!_vipEnterAnimationView.animating){
            [_vipEnterAnimationView enAnimation];
        }
    }
}

- (void)roomNotification:(IMNotificationModel *)notificationModel{
    switch (notificationModel.notify.type) {
        case RoomNotifyTypeSetManager:{
            if (notificationModel.notify.user.userid == userManager.curUserInfo.userid) {
                self->_isMgr = YES;
                self->bannedListBtn.hidden = NO;
                self->guardBtn.mj_y =  KScreenHeight - kTabBarHeight - 45 - 30 - 65 ;
               
            }
            [self displaySystemMessage:[NSString stringWithFormat:@"%@ 被主播设置为房管",notificationModel.notify.user.nick_name]];
        }
            break;
        case RoomNotifyTypeCancelManager:{
            if (notificationModel.notify.user.userid == userManager.curUserInfo.userid) {
                self->_isMgr = NO;
                self->bannedListBtn.hidden = YES;
                self->guardBtn.mj_y =  KScreenHeight - kTabBarHeight - 45 - 30  ;
               
            }
            [self displaySystemMessage:[NSString stringWithFormat:@"%@ 房管权限已被取消",notificationModel.notify.user.nick_name]];
        }
            break;
        default:
            break;
    }
}

- (void)giftAnimationNotification:(NSNotification *)notification{
    IMNotificationModel *notificationModel = [notification userInfo][@"data"];
    if (notificationModel.gift.type == GiftTypeNormal) {
        if (!continueGiftView) {
            continueGiftView = [[ContinueGiftView alloc]initWithFrame:CGRectMake(0, 0, normalGiftBackView.width, normalGiftBackView.height)];
            [normalGiftBackView addSubview:continueGiftView];
            //初始化礼物空位
            [continueGiftView initGift];
        }
        [continueGiftView popGiftWithNotification:notificationModel];
    }else if (notificationModel.gift.type == GiftTypeSplendid){
        if (!giftShowView) {
            giftShowView = [[GiftShowView alloc]init];
            giftShowView.delegate = self;
            [self.view addSubview:giftShowView];
        }
        if (notificationModel.gift != nil) {
            [giftShowView addModels:notificationModel];
        }
        if(!giftShowView.animating){
            [giftShowView enAnimationGift];
        }
    }
    [self displaySystemMessage:[NSString stringWithFormat:@"%@ 送给主播 %@x%d", notificationModel.gift.sender.nick_name, notificationModel.gift.title, notificationModel.gift.count]];
}

- (void)showUserCardNotification:(NSNotification *)notification{
    UserInfoModel *model = [notification userInfo][@"model"];
    if (model.userid == 0) {
        return;
    }
    if (_isMgr) {
        [UserCardView showSettingUserCardInView:self.view userModel:model delegate:self];
    }else{
        [UserCardView showUserCardInView:self.view userModel:model delegate:self];
    }
}

- (void)notificationLogout:(NSNotification *)notification{
    [self removeTimer];
}

- (void)textFieldTextChange{
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == keyField) {
        [keyField resignFirstResponder];
        if (keyField.text.length > 50) {
            [MBProgressHUD showTipMessageInWindow:@"字数最多50字"];
            return NO;
        }
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        NSString *trimedString = [keyField.text stringByTrimmingCharactersInSet:set];
        if ([trimedString length] == 0) {
            return NO;
        }
        [keyField resignFirstResponder];
        keyField.text = nil;
        [self sendMessage:trimedString];
    }
    return YES;
}

#pragma mark ——————————————————-  XLPageViewControllerDataSrouce ————————————————————--
//根据index创建对应的视图控制器，每个试图控制器只会被创建一次。
- (UIViewController *)pageViewController:(XLPageViewController *)pageViewController viewControllerForIndex:(NSInteger)index{
    if (index == 0) {
        ChatRoomViewController *vc = [[ChatRoomViewController alloc]init];
        vc.liveModel = self.liveModel;
        self.chatVC = vc;
        [self addChildViewController:vc];
        return vc;
    }else if (index == 1){
        LiveUserViewController *vc = [[LiveUserViewController alloc]init];
        vc.liveModel = self.liveModel;
        return vc;
    }else if (index == 2){
        ContributeRankViewController *vc = [[ContributeRankViewController alloc]init];
        vc.titleViewHidden = YES;
        vc.liveid = self.liveModel.liveid;
        [vc reqData];
        self.contriVc = vc;
        [self addChildViewController:vc];
        return vc;
    }
        
    return nil;
}

//根据index返回对应的标题
- (NSString *)pageViewController:(XLPageViewController *)pageViewController titleForIndex:(NSInteger)index{
    return pageTitleArray[index];
}

//返回分页数
- (NSInteger)pageViewControllerNumberOfPage{
    return pageTitleArray.count;
}

- (void)pageViewController:(XLPageViewController *)pageViewController didSelectedAtIndex:(NSInteger)index{
    if (index == 3){
        [self.contriVc reqData];
    }
}

#pragma mark ——————————————————  UserCardViewDelegate ————————————————————-
- (void)UserCardViewMsgBtnClick:(UserCardView *)view{
    TUIConversationCellData *data = [[TUIConversationCellData alloc]init];
    data.userID = [NSString stringWithFormat:@"%lld",view.userModel.userid];
    data.faceUrl = view.userModel.avatar;
    MessageChatViewController *vc = [[MessageChatViewController alloc]initWithConversationCellData:data senderName:view.userModel.nick_name];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)UserCardViewPageBtnClick:(UserCardView *)view{
    UserInfoViewController *vc = [[UserInfoViewController alloc]init];
    vc.anchorid = view.userModel.userid;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)UserCardViewOptBtnClick:(UserCardView *)view{
    if (view.optType == UserCardViewOptTypeMgr) {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"设置" message:[NSString stringWithFormat:@"用户：%@",view.userModel.nick_name] preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"禁言" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [CommonManager POST:@"live/banUser" parameters:@{@"anchorid":@(self.liveModel.anchorid),@"userid":@(view.userModel.userid),@"type":@(1)} success:^(id responseObject) {
                if (RESP_SUCCESS(responseObject)) {
                    [MBProgressHUD showTipMessageInView:@"禁言成功"];
                }else{
                    RESP_SHOW_ERROR_MSG(responseObject);
                }
            } failure:^(NSError *error) {
                RESP_FAILURE;
            }];
        }];
        UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        [alertVC addAction:action1];
        [alertVC addAction:action3];
        [self presentViewController:alertVC animated:YES completion:nil];
    }else{
        ReportCategoryViewController *vc = [[ReportCategoryViewController alloc]init];
        vc.relateid = self.liveModel.anchor.userid;
        vc.type = ReportTypeAnchor;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)UserCardViewVipChargeBtnClick:(UserCardView *)view{
    VipCenterViewController *vc = [[VipCenterViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark ——————————————————————————————— 屏幕旋转 ————————————————————————————————————-
- (void)statusBarOrientationChanged{
    
    [self.view endEditing:YES];
    [self.currentCardView hide];
    
    [super viewDidLayoutSubviews];
    CGFloat width = 0;
    CGFloat height = 0;
    CGFloat topHeight = 0;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    switch (orientation) {
        case UIInterfaceOrientationUnknown:
        case UIInterfaceOrientationPortraitUpsideDown:
        {
        }
            break;
        case UIInterfaceOrientationPortrait:
        {
            width = ScreenWidth;
            height = ScreenWidth * 9 / 16.0;
            topHeight = statusBarHeight;
            
            CGRect frame = CGRectMake(0, topHeight, width, height);
            containerView.frame = frame;
            containerBackView.frame = frame;
            superPlayer.isFullScreen = NO;
            [danmakuView.view setHidden:YES];
            
            shadowView.frame = self.view.bounds;
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
        {
            containerView.frame = CGRectMake(sideSafaWidth, 0, ScreenWidth - sideSafaWidth*2, ScreenHeight);
            containerBackView.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
            superPlayer.isFullScreen = YES;
            [danmakuView.view setHidden:NO];
            
            shadowView.frame = self.view.bounds;
        }
            break;
            
        default:
            break;
    }
    [giftChooseView setInterfaceOrientation:orientation];
}

- (BOOL)shouldAutorotate{
    return !superPlayer.isLockScreen;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    if (superPlayer.isLockScreen) {
        return UIInterfaceOrientationMaskLandscapeRight;
    }else{
        return UIInterfaceOrientationMaskPortrait|UIInterfaceOrientationMaskLandscapeLeft|UIInterfaceOrientationMaskLandscapeRight;
    }
}


- (void)keyboardWillAppear:(NSNotification *)notification{
    
    NSDictionary *info = [notification userInfo];
    
    //取出动画时长
    CGFloat animationDuration = [[info valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //取出键盘位置大小信息
    CGRect keyboardBounds = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    //rect转换
    //    CGRect keyboardRect = [self convertRect:keyboardBounds toView:nil];
    
    //记录Y轴变化
    CGFloat keyboardHeight = keyboardBounds.size.height;
    
    //上移动画options
    UIViewAnimationOptions options = (UIViewAnimationOptions)[[info valueForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue] << 16;
    
    [UIView animateWithDuration:animationDuration delay:0 options:options animations:^{
        self->keyboardToolBar.transform = CGAffineTransformMakeTranslation(0, -keyboardHeight-self->keyboardToolBar.height);
    } completion:nil];
}


- (void)keyboardWillDisappear:(NSNotification *)notification{
    NSDictionary *info = [notification userInfo];
    
    //取出动画时长
    CGFloat animationDuration = [[info valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //下移动画options
    UIViewAnimationOptions options = (UIViewAnimationOptions)[[info valueForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue] << 16;
    
    //回复动画
    [UIView animateWithDuration:animationDuration delay:0 options:options animations:^{
        self->keyboardToolBar.transform = CGAffineTransformIdentity;
    } completion:nil];
    
}

- (void)superPlayerBackAction:(SuperPlayerView *)player{
    [player pause];
    [player removeFromSuperview];
    player = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didMoveToParentViewController:(UIViewController *)parent{
    if (!parent) {
        [self removeTimer];
    }
}

- (void)superPlayerFullScreenChanged:(SuperPlayerView *)player{
    
}

#pragma mark ————————————————————————————————————— 懒加载 —————————————————————————————————————

- (NSMutableArray *)shareChannelArray1{
    if (!_shareChannelArray1) {
        _shareChannelArray1 = [NSMutableArray arrayWithArray:@[@(PYShareChannelWechat),@(PYShareChannelPengyouquan),@(PYShareChannelQQ),@(PYShareChannelWibo),@(PYShareChannelQZone),@(PYShareChannelMore)]];
    }
    return _shareChannelArray1;
}

- (NSMutableArray *)shareChannelArray2{
    if (!_shareChannelArray2) {
        _shareChannelArray2 = [NSMutableArray arrayWithArray:@[@(PYShareChannelReport),@(PYShareChannelLink)]];
    }
    return _shareChannelArray2;
}

- (void)removeTimer{
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    if (superPlayer) {
        [superPlayer resetPlayer];
        [superPlayer removeFromSuperview];
        superPlayer = nil;
    }
    [[TIMGroupManager sharedInstance] quitGroup:self.liveModel.chatroomid succ:^() {
        NSLog(@"succ");
    } fail:^(int code, NSString* err) {
        NSLog(@"failed code: %d %@", code, err);
    }];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
