//
//  LandscapeLiveViewController.h
//  NasiLive
//
//  Created by yun on 2020/2/25.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

#import "LiveModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LandscapeLiveViewController : RootViewController

@property (strong, nonatomic) LiveModel *liveModel;

@end

NS_ASSUME_NONNULL_END
