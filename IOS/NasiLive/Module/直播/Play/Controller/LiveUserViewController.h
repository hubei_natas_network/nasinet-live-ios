//
//  LiveUserViewController.h
//  NasiLive
//
//  Created by yun on 2020/3/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveUserViewController : RootViewController

@property (strong, nonatomic) LiveModel *liveModel;

@end

NS_ASSUME_NONNULL_END
