//
//  PortraitLiveViewController.h
//  NasiLive
//
//  Created by yun on 2020/2/26.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

#import "LiveModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PortraitLiveViewController : RootViewController

@property (strong, nonatomic) UserInfoModel *anchorModel;

@end

NS_ASSUME_NONNULL_END
