//
//  ContributeRankViewController.m
//  NasiLive
//
//  Created by yun on 2020/3/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "ContributeRankViewController.h"

#import "ContributeRankTableViewCell.h"

#import "UserCardView.h"

@interface ContributeRankViewController ()<UITableViewDelegate,UITableViewDataSource,UserCardViewDelegate>{
    NSMutableArray      *_datasource;
    
    UILabel             *_titleLabel;
    UILabel             *_nodataLabel;
}

@end

@implementation ContributeRankViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
    UIView *cornerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 44+14)];
    cornerView.backgroundColor = [UIColor whiteColor];
    cornerView.layer.cornerRadius = 7.f;
    [self.view addSubview:cornerView];
    
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 44)];
    _titleLabel.text = @"贡献榜";
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    _titleLabel.textColor = CFontColor;
    [self.view addSubview:_titleLabel];
    
    UIView *breakline = [[UIView alloc]initWithFrame:CGRectMake(0, 44 - 0.5f, self.view.width, 0.5f)];
    breakline.backgroundColor = CLineColor;
    [self.view addSubview:breakline];
    
    [self.view addSubview:self.tableView];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.frame = CGRectMake(0, 44, self.view.width, self.view.height - 44);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [ContributeRankTableViewCell registerWithTableView:self.tableView];
    [self removeTableMJFooter];
    [self removeTableMJHeader];
    
    _datasource = [NSMutableArray array];
    
    _nodataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 70, self.view.width, 20)];
    _nodataLabel.text = @"数据加载中...";
    _nodataLabel.textAlignment = NSTextAlignmentCenter;
    _nodataLabel.font = [UIFont systemFontOfSize:14];
    _nodataLabel.textColor = [UIColor colorWithHexString:@"666666"];
    [self.view addSubview:_nodataLabel];
}

- (void)viewWillLayoutSubviews{
    if (self.titleViewHidden) {
        _titleLabel.hidden = YES;
        self.tableView.frame = CGRectMake(0, 0, self.view.width, self.view.height - 44);
    }else{
        self.tableView.frame = CGRectMake(0, 44, self.view.width, self.view.height - 44);
    }
}

- (void)reqData{
    NSDictionary *params = @{
                             @"liveid":@(self.liveid)
    };
    
    [CommonManager POST:@"live/getContributeRank" parameters:params success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            [self->_datasource removeAllObjects];
            NSArray *models = [NSArray yy_modelArrayWithClass:[IntimacyModel class] json:responseObject[@"data"]];
            if (models.count > 0) {
                self->_nodataLabel.hidden = YES;
            }else{
                self->_nodataLabel.text = @"虚位以待~";
            }
            [self->_datasource addObjectsFromArray:models];
            [self.tableView reloadData];
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

#pragma mark ----------------------  tableviewdelegate + datasource ----------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _datasource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ContributeRankTableViewCell *cell = [ContributeRankTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.model = _datasource[indexPath.row];
    cell.index = indexPath.row;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    IntimacyModel *model = _datasource[indexPath.row];
    [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationShowUserCard object:nil userInfo:@{@"model":model.user}];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
