//
//  GuardianViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/12/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

NS_ASSUME_NONNULL_BEGIN

@class AnchorGuardianModel;

@interface GuardianViewController : RootViewController

@property (assign, nonatomic) int anchorid;
@property (strong, nonatomic) AnchorGuardianModel *guardModel;

@property (assign, nonatomic) BOOL isAnchor;

- (void)refreshData;

@end

NS_ASSUME_NONNULL_END
