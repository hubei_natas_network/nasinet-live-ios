//
//  ContributeRankViewController.h
//  NasiLive
//
//  Created by yun on 2020/3/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContributeRankViewController : RootViewController

@property (nonatomic,assign) long liveid;

@property (assign, nonatomic) BOOL titleViewHidden;

- (void)reqData;

@end

NS_ASSUME_NONNULL_END
