//
//  LiveGoodsViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/8/25.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "LiveGoodsViewController.h"
#import "QNH5ViewController.h"
#import "GoodsListTableViewCell.h"

#import "ShopGoodsModel.h"

#import "ImSDK.h"

@interface LiveGoodsViewController ()<UITableViewDelegate,UITableViewDataSource,GoodsListTableViewCellDelegate>{
    UILabel             *_titleLabel;
    UILabel             *_nodataLabel;
}

@end

@implementation LiveGoodsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    
    UIView *cornerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.width, 44+14)];
    cornerView.backgroundColor = [UIColor whiteColor];
    cornerView.layer.cornerRadius = 7.f;
    [self.view addSubview:cornerView];
    
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, self.view.width - 30, 44)];
    _titleLabel.text = [NSString stringWithFormat:@"共%lu件商品",(unsigned long)self.goodsList.count];
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    _titleLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    _titleLabel.textColor = CFontColor;
    [self.view addSubview:_titleLabel];
    
    UIView *breakline = [[UIView alloc]initWithFrame:CGRectMake(0, 44 - 0.5f, self.view.width, 0.5f)];
    breakline.backgroundColor = CLineColor;
    [self.view addSubview:breakline];
    
    [self.view addSubview:self.tableView];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.frame = CGRectMake(0, 44, self.view.width, self.view.height - 44);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self removeTableMJFooter];
    [self removeTableMJHeader];
    
    [GoodsListTableViewCell registerWithTableView:self.tableView];
}

- (void)viewWillLayoutSubviews{
    self.tableView.frame = CGRectMake(0, 44, self.view.width, self.view.height - 44);
}

#pragma mark ----------------------  tableviewdelegate + datasource ----------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.goodsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ShopGoodsModel *model = self.goodsList[indexPath.row];
    GoodsListTableViewCell *cell = [GoodsListTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.model = model;
    cell.numberIndex = indexPath.row + 1;
    cell.isAnchor = self.isAnchor;
    if (model.goodsid == self.explainingID) {
        cell.isExplaining = YES;
    }else{
        cell.isExplaining = NO;
    }
    if (self.isAnchor) {
        cell.delegate = self;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ShopGoodsModel *model = self.goodsList[indexPath.row];
    QNH5ViewController *vc = [[QNH5ViewController alloc]init];
    vc.rootHref = [NSString stringWithFormat:@"%@/h5/goods/%d",[configManager appConfig].site_domain,model.goodsid];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)operateBtnClick:(ShopGoodsModel *)model{
    [commonManager showLoadingAnimateInWindow];
    int type = model.goodsid == self.explainingID ? 0 : 1;
    [CommonManager POST:@"live/explainingGoods" parameters:@{@"goodsid":@(model.goodsid),@"type":@(type)} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            self.explainingID = type == 1 ? model.goodsid : 0;
            [self.tableView reloadData];
            //IM推送到直播间
            
            id goods = type == 0 ? [NSDictionary dictionary] : [model yy_modelToJSONObject];
            NSDictionary *dict = @{@"Action":@"ExplainingGoods",@"Data":@{@"goods":goods}};
            
            [[V2TIMManager sharedInstance]sendGroupCustomMessage:[NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil] to:self.chatroomid priority:V2TIM_PRIORITY_DEFAULT succ:nil fail:nil];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
