//
//  GuardianListViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/12/4.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"
#import "JXCategoryListContainerView.h"

NS_ASSUME_NONNULL_BEGIN





@interface GuardianListViewController : RootViewController<JXCategoryListContentViewDelegate>

@property (assign, nonatomic) int anchorid;



- (void)refreshData;

@end

NS_ASSUME_NONNULL_END
