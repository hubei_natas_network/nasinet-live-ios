//
//  ChatRoomViewController.m
//  NasiLive
//
//  Created by yun on 2020/2/27.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "ChatRoomViewController.h"

#import "RoomChatTableViewCell.h"
#import "UserCardView.h"

#import "ImSDK.h"

#import "RoomChatModel.h"

@interface ChatRoomViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray          *chatArray;
    
    TIMConversation         *conversation;
}

@end

@implementation ChatRoomViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor colorWithHexString:@"F4F7FF"];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    [RoomChatTableViewCell registerWithTableView:self.tableView];
    
    [self removeTableMJFooter];
    [self removeTableMJHeader];
    
    RoomChatModel *sysChatModel = [[RoomChatModel alloc]init];
    sysChatModel.chatType = RoomChatTypeSysMsg;
    sysChatModel.message = configManager.appConfig.room_notice;
    chatArray = [NSMutableArray arrayWithObject:sysChatModel];
    
}

- (void)addChatModel:(RoomChatModel *)chatModel{
    [chatArray addObject:chatModel];
    [self.tableView reloadData];
    //滚动到最底部
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:chatArray.count-1];
    [self.tableView scrollToRowAtIndexPath:indexpath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
}

#pragma mark -------------------- TableViewDelegate Datasource ----------------------------
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return chatArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RoomChatTableViewCell *cell = [RoomChatTableViewCell cellWithTableView:tableView indexPath:indexPath];
    cell.backViewHidden = YES;
    cell.chatModel = chatArray[indexPath.section];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 5;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 5)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RoomChatModel *chatModel = chatArray[indexPath.section];
    if (chatModel.chatType != RoomChatTypeSysMsg) {
        
        [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationShowUserCard object:nil userInfo:@{@"model":chatModel.sender}];
    }

   
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    //退出聊天室
    [[TIMGroupManager sharedInstance] quitGroup:self.liveModel.chatroomid succ:^() {
        NSLog(@"succ");
    } fail:^(int code, NSString* err) {
        NSLog(@"failed code: %d %@", code, err);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
