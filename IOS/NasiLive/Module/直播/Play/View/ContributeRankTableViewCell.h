//
//  ContributeRankTableViewCell.h
//  NasiLive
//
//  Created by yun on 2020/3/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

#import "IntimacyModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContributeRankTableViewCell : BaseTableViewCell

@property (strong, nonatomic) IntimacyModel *model;
@property (nonatomic,assign) int index;

@end

NS_ASSUME_NONNULL_END
