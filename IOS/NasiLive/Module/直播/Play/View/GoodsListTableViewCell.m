//
//  GoodsListTableViewCell.m
//  NasiLive
//
//  Created by yun11 on 2020/8/25.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "GoodsListTableViewCell.h"

#import "ShopGoodsModel.h"

@interface GoodsListTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *thumbImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (weak, nonatomic) IBOutlet UIButton *operateBtn;

@end

@implementation GoodsListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(ShopGoodsModel *)model{
    _model = model;
    [self.thumbImgView sd_setImageWithURL:[NSURL URLWithString:model.thumb_url] placeholderImage:IMAGE_NAMED(@"cover_loading")];
    self.titleLabel.text = model.title;
    self.priceLabel.text = [NSString stringWithFormat:@"￥ %@",model.price];
}

- (void)setIsAnchor:(BOOL)isAnchor{
    _isAnchor = isAnchor;
    self.operateBtn.hidden = !isAnchor;
}

- (void)setNumberIndex:(int)numberIndex{
    _numberIndex = numberIndex;
    self.numberLabel.text = [NSString stringWithFormat:@"%d",numberIndex];
}

- (void)setIsExplaining:(BOOL)isExplaining{
    _isExplaining = isExplaining;
    self.operateBtn.selected = isExplaining;
}

- (IBAction)operateBtnClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(operateBtnClick:)]) {
        [self.delegate operateBtnClick:self.model];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
