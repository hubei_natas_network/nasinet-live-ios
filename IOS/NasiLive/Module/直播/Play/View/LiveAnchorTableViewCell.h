//
//  LiveAnchorTableViewCell.h
//  NasiLive
//
//  Created by yun11 on 2020/9/29.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveAnchorTableViewCell : BaseTableViewCell

@property (strong, nonatomic) UserInfoModel *model;

@end

NS_ASSUME_NONNULL_END
