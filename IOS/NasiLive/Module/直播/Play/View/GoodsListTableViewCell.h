//
//  GoodsListTableViewCell.h
//  NasiLive
//
//  Created by yun11 on 2020/8/25.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

@class ShopGoodsModel;

NS_ASSUME_NONNULL_BEGIN

@protocol GoodsListTableViewCellDelegate <NSObject>

@optional

- (void)operateBtnClick:(ShopGoodsModel *)model;

@end

@interface GoodsListTableViewCell : BaseTableViewCell

@property (strong, nonatomic) ShopGoodsModel *model;
@property (assign, nonatomic) int numberIndex;
@property (assign, nonatomic) BOOL isExplaining;        //是否讲解中
@property (assign, nonatomic) BOOL isAnchor;            //是否主播

@property (weak, nonatomic) id<GoodsListTableViewCellDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
