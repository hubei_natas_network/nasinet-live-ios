//
//  UserCardView.h
//  NasiLive
//
//  Created by yun11 on 2020/7/4.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class UserCardView;

typedef NS_ENUM(NSInteger, UserCardViewOptType) {
    UserCardViewOptTypeReport,
    UserCardViewOptTypeMgr
};

@protocol UserCardViewDelegate <NSObject>

@optional

- (void)UserCardViewMsgBtnClick:(UserCardView *)view;
- (void)UserCardViewPageBtnClick:(UserCardView *)view;
- (void)UserCardViewOptBtnClick:(UserCardView *)view;
- (void)UserCardViewVipChargeBtnClick:(UserCardView *)view;

@end

@interface UserCardView : UIView

+ (instancetype)showUserCardInView:(UIView *)superView userModel:(UserInfoModel *)userModel delegate:(id<UserCardViewDelegate>)delegate;
+ (instancetype)showSettingUserCardInView:(UIView *)superView userModel:(UserInfoModel *)userModel delegate:(id<UserCardViewDelegate>)delegate;

@property (weak, nonatomic) id<UserCardViewDelegate> delegate;

@property (strong, nonatomic) UserInfoModel *userModel;

@property (assign, nonatomic) UserCardViewOptType optType;

- (void)show;
- (void)hide;

@end

NS_ASSUME_NONNULL_END
