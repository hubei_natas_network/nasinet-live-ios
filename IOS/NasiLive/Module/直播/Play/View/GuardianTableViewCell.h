//
//  GuardianTableViewCell.h
//  NasiLive
//
//  Created by yun11 on 2020/12/4.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@class AnchorGuardianModel;

@interface GuardianTableViewCell : BaseTableViewCell

@property (assign, nonatomic) int index;
@property (strong, nonatomic) AnchorGuardianModel *model;

@end

NS_ASSUME_NONNULL_END
