//
//  UserCardView.m
//  NasiLive
//
//  Created by yun11 on 2020/7/4.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "UserCardView.h"

#import <UIButton+WebCache.h>

@interface UserCardView()

@property (weak, nonatomic) UIView *superView;

@property (weak, nonatomic) IBOutlet UIImageView *vipLevelImgView;

@property (weak, nonatomic) IBOutlet UIButton *userIconBtn;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *ageGenderBtn;
@property (weak, nonatomic) IBOutlet UIImageView *userLevelImgView;
@property (weak, nonatomic) IBOutlet UILabel *fansCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *chargeVipBtn;
@property (weak, nonatomic) IBOutlet UIButton *optBtn;

@property (weak, nonatomic) IBOutlet UIButton *attentBtn;

@property (nonatomic, strong) UIView *backImageView;

@end

@implementation UserCardView

- (instancetype)init{
    if (self = [super init]) {
        self = [[NSBundle mainBundle]loadNibNamed:@"UserCardView" owner:self options:nil].firstObject;
    }
    return self;
}

+ (instancetype)showUserCardInView:(UIView *)superView userModel:(UserInfoModel *)userModel delegate:(id<UserCardViewDelegate>)delegate{
    UserCardView *cardView = [[self alloc]init];
    cardView.superView = superView;
    cardView.userModel = userModel;
    [cardView.optBtn setImage:IMAGE_NAMED(@"ic_card_jubao") forState:UIControlStateNormal];
    cardView.delegate = delegate;
    cardView.optType = UserCardViewOptTypeReport;
    [cardView loadUserBasicInfo];
    [cardView show];
    return cardView;
}

+ (instancetype)showSettingUserCardInView:(UIView *)superView userModel:(UserInfoModel *)userModel delegate:(id<UserCardViewDelegate>)delegate{
    UserCardView *cardView = [[self alloc]init];
    cardView.superView = superView;
    cardView.userModel = userModel;
    [cardView.optBtn setImage:IMAGE_NAMED(@"ic_card_set") forState:UIControlStateNormal];
    cardView.delegate = delegate;
    cardView.optType = UserCardViewOptTypeMgr;
    [cardView loadUserBasicInfo];
    [cardView show];
    return cardView;
}

- (void)show{
    self.centerX = self.superView.width/2;
    self.centerY = self.superView.height/2 - 20;
    [self.superView addSubview:self];
}

- (void)hide{
    [self.backImageView removeFromSuperview];
    self.backImageView = nil;
    [self removeFromSuperview];
}

- (void)willMoveToSuperview:(UIView *)newSuperview{
    if (newSuperview == nil) {
        return;
    }
    
    if (!self.backImageView) {
        self.backImageView = [[UIView alloc] initWithFrame:self.superView.bounds];
        self.backImageView.backgroundColor = [UIColor blackColor];
        self.backImageView.alpha = 0.3f;
        self.backImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
            [self hide];
        }];
        [self.backImageView addGestureRecognizer:tap];
    }
    [self.superView addSubview:self.backImageView];
    
    self.transform = CGAffineTransformMakeScale(0.4, 0.4);
    [UIView animateWithDuration:0.4f delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:20 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        self.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
    }];
    [super willMoveToSuperview:newSuperview];
}

- (void)setUserModel:(UserInfoModel *)userModel{
    _userModel = userModel;
    
    [self.userIconBtn sd_setBackgroundImageWithURL:[NSURL URLWithString:userModel.avatar] forState:UIControlStateNormal placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    self.nickNameLabel.text = userModel.nick_name;
    self.ageGenderBtn.selected = userModel.profile.gender;
    [self.ageGenderBtn setTitle:[NSString stringWithFormat:@"%d",userModel.profile.age] forState:UIControlStateNormal];
    [self.ageGenderBtn setBackgroundColor:userModel.profile.gender?[UIColor colorWithHexString:@"#8CD8FF"]:[UIColor colorWithHexString:@"#FF9CE4"]];
    self.fansCountLabel.text = [NSString stringWithFormat:@"粉丝：%d",userModel.fans_count];
    
    if (userModel.is_anchor) {
        [self.userLevelImgView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"ic_anchor_level_%d",userModel.anchor_level]]];
    }else{
        [self.userLevelImgView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"ic_user_level_%d",userModel.user_level]]];
    }
    
    if ([userModel checkVip]) {
        self.vipLevelImgView.hidden = NO;
        self.vipLevelImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"bg_card_vip_%d",[userModel getVipLevel]]];
        [self.chargeVipBtn setTitle:@"了解贵族特权" forState:UIControlStateNormal];
    }else{
        self.vipLevelImgView.hidden = YES;
        [self.chargeVipBtn setTitle:@"立即开通贵族" forState:UIControlStateNormal];
    }
    self.attentBtn.selected = userModel.isattent;
}

- (void)loadUserBasicInfo{
    [CommonManager POST:@"User/getUserBasicInfo" parameters:@{@"id":@(self.userModel.userid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.userModel = [UserInfoModel yy_modelWithDictionary:responseObject[@"data"]];
        }
    } failure:^(NSError *error) {
        
    }];
}

- (IBAction)optBtnClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(UserCardViewOptBtnClick:)]) {
        [self.delegate UserCardViewOptBtnClick:self];
    }
}

- (IBAction)chargeVipBtn:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(UserCardViewVipChargeBtnClick:)]) {
        [self.delegate UserCardViewVipChargeBtnClick:self];
    }
}

- (IBAction)attentBtnClick:(UIButton *)sender {
    if (!userManager.isLogined) {
        [kAppDelegate showLoginViewController];
        return;
    }
    [CommonManager POST:@"Anchor/attentAnchor" parameters:@{@"anchorid":@(self.userModel.userid),@"type":self.attentBtn.selected?@"0":@"1"} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.attentBtn.selected = !self.attentBtn.selected;
            self.userModel.isattent = self.attentBtn.selected;
            self.userModel.fans_count = [responseObject[@"data"][@"fans_count"] intValue];
            self.fansCountLabel.text = [NSString stringWithFormat:@"粉丝：%d",self.userModel.fans_count];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (IBAction)msgBtnClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(UserCardViewMsgBtnClick:)]) {
        [self.delegate UserCardViewMsgBtnClick:self];
    }
}

- (IBAction)pageBtnClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(UserCardViewPageBtnClick:)]) {
        [self.delegate UserCardViewPageBtnClick:self];
    }
}

- (IBAction)closeBtnClick:(UIButton *)sender {
    [self hide];
}

- (UIViewController *)appRootViewController{
    UIViewController *appRootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *topVC = appRootVC;
    while (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    return topVC;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
