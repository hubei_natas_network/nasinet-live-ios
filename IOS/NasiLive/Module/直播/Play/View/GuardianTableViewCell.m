//
//  GuardianTableViewCell.m
//  NasiLive
//
//  Created by yun11 on 2020/12/4.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "GuardianTableViewCell.h"

#import "AnchorGuardianModel.h"

@interface GuardianTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *userIconImgView;
@property (weak, nonatomic) IBOutlet UIImageView *rankImgView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *intimacyLabel;


@end

@implementation GuardianTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setIndex:(int)index{
    _index = index;
    if (index < 3) {
        self.rankImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_guardian_no%d",index+1]];
    }
    self.rankImgView.hidden = index >= 3;
}

- (void)setModel:(AnchorGuardianModel *)model{
    _model = model;
    
    [self.userIconImgView sd_setImageWithURL:[NSURL URLWithString:model.user.avatar] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    self.intimacyLabel.text = [NSString stringWithFormat:@"本周贡献%d金币",model.intimacy_week];
    
    self.nickNameLabel.text = model.user.nick_name;
    
    NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] initWithString:self.nickNameLabel.text attributes:nil];
    NSAttributedString *speaceString = [[NSAttributedString alloc]initWithString:@" "];
    
    //vip标识
    NSTextAttachment *vipAttchment = [[NSTextAttachment alloc]init];
    vipAttchment.bounds = CGRectMake(0, -4, 16, 19);//设置frame
    vipAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"vip_level_%d",[model.user getVipLevel]]];//设置图片
    NSAttributedString *vipString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(vipAttchment)];
    
    //等级标识
    NSTextAttachment *levelAttchment = [[NSTextAttachment alloc]init];
    levelAttchment.bounds = CGRectMake(0, -3, 34, 15);//设置frame
    levelAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_user_level_%d",model.user.user_level]];
    NSAttributedString *levelString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(levelAttchment)];
    
    
    //守护标识
    NSTextAttachment *guardAttchment = [[NSTextAttachment alloc]init];
    guardAttchment.bounds = CGRectMake(0, -3, 34, 15);//设置frame
    guardAttchment.image = [UIImage imageNamed:@"ic_guardian"];
    NSAttributedString *guardString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(guardAttchment)];
    
    //插入VIP图标
    if ([model.user checkVip]) {
        [titleStr appendAttributedString:speaceString];//插入到第几个下标
        [titleStr appendAttributedString:vipString];//插入到第几个下标
    }
    //插入等级图标
    [titleStr appendAttributedString:speaceString];//插入到第几个下标
    [titleStr appendAttributedString:levelString];//插入到第几个下标
    
    //插入守护图标
    [titleStr appendAttributedString:speaceString];//插入到第几个下标
    [titleStr appendAttributedString:guardString];//插入到第几个下标
    
    [self.nickNameLabel setAttributedText:titleStr];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
