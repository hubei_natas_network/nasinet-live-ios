//
//  ContributeRankCollectionViewCell.h
//  NasiLive
//
//  Created by yun11 on 2020/6/5.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IntimacyModel;

NS_ASSUME_NONNULL_BEGIN

@interface ContributeRankCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IntimacyModel *model;

@property (assign, nonatomic) int rankIndex;

@end

NS_ASSUME_NONNULL_END
