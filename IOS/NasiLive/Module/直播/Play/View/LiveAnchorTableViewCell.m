//
//  LiveAnchorTableViewCell.m
//  NasiLive
//
//  Created by yun11 on 2020/9/29.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "LiveAnchorTableViewCell.h"
#import "UserInfoViewController.h"

#import <UIButton+WebCache.h>

@interface LiveAnchorTableViewCell()

@property (weak, nonatomic) IBOutlet UIButton *iconBtn;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *roomNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *signatureLabel;
@property (weak, nonatomic) IBOutlet UIButton *ageGenderBtn;


@end

@implementation LiveAnchorTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(UserInfoModel *)model{
    _model = model;
    [self.iconBtn sd_setImageWithURL:[NSURL URLWithString:model.avatar] forState:UIControlStateNormal placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    self.signatureLabel.text = model.profile.signature;
    self.roomNumLabel.text = intstr(model.userid);
    [self.ageGenderBtn setBackgroundColor:model.profile.gender?[UIColor colorWithHexString:@"6FCAFF"]:[UIColor colorWithHexString:@"FF9CE4"]];
    self.ageGenderBtn.selected = model.profile.gender;
    [self.ageGenderBtn setTitle:[NSString stringWithFormat:@"%d",model.profile.age] forState:UIControlStateNormal];
    
    self.nickNameLabel.text = model.nick_name;
    NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] initWithString:self.nickNameLabel.text attributes:nil];
    NSAttributedString *speaceString = [[NSAttributedString alloc]initWithString:@"  "];
    
    //vip标识
    NSTextAttachment *vipAttchment = [[NSTextAttachment alloc]init];
    vipAttchment.bounds = CGRectMake(0, -4, 16, 19);//设置frame
    vipAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"vip_level_%d",[model getVipLevel]]];//设置图片
    NSAttributedString *vipString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(vipAttchment)];
    
    //主播等级标识
    NSTextAttachment *anchorLevelAttchment = [[NSTextAttachment alloc]init];
    anchorLevelAttchment.bounds = CGRectMake(0, -3, 34, 15);//设置frame
    anchorLevelAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_anchor_level_%d",model.anchor_level]];
    NSAttributedString *anchorLevelString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(anchorLevelAttchment)];
    
    //插入VIP图标
    if ([model checkVip]) {
        [titleStr appendAttributedString:speaceString];//插入到第几个下标
        [titleStr appendAttributedString:vipString];//插入到第几个下标
    }
    
    if (model.is_anchor && model.anchor_level > 0) {
        //插入主播等级图标
        [titleStr appendAttributedString:speaceString];//插入到第几个下标
        [titleStr appendAttributedString:anchorLevelString];//插入到第几个下标
    }
    
    [self.nickNameLabel setAttributedText:titleStr];
}

- (IBAction)iconBtnClick:(UIButton *)sender {
    UserInfoViewController *vc = [[UserInfoViewController alloc]init];
    vc.anchorid = self.model.userid;
    [self.viewController.navigationController pushViewController:vc animated:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
