//
//  ContributeRankCollectionViewCell.m
//  NasiLive
//
//  Created by yun11 on 2020/6/5.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "ContributeRankCollectionViewCell.h"

#import "IntimacyModel.h"

@interface ContributeRankCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *userIconImgView;
@property (weak, nonatomic) IBOutlet UIImageView *rankImgView;


@end

@implementation ContributeRankCollectionViewCell

- (void)setModel:(IntimacyModel *)model{
    _model = model;
    
    [self.userIconImgView sd_setImageWithURL:[NSURL URLWithString:model.user.avatar] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
}

- (void)setRankIndex:(int)rankIndex{
    _rankIndex = rankIndex;
    switch (rankIndex) {
        case 1:
        case 2:
        case 3:{
                self.rankImgView.hidden = NO;
                self.rankImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_live_user_rank%d",rankIndex]];
            }
            break;
        default:
            self.rankImgView.hidden = YES;
            break;
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

@end
