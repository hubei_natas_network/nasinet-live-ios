//
//  AnchorGuardianModel.m
//  NasiLive
//
//  Created by yun11 on 2020/12/4.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "AnchorGuardianModel.h"

@implementation AnchorGuardianModel

- (BOOL)checkGuard{
    if (self.expire_time.length > 0) {
        int i = [CommonManager compareOneDay:[CommonManager dateWithString:self.expire_time] withAnotherDay:[NSDate date]];
        if (i > 0) {
            return YES;
        }
        return NO;
    }
    return NO;
}

@end
