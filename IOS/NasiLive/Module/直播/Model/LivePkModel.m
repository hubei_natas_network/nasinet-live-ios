//
//  LivePkModel.m
//  NasiLive
//
//  Created by yun11 on 2020/12/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "LivePkModel.h"

@implementation LivePkModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"pkid" : @"id"
    };
}

@end
