//
//  RoomChatModel.h
//  Nasi
//
//  Created by yun on 2019/12/26.
//  Copyright © 2019 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, RoomChatType) {
    RoomChatTypeUserMsg = 0,
    RoomChatTypeSysMsg
};

@interface RoomChatModel : NSObject

@property (strong, nonatomic) UserInfoModel *sender; //发送人信息

@property (copy, nonatomic) NSString *message;
@property (copy, nonatomic) NSString *textColor;
@property (copy, nonatomic) NSString *nameColor;

@property (assign, nonatomic) BOOL   is_manager;
@property (assign, nonatomic) BOOL   is_guardian;

@property (nonatomic,assign) RoomChatType chatType;

@end

NS_ASSUME_NONNULL_END
