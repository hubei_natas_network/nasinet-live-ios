//
//  LivePkModel.h
//  NasiLive
//
//  Created by yun11 on 2020/12/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LivePkModel : NSObject

@property (assign, nonatomic) int pkid;
@property (assign, nonatomic) int home_anchorid;
@property (assign, nonatomic) int away_anchorid;
@property (assign, nonatomic) int home_score;
@property (assign, nonatomic) int away_score;

@property (copy, nonatomic) NSString *create_time;

@end

NS_ASSUME_NONNULL_END
