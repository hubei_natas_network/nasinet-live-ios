//
//  GiftModel.h
//  Nasi
//
//  Created by yun on 2019/12/31.
//  Copyright © 2019 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UserInfoModel;

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, GiftAnimatType) {
    GiftAnimatTypeNone = 0,
    GiftAnimatTypeGif = 1,
    GiftAnimatTypeSVGA = 2
};

typedef NS_ENUM(NSUInteger, GiftType) {
    GiftTypeNormal = 0,      //普通礼物
    GiftTypeSplendid = 1     //豪华礼物
};

typedef NS_ENUM(NSUInteger, SepcialType) {
    GiftSepcialTypeNone = 0,            //普通礼物
    GiftSepcialTypeAllChannel = 1,      //全频道礼物
    GiftSepcialTypeGuard = 2            //守护专属
};

@interface GiftModel : NSObject

@property (nonatomic,assign) int giftid;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *icon; //缩略图地址
@property (copy, nonatomic) NSString *animation; //动画地址
@property (nonatomic,assign) GiftAnimatType animat_type;
@property (nonatomic,assign) int duration; //动画时长
@property (nonatomic,assign) int price;
@property (nonatomic,assign) int status;
@property (nonatomic,assign) GiftType type;
@property (nonatomic,assign) SepcialType use_type;

//赠送礼物相关
@property (strong, nonatomic) UserInfoModel *sender;
@property (assign, nonatomic) int count;

@end

NS_ASSUME_NONNULL_END
