//
//  ShopGoodsModel.m
//  NasiLive
//
//  Created by yun11 on 2020/8/21.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "ShopGoodsModel.h"

@implementation ShopGoodsModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"goodsid" : @"id"
    };
}

// 当 JSON 转为 Model 完成后，该方法会被调用。
// 你可以在这里对数据进行校验，如果校验不通过，可以返回 NO，则该 Model 会被忽略。
// 你也可以在这里做一些自动转换不能完成的工作。
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    if (StrValid(dic[@"thumb_urls"])) {
        NSString *thumb_urls = dic[@"thumb_urls"];
        NSArray *thumbArr = [thumb_urls componentsSeparatedByString:@","];
        if (thumbArr.count > 0) {
            _thumb_url = thumbArr[0];
        }
    }
    return YES;
}

@end
