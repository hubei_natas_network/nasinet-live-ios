//
//  ShopGoodsModel.h
//  NasiLive
//
//  Created by yun11 on 2020/8/21.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShopGoodsModel : NSObject

@property (assign, nonatomic) int goodsid;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *thumb_url;
@property (copy, nonatomic) NSString *price;
@property (assign, nonatomic) BOOL live_explaining; //是否正在讲解中

@end

NS_ASSUME_NONNULL_END
