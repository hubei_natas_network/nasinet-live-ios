//
//  LiveModel.h
//  Nasi
//
//  Created by yun on 2019/12/26.
//  Copyright © 2019 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LivePkModel.h"

typedef NS_ENUM(NSInteger, LiveRoomType) {
    LiveRoomTypeNormal,
    LiveRoomTypePwd,
    LiveRoomTypeFee
};

typedef NS_ENUM(NSInteger, PkStatus) {
    PkStatusNO = 0,
    PkStatusInPk,
    PkStatusFinding,
};

@interface LiveModel : NSObject

@property (nonatomic,assign) long long liveid;
@property (nonatomic,assign) NSInteger anchorid;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSString *thumb;
@property (nonatomic,copy) NSString *pull_url;
@property (nonatomic,copy) NSString *acc_pull_url;
@property (nonatomic,copy) NSString *push_url;
@property (copy, nonatomic) NSString *start_time;
@property (copy, nonatomic) NSString *end_time;
@property (copy, nonatomic) NSString *chatroomid;

@property (assign, nonatomic) LiveRoomType room_type;
@property (assign, nonatomic) int price;
@property (copy, nonatomic) NSString *password;

@property (assign, nonatomic) NSInteger start_stamp;
@property (assign, nonatomic) NSInteger end_stamp;

@property (nonatomic,assign) NSInteger categoryid;
@property (nonatomic,assign) NSInteger hot;
@property (nonatomic,assign) NSInteger orientation; //1-横屏 2-竖屏

@property (assign, nonatomic) BOOL          link_on;
@property (assign, nonatomic) BOOL          link_status;
@property (assign, nonatomic) PkStatus      pk_status; //0-未开启 1-pk中 2-匹配中

@property (nonatomic,assign) int profit;

@property (strong, nonatomic) UserInfoModel *anchor;

@property (strong, nonatomic) LivePkModel *pkinfo;
@property (strong, nonatomic) LiveModel *pklive;

@end
