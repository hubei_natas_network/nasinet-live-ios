//
//  GiftModel.m
//  Nasi
//
//  Created by yun on 2019/12/31.
//  Copyright © 2019 yun7. All rights reserved.
//

#import "GiftModel.h"

@implementation GiftModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"giftid" : @"id"
             };
}

@end
