//
//  AnchorGuardianModel.h
//  NasiLive
//
//  Created by yun11 on 2020/12/4.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, AnchorGuardianType){
    AnchorGuardianTypeWeek = 0,
    AnchorGuardianTypeMonth,
    AnchorGuardianTypeYear
};

@interface AnchorGuardianModel : NSObject

@property (assign, nonatomic) int anchorid;
@property (assign, nonatomic) int uid;
@property (assign, nonatomic) AnchorGuardianType type;
@property (copy, nonatomic) NSString *expire_time;

@property (assign, nonatomic) int intimacy_week;
@property (assign, nonatomic) int intimacy;

@property (strong, nonatomic) UserInfoModel *user;

- (BOOL)checkGuard;

@end

NS_ASSUME_NONNULL_END
