//
//  LiveModel.m
//  Nasi
//
//  Created by yun on 2019/12/26.
//  Copyright © 2019 yun7. All rights reserved.
//

#import "LiveModel.h"

@implementation LiveModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{};
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{
             
    };
}

// 当 JSON 转为 Model 完成后，该方法会被调用。
// 你可以在这里对数据进行校验，如果校验不通过，可以返回 NO，则该 Model 会被忽略。
// 你也可以在这里做一些自动转换不能完成的工作。
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    _chatroomid = [NSString stringWithFormat:@"LIVEROOM_%ld",(long)_anchorid];
    return YES;
}

@end
