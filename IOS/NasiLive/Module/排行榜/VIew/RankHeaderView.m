//
//  RankHeaderView.m
//  NasiLive
//
//  Created by yun11 on 2020/7/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RankHeaderView.h"

#import "UserRankModel.h"
#import "AnchorRankModel.h"

@interface RankHeaderView ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImgView1;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel1;
@property (weak, nonatomic) IBOutlet UIImageView *vipImgView1;
@property (weak, nonatomic) IBOutlet UIImageView *levelImgView1;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *levelCenterLC1;

@property (weak, nonatomic) IBOutlet UIImageView *iconImgView2;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel2;
@property (weak, nonatomic) IBOutlet UIImageView *vipImgView2;
@property (weak, nonatomic) IBOutlet UIImageView *levelImgView2;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *levelCenterLC2;

@property (weak, nonatomic) IBOutlet UIImageView *iconImgView3;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel3;
@property (weak, nonatomic) IBOutlet UIImageView *vipImgView3;
@property (weak, nonatomic) IBOutlet UIImageView *levelImgView3;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *levelCenterLC3;

@property (strong, nonatomic) NSArray *iconImageViewArray;
@property (strong, nonatomic) NSArray *nickNameLabelArray;
@property (strong, nonatomic) NSArray *vipImgViewArray;
@property (strong, nonatomic) NSArray *levelImgViewArray;
@property (strong, nonatomic) NSArray *subTitleLabelArray;
@property (strong, nonatomic) NSArray *levelCenterLCArray;

@end

@implementation RankHeaderView

- (instancetype)init{
    self = [[NSBundle mainBundle]loadNibNamed:@"RankHeaderView" owner:self options:nil].firstObject;
    
    self.iconImageViewArray = @[self.iconImgView1,self.iconImgView2,self.iconImgView3];
    self.nickNameLabelArray = @[self.nickNameLabel1,self.nickNameLabel2,self.nickNameLabel3];
    self.vipImgViewArray = @[self.vipImgView1,self.vipImgView2,self.vipImgView3];
    self.levelImgViewArray = @[self.levelImgView1,self.levelImgView2,self.levelImgView3];
    self.subTitleLabelArray = @[self.subTitleLabel1,self.subTitleLabel2,self.subTitleLabel3];
    self.levelCenterLCArray = @[self.levelCenterLC1,self.levelCenterLC2,self.levelCenterLC3];
    
    [self.iconImgView1 addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        if ([self.delegate respondsToSelector:@selector(tapUserAtIndex:)]) {
            [self.delegate tapUserAtIndex:0];
        }
    }]];
    
    [self.iconImgView2 addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        if ([self.delegate respondsToSelector:@selector(tapUserAtIndex:)]) {
            [self.delegate tapUserAtIndex:1];
        }
    }]];
    
    [self.iconImgView3 addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
        if ([self.delegate respondsToSelector:@selector(tapUserAtIndex:)]) {
            [self.delegate tapUserAtIndex:2];
        }
    }]];
    
    return self;
}

- (void)setDatasource:(NSArray *)datasource{
    _datasource = datasource;
    for (int i = 0; i < datasource.count && i < 3; i++) {
        UserInfoModel *userModel;
        NSString *subtitle;
        if (self.type == 0) {
            UserRankModel *rankModel = datasource[i];
            userModel = rankModel.user;
            subtitle = rankModel.consume;
        }else{
            AnchorRankModel *rankModel = datasource[i];
            userModel = rankModel.anchor;
            subtitle = rankModel.income;
        }
        [self setUpIndex:i userModel:userModel subText:subtitle];
    }
    for (int i = 2; i > datasource.count-1; i--) {
        [self setUpIndex:i userModel:nil subText:@""];
    }
}

- (void)setUpIndex:(int)index userModel:(UserInfoModel *)userModel subText:(NSString *)subText{
    UIImageView *iconImgView = (UIImageView *)self.iconImageViewArray[index];
    UILabel *nickNameLabel = (UILabel *)self.nickNameLabelArray[index];
    UIImageView *vipImgView = (UIImageView *)self.vipImgViewArray[index];
    UIImageView *levelImgView = (UIImageView *)self.levelImgViewArray[index];
    UILabel *subTitleLabel = (UILabel *)self.subTitleLabelArray[index];
    NSLayoutConstraint *levelCenterLC = (NSLayoutConstraint *)self.levelCenterLCArray[index];
    
    if (userModel) {
        [iconImgView sd_setImageWithURL:[NSURL URLWithString:userModel.avatar] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
        nickNameLabel.text = userModel.nick_name;
        if (self.type == 0) {
            levelImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_user_level_%d",userModel.user_level]];
        }else{
            levelImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_anchor_level_%d",userModel.anchor_level]];
        }
        
        int vip = [userModel getVipLevel];
        vipImgView.hidden = vip == 0;
        if (vip > 0) {
            vipImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"vip_level_%d",vip]];
            levelCenterLC.constant = 12;
        }else{
            levelCenterLC.constant = 0;
        }
        subTitleLabel.text = subText;
    }else{
        iconImgView.image = IMAGE_NAMED(@"ic_avatar");
        nickNameLabel.text = @"虚位以待";
        vipImgView.hidden = YES;
        levelImgView.hidden = YES;
        subTitleLabel.hidden = YES;
    }
}

@end
