//
//  RankHeaderView.h
//  NasiLive
//
//  Created by yun11 on 2020/7/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol RankHeaderViewDelegate <NSObject>

@optional
- (void)tapUserAtIndex:(int)index;

@end

@interface RankHeaderView : UIView

@property (weak, nonatomic) id<RankHeaderViewDelegate> delegate;

@property (strong, nonatomic) NSArray *datasource;
@property (assign, nonatomic) int type; //0-土豪榜。1-主播榜

@end

NS_ASSUME_NONNULL_END
