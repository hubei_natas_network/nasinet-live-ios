//
//  RankTableViewCell.h
//  NasiLive
//
//  Created by yun11 on 2020/7/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

#import "UserRankModel.h"
#import "AnchorRankModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RankTableViewCell : BaseTableViewCell

@property (strong, nonatomic) UserRankModel *userModel;
@property (strong, nonatomic) AnchorRankModel *anchorModel;
@property (assign, nonatomic) int index;

@end

NS_ASSUME_NONNULL_END
