//
//  RankTableViewCell.m
//  NasiLive
//
//  Created by yun11 on 2020/7/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RankTableViewCell.h"

@interface RankTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *indexLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;

@property (weak, nonatomic) IBOutlet UIButton *attentBtn;

@end

@implementation RankTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setUserModel:(UserRankModel *)userModel{
    _userModel = userModel;
    [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:userModel.user.avatar] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    [self setNickNameLabelText:userModel.user type:0]; //富豪榜
    self.subTitleLabel.text = [NSString stringWithFormat:@"%@ 金币",userModel.consume];
    self.attentBtn.selected = userModel.user.isattent;
}

- (void)setAnchorModel:(AnchorRankModel *)anchorModel{
    _anchorModel = anchorModel;
    [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:anchorModel.anchor.avatar] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    [self setNickNameLabelText:anchorModel.anchor type:1]; //主播榜
    self.subTitleLabel.text = [NSString stringWithFormat:@"%@ 钻石",anchorModel.income];
    self.attentBtn.selected = anchorModel.anchor.isattent;
}

- (void)setIndex:(int)index{
    _index = index;
    if (index < 10) {
        self.indexLabel.text = [NSString stringWithFormat:@"0%d",index];
    }else{
        self.indexLabel.text = [NSString stringWithFormat:@"%d",index];
    }
}

- (void)setNickNameLabelText:(UserInfoModel *)model type:(int)type{
    self.nickNameLabel.text = model.nick_name;
    
    NSMutableAttributedString *titleStr = [[NSMutableAttributedString alloc] initWithString:self.nickNameLabel.text attributes:nil];
    NSAttributedString *speaceString = [[NSAttributedString alloc]initWithString:@" "];
    
    //vip标识
    NSTextAttachment *vipAttchment = [[NSTextAttachment alloc]init];
    vipAttchment.bounds = CGRectMake(0, -4, 16, 19);//设置frame
    vipAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"vip_level_%d",[model getVipLevel]]];//设置图片
    NSAttributedString *vipString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(vipAttchment)];
    
    //等级标识
    NSTextAttachment *levelAttchment = [[NSTextAttachment alloc]init];
    levelAttchment.bounds = CGRectMake(0, -3, 34, 15);//设置frame
    if (type == 0) {
        levelAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_user_level_%d",model.user_level]];
    }else{
        levelAttchment.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_anchor_level_%d",model.anchor_level]];
    }
    NSAttributedString *levelString = [NSAttributedString attributedStringWithAttachment:(NSTextAttachment *)(levelAttchment)];
    
    //插入VIP图标
    if ([model checkVip]) {
        [titleStr appendAttributedString:speaceString];//插入到第几个下标
        [titleStr appendAttributedString:vipString];//插入到第几个下标
    }
    //插入等级图标
    [titleStr appendAttributedString:speaceString];//插入到第几个下标
    [titleStr appendAttributedString:levelString];//插入到第几个下标
    
    [self.nickNameLabel setAttributedText:titleStr];
}

- (IBAction)attentBtnClick:(UIButton *)sender {
    NSDictionary *param = @{@"anchorid":self.anchorModel?@(self.anchorModel.anchor.userid):@(self.userModel.user.userid),
                            @"type":@(!sender.selected)
    };
    [CommonManager POST:@"Anchor/attentAnchor" parameters:param success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            if (self.anchorModel) {
                self.anchorModel.anchor.isattent = !sender.selected;
            }else{
                self.userModel.user.isattent = !sender.selected;
            }
            sender.selected = !sender.selected;
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
