//
//  RankHomeViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/7/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RankHomeViewController.h"
#import "RankHomeSubViewController.h"

#import "JXCategoryTitleView.h"
#import "JXCategoryView.h"

@interface RankHomeViewController ()<JXCategoryViewDelegate,JXCategoryListContainerViewDelegate>

@property (nonatomic, strong) NSArray <NSString *>          *titles;

@property (nonatomic, strong) JXCategoryTitleView           *categoryView;
@property (strong, nonatomic) JXCategoryListContainerView   *listContainerView;


@property (nonatomic, assign) NSInteger currentIndex;

@end

@implementation RankHomeViewController

- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _titles = @[@"富豪榜", @"明星榜"];
    
    [self createUI];
}

- (void)createUI{
    
    UIImageView *topBackView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenWidth * 242/1242)];
    topBackView.image = [UIImage imageNamed:@"bg_rank_top"];
    topBackView.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:topBackView];
    
    CGFloat top = topBackView.height - 30;
    _categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, top, [UIScreen mainScreen].bounds.size.width, 30)];
    self.categoryView.titles = self.titles;
    self.categoryView.backgroundColor = [UIColor clearColor];
    self.categoryView.delegate = self;
    self.categoryView.titleFont = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
    self.categoryView.titleSelectedColor = [UIColor whiteColor];
    self.categoryView.titleColor = [UIColor colorWithHexString:@"#E1C5FF"];
    self.categoryView.titleColorGradientEnabled = YES;
    self.categoryView.titleLabelZoomEnabled = NO;
    self.categoryView.contentScrollViewClickTransitionAnimationEnabled = NO;
    
    self.listContainerView = [[JXCategoryListContainerView alloc] initWithType:JXCategoryListContainerType_ScrollView delegate:self];
    self.listContainerView.frame = CGRectMake(0, self.categoryView.mj_maxY, KScreenWidth, KScreenHeight - self.categoryView.mj_maxY);
    self.listContainerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.listContainerView];
    
    self.categoryView.listContainer = self.listContainerView;
    [self.view addSubview:self.categoryView];
    
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorColor = [UIColor whiteColor];
    lineView.indicatorWidth = 12;
    lineView.indicatorHeight = 3;
    lineView.indicatorCornerRadius = 1.5;
    self.categoryView.indicators = @[lineView];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, kStatusBarHeight, 44, 44);
    [backBtn setImage:IMAGE_NAMED(@"ic_back_white") forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
}

#pragma mark - JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
}

- (void)categoryView:(JXCategoryBaseView *)categoryView didScrollSelectedItemAtIndex:(NSInteger)index {
}

#pragma mark - JXCategoryListContainerViewDelegate

- (void)listContainerViewDidScroll:(UIScrollView *)scrollView{
    if ([self isKindOfClass:[RankHomeViewController class]]) {
        CGFloat index = scrollView.contentOffset.x/scrollView.bounds.size.width;
        CGFloat absIndex = fabs(index - self.currentIndex);
        if (absIndex >= 1) {
            //”快速滑动的时候，只响应最外层VC持有的scrollView“，说实话，完全可以不用处理这种情况。如果你们的产品经理坚持认为这是个问题，就把这块代码加上吧。
            //嵌套使用的时候，最外层的VC持有的scrollView在翻页之后，就断掉一次手势。解决快速滑动的时候，只响应最外层VC持有的scrollView。子VC持有的scrollView却没有响应
            self.listContainerView.scrollView.panGestureRecognizer.enabled = NO;
            self.listContainerView.scrollView.panGestureRecognizer.enabled = YES;
            _currentIndex = floor(index);
        }
    }
}

- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index{
    RankHomeSubViewController *vc = [[RankHomeSubViewController alloc]init];
    vc.type = index;
    return vc;
}

- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return self.titles.count;
}



@end
