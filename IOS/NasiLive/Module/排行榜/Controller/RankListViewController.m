//
//  RankListViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/7/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RankListViewController.h"
#import "UserInfoViewController.h"

#import "RankHeaderView.h"
#import "RankTableViewCell.h"

@interface RankListViewController ()<UITableViewDelegate,UITableViewDataSource,RankHeaderViewDelegate>{
    RankHeaderView              *headerView;
    NSArray                     *datasource;
}

@end

@implementation RankListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self removeTableMJFooter];
    
    [RankTableViewCell registerWithTableView:self.tableView];
    
    headerView = [[RankHeaderView alloc]init];
    headerView.type = self.type;
    headerView.delegate = self;
    self.tableView.tableHeaderView = headerView;
    self.tableView.tableHeaderView.frame = CGRectMake(0, 0, KScreenWidth, KScreenWidth * 917/1242);
    
    [self.tableView.mj_header beginRefreshing];
}

- (void)headerRereshing{
    NSString *api = self.type == 0?@"Rank/getUserRankList":@"Rank/getAnchorRankList";
    [CommonManager POST:api parameters:@{@"type":@(self.subType)} success:^(id responseObject) {
        [self.tableView.mj_header endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            if (self.type == 0) {
                self->datasource = [NSArray yy_modelArrayWithClass:[UserRankModel class] json:responseObject[@"data"]];
            }else{
                self->datasource = [NSArray yy_modelArrayWithClass:[AnchorRankModel class] json:responseObject[@"data"]];
            }
            [self.tableView reloadData];
            self->headerView.datasource = self->datasource;
        }
    } failure:^(NSError *error) {
        [self.tableView.mj_header endRefreshing];
        RESP_FAILURE;
    }];
}

- (void)loadData{
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (datasource.count >= 3) {
        return datasource.count - 3;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    RankTableViewCell *cell = [RankTableViewCell cellWithTableView:tableView indexPath:indexPath];
    if (self.type == 0) {
        cell.userModel = datasource[indexPath.row + 3];
    }else{
        cell.anchorModel = datasource[indexPath.row + 3];
    }
    cell.index = indexPath.row + 4;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UserInfoViewController *vc = [[UserInfoViewController alloc]init];
    if (self.type == 0) {
        UserRankModel *model = datasource[indexPath.row + 3];
        vc.anchorid = model.user.userid;
    }else{
        AnchorRankModel *model = datasource[indexPath.row + 3];
        vc.anchorid = model.anchor.userid;
    }
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)tapUserAtIndex:(int)index{
    if (datasource.count > index) {
        UserInfoViewController *vc = [[UserInfoViewController alloc]init];
        if (self.type == 0) {
            UserRankModel *model = datasource[index];
            vc.anchorid = model.user.userid;
        }else{
            AnchorRankModel *model = datasource[index];
            vc.anchorid = model.anchor.userid;
        }
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (UIView *)listView {
    return self.view;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
