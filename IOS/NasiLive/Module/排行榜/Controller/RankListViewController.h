//
//  RankListViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/7/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

#import "JXCategoryView.h"

NS_ASSUME_NONNULL_BEGIN

@interface RankListViewController : RootViewController<JXCategoryListContentViewDelegate>

@property (assign, nonatomic) int type; //0-土豪榜。1-主播榜
@property (assign, nonatomic) int subType; //0-日榜 1-周榜 2-月榜 3-总榜

@end

NS_ASSUME_NONNULL_END
