//
//  UserRankModel.h
//  NasiLive
//
//  Created by yun11 on 2020/7/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserRankModel : NSObject

@property (assign, nonatomic) int uid;
@property (copy, nonatomic) NSString *consume;
@property (strong, nonatomic) UserInfoModel *user;

@end

NS_ASSUME_NONNULL_END
