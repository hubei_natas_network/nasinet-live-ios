//
//  AnchorRankModel.h
//  NasiLive
//
//  Created by yun11 on 2020/7/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AnchorRankModel : NSObject

@property (assign, nonatomic) int uid;
@property (copy, nonatomic) NSString *income;
@property (strong, nonatomic) UserInfoModel *anchor;

@end

NS_ASSUME_NONNULL_END
