//
//  SVPanGestureRecognizer.h
//  SVVideo
//
//  Created by yun11 on 2019/7/31.
//  Copyright © 2019 yun11. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, SVPanGestureRecognizerDirection) {
    SVPanGestureRecognizerDirectionVertical,
    SVPanGestureRecognizerDirectionHorizontal
};

@interface SVPanGestureRecognizer : UIPanGestureRecognizer

@property (nonatomic, assign) SVPanGestureRecognizerDirection direction;

@end

NS_ASSUME_NONNULL_END
