//
//  SVScaleVideoView.h
//  GKDYVideo
//
//  Created by gaokun on 2019/7/30.
//  Copyright © 2019 QuintGao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserSVPageController.h"
#import "SVideoListViewController.h"

#import "SVVideoView.h"

NS_ASSUME_NONNULL_BEGIN

@protocol SVScaleVideoViewProtocol <NSObject>

@property (strong, nonatomic) UIViewController *currentListVC;

@end

@interface SVScaleVideoView : UIView

@property (nonatomic, strong) SVVideoView *videoView;

- (instancetype)initWithVC:(UIViewController<SVScaleVideoViewProtocol> *)vc datasourceVC:(SVideoListViewController *)dataVC videos:(NSArray *)videos index:(NSInteger)index;

- (void)show;

@end

NS_ASSUME_NONNULL_END
