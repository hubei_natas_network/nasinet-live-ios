//
//  SVVideoControlView.m
//  SVVideo
//
//  Created by yun11 on 2018/9/23.
//  Copyright © 2018 yun11. All rights reserved.
//

#import "SVVideoControlView.h"
#import "GKLikeView.h"

@interface SVVideoItemButton : UIButton

@end

@implementation SVVideoItemButton

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.imageView sizeToFit];
    [self.titleLabel sizeToFit];
    
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    
    CGFloat imgW = self.imageView.frame.size.width;
    CGFloat imgH = self.imageView.frame.size.height;
    
    self.imageView.frame = CGRectMake((width - imgH) / 2, 0, imgW, imgH);
    
    CGFloat titleW = self.titleLabel.frame.size.width;
    CGFloat titleH = self.titleLabel.frame.size.height;
    
    self.titleLabel.frame = CGRectMake((width - titleW) / 2, height - titleH, titleW, titleH);
}

@end

@interface SVVideoControlView()<GKLikeViewDelegate>{
    BOOL                    _isInLikeRequesting;
}

@property (nonatomic, strong) UIImageView           *topShadowView;
@property (nonatomic, strong) UIImageView           *bottomShadowView;
@property (nonatomic, strong) UIImageView           *iconView;
@property (nonatomic, strong) GKLikeView            *likeView;
@property (nonatomic, strong) SVVideoItemButton     *commentBtn;
@property (nonatomic, strong) SVVideoItemButton     *shareBtn;
@property (strong, nonatomic) UIButton              *attentBtn;

@property (nonatomic, strong) UILabel               *nameLabel;
@property (nonatomic, strong) UILabel               *contentLabel;

@property (nonatomic, strong) UIButton              *playBtn;

@end

@implementation SVVideoControlView

- (instancetype)init {
    if (self = [super init]) {
        [self addSubview:self.coverImgView];
        [self addSubview:self.topShadowView];
        [self addSubview:self.bottomShadowView];
        [self addSubview:self.iconView];
        [self addSubview:self.attentBtn];
        [self addSubview:self.likeView];
        [self addSubview:self.commentBtn];
        [self addSubview:self.shareBtn];
        [self addSubview:self.nameLabel];
        [self addSubview:self.contentLabel];
        [self addSubview:self.sliderView];
        
        [self addSubview:self.playBtn];
        
        [self.coverImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        [self.topShadowView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.mas_equalTo(0);
            make.height.mas_equalTo(kTopHeight + 30);
        }];
        
        [self.bottomShadowView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.left.right.mas_equalTo(0);
            make.height.mas_equalTo(130);
        }];
        
        CGFloat bottomM = 0;
        
        self.sliderView.frame = CGRectMake(0, KScreenHeight - kTabBarHeight - 0.5, KScreenWidth, 1.0f);
        
        [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(15);
            make.right.offset(-85);
            make.bottom.equalTo(self).offset(-(15 + bottomM));
        }];
        
        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentLabel);
            make.bottom.equalTo(self.contentLabel.mas_top).offset(-15);
        }];
        
        [self.shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).offset(-15);
            make.bottom.equalTo(self.sliderView.mas_top).offset(-50.0f);
            make.width.height.mas_equalTo(55.0f);
        }];
        
        [self.commentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.shareBtn);
            make.bottom.equalTo(self.shareBtn.mas_top).offset(-17.0f);
            make.width.height.mas_equalTo(55.0f);
        }];
        
        [self.likeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.shareBtn);
            make.bottom.equalTo(self.commentBtn.mas_top).offset(-17.0f);
            make.width.height.mas_equalTo(55.0f);
        }];
        
        [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.shareBtn);
            make.bottom.equalTo(self.likeView.mas_top).offset(-25.0f);
            make.width.height.mas_equalTo(50.0f);
        }];
        
        [self.attentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.iconView);
            make.top.equalTo(self.iconView.mas_bottom).offset(-10);
            make.width.height.mas_offset(20);
        }];
        
        [self.playBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self);
        }];
    }
    return self;
}

- (void)setModel:(ShortVideoModel *)model {
    _model = model;
    
    self.sliderView.value = 0;
    
    [self.coverImgView sd_setImageWithURL:[NSURL URLWithString:model.thumb_url] placeholderImage:[UIImage imageNamed:@"img_sv_loading"]];
    
    self.nameLabel.text = [NSString stringWithFormat:@"@%@", model.author.nick_name];
    
    if ([model.author.avatar containsString:@"http"]) {
         [self.iconView sd_setImageWithURL:[NSURL URLWithString:model.author.avatar] placeholderImage:[UIImage imageNamed:@"ic_avatar"]];
    }else {
        self.iconView.image = [UIImage imageNamed:@"ic_avatar"];
    }
    
    self.contentLabel.text = model.title;
    
    [self.likeView setupLikeState:model.is_like];
    [self.likeView setupLikeCount:[NSString stringWithFormat:@"%d",model.like_count]];
    
    [self.commentBtn setTitle:[NSString stringWithFormat:@"%d",model.comment_count] forState:UIControlStateNormal];
    [self.shareBtn setTitle:[NSString stringWithFormat:@"%d",model.share_count] forState:UIControlStateNormal];
    
    if (userManager.curUserInfo.userid == model.author.userid) {
        self.attentBtn.hidden = YES;
    }else{
        self.attentBtn.hidden = NO;
        [self.attentBtn setSelected:model.author.isattent];
    }
}

#pragma mark - Public Methods
- (void)setProgress:(float)progress {
    self.sliderView.value = progress;
}

- (void)startLoading {
    [self.sliderView showLineLoading];
}

- (void)stopLoading {
    [self.sliderView hideLineLoading];
}

- (void)showPlayBtn {
    self.playBtn.hidden = NO;
}

- (void)hidePlayBtn {
    self.playBtn.hidden = YES;
}

- (void)showLikeAnimation {
    [self.likeView startAnimationWithIsLike:YES];
}

- (void)showUnLikeAnimation {
    [self.likeView startAnimationWithIsLike:NO];
}

#pragma mark - Action
- (void)controlViewDidClick {
    if ([self.delegate respondsToSelector:@selector(controlViewDidClickSelf:)]) {
        [self.delegate controlViewDidClickSelf:self];
    }
}

- (void)iconDidClick:(id)sender {
    if ([self.delegate respondsToSelector:@selector(controlViewDidClickIcon:)]) {
        [self.delegate controlViewDidClickIcon:self];
    }
}

- (void)attentBtnClick:(id)sender {
    if ([self.delegate respondsToSelector:@selector(controlViewDidClickAttent:)]) {
        [self.delegate controlViewDidClickAttent:self];
    }
}

- (void)praiseBtnClick:(id)sender {
    if ([self.delegate respondsToSelector:@selector(controlViewDidClickPriase:)]) {
        [self.delegate controlViewDidClickPriase:self];
    }
}

- (void)commentBtnClick:(id)sender {
    if ([self.delegate respondsToSelector:@selector(controlViewDidClickComment:)]) {
        [self.delegate controlViewDidClickComment:self];
    }
}

- (void)shareBtnClick:(id)sender {
    if ([self.delegate respondsToSelector:@selector(controlViewDidClickShare:)]) {
        [self.delegate controlViewDidClickShare:self];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    NSTimeInterval delayTime = 0.3f;
    
    if (touch.tapCount <= 1) {
        [self performSelector:@selector(controlViewDidClick) withObject:nil afterDelay:delayTime];
    }else {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(controlViewDidClick) object:nil];
        
        if ([self.delegate respondsToSelector:@selector(controlView:touchesBegan:withEvent:)]) {
            [self.delegate controlView:self touchesBegan:touches withEvent:event];
        }
    }
}

#pragma mark - GKLikeViewDelegate
- (void)starBtnClick:(GKLikeView *)sender{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    
    if (_isInLikeRequesting) return;
    
    _isInLikeRequesting = YES;
    
    NSDictionary *param = @{@"videoid":@(self.model.videoid),
                            @"type":self.model.is_like?@(0):@(1)
    };
    [CommonManager POST:@"shortvideo/likeVideo" parameters:param success:^(id responseObject) {
        self->_isInLikeRequesting = NO;
        if (RESP_SUCCESS(responseObject)) {
            self.model.is_like = !self.model.is_like;
            self.model.like_count = [responseObject[@"data"][@"like_count"] intValue];
            [self.likeView setupLikeCount:[NSString stringWithFormat:@"%d",self.model.like_count]];
            if (self.model.is_like) {
                [self.likeView startAnimationWithIsLike:YES];
            }else {
                [self.likeView startAnimationWithIsLike:NO];
            }
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        self->_isInLikeRequesting = NO;
        RESP_FAILURE;
    }];
}

#pragma mark - 懒加载
- (UIImageView *)coverImgView {
    if (!_coverImgView) {
        _coverImgView = [UIImageView new];
        _coverImgView.contentMode = UIViewContentModeScaleAspectFill;
        _coverImgView.clipsToBounds = YES;
    }
    return _coverImgView;
}

- (UIImageView *)topShadowView {
    if (!_topShadowView) {
        _topShadowView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_sv_shadow_top"]];
        _topShadowView.contentMode = UIViewContentModeScaleToFill;
        _topShadowView.clipsToBounds = YES;
    }
    return _topShadowView;
}

- (UIImageView *)bottomShadowView {
    if (!_bottomShadowView) {
        _bottomShadowView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_sv_shadow_bottom"]];
        _bottomShadowView.contentMode = UIViewContentModeScaleToFill;
        _bottomShadowView.clipsToBounds = YES;
    }
    return _bottomShadowView;
}

- (UIImageView *)iconView {
    if (!_iconView) {
        _iconView = [UIImageView new];
        _iconView.layer.cornerRadius = 25.0f;
        _iconView.layer.masksToBounds = YES;
        _iconView.layer.borderColor = [UIColor whiteColor].CGColor;
        _iconView.layer.borderWidth = 1.0f;
        _iconView.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *iconTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iconDidClick:)];
        [_iconView addGestureRecognizer:iconTap];
    }
    return _iconView;
}

- (UIButton *)attentBtn{
    if (!_attentBtn) {
        _attentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_attentBtn setImage:[UIImage imageNamed:@"ic_sv_attent"] forState:UIControlStateNormal];
        [_attentBtn setImage:[UIImage imageNamed:@"ic_sv_attented"] forState:UIControlStateSelected];
        [_attentBtn addTarget:self action:@selector(attentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _attentBtn;
}

- (GKLikeView *)likeView {
    if (!_likeView) {
        _likeView = [GKLikeView new];
        _likeView.delegate = self;
    }
    return _likeView;
}

- (SVVideoItemButton *)commentBtn {
    if (!_commentBtn) {
        _commentBtn = [SVVideoItemButton new];
        [_commentBtn setImage:[UIImage imageNamed:@"ic_sv_message"] forState:UIControlStateNormal];
        _commentBtn.titleLabel.font = [UIFont systemFontOfSize:13.0f];
        [_commentBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_commentBtn addTarget:self action:@selector(commentBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _commentBtn;
}

- (SVVideoItemButton *)shareBtn {
    if (!_shareBtn) {
        _shareBtn = [SVVideoItemButton new];
        [_shareBtn setImage:[UIImage imageNamed:@"ic_sv_share"] forState:UIControlStateNormal];
        _shareBtn.titleLabel.font = [UIFont systemFontOfSize:13.0f];
        [_shareBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_shareBtn addTarget:self action:@selector(shareBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareBtn;
}

- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [UILabel new];
        _nameLabel.textColor = [UIColor whiteColor];
        _nameLabel.font = [UIFont boldSystemFontOfSize:15.0f];
        _nameLabel.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *nameTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iconDidClick:)];
        [_nameLabel addGestureRecognizer:nameTap];
    }
    return _nameLabel;
}

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.numberOfLines = 0;
        _contentLabel.textColor = [UIColor whiteColor];
        _contentLabel.font = [UIFont systemFontOfSize:14.0f];
    }
    return _contentLabel;
}

- (GKSliderView *)sliderView {
    if (!_sliderView) {
        _sliderView = [GKSliderView new];
        _sliderView.isHideSliderBlock = YES;
        _sliderView.sliderHeight = 1.0f;
        _sliderView.maximumTrackTintColor = [UIColor clearColor];
        _sliderView.minimumTrackTintColor = [UIColor whiteColor];
    }
    return _sliderView;
}

- (UIButton *)playBtn {
    if (!_playBtn) {
        _playBtn = [UIButton new];
        [_playBtn setImage:[UIImage imageNamed:@"ic_sv_pause"] forState:UIControlStateNormal];
        [_playBtn addTarget:self action:@selector(controlViewDidClick) forControlEvents:UIControlEventTouchUpInside];
        _playBtn.hidden = YES;
    }
    return _playBtn;
}

@end
