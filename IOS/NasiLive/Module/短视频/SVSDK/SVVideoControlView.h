//
//  SVVideoControlView.h
//  SVVideo
//
//  Created by yun11 on 2018/9/23.
//  Copyright © 2018 yun11. All rights reserved.
//  播放器视图控制层

#import <UIKit/UIKit.h>
#import "ShortVideoModel.h"
#import "GKSliderView.h"

NS_ASSUME_NONNULL_BEGIN

@class SVVideoControlView;

@protocol SVVideoControlViewDelegate <NSObject>

- (void)controlViewDidClickSelf:(SVVideoControlView *)controlView;

- (void)controlViewDidClickIcon:(SVVideoControlView *)controlView;

- (void)controlViewDidClickAttent:(SVVideoControlView *)controlView;

- (void)controlViewDidClickPriase:(SVVideoControlView *)controlView;

- (void)controlViewDidClickComment:(SVVideoControlView *)controlView;

- (void)controlViewDidClickShare:(SVVideoControlView *)controlView;

- (void)controlView:(SVVideoControlView *)controlView touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event;

@end

@interface SVVideoControlView : UIView

@property (nonatomic, weak) id<SVVideoControlViewDelegate> delegate;

// 视频封面图:显示封面并播放视频
@property (nonatomic, strong) UIImageView           *coverImgView;

@property (nonatomic, strong) ShortVideoModel        *model;

@property (nonatomic, strong) GKSliderView          *sliderView;

- (void)setProgress:(float)progress;

- (void)startLoading;
- (void)stopLoading;

- (void)showPlayBtn;
- (void)hidePlayBtn;

- (void)showLikeAnimation;
- (void)showUnLikeAnimation;

@end

NS_ASSUME_NONNULL_END
