//
//  SVCommentView.h
//  SVVideo
//
//  Created by yun11 on 2019/5/1.
//  Copyright © 2019 yun11. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ShortVideoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SVCommentView : UIView

@property (strong, nonatomic) ShortVideoModel *videoModel;

@property (nonatomic,copy) void (^CommentCountBlock)(int count);

@end

NS_ASSUME_NONNULL_END
