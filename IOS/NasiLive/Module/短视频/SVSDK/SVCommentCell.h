//
//  SVCommentCell.h
//  Meet1V1
//
//  Created by yun on 2020/1/13.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "BaseTableViewCell.h"

@class SVCommentModel;

NS_ASSUME_NONNULL_BEGIN

@protocol SVCommentCellDelegate <NSObject>

@optional
- (void)commentLikeClick:(SVCommentModel *)model;
- (void)replyClick:(SVCommentModel *)model;

@end

@interface SVCommentCell : BaseTableViewCell

@property (strong, nonatomic) SVCommentModel *model;

@property (weak, nonatomic) id<SVCommentCellDelegate> delegate;

@property (assign, nonatomic) BOOL isShowReplyCount;

@end

NS_ASSUME_NONNULL_END
