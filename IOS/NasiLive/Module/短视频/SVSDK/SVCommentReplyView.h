//
//  SVCommentReplyView.h
//  SVVideo
//
//  Created by yun11 on 2019/5/1.
//  Copyright © 2019 yun11. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SVCommentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SVCommentReplyView : UIView

@property (strong, nonatomic) SVCommentModel *commentModel;

@end

NS_ASSUME_NONNULL_END
