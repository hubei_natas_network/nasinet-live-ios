//
//  SVVideoPlayer.h
//  SVVideo
//
//  Created by QuintGao on 2018/9/23.
//  Copyright © 2018 QuintGao. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, SVVideoPlayerStatus) {
    SVVideoPlayerStatusUnload,      // 未加载
    SVVideoPlayerStatusPrepared,    // 准备播放
    SVVideoPlayerStatusLoading,     // 加载中
    SVVideoPlayerStatusPlaying,     // 播放中
    SVVideoPlayerStatusPaused,      // 暂停
    SVVideoPlayerStatusEnded,       // 播放完成
    SVVideoPlayerStatusError        // 错误
};

@class SVVideoPlayer;

@protocol SVVideoPlayerDelegate <NSObject>

- (void)player:(SVVideoPlayer *)player statusChanged:(SVVideoPlayerStatus)status;

- (void)player:(SVVideoPlayer *)player currentTime:(float)currentTime totalTime:(float)totalTime progress:(float)progress;

- (void)openVipBtnClick;

@end

@interface SVVideoPlayer : NSObject

@property (nonatomic, weak) id<SVVideoPlayerDelegate>     delegate;

@property (nonatomic, assign) SVVideoPlayerStatus         status;

@property (nonatomic, assign) BOOL                          isPlaying;


/**
 根据指定url在指定视图上播放视频
 
 @param playView 播放视图
 @param url 播放地址
 */
- (void)playVideoWithView:(UIView *)playView url:(NSString *)url videoid:(int)videoid;

/**
 停止播放并移除播放视图
 */
- (void)removeVideo;

/**
 暂停播放
 */
- (void)pausePlay;

/**
 恢复播放
 */
- (void)resumePlay;

/**
 重新播放
 */
- (void)resetPlay;

@end

NS_ASSUME_NONNULL_END
