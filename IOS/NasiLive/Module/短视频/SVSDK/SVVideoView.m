//
//  SVVideoView.m
//  SVVideo
//
//  Created by yun11 on 2018/9/23.
//  Copyright © 2018 yun11. All rights reserved.
//

#import "SVVideoView.h"
#import "GKDYVideoPlayer.h"
#import "SVPanGestureRecognizer.h"
#import "GKBallLoadingView.h"

#import "VipCenterViewController.h"

#import "twEmojiView.h"

@interface SVVideoView()<UIScrollViewDelegate, SVVideoPlayerDelegate, SVVideoControlViewDelegate, UIGestureRecognizerDelegate,UITextFieldDelegate,twEmojiViewDelegate>

@property (nonatomic, strong) UIScrollView              *scrollView;

@property (strong, nonatomic) UIView                    *bottomView;
@property (strong, nonatomic) UIView                    *keyboardToolBarBackShadow;
@property (strong, nonatomic) UIView                    *keyboardToolBar;
@property (strong, nonatomic) UITextField               *inputTextKeyField;
@property (strong, nonatomic) UIButton                  *inputBtn;
@property (strong, nonatomic) UIButton                  *emojiBtn;
@property (strong, nonatomic) UIButton                  *toolbarEmojiBtn;
    
@property (strong, nonatomic) twEmojiView               *emojiView;

// 创建三个控制视图，用于滑动切换
@property (nonatomic, strong) SVVideoControlView        *topView;   // 顶部视图
@property (nonatomic, strong) SVVideoControlView        *ctrView;   // 中间视图
@property (nonatomic, strong) SVVideoControlView        *btmView;   // 底部视图

// 控制播放的索引，不完全等于当前播放内容的索引
@property (nonatomic, assign) NSInteger                 index;

@property (nonatomic, weak) UIViewController<SVVcProtocol>            *vc;
@property (nonatomic, assign) BOOL                      isPushed;

@property (nonatomic, strong) SVVideoPlayer             *player;

// 记录播放内容
@property (nonatomic, assign) int                       currentPlayId;

// 记录滑动前的播放状态
@property (nonatomic, assign) BOOL                      isPlaying_beforeScroll;

@property (nonatomic, assign) BOOL                      isRefreshingMore;
@property (nonatomic, assign) BOOL                      isMoreData;

@property (nonatomic, strong) SVPanGestureRecognizer    *panGesture;

@property (nonatomic, assign) BOOL                      interacting;
// 开始移动时的位置
@property (nonatomic, assign) CGFloat                   startLocationY;

@property (nonatomic, assign) CGPoint                   startLocation;
@property (nonatomic, assign) CGRect                    startFrame;

//用户索引
@property (strong, nonatomic) NSMutableDictionary       *userIndexes;


@end

@implementation SVVideoView

- (instancetype)initWithVC:(UIViewController<SVVcProtocol> *)vc isPushed:(BOOL)isPushed {
    if (self = [super init]) {
        self.vc = vc;
        self.isPushed = isPushed;
        
        [self addSubview:self.scrollView];
        
        if (!isPushed) {
            [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self);
            }];
        }else{
            [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.left.right.mas_equalTo(0);
                make.bottom.offset(-kTabBarHeight);
            }];
            [self addSubview:self.bottomView];
            [self addSubview:self.keyboardToolBarBackShadow];
            [self addSubview:self.keyboardToolBar];
            
            [self addSubview:self.emojiView];
            
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
            [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillDisappear:) name:UIKeyboardWillHideNotification object:nil];
        }
        
        [self.scrollView addGestureRecognizer:self.panGesture];
        
        if (isPushed)  {
            [self addSubview:self.backBtn];

            [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self).offset(15.0f);
                make.top.equalTo(self).offset(kTopSafeHeight + 20.0f);
                make.width.height.mas_equalTo(44.0f);
            }];
        }
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat controlW = CGRectGetWidth(self.scrollView.frame);
    CGFloat controlH = CGRectGetHeight(self.scrollView.frame);
    
    self.topView.frame   = CGRectMake(0, 0, controlW, controlH);
    self.ctrView.frame   = CGRectMake(0, controlH, controlW, controlH);
    self.btmView.frame   = CGRectMake(0, 2 * controlH, controlW, controlH);
}

#pragma mark - Public Methods
- (void)setModels:(NSArray *)models index:(NSInteger)index {
    [self.videos removeAllObjects];
    [self.videos addObjectsFromArray:models];
    
    self.isMoreData = models.count >= self.perPageSize;
    
    self.index = index;
    self.currentPlayIndex = index;
    
    if (models.count == 0) return;
    
    //为视频创建用户索引
    [self.userIndexes removeAllObjects];
    for (int i = 0; i < models.count; i++) {
        ShortVideoModel *model = models[i];
        if ([[self.userIndexes allKeys] containsObject:@(model.author.userid)]) {
            NSMutableArray *array = self.userIndexes[@(model.author.userid)];
            [array addObject:@(i)];
        }else{
            NSMutableArray *array = [NSMutableArray arrayWithObject:@(i)];
            self.userIndexes[@(model.author.userid)] = array;
        }
    }
    
    if (models.count == 1) {
        [self.ctrView removeFromSuperview];
        [self.btmView removeFromSuperview];
        
        self.scrollView.contentSize = CGSizeMake(0, KScreenHeight - kTabBarHeight);
        
        self.topView.hidden = NO;
        self.topView.model = [self.videos firstObject];
        
        [self playVideoFrom:self.topView];
    }else if (models.count == 2) {
        [self.btmView removeFromSuperview];
        
        self.scrollView.contentSize = CGSizeMake(0, (KScreenHeight - kTabBarHeight )* 2);
        
        self.topView.hidden = NO;
        self.ctrView.hidden = NO;
        self.topView.model  = [self.videos firstObject];
        self.ctrView.model  = [self.videos lastObject];
        
        if (index == 1) {
            self.scrollView.contentOffset = CGPointMake(0, KScreenHeight - kTabBarHeight);
            
            [self playVideoFrom:self.ctrView];
        }else {
            [self playVideoFrom:self.topView];
        }
    }else {
        self.topView.hidden = NO;
        self.ctrView.hidden = NO;
        self.btmView.hidden = NO;
        
        if (index == 0) {   // 如果是第一个，则显示上视图，且预加载中下视图
            self.topView.model = self.videos[index];
            self.ctrView.model = self.videos[index + 1];
            self.btmView.model = self.videos[index + 2];
            
            // 播放第一个
            [self playVideoFrom:self.topView];
        }else if (index == models.count - 1) { // 如果是最后一个，则显示最后视图，且预加载前两个
            self.btmView.model = self.videos[index];
            self.ctrView.model = self.videos[index - 1];
            self.topView.model = self.videos[index - 2];
            
            // 显示最后一个
            self.scrollView.contentOffset = CGPointMake(0, (KScreenHeight - kTabBarHeight )* 2);
            // 播放最后一个
            [self playVideoFrom:self.btmView];
        }else { // 显示中间，播放中间，预加载上下
            self.ctrView.model = self.videos[index];
            self.topView.model = self.videos[index - 1];
            self.btmView.model = self.videos[index + 1];
            
            // 显示中间
            self.scrollView.contentOffset = CGPointMake(0, KScreenHeight - kTabBarHeight);
            // 播放中间
            [self playVideoFrom:self.ctrView];
        }
    }
}

- (void)appendModels:(NSArray *)models {
    [self.videos addObjectsFromArray:models];
    for (int i = (self.videos.count - models.count); i < self.videos.count; i++) {
        ShortVideoModel *model = self.videos[i];
        if ([[self.userIndexes allKeys] containsObject:@(model.author.userid)]) {
            NSMutableArray *array = self.userIndexes[@(model.author.userid)];
            [array addObject:@(i)];
        }else{
            NSMutableArray *array = [NSMutableArray arrayWithObject:@(i)];
            self.userIndexes[@(model.author.userid)] = array;
        }
    }
}

- (void)updateUserModel:(int)userid attent:(BOOL)isattent{
    if (![self.userIndexes containsObjectForKey:@(userid)]) {
        return;
    }
    NSMutableArray *array = self.userIndexes[@(userid)];
    for (NSNumber *index in array) {
        ShortVideoModel *model = self.videos[[index intValue]];
        model.author.isattent = isattent;
    }
    [self.topView setModel:self.topView.model];
    [self.ctrView setModel:self.ctrView.model];
    [self.btmView setModel:self.btmView.model];
}

- (void)setCurrentPlayIndex:(NSInteger)currentPlayIndex{
    _currentPlayIndex = currentPlayIndex;
    // 播放到倒数第5个时，请求更多内容
    if (currentPlayIndex + 6 > self.videos.count && !self.isRefreshingMore) {
        [self refreshMore];
    }
}

- (void)pause {
    if (self.player.isPlaying) {
        self.isPlaying_beforeScroll = YES;
    }else {
        self.isPlaying_beforeScroll = NO;
    }
    
    [self.player pausePlay];
}

- (void)resume {
    if (self.isPlaying_beforeScroll) {
        [self.player resumePlay];
    }
}

- (void)destoryPlayer {
    self.scrollView.delegate = nil;
    [self.player removeVideo];
}

#pragma mark - Private Methods
- (void)playVideoFrom:(SVVideoControlView *)fromView {
    // 移除原来的播放
    [self.player removeVideo];
    
    // 取消原来视图的代理
    self.currentPlayView.delegate = nil;
    
    // 切换播放视图
    self.currentPlayId    = fromView.model.videoid;
    self.currentPlayView  = fromView;
    self.currentPlayIndex = [self indexOfModel:fromView.model];
    
    NSLog(@"当前播放索引====%zd", self.currentPlayIndex);

    // 设置新视图的代理
    self.currentPlayView.delegate = self;
    
    // 重新播放
    @weakify(self);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        @strongify(self);
        [self.player playVideoWithView:fromView.coverImgView url:fromView.model.play_url videoid:fromView.model.videoid];
    });
}

// 获取当前播放内容的索引
- (NSInteger)indexOfModel:(ShortVideoModel *)model {
    __block NSInteger index = 0;
    [self.videos enumerateObjectsUsingBlock:^(ShortVideoModel *obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (model.videoid == obj.videoid) {
            index = idx;
        }
    }];
    return index;
}

- (void)submitComment{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    NSString *content = self.inputTextKeyField.text;
    if (content.length > 50) {
        [MBProgressHUD showTipMessageInView:@"超出最大字数限制"];
        return;
    }
    NSDictionary *param = @{@"videoid":@(self.currentPlayId),
                            @"content":content
    };
    GKBallLoadingView *loadingView = [GKBallLoadingView loadingViewInView:self];
    [loadingView startLoading];
    [CommonManager POST:@"shortvideo/setComment" parameters:param success:^(id responseObject) {
        [loadingView stopLoading];
        [loadingView removeFromSuperview];
        if (RESP_SUCCESS(responseObject)) {
            self.currentPlayView.model.comment_count ++;
            [self.ctrView setModel:self.ctrView.model];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [loadingView stopLoading];
        [loadingView removeFromSuperview];
        RESP_FAILURE;
    }];
}

- (void)inputBtnClick{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    self.keyboardToolBarBackShadow.hidden = NO;
    self.inputTextKeyField.placeholder = @"说点什么吧...";
    [self.inputTextKeyField becomeFirstResponder];
}

- (void)emojiBtnClick:(UIButton *)sender{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    [self.inputTextKeyField resignFirstResponder];
    [UIView animateWithDuration:0.3 animations:^{
        self.keyboardToolBar.transform = CGAffineTransformMakeTranslation(0, -EmojiHeight-self.keyboardToolBar.height-kBottomSafeHeight);
        self.emojiView.transform = CGAffineTransformMakeTranslation(0, -EmojiHeight-kBottomSafeHeight);
    } completion:^(BOOL finished) {
        self.keyboardToolBarBackShadow.hidden = NO;
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSString *content = self.inputTextKeyField.text;
    if (content.length > 50) {
        [MBProgressHUD showTipMessageInView:@"超出最大字数限制"];
        return NO;
    }
    [self.inputTextKeyField resignFirstResponder];
    [self submitComment];
    return YES;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.currentPlayIndex == 0 && scrollView.contentOffset.y < 0) {
        self.scrollView.contentOffset = CGPointZero;
    }
    
    // slier处理
    if (scrollView.contentOffset.y == 0 || scrollView.contentOffset.y == KScreenHeight - kTabBarHeight || scrollView.contentOffset.y == 2 * KScreenHeight - kTabBarHeight) {
        if ([self.delegate respondsToSelector:@selector(videoView:didScrollIsCritical:)]) {
            [self.delegate videoView:self didScrollIsCritical:YES];
        }
    }else {
        if ([self.delegate respondsToSelector:@selector(videoView:didScrollIsCritical:)]) {
            [self.delegate videoView:self didScrollIsCritical:NO];
        }
    }
    
    // 小于等于三个，不用处理
    if (self.videos.count <= 3) return;
    
    // 上滑到第一个
    if (self.index == 0 && scrollView.contentOffset.y <= KScreenHeight - kTabBarHeight) {
        return;
    }
    // 下滑到最后一个
    if (self.index > 0 && self.index == self.videos.count - 1 && scrollView.contentOffset.y > KScreenHeight - kTabBarHeight) {
        return;
    }
    
    // 判断是从中间视图上滑还是下滑
    if (scrollView.contentOffset.y >= 2 * (KScreenHeight - kTabBarHeight)) {  // 上滑
        NSLog(@"上滑");
        [self.player removeVideo];  // 在这里移除播放，解决闪动的bug
        if (self.index == 0) {
            self.index += 2;
            
            scrollView.contentOffset = CGPointMake(0, KScreenHeight - kTabBarHeight);
            
            self.topView.model = self.ctrView.model;
            self.ctrView.model = self.btmView.model;
            
        }else {
            self.index += 1;
            
            if (self.index == self.videos.count - 1) {
                self.ctrView.model = self.videos[self.index - 1];
            }else {
                scrollView.contentOffset = CGPointMake(0, KScreenHeight - kTabBarHeight);
                
                self.topView.model = self.ctrView.model;
                self.ctrView.model = self.btmView.model;
            }
        }
        if (self.index < self.videos.count - 1 && self.videos.count >= 3) {
            self.btmView.model = self.videos[self.index + 1];
        }
    }else if (scrollView.contentOffset.y <= 0) { // 下滑
        [self.player removeVideo];  // 在这里移除播放，解决闪动的bug
        if (self.index == 1) {
            self.topView.model = self.videos[self.index - 1];
            self.ctrView.model = self.videos[self.index];
            self.btmView.model = self.videos[self.index + 1];
            self.index -= 1;
        }else {
            if (self.index == self.videos.count - 1) {
                self.index -= 2;
            }else {
                self.index -= 1;
            }
            scrollView.contentOffset = CGPointMake(0, KScreenHeight - kTabBarHeight);
            
            self.btmView.model = self.ctrView.model;
            self.ctrView.model = self.topView.model;
            
            if (self.index > 0) {
                self.topView.model = self.videos[self.index - 1];
            }
        }
    }
}

// 结束滚动后开始播放
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y == 0) {
        if (self.currentPlayId == self.topView.model.videoid) return;
        [self playVideoFrom:self.topView];
    }else if (scrollView.contentOffset.y == KScreenHeight - kTabBarHeight) {
        if (self.currentPlayId == self.ctrView.model.videoid) return;
        [self playVideoFrom:self.ctrView];
    }else if (scrollView.contentOffset.y == 2 * (KScreenHeight - kTabBarHeight)) {
        if (self.currentPlayId == self.btmView.model.videoid) return;
        [self playVideoFrom:self.btmView];
    }
}

- (void)refreshMore {
    if (self.isRefreshingMore || !self.isMoreData) return;
    self.isRefreshingMore = YES;
    
    [self.vc refreshMoreListWithSuccess:^(NSArray * _Nonnull list) {
        self.isRefreshingMore = NO;
        if (list) {
            [self.videos addObjectsFromArray:list];
        }
        
        if (self.scrollView.contentOffset.y > 2 * (KScreenHeight - kTabBarHeight)) {
            self.scrollView.contentOffset = CGPointMake(0, 2 * (KScreenHeight - kTabBarHeight));
        }
    } failure:^(NSError * _Nonnull error) {
        NSLog(@"%@", error);
        self.isRefreshingMore = NO;
    }];
}

#pragma mark - Gesture
- (void)handlePanGesture:(SVPanGestureRecognizer *)panGesture {
    if (self.currentPlayIndex == 0) {
        CGPoint location = [panGesture locationInView:panGesture.view];
        
        switch (panGesture.state) {
            case UIGestureRecognizerStateBegan: {
                self.startLocationY = location.y;
            }
                break;
            case UIGestureRecognizerStateChanged: {
                if (panGesture.direction == SVPanGestureRecognizerDirectionVertical) {
                    // 这里取整是解决上滑时可能出现的distance > 0的情况
                    CGFloat distance = ceil(location.y) - ceil(self.startLocationY);
                    if (distance > 0) { // 只要distance>0且没松手 就认为是下滑
                        self.scrollView.panGestureRecognizer.enabled = NO;
                    }
                    
                    if (self.scrollView.panGestureRecognizer.enabled == NO) {
                        if ([self.delegate respondsToSelector:@selector(videoView:didPanWithDistance:isEnd:)]) {
                            [self.delegate videoView:self didPanWithDistance:distance isEnd:NO];
                        }
                    }
                }
            }
                break;
            case UIGestureRecognizerStateFailed:
            case UIGestureRecognizerStateCancelled:
            case UIGestureRecognizerStateEnded: {
                if (self.scrollView.panGestureRecognizer.enabled == NO) {
                    CGFloat distance = location.y - self.startLocationY;
                    if ([self.delegate respondsToSelector:@selector(videoView:didPanWithDistance:isEnd:)]) {
                        [self.delegate videoView:self didPanWithDistance:distance isEnd:YES];
                    }
                    
                    self.scrollView.panGestureRecognizer.enabled = YES;
                }
            }
                break;
                
            default:
                break;
        }
        
        [panGesture setTranslation:CGPointZero inView:panGesture.view];
    }
}

// 允许多个手势响应
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}


/** 判断手势方向  */
- (BOOL)commitTranslation:(CGPoint)translation {
    CGFloat absX = fabs(translation.x);
    CGFloat absY = fabs(translation.y);
    // 设置滑动有效距离
    if (MAX(absX, absY) < 5)
        return NO;
    if (absX > absY ) {
        if (translation.x<0) {//向左滑动
            return NO;
        }else{//向右滑动
            return NO;
        }
    } else if (absY > absX) {
        if (translation.y<0) {//向上滑动
            return NO;
        }else{ //向下滑动
            return YES;
        }
    }
    return NO;
}

#pragma mark - SVVideoPlayerDelegate
- (void)player:(SVVideoPlayer *)player statusChanged:(SVVideoPlayerStatus)status {
    switch (status) {
        case SVVideoPlayerStatusUnload:   // 未加载
            
            break;
        case SVVideoPlayerStatusPrepared:   // 准备播放
            [self.currentPlayView startLoading];
            break;
        case SVVideoPlayerStatusLoading: {     // 加载中
            [self.currentPlayView hidePlayBtn];
        }
            break;
        case SVVideoPlayerStatusPlaying: {    // 播放中
            [self.currentPlayView stopLoading];
            [self.currentPlayView hidePlayBtn];
        }
            break;
        case SVVideoPlayerStatusPaused: {     // 暂停
            [self.currentPlayView stopLoading];
            [self.currentPlayView showPlayBtn];
        }
            break;
        case SVVideoPlayerStatusEnded: {   // 播放结束
            // 重新开始播放
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.player resetPlay];
            });
        }
            break;
        case SVVideoPlayerStatusError:   // 错误
            
            break;
            
        default:
            break;
    }
}

- (void)player:(SVVideoPlayer *)player currentTime:(float)currentTime totalTime:(float)totalTime progress:(float)progress {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.currentPlayView setProgress:progress];
    });
}

- (void)openVipBtnClick{
    [self.viewController.navigationController pushViewController:[[VipCenterViewController alloc]init] animated:YES];
}

#pragma mark - SVVideoControlViewDelegate
- (void)controlViewDidClickSelf:(SVVideoControlView *)controlView {
    if (self.player.isPlaying) {
        [self.player pausePlay];
    }else {
        [self.player resumePlay];
    }
}

- (void)controlView:(SVVideoControlView *)controlView touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
}

- (void)controlViewDidClickIcon:(SVVideoControlView *)controlView {
    if ([self.delegate respondsToSelector:@selector(videoView:didClickIcon:)]) {
        [self.delegate videoView:self didClickIcon:controlView.model];
    }
}

- (void)controlViewDidClickAttent:(SVVideoControlView *)controlView {
    if ([self.delegate respondsToSelector:@selector(videoView:didClickAttent:)]) {
        [self.delegate videoView:self didClickAttent:controlView.model];
    }
}

- (void)controlViewDidClickPriase:(SVVideoControlView *)controlView {
    if ([self.delegate respondsToSelector:@selector(videoView:didClickPraise:)]) {
        [self.delegate videoView:self didClickPraise:controlView.model];
    }
}

- (void)controlViewDidClickComment:(SVVideoControlView *)controlView {
    if ([self.delegate respondsToSelector:@selector(videoView:didClickComment:)]) {
        [self.delegate videoView:self didClickComment:controlView.model];
    }
}

- (void)controlViewDidClickShare:(SVVideoControlView *)controlView {
    if ([self.delegate respondsToSelector:@selector(videoView:didClickShare:)]) {
        [self.delegate videoView:self didClickShare:controlView.model];
    }
}

#pragma mark - Emoji 代理
- (void)sendimage:(NSString *)str {
    if ([str isEqual:@"msg_del"]) {
        [self.inputTextKeyField deleteBackward];
    }else {
        [self.inputTextKeyField insertText:str];
    }
}
- (void)clickSendEmojiBtn {
    NSString *content = self.inputTextKeyField.text;
    if (content.length > 50) {
        [MBProgressHUD showTipMessageInView:@"超出最大字数限制"];
        return;
    }
    [UIView animateWithDuration:0.3 animations:^{
        self.keyboardToolBar.transform = CGAffineTransformIdentity;
        self.emojiView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        self->_keyboardToolBarBackShadow.hidden = YES;
    }];
    [self submitComment];
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [UIScrollView new];
        _scrollView.pagingEnabled = YES;
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.delegate = self;
        _scrollView.scrollsToTop = NO;
        
        [_scrollView addSubview:self.topView];
        [_scrollView addSubview:self.ctrView];
        [_scrollView addSubview:self.btmView];
        _scrollView.contentSize = CGSizeMake(0, (KScreenHeight - kTabBarHeight )* 3);
        
        if (@available(iOS 11.0, *)) {
            _scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        }
    }
    return _scrollView;
}

- (SVVideoControlView *)topView {
    if (!_topView) {
        _topView = [SVVideoControlView new];
        _topView.hidden = YES;
    }
    return _topView;
}

- (SVVideoControlView *)ctrView {
    if (!_ctrView) {
        _ctrView = [SVVideoControlView new];
        _ctrView.hidden = YES;
    }
    return _ctrView;
}

- (SVVideoControlView *)btmView {
    if (!_btmView) {
        _btmView = [SVVideoControlView new];
        _btmView.hidden = YES;
    }
    return _btmView;
}

- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [UIButton new];
        [_backBtn setImage:[UIImage imageNamed:@"ic_back_white"] forState:UIControlStateNormal];
    }
    return _backBtn;
}

- (NSMutableArray *)videos {
    if (!_videos) {
        _videos = [NSMutableArray new];
    }
    return _videos;
}

- (SVVideoPlayer *)player {
    if (!_player) {
        _player = [SVVideoPlayer new];
        _player.delegate = self;
    }
    return _player;
}

- (SVPanGestureRecognizer *)panGesture {
    if (!_panGesture) {
        _panGesture = [[SVPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
        _panGesture.delegate = self;
        _panGesture.direction = SVPanGestureRecognizerDirectionVertical;
    }
    return _panGesture;
}

- (NSMutableDictionary *)userIndexes{
    if (!_userIndexes) {
        _userIndexes = [NSMutableDictionary dictionary];
    }
    return _userIndexes;
}

- (UIView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, KScreenHeight - kTabBarHeight, KScreenWidth, kTabBarHeight)];
        _bottomView.backgroundColor = CLineColor;
        
        _emojiBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _emojiBtn.frame = CGRectMake(_bottomView.width - 15 - 22, (kTabBarHeight-kBottomSafeHeight-22)/2, 22, 22);
        [_emojiBtn setImage:IMAGE_NAMED(@"ic_sv_emoji") forState:UIControlStateNormal];
        [_emojiBtn addTarget:self action:@selector(emojiBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_bottomView addSubview:_emojiBtn];
        
        _inputBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _inputBtn.frame = CGRectMake(15, (kTabBarHeight-kBottomSafeHeight-32)/2, _emojiBtn.mj_x - 30, 32);
        [_inputBtn setTitle:@"说点什么吧..." forState:UIControlStateNormal];
        [_inputBtn setTitleColor:CFontColorLightGray forState:UIControlStateNormal];
        [_inputBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        _inputBtn.titleLabel.font = FFont14;
        [_inputBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
        [_inputBtn setBackgroundColor:[UIColor whiteColor]];
        _inputBtn.layer.cornerRadius = 16;
        [_inputBtn addTarget:self action:@selector(inputBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_bottomView addSubview:_inputBtn];
        
    }
    return _bottomView;
}

- (UIView *)keyboardToolBar{
    if (!_keyboardToolBar) {
        //输入框
        _inputTextKeyField = [[UITextField alloc]initWithFrame:CGRectMake(15,10,KScreenWidth- 15 - 50, 32)];
        _inputTextKeyField.returnKeyType = UIReturnKeySend;
        _inputTextKeyField.delegate = self;
        _inputTextKeyField.textColor = [UIColor colorWithHexString:@"555555"];
        _inputTextKeyField.borderStyle = UITextBorderStyleNone;
        _inputTextKeyField.placeholder = @"说点什么吧...";
        _inputTextKeyField.backgroundColor = [UIColor whiteColor];
        _inputTextKeyField.layer.cornerRadius = 16;
        _inputTextKeyField.layer.masksToBounds = YES;
        UIView *fieldLeft = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 15, 30)];
        fieldLeft.backgroundColor = [UIColor clearColor];
        _inputTextKeyField.leftView = fieldLeft;
        _inputTextKeyField.leftViewMode = UITextFieldViewModeAlways;
        _inputTextKeyField.font = [UIFont systemFontOfSize:15];
        
        //emoji按钮
        _toolbarEmojiBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_toolbarEmojiBtn setImage:[UIImage imageNamed:@"ic_sv_emoji"] forState:UIControlStateNormal];
        _toolbarEmojiBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _toolbarEmojiBtn.layer.masksToBounds = YES;
        _toolbarEmojiBtn.layer.cornerRadius = 5;
        _toolbarEmojiBtn.selected = NO;
        [_toolbarEmojiBtn addTarget:self action:@selector(emojiBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        _toolbarEmojiBtn.frame = CGRectMake(KScreenWidth-55,7,50,40);
        
        //tool绑定键盘
        _keyboardToolBar = [[UIView alloc]initWithFrame:CGRectMake(0,KScreenHeight, KScreenWidth, 52)];
        _keyboardToolBar.backgroundColor = CLineColor;
        UIView *tooBgv = [[UIView alloc]initWithFrame:_keyboardToolBar.bounds];
        tooBgv.backgroundColor = CLineColor;
        tooBgv.alpha = 1;
        [_keyboardToolBar addSubview:tooBgv];
        
        [_keyboardToolBar addSubview:_toolbarEmojiBtn];
        [_keyboardToolBar addSubview:_inputTextKeyField];
    }
    return _keyboardToolBar;
}

- (UIView *)keyboardToolBarBackShadow {
    if (!_keyboardToolBarBackShadow) {
        _keyboardToolBarBackShadow = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight)];
        _keyboardToolBarBackShadow.backgroundColor = [UIColor clearColor];
        _keyboardToolBarBackShadow.hidden = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
            [self.inputTextKeyField resignFirstResponder];
            [UIView animateWithDuration:0.3 animations:^{
                self.keyboardToolBar.transform = CGAffineTransformIdentity;
                self.emojiView.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished) {
                self->_keyboardToolBarBackShadow.hidden = YES;
            }];
        }];
        [_keyboardToolBarBackShadow addGestureRecognizer:tap];
    }
    return _keyboardToolBarBackShadow;
}

- (twEmojiView *)emojiView{
    if (!_emojiView) {
        _emojiView = [[twEmojiView alloc]initWithFrame:CGRectMake(0, KScreenHeight, KScreenWidth, EmojiHeight)];
        _emojiView.delegate = self;
    }
    return _emojiView;
}

- (void)keyboardWillAppear:(NSNotification *)notification{
    
    NSDictionary *info = [notification userInfo];
    
    //取出动画时长
    CGFloat animationDuration = [[info valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //取出键盘位置大小信息
    CGRect keyboardBounds = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    //rect转换
    //    CGRect keyboardRect = [self convertRect:keyboardBounds toView:nil];
    
    //记录Y轴变化
    CGFloat keyboardHeight = keyboardBounds.size.height;
    
    //上移动画options
    UIViewAnimationOptions options = (UIViewAnimationOptions)[[info valueForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue] << 16;
    
    
    self.keyboardToolBarBackShadow.hidden = NO;
    [UIView animateWithDuration:animationDuration delay:0 options:options animations:^{
        self.keyboardToolBar.transform = CGAffineTransformMakeTranslation(0, -keyboardHeight-self.keyboardToolBar.height);
    } completion:nil];
}


- (void)keyboardWillDisappear:(NSNotification *)notification{
    NSDictionary *info = [notification userInfo];
    
    //取出动画时长
    CGFloat animationDuration = [[info valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //下移动画options
    UIViewAnimationOptions options = (UIViewAnimationOptions)[[info valueForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue] << 16;
    
    self.keyboardToolBarBackShadow.hidden = YES;
    //恢复动画
    [UIView animateWithDuration:animationDuration delay:0 options:options animations:^{
        self.keyboardToolBar.transform = CGAffineTransformIdentity;
        self.emojiView.transform = CGAffineTransformIdentity;
    } completion:nil];
    
}

@end
