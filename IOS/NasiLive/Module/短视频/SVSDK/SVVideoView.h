//
//  SVVideoView.h
//  SVVideo
//
//  Created by yun11 on 2018/9/23.
//  Copyright © 2018 yun11. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SVVideoControlView.h"

NS_ASSUME_NONNULL_BEGIN

@class SVVideoView;

@protocol SVVcProtocol <NSObject>

- (void)refreshMoreListWithSuccess:(void (^)(NSArray * _Nonnull))success failure:(void (^)(NSError * _Nonnull))failure;

@end

@protocol SVVideoViewDelegate <NSObject>

@optional

- (void)videoView:(SVVideoView *)videoView didClickIcon:(ShortVideoModel *)videoModel;
- (void)videoView:(SVVideoView *)videoView didClickAttent:(ShortVideoModel *)videoModel;
- (void)videoView:(SVVideoView *)videoView didClickPraise:(ShortVideoModel *)videoModel;
- (void)videoView:(SVVideoView *)videoView didClickComment:(ShortVideoModel *)videoModel;
- (void)videoView:(SVVideoView *)videoView didClickShare:(ShortVideoModel *)videoModel;
- (void)videoView:(SVVideoView *)videoView didScrollIsCritical:(BOOL)isCritical;
- (void)videoView:(SVVideoView *)videoView didPanWithDistance:(CGFloat)distance isEnd:(BOOL)isEnd;
- (void)videoView:(SVVideoView *)videoView didBottomBarClick:(int)type;

@end

@interface SVVideoView : UIView

@property (nonatomic, weak) id<SVVideoViewDelegate>     delegate;

@property (nonatomic, strong) NSMutableArray            *videos;

@property (assign, nonatomic) int                       perPageSize;

@property (nonatomic, strong) UIButton                  *backBtn;

// 当前播放内容的视图
@property (nonatomic, strong) SVVideoControlView        *currentPlayView;

// 当前播放内容的索引
@property (nonatomic, assign) NSInteger                 currentPlayIndex;

- (instancetype)initWithVC:(UIViewController<SVVcProtocol> *)vc isPushed:(BOOL)isPushed;

- (void)setModels:(NSArray *)models index:(NSInteger)index;

- (void)pause;
- (void)resume;
- (void)destoryPlayer;

- (void)updateUserModel:(int)userid attent:(BOOL)isattent;
- (void)updateCurrentVideoCommentCount:(int)count;

@end

NS_ASSUME_NONNULL_END
