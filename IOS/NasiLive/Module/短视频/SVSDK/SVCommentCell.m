//
//  SVCommentCell.m
//  Meet1V1
//
//  Created by yun on 2020/1/13.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "SVCommentCell.h"

#import "SVCommentModel.h"

@interface SVCommentCell()

@property (weak, nonatomic) IBOutlet UIImageView *userIconImgView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *ageGenderBtn;
@property (weak, nonatomic) IBOutlet UIImageView *levelImgView;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *replyCountLabel;
@end

@implementation SVCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(SVCommentModel *)model{
    _model = model;
    
    [self.userIconImgView sd_setImageWithURL:[NSURL URLWithString:model.user.avatar] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    self.nickNameLabel.text = model.user.nick_name;
    [self.ageGenderBtn setBackgroundColor:model.user.profile.gender?[UIColor colorWithHexString:@"6FCAFF"]:[UIColor colorWithHexString:@"FF9CE4"]];
    self.ageGenderBtn.selected = model.user.profile.gender;
    [self.ageGenderBtn setTitle:[NSString stringWithFormat:@"%d",model.user.profile.age] forState:UIControlStateNormal];
    [self.levelImgView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"ic_user_level_%d",model.user.user_level]]];
    self.contentLabel.attributedText = model.attrContent;
    self.timeLabel.text = [CommonManager formateBeautyDate:model.create_time];
    
    if (model.reply_count > 0) {
        self.replyCountLabel.hidden = NO;
        self.replyCountLabel.text = [NSString stringWithFormat:@"  %d条回复  ", model.reply_count];
    }else{
        self.replyCountLabel.hidden = YES;
    }
    
    self.likeCountLabel.text = [NSString stringWithFormat:@"%d", model.like_count];
    self.likeBtn.selected = model.liked;
}

- (IBAction)likeBtnClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(commentLikeClick:)]) {
        [self.delegate commentLikeClick:self.model];
    }
}

- (IBAction)replyBtnClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(replyClick:)]) {
        [self.delegate replyClick:self.model];
    }
}

- (void)setIsShowReplyCount:(BOOL)isShowReplyCount{
    _isShowReplyCount = isShowReplyCount;
    self.replyCountLabel.hidden = !isShowReplyCount;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
