//
//  SVCommentView.m
//  SVVideo
//
//  Created by yun11 on 2019/5/1.
//  Copyright © 2019 yun11. All rights reserved.
//

#import "SVCommentView.h"
#import "SVCommentReplyView.h"
#import "GKBallLoadingView.h"

#import "SVCommentModel.h"
#import "SVCommentCell.h"

#import "twEmojiView.h"

#define EmojiHeight 200
#define pagesize 20

@interface SVCommentView()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, twEmojiViewDelegate, SVCommentCellDelegate>{
    BOOL                                            _isInLikeRequesting;
}

@property (nonatomic, strong) UIView                *backShadowView;
@property (strong, nonatomic) UIView                *containerView;
@property (nonatomic, strong) UIView                *topView;
@property (nonatomic, strong) UILabel               *countLabel;
@property (nonatomic, strong) UIButton              *closeBtn;
@property (strong, nonatomic) UIView                *bottomView;

@property (strong, nonatomic) UIView                *keyboardToolBarBackShadow;
@property (strong, nonatomic) UIView                *keyboardToolBar;
@property (strong, nonatomic) UITextField           *inputTextKeyField;
@property (strong, nonatomic) UIButton              *inputBtn;
@property (strong, nonatomic) UIButton              *emojiBtn;
@property (strong, nonatomic) UIButton              *toolbarEmojiBtn;

@property (strong, nonatomic) twEmojiView           *emojiView;

@property (nonatomic, strong) UITableView           *tableView;

@property (nonatomic, strong) NSMutableArray        *commentArray;

@property (strong, nonatomic) SVCommentReplyView    *replayView;


@end

@implementation SVCommentView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        
        self.backgroundColor = [UIColor clearColor];
        
        self.commentArray = [NSMutableArray array];
        
        [self addSubview:self.backShadowView];
        
        [self addSubview:self.containerView];
        
        [self.containerView addSubview:self.topView];
        [self.containerView addSubview:self.tableView];
        [self.containerView addSubview:self.bottomView];
        
        [self addSubview:self.keyboardToolBarBackShadow];
        [self addSubview:self.keyboardToolBar];
        
        [self addSubview:self.emojiView];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillDisappear:) name:UIKeyboardWillHideNotification object:nil];
        
    }
    return self;
}

- (void)setVideoModel:(ShortVideoModel *)videoModel{
    if (_videoModel == videoModel) {
        return;
    }
    _videoModel = videoModel;
    self.countLabel.text = [NSString stringWithFormat:@"%d条评论", videoModel.comment_count];
    [self.commentArray removeAllObjects];
    [self.tableView reloadData];
    [self requestData:YES];
    
    if (videoModel.comment_count == 0) {
        [self.inputTextKeyField becomeFirstResponder];
    }
}

- (void)requestData:(BOOL)firstRequest{
    GKBallLoadingView *loadingView;
    if (firstRequest) {
        loadingView = [GKBallLoadingView loadingViewInView:self.tableView];
        [loadingView startLoading];
    }
    
    long lastid = 9999999999;
    if (self.commentArray.count > 0) {
        SVCommentModel *lastModel = self.commentArray[self.commentArray.count - 1];
        lastid = lastModel.commentid;
    }
    
    [CommonManager POST:@"shortvideo/getComments" parameters:@{@"videoid":@(self.videoModel.videoid),@"lastid":@(lastid),@"size":@(pagesize)} success:^(id responseObject) {
        if (firstRequest) {
            [loadingView stopLoading];
            [loadingView removeFromSuperview];
        }
        if (RESP_SUCCESS(responseObject)) {
            NSArray *dataArr = [NSArray yy_modelArrayWithClass:[SVCommentModel class] json:responseObject[@"data"]];
            [self.commentArray addObjectsFromArray:dataArr];
            [self.tableView reloadData];
            
            if (dataArr.count < pagesize ) {
                [self.tableView.mj_footer removeFromSuperview];
                self.tableView.mj_footer = nil;
            }else{
                MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(requestData:)];
                self.tableView.mj_footer = footer;
            }
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
        
    } failure:^(NSError *error) {
        [loadingView stopLoading];
        [loadingView removeFromSuperview];
        RESP_FAILURE;
    }];
}

- (void)submitComment{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    NSString *content = self.inputTextKeyField.text;
    if (content.length > 50) {
        [MBProgressHUD showTipMessageInView:@"超出最大字数限制"];
        return;
    }
    NSDictionary *param = @{@"videoid":@(self.videoModel.videoid),
                            @"content":content
    };
    GKBallLoadingView *loadingView = [GKBallLoadingView loadingViewInView:self.tableView];
    [loadingView startLoading];
    [CommonManager POST:@"shortvideo/setComment" parameters:param success:^(id responseObject) {
        [loadingView stopLoading];
        [loadingView removeFromSuperview];
        if (RESP_SUCCESS(responseObject)) {
            self.videoModel.comment_count ++;
            self.countLabel.text = [NSString stringWithFormat:@"%d条评论", self.videoModel.comment_count];
            if (self.CommentCountBlock) {
                self.CommentCountBlock(self.videoModel.comment_count);
            }
            
            SVCommentModel *model = [SVCommentModel yy_modelWithDictionary:responseObject[@"data"]];
            [self.commentArray insertObject:model atIndex:0];
            [self.tableView reloadData];
            [self.tableView scrollToTopAnimated:YES];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [loadingView stopLoading];
        [loadingView removeFromSuperview];
        RESP_FAILURE;
    }];
}

- (void)inputBtnClick{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    self.keyboardToolBarBackShadow.hidden = NO;
    self.inputTextKeyField.placeholder = @"说点什么吧...";
    [self.inputTextKeyField becomeFirstResponder];
}

- (void)emojiBtnClick:(UIButton *)sender{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    [self.inputTextKeyField resignFirstResponder];
    [UIView animateWithDuration:0.3 animations:^{
        self.keyboardToolBar.transform = CGAffineTransformMakeTranslation(0, -EmojiHeight-self.keyboardToolBar.height-kBottomSafeHeight);
        self.emojiView.transform = CGAffineTransformMakeTranslation(0, -EmojiHeight-kBottomSafeHeight);
    } completion:^(BOOL finished) {
        self.keyboardToolBarBackShadow.hidden = NO;
    }];
}

- (void)closeBtnClick{
    self.keyboardToolBarBackShadow.hidden = YES;
    [UIView animateWithDuration:0.3 animations:^{
        self.mj_y = self.height;
        self.keyboardToolBar.transform = CGAffineTransformIdentity;
        self.emojiView.transform = CGAffineTransformIdentity;
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    NSString *content = self.inputTextKeyField.text;
    if (content.length > 50) {
        [MBProgressHUD showTipMessageInView:@"超出最大字数限制"];
        return NO;
    }
    [self.inputTextKeyField resignFirstResponder];
    [self submitComment];
    return YES;
}

#pragma mark - <UITableViewDataSource, UITableViewDelegate>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.commentArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SVCommentCell *cell = [SVCommentCell cellWithTableView:tableView indexPath:indexPath];
    cell.model = self.commentArray[indexPath.row];
    cell.delegate = self;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SVCommentModel *model = self.commentArray[indexPath.row];
    self.replayView.commentModel = model;
    [UIView animateWithDuration:0.3 animations:^{
        self.replayView.mj_x = 0;
    }];
}

#pragma mark - SVCommentCellDelegate
- (void)replyClick:(SVCommentModel *)model{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    self.replayView.commentModel = model;
    [UIView animateWithDuration:0.3 animations:^{
        self.replayView.mj_x = 0;
    }];
}

- (void)commentLikeClick:(SVCommentModel *)model{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    
    if (_isInLikeRequesting) return;
    if (model.liked) return;
    
    _isInLikeRequesting = YES;
    [CommonManager POST:@"shortvideo/likeComment" parameters:@{@"commentid":@(model.commentid)} success:^(id responseObject) {
        self->_isInLikeRequesting = NO;
        if (RESP_SUCCESS(responseObject)) {
            model.liked = !model.liked;
            model.like_count = [responseObject[@"data"][@"like_count"] intValue];
            [self.tableView reloadData];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        self->_isInLikeRequesting = NO;
        RESP_FAILURE;
    }];
}

#pragma mark - Emoji 代理
- (void)sendimage:(NSString *)str {
    if ([str isEqual:@"msg_del"]) {
        [self.inputTextKeyField deleteBackward];
    }else {
        [self.inputTextKeyField insertText:str];
    }
}
- (void)clickSendEmojiBtn {
    NSString *content = self.inputTextKeyField.text;
    if (content.length > 50) {
        [MBProgressHUD showTipMessageInView:@"超出最大字数限制"];
        return;
    }
    [UIView animateWithDuration:0.3 animations:^{
        self.keyboardToolBar.transform = CGAffineTransformIdentity;
        self.emojiView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        self->_keyboardToolBarBackShadow.hidden = YES;
    }];
    [self submitComment];
}

#pragma mark - 懒加载
- (UIView *)backShadowView {
    if (!_backShadowView) {
        _backShadowView = [[UIView alloc]initWithFrame:self.bounds];
        _backShadowView.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
            [self closeBtnClick];
        }];
        [_backShadowView addGestureRecognizer:tap];
    }
    return _backShadowView;
}

- (UIView *)containerView{
    if (!_containerView) {
        _containerView = [[UIView alloc]initWithFrame:CGRectMake(0, self.height/3, self.width, self.height/3*2)];
        _containerView.backgroundColor = [UIColor whiteColor];
    }
    return _containerView;
}

- (UIView *)topView {
    if (!_topView) {
        _topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.containerView.width, 50.0f)];
        _topView.backgroundColor = [UIColor whiteColor];
        
        _countLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.containerView.width, 50)];
        _countLabel.font = [UIFont systemFontOfSize:17.0f];
        _countLabel.textColor = [UIColor blackColor];
        _countLabel.textAlignment = NSTextAlignmentCenter;
        [_topView addSubview:_countLabel];
        
        _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _closeBtn.frame = CGRectMake(self.containerView.width - 30 - 5, 10, 30, 30);
        [_closeBtn setImage:[UIImage imageNamed:@"ic_sv_close_black"] forState:UIControlStateNormal];
        [_closeBtn addTarget:self action:@selector(closeBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_topView addSubview:_closeBtn];
        
    }
    return _topView;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, self.topView.height, self.containerView.width, self.containerView.height - self.topView.height - self.bottomView.height) style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = [UIColor clearColor];
        
        [SVCommentCell registerWithTableView:_tableView];
        
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            // Fallback on earlier versions
        }
    }
    return _tableView;
}

- (UIView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, self.containerView.height - kTabBarHeight, self.containerView.width, kTabBarHeight)];
        _bottomView.backgroundColor = CLineColor;
        
        _emojiBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _emojiBtn.frame = CGRectMake(_bottomView.width - 15 - 22, (kTabBarHeight-kBottomSafeHeight-22)/2, 22, 22);
        [_emojiBtn setImage:IMAGE_NAMED(@"ic_sv_emoji") forState:UIControlStateNormal];
        [_emojiBtn addTarget:self action:@selector(emojiBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [_bottomView addSubview:_emojiBtn];
        
        _inputBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _inputBtn.frame = CGRectMake(15, (kTabBarHeight-kBottomSafeHeight-32)/2, _emojiBtn.mj_x - 30, 32);
        [_inputBtn setTitle:@"说点什么吧..." forState:UIControlStateNormal];
        [_inputBtn setTitleColor:CFontColorLightGray forState:UIControlStateNormal];
        [_inputBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        _inputBtn.titleLabel.font = FFont14;
        [_inputBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
        [_inputBtn setBackgroundColor:[UIColor whiteColor]];
        _inputBtn.layer.cornerRadius = 16;
        [_inputBtn addTarget:self action:@selector(inputBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [_bottomView addSubview:_inputBtn];
        
    }
    return _bottomView;
}

- (UIView *)keyboardToolBar{
    if (!_keyboardToolBar) {
        //输入框
        _inputTextKeyField = [[UITextField alloc]initWithFrame:CGRectMake(15,10,KScreenWidth- 15 - 50, 32)];
        _inputTextKeyField.returnKeyType = UIReturnKeySend;
        _inputTextKeyField.delegate = self;
        _inputTextKeyField.textColor = [UIColor colorWithHexString:@"555555"];
        _inputTextKeyField.borderStyle = UITextBorderStyleNone;
        _inputTextKeyField.placeholder = @"说点什么吧...";
        _inputTextKeyField.backgroundColor = [UIColor whiteColor];
        _inputTextKeyField.layer.cornerRadius = 16;
        _inputTextKeyField.layer.masksToBounds = YES;
        UIView *fieldLeft = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 15, 30)];
        fieldLeft.backgroundColor = [UIColor clearColor];
        _inputTextKeyField.leftView = fieldLeft;
        _inputTextKeyField.leftViewMode = UITextFieldViewModeAlways;
        _inputTextKeyField.font = [UIFont systemFontOfSize:15];
        
        //emoji按钮
        _toolbarEmojiBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_toolbarEmojiBtn setImage:[UIImage imageNamed:@"ic_sv_emoji"] forState:UIControlStateNormal];
        _toolbarEmojiBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        _toolbarEmojiBtn.layer.masksToBounds = YES;
        _toolbarEmojiBtn.layer.cornerRadius = 5;
        _toolbarEmojiBtn.selected = NO;
        [_toolbarEmojiBtn addTarget:self action:@selector(emojiBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        _toolbarEmojiBtn.frame = CGRectMake(KScreenWidth-55,7,50,40);
        
        //tool绑定键盘
        _keyboardToolBar = [[UIView alloc]initWithFrame:CGRectMake(0,self.height, KScreenWidth, 52)];
        _keyboardToolBar.backgroundColor = CLineColor;
        UIView *tooBgv = [[UIView alloc]initWithFrame:_keyboardToolBar.bounds];
        tooBgv.backgroundColor = CLineColor;
        tooBgv.alpha = 1;
        [_keyboardToolBar addSubview:tooBgv];
        
        [_keyboardToolBar addSubview:_toolbarEmojiBtn];
        [_keyboardToolBar addSubview:_inputTextKeyField];
    }
    return _keyboardToolBar;
}

- (UIView *)keyboardToolBarBackShadow {
    if (!_keyboardToolBarBackShadow) {
        _keyboardToolBarBackShadow = [[UIView alloc]initWithFrame:self.bounds];
        _keyboardToolBarBackShadow.backgroundColor = [UIColor clearColor];
        _keyboardToolBarBackShadow.hidden = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithActionBlock:^(id  _Nonnull sender) {
            [self.inputTextKeyField resignFirstResponder];
            [UIView animateWithDuration:0.3 animations:^{
                self.keyboardToolBar.transform = CGAffineTransformIdentity;
                self.emojiView.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished) {
                self->_keyboardToolBarBackShadow.hidden = YES;
            }];
        }];
        [_keyboardToolBarBackShadow addGestureRecognizer:tap];
    }
    return _keyboardToolBarBackShadow;
}

- (twEmojiView *)emojiView{
    if (!_emojiView) {
        _emojiView = [[twEmojiView alloc]initWithFrame:CGRectMake(0, self.height, self.width, EmojiHeight)];
        _emojiView.delegate = self;
    }
    return _emojiView;
}

- (SVCommentReplyView *)replayView{
    if (!_replayView) {
        _replayView = [[SVCommentReplyView alloc]initWithFrame:CGRectMake(KScreenWidth, 0, KScreenWidth, self.height)];
        [self addSubview:_replayView];
    }
    return _replayView;
}

- (void)keyboardWillAppear:(NSNotification *)notification{
    
    NSDictionary *info = [notification userInfo];
    
    //取出动画时长
    CGFloat animationDuration = [[info valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //取出键盘位置大小信息
    CGRect keyboardBounds = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    //rect转换
    //    CGRect keyboardRect = [self convertRect:keyboardBounds toView:nil];
    
    //记录Y轴变化
    CGFloat keyboardHeight = keyboardBounds.size.height;
    
    //上移动画options
    UIViewAnimationOptions options = (UIViewAnimationOptions)[[info valueForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue] << 16;
    
    
    self.keyboardToolBarBackShadow.hidden = NO;
    [UIView animateWithDuration:animationDuration delay:0 options:options animations:^{
        self.keyboardToolBar.transform = CGAffineTransformMakeTranslation(0, -keyboardHeight-self.keyboardToolBar.height);
    } completion:nil];
}


- (void)keyboardWillDisappear:(NSNotification *)notification{
    NSDictionary *info = [notification userInfo];
    
    //取出动画时长
    CGFloat animationDuration = [[info valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //下移动画options
    UIViewAnimationOptions options = (UIViewAnimationOptions)[[info valueForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue] << 16;
    
    self.keyboardToolBarBackShadow.hidden = YES;
    //恢复动画
    [UIView animateWithDuration:animationDuration delay:0 options:options animations:^{
        self.keyboardToolBar.transform = CGAffineTransformIdentity;
        self.emojiView.transform = CGAffineTransformIdentity;
    } completion:nil];
    
}

@end
