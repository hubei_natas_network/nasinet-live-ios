//
//  SVideoListCollectionViewCell.m
//  NasiLive
//
//  Created by yun11 on 2020/6/2.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "SVideoListCollectionViewCell.h"

@interface SVideoListCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@end

@implementation SVideoListCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(ShortVideoModel *)model{
    _model = model;
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:model.thumb_url] placeholderImage:[UIImage imageNamed:@"cover_loading"]];
    self.countLabel.text = [CommonManager formateCount:model.like_count];
}

@end
