//
//  SVideoListCollectionViewCell.h
//  NasiLive
//
//  Created by yun11 on 2020/6/2.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ShortVideoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SVideoListCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) ShortVideoModel *model;

@end

NS_ASSUME_NONNULL_END
