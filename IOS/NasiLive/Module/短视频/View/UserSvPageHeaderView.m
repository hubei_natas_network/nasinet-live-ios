//
//  UserSvPageHeaderView.m
//  NasiLive
//
//  Created by yun11 on 2020/6/2.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "UserSvPageHeaderView.h"

@interface UserSvPageHeaderView()

@property (nonatomic, assign) CGRect        bgImgFrame;
@property (nonatomic, strong) UIImageView   *bgImgView;

@property (nonatomic, strong) UIView        *contentView;
@property (strong, nonatomic) UIImageView   *iconImgView;
@property (strong, nonatomic) UIButton      *attentBtn;
@property (strong, nonatomic) UIButton      *chatBtn;
@property (strong, nonatomic) UILabel       *nickLabel;
@property (strong, nonatomic) UIImageView   *levelImgView;
@property (strong, nonatomic) UIButton      *ageBtn;
@property (strong, nonatomic) UILabel       *idLabel;
@property (strong, nonatomic) UILabel       *signatureLabel;
@property (strong, nonatomic) UILabel       *starCountLabel;
@property (strong, nonatomic) UILabel       *attentCountLabel;
@property (strong, nonatomic) UILabel       *fansCountLabel;

@end

@implementation UserSvPageHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.bgImgView];
        
        self.bgImgFrame = CGRectMake(0, 0, frame.size.width, kSvUserPageBgImgHeight);
        self.bgImgView.frame = self.bgImgFrame;
        
        [self addSubview:self.contentView];
        [self.contentView addSubview:self.iconImgView];
        [self.contentView addSubview:self.nickLabel];
        [self.contentView addSubview:self.attentBtn];
        [self.contentView addSubview:self.chatBtn];
        [self.contentView addSubview:self.ageBtn];
        [self.contentView addSubview:self.levelImgView];
        [self.contentView addSubview:self.idLabel];
        [self.contentView addSubview:self.signatureLabel];
        
        [self.contentView addSubview:self.starCountLabel];
        [self.contentView addSubview:self.attentCountLabel];
        [self.contentView addSubview:self.fansCountLabel];
        
        UILabel *starTitleLabel = [[UILabel alloc]init];
        starTitleLabel.font = [UIFont systemFontOfSize:14];
        starTitleLabel.text = @"获赞";
        starTitleLabel.textColor = CFontColorGray;
        [self.contentView addSubview:starTitleLabel];
        
        UILabel *attentTitleLabel = [[UILabel alloc]init];
        attentTitleLabel.font = [UIFont systemFontOfSize:14];
        attentTitleLabel.text = @"关注";
        attentTitleLabel.textColor = CFontColorGray;
        [self.contentView addSubview:attentTitleLabel];
        
        UILabel *fansTitleLabel = [[UILabel alloc]init];
        fansTitleLabel.font = [UIFont systemFontOfSize:14];
        fansTitleLabel.text = @"粉丝";
        fansTitleLabel.textColor = CFontColorGray;
        [self.contentView addSubview:fansTitleLabel];
        
        UIView *breakline = [[UIView alloc]init];
        breakline.backgroundColor = CLineColor;
        [self.contentView addSubview:breakline];
        
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self).insets(UIEdgeInsetsMake(kSvUserPageBgImgHeight, 0, 0, 0));
        }];
        
        [self.iconImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(9.0f);
            make.top.equalTo(self.contentView).offset(-18.0f);
            make.width.height.mas_equalTo(96.0f);
        }];
        
        [self.chatBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.iconImgView);
            make.right.offset(-15);
            make.height.mas_equalTo(36);
            make.width.mas_equalTo(45);
        }];
        
        [self.attentBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.iconImgView);
            make.left.equalTo(self.iconImgView.mas_right).offset(12);
            make.right.equalTo(self.chatBtn.mas_left).offset(-5);
            make.height.mas_equalTo(36);
        }];
        
        [self.nickLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.iconImgView.mas_bottom).offset(9);
            make.left.offset(15);
            make.height.mas_equalTo(20);
        }];
        
        [self.levelImgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.nickLabel);
            make.left.equalTo(self.nickLabel.mas_right).offset(12);
            make.height.mas_equalTo(13);
            make.width.mas_equalTo(22);
        }];
        
        [self.ageBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.nickLabel);
            make.left.equalTo(self.levelImgView.mas_right).offset(3.5f);
            make.height.mas_equalTo(13);
            make.width.mas_equalTo(22);
        }];
        
        [self.idLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.nickLabel.mas_bottom).offset(12);
            make.left.offset(16);
            make.height.mas_equalTo(12);
        }];
        
        [breakline mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(0.5f);
            make.top.equalTo(self.idLabel.mas_bottom).offset(12);
            make.left.offset(15);
            make.right.offset(-15);
        }];
        
        [self.signatureLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(breakline.mas_bottom).offset(12);
            make.left.offset(15);
            make.height.mas_equalTo(15);
        }];
        
        [self.starCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(15);
            make.top.equalTo(self.signatureLabel.mas_bottom).offset(20);
            make.height.mas_equalTo(15);
        }];
        [starTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.starCountLabel);
            make.left.mas_equalTo(self.starCountLabel.mas_right).offset(2.5);
        }];
        
        [self.attentCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(starTitleLabel.mas_right).offset(25);
            make.centerY.equalTo(self.starCountLabel);
            make.height.equalTo(self.starCountLabel);
        }];
        [attentTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.attentCountLabel);
            make.left.mas_equalTo(self.attentCountLabel.mas_right).offset(2.5);
        }];
        
        [self.fansCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(attentTitleLabel.mas_right).offset(25);
            make.centerY.equalTo(self.starCountLabel);
            make.height.equalTo(self.starCountLabel);
        }];
        [fansTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.fansCountLabel);
            make.left.mas_equalTo(self.fansCountLabel.mas_right).offset(2.5);
        }];
    }
    return self;
}

- (void)scrollViewDidScroll:(CGFloat)offsetY {
    CGRect frame = self.bgImgFrame;
    // 上下放大
    frame.size.height -= offsetY;
    frame.origin.y = offsetY;
    
    self.bgImgView.frame = frame;
}

- (void)setAuthor:(UserInfoModel *)author{
    _author = author;
    
    [self refreshUI];
}

- (void)refreshUI{
    
    [self.iconImgView sd_setImageWithURL:[NSURL URLWithString:self.author.avatar] placeholderImage:IMAGE_NAMED(@"ic_avatar")];
    
    self.nickLabel.text = self.author.nick_name;
    self.levelImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_user_level_%d",self.author.user_level]];
    [self.ageBtn setBackgroundColor:self.author.profile.gender?[UIColor colorWithHexString:@"6FCAFF"]:[UIColor colorWithHexString:@"FF9CE4"]];
    self.ageBtn.selected = self.author.profile.gender;
    [self.ageBtn setTitle:[NSString stringWithFormat:@"%d",self.author.profile.age] forState:UIControlStateNormal];
    
    self.idLabel.text = [NSString stringWithFormat:@"ID:%lld",self.author.userid];
    self.signatureLabel.text = self.author.profile.signature;
    
    if (userManager.curUserInfo.userid == self.author.userid) {
        self.attentBtn.hidden = YES;
        self.chatBtn.hidden = YES;
    }else{
        self.attentBtn.selected = self.author.isattent;
    }
    
    self.starCountLabel.text = [NSString stringWithFormat:@"%d",self.author.sv_star_count];
    self.attentCountLabel.text = [NSString stringWithFormat:@"%d",self.author.attent_count];
    self.fansCountLabel.text = [NSString stringWithFormat:@"%d",self.author.fans_count];
}

- (void)attentBtnClick{
    if ([self.delegate respondsToSelector:@selector(attentBtnClick:)]) {
        [self.delegate attentBtnClick:self.attentBtn];
    }
}

- (void)chatBtnClick{
    if ([self.delegate respondsToSelector:@selector(chatBtnClick)]) {
        [self.delegate chatBtnClick];
    }
}

#pragma mark - 懒加载
- (UIImageView *)bgImgView {
    if (!_bgImgView) {
        _bgImgView = [UIImageView new];
        _bgImgView.image = [UIImage imageNamed:@"bg_sv_page_header"];
        _bgImgView.contentMode = UIViewContentModeScaleAspectFill;
        _bgImgView.clipsToBounds = YES;
    }
    return _bgImgView;
}

- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [UIView new];
        _contentView.backgroundColor = [UIColor whiteColor];
    }
    return _contentView;
}

- (UIImageView *)iconImgView {
    if (!_iconImgView) {
        _iconImgView = [UIImageView new];
        _iconImgView.layer.cornerRadius = 48.0f;
        _iconImgView.layer.masksToBounds = YES;
        _iconImgView.layer.borderColor = [UIColor whiteColor].CGColor;
        _iconImgView.layer.borderWidth = 5;
    }
    return _iconImgView;
}

- (UIButton *)attentBtn{
    if (!_attentBtn) {
        _attentBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_attentBtn setImage:[UIImage imageNamed:@"button_sv_page_attent"] forState:UIControlStateNormal];
        [_attentBtn setImage:[UIImage imageNamed:@"button_sv_page_attented"] forState:UIControlStateSelected];
        [_attentBtn setBackgroundImage:[UIImage imageNamed:@"button_sv_page_attent_bg"] forState:UIControlStateNormal];
        [_attentBtn setBackgroundImage:[UIImage imageNamed:@"button_sv_page_attented_bg"] forState:UIControlStateSelected];
        _attentBtn.layer.cornerRadius = 2.5f;
        _attentBtn.layer.masksToBounds = YES;
        [_attentBtn addTarget:self action:@selector(attentBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _attentBtn;
}

- (UIButton *)chatBtn{
    if (!_chatBtn) {
        _chatBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_chatBtn setImage:[UIImage imageNamed:@"ic_sv_page_message"] forState:UIControlStateNormal];
        [_chatBtn setBackgroundColor:MAIN_COLOR];
        _chatBtn.layer.cornerRadius = 2.5f;
        [_chatBtn addTarget:self action:@selector(chatBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _chatBtn;
}

- (UILabel *)nickLabel{
    if (!_nickLabel) {
        _nickLabel = [[UILabel alloc]init];
        _nickLabel.textColor = [UIColor blackColor];
        _nickLabel.font = [UIFont systemFontOfSize:20 weight:UIFontWeightMedium];
    }
    return _nickLabel;
}

- (UIButton *)ageBtn{
    if (!_ageBtn) {
        _ageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_ageBtn setImage:[UIImage imageNamed:@"ic_girl_w_s"] forState:UIControlStateNormal];
        [_ageBtn setImage:[UIImage imageNamed:@"ic_boy_w_s"] forState:UIControlStateSelected];
        [_ageBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 0)];
        [_ageBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 2, 0, 0)];
        [_ageBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_ageBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        _ageBtn.titleLabel.font = [UIFont systemFontOfSize:8];
        _ageBtn.layer.cornerRadius = 1.5f;
    }
    return _ageBtn;
}

- (UIImageView *)levelImgView{
    if (!_levelImgView) {
        _levelImgView = [[UIImageView alloc]init];
        _levelImgView.layer.cornerRadius = 1.5f;
    }
    return _levelImgView;
}

- (UILabel *)idLabel{
    if (!_idLabel) {
        _idLabel = [[UILabel alloc]init];
        _idLabel.textColor = [UIColor blackColor];
        _idLabel.font = [UIFont systemFontOfSize:13];
    }
    return _idLabel;
}

- (UILabel *)starCountLabel{
    if (!_starCountLabel) {
        _starCountLabel = [[UILabel alloc]init];
        _starCountLabel.textColor = [UIColor blackColor];
        _starCountLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
        _starCountLabel.text = @"0";
    }
    return _starCountLabel;
}

- (UILabel *)attentCountLabel{
    if (!_attentCountLabel) {
        _attentCountLabel = [[UILabel alloc]init];
        _attentCountLabel.textColor = [UIColor blackColor];
        _attentCountLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
        _attentCountLabel.text = @"0";
    }
    return _attentCountLabel;
}

- (UILabel *)fansCountLabel{
    if (!_fansCountLabel) {
        _fansCountLabel = [[UILabel alloc]init];
        _fansCountLabel.textColor = [UIColor blackColor];
        _fansCountLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
        _fansCountLabel.text = @"0";
    }
    return _fansCountLabel;
}

- (UILabel *)signatureLabel{
    if (!_signatureLabel) {
        _signatureLabel = [[UILabel alloc]init];
        _signatureLabel.textColor = CFontColor;
        _signatureLabel.font = [UIFont systemFontOfSize:14];
    }
    return _signatureLabel;
}


@end
