//
//  UserSvPageHeaderView.h
//  NasiLive
//
//  Created by yun11 on 2020/6/2.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#define kSvUserPageBgImgHeight  (KScreenWidth * 150.0f / 375.0f)
#define kSvUserPageHeaderHeight kSvUserPageBgImgHeight + 230

@protocol UserSvPageHeaderViewDelegate <NSObject>

- (void)attentBtnClick:(UIButton *)sender;
- (void)chatBtnClick;

@end

@interface UserSvPageHeaderView : UIView

@property (strong, nonatomic) UserInfoModel *author;

@property (weak, nonatomic) id<UserSvPageHeaderViewDelegate> delegate;

- (void)scrollViewDidScroll:(CGFloat)contentOffsetY;

- (void)refreshUI;

@end

NS_ASSUME_NONNULL_END
