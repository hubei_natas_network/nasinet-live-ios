//
//  ShortVideoModel.m
//  NasiLive
//
//  Created by yun11 on 2020/5/12.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "ShortVideoModel.h"

@implementation ShortVideoModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"videoid" : @"id"};
}

@end
