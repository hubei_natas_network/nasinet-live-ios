//
//  SVCommentModel.m
//  Meet1V1
//
//  Created by yun on 2020/1/13.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "SVCommentModel.h"

@implementation SVCommentModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"commentid" : @"id"
    };
}

// 当 JSON 转为 Model 完成后，该方法会被调用。
// 你可以在这里对数据进行校验，如果校验不通过，可以返回 NO，则该 Model 会被忽略。
// 你也可以在这里做一些自动转换不能完成的工作。
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    if (StrValid(dic[@"content"])) {
        NSString *content = dic[@"content"];
        _content = [content stringByRemovingPercentEncoding];
        //匹配表情文字
        NSArray *resultArr  = [CommonManager machesWithPattern:emojiPattern andStr:content];
        NSUInteger lengthDetail = 0;
        _attrContent = [[NSMutableAttributedString alloc]initWithString:content];
        //遍历所有的result 取出range
        for (NSTextCheckingResult *result in resultArr) {
            //取出图片名
            NSString *imageName = [content substringWithRange:NSMakeRange(result.range.location, result.range.length)];
            NSLog(@"--------%@",imageName);
            NSTextAttachment *attach = [[NSTextAttachment alloc] init];
            UIImage *emojiImage = [UIImage imageNamed:imageName];
            NSAttributedString *imageString;
            if (emojiImage) {
                attach.image = emojiImage;
                attach.bounds = CGRectMake(0, -2, 15, 15);
                imageString =   [NSAttributedString attributedStringWithAttachment:attach];
            }else{
                imageString =   [[NSMutableAttributedString alloc]initWithString:imageName];
            }
            //图片附件的文本长度是1
            NSLog(@"emoj===%zd===size-w:%f==size-h:%f",imageString.length,imageString.size.width,imageString.size.height);
            NSUInteger length = _attrContent.length;
            NSRange newRange = NSMakeRange(result.range.location - lengthDetail, result.range.length);
            [_attrContent replaceCharactersInRange:newRange withAttributedString:imageString];
            
            lengthDetail += length - _attrContent.length;
        }
        if (_rootid > 0 && _rootid != _tocommentid) {
            NSAttributedString *toStr = [[NSAttributedString alloc]initWithString:[NSString stringWithFormat:@"回复%@：",_touser.nick_name] attributes:@{NSForegroundColorAttributeName:RGB_COLOR(@"#676767", 1)}];
            [_attrContent insertAttributedString:toStr atIndex:0];
        }
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 3; // 调整行间距
        NSRange range = NSMakeRange(0, [_attrContent length]);
        [_attrContent addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
    }
    if (StrValid(dic[@"create_time"])) {
        _beauty_time = [CommonManager formateBeautyDate:dic[@"create_time"]];
    }
    return YES;
}

@end
