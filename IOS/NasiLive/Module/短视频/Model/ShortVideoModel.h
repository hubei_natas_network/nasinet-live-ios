//
//  ShortVideoModel.h
//  NasiLive
//
//  Created by yun11 on 2020/5/12.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShortVideoModel : NSObject

@property (assign, nonatomic) int videoid;
@property (assign, nonatomic) int uid;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *play_url;
@property (copy, nonatomic) NSString *thumb_url;
@property (copy, nonatomic) NSString *create_time;
@property (assign, nonatomic) int comment_count;
@property (assign, nonatomic) int play_count;
@property (assign, nonatomic) int like_count;
@property (assign, nonatomic) int share_count;

@property (assign, nonatomic) BOOL is_like;
@property (nonatomic, assign) BOOL collected;


@property (strong, nonatomic) UserInfoModel *author;

@end

NS_ASSUME_NONNULL_END
