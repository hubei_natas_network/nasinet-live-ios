//
//  SVCommentModel.h
//  Meet1V1
//
//  Created by yun on 2020/1/13.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SVCommentModel : NSObject

@property (nonatomic,assign) int commentid;
@property (nonatomic,assign) int rootid;    //根节点评论id
@property (nonatomic,assign) int tocommentid;    //上级节点评论id
@property (nonatomic,assign) int videoid;
@property (nonatomic,assign) int uid;
@property (nonatomic,assign) int touid;
@property (copy, nonatomic) NSString *content;
@property (copy, nonatomic) NSMutableAttributedString *attrContent;
@property (copy, nonatomic) NSString *create_time;
@property (copy, nonatomic) NSString *beauty_time;
@property (nonatomic,assign) int like_count;
@property (nonatomic,assign) int reply_count;

@property (nonatomic,assign) BOOL liked;

@property (strong, nonatomic) UserInfoModel *user;
@property (strong, nonatomic) UserInfoModel *touser;

@end

NS_ASSUME_NONNULL_END
