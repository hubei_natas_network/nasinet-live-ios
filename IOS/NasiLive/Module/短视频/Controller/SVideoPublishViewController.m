//
//  SVideoPublishViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/5/21.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "SVideoPublishViewController.h"

@import TXLiteAVSDK_Professional;
#import "TXUGCPublish.h"

#import "PhotoUtil.h"

@interface SVideoPublishViewController ()<UITextViewDelegate,TXVideoPublishListener>

@property (strong, nonatomic) UITextView    *titleTextView;
@property (strong, nonatomic) UIImageView   *thumbImgView;
@property (strong, nonatomic) UIButton      *saveFileBtn;

@property (assign, nonatomic) BOOL          saved;

@end

#define TextViewPlaceHolder @"输入视频的标题,让更多人看到吧~"

@implementation SVideoPublishViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    // 显示导航栏
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self createNavBar];
    
    TXVideoInfo *videoinfo = [TXVideoInfoReader getVideoInfoWithAsset:self.result.media.videoAsset];
    
    self.thumbImgView = [[UIImageView alloc]initWithFrame:CGRectMake(KScreenWidth - 80 - 15, kTopHeight + 15, 80, 110)];
    self.thumbImgView.image = videoinfo.coverImage;
    self.thumbImgView.layer.cornerRadius = 5;
    self.thumbImgView.layer.masksToBounds = YES;
    [self.view addSubview:self.thumbImgView];
    
    self.titleTextView = [[UITextView alloc]initWithFrame:CGRectMake(15, kTopHeight + 15, self.thumbImgView.mj_x - 15*2, 100)];
    self.titleTextView.textColor = CFontColorLightGray;
    self.titleTextView.font = FFont15;
    self.titleTextView.text = TextViewPlaceHolder;
    self.titleTextView.delegate = self;
    [self.view addSubview:self.titleTextView];
    
    UIButton *publishBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    publishBtn.frame = CGRectMake((KScreenWidth-148)/2, KScreenHeight - kBottomSafeHeight - 52 - 15, 148, 52);
    [publishBtn setImage:IMAGE_NAMED(@"ic_sv_publish_submit") forState:UIControlStateNormal];
    [publishBtn addTarget:self action:@selector(publishBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:publishBtn];
    
    self.saveFileBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.saveFileBtn setTitle:@"保存至本地" forState:UIControlStateNormal];
    [self.saveFileBtn setTitle:@"保存至本地" forState:UIControlStateSelected];
    [self.saveFileBtn setTitleColor:MAIN_COLOR forState:UIControlStateSelected];
    [self.saveFileBtn setTitleColor:CFontColorLightGray forState:UIControlStateNormal];
    [self.saveFileBtn setImage:IMAGE_NAMED(@"ic_sv_save_seleted") forState:UIControlStateSelected];
    [self.saveFileBtn setImage:IMAGE_NAMED(@"ic_sv_save") forState:UIControlStateNormal];
    [self.saveFileBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    self.saveFileBtn.titleLabel.font = FFont12;
    self.saveFileBtn.frame = CGRectMake(15, publishBtn.mj_y - 30 - 20, 100, 20);
    [self.saveFileBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    [self.saveFileBtn addTarget:self action:@selector(saveFileBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.saveFileBtn];
}

- (void)createNavBar{
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(0, kStatusBarHeight, KScreenWidth, kNavBarHeight)];
    [self.view addSubview:navView];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(0, 0, 44, 44);
    [backBtn setImage:IMAGE_NAMED(@"ic_back") forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, kNavBarHeight)];
    titleLabel.text = @"发布";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightMedium];
    titleLabel.textColor = [UIColor blackColor];
    [navView addSubview:titleLabel];
}

- (void)publishBtnClick{
    
    if (self.saveFileBtn.selected && !self.saved) {
        [self saveToAlbum];
    }
    
    NSString *title = self.titleTextView.text;
    if ([title isEqualToString:TextViewPlaceHolder] || title.length == 0) {
        [self.titleTextView becomeFirstResponder];
        return;
    }
    if (title.length > 20) {
        [MBProgressHUD showTipMessageInView:@"标题超过最大字数限制"];
        return;
    }
    
    [self uploadToVod];
}

- (void)submitToServerWithTitle:(NSString *)title thumbUrl:(NSString *)thumbUrl playUrl:(NSString *)playUrl{
    NSDictionary *param = @{@"title":title,
                            @"thumb_url":thumbUrl,
                            @"play_url":playUrl
    };
    [CommonManager POST:@"Shortvideo/publish" parameters:param success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
            if (self.backMode == TCBackModePop) {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }else{
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)uploadToVod{
    [commonManager showLoadingAnimateInWindow];
    //存储封面图片
    NSString *coverImageName = [CommonManager getNameBaseCurrentTime:@".jpg"];
    NSString *coverImagePath = [CommonManager getFilePathWithFileName:coverImageName];
    [UIImagePNGRepresentation(self.thumbImgView.image) writeToFile:coverImagePath atomically:YES];
    [CommonManager POST:@"Config/getSignForVod" parameters:@{} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            NSString *signature = responseObject[@"data"][@"signature"];
            TXUGCPublish *_videoPublish = [[TXUGCPublish alloc] initWithUserID:[NSString stringWithFormat:@"%lld",userManager.curUserInfo.userid]];
            _videoPublish.delegate = self;
            TXPublishParam *publishParam = [[TXPublishParam alloc] init];

            publishParam.signature  = signature;
            publishParam.videoPath  = self.result.media.videoPath;
            publishParam.coverPath = coverImagePath;
            [_videoPublish publishVideo:publishParam];
        }else{
            [commonManager hideAnimateHud];
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)onPublishComplete:(TXPublishResult*)result {
    if (result.retCode == 0) {
        //提交到服务器
        NSString *thumb_url = result.coverURL;
        NSString *play_url = result.videoURL;
        [self submitToServerWithTitle:self.titleTextView.text thumbUrl:thumb_url playUrl:play_url];
    }else{
        [commonManager hideAnimateHud];
        [commonManager showErrorAnimateInWindow];
    }
}

- (void)saveToAlbum{
    if (self.saved) {
        return;
    }
    [PhotoUtil saveAssetToAlbum:[NSURL fileURLWithPath: self.result.media.videoPath]
                     completion:^(BOOL success, NSError * _Nullable error) {
        self.saved = YES;
    }];
}

- (void)saveFileBtnClick:(UIButton *)sender{
    self.saveFileBtn.selected = !self.saveFileBtn.selected;
}

- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:TextViewPlaceHolder]) {
        textView.text = @"";
        textView.textColor = CFontColorGray;
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length == 0) {
        textView.text = TextViewPlaceHolder;
        textView.textColor = CFontColorLightGray;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
