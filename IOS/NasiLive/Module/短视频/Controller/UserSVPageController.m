//
//  UserSVPageController.m
//  NasiLive
//
//  Created by yun11 on 2020/6/2.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "UserSVPageController.h"
#import "UserMomentListViewController.h"
#import "MessageChatViewController.h"
#import "ReportCategoryViewController.h"

#import "UserSvPageHeaderView.h"

#import "JXPagerView.h"
#import "JXCategoryTitleView.h"
#import "JXCategoryView.h"
#import <IQKeyboardManager.h>

#import "SVScaleVideoView.h"
#import "SVCommentView.h"
#import "SharePanelView.h"

#import <ImSDK.h>

static const CGFloat JXheightForHeaderInSection = 44;

@interface UserSVPageController ()<JXCategoryViewDelegate,JXPagerViewDelegate, JXPagerMainTableViewGestureDelegate,SVVideoViewDelegate, SharePanelViewDelegate,UserSvPageHeaderViewDelegate,SVScaleVideoViewProtocol>

@property (nonatomic, strong) JXPagerView                   *pagerView;
@property (nonatomic, strong) UserSvPageHeaderView          *userHeaderView;
@property (nonatomic, strong) JXCategoryTitleView           *categoryView;

@property (strong, nonatomic) UIView        *navigationBar;
@property (strong, nonatomic) UIView        *navigationBackgroundView;
@property (strong, nonatomic) UIButton      *backBtn;
@property (strong, nonatomic) UILabel       *titleLabel;

@property (nonatomic, strong) NSArray <NSString *>  *titles;


@property (nonatomic, weak) SVScaleVideoView      *scaleView;

@property (strong, nonatomic) SVCommentView                     *commentView;


@end

@implementation UserSVPageController
@synthesize currentListVC;

- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [IQKeyboardManager sharedManager].enable = NO;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    
    if (self.scaleView) {
        [self.view bringSubviewToFront:self.scaleView];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // 默认点击第一个
    [self categoryView:self.categoryView didSelectedItemAtIndex:0];
    
    [self.scaleView.videoView resume];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    
    [self.scaleView.videoView pause];
}

- (void)dealloc {
    [self.scaleView.videoView destoryPlayer];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _titles = @[@"作品", @"动态", @"喜欢"];
    
    _categoryView = [[JXCategoryTitleView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, JXheightForHeaderInSection)];
    self.categoryView.titles = self.titles;
    self.categoryView.backgroundColor = [UIColor whiteColor];
    self.categoryView.delegate = self;
    self.categoryView.titleSelectedColor = [UIColor blackColor];
    self.categoryView.titleColor = CFontColorGray;
    self.categoryView.titleColorGradientEnabled = YES;
    self.categoryView.titleLabelZoomEnabled = NO;
    self.categoryView.contentScrollViewClickTransitionAnimationEnabled = NO;
    
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorColor = MAIN_COLOR;
    lineView.indicatorWidth = KScreenWidth/3 - 15*2;
    lineView.indicatorHeight = 1.5;
    lineView.indicatorCornerRadius = 0;
    self.categoryView.indicators = @[lineView];
    
    _pagerView = [[JXPagerView alloc] initWithDelegate:self];
    _pagerView.pinSectionHeaderVerticalOffset = kTopHeight;
    self.pagerView.mainTableView.gestureDelegate = self;
    [self.view addSubview:self.pagerView];
    
    self.categoryView.listContainer = (id<JXCategoryViewListContainer>)self.pagerView.listContainerView;
    
    //导航栏隐藏的情况，处理扣边返回，下面的代码要加上
    [self.pagerView.listContainerView.scrollView.panGestureRecognizer requireGestureRecognizerToFail:self.navigationController.interactivePopGestureRecognizer];
    [self.pagerView.mainTableView.panGestureRecognizer requireGestureRecognizerToFail:self.navigationController.interactivePopGestureRecognizer];
    
    
    [self createNavBar];
}

- (void)createNavBar{
    _navigationBar = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, kTopHeight)];
    _navigationBar.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_navigationBar];
    
    _navigationBackgroundView = [[UIView alloc]initWithFrame:_navigationBar.bounds];
    _navigationBackgroundView.backgroundColor = [UIColor whiteColor];
    _navigationBackgroundView.alpha = 0;
    [_navigationBar addSubview:_navigationBackgroundView];
    
    _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backBtn setImage:[UIImage imageNamed:@"ic_back_white"] forState:UIControlStateNormal];
    [_backBtn setImage:[UIImage imageNamed:@"ic_back"] forState:UIControlStateSelected];
    _backBtn.frame = CGRectMake(0, kStatusBarHeight, 44, 44);
    [_backBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [_navigationBar addSubview:_backBtn];
    
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(44, kStatusBarHeight, KScreenWidth - 44*2, 44)];
    _titleLabel.textColor = [UIColor blackColor];
    _titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.text = self.author.nick_name;
    _titleLabel.alpha = 0;
    [_navigationBar addSubview:_titleLabel];
    
    [self getUserInfo];
}

- (void)getUserInfo{
    [CommonManager POST:@"shortvideo/getUserInfo" parameters:@{@"authorid":@(self.author.userid)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.author.sv_star_count = [responseObject[@"data"][@"star_count"] intValue];
            self.author.attent_count = [responseObject[@"data"][@"attent_count"] intValue];
            self.author.fans_count = [responseObject[@"data"][@"fans_count"] intValue];
            self.userHeaderView.author = self.author;
            
            self->_titles = @[[NSString stringWithFormat:@"作品 %d",[responseObject[@"data"][@"video_count"] intValue]],
                              [NSString stringWithFormat:@"动态 %d",[responseObject[@"data"][@"moment_count"] intValue]],
                              [NSString stringWithFormat:@"喜欢 %d",[responseObject[@"data"][@"like_video_count"] intValue]]];
            self.categoryView.titles = self.titles;
            [self.categoryView reloadDataWithoutListContainer];
        }
    } failure:^(NSError *error) {
        
    }];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.pagerView.frame = self.view.bounds;
}

- (void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - JXPagerViewDelegate

- (UIView *)tableHeaderViewInPagerView:(JXPagerView *)pagerView {
    return self.userHeaderView;
}

- (NSUInteger)tableHeaderViewHeightInPagerView:(JXPagerView *)pagerView {
    return kSvUserPageHeaderHeight;
}

- (NSUInteger)heightForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return JXheightForHeaderInSection;
}

- (UIView *)viewForPinSectionHeaderInPagerView:(JXPagerView *)pagerView {
    return self.categoryView;
}

- (NSInteger)numberOfListsInPagerView:(JXPagerView *)pagerView {
    //和categoryView的item数量一致
    return self.categoryView.titles.count;
}

- (id<JXPagerViewListViewDelegate>)pagerView:(JXPagerView *)pagerView initListAtIndex:(NSInteger)index {
    switch (index) {
        case 0:{
            SVideoListViewController *listVc = [[SVideoListViewController alloc] init];
            listVc.authorid = (int)self.author.userid;
            listVc.type = 0;
            kWeakSelf(listVc)
            @weakify(self);
            listVc.itemClickBlock = ^(NSArray * _Nonnull videos, NSInteger index) {
                @strongify(self);
                [self showVideoVCWithVideos:videos datasourceVC:weaklistVc index:index];
            };
            return listVc;
        }
            break;
        case 1:{
            UserMomentListViewController *listVc = [[UserMomentListViewController alloc] init];
            listVc.userid = (int)self.author.userid;
            return listVc;
        }
            break;
        case 2:{
            SVideoListViewController *listVc = [[SVideoListViewController alloc] init];
            listVc.authorid = (int)self.author.userid;
            listVc.type = 1;
            kWeakSelf(listVc)
            @weakify(self);
            listVc.itemClickBlock = ^(NSArray * _Nonnull videos, NSInteger index) {
                @strongify(self);
                [self showVideoVCWithVideos:videos datasourceVC:weaklistVc index:index];
            };
            return listVc;
        }
            break;
            
        default:
            break;
    }
    return nil;
}

- (void)showVideoVCWithVideos:(NSArray *)videos datasourceVC:(SVideoListViewController *)vc index:(NSInteger)index {
    SVScaleVideoView *scaleView = [[SVScaleVideoView alloc] initWithVC:self datasourceVC:vc videos:videos index:index];
    scaleView.videoView.delegate = self;
    [scaleView show];
    
    self.scaleView = scaleView;
}

#pragma mark - JXCategoryViewDelegate

- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index {
    self.navigationController.interactivePopGestureRecognizer.enabled = (index == 0);
    if (index == 0 || index == 2) {
        self.currentListVC = (SVideoListViewController *)self.pagerView.validListDict[@(index)];
    }
}

#pragma mark - JXPagerMainTableViewGestureDelegate

- (BOOL)mainTableViewGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    //禁止categoryView左右滑动的时候，上下和左右都可以滚动
    if (otherGestureRecognizer == self.categoryView.collectionView.panGestureRecognizer) {
        return NO;
    }
    return [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]];
}

- (void)mainTableViewDidScroll:(UIScrollView *)scrollView {
    [self.userHeaderView scrollViewDidScroll:scrollView.contentOffset.y];
    
    // 导航栏显隐
    CGFloat offsetY = scrollView.contentOffset.y;
    // 0-200 0
    // 200 - KDYHeaderHeigh - kNavBarheight 渐变从0-1
    // > KDYHeaderHeigh - kNavBarheight 1
    CGFloat alpha = 0;
    if (offsetY < 200) {
        alpha = 0;
    }else if (offsetY > (kSvUserPageHeaderHeight - kTopHeight)) {
        alpha = 1;
    }else {
        alpha = (offsetY - 200) / (kSvUserPageHeaderHeight - kTopHeight - 200);
    }
    self.StatusBarStyle = alpha > 0.2 ? UIStatusBarStyleDefault:UIStatusBarStyleLightContent;
    self.backBtn.selected = alpha > 0.2;
    self.titleLabel.alpha = alpha;
    self.navigationBackgroundView.alpha = alpha;
}


- (UserSvPageHeaderView *)userHeaderView{
    if (!_userHeaderView) {
        _userHeaderView = [[UserSvPageHeaderView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, kSvUserPageHeaderHeight)];
        _userHeaderView.author = self.author;
        _userHeaderView.delegate = self;
    }
    return _userHeaderView;
}

#pragma mark - UserSvPageHeaderViewDelegate
- (void)attentBtnClick:(UIButton *)sender{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    int type = self.author.isattent ? 0 : 1;
    [CommonManager POST:@"Anchor/attentAnchor" parameters:@{@"anchorid":@(self.author.userid),@"type":@(type)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.author.isattent = !self.author.isattent;
            self.author.fans_count = [responseObject[@"data"][@"fans_count"] intValue];
            [self.userHeaderView refreshUI];
            
            SVideoListViewController *svc1 = (SVideoListViewController *)self.pagerView.validListDict[@(0)];
            SVideoListViewController *svc3 = (SVideoListViewController *)self.pagerView.validListDict[@(2)];
            [svc1 updateUserModel:self.author.userid attent:self.author.isattent];
            [svc3 updateUserModel:self.author.userid attent:self.author.isattent];
            
            !self.SvAttentUserBlock? : self.SvAttentUserBlock(self.author.userid, self.author.isattent);
            
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (void)chatBtnClick{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    TUIConversationCellData *data = [[TUIConversationCellData alloc]init];
    data.userID = [NSString stringWithFormat:@"%lld",self.author.userid];
    data.faceUrl = self.author.avatar;
    MessageChatViewController *vc = [[MessageChatViewController alloc]initWithConversationCellData:data senderName:self.author.nick_name];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - SharePanelViewDelegate
- (void)shareWithChannel:(PYShareChannel)channel panel:(nonnull SharePanelView *)panelView{
    if (channel == PYShareChannelReport) {
        ReportCategoryViewController *vc = [[ReportCategoryViewController alloc]init];
        vc.relateid = self.scaleView.videoView.currentPlayView.model.videoid;
        vc.type = ReportTypeShortVideo;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (channel == PYShareChannelCollect){
        if (![CommonManager checkAndLogin]) {
            return;
        }
        [commonManager showLoadingAnimateInWindow];
        [CommonManager POST:@"Shortvideo/collect" parameters:@{@"videoid":@(self.scaleView.videoView.currentPlayView.model.videoid), @"type":@(!self.scaleView.videoView.currentPlayView.model.collected)} success:^(id responseObject) {
            [commonManager hideAnimateHud];
            if (RESP_SUCCESS(responseObject)) {
                [commonManager showSuccessAnimateInWindow];
                self.scaleView.videoView.currentPlayView.model.collected = !self.scaleView.videoView.currentPlayView.model.collected;
                [panelView setCollectBtnSelected:self.scaleView.videoView.currentPlayView.model.collected];
            }else{
                RESP_SHOW_ERROR_MSG(responseObject);
            }
        } failure:^(NSError *error) {
            [commonManager hideAnimateHud];
            RESP_FAILURE;
        }];
    }else if (channel == PYShareChannelLink){
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = [NSString stringWithFormat:@"%@%d",[configManager appConfig].share_shortvideo_url,self.scaleView.videoView.currentPlayView.model.videoid];
        [MBProgressHUD showTipMessageInView:@"链接已复制到剪贴板"];
    }else{
        SSDKPlatformType type = [SharePanelView getSSDKPlatformTypeBy:channel];
        //统一创建分享参数
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:self.scaleView.videoView.currentPlayView.model.title
                                         images:self.scaleView.videoView.currentPlayView.model.thumb_url
                                            url:[NSURL URLWithString:[NSString stringWithFormat:@"%@%d",[configManager appConfig].share_shortvideo_url,self.scaleView.videoView.currentPlayView.model.videoid]]
                                          title:@"推荐你看这个视频"
                                           type:SSDKContentTypeAuto];
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            switch (state) {
                case SSDKResponseStateSuccess:
                    [MBProgressHUD showTipMessageInView:@"分享成功"];
                    break;
                case SSDKResponseStateFail:
                {
                    [MBProgressHUD showTipMessageInView:@"分享失败"];
                    //失败
                    break;
                }
                case SSDKResponseStateCancel:
                    //取消
                    break;
                    
                default:
                    break;
            }
        }];
    }
}

#pragma mark - SVVideoViewDelegate
- (void)videoView:(SVVideoView *)videoView didClickIcon:(ShortVideoModel *)videoModel {
    UserSVPageController *vc = [[UserSVPageController alloc]init];
    vc.author = videoModel.author;
    vc.SvAttentUserBlock = ^(int userid, BOOL attented) {
        [videoView updateUserModel:userid attent:attented];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)videoView:(SVVideoView *)videoView didClickAttent:(nonnull ShortVideoModel *)videoModel {
    ShortVideoModel *model = videoModel;

    int type = model.author.isattent ? 0 : 1;
    [CommonManager POST:@"Anchor/attentAnchor" parameters:@{@"anchorid":@(model.author.userid),@"type":@(type)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            model.author.isattent = !model.author.isattent;
            model.author.fans_count = [responseObject[@"data"][@"fans_count"] intValue];
            if (model.author.userid == self.author.userid) {
                self.author.isattent = model.author.isattent;
                self.author.fans_count = model.author.fans_count;
                [self.userHeaderView refreshUI];
            }
            videoView.currentPlayView.model = videoModel;
            [self.scaleView.videoView updateUserModel:model.author.userid attent:model.author.isattent];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}


- (void)videoView:(SVVideoView *)videoView didClickComment:(ShortVideoModel *)videoModel {
    if (!_commentView) {
        _commentView = [[SVCommentView alloc]initWithFrame:CGRectMake(0, KScreenHeight, KScreenWidth, KScreenHeight)];
        _commentView.CommentCountBlock = ^(int count) {
            videoView.currentPlayView.model = videoModel;
        };
        [kAppWindow addSubview:_commentView];
    }
    _commentView.videoModel = videoModel;
    [UIView animateWithDuration:0.3 animations:^{
        self->_commentView.mj_y = 0;
    }];
}

- (void)videoView:(SVVideoView *)videoView didClickShare:(ShortVideoModel *)videoModel {
    SharePanelView *panelView = [SharePanelView showPanelInView:kAppWindow];
    panelView.delegate = self;
    [panelView setCollectBtnSelected:videoModel.collected];
}

- (void)videoView:(SVVideoView *)videoView didScrollIsCritical:(BOOL)isCritical {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
