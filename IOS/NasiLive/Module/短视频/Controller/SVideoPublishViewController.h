//
//  SVideoPublishViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/5/21.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <UGCKit/UGCKit.h>

typedef NS_ENUM(NSInteger, TCBackMode) {
    TCBackModePop,
    TCBackModeDismiss
};

NS_ASSUME_NONNULL_BEGIN

@interface SVideoPublishViewController : UIViewController

@property (assign, nonatomic) TCBackMode backMode;
@property (strong, nonatomic) UGCKitResult *result;

@end

NS_ASSUME_NONNULL_END
