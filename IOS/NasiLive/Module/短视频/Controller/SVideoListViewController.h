//
//  SVideoListViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/6/2.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"
#import <JXPagerView.h>
#import <JXCategoryListContainerView.h>
#import "SearchVcProtocal.h"

#import "SVVideoView.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, SvideoListType) {
    SvideoListTypeProduction = 0,       //作品
    SvideoListTypeLike,
    SvideoListTypeCollect,
    SvideoListTypeSearch,
};

@interface SVideoListViewController : RootViewController<JXPagerViewListViewDelegate,SVVcProtocol,SearchVcProtocal,JXCategoryListContentViewDelegate>

@property (assign, nonatomic) int               authorid;

@property (nonatomic, copy) void(^itemClickBlock)(NSArray *videos, NSInteger index);
@property (nonatomic, copy) void (^SvAttentUserBlock)(int userid, BOOL attented);

@property (nonatomic, assign) NSInteger                     selectedIndex;

@property (assign, nonatomic) SvideoListType                type;  //0-作品 1-喜欢 2-收藏 3-搜索

- (void)refreshData;
- (void)updateUserModel:(int)userid attent:(BOOL)isattent;

@end

NS_ASSUME_NONNULL_END
