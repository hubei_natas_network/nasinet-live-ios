//
//  SVideoHomeViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/5/12.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "SVideoHomeViewController.h"
#import "UserInfoViewController.h"
#import "SVideoPublishViewController.h"
#import "UserSVPageController.h"
#import "ReportCategoryViewController.h"

#import "SVVideoView.h"
#import "SVCommentView.h"
#import "GKSlidePopupView.h"
#import "GKBallLoadingView.h"
#import "SharePanelView.h"

#import "ShortVideoModel.h"

#import <IQKeyboardManager.h>
#import <TZImagePickerController.h>

#import <UGCKit/UGCKit.h>

#import "TXUGCPublish.h"
@import TXLiteAVSDK_Professional;


#define kTitleViewY         (kTopSafeHeight + 20.0f)
// 过渡中心点
#define kTransitionCenter   20.0f

#define pagesize            20

@interface SVideoHomeViewController ()<SVVideoViewDelegate,SVVcProtocol,TZImagePickerControllerDelegate,SharePanelViewDelegate>{
    UIView                                                      *selTypeView;
    UIView                                                      *shadowView;
}

@property (nonatomic, strong) SVVideoView                       *videoView;

@property (nonatomic, strong) IBOutlet UIView                   *topView;
@property (weak, nonatomic) IBOutlet UIButton                   *shootBtn;

@property (nonatomic, strong) IBOutlet UIView                   *refreshView;
@property (nonatomic, strong) IBOutlet UILabel                  *refreshLabel;

@property (nonatomic, strong) UIView                            *loadingBgView;
@property (nonatomic, strong) GKBallLoadingView                 *refreshLoadingView;

@property (nonatomic, strong) IBOutlet UIButton                 *searchBtn;
    
@property (nonatomic, strong) IBOutlet UIButton                 *recBtn;
@property (nonatomic, strong) IBOutlet UIButton                 *hotBtn;
@property (strong, nonatomic) IBOutlet UIView                   *topUnderline;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint         *topUnderlineCenterXLC;

@property (nonatomic, assign) BOOL                              isRefreshing;

@property (assign, nonatomic) int                               selectedItemIndex;

@property (strong, nonatomic) SVCommentView                     *commentView;

@end

@implementation SVideoHomeViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [IQKeyboardManager sharedManager].enable = NO;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    [self shadowTap];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.videoView resume];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].enableAutoToolbar = YES;
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    
    // 停止播放
    [self.videoView pause];
}

- (void)dealloc {
    if (_videoView) {
        [_videoView destoryPlayer];
    }
    
    NSLog(@"playerVC dealloc");
}

- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view insertSubview:self.videoView atIndex:0];
    
    [self.videoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-kTabBarHeight);
    }];
    
    [self.view addSubview:self.loadingBgView];
    
    self.loadingBgView.frame = CGRectMake(KScreenWidth - 15 - 44, kTopSafeHeight + kStatusBarHeight, 44, 44);
    self.refreshLoadingView = [GKBallLoadingView loadingViewInView:self.loadingBgView];
    
    [self refreshFirst:YES];
}

- (void)refreshFirst:(BOOL)first{
    // 加载数据
    GKBallLoadingView *loadingView;
    UIView *ballContainerView;
    if (first) {
        ballContainerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight)];
        [self.view addSubview:ballContainerView];
        loadingView = [GKBallLoadingView loadingViewInView:ballContainerView];
        [loadingView startLoading];
    }else{
        [self.refreshLoadingView startLoading];
        self.isRefreshing = YES;
        self.shootBtn.hidden = YES;
    }
    
    NSString *api = self.selectedItemIndex == 0?@"Shortvideo/getRandomList":@"Shortvideo/getHotList";
    [CommonManager POST:api parameters:@{@"page":@(1),@"size":@(pagesize)} success:^(id responseObject) {
        if (first) {
            [loadingView stopLoading];
            [loadingView removeFromSuperview];
            [ballContainerView removeFromSuperview];
        }else{
            [self.refreshLoadingView stopLoading];
            self.loadingBgView.alpha = 0;
            self.shootBtn.hidden = NO;
            self.isRefreshing = NO;
        }
        NSArray *list = [NSArray yy_modelArrayWithClass:[ShortVideoModel class] json:responseObject[@"data"]];
        if (list) {
            [self.videoView setModels:list index:0];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
        if (first) {
            [loadingView stopLoading];
            [loadingView removeFromSuperview];
        }else{
            [self.refreshLoadingView stopLoading];
            self.loadingBgView.alpha = 0;
            self.shootBtn.hidden = NO;
            self.isRefreshing = NO;
        }
    }];
}

- (void)refreshMoreListWithSuccess:(void (^)(NSArray * _Nonnull))success failure:(void (^)(NSError * _Nonnull))failure {
    int page = ceil((self.videoView.videos.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    NSString *api = self.selectedItemIndex == 0?@"Shortvideo/getRandomList":@"Shortvideo/getHotList";
    [CommonManager POST:api parameters:@{@"page":@(page),@"size":@(pagesize)} success:^(id responseObject) {
        NSArray *list = [NSArray yy_modelArrayWithClass:[ShortVideoModel class] json:responseObject[@"data"]];
        success(list);
    } failure:^(NSError *error) {
        RESP_FAILURE;
        failure(error);
    }];
}

- (IBAction)searchClick:(id)sender {
    
}

- (IBAction)itemClick:(UIButton *)sender {
    if (sender == self.recBtn && self.selectedItemIndex != 0) {
        self.selectedItemIndex = 0;
        [self refreshFirst:YES];
        self.topUnderlineCenterXLC.constant = -35;
    }else if (sender == self.hotBtn && self.selectedItemIndex != 1){
        self.selectedItemIndex = 1;
        [self refreshFirst:YES];
        self.topUnderlineCenterXLC.constant = 35;
    }
    
    [UIView animateWithDuration:0.2f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)shootClick:(UIButton *)sender {
    if (![CommonManager checkAndLogin]) return;
    if (!shadowView) {
        shadowView = [[UIView alloc]initWithFrame:self.view.bounds];
        shadowView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
        shadowView.userInteractionEnabled = YES;
        shadowView.alpha = 0;
        shadowView.hidden = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shadowTap)];
        [shadowView addGestureRecognizer:tap];
        [self.view addSubview:shadowView];
    }
    
    CGFloat h = 250;
    if (!selTypeView) {
        selTypeView = [[UIView alloc]initWithFrame:CGRectMake(KScreenWidth - 130 - 14, kStatusBarHeight + kNavBarHeight, 130, 0)];
        selTypeView.backgroundColor = [UIColor whiteColor];
        selTypeView.layer.cornerRadius = 12.5;
        selTypeView.layer.masksToBounds = YES;
        selTypeView.alpha = 0;
        [self.view addSubview:selTypeView];
        
        CGFloat btnH = h/5;
        CGFloat btnW = selTypeView.width - 30;
        UIButton *publishBtn1 = [self createPublishBtnWithImageName:@"ic_sv_pb_camera" title:@"录制" frame:CGRectMake(15, 0, btnW, btnH) selector:@selector(UGCBtnRecordClick)];
        [selTypeView addSubview:publishBtn1];
        
        UIButton *publishBtn2 = [self createPublishBtnWithImageName:@"ic_sv_pb_hepai" title:@"双屏合拍" frame:CGRectMake(15, btnH, btnW, btnH) selector:@selector(UGCBtnShuangpingClick)];
        [selTypeView addSubview:publishBtn2];
        
        UIButton *publishBtn3 = [self createPublishBtnWithImageName:@"ic_sv_pb_sanping" title:@"三屏合拍" frame:CGRectMake(15, btnH*2, btnW, btnH) selector:@selector(UGCBtnSanpingClick)];
        [selTypeView addSubview:publishBtn3];
        
        UIButton *publishBtn4 = [self createPublishBtnWithImageName:@"ic_sv_pb_bianji" title:@"视频编辑" frame:CGRectMake(15, btnH*3, btnW, btnH) selector:@selector(UGCBtnVideoEditClick)];
        [selTypeView addSubview:publishBtn4];
        
        UIButton *publishBtn5 = [self createPublishBtnWithImageName:@"ic_sv_pb_pic" title:@"照片编辑" frame:CGRectMake(15, btnH*4, btnW, btnH) selector:@selector(UGCBtnImgEditClick)];
        [selTypeView addSubview:publishBtn5];
    }
    
    [UIView animateWithDuration:0.2f animations:^{
        self->selTypeView.height = h;
        self->selTypeView.alpha = 1;
        self->shadowView.hidden = NO;
        self->shadowView.alpha = 1;
    }];
}

- (void)shadowTap{
    selTypeView.height = 0;
    selTypeView.alpha = 0;
    shadowView.hidden = YES;
    shadowView.alpha = 0;
}

- (void)UGCBtnRecordClick{
    UGCKitRecordConfig *config = [[UGCKitRecordConfig alloc]init];
    config.minDuration = 5;
    config.maxDuration = 30;
    config.tiSdkKey = configManager.appConfig.tisdk_key;
    UGCKitRecordViewController *recordViewController = [[UGCKitRecordViewController alloc] initWithConfig:config theme:nil];
    [self.navigationController pushViewController:recordViewController animated:YES];
    __weak UINavigationController *weakNav = self.navigationController;
    __weak __typeof(self) wself = self;
    recordViewController.completion = ^(UGCKitResult *result) {
        if (result.cancelled) {
            // 用户取消录制，退出录制界面
            [wself.navigationController popViewControllerAnimated:YES];
        } else {
            // 录制成功, 用结果进行下一步处理
            [wself showEditViewController:result rotation:TCEditRotation0 inNavigationController:weakNav backMode:TCBackModePop];
        }
    };
}
- (void)UGCBtnShuangpingClick{
    UGCKitRecordConfig *config = [[UGCKitRecordConfig alloc] init];
    config.recordStyle = UGCKitRecordStyleDuet;
    [self hepaiClick:config];
}
- (void)UGCBtnSanpingClick{
    UGCKitRecordConfig *config = [[UGCKitRecordConfig alloc] init];
    config.recordStyle = UGCKitRecordStyleTrio;
    config.ratio = VIDEO_ASPECT_RATIO_4_3;
    [self hepaiClick:config];
}
- (void)UGCBtnVideoEditClick{
    UGCKitMediaPickerConfig *config = [[UGCKitMediaPickerConfig alloc] init];
    config.mediaType = UGCKitMediaTypeVideo;
    config.maxItemCount = 3;
    UGCKitMediaPickerViewController *imagePickerController = [[UGCKitMediaPickerViewController alloc] initWithConfig:config theme:nil];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:imagePickerController];
    nav.modalPresentationStyle = UIModalPresentationFullScreen;
    __weak __typeof(self) wself = self;
    __weak UINavigationController *navigationController = nav;
    imagePickerController.completion = ^(UGCKitResult *result) {
        if (!result.cancelled && result.code == 0) {
            [wself showVideoCutView:result inNavigationController:navigationController];
        } else {
            NSLog(@"isCancelled: %c, failed: %@", result.cancelled ? 'y' : 'n', result.info[NSLocalizedDescriptionKey]);
            [wself dismissViewControllerAnimated:YES completion:^{
                if (!result.cancelled) {
                    UIAlertController *alert =
                    [UIAlertController alertControllerWithTitle:result.info[NSLocalizedDescriptionKey]
                                                        message:nil
                                                 preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"确定"
                                                              style:UIAlertActionStyleDefault
                                                            handler:nil]];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }];
        }
    };
    [self presentViewController:nav animated:YES completion:NULL];
}
- (void)UGCBtnImgEditClick{
    UGCKitMediaPickerConfig *config = [[UGCKitMediaPickerConfig alloc] init];
    config.mediaType = UGCKitMediaTypePhoto;
    config.minItemCount = 3;
    config.maxItemCount = 10;
    
    UGCKitMediaPickerViewController *imagePickerController = [[UGCKitMediaPickerViewController alloc] initWithConfig:config theme:nil];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:imagePickerController];
    nav.modalPresentationStyle = UIModalPresentationFullScreen;
    __weak __typeof(self) wself = self;
    __weak UINavigationController *navigationController = nav;
    imagePickerController.completion = ^(UGCKitResult *result) {
        if (!result.cancelled && result.code == 0) {
            [wself showVideoCutView:result inNavigationController:navigationController];
        } else {
            NSLog(@"isCancelled: %c, failed: %@", result.cancelled ? 'y' : 'n', result.info[NSLocalizedDescriptionKey]);
            [wself dismissViewControllerAnimated:YES completion:nil];
        }
    };
    [self presentViewController:nav animated:YES completion:NULL];
}

- (void)hepaiClick:(UGCKitRecordConfig *)config{
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:9 delegate:self];
    imagePickerVc.allowTakeVideo = NO;
    imagePickerVc.allowPickingGif = NO;
    imagePickerVc.allowPickingVideo = YES;
    imagePickerVc.allowPickingImage = NO;
    imagePickerVc.maxImagesCount = 1;
    imagePickerVc.showPhotoCannotSelectLayer = YES;
    imagePickerVc.allowPickingOriginalPhoto = NO;
    imagePickerVc.naviTitleColor = [UIColor colorWithHexString:@"333333"];
    imagePickerVc.barItemTextColor = [UIColor colorWithHexString:@"333333"];
    imagePickerVc.statusBarStyle = UIStatusBarStyleDefault;
    [imagePickerVc setDidFinishPickingVideoHandle:^(UIImage *coverImage, PHAsset *asset) {
        [MBProgressHUD showTipMessageInWindow:@"视频处理中"];
        NSArray * assetResources = [PHAssetResource assetResourcesForAsset: asset];
        PHAssetResource * resource;
        for (PHAssetResource * assetRes in assetResources) {
            if (assetRes.type == PHAssetResourceTypePairedVideo || assetRes.type == PHAssetResourceTypeVideo) {
                resource = assetRes;
            }
        }
        NSString *videoFileName = @"tempAssetVideo.mov";
        if (resource.originalFilename) {
            videoFileName = resource.originalFilename;
        }
        PHVideoRequestOptions * options = [[PHVideoRequestOptions alloc] init];
        options.version = PHImageRequestOptionsVersionCurrent;
        options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
        NSString * PATH_MOVIE_FILE = [NSTemporaryDirectory() stringByAppendingPathComponent: videoFileName];
        [[NSFileManager defaultManager] removeItemAtPath: PATH_MOVIE_FILE error: nil];
        [[PHAssetResourceManager defaultManager] writeDataForAssetResource: resource toFile: [NSURL fileURLWithPath: PATH_MOVIE_FILE] options: nil completionHandler: ^(NSError * _Nullable error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUD];
                if (!error) {
                    if (config.recordStyle == UGCKitRecordStyleTrio) {
                        config.chorusVideos = @[PATH_MOVIE_FILE,PATH_MOVIE_FILE];
                    }else{
                        config.chorusVideos = @[PATH_MOVIE_FILE];
                    }
                    [self showRecordViewControllerWithConfig:config];
                }
            });
        }];
    }];
    imagePickerVc.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:imagePickerVc animated:YES completion:nil];
}

- (void)showRecordViewControllerWithConfig:(UGCKitRecordConfig *)config{
    UGCKitRecordViewController *videoRecord = [[UGCKitRecordViewController alloc] initWithConfig:config theme:nil];
    __weak UINavigationController *weakNavigation = self.navigationController;;
    __weak __typeof(self) wself = self;
    videoRecord.completion = ^(UGCKitResult *result) {
        if (result.code == 0 && !result.cancelled) {
            [wself showEditViewController:result rotation:TCEditRotation0 inNavigationController:weakNavigation backMode:TCBackModePop];
        } else {
            [wself.navigationController popViewControllerAnimated:YES];
        }
    };
    [self.navigationController pushViewController:videoRecord animated:YES];
}

- (void)showVideoCutView:(UGCKitResult *)result inNavigationController:(UINavigationController *)nav {
    UGCKitCutViewController *vc = [[UGCKitCutViewController alloc] initWithMedia:result.media theme:nil];
    __weak __typeof(self) wself = self;
    __weak UINavigationController *weakNavigation = nav;
    vc.completion = ^(UGCKitResult *result, int rotation) {
        if ([result isCancelled]) {
            [wself dismissViewControllerAnimated:YES completion:nil];
        } else {
            [wself showEditViewController:result rotation:rotation inNavigationController:weakNavigation backMode:TCBackModeDismiss];
        }
    };
    [nav pushViewController:vc animated:YES];
}

- (void)showEditViewController:(UGCKitResult *)result
                      rotation:(TCEditRotation)rotation
        inNavigationController:(UINavigationController *)nav
                      backMode:(TCBackMode)backMode {
    UGCKitMedia *media = result.media;
    UGCKitEditConfig *config = [[UGCKitEditConfig alloc] init];
    config.rotation = (TCEditRotation)(rotation / 90);
    
    __weak __typeof(self) wself = self;
    UGCKitEditViewController *vc = [[UGCKitEditViewController alloc] initWithMedia:media
                                                                            config:config
                                                                             theme:nil];
    __weak UINavigationController *weakNavigation = nav;
    
    vc.onTapNextButton = ^(void (^finish)(BOOL)) {
        //下一步 发布页
        finish(YES);
    };
    
    vc.completion = ^(UGCKitResult *result) {
        __strong __typeof(wself) self = wself; if (self == nil) { return; }
        if (result.cancelled) {
            if (backMode == TCBackModePop)  {
                [weakNavigation popViewControllerAnimated:YES];
            } else {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        } else {
            SVideoPublishViewController *vc = [[SVideoPublishViewController alloc]init];
            vc.result = result;
            vc.backMode = backMode;
            [weakNavigation pushViewController:vc animated:YES];
        }
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:CACHE_PATH_LIST];
    };
    [nav pushViewController:vc animated:YES];
}

#pragma mark - SharePanelViewDelegate
- (void)shareWithChannel:(PYShareChannel)channel panel:(nonnull SharePanelView *)panelView{
    if (channel == PYShareChannelReport) {
        ReportCategoryViewController *vc = [[ReportCategoryViewController alloc]init];
        vc.relateid = self.videoView.currentPlayView.model.videoid;
        vc.type = ReportTypeShortVideo;
        [self.navigationController pushViewController:vc animated:YES];
    }else if (channel == PYShareChannelCollect){
        if (![CommonManager checkAndLogin]) {
            return;
        }
        [commonManager showLoadingAnimateInWindow];
        [CommonManager POST:@"Shortvideo/collect" parameters:@{@"videoid":@(self.videoView.currentPlayView.model.videoid), @"type":@(!self.videoView.currentPlayView.model.collected)} success:^(id responseObject) {
            [commonManager hideAnimateHud];
            if (RESP_SUCCESS(responseObject)) {
                [commonManager showSuccessAnimateInWindow];
                self.videoView.currentPlayView.model.collected = !self.videoView.currentPlayView.model.collected;
                [panelView setCollectBtnSelected:self.videoView.currentPlayView.model.collected];
            }else{
                RESP_SHOW_ERROR_MSG(responseObject);
            }
        } failure:^(NSError *error) {
            [commonManager hideAnimateHud];
            RESP_FAILURE;
        }];
    }else if (channel == PYShareChannelLink){
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = [NSString stringWithFormat:@"%@%d",[configManager appConfig].share_shortvideo_url,self.videoView.currentPlayView.model.videoid];
        [MBProgressHUD showTipMessageInView:@"链接已复制到剪贴板"];
    }else{
        SSDKPlatformType type = [SharePanelView getSSDKPlatformTypeBy:channel];
        //统一创建分享参数
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:self.videoView.currentPlayView.model.title
                                         images:self.videoView.currentPlayView.model.thumb_url
                                            url:[NSURL URLWithString:[NSString stringWithFormat:@"%@%d",[configManager appConfig].share_shortvideo_url,self.videoView.currentPlayView.model.videoid]]
                                          title:@"推荐你看这个视频"
                                           type:SSDKContentTypeAuto];
        [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            switch (state) {
                case SSDKResponseStateSuccess:{
                    [MBProgressHUD showTipMessageInView:@"分享成功"];
                    [CommonManager POST:@"shortvideo/addShareCount" parameters:@{@"videoid":@(self.videoView.currentPlayView.model.videoid)} success:^(id responseObject) {
                        self.videoView.currentPlayView.model.share_count ++;
                        self.videoView.currentPlayView.model = self.videoView.currentPlayView.model;
                    } failure:nil];
                }
                    break;
                case SSDKResponseStateFail:
                {
                    [MBProgressHUD showTipMessageInView:@"分享失败"];
                    //失败
                    break;
                }
                case SSDKResponseStateCancel:
                    //取消
                    break;
                    
                default:
                    break;
            }
        }];
    }
}

#pragma mark - SVVideoViewDelegate
- (void)videoView:(SVVideoView *)videoView didClickIcon:(ShortVideoModel *)videoModel {
    UserSVPageController *vc = [[UserSVPageController alloc]init];
    vc.author = videoModel.author;
    @weakify(self);
    vc.SvAttentUserBlock = ^(int userid, BOOL attented) {
        @strongify(self);
        [self.videoView updateUserModel:userid attent:attented];
    };
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)videoView:(SVVideoView *)videoView didClickAttent:(nonnull ShortVideoModel *)videoModel {
    ShortVideoModel *model = videoModel;
    
    int type = model.author.isattent ? 0 : 1;
    [CommonManager POST:@"Anchor/attentAnchor" parameters:@{@"anchorid":@(model.author.userid),@"type":@(type)} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            model.author.isattent = !model.author.isattent;
            model.author.fans_count = [responseObject[@"data"][@"fans_count"] intValue];
            videoView.currentPlayView.model = videoModel;
            [self.videoView updateUserModel:model.author.userid attent:model.author.isattent];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}


- (void)videoView:(SVVideoView *)videoView didClickComment:(ShortVideoModel *)videoModel {
    if (!_commentView) {
        _commentView = [[SVCommentView alloc]initWithFrame:CGRectMake(0, KScreenHeight, KScreenWidth, KScreenHeight)];
        _commentView.CommentCountBlock = ^(int count) {
            videoView.currentPlayView.model = videoModel;
        };
        [kAppWindow addSubview:_commentView];
    }
    _commentView.videoModel = videoModel;
    [UIView animateWithDuration:0.3 animations:^{
        self->_commentView.mj_y = 0;
    }];
}

- (void)videoView:(SVVideoView *)videoView didClickShare:(ShortVideoModel *)videoModel {
    SharePanelView *panelView = [SharePanelView showPanelInView:kAppWindow];
    panelView.delegate = self;
    [panelView setCollectBtnSelected:videoModel.collected];
}

- (void)videoView:(SVVideoView *)videoView didScrollIsCritical:(BOOL)isCritical {
    
}

- (void)videoView:(SVVideoView *)videoView didPanWithDistance:(CGFloat)distance isEnd:(BOOL)isEnd {
    if (self.isRefreshing) return;
    
    if (isEnd) {
        [UIView animateWithDuration:0.25 animations:^{
            CGRect frame = self.topView.frame;
            frame.origin.y = kTitleViewY;
            self.topView.frame = frame;
            self.refreshView.frame = frame;
            
            CGRect loadingFrame = self.loadingBgView.frame;
            loadingFrame.origin.y = kTitleViewY;
            self.loadingBgView.frame = loadingFrame;
            
            self.refreshView.alpha      = 0;
            self.topView.alpha        = 1;
            self.loadingBgView.alpha    = 1;
        }];
        
        if (distance >= 2 * kTransitionCenter) { // 刷新
            [self refreshFirst:NO];
        }else {
            self.loadingBgView.alpha = 0;
        }
    }else {
        if (distance < 0) {
            self.refreshView.alpha = 0;
            self.topView.alpha = 1;
        }else if (distance > 0 && distance < kTransitionCenter) {
            CGFloat alpha = distance / kTransitionCenter;
            
            self.refreshView.alpha      = 0;
            self.topView.alpha        = 1 - alpha;
            self.loadingBgView.alpha    = 0;
            
            // 位置改变
            CGRect frame = self.topView.frame;
            frame.origin.y = kTitleViewY + distance;
            self.topView.frame = frame;
            self.refreshView.frame = frame;
            
            CGRect loadingFrame = self.loadingBgView.frame;
            loadingFrame.origin.y = frame.origin.y;
            self.loadingBgView.frame = loadingFrame;
        }else if (distance >= kTransitionCenter && distance <= 2 * kTransitionCenter) {
            CGFloat alpha = (2 * kTransitionCenter - distance) / kTransitionCenter;
            
            self.refreshView.alpha      = 1 - alpha;
            self.topView.alpha        = 0;
            self.loadingBgView.alpha    = 1 - alpha;
            
            // 位置改变
            CGRect frame = self.topView.frame;
            frame.origin.y = kTitleViewY + distance;
            self.topView.frame    = frame;
            self.refreshView.frame  = frame;
            
            CGRect loadingFrame = self.loadingBgView.frame;
            loadingFrame.origin.y = frame.origin.y;
            self.loadingBgView.frame = loadingFrame;
            
            [self.refreshLoadingView startLoadingWithProgress:(1 - alpha)];
        }else {
            self.topView.alpha    = 0;
            self.refreshView.alpha  = 1;
            self.loadingBgView.alpha = 1;
            [self.refreshLoadingView startLoadingWithProgress:1];
        }
    }
}

#pragma mark - 懒加载
- (SVVideoView *)videoView {
    if (!_videoView) {
        _videoView = [[SVVideoView alloc] initWithVC:self isPushed:NO];
        _videoView.perPageSize = pagesize;
        _videoView.delegate = self;
    }
    return _videoView;
}

- (UIView *)loadingBgView {
    if (!_loadingBgView) {
        _loadingBgView = [UIView new];
        _loadingBgView.backgroundColor = [UIColor clearColor];
        _loadingBgView.alpha = 0.0f;
    }
    return _loadingBgView;
}

- (UIButton *)createPublishBtnWithImageName:(NSString *)imageName title:(NSString *)title frame:(CGRect)frame selector:(nonnull SEL)selector{
    UIButton *publishTextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [publishTextBtn setImage:IMAGE_NAMED(imageName) forState:UIControlStateNormal];
    [publishTextBtn setTitle:title forState:UIControlStateNormal];
    [publishTextBtn setTitleColor:[UIColor colorWithHexString:@"444444"] forState:UIControlStateNormal];
    publishTextBtn.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
    [publishTextBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    [publishTextBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [publishTextBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    publishTextBtn.frame = frame;
    [publishTextBtn addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    return publishTextBtn;
}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
