//
//  UserSVPageController.h
//  NasiLive
//
//  Created by yun11 on 2020/6/2.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"
#import "SVideoListViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserSVPageController : RootViewController

@property (strong, nonatomic) UserInfoModel *author;

@property (nonatomic,copy) void (^SvAttentUserBlock)(int userid, BOOL attented);

@end

NS_ASSUME_NONNULL_END
