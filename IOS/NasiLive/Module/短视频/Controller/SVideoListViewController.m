//
//  SVideoListViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/6/2.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "SVideoListViewController.h"
#import "GKBallLoadingView.h"

#import "SVideoListCollectionViewCell.h"
#import "ShortVideoModel.h"

#define pagesize 20

@interface SVideoListViewController ()<UIViewControllerTransitioningDelegate,UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UIView                *loadingBgView;

@property (nonatomic, strong) NSMutableArray        *videos;

@property (nonatomic, copy) void(^scrollCallback)(UIScrollView *scrollView);

@property (nonatomic, assign) BOOL                  isRefresh;

@property (copy, nonatomic) NSString                *api;
@property (strong, nonatomic) NSMutableDictionary   *params;
@property (copy, nonatomic) NSString                *keyword;

//用户索引
@property (strong, nonatomic) NSMutableDictionary       *userIndexes;

@end

@implementation SVideoListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGFloat width = (KScreenWidth - 2) / 3;
    CGFloat height = width * 167 / 124;
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    layout.itemSize = CGSizeMake(width, height);
    layout.minimumLineSpacing = 1.0f;
    layout.minimumInteritemSpacing = 1.0f;
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.alwaysBounceVertical = YES;
    [self.collectionView registerNib:[UINib nibWithNibName:@"SVideoListCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"SVideoListCollectionViewCell"];
    [self.view addSubview:self.collectionView];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [self.view addSubview:self.loadingBgView];
    self.loadingBgView.frame = CGRectMake(0, 0, KScreenWidth, 200.f);
    
    if (!(self.type == SvideoListTypeSearch && self.keyword.length == 0)) {
        [self refreshData];
    }
    
}

- (void)refreshData {
    if (self.isRefresh) return;
    self.isRefresh = YES;
    // 数据加载
    [self.loadingBgView setHidden:NO];
    GKBallLoadingView *loadingView = [GKBallLoadingView loadingViewInView:self.loadingBgView];
    [loadingView startLoading];
    
    self.params[@"page"] = @(1);
    
    [CommonManager POST:self.api parameters:self.params success:^(id responseObject) {
        [loadingView stopLoading];
        [loadingView removeFromSuperview];
        [self.loadingBgView setHidden:YES];
        if (RESP_SUCCESS(responseObject)) {
            NSArray *dataArr = [NSArray yy_modelArrayWithClass:[ShortVideoModel class] json:responseObject[@"data"]];
            if (dataArr.count < pagesize) {
                [self.collectionView.mj_footer removeFromSuperview];
                self.collectionView.mj_footer = nil;
            }else{
                self.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
            }
            [self.videos removeAllObjects];
            [self.videos addObjectsFromArray:dataArr];
            
            //为视频创建用户索引
            [self.userIndexes removeAllObjects];
            for (int i = 0; i < dataArr.count; i++) {
                ShortVideoModel *model = dataArr[i];
                if ([[self.userIndexes allKeys] containsObject:@(model.author.userid)]) {
                    NSMutableArray *array = self.userIndexes[@(model.author.userid)];
                    [array addObject:@(i)];
                }else{
                    NSMutableArray *array = [NSMutableArray arrayWithObject:@(i)];
                    self.userIndexes[@(model.author.userid)] = array;
                }
            }
            
            [self.collectionView reloadData];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
        self.isRefresh = NO;
    } failure:^(NSError *error) {
        self.isRefresh = NO;
        [loadingView stopLoading];
        [loadingView removeFromSuperview];
        RESP_FAILURE;
    }];
}

- (void)loadMore{
    int page = ceil((self.videos.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    self.params[@"page"] = @(page);
    [CommonManager POST:self.api parameters:self.params success:^(id responseObject) {
        [self.collectionView.mj_footer endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            NSArray *dataArr = [NSArray yy_modelArrayWithClass:[ShortVideoModel class] json:responseObject[@"data"]];
            if (dataArr.count < pagesize) {
                [self.collectionView.mj_footer removeFromSuperview];
                self.collectionView.mj_footer = nil;
            }else{
                self.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
            }
            [self.videos addObjectsFromArray:dataArr];
            //为视频创建用户索引
            [self.userIndexes removeAllObjects];
            for (int i = 0; i < dataArr.count; i++) {
                ShortVideoModel *model = dataArr[i];
                if ([[self.userIndexes allKeys] containsObject:@(model.author.userid)]) {
                    NSMutableArray *array = self.userIndexes[@(model.author.userid)];
                    [array addObject:@(i)];
                }else{
                    NSMutableArray *array = [NSMutableArray arrayWithObject:@(i)];
                    self.userIndexes[@(model.author.userid)] = array;
                }
            }
            
            [self.collectionView reloadData];
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
        
    } failure:^(NSError *error) {
        [self.collectionView.mj_footer endRefreshing];
        RESP_FAILURE;
    }];
}

- (void)refreshMoreListWithSuccess:(void (^)(NSArray * _Nonnull))success failure:(void (^)(NSError * _Nonnull))failure {
    if (self.videos.count < pagesize) {
        success(nil);
        return;
    }
    int page = ceil((self.videos.count + 0.1) / pagesize);
    if (page == 0) {
        page = 1;
    }
    self.params[@"page"] = @(page);
    [CommonManager POST:self.api parameters:self.params success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            NSArray *dataArr = [NSArray yy_modelArrayWithClass:[ShortVideoModel class] json:responseObject[@"data"]];
            if (dataArr.count < pagesize) {
                [self.collectionView.mj_footer removeFromSuperview];
                self.collectionView.mj_footer = nil;
            }else{
                self.collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
            }
            [self.videos addObjectsFromArray:dataArr];
            
            //为视频创建用户索引
            [self.userIndexes removeAllObjects];
            for (int i = 0; i < dataArr.count; i++) {
                ShortVideoModel *model = dataArr[i];
                if ([[self.userIndexes allKeys] containsObject:@(model.author.userid)]) {
                    NSMutableArray *array = self.userIndexes[@(model.author.userid)];
                    [array addObject:@(i)];
                }else{
                    NSMutableArray *array = [NSMutableArray arrayWithObject:@(i)];
                    self.userIndexes[@(model.author.userid)] = array;
                }
            }
            
            [self.collectionView reloadData];
            success(dataArr);
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
        
    } failure:^(NSError *error) {
        RESP_FAILURE;
        failure(error);
    }];
}

- (void)updateUserModel:(int)userid attent:(BOOL)isattent{
    if (![self.userIndexes containsObjectForKey:@(userid)]) {
        return;
    }
    NSMutableArray *array = self.userIndexes[@(userid)];
    for (NSNumber *index in array) {
        ShortVideoModel *model = self.videos[[index intValue]];
        model.author.isattent = isattent;
    }
}

#pragma mark - <UICollectionViewDataSource, UICollectionViewDelegate>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.videos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SVideoListCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SVideoListCollectionViewCell" forIndexPath:indexPath];
    cell.model = self.videos[indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedIndex = indexPath.item;
    !self.itemClickBlock ? : self.itemClickBlock(self.videos, indexPath.item);
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    !self.scrollCallback ? : self.scrollCallback(scrollView);
}

#pragma mark - GKPageListViewDelegate
- (UIView *)listView {
    return self.view;
}

- (UIScrollView *)listScrollView {
    return self.collectionView;
}

- (void)listViewDidScrollCallback:(void (^)(UIScrollView *))callback {
    self.scrollCallback = callback;
}

#pragma mark - 懒加载
- (UIView *)loadingBgView {
    if (!_loadingBgView) {
        _loadingBgView = [UIView new];
    }
    return _loadingBgView;
}

- (NSMutableArray *)videos {
    if (!_videos) {
        _videos = [NSMutableArray new];
    }
    return _videos;
}

- (NSString *)api{
    if (!_api) {
        switch (self.type) {
            case SvideoListTypeProduction:
                _api = @"shortvideo/getListByUser";
                break;
            case SvideoListTypeLike:
                _api = @"shortvideo/getListUserLike";
                break;
            case SvideoListTypeCollect:
                _api = @"shortvideo/getCollection";
                break;
            case SvideoListTypeSearch:
                _api = @"shortvideo/search";
                break;
            default:
                break;
        }
    }
    return _api;
}

- (NSMutableDictionary *)params{
    if (!_params) {
        _params = [NSMutableDictionary dictionary];
        _params[@"size"] = @(pagesize);
        switch (self.type) {
            case SvideoListTypeProduction:
            case SvideoListTypeLike:
            case SvideoListTypeCollect:{
                _params[@"userid"] = @(self.authorid);
            }
                break;
            case SvideoListTypeSearch:{
                _params[@"keyword"] = self.keyword;
            }
                break;
            default:
                break;
        }
    }
    return _params;
}

- (NSMutableDictionary *)userIndexes{
    if (!_userIndexes) {
        _userIndexes = [NSMutableDictionary dictionary];
    }
    return _userIndexes;
}

- (void)setKeyword:(NSString *)keyword{
    if (keyword.length == 0 || [keyword isEqualToString:_keyword]) {
        return;
    }
    _keyword = keyword;
    self.params[@"keyword"] = keyword;
    [self refreshData];
}

#pragma mark - SearchVcProtocal
- (void)searchKeyword:(NSString *)keyword{
    self.keyword = keyword;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
