//
//  ShopRootViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/8/21.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <NasiLiveSDK/NasiH5ViewController.h>

NS_ASSUME_NONNULL_BEGIN

@interface QNH5ViewController : NasiH5ViewController

@end

NS_ASSUME_NONNULL_END
