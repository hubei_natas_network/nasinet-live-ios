//
//  ShopRootViewController.m
//  NasiLive
//
//  Created by yun11 on 2020/8/21.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "QNH5ViewController.h"
#import "MessageChatViewController.h"

#import <AlipaySDK/AlipaySDK.h>
#import <WXApi.h>
#import <ImSDK.h>

@interface QNH5ViewController ()

@end

@implementation QNH5ViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

- (void)viewWillDisappear:(BOOL)animated{
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(paySuccessNotification) name:KNotificationPaySuccess object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(payFailNotification) name:KNotificationPayFail object:nil];
}

- (void)logout{
    [userManager logout:nil];
    [kAppDelegate showLoginViewController];
}

- (void)pay:(NSDictionary *)dict{
    if (!StrValid(dict[@"order_no"])) {
        //支付保证金
        [commonManager showLoadingAnimateInWindow];
        [CommonManager POST:@"shop/payDeposit" parameters:dict success:^(id responseObject) {
            [commonManager hideAnimateHud];
            if (RESP_SUCCESS(responseObject)) {
                if ([dict[@"pay_channel"] intValue] == 1) {
                    PayReq *request = [[PayReq alloc] init];
                    request.partnerId = responseObject[@"data"][@"partnerid"];
                    request.prepayId = responseObject[@"data"][@"prepayid"];
                    request.package = @"Sign=WXPay";
                    request.nonceStr = responseObject[@"data"][@"noncestr"];
                    request.timeStamp = [responseObject[@"data"][@"timestamp"] intValue];
                    request.sign = responseObject[@"data"][@"sign"];
                    [WXApi sendReq: request completion:nil];
                }else{
                    [[AlipaySDK defaultService] payOrder:responseObject[@"data"][@"paystr"] fromScheme:@"nasilive" callback:^(NSDictionary *resultDic) {
                        NSLog(@"1");
                    }];
                }
            }
        } failure:^(NSError *error) {
            [commonManager hideAnimateHud];
            RESP_FAILURE;
        }];
    }else{
        if ([dict[@"pay_channel"] intValue] == 1) {
            [self wxPay:dict];
        }else{
            [self alipay:dict];
        }
    }
}

- (void)chat:(NSDictionary *)dict{
    TUIConversationCellData *data = [[TUIConversationCellData alloc]init];
    data.userID = dict[@"uid"];
    MessageChatViewController *vc = [[MessageChatViewController alloc]initWithConversationCellData:data senderName:dict[@"nick_name"]];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)alipay:(NSDictionary *)dict{
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"shop/getAliPayOrder" parameters:dict success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            [[AlipaySDK defaultService] payOrder:responseObject[@"data"][@"paystr"] fromScheme:@"nasilive" callback:^(NSDictionary *resultDic) {
                NSLog(@"1");
            }];
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)wxPay:(NSDictionary *)dict{
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"shop/getWxPayOrder" parameters:dict success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            PayReq *request = [[PayReq alloc] init];
            request.partnerId = responseObject[@"data"][@"partnerid"];
            request.prepayId = responseObject[@"data"][@"prepayid"];
            request.package = @"Sign=WXPay";
            request.nonceStr = responseObject[@"data"][@"noncestr"];
            request.timeStamp = [responseObject[@"data"][@"timestamp"] intValue];
            request.sign = responseObject[@"data"][@"sign"];
            [WXApi sendReq: request completion:nil];
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)paySuccessNotification{
    //支付成功通知
    [self.webView evaluateJavaScript:@"payCallback('SUCCESS')" completionHandler:nil];
}

- (void)payFailNotification{
    //支付失败通知
    [self.webView evaluateJavaScript:@"payCallback('FAIL')" completionHandler:nil];
}

- (void)handlerJavaScriptMethod:(NSString *)method data:(NSDictionary *)data{
    if ([method isEqualToString:@"pay"]){
        [self pay:data];
    }else if ([method isEqualToString:@"chat"]){
        [self chat:data];
    }else if ([method isEqualToString:@"setStatusBarTintStyle"]){
        [self setStatusBarTintStyle:data];
    }else if ([method isEqualToString:@"logout"]){
        [self logout];
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (void)setRootHref:(NSString *)rootHref{
    [super setRootHref:[rootHref stringByAppendingFormat:@"?uid=%lld&token=%@",[userManager curUserInfo].userid,[[userManager curUserInfo].token stringByURLEncode]]];
}

@end
