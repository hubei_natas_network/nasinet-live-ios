//
//  LoginViewController.m
//  Meet_One
//
//  Created by yun on 2019/12/12.
//  Copyright © 2019 yun7. All rights reserved.
//

#import "LoginViewController.h"

#import <ShareSDK/ShareSDK.h>
#import "WXApi.h"

#import "PhoneLoginController.h"
#import "H5ViewController.h"

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@end

@implementation LoginViewController

- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.closeBtn.hidden = AppConfig_NeedLogin;
}

- (IBAction)phoneBtnClick:(UIButton *)sender {
    PhoneLoginController *vc = [[PhoneLoginController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)wxBtnClick:(UIButton *)sender {
    //构造SendAuthReq结构体
    SendAuthReq* req = [[SendAuthReq alloc]init];
    req.scope = @"snsapi_userinfo";
    req.state = @"123";
    //第三方向微信终端发送一个SendAuthReq消息结构
    [WXApi sendReq:req completion:nil];
}

- (IBAction)qqBtnClick:(UIButton *)sender {
    [ShareSDK authorize:SSDKPlatformTypeQQ
               settings:nil
         onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error) {
        if (state == SSDKResponseStateSuccess){
            NSLog(@"%@",user.credential);
            NSLog(@"%ld",user.gender);
            [userManager login:kUserLoginTypeQQ params:@{@"unionid":user.credential.rawData[@"unionid"],@"nick_name":user.nickname,@"gender":@(user.gender),@"icon":user.icon,@"login_platform":@(1)} completion:^(BOOL success, NSString *des) {
                if (!success) {
                    [MBProgressHUD showTipMessageInWindow:@"授权失败"];
                }
            }];
        }else if (state == SSDKResponseStateCancel){
            [MBProgressHUD showTipMessageInWindow:@"取消授权"];
        }else{
            [MBProgressHUD showTipMessageInWindow:@"授权失败"];
        }
    }];
}

- (IBAction)protocalBtnClick:(UIButton *)sender {
    H5ViewController *vc = [[H5ViewController alloc]init];
    vc.href = HREF_RegistProtocol;
    vc.title = @"用户协议和隐私条款";
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)closeBtnClick:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
