//
//  PhoneLoginController.m
//  Nasi
//
//  Created by yun on 2019/12/20.
//  Copyright © 2019 yun7. All rights reserved.
//

#import "PhoneLoginController.h"

#import "PhoneRegistController.h"
#import "ForgetPwdController.h"

#import "WXApi.h"

@interface PhoneLoginController ()

@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;

@end

@implementation PhoneLoginController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)loginClick:(UIButton *)sender {
    NSString *mobile = _mobileTextField.text;
    NSString *pwd = _pwdTextField.text;
    
    if (![CommonManager checkPhone:mobile]) {
        [_mobileTextField becomeFirstResponder];
        [MBProgressHUD showTipMessageInView:@"请填写正确的手机号"];
        return;
    }

    if (pwd.length < 6 || pwd.length > 18) {
        [_pwdTextField becomeFirstResponder];
        [MBProgressHUD showTipMessageInView:@"密码长度为6-18位"];
        return;
    }
    
    [userManager login:kUserLoginTypePwd params:@{@"account":mobile,@"pwd":pwd,@"login_platform":@(1)} completion:^(BOOL success, NSString *des) {
        if (!success) {
            [MBProgressHUD showTipMessageInWindow:des];
        }
    }];
    
}

- (IBAction)registBtnClick:(id)sender {
    [self.navigationController pushViewController:[[PhoneRegistController alloc]init] animated:YES];
}

- (IBAction)wxLoginClick:(UIButton *)sender {
    //构造SendAuthReq结构体
    SendAuthReq* req = [[SendAuthReq alloc]init];
    req.scope = @"snsapi_userinfo";
    req.state = @"123";
    //第三方向微信终端发送一个SendAuthReq消息结构
    [WXApi sendReq:req completion:nil];
}

- (IBAction)qqLoginClick:(UIButton *)sender {
}

- (IBAction)forgetPwdClick:(UIButton *)sender {
    [self.navigationController pushViewController:[[ForgetPwdController alloc]init] animated:YES];
}

- (IBAction)backClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
