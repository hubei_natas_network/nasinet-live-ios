//
//  ForgetPwdController.m
//  Nasi
//
//  Created by yun on 2019/12/20.
//  Copyright © 2019 yun7. All rights reserved.
//

#import "ForgetPwdController.h"

@interface ForgetPwdController (){
    NSTimer *timer;
    NSInteger timerCount;
}

@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UIButton *sendCodeBtn;

@end

@implementation ForgetPwdController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)registBtnClick:(id)sender {
    NSString *mobile = _mobileTextField.text;
    NSString *pwd = _pwdTextField.text;
    NSString *code = _codeTextField.text;
    
    if (![CommonManager checkPhone:mobile]) {
        [_mobileTextField becomeFirstResponder];
        [MBProgressHUD showTipMessageInView:@"请填写正确的手机号"];
        return;
    }

    if (pwd.length < 6 || pwd.length > 18) {
        [_pwdTextField becomeFirstResponder];
        [MBProgressHUD showTipMessageInView:@"密码长度为6-18位"];
        return;
    }
    
    if (code.length < 4) {
        [MBProgressHUD showTipMessageInView:@"验证码有误"];
        return;
    }
    
    [commonManager showLoadingAnimateInView:self.view];
    [CommonManager POST:@"User/changePwd" parameters:@{@"mobile":mobile,@"pwd":pwd,@"smscode":code} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            [commonManager showSuccessAnimateInView:self.view];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (IBAction)sendCodeClick:(UIButton *)sender {
    NSString *mobile = _mobileTextField.text;
    if (![CommonManager checkPhone:mobile]) {
        [_mobileTextField becomeFirstResponder];
        [MBProgressHUD showTipMessageInView:@"请填写正确的手机号"];
        return;
    }
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"user/sendCode" parameters:@{@"mobile":mobile} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            sender.enabled = NO;
            [sender setTitle:@"发送(60)" forState:UIControlStateNormal];
            self->timerCount = 60;
            if (!self->timer) {
                self->timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(timerGo) userInfo:nil repeats:YES];
            }
            [self->timer fire];
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

- (void)timerGo{
    timerCount --;
    if (timerCount == 0) {
        [timer invalidate];
        self.sendCodeBtn.enabled = YES;
        [self.sendCodeBtn setTitle:@"发送" forState:UIControlStateNormal];
    }else{
        [self.sendCodeBtn setTitle:[NSString stringWithFormat:@"发送(%ld)",(long)timerCount] forState:UIControlStateNormal];
    }
}

- (IBAction)backClick:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
