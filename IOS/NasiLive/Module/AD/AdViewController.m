//
//  AdViewController.m
//  Avideo
//
//  Created by yun7_mac on 2019/3/6.
//  Copyright © 2019 yun_7. All rights reserved.
//

#import "AdViewController.h"
#import "LoginViewController.h"
#import "H5ViewController.h"

#import "ConfigModel.h"

#import "AdModel.h"

@interface AdViewController (){
    NSTimer *_timer;
    NSInteger _timerCount;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn;

@property (strong, nonatomic) AdModel *adModel;

@end

@implementation AdViewController

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.closeBtn.hidden = YES;
    
    [[NasiLiveManager sharedInstance]initAccessKey:NasiSDK_AccessKey secretKey:NasiSDK_SecretKey CallBack:^(int code) {
        NasiLiveConfig *config = [[NasiLiveConfig alloc]init];
        config.naviTitleColor = CNavBgFontColor;
        config.naviBgColor = CNavBgColor;
        config.viewBgColor = CViewBgColor;
        [NasiLiveManager sharedInstance].config = config;
        [self loadConfig];
    }];
}

- (void)loadConfig{
    [configManager loadAppConfigFromServer:^(BOOL success, NSDictionary * _Nullable respDict) {
        if (success) {
//            if (StrValid(dic[@"newversion"])) {
//                [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"" contentText:@"发现新版本，请前往更新" singleButton:@"立即前往" buttonClick:^{
//                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:dic[@"newversion"]] options:nil completionHandler:nil];
//                }];
//            }
            if (ValidDict(respDict)) {
                self.adModel = [AdModel yy_modelWithDictionary:respDict];
                [self.imgView sd_setImageWithURL:[NSURL URLWithString:self.adModel.image_url] placeholderImage:IMAGE_NAMED(@"launchImage")];
                self.imgView.userInteractionEnabled = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGes)];
                [self.imgView addGestureRecognizer:tap];
                
                self->_timer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(timerGo) userInfo:nil repeats:YES];
                self->_timerCount = 3;
            }else{
                if (AppConfig_NeedLogin) {
                    [kAppDelegate initUserManager];
                }else{
                    kAppDelegate.mainTabBar = [MainTabBarController new];
                    kAppDelegate.window.rootViewController = kAppDelegate.mainTabBar;
                }
            }
            
        }else{
            [QNAlertView showAlertViewWithType:QNAlertTypeTitle title:@"提醒" contentText:@"配置信息读取失败" singleButton:@"重试" buttonClick:^{
                [self loadConfig];
            }];
        }
    }];
}

- (void)timerGo{
    if (_timerCount > 0) {
        self.closeBtn.hidden = NO;
        self.closeBtn.enabled = NO;
        [self.closeBtn setTitle:[NSString stringWithFormat:@"%ld",(long)_timerCount] forState:UIControlStateDisabled];
        _timerCount --;
    }else{
        [_timer invalidate];
        _timer = nil;
        self.closeBtn.enabled = YES;
        if (AppConfig_AutoEnter) {
            [self closeAdClick:self.closeBtn];
        }
    }
}

- (void)tapGes{
    if (self.adModel) {
        if (self.adModel.jump_type == AdJumpTypeInApp) {
            H5ViewController *vc = [[H5ViewController alloc]init];
            vc.href = self.adModel.jump_url;
            [self.navigationController pushViewController:vc animated:YES];
        }else{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.adModel.jump_url] options:@{} completionHandler:nil];
        }
    }
}

- (IBAction)closeAdClick:(UIButton *)sender {
    if (AppConfig_NeedLogin) {
        [kAppDelegate initUserManager];
    }else{
        kAppDelegate.mainTabBar = [MainTabBarController new];
        kAppDelegate.window.rootViewController = kAppDelegate.mainTabBar;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
