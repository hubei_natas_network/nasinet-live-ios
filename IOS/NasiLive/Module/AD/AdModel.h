//
//  AdModel.h
//  Nasi
//
//  Created by yun on 2020/1/7.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, AdJumpType) {
    AdJumpTypeInApp = 1,
    AdJumpTypeInBrowser = 2,
};

@interface AdModel : NSObject

@property (nonatomic,assign) int adid;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *image_url;
@property (copy, nonatomic) NSString *jump_url;
@property (assign, nonatomic) AdJumpType jump_type;

@end

NS_ASSUME_NONNULL_END
