//
//  AdModel.m
//  Nasi
//
//  Created by yun on 2020/1/7.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "AdModel.h"

@implementation AdModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"adid" : @"id"
             };
}

@end
