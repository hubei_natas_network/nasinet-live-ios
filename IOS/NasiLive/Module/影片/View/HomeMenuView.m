//
//  HomeMenuVIew.m
//  NasiBBS
//
//  Created by kevin on 2021/3/25.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "HomeMenuView.h"
#import "VideoMenuCell.h"
#import "VideoListViewController.h"

@interface HomeMenuView () <UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic ,strong) UICollectionView * collectionView; 

@end
@implementation HomeMenuView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
    
        [self creatUI];
    }
    
    return self;
}

-(void)creatUI
{
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc]init];
    flow.scrollDirection = UICollectionViewScrollDirectionVertical;
    flow.itemSize = CGSizeMake(90, 42);
    flow.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10);
    flow.minimumLineSpacing =  7;
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0,0, KScreenWidth, 200 ) collectionViewLayout:flow];
    self.collectionView.delegate   = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:@"VideoMenuCell" bundle:nil] forCellWithReuseIdentifier:@"VideoMenuCell"];
    self.collectionView.backgroundColor = MAIN_COLOR ;
    [self.collectionView reloadData];

    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
   
    return configManager.videoCategorys.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    VideoMenuCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"VideoMenuCell" forIndexPath:indexPath];
    cell.title.text = configManager.videoCategorys[indexPath.row][@"title"];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    VideoListViewController *vc = [[VideoListViewController alloc]init];
    vc.cateDict = configManager.videoCategorys[indexPath.row];
    vc.isScreen = YES ;
    [[kAppDelegate getCurrentUIVC].navigationController pushViewController:vc animated:YES];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(90, 42);
}

@end
