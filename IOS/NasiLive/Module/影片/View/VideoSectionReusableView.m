//
//  VideoSectionReusableView.m
//  MiyouBBS
//
//  Created by 陈曌然 on 2021/3/12.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "VideoSectionReusableView.h"

@implementation VideoSectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (IBAction)tapAction:(id)sender {
    if (_clickBlock) {
        self.clickBlock();
    }
}

@end
