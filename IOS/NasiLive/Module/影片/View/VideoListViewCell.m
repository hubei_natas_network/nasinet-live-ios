//
//  VideoListViewCell.m
//  MiyouBBS
//
//  Created by 陈曌然 on 2021/3/12.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "VideoListViewCell.h"

@interface VideoListViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imageVIew;
@property (weak, nonatomic) IBOutlet UILabel *lable;

@end
@implementation VideoListViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setModel:(VideoModel *)model
{
    _model  = model ;
    [_imageVIew sd_setImageWithURL:[NSURL URLWithString:model.thumb_url] placeholderImage:IMAGE_NAMED(@"cover_loading")];
    _lable.text = model.title ;
    
    
}
@end
