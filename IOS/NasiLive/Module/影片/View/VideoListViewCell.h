//
//  VideoListViewCell.h
//  MiyouBBS
//
//  Created by 陈曌然 on 2021/3/12.
//  Copyright © 2021 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface VideoListViewCell : UICollectionViewCell

@property (nonatomic , strong) VideoModel    *model ;
@end

NS_ASSUME_NONNULL_END
