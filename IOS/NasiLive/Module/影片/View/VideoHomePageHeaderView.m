//
//  VideoHomePageHeaderView.m
//  MiyouBBS
//
//  Created by yun11 on 2021/3/6.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "VideoHomePageHeaderView.h"
#import <TYCyclePagerView.h>
#import "TYPageControl.h"
#import "TYCyclePagerViewCell.h"
#import "H5ViewController.h"
#import "HeadGameCollectionViewCell.h"
#import "AdModel.h"

@interface VideoHomePageHeaderView ()<TYCyclePagerViewDataSource,TYCyclePagerViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource>{
    
    TYCyclePagerView            *tyCyclePagerView;

}

@property (strong, nonatomic) UIScrollView       * gameScrollView;
@property (strong, nonatomic)UICollectionView    * gameCollectionView ;

@end

@implementation VideoHomePageHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
       
        tyCyclePagerView = [[TYCyclePagerView alloc]init];
        tyCyclePagerView.isInfiniteLoop = YES;
        tyCyclePagerView.autoScrollInterval = 3.0;
        tyCyclePagerView.dataSource = self;
        tyCyclePagerView.delegate = self;
        // registerClass or registerNib
        [tyCyclePagerView registerClass:[TYCyclePagerViewCell class] forCellWithReuseIdentifier:@"TYCyclePagerViewCellID"];
        
        tyCyclePagerView.frame = CGRectMake(0, 5, kScreenWidth, kScreenWidth / 345 * 125);
        [self addSubview:tyCyclePagerView];
        
        UIImageView *gameFlagImgView = [[UIImageView alloc]initWithFrame:CGRectMake(15, tyCyclePagerView.mj_maxY + 15, 19, 15)];
        gameFlagImgView.image = IMAGE_NAMED(@"ic_home_section_pup");
        [self addSubview:gameFlagImgView];
        
        UILabel *gameFlagLabel = [[UILabel alloc]init];
        gameFlagLabel.text = @"推荐游戏";
        gameFlagLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
        gameFlagLabel.textColor = [UIColor colorWithHexString:@"#333333"];
        
        [self addSubview:gameFlagLabel];
        [gameFlagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(gameFlagImgView.mas_right).offset(7);
            make.centerY.equalTo(gameFlagImgView.mas_centerY);
        }];
        

        [self addSubview:self.gameCollectionView];
        _gameCollectionView.frame = CGRectMake(0, gameFlagImgView.mj_maxY + 15, kScreenWidth, 140) ;
        self.backgroundColor = [UIColor whiteColor];
        
        UIImageView *videoFlagImgView = [[UIImageView alloc]initWithFrame:CGRectMake(15, _gameCollectionView.mj_maxY + 15, 19, 15)];
        videoFlagImgView.image = IMAGE_NAMED(@"ic_home_section_yellow");
        [self addSubview:videoFlagImgView];
        
        UILabel *videoFlagLabel = [[UILabel alloc]init];
        videoFlagLabel.text = @"精选大片";
        videoFlagLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
        videoFlagLabel.textColor = [UIColor colorWithHexString:@"#333333"];
        
        [self addSubview:videoFlagLabel];
        [videoFlagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(videoFlagImgView.mas_right).offset(7);
            make.centerY.equalTo(videoFlagImgView.mas_centerY);
        }];

        
    }
    return self;
}



- (void)setAdsArray:(NSMutableArray *)adsArray{
    _adsArray = adsArray ;
    [tyCyclePagerView reloadData];
}

- (void)setGameArray:(NSMutableArray *)gameArray{
    _gameArray = gameArray ;
    [_gameCollectionView reloadData];
}

#pragma mark -------------------- UICollectionViewDelegate  --------------------------------

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    return _gameArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
 
    HeadGameCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"headGameCell" forIndexPath:indexPath];
    cell.gameModel = self.gameArray[indexPath.row];//设置cell的数据
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    return 5;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return CGSizeMake(106.5, 140);
    }
    
    return CGSizeMake(92, 66);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    GameModel *game = self.gameArray[indexPath.row];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:game.link_url] options:@{} completionHandler:nil];
}


#pragma mark -------------------- TYCyclePagerViewDataSource  --------------------------------

- (NSInteger)numberOfItemsInPagerView:(TYCyclePagerView *)pageView {
    return self.adsArray.count;
}

- (UICollectionViewCell *)pagerView:(TYCyclePagerView *)pagerView cellForItemAtIndex:(NSInteger)index {
    AdModel *model = self.adsArray[index];
    TYCyclePagerViewCell *cell = [pagerView dequeueReusableCellWithReuseIdentifier:@"TYCyclePagerViewCellID" forIndex:index];
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:model.image_url] placeholderImage:IMAGE_NAMED(@"cover_loading")];
    return cell;
}

- (TYCyclePagerViewLayout *)layoutForPagerView:(TYCyclePagerView *)pageView {
    TYCyclePagerViewLayout *layout = [[TYCyclePagerViewLayout alloc]init];
    layout.itemSize = CGSizeMake(pageView.width - 10, pageView.height );
    layout.layoutType = TYCyclePagerTransformLayoutNormal;
    layout.itemSpacing = 5;
    return layout;
}

- (void)pagerView:(TYCyclePagerView *)pageView didScrollFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    //    pageControl.currentPage = toIndex;
    //[_pageControl setCurrentPage:newIndex animate:YES];
}

- (void)pagerView:(TYCyclePagerView *)pageView didSelectedItemCell:(__kindof UICollectionViewCell *)cell atIndex:(NSInteger)index{
    AdModel *model = self.adsArray[index];
    if (model.jump_type == AdJumpTypeInApp) {
        H5ViewController *vc = [[H5ViewController alloc]init];
        vc.href = model.jump_url;
        [[kAppDelegate getCurrentUIVC].navigationController pushViewController:vc animated:YES];
    }else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:model.jump_url] options:@{} completionHandler:nil];
    }
}


-(UICollectionView *) gameCollectionView
{
    if (!_gameCollectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumLineSpacing = 1;  //行间距
        flowLayout.minimumInteritemSpacing = 1; //列间距
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 2, 2, 0);
//        flowLayout.estimatedItemSize = CGSizeMake(50, 50);  //预定的itemsize
        flowLayout.itemSize = CGSizeMake(92, 66); //固定的itemsize
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;//滑动的方向 垂直
        _gameCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _gameCollectionView.delegate = self; //设置代理
        _gameCollectionView.dataSource = self;   //设置数据来源
        _gameCollectionView.backgroundColor = [UIColor whiteColor];
        _gameCollectionView.showsHorizontalScrollIndicator = NO ;
        
        
        //注册 cell  为了cell的重用机制  使用NIB  也可以使用代码 registerClass xxxx
        [_gameCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([HeadGameCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:@"headGameCell"];

    }
    return _gameCollectionView;
}
@end
