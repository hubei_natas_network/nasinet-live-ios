//
//  VideoHomePageHeaderView.h
//  MiyouBBS
//
//  Created by yun11 on 2021/3/6.
//  Copyright © 2021 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


#define kVideoHomeHeaderHeight (5 + kScreenWidth / 345 * 125 + 15 + 15 + 140 + 30)
@interface VideoHomePageHeaderView : UICollectionReusableView

@property (strong, nonatomic) NSMutableArray     * adsArray;
@property (strong, nonatomic) NSMutableArray     * gameArray;

@end

NS_ASSUME_NONNULL_END
