//
//  VideoMenuCell.m
//  MiyouBBS
//
//  Created by 陈曌然 on 2021/3/12.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "VideoMenuCell.h"

@interface VideoMenuCell ()
@property (weak, nonatomic) IBOutlet UIView *bgView;

@end
@implementation VideoMenuCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _bgView.layer.cornerRadius = 5 ;
}

@end
