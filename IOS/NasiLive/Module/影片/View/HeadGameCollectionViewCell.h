//
//  HeadGameCollectionViewCell.h
//  MiyouBBS
//
//  Created by 陈曌然 on 2021/3/11.
//  Copyright © 2021 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface HeadGameCollectionViewCell : UICollectionViewCell

@property(nonatomic ,strong) GameModel *gameModel ;
@end

NS_ASSUME_NONNULL_END
