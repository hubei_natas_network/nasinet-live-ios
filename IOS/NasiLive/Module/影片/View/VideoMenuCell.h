//
//  VideoMenuCell.h
//  MiyouBBS
//
//  Created by 陈曌然 on 2021/3/12.
//  Copyright © 2021 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoMenuCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;

@end

NS_ASSUME_NONNULL_END
