//
//  HeadGameCollectionViewCell.m
//  MiyouBBS
//
//  Created by 陈曌然 on 2021/3/11.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "HeadGameCollectionViewCell.h"

@interface HeadGameCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
@implementation HeadGameCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
 
}

- (void)setGameModel:(GameModel *)gameModel{
    _gameModel = gameModel ;
    [_imageView sd_setImageWithURL:[NSURL URLWithString:gameModel.thumb] placeholderImage:IMAGE_NAMED(@"cover_loading")];
    _name.text = _gameModel.title ;
    
}

@end
