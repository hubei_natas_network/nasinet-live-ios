//
//  GameModel.h
//  MiyouBBS
//
//  Created by 陈曌然 on 2021/3/11.
//  Copyright © 2021 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GameModel : NSObject
@property (nonatomic,assign) int gameid;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *link_url;
@property (copy, nonatomic) NSString *thumb;

@end

NS_ASSUME_NONNULL_END
