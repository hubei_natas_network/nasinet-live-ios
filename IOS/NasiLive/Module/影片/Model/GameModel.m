//
//  GameModel.m
//  MiyouBBS
//
//  Created by 陈曌然 on 2021/3/11.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "GameModel.h"

@implementation GameModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"gameid" : @"id"
             };
}

// 当 JSON 转为 Model 完成后，该方法会被调用。
// 你可以在这里对数据进行校验，如果校验不通过，可以返回 NO，则该 Model 会被忽略。
// 你也可以在这里做一些自动转换不能完成的工作。
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    if (StrValid(dic[@"thumb"])) {
        NSString *image_url = dic[@"thumb"];
        if ([image_url hasPrefix:@"http"]) {
            _thumb = image_url;
        }else{
            _thumb = [NSString stringWithFormat:@"%@/%@",[configManager appConfig].file_server_domain,image_url];
        }
    }
    return YES;
}
@end
