//
//  VideoModel.m
//  Avideo
//
//  Created by yun7_mac on 2019/3/7.
//  Copyright © 2019 XKX. All rights reserved.
//

#import "VideoModel.h"

@implementation VideoModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"videoid" : @"id"
             };
}

- (NSString *)thumb_url{
    if (StrValid(_thumb_url) && [_thumb_url hasPrefix:@"http"]) {
        return _thumb_url;
    }else{
        return [NSString stringWithFormat:@"%@/%@",[configManager appConfig].file_server_domain,_thumb_url];
    }
}

- (NSString *)play_url{
    if (StrValid(_play_url) && [_play_url hasPrefix:@"http"]) {
        return _play_url;
    }else{
        return [NSString stringWithFormat:@"%@/%@",[configManager appConfig].file_server_domain,_play_url];
    }
}

- (NSString *)download_url{
    if (StrValid(_download_url) && [_download_url hasPrefix:@"http"]) {
        return _download_url;
    }else{
        return [NSString stringWithFormat:@"%@/%@",[configManager appConfig].file_server_domain,_download_url];
    }
}

@end
