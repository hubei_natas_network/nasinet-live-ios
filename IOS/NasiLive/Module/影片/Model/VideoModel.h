//
//  VideoModel.h
//  Avideo
//
//  Created by yun7_mac on 2019/3/7.
//  Copyright © 2019 XKX. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoModel : NSObject

@property (nonatomic,assign) long long  videoid;
@property (copy, nonatomic) NSString    *title;
@property (copy, nonatomic) NSString    *sub_title;
@property (copy, nonatomic) NSString    *thumb_url;
@property (copy, nonatomic) NSString    *play_url;
@property (copy, nonatomic) NSString    *download_url;
@property (copy, nonatomic) NSString    *keyword;
@property (nonatomic,assign) long long  click_count; //播放量
@property (nonatomic,assign) long long  collect_count; //收藏量
@property (assign, nonatomic) int       like_count;
@property (assign, nonatomic) int       download_count;

@property (assign, nonatomic) BOOL      allow_download;

@property (assign, nonatomic) BOOL      liked;
@property (assign, nonatomic) BOOL      collected;

@property (copy, nonatomic) NSString    *create_time;

@end

NS_ASSUME_NONNULL_END
