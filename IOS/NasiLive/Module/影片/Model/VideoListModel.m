//
//  ViewListModel.m
//  MiyouBBS
//
//  Created by 陈曌然 on 2021/3/12.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "VideoListModel.h"
#import "VideoModel.h"

@implementation VideoListModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"videoid" : @"id"
             };
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"recVideos":[VideoModel class]
    };
}
@end
