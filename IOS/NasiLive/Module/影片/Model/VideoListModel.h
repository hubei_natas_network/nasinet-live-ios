//
//  ViewListModel.h
//  MiyouBBS
//
//  Created by 陈曌然 on 2021/3/12.
//  Copyright © 2021 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoListModel : NSObject
@property (nonatomic,assign) long long  videoid;

@property (copy, nonatomic) NSString    *title;
@property (copy, nonatomic) NSString    *sub_title;
@property (copy, nonatomic) NSArray     *recVideos ;
@end

NS_ASSUME_NONNULL_END
