//
//  VideoRootViewController.m
//  Nasi
//
//  Created by yun on 2021/03/06.
//  Copyright © 2019 yun7. All rights reserved.
//

#import "VideoRootViewController.h"
#import "MomentListViewController.h"
#import "PublishMomentViewController.h"
#import "PublishTextMomentViewController.h"
#import "DownloadViewController.h"
#import "VideoListViewController.h"
#import "MomentListViewController.h"

#import "SearchViewController.h"

#import "PopTextNotificationView.h"
#import "PopImgNotificationView.h"
#import "UpdateNotificationView.h"
#import "HomeMenuView.h"

#import "H5ViewController.h"

#import "XLPageViewController.h"
#import "VideoNewHomeController.h"


@interface VideoRootViewController ()<XLPageViewControllerDelegate, XLPageViewControllerDataSrouce, PopNotificationViewDelegate>{
    UIView *selTypeView;
    UIView *shadowView;
    HomeMenuView *menuView;
    
    NSMutableArray  *pageTitleArr;
    NSMutableArray  *cateArray;
}

@property (strong, nonatomic) PopNotificationView *popNotificationView;
@property (strong, nonatomic)XLPageViewController *pageViewController ;
@end

@implementation VideoRootViewController

- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self shadowTap];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pageTitleArr = [NSMutableArray array];
    cateArray = [NSMutableArray array];
    [pageTitleArr addObject:@"推荐"];
    for (NSDictionary *dict in configManager.videoCategorys) {
        if ([dict[@"is_homenav"] intValue] == 1) {
            [pageTitleArr addObject:dict[@"title"]];
            [cateArray addObject:dict];
        }
    }
    
    [self createUI];
    
    if ([userManager isLogined]) {
        [userManager refreshFromServer];
    }
    [self checkVersionUpdate];
}

- (void)createUI{
    

  
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, kStatusBarHeight + 40)];
    [self.view addSubview:headerView];
    headerView.backgroundColor = MAIN_COLOR ;

    
    UIButton *downloadBtn = [[UIButton alloc]init];
    [downloadBtn addTarget:self action:@selector(downloadBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [downloadBtn setImage:IMAGE_NAMED(@"home_btn_download") forState:UIControlStateNormal];
    [headerView addSubview:downloadBtn];
    [downloadBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(34);
        make.top.mas_equalTo(kStatusBarHeight + 6);
        make.right.mas_equalTo(-10);
    }];
    
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.backgroundColor = RGB(253, 190, 65);
    searchBtn.layer.cornerRadius = 5;
    [searchBtn setImage:IMAGE_NAMED(@"ic_search") forState:UIControlStateNormal];
    [searchBtn setTitle:@"搜索帖子/视频/用户" forState:UIControlStateNormal];
    [searchBtn setTitleColor:[UIColor colorWithHexString:@"#FFEAC0"] forState:UIControlStateNormal];
    searchBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [searchBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [searchBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    [searchBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 22, 0, 0)];
    [searchBtn addTarget:self action:@selector(searchBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:searchBtn];
    [searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(15);
        make.top.mas_equalTo(kStatusBarHeight + 6);
        make.height.mas_equalTo(34);
        make.right.equalTo(downloadBtn.mas_left).offset(-10);
    }];
    
    
    XLPageViewControllerConfig *config = [XLPageViewControllerConfig defaultConfig];
    config.separatorLineHidden = YES;
    config.titleNormalColor   = [UIColor whiteColor];
    config.titleSelectedColor = [UIColor whiteColor];
    config.titleNormalFont = FFont15;
    config.titleViewBackgroundColor = MAIN_COLOR ;
    config.titleSelectedFont = [UIFont boldSystemFontOfSize:18];
    config.titleSpace = 15.f;
    
    config.titleViewHeight = 44;
    config.titleViewInset = UIEdgeInsetsMake(5, 15, 0, 44);
    
    //    config.shadowLineColor = MAIN_COLOR;
    //    config.shadowLineWidth = 15;
    config.shadowLineHidden = YES;
    
    XLPageViewController *pageViewController = [[XLPageViewController alloc] initWithConfig:config];
   
    pageViewController.view.frame = CGRectMake(0, headerView.height, KScreenWidth, KScreenHeight - headerView.height - kTabBarHeight);
    pageViewController.delegate = self;
    pageViewController.dataSource = self;
    self.pageViewController = pageViewController ;
    [self.view addSubview:pageViewController.view];
    [self addChildViewController:pageViewController];
    [pageViewController setSelectedIndex:0];
    
    
    UIView *menuView = [[UIView alloc]init];
    menuView.backgroundColor = MAIN_COLOR;
    [self.view addSubview:menuView];
    [menuView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(44);
        make.right.offset(0);
        make.top.equalTo(headerView.mas_bottom);
    }];
    UIButton *menuBtn = [[UIButton alloc]init];
    [menuBtn addTarget:self action:@selector(publishBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [menuBtn setImage:IMAGE_NAMED(@"ic_home_menu") forState:UIControlStateNormal];
    [menuView addSubview:menuBtn];
    [menuBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.mas_equalTo(44);
        make.center.equalTo(menuView);
    }];
}

- (void)loadPopAd{
    [CommonManager POST:@"config/getHomePopAd" parameters:nil success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            AdModel *model = [AdModel yy_modelWithDictionary:responseObject[@"data"]];
            if (model && model.image_url.length > 0) {
                self.popNotificationView = [PopImgNotificationView showInWindowWithAdModel:model delegate:self];
            }else if (model){
                self.popNotificationView = [PopTextNotificationView showInWindowWithAdModel:model delegate:self];
            }
        }
    } failure:nil];
}

- (void)checkVersionUpdate{
    int newVersionInt = [[configManager.appConfig.version_ios stringByReplacingOccurrencesOfString:@"." withString:@""] intValue];
    int currentVersionInt = [[[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] stringByReplacingOccurrencesOfString:@"." withString:@""] intValue];
    if (currentVersionInt < newVersionInt) {
        AdModel *adModel = [[AdModel alloc]init];
        adModel.title = configManager.appConfig.update_info_ios;
        adModel.jump_url = configManager.appConfig.dl_web_url;
        adModel.jump_type = AdJumpTypeInBrowser;
        self.popNotificationView = [UpdateNotificationView showInWindowWithAdModel:adModel delegate:self];
        kWeakSelf(self);
        self.popNotificationView.onCloseClick = ^{
            [weakself loadPopAd];
        };
    }else{
        [self loadPopAd];
    }
}


- (void)publishBtnClick{
    if (![CommonManager checkAndLogin]) {
        return;
    }
    if (!shadowView) {
        shadowView = [[UIView alloc]initWithFrame:self.view.bounds];
        shadowView.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
        shadowView.userInteractionEnabled = YES;
        shadowView.alpha = 0;
        shadowView.hidden = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shadowTap)];
        [shadowView addGestureRecognizer:tap];
        [self.view addSubview:shadowView];
    }
    
    if (!menuView) {
        menuView = [[HomeMenuView alloc]initWithFrame:CGRectMake(0, kStatusBarHeight + 40 +44, kScreenWidth, 0)];
        [self.view addSubview:menuView];
    }
    
    CGFloat h = 150;

    
    [UIView animateWithDuration:0.2f animations:^{
        self->menuView.height = h;
        self->menuView.alpha = 1;
        self->shadowView.hidden = NO;
        self->shadowView.alpha = 1;
    }];
}

- (void)downloadBtnClick{
    [self.navigationController pushViewController:[[DownloadViewController alloc]init] animated:YES];
}

- (void)shadowTap{
    menuView.height = 0;
    menuView.alpha = 0;
    shadowView.hidden = YES;
    shadowView.alpha = 0;
}

- (void)publishTextBtnClick{
    PublishTextMomentViewController *vc = [[PublishTextMomentViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}


- (void)searchBtnClick{
    SearchViewController *vc = [[SearchViewController alloc]init];
    [self.navigationController pushViewController:vc animated:NO];
}

- (UIButton *)createPublishBtnWithImageName:(NSString *)imageName title:(NSString *)title frame:(CGRect)frame selector:(nonnull SEL)selector{
    UIButton *publishTextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [publishTextBtn setImage:IMAGE_NAMED(imageName) forState:UIControlStateNormal];
    [publishTextBtn setTitle:title forState:UIControlStateNormal];
    [publishTextBtn setTitleColor:[UIColor colorWithHexString:@"444444"] forState:UIControlStateNormal];
    publishTextBtn.titleLabel.font = [UIFont systemFontOfSize:15 weight:UIFontWeightMedium];
    [publishTextBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    [publishTextBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [publishTextBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    publishTextBtn.frame = frame;
    [publishTextBtn addTarget:self action:selector forControlEvents:UIControlEventTouchUpInside];
    return publishTextBtn;
}

#pragma mark - —————————————————— XLPageViewControllerDataSrouce ——————————————————————————

//根据index创建对应的视图控制器，每个试图控制器只会被创建一次。
- (UIViewController *)pageViewController:(XLPageViewController *)pageViewController viewControllerForIndex:(NSInteger)index{
    if (index == 0){
        VideoNewHomeController *vc = [[VideoNewHomeController alloc]init];
        return vc;
    }else{
        VideoListViewController *vc = [[VideoListViewController alloc]init];
        vc.isHideNav = YES;
        vc.isScreen = NO ;
        vc.cateDict = cateArray[index-1];
        return vc;
    }
}

//根据index返回对应的标题
- (NSString *)pageViewController:(XLPageViewController *)pageViewController titleForIndex:(NSInteger)index{
    return pageTitleArr[index];
}

//返回分页数
- (NSInteger)pageViewControllerNumberOfPage{
    return pageTitleArr.count;
}

- (void)pageViewController:(XLPageViewController *)pageViewController didSelectedAtIndex:(NSInteger)index{
    
}

#pragma mark - —————————————————— PopNotificationViewDelegate ——————————————————————————
- (void)popNotificationViewClick:(AdModel *)model{
    [self.popNotificationView hide];
    if (model.jump_type == AdJumpTypeInApp) {
        H5ViewController *vc = [[H5ViewController alloc]init];
        vc.href = model.jump_url;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:model.jump_url] options:@{} completionHandler:nil];
    }
}

@end
