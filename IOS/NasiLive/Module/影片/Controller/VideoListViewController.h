//
//  VideoListViewController.h
//  MiyouBBS
//
//  Created by 陈曌然 on 2021/3/12.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "RootViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface VideoListViewController : RootViewController

@property (assign, nonatomic) BOOL isHideNav;

@property (strong, nonatomic) NSDictionary *cateDict;

@property (assign, nonatomic) BOOL isScreen;
@end

NS_ASSUME_NONNULL_END
