//
//  VideoNewHomeController.m
//  NasiBBS
//
//  Created by kevin on 2021/3/24.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "VideoNewHomeController.h"
#import "VideoListViewController.h"
#import "VideoInfoViewController.h"

#import "VideoHomePageHeaderView.h"
#import "VideoListViewCell.h"
#import "VideoSectionReusableView.h"

#import "AdModel.h"
#import "GameModel.h"
#import "VideoListModel.h"


@interface VideoNewHomeController ()  <UICollectionViewDelegate,UICollectionViewDataSource>
{
    
}
@property (nonatomic , strong) NSMutableArray       * videoDataSource ;

@property (nonatomic, strong) VideoHomePageHeaderView       *headerView;

@end

@interface VideoNewHomeController ()

@end

@implementation VideoNewHomeController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpUI];
}

-(void)setUpUI
{
   
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc]init];
    flow.scrollDirection = UICollectionViewScrollDirectionVertical ;

    flow.sectionInset = UIEdgeInsetsMake(15, 15, 15, 15);
    flow.headerReferenceSize  = CGSizeMake(kScreenWidth, 40);
    
    CGFloat height = kScreenHeight - kTabBarHeight - kTopHeight-30 ;
    self.collectionView = [self createCollectionViewWithFrame:CGRectMake(0, 0, kScreenWidth, height) flowLayout:flow];
    self.collectionView.delegate   = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.contentInset  = UIEdgeInsetsMake(0, 0, 10, 0);
    [self.view addSubview:self.collectionView];
    [self.collectionView registerNib:[UINib nibWithNibName:@"VideoListViewCell" bundle:nil] forCellWithReuseIdentifier:@"VideoListViewCell"];
  
    [self.collectionView registerNib:[UINib nibWithNibName:@"VideoSectionReusableView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"VideoSectionReusable"];
    [self.collectionView registerClass:[VideoHomePageHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"VideoHeader"];
    self.view.backgroundColor           = [UIColor clearColor];
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self removeCollectionViewMJFooter];
    [self setupCollectionViewMJHeader];
    [self.collectionView.mj_header beginRefreshing];
}
- (void)headerRereshing{
    
    [self requestVideo];
    
}

- (void)requestVideo{
    [CommonManager POST:@"video/getHomeVideos" parameters:nil success:^(id responseObject) {
        [commonManager hideAnimateHud];
        [self.collectionView.mj_header endRefreshing];
        if (RESP_SUCCESS(responseObject)) {
            
            NSArray *videoOneAry =  responseObject[@"data"][@"recent"] ;
            NSMutableDictionary  *  dict=  [NSMutableDictionary dictionary];
            [dict setValue:@"最新更新" forKey:@"title"];
            [dict setValue:videoOneAry forKey:@"recVideos"];
            
            VideoListModel *model =  [VideoListModel yy_modelWithJSON:dict] ;
            NSArray *viedeoTwo = [NSArray yy_modelArrayWithClass:[VideoListModel class] json:responseObject[@"data"][@"cates"]];
             NSMutableArray *dataAry =[NSMutableArray array];
            dataAry     = [viedeoTwo mutableCopy];
            [dataAry insertObject:model atIndex:0];
            [dataAry insertObject:@[] atIndex:0];//添加头部视图位置
            
            self.videoDataSource = dataAry ;
            [self.collectionView reloadData];
            
        }else{
            RESP_SHOW_ERROR_MSG(responseObject);
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        [self.tableView.mj_header endRefreshing];
        RESP_FAILURE;
    }];
}
- (void)requestHeaderData{
    [CommonManager POST:@"home/getRecData" parameters:nil success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {

            NSArray *adAry   = [NSArray yy_modelArrayWithClass:[AdModel class] json:responseObject[@"data"][@"ads"]];
            NSArray *gameAry = [NSArray yy_modelArrayWithClass:[GameModel class] json:responseObject[@"data"][@"games"]];
            self.headerView.gameArray = [gameAry copy] ;
            self.headerView.adsArray = [adAry copy] ;
            
            
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:nil];
}




#pragma mark -------------------- UICollectionViewDelegate  --------------------------------

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.videoDataSource.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    VideoListModel *model    =  self.videoDataSource[section];
    return model.recVideos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section >0) {
        VideoListViewCell   *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"VideoListViewCell" forIndexPath:indexPath];
        VideoListModel *model    =   self.videoDataSource[indexPath.section];
        VideoModel *subModel     =   model.recVideos[indexPath.row];
        cell.model               = subModel;
        return cell;
    
    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section >0) {
        VideoListModel *model    =   self.videoDataSource[indexPath.section];
        VideoModel *subModel     =   model.recVideos[indexPath.row];
        VideoInfoViewController *vc = [[VideoInfoViewController alloc]init];
        vc.model = subModel;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 14;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 9;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return CGSizeMake(0, 0);
    }
    
    if (indexPath.section>1) {
        CGFloat itemW = (kScreenWidth - 30 - 9)/2;
        CGFloat itemH = itemW * 94/168 + 7 + 14;
        return CGSizeMake(itemW, itemH);
    }else{
        CGFloat itemW = (kScreenWidth - 40 - 9)/3;
        CGFloat itemH = itemW * 147/109 + 7 + 14;
        return CGSizeMake(itemW, itemH);
    }
    
   
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
  
    if (indexPath.section == 0) {
        VideoHomePageHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"VideoHeader" forIndexPath:indexPath];
        header.backgroundColor = [UIColor clearColor];
        self.headerView  = header ;
        [self requestHeaderData];
        return header ;
    }
    VideoSectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"VideoSectionReusable" forIndexPath:indexPath];
    VideoListModel *model    =  self.videoDataSource[indexPath.section];
    header.title.text = model.title    ;
    kWeakSelf(self)
    header.clickBlock = ^{
        if (model.recVideos.count >0) {
            VideoListViewController *vc = [[VideoListViewController alloc]init];
            vc.cateDict = @{@"id":@(model.videoid),@"title":model.title};
            vc.isScreen = YES ;
           [ weakself.navigationController pushViewController:vc animated:YES];

        }
       
    };
    return header;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGSizeMake(KScreenWidth, kVideoHomeHeaderHeight);;
    }
    return CGSizeMake(KScreenWidth, 40);
}



- (NSMutableArray *)videoDataSource
{
    if (!_videoDataSource) {
        _videoDataSource = [NSMutableArray array];
    }
    return _videoDataSource ;
}

@end
