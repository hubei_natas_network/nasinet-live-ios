//
//  VideoInfoViewController.h
//  MiyouBBS
//
//  Created by yun11 on 2021/3/8.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "RootViewController.h"

NS_ASSUME_NONNULL_BEGIN

@class VideoModel;

@interface VideoInfoViewController : RootViewController

@property (strong, nonatomic) VideoModel *model;

@end

NS_ASSUME_NONNULL_END
