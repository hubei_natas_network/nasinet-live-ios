//
//  VideoInfoViewController.m
//  MiyouBBS
//
//  Created by yun11 on 2021/3/8.
//  Copyright © 2021 yun7. All rights reserved.
//

#import "VideoInfoViewController.h"
#import "DownloadViewController.h"
#import "VipCenterViewController.h"
#import "InviteViewController.h"
#import "H5ViewController.h"

#import "VipOpenPopView.h"

#import "HJDownloadManager.h"
#import "SJVideoPlayer.h"

#import "VideoModel.h"
#import "AdModel.h"

#define VideoItemW (kScreenWidth - 15*2 - 9)/2
#define VideoItemH (VideoItemW * 94/168 + 12 + 7)

@interface VideoInfoViewController(){
}

@property (strong, nonatomic) SJVideoPlayer     *videoPlayer;
@property (strong, nonatomic) UIScrollView      *scrollView;
@property (strong, nonatomic) UIView            *recVideoView;
@property (strong, nonatomic) UIButton          *refreshBtn;
@property (strong, nonatomic) UIImageView       *maskView;
@property (strong, nonatomic) UIImageView       *adView;
@property (strong, nonatomic) UIImageView       *vipView;
@property (strong, nonatomic) UIButton          *backBtn;

@property (strong, nonatomic) UIButton          *likeBtn;
@property (strong, nonatomic) UIButton          *collectBtn;
@property (strong, nonatomic) UIButton          *downloadBtn;

@property (strong, nonatomic) AdModel           *adModel;
@property (strong, nonatomic) NSMutableArray    *recVideoArray;

@end

@implementation VideoInfoViewController

- (instancetype)init{
    if (self = [super init]) {
        self.StatusBarStyle = UIStatusBarStyleLightContent;
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.videoPlayer vc_viewDidAppear];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.videoPlayer vc_viewWillDisappear];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.videoPlayer vc_viewDidDisappear];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 隐藏导航栏
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [self creatreUI];
}

- (void)creatreUI{
    
    UIView *statusBarView = [[UIView alloc]init];
    [self.view addSubview:statusBarView];
    [statusBarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.leading.trailing.offset(0);
        make.height.mas_equalTo(kStatusBarHeight);
    }];
    statusBarView.backgroundColor = [UIColor blackColor];
    
    _videoPlayer = [SJVideoPlayer player];
    [self.view addSubview:_videoPlayer.view];
    [_videoPlayer.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(kStatusBarHeight);
        make.leading.trailing.offset(0);
        make.height.equalTo(self->_videoPlayer.view.mas_width).multipliedBy(9 / 16.0f);
    }];
    _videoPlayer.URLAsset.title = self.model.title;
    
    _scrollView = [[UIScrollView alloc]init];
    _scrollView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_scrollView];
    [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_videoPlayer.view.mas_bottom);
        make.left.right.bottom.offset(0);
    }];
    
    _vipView = [[UIImageView alloc]initWithImage:IMAGE_NAMED(@"videpage_vip_banner")];
    _vipView.frame = CGRectMake(11, 4, kScreenWidth - 22, (kScreenWidth - 22)*80.0/353);
    [_scrollView addSubview:_vipView];
    _vipView.userInteractionEnabled = YES;
    [_vipView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(vipBtnClick)]];
    
    CGFloat optBtnW = 18;
    _downloadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_downloadBtn setImage:IMAGE_NAMED(@"videopage_download") forState:UIControlStateNormal];
    [_downloadBtn setImage:IMAGE_NAMED(@"videopage_download_pre") forState:UIControlStateSelected];
    [_downloadBtn addTarget:self action:@selector(downloadBtnClick) forControlEvents:UIControlEventTouchUpInside];
    _downloadBtn.frame = CGRectMake(kScreenWidth - optBtnW - 14, _vipView.mj_maxY + 15, optBtnW, optBtnW);
    [_scrollView addSubview:_downloadBtn];
    
    _collectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_collectBtn setImage:IMAGE_NAMED(@"videopage_collect") forState:UIControlStateNormal];
    [_collectBtn setImage:IMAGE_NAMED(@"videopage_collect_pre") forState:UIControlStateSelected];
    [_collectBtn addTarget:self action:@selector(collectBtnClick) forControlEvents:UIControlEventTouchUpInside];
    _collectBtn.frame = CGRectMake(_downloadBtn.mj_x - optBtnW - 22, _vipView.mj_maxY + 15, optBtnW, optBtnW);
    [_scrollView addSubview:_collectBtn];
    
    _likeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_likeBtn setImage:IMAGE_NAMED(@"videopage_like") forState:UIControlStateNormal];
    [_likeBtn setImage:IMAGE_NAMED(@"videopage_like_pre") forState:UIControlStateSelected];
    [_likeBtn addTarget:self action:@selector(likeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    _likeBtn.frame = CGRectMake(_collectBtn.mj_x - optBtnW - 22, _vipView.mj_maxY + 15, optBtnW, optBtnW);
    [_scrollView addSubview:_likeBtn];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    [_scrollView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(14);
        make.right.equalTo(_likeBtn.mas_left).offset(-20);
        make.top.mas_equalTo(_vipView.mas_bottom).mas_offset(@17);
    }];
    titleLabel.text = self.model.title;
    titleLabel.numberOfLines = 0;
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.font = MEDSYSTEMFONT(15);
    
    UILabel *timeLabel = [[UILabel alloc]init];
    timeLabel.text = self.model.create_time;
    timeLabel.textColor = CFontColor2;
    timeLabel.font = FFont12;
    [_scrollView addSubview:timeLabel];
    [timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLabel);
        make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(@14);
    }];
    
    UILabel *viewCountLabel = [[UILabel alloc]init];
    viewCountLabel.textAlignment = NSTextAlignmentRight;
    viewCountLabel.text = [NSString stringWithFormat:@"%lld次播放",self.model.click_count];
    viewCountLabel.textAlignment = NSTextAlignmentRight;
    viewCountLabel.textColor = CFontColor2;
    viewCountLabel.font = FFont12;
    [_scrollView addSubview:viewCountLabel];
    [viewCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_downloadBtn.mas_right);
        make.top.mas_equalTo(titleLabel.mas_bottom).mas_offset(@10);
    }];
    
    _adView = [[UIImageView alloc]init];
    [_scrollView addSubview:_adView];
    [_adView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15);
        make.width.mas_equalTo(kScreenWidth - 30);
        make.height.mas_equalTo((kScreenWidth - 30)*120/345);
        make.top.mas_equalTo(timeLabel.mas_bottom).offset(18);
    }];
    _adView.backgroundColor = [UIColor clearColor];
    _adView.contentMode = UIViewContentModeScaleAspectFill;
    _adView.layer.masksToBounds = YES;
    _adView.userInteractionEnabled = YES;
    [_adView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(adTap)]];
    
    UILabel *recTitleLabel = [[UILabel alloc]init];
    recTitleLabel.text = @"相关推荐";
    recTitleLabel.textColor = CFontColor;
    recTitleLabel.font = MEDSYSTEMFONT(15);
    [_scrollView addSubview:recTitleLabel];
    [recTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(15);
        make.top.mas_equalTo(_adView.mas_bottom).mas_offset(@18);
    }];
    
    _recVideoView = [[UIView alloc]init];
    [_scrollView addSubview:_recVideoView];
    [_recVideoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(0);
        make.width.mas_equalTo(kScreenWidth);
        make.height.mas_equalTo(VideoItemH*3 + 12*2);
        make.top.equalTo(recTitleLabel.mas_bottom).offset(15);
    }];
    
    _refreshBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_refreshBtn setImage:IMAGE_NAMED(@"videopage_refresh") forState:UIControlStateNormal];
    [_refreshBtn setTitle:@"换一换" forState:UIControlStateNormal];
    [_refreshBtn setTitleColor:[UIColor colorWithHexString:@"#646464"] forState:UIControlStateNormal];
    [_refreshBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    _refreshBtn.titleLabel.font = MEDSYSTEMFONT(12);
    [_refreshBtn addTarget:self action:@selector(refreshBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [_scrollView addSubview:_refreshBtn];
    [_refreshBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_recVideoView);
        make.top.equalTo(_recVideoView.mas_bottom).offset(25);
        make.width.mas_equalTo(68);
        make.height.mas_equalTo(20);
    }];
   
    
    [self loadVideoInfo];
    [self loadAd];
    [self loadRecVideos];
    
    //是否已下载
    NSMutableArray *downloadModels = [kHJDownloadManager downloadModels];
    for (HJDownloadModel *dwModel in downloadModels) {
        if (dwModel.downloadID == self.model.videoid) {
            _downloadBtn.selected = YES;
        }
    }
    
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.scrollView.contentSize = CGSizeMake(kScreenWidth, self.refreshBtn.mj_maxY + 30);
}

//渲染相关视频
- (void)fillRecView{
    if (self.recVideoArray.count == 0) {
        return;
    }
    [self.recVideoView removeAllSubviews];
    for (int i = 0; i < self.recVideoArray.count; i++) {
        VideoModel *model = self.recVideoArray[i];
        UIView *itemView = [[UIView alloc]initWithFrame:CGRectMake(15 + (VideoItemW + 9) * (i%2), (VideoItemH + 12)*(i/2), VideoItemW, VideoItemH)];
        itemView.tag = i;
        itemView.userInteractionEnabled = YES;
        [itemView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(videoItemTap:)]];
        
        [self.recVideoView addSubview:itemView];
        
        UIImageView *thumbImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, VideoItemW, VideoItemW * 94/168)];
        [thumbImgView sd_setImageWithURL:[NSURL URLWithString:model.thumb_url] placeholderImage:IMAGE_NAMED(@"cover_loading")];
        thumbImgView.contentMode = UIViewContentModeScaleAspectFill;
        thumbImgView.layer.masksToBounds = YES;
        thumbImgView.layer.cornerRadius = 5;
        [itemView addSubview:thumbImgView];
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, thumbImgView.mj_maxY + 7, itemView.width, 12)];
        titleLabel.font = FFont12;
        titleLabel.textColor = CFontColor;
        titleLabel.text = model.title;
        [itemView addSubview:titleLabel];
    }
}

//读取播放地址 下载地址
- (void)loadVideoInfo{
    [commonManager showLoadingAnimateInWindow];
    [CommonManager POST:@"video/getVideoInfo" parameters:@{@"videoid":@(self.model.videoid)} success:^(id responseObject) {
        [commonManager hideAnimateHud];
        if (RESP_SUCCESS(responseObject)) {
            self.model = [VideoModel yy_modelWithDictionary:responseObject[@"data"]];
            self->_videoPlayer.assetURL = [NSURL URLWithString:self.model.play_url];
            [self->_videoPlayer play];
            self.likeBtn.selected = self.model.liked;
            self.collectBtn.selected = self.model.collected;
        }else{
          
            if (ValidStr(responseObject[@"msg"])) {
                [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
            }
        }
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        RESP_FAILURE;
    }];
}

//加载页面广告
- (void)loadAd{
    [CommonManager POST:@"ads/getPlayPageAd" parameters:nil success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.adModel = [AdModel yy_modelWithDictionary:responseObject[@"data"]];
            [self.adView sd_setImageWithURL:[NSURL URLWithString:self.adModel.image_url] placeholderImage:IMAGE_NAMED(@"cover_loading")];
        }else{
            if (ValidStr(responseObject[@"msg"])) {
                [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
            }
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

//加载相关影片
- (void)loadRecVideos{
    if (self.recVideoArray.count > 0) {
        //非第一次加载
        [commonManager showLoadingAnimateInView:self.view];
    }
    [CommonManager POST:@"video/getCorrelation" parameters:@{@"keyword":self.model.keyword} success:^(id responseObject) {
        if (self.recVideoArray.count > 0){
            [commonManager hideAnimateHud];
        }
        if (RESP_SUCCESS(responseObject)) {
            [self.recVideoArray removeAllObjects];
            [self.recVideoArray addObjectsFromArray:[NSArray yy_modelArrayWithClass:[VideoModel class] json:responseObject[@"data"]]];
            [self fillRecView];
        }else{
            if (ValidStr(responseObject[@"msg"])) {
                [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
            }
        }
    } failure:^(NSError *error) {
        if (self.recVideoArray.count > 0){
            [commonManager hideAnimateHud];
        }
        RESP_FAILURE;
    }];
}

- (void)refreshBtnClick{
    [self loadRecVideos];
}

- (void)downloadBtnClick{
    if (!self.model.allow_download) {
        [MBProgressHUD showTipMessageInView:@"该影片禁止下载"];
        return;
    }
    if (_downloadBtn.selected) {
        [self.navigationController pushViewController:[[DownloadViewController alloc]init] animated:YES];
    }else{
        HJDownloadModel *model = [[HJDownloadModel alloc] init];
        model.urlString = self.model.download_url;
        model.fileName = self.model.title;
        model.imgUrl = self.model.thumb_url;
        model.downloadID = self.model.videoid;
        [kHJDownloadManager startWithDownloadModel:model];
        self->_downloadBtn.selected = YES;
        [MBProgressHUD showTipMessageInView:@"已加入下载队列，您可前往下载管理查看"];
    }
}

- (void)likeBtnClick{
    NSDictionary *params = @{
        @"videoid":@(self.model.videoid),
        @"type":self.model.liked ? @"0":@"1"
    };
    [CommonManager POST:@"video/likevideo" parameters:params success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.model.liked = !self.model.liked;
            self->_likeBtn.selected = !self->_likeBtn.selected;
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (void)collectBtnClick{
    NSDictionary *params = @{
        @"videoid":@(self.model.videoid),
        @"type":self.model.collected ? @"0":@"1"
    };
    [CommonManager POST:@"video/collectvideo" parameters:params success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.model.collected = !self.model.collected;
            self->_collectBtn.selected = !self->_collectBtn.selected;
        }else{
            [MBProgressHUD showTipMessageInView:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        RESP_FAILURE;
    }];
}

- (void)vipBtnClick{
    [VipOpenPopView showInView:self.view];
}

- (void)adTap{
    if (!self.adModel) {
        return;
    }
    if (self.adModel.jump_type == AdJumpTypeInApp) {
        H5ViewController *vc = [[H5ViewController alloc]init];
        vc.href = self.adModel.jump_url;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.adModel.jump_url] options:@{} completionHandler:nil];
    }
}

- (void)videoItemTap:(UIGestureRecognizer *)gest{
    VideoModel *model = self.recVideoArray[gest.view.tag];
    VideoInfoViewController *vc = [[VideoInfoViewController alloc]init];
    vc.model = model;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSMutableArray *)recVideoArray{
    if (!_recVideoArray) {
        _recVideoArray = [NSMutableArray arrayWithCapacity:6];
    }
    return _recVideoArray;
}

@end
