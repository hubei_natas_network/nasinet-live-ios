//
//  BaseTableViewCell.h
//  Nasi
//
//  Created by yun on 2019/12/24.
//  Copyright © 2019 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NasiLiveSDK/NasiTableViewCell.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseTableViewCell : NasiTableViewCell

@end

NS_ASSUME_NONNULL_END
