//
//  H5ViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/4/17.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RootViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface H5ViewController : RootViewController

//是否隐藏导航栏
@property (assign, nonatomic) BOOL isNavHidden;

@property (copy, nonatomic) NSString *href;

@end

NS_ASSUME_NONNULL_END
