//
//  MainTabBarController.m
//  yun_7
//
//  Created by xhx on 2017/5/18.
//  Copyright © 2017年 xhx. All rights reserved.
//

#import "MainTabBarController.h"

#import "RootNavigationController.h"
#import "LiveHomeViewController.h"
#import "MessageHomeViewController.h"
#import "MineHomeViewController.h"
#import "MomentRootViewController.h"
#import "SVideoHomeViewController.h"
#import "GroupHomeViewController.h"
#import "LandscapeLiveViewController.h"
#import "PortraitLiveViewController.h"
#import "LiveBroadcastViewController.h"
#import "MlvbLiveBroadcastViewController.h"
#import "VideoRootViewController.h"

#import "UITabBar+CustomBadge.h"
#import "XYTabBar.h"

#import "StreamerAnimationView.h"

#import "StreamerModel.h"

#import <ImSDK.h>

@interface MainTabBarController ()<UITabBarControllerDelegate>{
    StreamerAnimationView               *streamerAnimationView;
    
    NSTimer                             *schedulerTimer;     //测试用定时器
}

@property (nonatomic,strong) NSMutableArray * VCS;//tabbar root VC

@end

@implementation MainTabBarController

  
- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    //初始化tabbar
    [self setUpTabBar];
    //添加子控制器
    [self setUpAllChildViewController];
    
    [[UITabBar appearance] setBackgroundColor:CNavBgColor];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateMessageNum:) name:KNotificationUpdateMessageNum object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(broadcastStreamer:) name:KNotificationBroadcastStreamer object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}


#pragma mark ————— 初始化TabBar —————
-(void)setUpTabBar{
    //设置背景色 去掉分割线
    [self setValue:[XYTabBar new] forKey:@"tabBar"];
    [self.tabBar setBackgroundColor:CNavBgColor];
    [self.tabBar setBackgroundImage:[UIImage new]];
    //通过这两个参数来调整badge位置
    //    [self.tabBar setTabIconWidth:29];
    //    [self.tabBar setBadgeTop:9];
    if(@available(iOS 13.0,*)){
        [[UITabBar appearance] setTintColor:MAIN_COLOR];
        [[UITabBar appearance] setUnselectedItemTintColor:CFontColor2];
    }
}

#pragma mark - ——————— 初始化VC ————————
- (void)setUpAllChildViewController{
    _VCS = @[].mutableCopy;
    
    VideoRootViewController *momentVC = [[VideoRootViewController alloc]init];
    [self setupChildViewController:momentVC title:@"首页" imageName:@"tab_home" seleceImageName:@"tab_home_sel"];
    
    SVideoHomeViewController *svVC = [[SVideoHomeViewController alloc]init];
    [self setupChildViewController:svVC title:@"短视频" imageName:@"tab_video" seleceImageName:@"tab_video_sel"];
    
    LiveHomeViewController *liveVC = [[LiveHomeViewController alloc]init];
    [self setupChildViewController:liveVC title:@"直播" imageName:@"tab_live" seleceImageName:@"tab_live_sel"];
    
    MomentRootViewController *groupVC = [[MomentRootViewController alloc]init];
    [self setupChildViewController:groupVC title:@"动态" imageName:@"tab_group" seleceImageName:@"tab_group_sel"];
    
    MineHomeViewController *MineVC = [[MineHomeViewController alloc]init];
    [self setupChildViewController:MineVC title:@"我的" imageName:@"tab_mine" seleceImageName:@"tab_mine_sel"];
    
    self.viewControllers = _VCS;
}

- (void)setupChildViewController:(UIViewController*)controller title:(NSString *)title imageName:(NSString *)imageName seleceImageName:(NSString *)selectImageName{
    controller.title = title;
    controller.tabBarItem.title = title;//跟上面一样效果
    controller.tabBarItem.image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    controller.tabBarItem.selectedImage = [[UIImage imageNamed:selectImageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    //未选中字体颜色
    [controller.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:CFontColor2,NSFontAttributeName:SYSTEMFONT(11.0f)} forState:UIControlStateNormal];
    
    //选中字体颜色
    [controller.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:MAIN_COLOR,NSFontAttributeName:SYSTEMFONT(11.0f)} forState:UIControlStateSelected];
    //包装导航控制器
    RootNavigationController *nav = [[RootNavigationController alloc]initWithRootViewController:controller];
    
//    [self addChildViewController:nav];
    [_VCS addObject:nav];
    
}


- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    //    NSLog(@"选中 %ld",tabBarController.selectedIndex);
}

- (void)setRedDotWithIndex:(NSInteger)index isShow:(BOOL)isShow{
    if (isShow) {
        [self.tabBar setBadgeStyle:kCustomBadgeStyleRedDot value:0 atIndex:index];
    }else{
        [self.tabBar setBadgeStyle:kCustomBadgeStyleNone value:0 atIndex:index];
    }
}

- (void)updateMessageNum:(NSNotification *)notification{
    __block int num = 0;
    [[V2TIMManager sharedInstance] getConversationList:0 count:1000000 succ:^(NSArray<V2TIMConversation *> *list, uint64_t nextSeq, BOOL isFinished) {
        for (V2TIMConversation *conv in list) {
            if (conv.type == V2TIM_C2C && ![conv.userID isEqualToString:[configManager appConfig].txim_broadcast]&& ![conv.userID isEqualToString:[configManager appConfig].txim_admin]) {
                num += conv.unreadCount;
            }
        }
    } fail:^(int code, NSString *desc) {
        
    }];
    
    if (num > 0) {
        [self.tabBar setBadgeStyle:kCustomBadgeStyleNumber value:num atIndex:3];
    }else{
        [self.tabBar setBadgeStyle:kCustomBadgeStyleNone value:0 atIndex:3];
    }
}

- (void)broadcastStreamer:(NSNotification *)notification{
    IMNotificationModel *notificationModel = [notification userInfo][@"data"];
    
    UIViewController *currentVc = [kAppDelegate getCurrentUIVC];
    if (([currentVc isKindOfClass:[LandscapeLiveViewController class]] || [currentVc isKindOfClass:[PortraitLiveViewController class]] || [currentVc isKindOfClass:[MlvbLiveBroadcastViewController class]]) && notificationModel.streamer.type != StreamerTypeAllChannelGift) {
        return;
    }
    if (!streamerAnimationView) {
        streamerAnimationView = [[StreamerAnimationView alloc]init];
        streamerAnimationView.delegate = self;
        [self.view addSubview:streamerAnimationView];
    }
    if (notificationModel.streamer != nil) {
        [streamerAnimationView addModels:notificationModel];
    }
    if(!streamerAnimationView.animating){
        [streamerAnimationView enAnimation];
    }
}



- (BOOL)shouldAutorotate {
    return [self.selectedViewController shouldAutorotate];
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return [self.selectedViewController supportedInterfaceOrientations];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}


@end
