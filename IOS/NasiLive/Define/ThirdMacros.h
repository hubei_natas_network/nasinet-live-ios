//
//  ThirdMacros.h
//  yun_7
//
//  Created by xhx on 2017/5/18.
//  Copyright © 2017年 xhx. All rights reserved.
//

//第三方配置

#ifndef ThirdMacros_h
#define ThirdMacros_h

#define NasiSDK_AccessKey           @""
#define NasiSDK_SecretKey           @""

// 友盟统计
//#define UMengKey @"59281b4a5312dd3f5f0005ed"


#define kTXLicense                  @""
#define kTXLicenseKey               @""

#define kTXUGCLicense               @""
#define kTXUGCLicenseKey            @""

#endif /* ThirdMacros_h */
