//
//  CommonMacros.h
//  yun_7
//
//  Created by xhx on 2017/5/31.
//  Copyright © 2017年 xhx. All rights reserved.
//

//全局标记字符串，用于 通知 存储

#ifndef CommonMacros_h
#define CommonMacros_h

#pragma mark - ——————— 用户相关 ————————
//登录状态改变通知
#define KNotificationLoginStateChange @"loginStateChange"

//自动登录成功
#define KNotificationAutoLoginSuccess @"KNotificationAutoLoginSuccess"

//被踢下线
#define KNotificationOnKick @"KNotificationOnKick"

//下载完成通知
#define KNotificationDownloadComplate @"KNotificationDownloadComplate"

//用户信息缓存 名称
#define KUserCacheName @"KUserCacheName"

//配置信息缓存 名称
#define KConfigCacheName @"KConfigCacheName"

//TXCOS信息缓存 名称
#define KTxCosCacheName @"KTxCosCacheName"

//用户标签缓存 名称
#define KUserTagsCacheName @"KUserTagsCacheName"

//直播分类缓存 名称
#define KLiveCategorysCacheName @"KLiveCategorysCacheName"

//影片分类缓存 名称
#define KVideoCategorysCacheName @"KVideoCategorysCacheName"

//用户model缓存
#define KUserModelCache @"KUserModelCache"

//配置信息缓存
#define KConfigCache @"KConfigCache"

//TXCOS信息缓存
#define KTxCosCache @"KTxCosCache"

//用户标签缓存
#define KUserTagsCache @"KUserTagsCache"

//直播分类标签缓存
#define KLiveCategorysCache @"KLiveCategorysCache"
//影片分类标签缓存
#define KVideoCategorysCache @"KVideoCategorysCache"



#pragma mark - ——————— 网络状态相关 ————————

//网络状态变化
#define KNotificationNetWorkStateChange @"KNotificationNetWorkStateChange"


//IM通知
#define KNotificationInRoomIM @"KNotificationInRoomIM"
#define KNotificationGroupEvent @"KNotificationGroupEvent"
#define KNotificationGiftAnimation @"KNotificationGiftAnimation"
#define KNotificationUpdateMessageNum @"KNotificationUpdateMessageNum"
#define KNotificationBroadcastStreamer @"KNotificationBroadcastStreamer"

#define KNotificationShowUserCard @"KNotificationShowUserCard"

#define KNotificationPaySuccess @"KNotificationPaySuccess"
#define KNotificationPayFail @"KNotificationPayFail"

#define KNotificationLoginOut @"KNotificationLoginOut"

#define KEnterRoomMessage @"进入直播间"
#endif /* CommonMacros_h */
