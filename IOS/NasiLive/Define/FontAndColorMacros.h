//
//  FontAndColorMacros.h
//  yun_7
//
//  Created by xhx on 2017/5/18.
//  Copyright © 2017年 xhx. All rights reserved.
//

//字体大小和颜色配置

#ifndef FontAndColorMacros_h
#define FontAndColorMacros_h

#pragma mark -  间距区

//默认间距
#define KNormalSpace 12.0f

#pragma mark -  颜色区
//主题色 导航栏颜色
#define MAIN_COLOR  [UIColor colorWithHexString:@"#FFB21C"]
#define CNavBgColor  [UIColor colorWithHexString:@"FFFFFF"]
#define CNavBgFontColor  [UIColor colorWithHexString:@"060606"]

//默认页面背景色
#define CViewBgColor [UIColor colorWithHexString:@"ffffff"]

//分割线颜色
#define CLineColor [UIColor colorWithHexString:@"F4F4F4"]

//主字色
#define CFontColor [UIColor colorWithHexString:@"333333"]

//灰色字体
#define CFontColorGray [UIColor colorWithHexString:@"666666"]

//浅灰色字体
#define CFontColorLightGray [UIColor colorWithHexString:@"B4B4B4"]

//次级字色
#define CFontColor1 [UIColor colorWithHexString:@"F6FBFF"]

//再次级字色
#define CFontColor2 [UIColor colorWithHexString:@"8E8F91"]


#pragma mark -  字体区

//emoji规则
#define emojiPattern @"\\[\\w+\\]"

//字体
#define SYS_Font(a) [UIFont systemFontOfSize:(a)]

#define FFont12 [UIFont systemFontOfSize:12.0f]
#define FFont13 [UIFont systemFontOfSize:13.0f]
#define FFont14 [UIFont systemFontOfSize:14.0f]
#define FFont15 [UIFont systemFontOfSize:15.0f]
#define FFont16 [UIFont systemFontOfSize:16.0f]

#define RGB(r,g,b)          [UIColor colorWithRed:(r)/255.f \
green:(g)/255.f \
blue:(b)/255.f \
alpha:1.f]


#define RGB_COLOR(_STR_,a) ([UIColor colorWithRed:[[NSString stringWithFormat:@"%lu", strtoul([[_STR_ substringWithRange:NSMakeRange(1, 2)] UTF8String], 0, 16)] intValue] / 255.0 green:[[NSString stringWithFormat:@"%lu", strtoul([[_STR_ substringWithRange:NSMakeRange(3, 2)] UTF8String], 0, 16)] intValue] / 255.0 blue:[[NSString stringWithFormat:@"%lu", strtoul([[_STR_ substringWithRange:NSMakeRange(5, 2)] UTF8String], 0, 16)] intValue] / 255.0 alpha:a])

#endif /* FontAndColorMacros_h */
