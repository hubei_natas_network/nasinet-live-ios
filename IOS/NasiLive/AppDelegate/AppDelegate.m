//
//  AppDelegate.m
//  yun_7
//
//  Created by yun_7 on 2018/8/13.
//  Copyright © 2018年 yun_7. All rights reserved.
//

#import "AppDelegate.h"

#import "IQKeyboardManager.h"
#import "AdViewController.h"

#import "ImSDK.h"
#import <TUIKit.h>
#import <QCloudCOSXML.h>
#import <QCloudCore.h>

#import <AFNetworking.h>

#import <AlipaySDK/AlipaySDK.h>

#import <ShareSDK/ShareSDK.h>

#import "WXApi.h"

@import TXLiteAVSDK_Professional;

@interface AppDelegate ()<WXApiDelegate,TIMMessageListener,TIMGroupEventListener,TIMUserStatusListener,QCloudSignatureProvider,QCloudCredentailFenceQueueDelegate>

@property (nonatomic, strong) QCloudCredentailFenceQueue* credentialFenceQueue;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //初始化window
    [self initWindow];
    //初始化app服务
    [self initService];
    //初始化用户信息
    [userManager loadUserInfo];
    
    //初始化广告页面
    AdViewController *adVc = [[AdViewController alloc]init];
    self.window.rootViewController = adVc;
    
    //网络监听
    [self monitorNetworkStatus];
    
    IQKeyboardManager *keyboardManager = [IQKeyboardManager sharedManager]; // 获取类库的单例变量
    
    keyboardManager.enable = YES; // 控制整个功能是否启用
    
    keyboardManager.shouldResignOnTouchOutside = YES; // 控制点击背景是否收起键盘
    
    keyboardManager.shouldToolbarUsesTextFieldTintColor = YES; // 控制键盘上的工具条文字颜色是否用户自定义
    
    keyboardManager.toolbarManageBehaviour = IQAutoToolbarBySubviews; // 有多个输入框时，可以通过点击Toolbar 上的“前一个”“后一个”按钮来实现移动到不同的输入框
    
    keyboardManager.enableAutoToolbar = YES; // 控制是否显示键盘上的工具条
    
    keyboardManager.shouldShowToolbarPlaceholder = YES; // 是否显示占位文字
    
    keyboardManager.placeholderFont = [UIFont boldSystemFontOfSize:17]; // 设置占位文字的字体
    
    keyboardManager.keyboardDistanceFromTextField = 10.0f; // 输入框距离键盘的距离

    return YES;
}

- (void)launchSdk{
    
    [ShareSDK registPlatforms:^(SSDKRegister *platformsRegister) {
        
        //更新到4.3.3或者以上版本，微信初始化需要使用以下初始化
        [platformsRegister setupWeChatWithAppId:configManager.appConfig.wx_appid appSecret:@"" universalLink:[NSString stringWithFormat:@"%@wx_conn/",configManager.appConfig.universal_link]];
        //QQ
        [platformsRegister setupQQWithAppId:configManager.appConfig.qq_appid appkey:configManager.appConfig.qq_appkey enableUniversalLink:YES universalLink:[NSString stringWithFormat:@"%@qq_conn/%@/",configManager.appConfig.universal_link,configManager.appConfig.qq_appid]];
        
    }];
    
    //向微信注册
    [WXApi registerApp:configManager.appConfig.wx_appid universalLink:[NSString stringWithFormat:@"%@wx_conn/",configManager.appConfig.universal_link]];
//
    //腾讯IM UIKit
    [[TUIKit sharedInstance] setupWithAppId:[configManager.appConfig.im_sdkappid integerValue]];
    TUIKitConfig *config = [TUIKitConfig defaultConfig];
    // 修改默认头像
    config.defaultAvatarImage = [UIImage imageNamed:@"ic_avatar"];
    config.avatarType = TAvatarTypeRounded;
    
    //注册腾讯IM
    TIMManager * manager = [TIMManager sharedInstance];
    
    TIMSdkConfig *timConfig = [[TIMSdkConfig alloc]init];
    timConfig.sdkAppId = [configManager.appConfig.im_sdkappid intValue];
    timConfig.disableLogPrint = YES;
    [manager initSdk:timConfig];
    [manager addMessageListener:self];
    
    TIMUserConfig *timUserConfig = [[TIMUserConfig alloc]init];
    timUserConfig.userStatusListener = self;
    [manager setUserConfig:timUserConfig];
    
    //腾讯云存储
    QCloudServiceConfiguration* configuration = [QCloudServiceConfiguration new];
    configuration.appID = configManager.appConfig.qcloud_appid;
    configuration.signatureProvider = self;
    QCloudCOSXMLEndPoint *endpoint = [[QCloudCOSXMLEndPoint alloc] init];
    endpoint.regionName = configManager.appConfig.cos_region;//服务地域名称，可用的地域请参考注释
    endpoint.useHTTPS = YES;
//    endpoint.suffix = [NSString stringWithFormat:@"pic.%@.myqcloud.com",configManager.appConfig.cos_region];
    configuration.endpoint = endpoint;
    [QCloudCOSXMLService registerDefaultCOSXMLWithConfiguration:configuration];
    [QCloudCOSTransferMangerService registerDefaultCOSTransferMangerWithConfiguration:configuration];
    
    self.credentialFenceQueue = [QCloudCredentailFenceQueue new];
    self.credentialFenceQueue.delegate = self;
    
    [TXLiveBase setLicenceURL:kTXLicense key:kTXLicenseKey];
    [TXUGCBase setLicenceURL:kTXUGCLicense key:kTXUGCLicenseKey];
}

- (void)launchSdkNeedLogin{
    //腾讯IM
    TIMLoginParam * tim_login_param = [[TIMLoginParam alloc ]init];
    // identifier 为用户名
    tim_login_param.identifier = [NSString stringWithFormat:@"%lld",userManager.curUserInfo.userid];
    //userSig 为用户登录凭证
    tim_login_param.userSig = userManager.curUserInfo.txim_sign;
    //appidAt3rd App 用户使用 OAuth 授权体系分配的 Appid，在私有帐号情况下，填写与 SDKAppID 一样
    tim_login_param.appidAt3rd = configManager.appConfig.im_sdkappid;
    [[TIMManager sharedInstance] login: tim_login_param succ:^(){
        NSLog(@"Login Succ");
        //更新tabbar未读消息数量
        [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationUpdateMessageNum object:nil userInfo:nil];
        [[TIMFriendshipManager sharedInstance] getSelfProfile:^(TIMUserProfile *profile) {
            UserInfoModel *user = userManager.curUserInfo;
            NSDictionary *profileDic = @{TIMProfileTypeKey_Nick:user.nick_name,
                                         TIMProfileTypeKey_FaceUrl:StrValid(user.avatar) ? user.avatar : @"",
                                         TIMProfileTypeKey_SelfSignature:user.profile.signature
            };
            [[TIMFriendshipManager sharedInstance] modifySelfProfile:profileDic succ:nil fail:nil];
        } fail:^(int code, NSString *msg) {

        }];
    } fail:^(int code, NSString * err) {
        [userManager logout:nil];
    }];
}

- (void)logoutSdk{
    [[TIMManager sharedInstance] logout:nil fail:nil];
}

- (void)applicationWillResignActive:(UIApplication *)application {
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self logoutSdk];
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    if ([url.host isEqualToString:@"safepay"]) {
        // 支付跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
            if ([resultDic[@"resultStatus"] intValue] == 9000) {
                [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationPaySuccess object:nil userInfo:nil];
            }else{
                [MBProgressHUD showTipMessageInWindow:resultDic[@"memo"]];
                [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationPayFail object:nil userInfo:nil];
            }
        }];
    }else{
        return [WXApi handleOpenURL:url delegate:self];
    }
    return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void(^)(NSArray<id<UIUserActivityRestoring>> * __nullable restorableObjects))restorationHandler {
    return [WXApi handleOpenUniversalLink:userActivity delegate:self];
}

- (void)onReq:(BaseReq *)req{
    
}

- (void)onResp:(BaseResp *)resp{
    if ([resp isKindOfClass:[SendAuthResp class]]) {
        SendAuthResp *saResp = (SendAuthResp *)resp;
        if (resp.errCode == 0) {
            [userManager login:kUserLoginTypeWeChat params:@{@"code":saResp.code,@"login_platform":@(1)} completion:^(BOOL success, NSString *des) {
                if (!success) {
                    [MBProgressHUD showTipMessageInWindow:@"授权失败"];
                }
            }];
        }else{
            [MBProgressHUD showTipMessageInWindow:@"授权失败"];
        }
    }else if ([resp isKindOfClass:[PayResp class]]){
        PayResp *response=(PayResp *)resp;
        switch(response.errCode){
            case WXSuccess:
                //服务器端查询支付通知或查询API返回的结果再提示成功
                [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationPaySuccess object:nil userInfo:nil];
                break;
            case WXErrCodeUserCancel:
                [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationPayFail object:nil userInfo:nil];
                break;
            default:{
                [MBProgressHUD showTipMessageInWindow:@"支付失败"];
                [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationPayFail object:nil userInfo:nil];
            }
                break;
        }
    }
    
}

#pragma mark TIMMessageListener
- (void)onRecvC2CCustomMessage:(NSString *)msgID sender:(V2TIMUserInfo *)info customData:(NSData *)data{
    [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationUpdateMessageNum object:nil userInfo:nil];
    NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    IMNotificationModel *notificationModel = [IMNotificationModel yy_modelWithDictionary:dataDict];
    if (notificationModel.action == IMNotificationActionStreamer){
        [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationBroadcastStreamer object:nil userInfo:@{@"data":notificationModel}];
    }else if (notificationModel.action == IMNotificationActionSystemMessage){
        //系统消息
    }else{
        
    }
}

- (void)onRecvGroupCustomMessage:(NSString *)msgID groupID:(NSString *)groupID sender:(V2TIMGroupMemberInfo *)info customData:(NSData *)data{
    NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    IMNotificationModel *notificationModel = [IMNotificationModel yy_modelWithDictionary:dataDict];
    notificationModel.roomid = groupID;
    if (notificationModel.action == IMNotificationActionRoomMessage || notificationModel.action == IMNotificationActionLiveFinished || notificationModel.action == IMNotificationActionExplainingGoods || notificationModel.action == IMNotificationActionRoomNotification) {
        //收到聊天室消息
        if ([info.userID isEqualToString:[NSString stringWithFormat:@"%lld",userManager.curUserInfo.userid]]) {
            //自己发送的
            return;
        }
        [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationInRoomIM object:nil userInfo:@{@"data":notificationModel}];
    }else if (notificationModel.action == IMNotificationActionGiftAnimation){
        //播放礼物动画
        if (notificationModel.gift.sender.userid == userManager.curUserInfo.userid) {
            //自己发送的
            return;
        }
        [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationGiftAnimation object:nil userInfo:@{@"data":notificationModel}];
    }else if (notificationModel.action == IMNotificationActionLiveGroupMemberJoinExit){
        [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationGroupEvent object:nil userInfo:@{@"data":notificationModel}];
    }
}

#pragma mark TIMUserStatusListener
- (void)onKickedOffline{
    [userManager logout:^(BOOL success, NSString *des) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showTipMessageInWindow:@"账号已在别的设备登录，您被强制下线"];
        });
    }];
}

#pragma mark QCloudCredentailFenceQueueDelegate
- (void)fenceQueue:(QCloudCredentailFenceQueue *)queue requestCreatorWithContinue:(QCloudCredentailFenceQueueContinue)continueBlock{
   QCloudCredential* credential = [QCloudCredential new];
   credential.secretID = configManager.txCosModel.tmpSecretId;
   credential.secretKey = configManager.txCosModel.tmpSecretKey;
   credential.startDate = [NSDate dateWithTimeIntervalSince1970:configManager.txCosModel.startTime];
   credential.token = configManager.txCosModel.sessionToken;
   QCloudAuthentationV5Creator* creator = [[QCloudAuthentationV5Creator alloc] initWithCredential:credential];
   continueBlock(creator, nil);
}

#pragma --mark QCloudSignatureProvider
- (void)signatureWithFields:(QCloudSignatureFields*)fileds
                     request:(QCloudBizHTTPRequest*)request
                  urlRequest:(NSMutableURLRequest*)urlRequst
                   compelete:(QCloudHTTPAuthentationContinueBlock)continueBlock{
    [self.credentialFenceQueue performAction:^(QCloudAuthentationCreator *creator, NSError *error) {
        if (error) {
            continueBlock(nil, error);
        } else {
            QCloudSignature* signature =  [creator signatureForData:urlRequst];
            continueBlock(signature, nil);
        }
    }];
}



@end
