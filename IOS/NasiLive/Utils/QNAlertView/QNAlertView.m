//
//  QNAlertView.m
//  RoadBird
//
//  Created by mryun11 on 2017/10/20.
//  Copyright © 2017年 tang. All rights reserved.
//

#import "QNAlertView.h"
#import "QNBlockButton.h"

#define mAlertWidth 265.0f
#define LightFontColor [UIColor colorWithHexString:@"5c5c5c"]
#define LineColor [UIColor colorWithHexString:@"ededed"]

@interface QNAlertView ()<UITextFieldDelegate ,UIGestureRecognizerDelegate>

@property(nonatomic,assign) QNAlertType type;
@property(nonatomic, strong) UIImageView *alertTitleIcon;
@property (nonatomic, strong) UILabel *alertTitleLabel;
@property (nonatomic, strong) UILabel *alertContentLabel;
@property (nonatomic, strong) QNBlockButton *leftBtn;
@property (nonatomic, strong) QNBlockButton *rightBtn;
@property(nonatomic, strong) QNBlockButton *singleBtn;
@property (nonatomic, strong) UIView *backImageView;
@property(nonatomic, strong) UITextField *textField;
@property(nonatomic, strong) QNTextField *QNTextField;

@end

@implementation QNAlertView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#define mTitleYOffset 20.0f
#define mTitleHeight 25.0f

#define mTitleIconWidth 50.f
#define mTitleIconHeight 50.f

#define mTextFieldHeight 40.f

#define mPaddingX 20.f
#define mSinglePaddingX 30.f
#define mBtnMarginX 10.f
#define mButtonHeight 40.0f
#define mButtonBottomOffset 30.0f

+ (instancetype)showAlertViewWithType:(QNAlertType)type title:(NSString *)title contentText:(NSString *)content singleButton:(NSString *)buttonText buttonClick:(void (^)())buttonClickBlock{
    QNAlertView *alert = [[self alloc]initWithType:type title:title contentText:content singleButton:buttonText buttonClick:buttonClickBlock];
    [alert show];
    return alert;
}

+ (instancetype)showAlertViewWithType:(QNAlertType)type title:(NSString *)title contentText:(NSString *)content leftButtonTitle:(NSString *)leftTitle leftClick:(void (^)())leftClickBlock rightButtonTitle:(NSString *)rigthTitle rightClick:(void (^)())rightClickBlock{
    QNAlertView *alert = [[self alloc]initWithType:type title:title contentText:content leftButtonTitle:leftTitle leftClick:leftClickBlock rightButtonTitle:rigthTitle rightClick:rightClickBlock];
    [alert show];
    return alert;
}

+ (instancetype)showTextFieldWithTitle:(NSString *)title placeHolder:(NSString *)placeHolder leftButtonTitle:(NSString *)leftTitle leftClick:(void (^)(NSString *text))leftClickBlock rightButtonTitle:(NSString *)rigthTitle rightClick:(void (^)(NSString *text))rightClickBlock{
    QNAlertView *alert = [[self alloc]initTextFieldWithTitle:title placeHolder:placeHolder leftButtonTitle:leftTitle leftClick:leftClickBlock rightButtonTitle:rigthTitle rightClick:rightClickBlock];
    [alert show];
    return alert;
}

+ (instancetype)showQNTextFieldWithTitle:(NSString *)title placeHolder:(NSString *)placeHolder textFieldType:(QNTextFieldType)type leftButtonTitle:(NSString *)leftTitle leftClick:(void (^)(NSString *))leftClickBlock rightButtonTitle:(NSString *)rigthTitle rightClick:(void (^)(NSString *))rightClickBlock{
    QNAlertView *alert = [[self alloc]initQNTextFieldWithTitle:title placeHolder:placeHolder textFieldType:type leftButtonTitle:leftTitle leftClick:leftClickBlock rightButtonTitle:rigthTitle rightClick:rightClickBlock];
    [alert show];
    return alert;
}

+ (instancetype)showAlertViewWithImage:(UIImage *)image title:(NSString *)title contentText:(NSString *)content singleButton:(NSString *)buttonText buttonClick:(void (^)())buttonClickBlock{
    QNAlertView *alert = [[self alloc]initWithImage:image title:title contentText:content singleButton:buttonText buttonClick:buttonClickBlock];
    [alert show];
    return alert;
}

+ (instancetype)showAlertViewWithImageUrl:(NSString *)imageUrl title:(NSString *)title contentText:(NSString *)content leftButtonTitle:(NSString *)leftTitle leftClick:(void (^)())leftClickBlock rightButtonTitle:(NSString *)rigthTitle rightClick:(void (^)())rightClickBlock{
    QNAlertView *alert = [[self alloc]initWithImageUrl:imageUrl title:title contentText:content leftButtonTitle:leftTitle leftClick:leftClickBlock rightButtonTitle:rigthTitle rightClick:rightClickBlock];
    [alert show];
    return alert;
}

- (instancetype)initWithImage:(UIImage *)image title:(NSString *)title contentText:(NSString *)content singleButton:(NSString *)buttonText buttonClick:(void (^)(void))buttonClickBlock{
    if (self = [super init]) {
        __weak typeof(self) weakSelf = self;
        
//        self.type = type;
        self.layer.cornerRadius = 10.0;
        self.backgroundColor = [UIColor whiteColor];
        
        self.alertTitleIcon = [[UIImageView alloc]initWithFrame:CGRectMake(0, mTitleYOffset, mTitleIconWidth, mTitleIconHeight)];
        self.alertTitleIcon.image = image;
        self.alertTitleIcon.centerX = mAlertWidth/2;
        [self addSubview:self.alertTitleIcon];
        
        self.alertTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.alertTitleIcon.frame) + 10, mAlertWidth, mTitleHeight)];
        self.alertTitleLabel.textAlignment = NSTextAlignmentCenter;
        self.alertTitleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
        self.alertTitleLabel.textColor = [UIColor colorWithRed:56.0/255.0 green:64.0/255.0 blue:71.0/255.0 alpha:1];
        [self addSubview:self.alertTitleLabel];
        self.alertTitleLabel.text = title;
        
        CGFloat contentLabelWidth = mAlertWidth - mPaddingX*2;
        
        self.alertContentLabel = [[UILabel alloc] init];
        self.alertContentLabel.text = content;
        self.alertContentLabel.numberOfLines = 0;
        self.alertContentLabel.textAlignment = self.alertTitleLabel.textAlignment = NSTextAlignmentCenter;
        self.alertContentLabel.textColor = LightFontColor;
        self.alertContentLabel.font = [UIFont systemFontOfSize:15.0f];
        
        CGFloat contentHeight = [self.alertContentLabel.text boundingRectWithSize:CGSizeMake(contentLabelWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.alertContentLabel.font} context:nil].size.height + mButtonBottomOffset*2;
        if (contentHeight < 60) {
            contentHeight = 60;
        }
        self.alertContentLabel.frame = CGRectMake((mAlertWidth - contentLabelWidth) * 0.5, CGRectGetMaxY(self.alertTitleLabel.frame) + 10, contentLabelWidth, contentHeight);
        [self addSubview:self.alertContentLabel];
        
        
        if (buttonText.length > 0) {
            self.singleBtn = [QNBlockButton buttonWithType:UIButtonTypeCustom];
            self.singleBtn.frame = CGRectMake(mPaddingX, CGRectGetMaxY(self.alertContentLabel.frame), mAlertWidth - mPaddingX*2, mButtonHeight);
            
            
            [self.singleBtn setBackgroundImage:[UIImage imageWithColor:MAIN_COLOR] forState:UIControlStateNormal];
            [self.singleBtn setTitle:buttonText forState:UIControlStateNormal];
            [self.singleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            self.singleBtn.block = ^(UIButton *button) {
                [weakSelf dismissAlert];
                if (buttonClickBlock) {
                    buttonClickBlock();
                }
            };
            [self addSubview:self.singleBtn];
        }
        
        
        self.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        
        CGRect afterFrame;
        if (self.singleBtn) {
            afterFrame = CGRectMake(([UIScreen mainScreen].bounds.size.width - mAlertWidth) * 0.5, ([UIScreen mainScreen].bounds.size.height - CGRectGetMaxY(self.singleBtn.frame) - mButtonBottomOffset) * 0.5, mAlertWidth, CGRectGetMaxY(self.singleBtn.frame) + mButtonBottomOffset);
        }else{
            afterFrame = CGRectMake(([UIScreen mainScreen].bounds.size.width - mAlertWidth) * 0.5, ([UIScreen mainScreen].bounds.size.height - CGRectGetMaxY(self.alertContentLabel.frame) - mButtonBottomOffset) * 0.5, mAlertWidth, CGRectGetMaxY(self.alertContentLabel.frame) + mButtonBottomOffset);
        }
        
        self.frame = afterFrame;
    }
    return self;
}

- (instancetype)initWithImageUrl:(NSString *)imageUrl title:(NSString *)title contentText:(NSString *)content leftButtonTitle:(NSString *)leftTitle leftClick:(void (^)())leftClickBlock rightButtonTitle:(NSString *)rigthTitle rightClick:(void (^)())rightClickBlock{
    if (self = [super init]) {
        __weak typeof(self) weakSelf = self;
        
        //        self.type = type;
        self.layer.cornerRadius = 10.0;
        self.backgroundColor = [UIColor whiteColor];
        
        self.alertTitleIcon = [[UIImageView alloc]initWithFrame:CGRectMake(0, mTitleYOffset, mTitleIconWidth, mTitleIconHeight)];
        self.alertTitleIcon.layer.cornerRadius = mTitleIconWidth*0.5;
        self.alertTitleIcon.layer.masksToBounds = YES;
        [self.alertTitleIcon sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"ic_avatar"]];
        self.alertTitleIcon.centerX = mAlertWidth/2;
        [self addSubview:self.alertTitleIcon];
        
        self.alertTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.alertTitleIcon.frame) + 10, mAlertWidth, mTitleHeight)];
        self.alertTitleLabel.textAlignment = NSTextAlignmentCenter;
        self.alertTitleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
        self.alertTitleLabel.textColor = [UIColor colorWithRed:56.0/255.0 green:64.0/255.0 blue:71.0/255.0 alpha:1];
        [self addSubview:self.alertTitleLabel];
        self.alertTitleLabel.text = title;
        
        CGFloat contentLabelWidth = mAlertWidth - mPaddingX*2;
        
        self.alertContentLabel = [[UILabel alloc] init];
        self.alertContentLabel.text = content;
        self.alertContentLabel.numberOfLines = 0;
        self.alertContentLabel.textAlignment = self.alertTitleLabel.textAlignment = NSTextAlignmentCenter;
        self.alertContentLabel.textColor = LightFontColor;
        self.alertContentLabel.font = [UIFont systemFontOfSize:15.0f];
        
        CGFloat contentHeight = [self.alertContentLabel.text boundingRectWithSize:CGSizeMake(contentLabelWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.alertContentLabel.font} context:nil].size.height + mButtonBottomOffset*2;
        if (contentHeight < 60) {
            contentHeight = 60;
        }
        self.alertContentLabel.frame = CGRectMake((mAlertWidth - contentLabelWidth) * 0.5, CGRectGetMaxY(self.alertTitleLabel.frame) + 10, contentLabelWidth, contentHeight);
        [self addSubview:self.alertContentLabel];
        
        self.leftBtn = [QNBlockButton buttonWithType:UIButtonTypeCustom];
        self.rightBtn = [QNBlockButton buttonWithType:UIButtonTypeCustom];
        
        CGFloat btnW = (mAlertWidth - mPaddingX*2 - mBtnMarginX)/2;
        self.leftBtn.frame = CGRectMake(mPaddingX, CGRectGetMaxY(self.alertContentLabel.frame), btnW, mButtonHeight);
        self.rightBtn.frame = CGRectMake(CGRectGetMaxX(self.leftBtn.frame) + mBtnMarginX, CGRectGetMaxY(self.alertContentLabel.frame), btnW, mButtonHeight);
        
        [self.rightBtn setBackgroundImage:[UIImage imageWithColor:MAIN_COLOR] forState:UIControlStateNormal];
        [self.rightBtn setTitle:rigthTitle forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.rightBtn.block = ^(UIButton *button) {
            [weakSelf dismissAlert];
            if (rightClickBlock) {
                rightClickBlock();
            }
        };
        [self addSubview:self.rightBtn];
        
        [self.leftBtn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [self.leftBtn setTitle:leftTitle forState:UIControlStateNormal];
        self.leftBtn.titleLabel.font = self.rightBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [self.leftBtn setTitleColor:LightFontColor forState:UIControlStateNormal];
        self.leftBtn.block = ^(UIButton *button) {
            [weakSelf dismissAlert];
            if (leftClickBlock) {
                leftClickBlock();
            }
        };
        self.leftBtn.layer.masksToBounds = self.rightBtn.layer.masksToBounds = YES;
        self.leftBtn.layer.cornerRadius = self.rightBtn.layer.cornerRadius = mButtonHeight/2;
        self.leftBtn.layer.borderWidth = .7f;
        self.leftBtn.layer.borderColor = LineColor.CGColor;
        [self addSubview:self.leftBtn];
        
        self.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        
        CGRect afterFrame = CGRectMake(([UIScreen mainScreen].bounds.size.width - mAlertWidth) * 0.5, ([UIScreen mainScreen].bounds.size.height - CGRectGetMaxY(self.leftBtn.frame) - mButtonBottomOffset) * 0.5, mAlertWidth, CGRectGetMaxY(self.leftBtn.frame) + mButtonBottomOffset);
        self.frame = afterFrame;
    }
    return self;
}


- (instancetype)initWithType:(QNAlertType)type title:(NSString *)title contentText:(NSString *)content singleButton:(NSString *)buttonText buttonClick:(void (^)())buttonClickBlock{
    if (self = [super init]) {
        __weak typeof(self) weakSelf = self;
        
        self.type = type;
        self.layer.cornerRadius = 10.0;
        self.backgroundColor = [UIColor whiteColor];
        
        if (type == QNAlertTypeTitle) {
            self.alertTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, mTitleYOffset, mAlertWidth, mTitleHeight)];
            self.alertTitleLabel.textAlignment = NSTextAlignmentCenter;
            self.alertTitleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
            self.alertTitleLabel.textColor = [UIColor colorWithRed:56.0/255.0 green:64.0/255.0 blue:71.0/255.0 alpha:1];
            [self addSubview:self.alertTitleLabel];
            self.alertTitleLabel.text = title;
        }else{
            self.alertTitleIcon = [[UIImageView alloc]initWithFrame:CGRectMake(0, mTitleYOffset, mTitleIconWidth, mTitleIconHeight)];
            switch (type) {
                case QNAlertTypeWarning:
                    self.alertTitleIcon.image = [UIImage imageNamed:@"icon_warning.png"];
                    break;
                case QNAlertTypeWating:
                    self.alertTitleIcon.image = [UIImage imageNamed:@"icon_wating.png"];
                    break;
                case QNAlertTypePrompt:
                    self.alertTitleIcon.image = [UIImage imageNamed:@"icon_prompt.png"];
                    break;
                case QNAlertTypeComplate:
                    self.alertTitleIcon.image = [UIImage imageNamed:@"icon_complate.png"];
                    break;
                case QNAlertTypeLighting:
                    self.alertTitleIcon.image = [UIImage imageNamed:@"icon_light.png"];
                    break;
                default:
                    self.alertTitleIcon.image = [UIImage imageNamed:@"icon_warning.png"];
                    break;
            }
            self.alertTitleIcon.centerX = mAlertWidth/2;
            [self addSubview:self.alertTitleIcon];
            
        }
        
        CGFloat contentLabelWidth = mAlertWidth - mPaddingX*2;
        
        self.alertContentLabel = [[UILabel alloc] init];
        self.alertContentLabel.text = content;
        self.alertContentLabel.numberOfLines = 0;
        self.alertContentLabel.textAlignment = self.alertTitleLabel.textAlignment = NSTextAlignmentCenter;
        self.alertContentLabel.textColor = LightFontColor;
        self.alertContentLabel.font = [UIFont systemFontOfSize:15.0f];
        
        CGFloat contentHeight = [self.alertContentLabel.text boundingRectWithSize:CGSizeMake(contentLabelWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.alertContentLabel.font} context:nil].size.height + mButtonBottomOffset*2;
        if (contentHeight < 60) {
            contentHeight = 60;
        }
        self.alertContentLabel.frame = CGRectMake((mAlertWidth - contentLabelWidth) * 0.5, type == QNAlertTypeTitle ? CGRectGetMaxY(self.alertTitleLabel.frame):CGRectGetMaxY(self.alertTitleIcon.frame), contentLabelWidth, contentHeight);
        [self addSubview:self.alertContentLabel];
        
        
        
        self.singleBtn = [QNBlockButton buttonWithType:UIButtonTypeCustom];
        self.singleBtn.frame = CGRectMake(mSinglePaddingX, CGRectGetMaxY(self.alertContentLabel.frame), mAlertWidth - mSinglePaddingX*2, mButtonHeight);
        self.singleBtn.layer.cornerRadius = mButtonHeight/2;
        self.singleBtn.layer.masksToBounds = YES;
        
        [self.singleBtn setBackgroundImage:[UIImage imageWithColor:MAIN_COLOR] forState:UIControlStateNormal];
        [self.singleBtn setTitle:buttonText forState:UIControlStateNormal];
        [self.singleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        self.singleBtn.block = ^(UIButton *button) {
            [weakSelf dismissAlert];
            if (buttonClickBlock) {
                buttonClickBlock();
            }
        };
        [self addSubview:self.singleBtn];
        
        self.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        
        CGRect afterFrame = CGRectMake(([UIScreen mainScreen].bounds.size.width - mAlertWidth) * 0.5, ([UIScreen mainScreen].bounds.size.height - CGRectGetMaxY(self.singleBtn.frame) - mButtonBottomOffset) * 0.5, mAlertWidth, CGRectGetMaxY(self.singleBtn.frame) + mButtonBottomOffset);
        self.frame = afterFrame;
    }
    return self;
}

- (instancetype)initWithType:(QNAlertType)type title:(NSString *)title contentText:(NSString *)content leftButtonTitle:(NSString *)leftTitle leftClick:(void (^)())leftClickBlock rightButtonTitle:(NSString *)rigthTitle rightClick:(void (^)())rightClickBlock{
    if (self = [super init]) {
        __weak typeof(self) weakSelf = self;
        
        self.type = type;
        self.layer.cornerRadius = 10.0;
        self.backgroundColor = [UIColor whiteColor];
        
        if (type == QNAlertTypeTitle) {
            self.alertTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, mTitleYOffset, mAlertWidth, mTitleHeight)];
            self.alertTitleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
            self.alertTitleLabel.textColor = [UIColor colorWithRed:56.0/255.0 green:64.0/255.0 blue:71.0/255.0 alpha:1];
            self.alertTitleLabel.textAlignment = NSTextAlignmentCenter;
            [self addSubview:self.alertTitleLabel];
            self.alertTitleLabel.text = title;
        }else{
            self.alertTitleIcon = [[UIImageView alloc]initWithFrame:CGRectMake(0, mTitleYOffset, mTitleIconWidth, mTitleIconHeight)];
            switch (type) {
                case QNAlertTypeWarning:
                    self.alertTitleIcon.image = [UIImage imageNamed:@"icon_warning.png"];
                    break;
                case QNAlertTypeWating:
                    self.alertTitleIcon.image = [UIImage imageNamed:@"icon_wating.png"];
                    break;
                case QNAlertTypePrompt:
                    self.alertTitleIcon.image = [UIImage imageNamed:@"icon_prompt.png"];
                    break;
                case QNAlertTypeComplate:
                    self.alertTitleIcon.image = [UIImage imageNamed:@"icon_complate.png"];
                    break;
                case QNAlertTypeLighting:
                    self.alertTitleIcon.image = [UIImage imageNamed:@"icon_light.png"];
                    break;
                default:
                    self.alertTitleIcon.image = [UIImage imageNamed:@"icon_warning.png"];
                    break;
            }
            self.alertTitleIcon.centerX = mAlertWidth/2;
            [self addSubview:self.alertTitleIcon];
            
        }
        
        CGFloat contentLabelWidth = mAlertWidth - mPaddingX*2;
        
        self.alertContentLabel = [[UILabel alloc] init];
        self.alertContentLabel.text = content;
        self.alertContentLabel.numberOfLines = 0;
        self.alertContentLabel.textAlignment = self.alertTitleLabel.textAlignment = NSTextAlignmentCenter;
        self.alertContentLabel.textColor = LightFontColor;
        self.alertContentLabel.font = [UIFont systemFontOfSize:15.0f];
        
        CGFloat contentHeight = [self.alertContentLabel.text boundingRectWithSize:CGSizeMake(contentLabelWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.alertContentLabel.font} context:nil].size.height + mButtonBottomOffset*2;
        if (contentHeight < 60) {
            contentHeight = 60;
        }
        self.alertContentLabel.frame = CGRectMake((mAlertWidth - contentLabelWidth) * 0.5, type == QNAlertTypeTitle ? CGRectGetMaxY(self.alertTitleLabel.frame):CGRectGetMaxY(self.alertTitleIcon.frame), contentLabelWidth, contentHeight);
        [self addSubview:self.alertContentLabel];
        
        
        self.leftBtn = [QNBlockButton buttonWithType:UIButtonTypeCustom];
        self.rightBtn = [QNBlockButton buttonWithType:UIButtonTypeCustom];
        
        CGFloat btnW = (mAlertWidth - mPaddingX*2 - mBtnMarginX)/2;
        self.leftBtn.frame = CGRectMake(mPaddingX, CGRectGetMaxY(self.alertContentLabel.frame), btnW, mButtonHeight);
        self.rightBtn.frame = CGRectMake(CGRectGetMaxX(self.leftBtn.frame) + mBtnMarginX, CGRectGetMaxY(self.alertContentLabel.frame), btnW, mButtonHeight);
        
        [self.rightBtn setBackgroundImage:[UIImage imageWithColor:MAIN_COLOR] forState:UIControlStateNormal];
        [self.rightBtn setTitle:rigthTitle forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.rightBtn.block = ^(UIButton *button) {
            [weakSelf dismissAlert];
            if (rightClickBlock) {
                rightClickBlock();
            }
        };
        [self addSubview:self.rightBtn];
        
        [self.leftBtn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [self.leftBtn setTitle:leftTitle forState:UIControlStateNormal];
        self.leftBtn.titleLabel.font = self.rightBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [self.leftBtn setTitleColor:LightFontColor forState:UIControlStateNormal];
        self.leftBtn.block = ^(UIButton *button) {
            [weakSelf dismissAlert];
            if (leftClickBlock) {
                leftClickBlock();
            }
        };
        self.leftBtn.layer.masksToBounds = self.rightBtn.layer.masksToBounds = YES;
        self.leftBtn.layer.cornerRadius = self.rightBtn.layer.cornerRadius = mButtonHeight/2;
        self.leftBtn.layer.borderWidth = .7f;
        self.leftBtn.layer.borderColor = LineColor.CGColor;
        [self addSubview:self.leftBtn];
        
        self.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        
        CGRect afterFrame = CGRectMake(([UIScreen mainScreen].bounds.size.width - mAlertWidth) * 0.5, ([UIScreen mainScreen].bounds.size.height - CGRectGetMaxY(self.leftBtn.frame) - mButtonBottomOffset) * 0.5, mAlertWidth, CGRectGetMaxY(self.leftBtn.frame) + mButtonBottomOffset);
        self.frame = afterFrame;
    }
    return self;
}

- (instancetype)initTextFieldWithTitle:(NSString *)title placeHolder:(NSString *)placeHolder leftButtonTitle:(NSString *)leftTitle leftClick:(void (^)(NSString *))leftClickBlock rightButtonTitle:(NSString *)rigthTitle rightClick:(void (^)(NSString *))rightClickBlock{
    if (self = [super init]) {
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillDisappear:) name:UIKeyboardWillHideNotification object:nil];
        
        __weak typeof(self) weakSelf = self;
        
        self.layer.cornerRadius = 10.0;
        self.backgroundColor = [UIColor whiteColor];
        
        self.alertTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, mTitleYOffset, mAlertWidth, mTitleHeight)];
        self.alertTitleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
        self.alertTitleLabel.textColor = [UIColor colorWithRed:56.0/255.0 green:64.0/255.0 blue:71.0/255.0 alpha:1];
        self.alertTitleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.alertTitleLabel];
        self.alertTitleLabel.text = title;
        
        CGFloat textFieldWidth = mAlertWidth - mPaddingX*2;
        self.textField = [[UITextField alloc]initWithFrame:CGRectMake(mPaddingX, CGRectGetMaxY(self.alertTitleLabel.frame) + mTitleYOffset, textFieldWidth, mTextFieldHeight)];
        self.textField.delegate = self;
        self.textField.textColor = [UIColor colorWithRed:99/255.0 green:99/255.0 blue:99/255.0 alpha:1];
        self.textField.placeholder = placeHolder;
        self.textField.font = [UIFont systemFontOfSize:15];
        self.textField.layer.borderColor = [UIColor colorWithRed:200/255.0 green:200/255.0 blue:200/255.0 alpha:1].CGColor;
        self.textField.layer.borderWidth = .6f;
        self.textField.layer.cornerRadius = 3.f;
        self.textField.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, self.textField.height)];
        self.textField.leftView.backgroundColor = [UIColor clearColor];
        self.textField.leftViewMode = UITextFieldViewModeAlways;
        [self addSubview:self.textField];
        
        self.leftBtn = [QNBlockButton buttonWithType:UIButtonTypeCustom];
        self.rightBtn = [QNBlockButton buttonWithType:UIButtonTypeCustom];
        
        CGFloat btnW = (mAlertWidth - mPaddingX*2 - mBtnMarginX)/2;
        self.leftBtn.frame = CGRectMake(mPaddingX, CGRectGetMaxY(self.textField.frame) + mButtonBottomOffset, btnW, mButtonHeight);
        self.rightBtn.frame = CGRectMake(CGRectGetMaxX(self.leftBtn.frame) + mBtnMarginX, CGRectGetMaxY(self.textField.frame) + mButtonBottomOffset, btnW, mButtonHeight);
        
        [self.rightBtn setBackgroundImage:[UIImage imageWithColor:MAIN_COLOR] forState:UIControlStateNormal];
        [self.rightBtn setTitle:rigthTitle forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.rightBtn.block = ^(UIButton *button) {
            if (weakSelf.textField.text.length > 0){
                [weakSelf dismissAlert];
                if (rightClickBlock) {
                    rightClickBlock(weakSelf.textField.text);
                }
            }
        };
        [self addSubview:self.rightBtn];
        
        [self.leftBtn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
        [self.leftBtn setTitle:leftTitle forState:UIControlStateNormal];
        self.leftBtn.titleLabel.font = self.rightBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [self.leftBtn setTitleColor:LightFontColor forState:UIControlStateNormal];
        self.leftBtn.block = ^(UIButton *button) {
            [weakSelf dismissAlert];
            if (leftClickBlock) {
                leftClickBlock(weakSelf.textField.text);
            }
        };
        self.leftBtn.layer.masksToBounds = self.rightBtn.layer.masksToBounds = YES;
        self.leftBtn.layer.cornerRadius = self.rightBtn.layer.cornerRadius = mButtonHeight/2;
        self.leftBtn.layer.borderWidth = .7f;
        self.leftBtn.layer.borderColor = LineColor.CGColor;
        [self addSubview:self.leftBtn];
        
        self.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        
        CGRect afterFrame = CGRectMake(([UIScreen mainScreen].bounds.size.width - mAlertWidth) * 0.5, ([UIScreen mainScreen].bounds.size.height - CGRectGetMaxY(self.leftBtn.frame) - mButtonBottomOffset) * 0.5, mAlertWidth, CGRectGetMaxY(self.leftBtn.frame) + mButtonBottomOffset);
        self.frame = afterFrame;
    }
    return self;
}

- (instancetype)initQNTextFieldWithTitle:(NSString *)title
                              placeHolder:(NSString *)placeHolder
                            textFieldType:(QNTextFieldType)type
                          leftButtonTitle:(NSString *)leftTitle
                                leftClick: (void (^)(NSString *))leftClickBlock
                         rightButtonTitle:(NSString *)rigthTitle
                               rightClick: (void (^)(NSString *))rightClickBlock{
    if (self = [super init]) {
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillDisappear:) name:UIKeyboardWillHideNotification object:nil];
        
        __weak typeof(self) weakSelf = self;
        
        self.layer.cornerRadius = 10.0;
        self.backgroundColor = [UIColor whiteColor];
        
        self.alertTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, mTitleYOffset, mAlertWidth, mTitleHeight)];
        self.alertTitleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
        self.alertTitleLabel.textColor = [UIColor colorWithRed:56.0/255.0 green:64.0/255.0 blue:71.0/255.0 alpha:1];
        self.alertTitleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.alertTitleLabel];
        self.alertTitleLabel.text = title;
        
        CGFloat textFieldWidth = mAlertWidth - mPaddingX*2;
        self.QNTextField = [[QNTextField alloc]initWithType:type frame:CGRectMake(mPaddingX, CGRectGetMaxY(self.alertTitleLabel.frame) + mTitleYOffset, textFieldWidth, mTextFieldHeight)];
        self.QNTextField.textColor = [UIColor colorWithRed:99/255.0 green:99/255.0 blue:99/255.0 alpha:1];
        self.QNTextField.placeholder = placeHolder;
        self.QNTextField.font = [UIFont systemFontOfSize:15];
        self.QNTextField.layer.borderColor = [UIColor colorWithRed:200/255.0 green:200/255.0 blue:200/255.0 alpha:1].CGColor;
        self.QNTextField.layer.borderWidth = .6f;
        self.QNTextField.layer.cornerRadius = 3.f;
        self.QNTextField.leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, self.QNTextField.height)];
        self.QNTextField.leftView.backgroundColor = [UIColor clearColor];
        self.QNTextField.leftViewMode = UITextFieldViewModeAlways;
        [self addSubview:self.QNTextField];
        
        if (!leftTitle) {
            self.rightBtn = [QNBlockButton buttonWithType:UIButtonTypeCustom];
            self.rightBtn.frame = CGRectMake(mSinglePaddingX, CGRectGetMaxY(self.QNTextField.frame) + mButtonBottomOffset, mAlertWidth - mSinglePaddingX*2, mButtonHeight);
            self.rightBtn.layer.cornerRadius = mButtonHeight/2;
            self.rightBtn.layer.masksToBounds = YES;
            
            [self.rightBtn setBackgroundImage:[UIImage imageWithColor:MAIN_COLOR] forState:UIControlStateNormal];
            [self.rightBtn setTitle:rigthTitle forState:UIControlStateNormal];
            [self.rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            self.rightBtn.block = ^(UIButton *button) {
                if (self.requireInput && weakSelf.QNTextField.text.length == 0) {
                    return;
                }
                [weakSelf dismissAlert];
                if (rightClickBlock) {
                    rightClickBlock(weakSelf.QNTextField.text);
                }
            };
            [self addSubview:self.rightBtn];
        }else{
            self.leftBtn = [QNBlockButton buttonWithType:UIButtonTypeCustom];
            self.rightBtn = [QNBlockButton buttonWithType:UIButtonTypeCustom];
            
            CGFloat btnW = (mAlertWidth - mPaddingX*2 - mBtnMarginX)/2;
            self.leftBtn.frame = CGRectMake(mPaddingX, CGRectGetMaxY(self.QNTextField.frame) + mButtonBottomOffset, btnW, mButtonHeight);
            self.rightBtn.frame = CGRectMake(CGRectGetMaxX(self.leftBtn.frame) + mBtnMarginX, CGRectGetMaxY(self.QNTextField.frame) + mButtonBottomOffset, btnW, mButtonHeight);
            
            [self.rightBtn setBackgroundImage:[UIImage imageWithColor:MAIN_COLOR] forState:UIControlStateNormal];
            [self.rightBtn setTitle:rigthTitle forState:UIControlStateNormal];
            [self.rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.rightBtn.block = ^(UIButton *button) {
                if (self.requireInput && weakSelf.QNTextField.text.length == 0) {
                    return;
                }
                [weakSelf dismissAlert];
                if (rightClickBlock) {
                    rightClickBlock(weakSelf.QNTextField.text);
                }
            };
            [self addSubview:self.rightBtn];
            
            [self.leftBtn setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
            [self.leftBtn setTitle:leftTitle forState:UIControlStateNormal];
            self.leftBtn.titleLabel.font = self.rightBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
            [self.leftBtn setTitleColor:LightFontColor forState:UIControlStateNormal];
            self.leftBtn.block = ^(UIButton *button) {
                [weakSelf dismissAlert];
                if (leftClickBlock) {
                    leftClickBlock(weakSelf.QNTextField.text);
                }
            };
            self.leftBtn.layer.masksToBounds = self.rightBtn.layer.masksToBounds = YES;
            self.leftBtn.layer.cornerRadius = self.rightBtn.layer.cornerRadius = mButtonHeight/2;
            self.leftBtn.layer.borderWidth = .7f;
            self.leftBtn.layer.borderColor = LineColor.CGColor;
            [self addSubview:self.leftBtn];
        }
        
        self.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        
        CGRect afterFrame = CGRectMake(([UIScreen mainScreen].bounds.size.width - mAlertWidth) * 0.5, ([UIScreen mainScreen].bounds.size.height - CGRectGetMaxY(self.rightBtn.frame) - mButtonBottomOffset) * 0.5, mAlertWidth, CGRectGetMaxY(self.rightBtn.frame) + mButtonBottomOffset);
        self.frame = afterFrame;
    }
    return self;
}

- (void)show{
    UIViewController *topVC = [self appRootViewController];
    [topVC.view addSubview:self];
}

- (void)dismissAlert{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    [self.backImageView removeFromSuperview];
    self.backImageView = nil;
    [self removeFromSuperview];
    if (self.dismissBlock) {
        self.dismissBlock();
    }
}

- (void)willMoveToSuperview:(UIView *)newSuperview{
    if (newSuperview == nil) {
        return;
    }
    UIViewController *topVC = [self appRootViewController];
    
    if (!self.backImageView) {
        self.backImageView = [[UIView alloc] initWithFrame:topVC.view.bounds];
        self.backImageView.backgroundColor = [UIColor blackColor];
        self.backImageView.alpha = 0.6f;
        self.backImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backViewTap)];
        tap.delegate = self;
        [self.backImageView addGestureRecognizer:tap];
    }
    [topVC.view addSubview:self.backImageView];
    
    self.transform = CGAffineTransformMakeScale(0.4, 0.4);
    [UIView animateWithDuration:0.4f delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:20 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        self.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
    }];
    [super willMoveToSuperview:newSuperview];
}

- (void)backViewTap{
    if (!self.cancelBackTap) {
        [self dismissAlert];
    }
}

- (void)setTextFieldText:(NSString *)textFieldText{
    _textFieldText = textFieldText;
    self.textField.text = textFieldText;
}

- (UIViewController *)appRootViewController{
    UIViewController *appRootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *topVC = appRootVC;
    while (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    return topVC;
}

- (void)keyboardWillAppear:(NSNotification *)notification{
    
    NSDictionary *info = [notification userInfo];
    
    //取出动画时长
    CGFloat animationDuration = [[info valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //取出键盘位置大小信息
    CGRect keyboardBounds = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    //rect转换
//    CGRect keyboardRect = [self convertRect:keyboardBounds toView:nil];
    
    //记录Y轴变化
    CGFloat keyboardHeight = keyboardBounds.size.height;
    
    //上移动画options
    UIViewAnimationOptions options = (UIViewAnimationOptions)[[info valueForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue] << 16;
    
    [UIView animateWithDuration:animationDuration delay:0 options:options animations:^{
        self.transform = CGAffineTransformMakeTranslation(0, -keyboardHeight/2);
    } completion:nil];
}


- (void)keyboardWillDisappear:(NSNotification *)notification{
    NSDictionary *info = [notification userInfo];
    
    //取出动画时长
    CGFloat animationDuration = [[info valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //下移动画options
    UIViewAnimationOptions options = (UIViewAnimationOptions)[[info valueForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue] << 16;
    
    //回复动画
    [UIView animateWithDuration:animationDuration delay:0 options:options animations:^{
        self.transform = CGAffineTransformIdentity;
    } completion:nil];
    
}

@end

@implementation UIImage (QNColorful)

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end

