//
//  QNAlertView.h
//  RoadBird
//
//  Created by mryun11 on 2017/10/20.
//  Copyright © 2017年 tang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QNTextField.h"

typedef enum{
    QNAlertTypeTitle,
    QNAlertTypeWarning,
    QNAlertTypeWating,
    QNAlertTypePrompt,
    QNAlertTypeComplate,
    QNAlertTypeLighting
} QNAlertType;

@interface QNAlertView : UIView

+ (instancetype)showAlertViewWithType:(QNAlertType)type
                                title:(NSString *)title
                          contentText:(NSString *)content
                         singleButton:(NSString *)buttonText
                          buttonClick:(void (^)())buttonClickBlock;

+ (instancetype)showAlertViewWithType:(QNAlertType)type
                                title:(NSString *)title
                          contentText:(NSString *)content
                      leftButtonTitle:(NSString *)leftTitle
                            leftClick: (void (^)())leftClickBlock
                     rightButtonTitle:(NSString *)rigthTitle
                           rightClick: (void (^)())rightClickBlock;

+ (instancetype)showTextFieldWithTitle:(NSString *)title
                           placeHolder:(NSString *)placeHolder
                       leftButtonTitle:(NSString *)leftTitle
                             leftClick: (void (^)(NSString *text))leftClickBlock
                      rightButtonTitle:(NSString *)rigthTitle
                            rightClick: (void (^)(NSString *text))rightClickBlock;

+ (instancetype)showQNTextFieldWithTitle:(NSString *)title
                           placeHolder:(NSString *)placeHolder
                         textFieldType:(QNTextFieldType)type
                       leftButtonTitle:(NSString *)leftTitle
                             leftClick: (void (^)(NSString *))leftClickBlock
                      rightButtonTitle:(NSString *)rigthTitle
                            rightClick: (void (^)(NSString *))rightClickBlock;

+ (instancetype)showAlertViewWithImage:(UIImage *)image
                                 title:(NSString *)title
                           contentText:(NSString *)content
                          singleButton:(NSString *)buttonText
                           buttonClick:(void (^)())buttonClickBlock;

+ (instancetype)showAlertViewWithImageUrl:(NSString *)imageUrl
                                    title:(NSString *)title
                              contentText:(NSString *)content
                          leftButtonTitle:(NSString *)leftTitle
                                leftClick: (void (^)())leftClickBlock
                         rightButtonTitle:(NSString *)rigthTitle
                               rightClick: (void (^)())rightClickBlock;

- (instancetype)initWithType:(QNAlertType)type
                       title:(NSString *)title
                 contentText:(NSString *)content
                singleButton:(NSString *)buttonText
                 buttonClick:(void (^)())buttonClickBlock;


- (instancetype)initWithType:(QNAlertType)type
                       title:(NSString *)title
                 contentText:(NSString *)content
             leftButtonTitle:(NSString *)leftTitle
                   leftClick: (void (^)())leftClickBlock
            rightButtonTitle:(NSString *)rigthTitle
                  rightClick: (void (^)())rightClickBlock;

- (instancetype)initTextFieldWithTitle:(NSString *)title
                           placeHolder:(NSString *)placeHolder
                       leftButtonTitle:(NSString *)leftTitle
                             leftClick: (void (^)(NSString *))leftClickBlock
                      rightButtonTitle:(NSString *)rigthTitle
                            rightClick: (void (^)(NSString *))rightClickBlock;

- (instancetype)initQNTextFieldWithTitle:(NSString *)title
                              placeHolder:(NSString *)placeHolder
                            textFieldType:(QNTextFieldType)type
                          leftButtonTitle:(NSString *)leftTitle
                                leftClick: (void (^)(NSString *))leftClickBlock
                         rightButtonTitle:(NSString *)rigthTitle
                               rightClick: (void (^)(NSString *))rightClickBlock;


- (void)show;

@property (nonatomic, copy) dispatch_block_t dismissBlock;
@property (copy, nonatomic) NSString *textFieldText;

@property(nonatomic, assign) BOOL cancelBackTap;    //是否取消点击背景关闭，默认 NO
@property(nonatomic, assign) BOOL requireInput;     //是否必须输入内容才可关闭，默认 NO

@end

@interface UIImage (QNColorful)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
