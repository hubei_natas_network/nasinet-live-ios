//
//  QNBlockButton.h
//  RoadBird
//
//  Created by mryun11 on 2017/10/20.
//  Copyright © 2017年 tang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^Block)(UIButton * button);

@interface QNBlockButton : UIButton

@property (nonatomic, copy) Block block;

@end
