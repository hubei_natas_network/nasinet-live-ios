//
//  QNBlockButton.m
//  RoadBird
//
//  Created by mryun11 on 2017/10/20.
//  Copyright © 2017年 tang. All rights reserved.
//

#import "QNBlockButton.h"

@implementation QNBlockButton

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self addTarget:self action:@selector(doAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)doAction:(UIButton *)button {
    self.block(button);
}

@end
