//
//  QNActivityIndicatorView.h
//  QNActivityIndicatorExample
//
//  Created by iDress on 5/23/15.
//  Copyright (c) 2015 iDress. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QNActivityIndicatorAnimationProtocol.h"

typedef NS_ENUM(NSUInteger, QNActivityIndicatorAnimationType) {
    QNActivityIndicatorAnimationTypeNineDots,
    QNActivityIndicatorAnimationTypeTriplePulse,
    QNActivityIndicatorAnimationTypeFiveDots,
    QNActivityIndicatorAnimationTypeRotatingSquares,
    QNActivityIndicatorAnimationTypeDoubleBounce,
    QNActivityIndicatorAnimationTypeRippleAnimation,
    QNActivityIndicatorAnimationTypeTwoDots,
    QNActivityIndicatorAnimationTypeThreeDots,
    QNActivityIndicatorAnimationTypeBallPulse,
    QNActivityIndicatorAnimationTypeBallClipRotate,
    QNActivityIndicatorAnimationTypeBallClipRotatePulse,
    QNActivityIndicatorAnimationTypeBallClipRotateMultiple,
    QNActivityIndicatorAnimationTypeBallRotate,
    QNActivityIndicatorAnimationTypeBallZigZag,
    QNActivityIndicatorAnimationTypeBallZigZagDeflect,
    QNActivityIndicatorAnimationTypeBallTrianglePath,
    QNActivityIndicatorAnimationTypeBallScale,
    QNActivityIndicatorAnimationTypeLineScale,
    QNActivityIndicatorAnimationTypeLineScaleParty,
    QNActivityIndicatorAnimationTypeBallScaleMultiple,
    QNActivityIndicatorAnimationTypeBallPulseSync,
    QNActivityIndicatorAnimationTypeBallBeat,
    QNActivityIndicatorAnimationType3DotsFadeAnimation,
    QNActivityIndicatorAnimationTypeLineScalePulseOut,
    QNActivityIndicatorAnimationTypeLineScalePulseOutRapid,
    QNActivityIndicatorAnimationTypeLineJumpUpAndDownAnimation,
    QNActivityIndicatorAnimationTypeBallScaleRipple,
    QNActivityIndicatorAnimationTypeBallScaleRippleMultiple,
    QNActivityIndicatorAnimationTypeTriangleSkewSpin,
    QNActivityIndicatorAnimationTypeBallGridBeat,
    QNActivityIndicatorAnimationTypeBallGridPulse,
    QNActivityIndicatorAnimationTypeRotatingSandglass,
    QNActivityIndicatorAnimationTypeRotatingTrigons,
    QNActivityIndicatorAnimationTypeTripleRings,
    QNActivityIndicatorAnimationTypeCookieTerminator,
    QNActivityIndicatorAnimationTypeBallSpinFadeLoader,
    QNActivityIndicatorAnimationTypeBallLoopScale,
    QNActivityIndicatorAnimationTypeExchangePosition,
    QNActivityIndicatorAnimationTypeRotaingCurveEaseOut,
    QNActivityIndicatorAnimationTypeLoadingSuccess,
    QNActivityIndicatorAnimationTypeLoadingFail,
    QNActivityIndicatorAnimationTypeBallRotaingAroundBall,
};

@interface QNActivityIndicatorView : UIView

+ (instancetype)showInWindow:(QNActivityIndicatorAnimationType)type;
+ (instancetype)showInWindow:(QNActivityIndicatorAnimationType)type tintColor:(UIColor *)tintColor;

+ (instancetype)showInView:(UIView *)view type:(QNActivityIndicatorAnimationType)type tintColor:(UIColor *)tintColor;

- (id)initWithType:(QNActivityIndicatorAnimationType)type;
- (id)initWithType:(QNActivityIndicatorAnimationType)type tintColor:(UIColor *)tintColor;
- (id)initWithType:(QNActivityIndicatorAnimationType)type tintColor:(UIColor *)tintColor indicatorSize:(CGFloat)indicatorSize containerSize:(CGFloat)containerSize;

@property (nonatomic) QNActivityIndicatorAnimationType type;
@property (nonatomic, strong) UIColor *tintColor;
@property (nonatomic) CGFloat containerSize;
@property (nonatomic) CGFloat indicatorSize;

@property (nonatomic, readonly) BOOL animating;

+ (id<QNActivityIndicatorAnimationProtocol>)activityIndicatorAnimationForAnimationType:(QNActivityIndicatorAnimationType)type;

- (void)startAnimating;
- (void)stopAnimating;

- (void)hidden;
- (void)hideAnimated:(BOOL)animated afterDelay:(NSTimeInterval)delay;

@end
