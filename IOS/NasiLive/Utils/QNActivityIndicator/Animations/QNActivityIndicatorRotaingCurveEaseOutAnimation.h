//
//  QNActivityIndicatorRotaingCurveEaseOutAnimation.h
//  QNActivityIndicatorExample
//
//  Created by Libo on 2017/12/28.
//  Copyright © 2017年 iDress. All rights reserved.
//  旋转，曲线由快到慢不断循环

#import "QNActivityIndicatorAnimation.h"

@interface QNActivityIndicatorRotaingCurveEaseOutAnimation : QNActivityIndicatorAnimation

@end
