//
//  QNActivityIndicator3DotsFadeAnimation.h
//  QNActivityIndicatorView
//
//  Created by 乐升平 on 2018/11/19.
//  Copyright © 2018 乐升平. All rights reserved.
//  这个动画类似于微信王者荣耀中网络卡顿时的加载动画

#import "QNActivityIndicatorAnimation.h"

NS_ASSUME_NONNULL_BEGIN

@interface QNActivityIndicator3DotsFadeAnimation : QNActivityIndicatorAnimation

@end

NS_ASSUME_NONNULL_END
