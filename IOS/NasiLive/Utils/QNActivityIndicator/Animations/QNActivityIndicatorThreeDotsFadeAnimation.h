//
//  QNActivityIndicatorThreeDotsFadeAnimation.h
//  QNActivityIndicatorView
//
//  Created by 乐升平 on 2018/11/19.
//  Copyright © 2018 乐升平. All rights reserved.
//

#import "QNActivityIndicatorAnimation.h"

NS_ASSUME_NONNULL_BEGIN

@interface QNActivityIndicatorThreeDotsFadeAnimation : QNActivityIndicatorAnimation

@end

NS_ASSUME_NONNULL_END
