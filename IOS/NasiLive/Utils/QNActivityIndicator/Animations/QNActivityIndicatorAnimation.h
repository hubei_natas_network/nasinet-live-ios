//
//  QNActivityIndicatorAnimation.h
//  QNActivityIndicatorExample
//
//  Created by iDress on 8/10/16.
//  Copyright © 2016 iDress. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "QNActivityIndicatorAnimationProtocol.h"

@interface QNActivityIndicatorAnimation : NSObject <QNActivityIndicatorAnimationProtocol>

- (CABasicAnimation *)createBasicAnimationWithKeyPath:(NSString *)keyPath;
- (CAKeyframeAnimation *)createKeyframeAnimationWithKeyPath:(NSString *)keyPath;
- (CAAnimationGroup *)createAnimationGroup;

@end
