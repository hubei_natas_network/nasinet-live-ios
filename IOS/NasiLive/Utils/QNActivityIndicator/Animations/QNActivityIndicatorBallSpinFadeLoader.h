//
//  QNActivityIndicatorBallSpinFadeLoader.h
//  CheeseDigest
//
//  Created by Robin.Chao on 9/8/15.
//  Copyright (c) 2015 mRocker. All rights reserved.
//

#import "QNActivityIndicatorAnimation.h"

@interface QNActivityIndicatorBallSpinFadeLoader : QNActivityIndicatorAnimation

@end
