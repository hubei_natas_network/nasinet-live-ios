//
//  QNActivityIndicatorView.m
//  QNActivityIndicatorExample
//
//  Created by iDress on 5/23/15.
//  Copyright (c) 2015 iDress. All rights reserved.
//

#import "QNActivityIndicatorView.h"

#import "QNActivityIndicatorNineDotsAnimation.h"
#import "QNActivityIndicatorTriplePulseAnimation.h"
#import "QNActivityIndicatorFiveDotsAnimation.h"
#import "QNActivityIndicatorRotatingSquaresAnimation.h"
#import "QNActivityIndicatorDoubleBounceAnimation.h"
#import "QNActivityIndicatorRippleAnimation.h"
#import "QNActivityIndicatorTwoDotsAnimation.h"
#import "QNActivityIndicatorThreeDotsAnimation.h"
#import "QNActivityIndicatorBallPulseAnimation.h"
#import "QNActivityIndicatorBallClipRotateAnimation.h"
#import "QNActivityIndicatorBallClipRotatePulseAnimation.h"
#import "QNActivityIndicatorBallClipRotateMultipleAnimation.h"
#import "QNActivityIndicatorBallRotateAnimation.h"
#import "QNActivityIndicatorBallZigZagAnimation.h"
#import "QNActivityIndicatorBallZigZagDeflectAnimation.h"
#import "QNActivityIndicatorBallTrianglePathAnimation.h"
#import "QNActivityIndicatorBallScaleAnimation.h"
#import "QNActivityIndicatorLineScaleAnimation.h"
#import "QNActivityIndicatorLineScalePartyAnimation.h"
#import "QNActivityIndicatorBallScaleMultipleAnimation.h"
#import "QNActivityIndicatorBallPulseSyncAnimation.h"
#import "QNActivityIndicatorBallBeatAnimation.h"
#import "QNActivityIndicatorLineScalePulseOutAnimation.h"
#import "QNActivityIndicatorLineScalePulseOutRapidAnimation.h"
#import "QNActivityIndicatorLineJumpUpAndDownAnimation.h"
#import "QNActivityIndicatorBallScaleRippleAnimation.h"
#import "QNActivityIndicatorBallScaleRippleMultipleAnimation.h"
#import "QNActivityIndicatorTriangleSkewSpinAnimation.h"
#import "QNActivityIndicatorBallGridBeatAnimation.h"
#import "QNActivityIndicatorBallGridPulseAnimation.h"
#import "QNActivityIndicatorRotatingSandglassAnimation.h"
#import "QNActivityIndicatorRotatingTrigonAnimation.h"
#import "QNActivityIndicatorTripleRingsAnimation.h"
#import "QNActivityIndicatorCookieTerminatorAnimation.h"
#import "QNActivityIndicatorBallSpinFadeLoader.h"
#import "QNActivityIndicatorBallLoopScaleAnimation.h"
#import "QNActivityIndicator3DotsExchangePositionAnimation.h"
#import "QNActivityIndicatorRotaingCurveEaseOutAnimation.h"
#import "QNActivityIndicatorLoadingSuccessAnimation.h"
#import "QNActivityIndicatorLoadingFailAnimation.h"
#import "QNActivityIndicatorBallRotaingAroundBallAnimation.h"
#import "QNActivityIndicator3DotsFadeAnimation.h"

static const CGFloat kQNActivityContainerDefaultSize = 80.0f;
static const CGFloat kQNActivityIndicatorDefaultSize = 40.0f;

@interface QNActivityIndicatorView () {
    CALayer *_animationLayer;
    UIView *_containerView;
}

@property (nonatomic, weak) NSTimer *hideDelayTimer;

@end

@implementation QNActivityIndicatorView

#pragma mark -
#pragma mark Constructors

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _tintColor = [UIColor whiteColor];
        _indicatorSize = kQNActivityIndicatorDefaultSize;
        _containerSize = kQNActivityContainerDefaultSize;
        [self commonInit];
    }
    return self;
}

+ (instancetype)showInWindow:(QNActivityIndicatorAnimationType)type{
    QNActivityIndicatorView *qnAc =[[QNActivityIndicatorView alloc] initWithType:type];
    [qnAc showInWindow];
    return qnAc;
}

+ (instancetype)showInWindow:(QNActivityIndicatorAnimationType)type tintColor:(UIColor *)tintColor;{
    QNActivityIndicatorView *qnAc =[[QNActivityIndicatorView alloc] initWithType:type tintColor:tintColor];
    [qnAc showInWindow];
    return qnAc;
}

+ (instancetype)showInView:(UIView *)view type:(QNActivityIndicatorAnimationType)type tintColor:(UIColor *)tintColor{
    QNActivityIndicatorView *qnAc =[[QNActivityIndicatorView alloc] initWithType:type tintColor:tintColor];
    [qnAc showInView:view];
    return qnAc;
}

- (id)initWithType:(QNActivityIndicatorAnimationType)type {
    return [self initWithType:type tintColor:[UIColor whiteColor] indicatorSize:kQNActivityIndicatorDefaultSize containerSize:kQNActivityContainerDefaultSize ];
}

- (id)initWithType:(QNActivityIndicatorAnimationType)type tintColor:(UIColor *)tintColor {
    return [self initWithType:type tintColor:tintColor indicatorSize:kQNActivityIndicatorDefaultSize containerSize:kQNActivityContainerDefaultSize];
}

- (id)initWithType:(QNActivityIndicatorAnimationType)type tintColor:(UIColor *)tintColor indicatorSize:(CGFloat)indicatorSize containerSize:(CGFloat)containerSize {
    self = [super init];
    if (self) {
        _type = type;
        _indicatorSize = indicatorSize;
        _containerSize = containerSize;
        _tintColor = tintColor;
        [self commonInit];
    }
    return self;
}

#pragma mark -
#pragma mark Methods

- (void)commonInit {
    self.userInteractionEnabled = YES;
    self.hidden = YES;
    
    _containerView = [[UIView alloc]init];
    _containerView.backgroundColor = [UIColor whiteColor];
    _containerView.layer.cornerRadius = 2.f;
    _containerView.layer.shadowColor = [UIColor blackColor].CGColor;
    _containerView.layer.shadowOffset = CGSizeZero;
    _containerView.layer.shadowRadius = 5.f;
    _containerView.layer.shadowOpacity = .1f;
    [self addSubview:_containerView];
    
    _animationLayer = [[CALayer alloc] init];
    _animationLayer.frame = _containerView.layer.bounds;
    [_containerView.layer addSublayer:_animationLayer];

    [self setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    [self setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
}

- (void)setupAnimation {
    _animationLayer.sublayers = nil;
    
    id<QNActivityIndicatorAnimationProtocol> animation = [QNActivityIndicatorView activityIndicatorAnimationForAnimationType:_type];
    
    if ([animation respondsToSelector:@selector(setupAnimationInLayer:withSize:tintColor:)]) {
        [animation setupAnimationInLayer:_animationLayer withSize:CGSizeMake(_indicatorSize, _indicatorSize) tintColor:_tintColor];
        _animationLayer.speed = 0.0f;
    }
}

- (void)startAnimating {
    if (!_animationLayer.sublayers) {
        [self setupAnimation];
    }
    self.hidden = NO;
    _animationLayer.speed = 1.0f;
    _animating = YES;
}

- (void)stopAnimating {
    _animationLayer.speed = 0.0f;
    _animating = NO;
    self.hidden = YES;
}

#pragma mark -
#pragma mark Setters

- (void)setType:(QNActivityIndicatorAnimationType)type {
    if (_type != type) {
        _type = type;
        
        [self setupAnimation];
    }
}

- (void)setIndicatorSize:(CGFloat)indicatorSize {
    if (_indicatorSize != indicatorSize) {
        _indicatorSize = indicatorSize;
        
        [self setupAnimation];
        [self invalidateIntrinsicContentSize];
    }
}

- (void)setContainerSize:(CGFloat)containerSize {
    if (_containerSize != containerSize) {
        _containerSize = containerSize;
        
        [self setupAnimation];
        [self invalidateIntrinsicContentSize];
    }
}

- (void)setTintColor:(UIColor *)tintColor {
    if (![_tintColor isEqual:tintColor]) {
        _tintColor = tintColor;
        
        CGColorRef tintColorRef = tintColor.CGColor;
        for (CALayer *sublayer in _animationLayer.sublayers) {
            sublayer.backgroundColor = tintColorRef;
            
            if ([sublayer isKindOfClass:[CAShapeLayer class]]) {
                CAShapeLayer *shapeLayer = [[CAShapeLayer alloc] init];
                shapeLayer.strokeColor = tintColorRef;
                shapeLayer.fillColor = tintColorRef;
            }
        }
    }
}

#pragma mark -
#pragma mark Getters

+ (id<QNActivityIndicatorAnimationProtocol>)activityIndicatorAnimationForAnimationType:(QNActivityIndicatorAnimationType)type {
    switch (type) {
        case QNActivityIndicatorAnimationTypeNineDots:
            return [[QNActivityIndicatorNineDotsAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeTriplePulse:
            return [[QNActivityIndicatorTriplePulseAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeFiveDots:
            return [[QNActivityIndicatorFiveDotsAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeRotatingSquares:
            return [[QNActivityIndicatorRotatingSquaresAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeDoubleBounce:
            return [[QNActivityIndicatorDoubleBounceAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeRippleAnimation:
            return [[QNActivityIndicatorRippleAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeTwoDots:
            return [[QNActivityIndicatorTwoDotsAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeThreeDots:
            return [[QNActivityIndicatorThreeDotsAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeBallPulse:
            return [[QNActivityIndicatorBallPulseAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeBallClipRotate:
            return [[QNActivityIndicatorBallClipRotateAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeBallClipRotatePulse:
            return [[QNActivityIndicatorBallClipRotatePulseAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeBallClipRotateMultiple:
            return [[QNActivityIndicatorBallClipRotateMultipleAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeBallRotate:
            return [[QNActivityIndicatorBallRotateAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeBallZigZag:
            return [[QNActivityIndicatorBallZigZagAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeBallZigZagDeflect:
            return [[QNActivityIndicatorBallZigZagDeflectAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeBallTrianglePath:
            return [[QNActivityIndicatorBallTrianglePathAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeBallScale:
            return [[QNActivityIndicatorBallScaleAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeLineScale:
            return [[QNActivityIndicatorLineScaleAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeLineScaleParty:
            return [[QNActivityIndicatorLineScalePartyAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeBallScaleMultiple:
            return [[QNActivityIndicatorBallScaleMultipleAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeBallPulseSync:
            return [[QNActivityIndicatorBallPulseSyncAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeBallBeat:
            return [[QNActivityIndicatorBallBeatAnimation alloc] init];
        case QNActivityIndicatorAnimationType3DotsFadeAnimation:
            return [[QNActivityIndicator3DotsFadeAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeLineScalePulseOut:
            return [[QNActivityIndicatorLineScalePulseOutAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeLineScalePulseOutRapid:
            return [[QNActivityIndicatorLineScalePulseOutRapidAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeLineJumpUpAndDownAnimation:
            return [[QNActivityIndicatorLineJumpUpAndDownAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeBallScaleRipple:
            return [[QNActivityIndicatorBallScaleRippleAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeBallScaleRippleMultiple:
            return [[QNActivityIndicatorBallScaleRippleMultipleAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeTriangleSkewSpin:
            return [[QNActivityIndicatorTriangleSkewSpinAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeBallGridBeat:
            return [[QNActivityIndicatorBallGridBeatAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeBallGridPulse:
            return [[QNActivityIndicatorBallGridPulseAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeRotatingSandglass:
            return [[QNActivityIndicatorRotatingSandglassAnimation alloc]init];
        case QNActivityIndicatorAnimationTypeRotatingTrigons:
            return [[QNActivityIndicatorRotatingTrigonAnimation alloc]init];
        case QNActivityIndicatorAnimationTypeTripleRings:
            return [[QNActivityIndicatorTripleRingsAnimation alloc]init];
        case QNActivityIndicatorAnimationTypeCookieTerminator:
            return [[QNActivityIndicatorCookieTerminatorAnimation alloc]init];
        case QNActivityIndicatorAnimationTypeBallSpinFadeLoader:
            return [[QNActivityIndicatorBallSpinFadeLoader alloc] init];
        case QNActivityIndicatorAnimationTypeBallLoopScale:
            return [[QNActivityIndicatorBallLoopScaleAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeExchangePosition:
            return [[QNActivityIndicator3DotsExchangePositionAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeRotaingCurveEaseOut:
            return [[QNActivityIndicatorRotaingCurveEaseOutAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeLoadingSuccess:
            return [[QNActivityIndicatorLoadingSuccessAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeLoadingFail:
            return [[QNActivityIndicatorLoadingFailAnimation alloc] init];
        case QNActivityIndicatorAnimationTypeBallRotaingAroundBall:
            return [[QNActivityIndicatorBallRotaingAroundBallAnimation alloc] init];
            
    }
    return nil;
}

#pragma mark -
#pragma mark Layout

- (void)layoutSubviews {
    [super layoutSubviews];
    
    _containerView.frame = CGRectMake((self.bounds.size.width - _containerSize)/2, (self.bounds.size.height - _containerSize)/2, _containerSize, _containerSize);
    _animationLayer.frame = _containerView.bounds;

    BOOL animating = _animating;

    if (animating)
        [self stopAnimating];

    [self setupAnimation];

    if (animating)
        [self startAnimating];
}

- (CGSize)intrinsicContentSize {
    return CGSizeMake(_indicatorSize, _indicatorSize);
}

- (void)showInWindow{
    UIViewController *topVC = [self appRootViewController];
    [topVC.view addSubview:self];
    CGRect frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    self.frame = frame;
    [self startAnimating];
}

- (void)showInView:(UIView *)view{
    [view addSubview:self];
    CGRect frame = view.bounds;
    self.frame = frame;
    [self startAnimating];
}

- (UIViewController *)appRootViewController{
    UIViewController *appRootVC = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *topVC = appRootVC;
    while (topVC.presentedViewController) {
        topVC = topVC.presentedViewController;
    }
    return topVC;
}

- (void)hidden{
    [self stopAnimating];
    [self removeFromSuperview];
}

- (void)hideAnimated:(BOOL)animated{
    if (animated) {
        [UIView animateWithDuration:0.5f animations:^{
            [self hidden];
        }];
    }else{
        [self hidden];
    }
}

- (void)hideAnimated:(BOOL)animated afterDelay:(NSTimeInterval)delay{
    // Cancel any scheduled hideAnimated:afterDelay: calls
    [self.hideDelayTimer invalidate];

    NSTimer *timer = [NSTimer timerWithTimeInterval:delay target:self selector:@selector(handleHideTimer:) userInfo:@(animated) repeats:NO];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    self.hideDelayTimer = timer;
}

- (void)handleHideTimer:(NSTimer *)timer{
    [self hideAnimated:[timer.userInfo boolValue]];
    [timer invalidate];
}

@end
