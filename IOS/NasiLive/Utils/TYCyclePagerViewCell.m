//
//  TYCyclePagerViewCell.m
//  TYCyclePagerViewDemo
//
//  Created by tany on 2017/6/14.
//  Copyright © 2017年 tany. All rights reserved.
//

#import "TYCyclePagerViewCell.h"

@interface TYCyclePagerViewCell ()
@end

@implementation TYCyclePagerViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        [self addImgView];
    }
    return self;
}


- (void)addImgView {
    UIImageView *imgView = [[UIImageView alloc]init];
    imgView.layer.cornerRadius = 10.f;
    imgView.layer.masksToBounds = YES;
    imgView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:imgView];
    _imgView = imgView;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _imgView.frame = self.bounds;
}

@end
