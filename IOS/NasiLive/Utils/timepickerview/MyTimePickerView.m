//
//  MyTimePicker.m
//  TimePicker
//
//  Created by App on 1/13/16.
//  Copyright © 2016 App. All rights reserved.
//

#import "MyTimePickerView.h"
#import "MyTimeTool.h"

#define SCREENSIZE [UIScreen mainScreen].bounds.size
#define HEIGHTCOUNT 0.5

#define HOURARRAY @[@"0点", @"1点", @"2点", @"3点", @"4点", @"5点", @"6点", @"7点", @"8点", @"9点", @"10点", @"11点", @"12点", @"13点", @"14点", @"15点", @"16点", @"17点", @"18点", @"19点", @"20点", @"21点", @"22点", @"23点"]
#define MINUTEARRAY @[@"0分", @"10分", @"20分", @"30分", @"40分", @"50分"]

#define COLOR(R, G, B, A) [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:A]  

typedef void(^CompleteBolck) (NSDictionary *);

@interface MyTimePickerView ()<UIPickerViewDataSource, UIPickerViewDelegate>


@property (strong, nonatomic)   UIPickerView *pickerView;

@property (nonatomic, strong) NSArray *dayArray;
@property (nonatomic, strong) NSArray *showDayArray;
@property (nonatomic, strong) NSArray *hourArray;
@property (nonatomic, strong) NSArray *minuteArray;
@property (nonatomic, strong) NSArray *totalArray;


@property (nonatomic, assign) NSInteger columnIndex;
@property (nonatomic, assign) NSInteger rowIndex;

@property (nonatomic, copy) CompleteBolck completeBlock;
@property (nonatomic, copy) NSString *deadLine;
@property (nonatomic, copy) NSDate *beginDate;
@property (nonatomic, copy) NSString *title;

@end

@implementation MyTimePickerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(id)initWithFrame:(CGRect)frame   be:(NSDate *)beginDate  deadLine:(NSString *)deadLine title:(NSString *)title
{
    self = [super initWithFrame:frame];
    if (self) {
        
        //设置半透明
        self.backgroundColor = COLOR(58, 59, 58, 0.5);
       // self.alpha= 0.8;//会让子空间夜透明
        _deadLine = deadLine;
        _title = title;
        _beginDate = beginDate;
        [self  initUI];
        [self  initData];
    }
    return self;
    
}


-(void)initUI
{
    CGRect rect = CGRectMake( 0,  0.6* self.height ,  self.width , 0.4* self.height);
    UIView *bg= [[UIView alloc]initWithFrame:rect];
    
   
    CGRect rTool=  CGRectMake( 0,  0 ,  self.width ,  40);
    UIView *tool= [[UIView alloc]initWithFrame:rTool];
    tool.backgroundColor = CViewBgColor;
    [bg addSubview:tool];
    
    
    UIButton *btnCancel= [[UIButton alloc]initWithFrame: CGRectMake( 0, 0,  80,  40)];
    btnCancel.titleLabel.font= [UIFont systemFontOfSize: 16];
    [btnCancel setTitle: @"取消" forState: UIControlStateNormal];
    [btnCancel setTitleColor:CFontColor1 forState:UIControlStateNormal];
    [btnCancel addTarget:self action:@selector(cancelBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [tool addSubview: btnCancel];
    
    
    UIButton *btnOK= [[UIButton alloc]initWithFrame: CGRectMake(self.width - 80,0,  80, 40)];
    btnOK.titleLabel.font= [UIFont systemFontOfSize: 16];
    [btnOK setTitle: @"选择" forState: UIControlStateNormal];
    [btnOK setTitleColor:CFontColor1 forState:UIControlStateNormal];
    [btnOK addTarget:self action:@selector(sureBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [tool addSubview: btnOK];
    
    
    
    UILabel  * lblTitle= [[UILabel alloc]initWithFrame: CGRectMake( 80, 0, self.width- 160,  40)];
    lblTitle.textColor = CFontColor1;
    lblTitle.text = _title;
    lblTitle.textAlignment = NSTextAlignmentCenter;
   
    [tool addSubview: lblTitle];
    
    
    CGRect rTime =CGRectMake( 0,   40,  self.width ,  0.4* self.height- 40);
    
    _pickerView =[[UIPickerView alloc]initWithFrame:rTime];
    _pickerView.dataSource = self;
    _pickerView.delegate = self;
    _pickerView.backgroundColor= [UIColor whiteColor];
    
    [bg addSubview: _pickerView];
    
    [self addSubview: bg];

    UITapGestureRecognizer*tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(remove:)];
    
    [self addGestureRecognizer:tapGesture];
    
    
}


-(void)remove:(id)sender
{
    [ self  removeFromSuperview];
}



-(void)awakeFromNib{
    NSLog(@"awakeFromNib---");
   
}



//-(void)layoutSubviews{
//    [super layoutSubviews];
//    NSLog(@"layout subviews---%@", NSStringFromCGSize(SCREENSIZE));
////    self.frame =
////    self.topLabel.frame = CGRectMake(0, 0.2 * 0.06 * SCREENSIZE.height, SCREENSIZE.width, 0.6 * 0.1 *SCREENSIZE.height);
////    self.pickerView.frame = CGRectMake(0, 0.1 * SCREENSIZE.height, SCREENSIZE.width, 0.3 * SCREENSIZE.height);
////    self.cancelBtn.frame = CGRectMake( 0.2 * 0.0.06 * SCREENSIZE.height, self.cancelBtn.frame.origin.y, 100, 60);
////    self.sureBtn.frame = CGRectMake(SCREENSIZE.width - 0.2 * 0.1 * SCREENSIZE.height, self.cancelBtn.frame.origin.y, 60, 60);
//    
//}
 

+(void)showTimePickerView:(NSDate *)beginDate  day:(CGFloat) day    title:(NSString *)title  CompleteBlock:(void (^)(NSDictionary *infoDic))completeBlock
{
    NSDate * endDate=  [NSDate dateWithTimeInterval: day*24*60*60     sinceDate: beginDate];
   NSString *deadLine= [MyTimeTool summaryTimeUsingDate: endDate];
    
    CGRect rect= CGRectMake( 0, 0, KScreenWidth, KScreenHeight);
    MyTimePickerView *timePickerView = [ [MyTimePickerView alloc]initWithFrame: rect   be:beginDate   deadLine :deadLine  title: title];
    timePickerView.completeBlock = completeBlock;
    
    [[UIApplication sharedApplication].keyWindow addSubview:timePickerView ];
    //    CGRect frame = CGRectMake(0, (1-HEIGHTCOUNT) * 0.5 * SCREENSIZE.height, SCREENSIZE.width, HEIGHTCOUNT * SCREENSIZE.height);
    [UIView animateWithDuration:0.5 animations:^{
        
    } completion:^(BOOL finished) {  }];

    
}

+(void)showTimePickerView:(NSDate *)beginDate  second:(CGFloat) second    title:(NSString *)title  CompleteBlock:(void (^)(NSDictionary *infoDic))completeBlock
{
    NSDate * endDate=  [NSDate dateWithTimeInterval:second     sinceDate: beginDate];
    NSString *deadLine= [MyTimeTool summaryTimeUsingDate: endDate];
    
    CGRect rect= CGRectMake( 0, 0, KScreenWidth, KScreenHeight);
    MyTimePickerView *timePickerView = [ [MyTimePickerView alloc]initWithFrame: rect   be:beginDate   deadLine :deadLine  title: title];
    timePickerView.completeBlock = completeBlock;
    
    [[UIApplication sharedApplication].keyWindow addSubview:timePickerView ];
    //    CGRect frame = CGRectMake(0, (1-HEIGHTCOUNT) * 0.5 * SCREENSIZE.height, SCREENSIZE.width, HEIGHTCOUNT * SCREENSIZE.height);
    [UIView animateWithDuration:0.5 animations:^{
        
    } completion:^(BOOL finished) {  }];
    
}

 
- (void)sureBtnAction:(id)sender {
    if(self.completeBlock){
        NSMutableDictionary *infoDic = [NSMutableDictionary dictionary];
        NSInteger firstIndex = [_pickerView selectedRowInComponent:0];
        NSInteger secodnIndex = [_pickerView selectedRowInComponent:1];
        NSInteger thirdIndex = [_pickerView selectedRowInComponent:2];
        NSMutableString *dateValue = [[NSMutableString alloc] initWithString:_dayArray[firstIndex]];
        [dateValue appendString:[self formatOriArray:_hourArray][secodnIndex]];
        [dateValue appendString:[self formatOriArray:_minuteArray][thirdIndex]];
        infoDic[@"time_value"] = dateValue;
        
        
        NSDateFormatter *f = [[NSDateFormatter alloc] init];
        [f setDateFormat:@"yyyyMMddHHmm"];
        NSDate * date = [f dateFromString: dateValue] ;
         
        infoDic[@"date"]=   date;
        
        
        self.completeBlock(infoDic);
    }
    
    [self   removeFromSuperview];
}

-(NSArray *)formatOriArray:(NSArray *)oriArray{
    NSMutableArray *newArray = [NSMutableArray array];
    for (NSString *hourStr in oriArray) {
        NSString *tmpStr = [self removeLastChareacter:hourStr];
        [newArray addObject: [self append0IfNeed:tmpStr]];
    }
    return newArray;
}

-(NSString *)removeLastChareacter:(NSString *)oriString{
    return [oriString substringToIndex:oriString.length -1];
}

-(NSString *)append0IfNeed:(NSString *)oriString{
    if(oriString.length <2) return [NSString stringWithFormat:@"0%@", oriString];
    return oriString;
}

- (void)cancelBtnAction:(id)sender {
    [self  removeFromSuperview];
}

#pragma mark - pickerview data and delegate
-(void)initData{
    //_dayArray = [MyTimeTool daysFromNowToDeadLine:self.deadLine];
    
    _dayArray =[MyTimeTool  daysFromDateToDeadLine: self.beginDate  dl:self.deadLine];
    _showDayArray = [self genShowDayArrayByDayArray:_dayArray];
    _hourArray = [self validHourArray];
    _minuteArray = [self validMinuteArray];
}

-(NSArray *)genShowDayArrayByDayArray:(NSArray *)dayArray{
 
    NSMutableArray *showDayArray = [NSMutableArray arrayWithArray:dayArray];
    for (int i = 0; i< showDayArray.count; i++) {
        
       showDayArray[i] = [MyTimeTool displayedSummaryTimeUsingString2:dayArray[i]] ;
    }
    return showDayArray;
}

 

-(NSArray *)validHourArray{
    int startIndex = [MyTimeTool DateHour: _beginDate];
    if ([MyTimeTool  DateMinute:_beginDate] >= 50) startIndex++;
    return [HOURARRAY subarrayWithRange:NSMakeRange(startIndex, HOURARRAY.count - startIndex)];
}

-(NSArray *)validMinuteArray{
    int startIndex = [MyTimeTool  DateMinute:_beginDate] / 10 +1;
    if ([MyTimeTool  DateMinute:_beginDate] >= 50) startIndex = 0;
    return [MINUTEARRAY subarrayWithRange:NSMakeRange(startIndex, MINUTEARRAY.count - startIndex)];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    switch (component) {
        case 0:
            return self.showDayArray.count;
        case 1:
            return self.hourArray.count;
        case 2:
            return self.minuteArray.count;
        default:
            return 0;
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    int firstComponentSelectedRow = [self.pickerView selectedRowInComponent:0];
    if (firstComponentSelectedRow == 0) {
        _hourArray = [self validHourArray];
        _minuteArray = [self validMinuteArray];
        int secondComponentSelectedRow = [self.pickerView selectedRowInComponent:1];
        if (secondComponentSelectedRow == 0 || component ==0) {
            _minuteArray = [self validMinuteArray];
//            if(component == 1) [self.pickerView selectRow:0 inComponent:2 animated:YES];
        }else{
            _minuteArray = MINUTEARRAY;
        }
    }else{
        _hourArray = HOURARRAY;
        _minuteArray = MINUTEARRAY;
    }
    [self.pickerView reloadAllComponents];
    
    //当第一列滑到第一个位置时，第二，三列滚回到0位置
    if(component == 0){
        [self.pickerView selectRow:0 inComponent:1 animated:YES];
        [self.pickerView selectRow:0 inComponent:2 animated:YES];
    }
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel *label;
    if (view) {
        label = (UILabel *)view;
    }else{
        label = [[UILabel alloc] init];
    }
    label.textAlignment = NSTextAlignmentCenter;
    switch (component) {
        case 0:
            label.text = self.showDayArray[row];
            //            [label setBackgroundColor:[UIColor redColor]];
            break;
        case 1:
            label.text = self.hourArray[row];
            //            [label setBackgroundColor:[UIColor greenColor]];
            break;
        case 2:
            label.text = self.minuteArray[row];
            //            [label setBackgroundColor:[UIColor lightGrayColor]];
            break;
        default:
            break;
    }
    
    return label;
}

//view的宽度
-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    return self.frame.size.width / 3.0;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 44;
}


@end
