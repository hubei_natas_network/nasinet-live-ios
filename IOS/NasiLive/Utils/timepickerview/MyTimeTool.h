//
//  TimeTool.h
//  TimePicker
//
//  Created by App on 1/14/16.
//  Copyright © 2016 App. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyTimeTool : NSObject

+(NSArray *)daysFromNowToDeadLine:(NSString *)deadLine;
+(NSArray *)daysFromDateToDeadLine:(NSDate*)date   dl:(NSString *)deadLine;
+(int)currentDateHour;

+(int)currentDateMinute;

+(int)DateHour:(NSDate *) date;

+(int)DateMinute:(NSDate *) date;

+(NSString *)displayedSummaryTimeUsingString:(NSString *)string;
+(NSString *)displayedSummaryTimeUsingString2:(NSString *)string;
+(NSString *)summaryTimeUsingDate:(NSDate *)date;

@end
