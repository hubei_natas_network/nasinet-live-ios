//
//  MyTimePicker.h
//  TimePicker
//
//  Created by App on 1/13/16.
//  Copyright © 2016 App. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CompleteBolck) (NSDictionary *);

@interface MyTimePickerView : UIView

 
+(void)showTimePickerView:(NSDate *)beginDate  day:(CGFloat) day    title:(NSString *)title  CompleteBlock:(void (^)(NSDictionary *infoDic))completeBlock;


+(void)showTimePickerView:(NSDate *)beginDate  second:(CGFloat) second    title:(NSString *)title  CompleteBlock:(void (^)(NSDictionary *infoDic))completeBlock;

@end
