//
//  NasiH5ViewController.h
//  NasiLive
//
//  Created by yun11 on 2020/8/21.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "NasiRootViewController.h"

#import <WebKit/WebKit.h>
#import <JavaScriptCore/JavaScriptCore.h>

NS_ASSUME_NONNULL_BEGIN

@interface NasiH5ViewController : NasiRootViewController

@property (nonatomic, strong) WKWebView     *webView;
@property (strong, nonatomic) UIView        *statusBarBackView;

@property (copy, nonatomic) NSString        *rootHref;

- (void)popWebView;
- (void)setStatusBarBackColor:(NSDictionary *)dict;
- (void)setStatusBarTintStyle:(NSDictionary *)dict;

//JS调用OC方法
- (void)handlerJavaScriptMethod:(NSString *)method data:(NSDictionary *)data;

@end

NS_ASSUME_NONNULL_END
