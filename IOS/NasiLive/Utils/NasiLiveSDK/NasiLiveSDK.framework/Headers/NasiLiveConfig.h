//
//  NasiLiveConfig.h
//  NasiLiveSDK
//
//  Created by yun11 on 2020/8/17.
//  Copyright © 2020 yun11. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NasiLiveConfig : NSObject

/**
 * 导航栏背景色
 */
@property (strong, nonatomic) UIColor *naviBgColor;

/**
 * 导航栏字体色
 */
@property (strong, nonatomic) UIColor *naviTitleColor;

/**
 * 页面背景色
 */
@property (strong, nonatomic) UIColor *viewBgColor;

@end

