//
//  NasiTableViewCell.h
//  Nasi
//
//  Created by yun on 2019/12/24.
//  Copyright © 2019 yun7. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NasiTableViewCell : UITableViewCell

+ (void)registerWithTableView:(UITableView *)tableView;

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath;

+ (CGFloat)cellHeight;

@end

NS_ASSUME_NONNULL_END
