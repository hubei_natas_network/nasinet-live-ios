//
//  CommonManager.h
//  JYZC
//
//  Created by yun_7 on 2018/8/15.
//  Copyright © 2018年 yun_7. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NasiCommonManager : NSObject

+ (NSDate *)dateWithString:(NSString *)dateStr;

+ (NSDate *)dateWithString:(NSString *)dateStr formatter:(NSString *)formatter;

+ (NSString *)stringWithDate:(NSDate *)date formatter:(NSString *)formatter;

+ (NSDateComponents *)calculateComponentsFrom:(NSDate *)fromDate to:(NSDate *)toDate;

//日期对比
+ (int)compareOneDay:(NSDate *)currentDay withAnotherDay:(NSDate *)BaseDay;

+ (NSString *)formateSecond:(NSInteger)count withHour:(BOOL)hour;

+ (NSString *)formateBeautyDate:(NSString *)date;

+ (NSString *)formateCount:(NSInteger)count;

//手机号
+ (BOOL)checkPhone:(NSString *)phone;

//邮箱
+ (BOOL)checkEmailAddress:(NSString *)email;

//身份证号
+ (BOOL)checksimpleVerifyIdentityCardNum:(NSString *)cardNum;

//车牌
+ (BOOL)checkCarNumber:(NSString *)carNum;

//设置行间距
+ (NSAttributedString *)getAttributedStringWithString:(NSString *)string lineSpace:(CGFloat)lineSpace;
//设置行间距 颜色
+ (NSAttributedString *)getAttributedStringWithString:(NSString *)string lineSpace:(CGFloat)lineSpace colorfulRanges:(NSArray<NSString *> *)colorfulRanges colors:(NSArray<UIColor *> *)colors;

//图片高斯模糊
+ (UIImage *)blurryImage:(UIImage *)image withBlurLevel:(CGFloat)blur;

//根据文件名拼接文件路径
+ (NSString *)getFilePathWithFileName:(NSString *)fileName;
//以当前时间合成视频名称
+ (NSString *)getNameBaseCurrentTime:(NSString *)suf;

//文字匹配
+ (NSArray <NSTextCheckingResult *> *)machesWithPattern:(NSString *)pattern  andStr:(NSString *)str;


@end
