//
//  NasiLiveManager.h
//  NasiLiveSDK
//
//  Created by yun11 on 2020/8/17.
//  Copyright © 2020 yun11. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NasiLiveConfig.h"

typedef void (^NasiLiveInitCallbackBlock)(int code);

@interface NasiLiveManager : NSObject

@property (strong, nonatomic) NasiLiveConfig *config;

+ (instancetype)sharedInstance;

/**
 * 初始化函数
 *
 * @param access_key             在线鉴权AccessKey
 * @param secret_key             在线鉴权SecretKey
 * @param block                      初始化回调block
 */
- (void)initAccessKey:(NSString *)access_key secretKey:(NSString *)secret_key CallBack:(NasiLiveInitCallbackBlock)block;

@end

