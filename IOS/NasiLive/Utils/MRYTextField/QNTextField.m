//
//  QNTextField.m
//  RoadBird
//
//  Created by mryun11 on 2017/10/24.
//  Copyright © 2017年 tang. All rights reserved.
//

#import "QNTextField.h"

@interface QNTextField()<UITextFieldDelegate>

@property(nonatomic, assign) QNTextFieldType type;

@end

@implementation QNTextField

- (instancetype)initWithType:(QNTextFieldType)type frame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.type = type;
        self.delegate = self;
        
        UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
        leftView.backgroundColor = [UIColor clearColor];
        self.leftView = leftView;
        self.leftViewMode = UITextFieldViewModeAlways;
        
        if (type == QNTextFieldTypeNumber) {
            self.keyboardType = UIKeyboardTypeNumberPad;
        }else if (type == QNTextFieldTypePhoneNumber){
            self.keyboardType = UIKeyboardTypePhonePad;
        }else if (type == QNTextFieldTypeDecimal){
            self.keyboardType = UIKeyboardTypeDecimalPad;
        }
    }
    return self;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (self.type == QNTextFieldTypeNumber) {
        NSString *regex = @"[0-9]*";
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
        if ([pred evaluateWithObject:string]) {
            return YES;
        }
        return NO;
    }else if (self.type == QNTextFieldTypeDecimal){
        NSString *regex = @"[0-9]*";
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
        if ([pred evaluateWithObject:string] || [string isEqualToString:@"."]) {
            if (string.length == 0) {
                return YES;
            }
            if ([textField.text containsString:@"."] && [string containsString:@"."]) {
                return NO;
            }
            if (textField.text.length == 0 && [string hasPrefix:@"."]) {
                return NO;
            }
            if ([textField.text rangeOfString:@"."].location > 0 && [textField.text rangeOfString:@"."].length > 0) {
                if ([textField.text substringFromIndex:[textField.text rangeOfString:@"."].location].length > 2) {
                    return NO;
                }
            }
            return YES;
        }
        return NO;
    }else if(self.type == QNTextFieldTypePhoneNumber){
        if (textField.text.length >= 11) {
            return NO;
        }
        NSString *regex = @"[0-9]*";
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
        if ([pred evaluateWithObject:string]) {
            return YES;
        }
        return NO;
    }else if (self.type == QNTextFieldTypeNoSpecialWord){
        NSString *regex = @"[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？%+_]";
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
        if ([pred evaluateWithObject:string]){
            return NO;
        }
    }
    return YES;
}

@end
