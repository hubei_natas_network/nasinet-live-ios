//
//  QNTextField.h
//  RoadBird
//
//  Created by mryun11 on 2017/10/24.
//  Copyright © 2017年 tang. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    QNTextFieldTypeNormal,
    QNTextFieldTypeNumber,
    QNTextFieldTypeDecimal,
    QNTextFieldTypePhoneNumber,
    QNTextFieldTypeNoSpecialWord,
}QNTextFieldType;

@interface QNTextField : UITextField

- (instancetype)initWithType:(QNTextFieldType)type frame:(CGRect)frame;

@end
