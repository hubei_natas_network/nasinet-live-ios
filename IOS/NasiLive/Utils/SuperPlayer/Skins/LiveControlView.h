//
//  LiveControlView.h
//  NasiLive
//
//  Created by yun on 2020/2/26.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "SuperPlayerControlView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LiveControlView : SuperPlayerControlView

/** 标题 */
@property (nonatomic, strong) UILabel                 *titleLabel;
/** 开始播放按钮 */
@property (nonatomic, strong) UIButton                *startBtn;
/** 全屏按钮 */
@property (nonatomic, strong) UIButton                *fullScreenBtn;
/** 锁定屏幕方向按钮 */
@property (nonatomic, strong) UIButton                *lockBtn;

/** 热度信息 */
@property (nonatomic, strong) UIButton                *hotBtn;

/** 礼物按钮 */
@property (nonatomic, strong) UIButton                *giftBtn;
/** 发消息按钮 */
@property (nonatomic, strong) UIButton                *msgBtn;
/** 分享按钮 */
@property (nonatomic, strong) UIButton                *shareBtn;

/** 返回按钮*/
@property (nonatomic, strong) UIButton                *backBtn;
/// 是否禁用返回
@property BOOL                                        disableBackBtn;
/** bottomView*/
@property (nonatomic, strong) UIImageView             *bottomImageView;
/** topView */
@property (nonatomic, strong) UIImageView             *topImageView;
/** 弹幕按钮 */
@property (nonatomic, strong) UIButton                *danmakuBtn;
/// 是否禁用弹幕
@property BOOL                                        disableDanmakuBtn;
/** 截图按钮 */
@property (nonatomic, strong) UIButton                *captureBtn;
/// 是否禁用截图
@property BOOL                                        disableCaptureBtn;
/** 更多按钮 */
@property (nonatomic, strong) UIButton                *moreBtn;
/// 是否禁用更多
@property BOOL                                        disableMoreBtn;
/** 播放按钮 */
@property (nonatomic, strong) UIButton                *playeBtn;
/** 加载失败按钮 */
@property (nonatomic, strong) UIButton                *middleBtn;

/** 更多设置View */
@property (nonatomic, strong) MoreContentView        *moreContentView;

/// 画面比例
@property CGFloat videoRatio;

/** 是否全屏播放 */
@property (nonatomic, assign,getter=isFullScreen)BOOL fullScreen;
@property (nonatomic, assign,getter=isLockScreen)BOOL isLockScreen;

@property (copy, nonatomic) NSString *hotValue;

@end

NS_ASSUME_NONNULL_END
