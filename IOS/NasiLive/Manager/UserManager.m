//
//  UserManager.m
//  yun_7
//
//  Created by yun_7 on 2017/5/22.
//  Copyright © 2017年 yun_7. All rights reserved.
//

#import "UserManager.h"

@implementation UserManager

SINGLETON_FOR_CLASS(UserManager);

- (instancetype)init{
    self = [super init];
    if (self) {
        //被踢下线
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(onKick)
                                                     name:KNotificationOnKick
                                                   object:nil];
    }
    return self;
}

#pragma mark ————— 注册 —————
- (void)registParams:(NSDictionary *)params completion:(appLoginBlock)completion{
    [MBProgressHUD showActivityMessageInView:@"注册中..."];
    [QNNetworkHelper POST:API(@"user/regist") parameters:params success:^(id responseObject) {
        [MBProgressHUD hideHUD];
        [self LoginSuccess:responseObject completion:completion];
    } failure:^(NSError *error) {
        [MBProgressHUD hideHUD];
        if (completion) {
            completion(NO,error.localizedDescription);
        }
    }];
}

#pragma mark ————— 三方登录 —————
- (void)login:(UserLoginType )loginType completion:(appLoginBlock)completion{
    [self login:loginType params:nil completion:completion];
}

#pragma mark ————— 带参数登录 —————
- (void)login:(UserLoginType )loginType params:(NSDictionary *)params completion:(appLoginBlock)completion{
    if (loginType == kUserLoginTypePwd) {
        [self loginToServer:params completion:completion];
    }else if (loginType == kUserLoginTypeWeChat){
        [self loginByWx:params completion:completion];
    }else if (loginType == kUserLoginTypeQQ){
        [self loginByQQ:params completion:completion];
    }
}

#pragma mark ————— 手动登录到服务器 —————
- (void)loginToServer:(NSDictionary *)params completion:(appLoginBlock)completion{
    [commonManager showLoadingAnimateInWindow];
    [QNNetworkHelper POST:API(@"user/login") parameters:params success:^(id responseObject) {
        [commonManager hideAnimateHud];
        [self LoginSuccess:responseObject completion:completion];
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        if (completion) {
            completion(NO,error.localizedDescription);
        }
    }];
}

#pragma mark ————— 自动登录到服务器 —————
- (void)autoLoginToServer:(appLoginBlock)completion{
    [QNNetworkHelper POST:API(@"user/autologin") parameters:@{@"uid":@(userManager.curUserInfo.userid), @"token":userManager.curUserInfo.token} success:^(id responseObject) {
        self.curUserInfo = [UserInfoModel yy_modelWithDictionary:responseObject[@"data"]];
        [self saveUserInfo];
    } failure:^(NSError *error) {
        if (completion) {
            completion(NO,error.localizedDescription);
        }
    }];
}

- (void)loginByWx:(NSDictionary *)params completion:(appLoginBlock)completion{
    [commonManager showLoadingAnimateInWindow];
    [QNNetworkHelper POST:API(@"user/wxlogin") parameters:params success:^(id responseObject) {
        [commonManager hideAnimateHud];
        [self LoginSuccess:responseObject completion:completion];
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        if (completion) {
            completion(NO,error.localizedDescription);
        }
    }];
}

- (void)loginByQQ:(NSDictionary *)params completion:(appLoginBlock)completion{
    [commonManager showLoadingAnimateInWindow];
    [QNNetworkHelper POST:API(@"user/qqlogin") parameters:params success:^(id responseObject) {
        [commonManager hideAnimateHud];
        [self LoginSuccess:responseObject completion:completion];
    } failure:^(NSError *error) {
        [commonManager hideAnimateHud];
        if (completion) {
            completion(NO,error.localizedDescription);
        }
    }];
}

#pragma mark ————— 登录成功处理 —————
- (void)LoginSuccess:(id )responseObject completion:(appLoginBlock)completion{
    if (ValidDict(responseObject)) {
        if (RESP_SUCCESS(responseObject)) {
            self.curUserInfo = [UserInfoModel yy_modelWithDictionary:responseObject[@"data"]];
            [self saveUserInfo];
            KPostNotification(KNotificationLoginStateChange, @YES);
        }else{
            if (completion) {
                completion(NO,responseObject[@"msg"]);
            }
        }
    }else{
        if (completion) {
            completion(NO,@"登录异常");
        }
    }
    
}
#pragma mark ————— 从服务器刷新用户信息 —————
- (void)refreshFromServer{
    [CommonManager POST:@"User/getUserInfo" parameters:@{} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            self.curUserInfo = [UserInfoModel yy_modelWithDictionary:responseObject[@"data"]];
            [self saveUserInfo];
        }
    } failure:^(NSError *error) {
    }];
}

#pragma mark ————— 储存用户信息 —————
- (BOOL)saveUserInfo{
    if (self.curUserInfo) {
        YYCache *cache = [[YYCache alloc]initWithName:KUserCacheName];
        NSDictionary *dic = [self.curUserInfo yy_modelToJSONObject];
        [[cache diskCache] setObject:dic forKey:KUserModelCache];
        return YES;
    }
    return NO;
}
#pragma mark ————— 加载缓存的用户信息 —————
- (BOOL)loadUserInfo{
    YYCache *cache = [[YYCache alloc]initWithName:KUserCacheName];
    NSDictionary * userDic = (NSDictionary *)[[cache diskCache] objectForKey:KUserModelCache];
    if (userDic) {
        self.curUserInfo = [UserInfoModel yy_modelWithJSON:userDic];
        return YES;
    }
    return NO;
}
#pragma mark ————— 被踢下线 —————
- (void)onKick{
    [self logout:nil];
}
#pragma mark ————— 退出登录 —————
- (void)logout:(void (^)(BOOL, NSString *))completion{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:KNotificationLogout object:nil];//被踢下线通知
    
    self.curUserInfo = nil;

//    //移除缓存
    YYCache *cache = [[YYCache alloc]initWithName:KUserCacheName];
    [cache removeAllObjectsWithBlock:^{
        if (completion) {
            completion(YES,nil);
        }
    }];
    
    KPostNotification(KNotificationLoginStateChange, @NO);
}

- (BOOL)isLogined{
    [self loadUserInfo];
    return self.curUserInfo != nil;
}

@end
