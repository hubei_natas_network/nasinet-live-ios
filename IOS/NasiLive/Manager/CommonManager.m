//
//  CommonManager.m
//  JYZC
//
//  Created by yun_7 on 2018/8/15.
//  Copyright © 2018年 yun_7. All rights reserved.
//

#import "CommonManager.h"

@interface CommonManager(){
    QNActivityIndicatorView *qnAcHud;
}

@end

@implementation CommonManager

SINGLETON_FOR_CLASS(CommonManager);

+ (NSURLSessionTask *)POST:(NSString *)api
                parameters:(id)parameters
                   success:(QNHttpRequestSuccess)success
                   failure:(QNHttpRequestFailed)failure {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:parameters];
    if (userManager.curUserInfo) {
        params[@"uid"] = @(userManager.curUserInfo.userid);
        params[@"token"] = userManager.curUserInfo.token;
    }
    NSLog(@"%@\n%@\n",API(api),params);
    return [QNNetworkHelper POST:API(api) parameters:params success:^(id responseObject) {
        NSLog(@"%@\n%@",API(api),responseObject);
        if (RESP_LOGINTIMEOUT(responseObject)) {
            RESP_SHOW_ERROR_MSG(responseObject);
            [userManager logout:nil];
            [kAppDelegate showLoginViewController];
            success ? success(responseObject) : nil;
        }else{
            success ? success(responseObject) : nil;
        }
    } failure:^(NSError *error) {
        failure ? failure(error) : nil;
    }];
}

+ (BOOL)checkAndLogin{
    if (!userManager.isLogined) {
        [kAppDelegate showLoginViewController];
        return NO;
    }
    return YES;
}

+ (void)loadCosTempKey{
    [CommonManager POST:@"Config/getTempKeysForCos" parameters:@{} success:^(id responseObject) {
        if (RESP_SUCCESS(responseObject)) {
            configManager.txCosModel = [TxCosModel yy_modelWithDictionary:responseObject[@"data"]];
            [configManager saveTxCosModel];
        }else{
        }
    } failure:^(NSError *error) {
    }];
}

- (void)showLoadingAnimateInWindow{
    if (qnAcHud) {
        [qnAcHud hidden];
    }
    qnAcHud = [QNActivityIndicatorView showInWindow:QNActivityIndicatorAnimationTypeBallClipRotate tintColor:MAIN_COLOR];
}

- (void)showLoadingAnimateInView:(UIView *)view{
    if (qnAcHud) {
        [qnAcHud hidden];
    }
    qnAcHud = [QNActivityIndicatorView showInView:view type:QNActivityIndicatorAnimationTypeBallClipRotate tintColor:MAIN_COLOR];
}

- (void)showErrorAnimateInWindow{
    if (qnAcHud) {
        [qnAcHud hidden];
    }
    qnAcHud = [QNActivityIndicatorView showInWindow:QNActivityIndicatorAnimationTypeLoadingFail tintColor:MAIN_COLOR];
    [qnAcHud hideAnimated:YES afterDelay:1.5f];
}

- (void)showErrorAnimateInView:(UIView *)view{
    if (qnAcHud) {
        [qnAcHud hidden];
    }
    qnAcHud = [QNActivityIndicatorView showInView:view type:QNActivityIndicatorAnimationTypeLoadingFail tintColor:MAIN_COLOR];
    [qnAcHud hideAnimated:YES afterDelay:1.5f];
}

- (void)showSuccessAnimateInWindow{
    if (qnAcHud) {
        [qnAcHud hidden];
    }
    qnAcHud = [QNActivityIndicatorView showInWindow:QNActivityIndicatorAnimationTypeLoadingSuccess tintColor:MAIN_COLOR];
    [qnAcHud hideAnimated:YES afterDelay:1.5f];
}

- (void)showSuccessAnimateInView:(UIView *)view{
    if (qnAcHud) {
        [qnAcHud hidden];
    }
    qnAcHud = [QNActivityIndicatorView showInView:view type:QNActivityIndicatorAnimationTypeLoadingSuccess tintColor:MAIN_COLOR];
    [qnAcHud hideAnimated:YES afterDelay:1.5f];
}

- (void)hideAnimateHud{
    if (qnAcHud) {
        [qnAcHud hidden];
    }
}

@end
