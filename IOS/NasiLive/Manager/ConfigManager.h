//
//  ConfigManager.h
//  Avideo
//
//  Created by yun7_mac on 2019/3/6.
//  Copyright © 2019 yun_7. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ConfigModel;
@class TxCosModel;
@class GuardPriceModel;

NS_ASSUME_NONNULL_BEGIN

typedef void (^configBlock)(BOOL success, NSDictionary * _Nullable respDict);

#define configManager [ConfigManager sharedConfigManager]

@interface ConfigManager : NSObject
//单例
SINGLETON_FOR_HEADER(ConfigManager)

@property (strong, nonatomic) ConfigModel *appConfig;
@property (strong, nonatomic) NSArray *userTags;
@property (strong, nonatomic) TxCosModel *txCosModel;
@property (strong, nonatomic) NSArray *liveCategorys;
@property (strong, nonatomic) NSArray *videoCategorys;

@property (strong, nonatomic) GuardPriceModel *guardPriceModel;

- (void)loadAppConfigFromServer:(configBlock)configBlock;

- (void)saveTxCosModel;

@end

NS_ASSUME_NONNULL_END
