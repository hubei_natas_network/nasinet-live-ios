//
//  UserInfo.h
//  yun_7
//
//  Created by yun_7 on 2017/5/23.
//  Copyright © 2017年 yun_7. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GiftModel.h"
#import "UserTagModel.h"
#import "IntimacyModel.h"
#import "UserProfileModel.h"

@class LiveModel;

typedef NS_ENUM(NSUInteger, UserOnlineStatus) {
    UserOnlineStatusOnline = 1,
    UserOnlineStatusBackground = 2,
    UserOnlineStatusConnecting = 3,
    UserOnlineStatusOffline = 9
};

@interface UserInfoModel : NSObject

@property (nonatomic,assign) long long userid;//用户ID
@property (nonatomic,copy) NSString * nick_name; //昵称
@property (nonatomic,copy) NSString * mobile; //手机号
@property (nonatomic,copy) NSString * avatar; //头像
@property (nonatomic,copy) NSString * cover_img; //封面图
@property (nonatomic,copy) NSString * token; //

@property (strong, nonatomic) UserProfileModel *profile;

@property (nonatomic,assign) int gold;//金币
@property (nonatomic,assign) int diamond;//钻石
@property (nonatomic,assign) int diamond_total;//累计钻石

@property (nonatomic,assign) int anchor_level;//主播星级
@property (nonatomic,assign) int anchor_point; //主播经验值

@property (nonatomic,assign) int user_level; //用户等级
@property (nonatomic,assign) int point; //用户等级经验值

@property (nonatomic,assign) int video_price; //视频价格
@property (nonatomic,assign) int voice_price; //语音价格

@property (nonatomic,assign) BOOL is_anchor; //是否认证主播 1-已认证
@property (nonatomic,assign) UserOnlineStatus online_status; //在线状态 1-在线 2-进入后台（离开）3-通话中 9-离线

@property (nonatomic,assign) int guildid; //所属公会id
@property (nonatomic,assign) int agentid; //所属代理id

@property (copy, nonatomic) NSString *regist_time; //注册时间
@property (copy, nonatomic) NSString *last_online; //上次在线时间

@property (copy, nonatomic) NSString *vip_date; //vip到期时间
@property (assign, nonatomic) int vip_level; //vip等级

@property (nonatomic,assign) int attent_count; //关注数量
@property (nonatomic,assign) int fans_count; //粉丝数量
@property (nonatomic,assign) int vistor_count; //访客数量
@property (nonatomic,assign) int sv_star_count; //短视频点赞数量
@property (nonatomic,assign) int click_count; //动态累计浏览数量
@property (nonatomic,copy) NSString *left_watch_count; //剩余免费观影次数
@property (nonatomic,assign) long long gift_spend; //累计送礼消耗金币数

@property (strong, nonatomic) NSMutableArray *guard_anchors;//守护的主播id数组


@property (copy, nonatomic) NSString *txim_sign;
@property (strong, nonatomic) NSDictionary *txcam_credentials; //腾讯云服务CAM

@property (copy, nonatomic) NSString *connect_rate; //接通率

@property (nonatomic,assign) BOOL isattent; //是否已关注

@property (strong, nonatomic) NSMutableArray *gift_show; //礼物柜
@property (strong, nonatomic) NSMutableArray *intimacys; //亲密用户
@property (strong, nonatomic) NSMutableArray *tags; //亲密用户

@property (strong, nonatomic) LiveModel *live;

- (BOOL)checkVip;
- (int)getVipLevel;
- (BOOL)checkGuardAnchor:(int)anchorid;

+ (instancetype)copyUserModelFromModel:(UserInfoModel *)model;

@end
