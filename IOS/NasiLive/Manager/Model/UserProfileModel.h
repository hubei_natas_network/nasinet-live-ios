//
//  UserProfileModel.h
//  NasiLive
//
//  Created by yun11 on 2020/6/1.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserProfileModel : NSObject

@property (nonatomic,assign) int uid; //年龄
@property (copy, nonatomic) NSString *signature; //个性签名
@property (copy, nonatomic) NSString *career; //职业
@property (copy, nonatomic) NSString *city; //城市
@property (copy, nonatomic) NSString *birthday; //生日
@property (copy, nonatomic) NSString *constellation; //星座
@property (nonatomic,assign) int age; //年龄
@property (nonatomic,assign) BOOL gender; //性别  0-女 1-男
@property (nonatomic,assign) int height; //身高
@property (nonatomic,assign) int weight; //体重
@property (strong, nonatomic) NSMutableArray *photos; //相册

@end

NS_ASSUME_NONNULL_END
