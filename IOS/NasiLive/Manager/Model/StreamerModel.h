//
//  StreamerModel.h
//  NasiLive
//
//  Created by yun11 on 2020/7/11.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VipModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, StreamerType) {
    StreamerTypeVIP = 1,
    StreamerTypeAllChannelGift = 2,
};

@interface StreamerModel : NSObject

@property (strong, nonatomic) UserInfoModel *user;
@property (strong, nonatomic) VipModel *vip;
@property (assign, nonatomic) StreamerType type;
@property (strong, nonatomic) GiftModel   *gift;
@property (strong, nonatomic) UserInfoModel     *anchor;



@end

NS_ASSUME_NONNULL_END
