//
//  IMNotificationModel.h
//  Nasi
//
//  Created by yun on 2019/12/25.
//  Copyright © 2019 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LiveModel.h"
#import "RoomChatModel.h"
#import "GiftModel.h"
#import "StreamerModel.h"
#import "ShopGoodsModel.h"
#import "RoomNotifyModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, IMNotificationAction) {
    IMNotificationActionRoomMessage,                //收到房间内消息
    IMNotificationActionGiftAnimation,              //播放礼物动画
    IMNotificationActionLiveFinished,
    IMNotificationActionLiveGroupMemberJoinExit,    //直播间用户进入退出
    IMNotificationActionStreamer,                   //横幅推送
    IMNotificationActionExplainingGoods,            //开始讲解商品
    IMNotificationActionRoomNotification,           //直播间通知消息
    IMNotificationActionSystemMessage = 9001        //系统消息
};

@interface IMNotificationModel : NSObject

@property (nonatomic,assign) IMNotificationAction action;

@property (copy, nonatomic) NSString *roomid;
@property (strong, nonatomic) LiveModel *live;
@property (strong, nonatomic) RoomChatModel *chat;
@property (strong, nonatomic) GiftModel *gift;
@property (strong, nonatomic) StreamerModel *streamer;
@property (strong, nonatomic) ShopGoodsModel *goods;
@property (strong, nonatomic) RoomNotifyModel *notify;

@property (copy, nonatomic) NSString *message;

@end

NS_ASSUME_NONNULL_END
