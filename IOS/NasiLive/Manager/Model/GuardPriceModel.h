//
//  GuardPriceModel.h
//  NasiLive
//
//  Created by yun11 on 2020/12/3.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GuardPriceModel : NSObject

@property (assign, nonatomic) int week_price;
@property (assign, nonatomic) int month_price;
@property (assign, nonatomic) int year_price;

@end

NS_ASSUME_NONNULL_END
