//
//  IMNotificationModel.m
//  Nasi
//
//  Created by yun on 2019/12/25.
//  Copyright © 2019 yun7. All rights reserved.
//

#import "IMNotificationModel.h"

@implementation IMNotificationModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"live" : @"Data.live",
             @"chat" : @"Data.chat",
             @"gift" : @"Data.gift",
             @"streamer" : @"Data.streamer",
             @"goods" : @"Data.goods",
             @"notify" : @"Data.notify"
             };
}

// 当 JSON 转为 Model 完成后，该方法会被调用。
// 你可以在这里对数据进行校验，如果校验不通过，可以返回 NO，则该 Model 会被忽略。
// 你也可以在这里做一些自动转换不能完成的工作。
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    if (StrValid(dic[@"Action"])) {
        if ([dic[@"Action"] isEqualToString:@"RoomMessage"]) {
            _action = IMNotificationActionRoomMessage;
        }else if ([dic[@"Action"] isEqualToString:@"GiftAnimation"]) {
            _action = IMNotificationActionGiftAnimation;
        }else if ([dic[@"Action"] isEqualToString:@"LiveFinished"]) {
            _action = IMNotificationActionLiveFinished;
        }else if ([dic[@"Action"] isEqualToString:@"SystemMessage"]) {
            _action = IMNotificationActionSystemMessage;
        }else if ([dic[@"Action"] isEqualToString:@"LiveGroupMemberJoinExit"]) {
            _action = IMNotificationActionLiveGroupMemberJoinExit;
        }else if ([dic[@"Action"] isEqualToString:@"BroadcastStreamer"]) {
            _action = IMNotificationActionStreamer;
        }else if ([dic[@"Action"] isEqualToString:@"ExplainingGoods"]) {
            _action = IMNotificationActionExplainingGoods;
        }else if ([dic[@"Action"] isEqualToString:@"RoomNotification"]) {
            _action = IMNotificationActionRoomNotification;
        }
    }
    return YES;
}

@end
