//
//  RoomNotifyModel.h
//  NasiLive
//
//  Created by yun11 on 2020/10/27.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LivePkModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, RoomNotifyType) {
    RoomNotifyTypeSetManager,                   //设置房管
    RoomNotifyTypeCancelManager,                //取消房管
    RoomNotifyTypeGuardAnchor,                  //守护主播
    RoomNotifyTypeLinkOn,                       //连麦功能开启
    RoomNotifyTypeLinkOff,                      //连麦功能关闭
    RoomNotificationReciveLinkRequest,          //收到连麦请求
    RoomNotificationAcceptLinkRequest,          //接受连麦请求
    RoomNotificationRefuseLinkRequest,          //拒绝连麦请求
    RoomNotificationStopLink,                   //结束连麦
    RoomNotifyTypePkStart,                      //PK开始
    RoomNotifyTypePkEnd,                        //PK开始
    RoomNotifyTypePkScoreChange,                //PK分数变化
};

@interface RoomNotifyModel : NSObject

@property (assign, nonatomic) RoomNotifyType    type;
@property (strong, nonatomic) UserInfoModel     *user;
@property (copy, nonatomic) NSString            *link_acc_url;
@property (assign, nonatomic) NSInteger         touid;
@property (strong, nonatomic) LiveModel         *pklive;
@property (strong, nonatomic) LivePkModel       *pkinfo;

@end

NS_ASSUME_NONNULL_END
