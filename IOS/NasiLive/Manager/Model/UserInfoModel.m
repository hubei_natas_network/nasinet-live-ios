//
//  UserInfo.m
//  yun_7
//
//  Created by yun_7 on 2017/5/23.
//  Copyright © 2017年 yun_7. All rights reserved.
//

#import "UserInfoModel.h"

@implementation UserInfoModel


//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"userid" : @"id",
             @"mobile" : @"account"
    };
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"gift_show":[GiftModel class],
             @"intimacys":[IntimacyModel class],
             @"tags":[UserTagModel class]
    };
}

- (BOOL)checkVip{
    if (self.vip_date.length > 0 && self.vip_level > 0) {
        int i = [CommonManager compareOneDay:[CommonManager dateWithString:self.vip_date] withAnotherDay:[NSDate date]];
        if (i > 0) {
            return YES;
        }
        return NO;
    }
    return NO;
}

- (int)getVipLevel{
    if ([self checkVip]) {
        return self.vip_level;
    }
    return 0;
}
- (BOOL)checkGuardAnchor:(int)anchorid{
    if ([self.guard_anchors containsObject:@(anchorid)]) {
        return YES;
    }
    return NO;
}


+ (instancetype)copyUserModelFromModel:(UserInfoModel *)model{
    UserInfoModel *copyModel = [[self alloc]init];
    copyModel.nick_name = model.nick_name;
    copyModel.vip_date = model.vip_date;
    copyModel.vip_level = model.vip_level;
    copyModel.avatar = model.avatar;
    copyModel.userid = model.userid;
    copyModel.user_level = model.user_level;
    copyModel.anchor_level = model.anchor_level;
    copyModel.is_anchor = model.is_anchor;
    return copyModel;
}

@end
