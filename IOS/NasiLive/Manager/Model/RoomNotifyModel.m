//
//  RoomNotifyModel.m
//  NasiLive
//
//  Created by yun11 on 2020/10/27.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "RoomNotifyModel.h"

@interface RoomNotifyModel()

@property (strong, nonatomic) NSDictionary *notifyTypeDefineDict;

@end

@implementation RoomNotifyModel

// 当 JSON 转为 Model 完成后，该方法会被调用。
// 你可以在这里对数据进行校验，如果校验不通过，可以返回 NO，则该 Model 会被忽略。
// 你也可以在这里做一些自动转换不能完成的工作。
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    if (StrValid(dic[@"type"])) {
        _type = (RoomNotifyType)[self.notifyTypeDefineDict[dic[@"type"]] intValue];
    }
    return YES;
}

- (NSDictionary *)notifyTypeDefineDict{
    if (!_notifyTypeDefineDict) {
        _notifyTypeDefineDict = @{
            @"RoomNotifyTypeSetManager":@(RoomNotifyTypeSetManager),
            @"RoomNotifyTypeCancelManager":@(RoomNotifyTypeCancelManager),
            @"RoomNotifyTypeLinkOn":@(RoomNotifyTypeLinkOn),
            @"RoomNotifyTypeLinkOff":@(RoomNotifyTypeLinkOff),
            @"RoomNotificationReciveLinkRequest":@(RoomNotificationReciveLinkRequest),
            @"RoomNotificationAcceptLinkRequest":@(RoomNotificationAcceptLinkRequest),
            @"RoomNotificationRefuseLinkRequest":@(RoomNotificationRefuseLinkRequest),
            @"RoomNotificationStopLink":@(RoomNotificationStopLink),
            @"RoomNotifyTypeGuardAnchor":@(RoomNotifyTypeGuardAnchor),
            @"RoomNotifyTypePkStart":@(RoomNotifyTypePkStart),
            @"RoomNotifyTypePkEnd":@(RoomNotifyTypePkEnd),
            @"RoomNotifyTypePkScoreChange":@(RoomNotifyTypePkScoreChange),
        };
    }
    return _notifyTypeDefineDict;
}

@end
