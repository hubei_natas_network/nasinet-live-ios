//
//  UserProfileModel.m
//  NasiLive
//
//  Created by yun11 on 2020/6/1.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "UserProfileModel.h"

@implementation UserProfileModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
    };
}

// 当 JSON 转为 Model 完成后，该方法会被调用。
// 你可以在这里对数据进行校验，如果校验不通过，可以返回 NO，则该 Model 会被忽略。
// 你也可以在这里做一些自动转换不能完成的工作。
- (BOOL)modelCustomTransformFromDictionary:(NSDictionary *)dic {
    if (StrValid(dic[@"career"])) {
        _career = dic[@"career"];
    }else{
        _career = @"未知";
    }
    if (StrValid(dic[@"birthday"])) {
        _birthday = dic[@"birthday"];
    }else{
        _birthday = @"未知";
    }
    if (StrValid(dic[@"city"])) {
        _city = dic[@"city"];
    }else{
        _city = @"未知";
    }
    if (StrValid(dic[@"constellation"])) {
        _constellation = dic[@"constellation"];
    }else{
        _constellation = @"未知";
    }
    if (StrValid(dic[@"signature"])) {
        _signature = dic[@"signature"];
    }else{
        _signature = @"暂无个性签名~";
    }
    if (StrValid(dic[@"photos"])) {
        NSString *photosStr = dic[@"photos"];
        _photos = [NSMutableArray arrayWithArray:[photosStr componentsSeparatedByString:@","]];
    }
    return YES;
}

@end
