//
//  TxCosModel.m
//  Nasi
//
//  Created by yun on 2020/1/14.
//  Copyright © 2020 yun7. All rights reserved.
//

#import "TxCosModel.h"

@implementation TxCosModel

//返回一个 Dict，将 Model 属性名对映射到 JSON 的 Key。
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{
        @"tmpSecretId":@"credentials.tmpSecretId",
        @"tmpSecretKey":@"credentials.tmpSecretKey",
        @"sessionToken":@"credentials.sessionToken"
    };
}

@end
