//
//  ConfigModel.h
//  Nasi
//
//  Created by yun on 2019/12/18.
//  Copyright © 2019 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ConfigModel : NSObject

@property (copy, nonatomic) NSString    *socket_server;
@property (copy, nonatomic) NSString    *qcloud_appid;              //腾讯云appid
@property (copy, nonatomic) NSString    *cos_region;                //腾讯云存储region
@property (copy, nonatomic) NSString    *cos_bucket;                //腾讯云存储bucket
@property (copy, nonatomic) NSString    *cos_folder_image;          //腾讯云存储图片存放地址
@property (copy, nonatomic) NSString    *cos_folder_blurimage;      //腾讯云存储高斯模糊图片存放地址
@property (copy, nonatomic) NSString    *im_sdkappid;               //trtc+im sdkappid
@property (copy, nonatomic) NSString    *wx_appid;                  //微信开放平台
@property (copy, nonatomic) NSString    *universal_link;
@property (copy, nonatomic) NSString    *txim_admin;
@property (copy, nonatomic) NSString    *txim_broadcast;            

@property (copy, nonatomic) NSString    *qq_appid;         
@property (copy, nonatomic) NSString    *qq_appkey;

@property (assign, nonatomic) int       beauty_channel;             //美颜渠道
@property (copy, nonatomic) NSString    *tisdk_key;

@property (assign, nonatomic) int       agent_ratio;                //代理返佣比例

@property (copy, nonatomic) NSString    *room_notice;               //直播间公告

@property (assign, nonatomic) int       exchange_rate;              //钻石兑换人民币汇率
@property (assign, nonatomic) int       withdraw_min;               //单次最低提现金额

@property (assign, nonatomic) BOOL      switch_iap;                 //是否开启内购

@property (copy, nonatomic) NSString    *site_domain;               //Web端域名
@property (copy, nonatomic) NSString    *dl_web_url;                //下载页地址
@property (copy, nonatomic) NSString    *version_ios;               //最新版本号
@property (copy, nonatomic) NSString    *update_info_ios;           //新版本信息
@property (copy, nonatomic) NSString    *share_live_url;            //直播间分享地址
@property (copy, nonatomic) NSString    *share_shortvideo_url;      //短视频分享地址
@property (copy, nonatomic) NSString    *share_moment_url;          //动态分享地址

@property (copy, nonatomic) NSString    *file_server_domain;

@end

NS_ASSUME_NONNULL_END
