//
//  TxCosModel.h
//  Nasi
//
//  Created by yun on 2020/1/14.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TxCosModel : NSObject

@property (copy, nonatomic) NSString *tmpSecretId;
@property (copy, nonatomic) NSString *tmpSecretKey;
@property (copy, nonatomic) NSString *sessionToken;
@property (nonatomic,assign) NSInteger startTime;
@property (nonatomic,assign) NSInteger expiredTime;
@property (copy, nonatomic) NSString *expiration;

@end

NS_ASSUME_NONNULL_END
