//
//  UserWithdrawAccountModel.h
//  NasiLive
//
//  Created by yun11 on 2020/7/9.
//  Copyright © 2020 yun7. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserWithdrawAccountModel : NSObject

@property (assign, nonatomic) int accountid;
@property (assign, nonatomic) int uid;
@property (assign, nonatomic) BOOL isDefault;       //是否默认账户
@property (copy, nonatomic) NSString *alipay_name; //支付宝姓名
@property (copy, nonatomic) NSString *alipay_account; //支付宝账号


@end

NS_ASSUME_NONNULL_END
