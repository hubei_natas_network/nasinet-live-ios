//
//  ConfigManager.m
//  Avideo
//
//  Created by yun7_mac on 2019/3/6.
//  Copyright © 2019 yun_7. All rights reserved.
//

#import "ConfigManager.h"
#import "ConfigModel.h"
#import "GuardPriceModel.h"

@implementation ConfigManager

SINGLETON_FOR_CLASS(ConfigManager);

- (void)loadAppConfigFromServer:(configBlock)configBlock{
    [QNNetworkHelper POST:API(@"Config/getCommonConfig") parameters:@{@"version":[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],@"platform":@"1"} success:^(id responseObject) {
        if (ValidDict(responseObject)) {
            if (RESP_SUCCESS(responseObject)) {
                NSDictionary *configDict = responseObject[@"data"][@"config"];
                self.userTags = [NSArray yy_modelArrayWithClass:[UserTagModel class] json:responseObject[@"data"][@"user_tag"]];
                self.guardPriceModel = [GuardPriceModel yy_modelWithDictionary:responseObject[@"data"][@"guard_price"]];
                [self saveUserTags];
                self.liveCategorys = responseObject[@"data"][@"live_category"];
                self.videoCategorys = responseObject[@"data"][@"video_category"];
                self.appConfig = [ConfigModel yy_modelWithDictionary:configDict];
                [self saveAppConfig];
                //读取到配置信息后 进行第三方sdk初始化
                [[AppDelegate shareAppDelegate] launchSdk];
                if (userManager.isLogined) {
                    [[AppDelegate shareAppDelegate] launchSdkNeedLogin];
                }
                configBlock(YES,responseObject[@"data"][@"launch_ad"]);
            }else{
                configBlock(NO,nil);
            }
        }else{
            configBlock(NO,nil);
        }
    } failure:^(NSError *error) {
        configBlock(NO,nil);
    }];
}

#pragma mark ————— 储存配置信息 —————
- (void)saveAppConfig{
    if (self.appConfig) {
        YYCache *cache = [[YYCache alloc]initWithName:KConfigCacheName];
        NSDictionary *dic = [self.appConfig yy_modelToJSONObject];
        [cache setObject:dic forKey:KConfigCache];
    }
}
- (void)saveUserTags{
    if (self.appConfig) {
        YYCache *cache = [[YYCache alloc]initWithName:KUserTagsCacheName];
        NSDictionary *dic = [self.appConfig yy_modelToJSONObject];
        [cache setObject:dic forKey:KUserTagsCache];
    }
}
- (void)saveLiveCategorys{
    if (self.liveCategorys) {
        YYCache *cache = [[YYCache alloc]initWithName:KLiveCategorysCacheName];
        NSDictionary *dic = [self.liveCategorys yy_modelToJSONObject];
        [cache setObject:dic forKey:KLiveCategorysCache];
    }
}
- (void)saveTxCosModel{
    if (self.txCosModel) {
        YYCache *cache = [[YYCache alloc]initWithName:KUserTagsCacheName];
        NSDictionary *dic = [self.txCosModel yy_modelToJSONObject];
        [cache setObject:dic forKey:KTxCosCache];
    }
}
#pragma mark ————— 加载缓存的配置信息 —————
- (BOOL)loadAppConfig{
    YYCache *cache = [[YYCache alloc]initWithName:KConfigCacheName];
    NSDictionary * dict = (NSDictionary *)[cache objectForKey:KConfigCache];
    if (dict) {
        self.appConfig = [ConfigModel yy_modelWithJSON:dict];
        return YES;
    }
    return NO;
}

- (ConfigModel *)appConfig{
    if (!_appConfig) {
        YYCache *cache = [[YYCache alloc]initWithName:KConfigCacheName];
        NSDictionary * dict = (NSDictionary *)[cache objectForKey:KConfigCache];
        if (dict) {
            _appConfig = [ConfigModel yy_modelWithJSON:dict];
        }
    }
    return _appConfig;
}

- (NSArray *)userTags{
    if (!_userTags) {
        YYCache *cache = [[YYCache alloc]initWithName:KUserTagsCacheName];
        _userTags = (NSArray *)[cache objectForKey:KUserTagsCache];
    }
    return _userTags;
}

- (TxCosModel *)txCosModel{
    if (!_txCosModel) {
        YYCache *cache = [[YYCache alloc]initWithName:KTxCosCacheName];
        NSDictionary * dict = (NSDictionary *)[cache objectForKey:KTxCosCache];
        if (dict) {
            _txCosModel = [TxCosModel yy_modelWithJSON:dict];
        }
    }
    return _txCosModel;
}

- (NSArray *)liveCategorys{
    if (!_liveCategorys) {
        YYCache *cache = [[YYCache alloc]initWithName:KLiveCategorysCacheName];
        _liveCategorys = (NSArray *)[cache objectForKey:KLiveCategorysCache];
    }
    return _liveCategorys;
}
- (NSArray *)videoCategorys{
    if (!_videoCategorys) {
        YYCache *cache = [[YYCache alloc]initWithName:KVideoCategorysCacheName];
        _videoCategorys = (NSArray *)[cache objectForKey:KVideoCategorysCache];
    }
    return _videoCategorys;
}
@end
