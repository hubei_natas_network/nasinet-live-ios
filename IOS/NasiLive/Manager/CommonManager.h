//
//  CommonManager.h
//  JYZC
//
//  Created by yun_7 on 2018/8/15.
//  Copyright © 2018年 yun_7. All rights reserved.
//

#import <Foundation/Foundation.h>

#define commonManager [CommonManager sharedCommonManager]

@interface CommonManager : NasiCommonManager

//单例
SINGLETON_FOR_HEADER(CommonManager)

/** 请求成功的Block */
typedef void(^QNHttpRequestSuccess)(id responseObject);

/** 请求失败的Block */
typedef void(^QNHttpRequestFailed)(NSError *error);

+ (NSURLSessionTask *)POST:(NSString *)api
                parameters:(id)parameters
                   success:(QNHttpRequestSuccess)success
                   failure:(QNHttpRequestFailed)failure;

//加载腾讯云存储临时秘钥
+ (void)loadCosTempKey;

+ (BOOL)checkAndLogin;

- (void)showLoadingAnimateInWindow;
- (void)showErrorAnimateInWindow;
- (void)showSuccessAnimateInWindow;
- (void)showLoadingAnimateInView:(UIView *)view;
- (void)showErrorAnimateInView:(UIView *)view;
- (void)showSuccessAnimateInView:(UIView *)view;
- (void)hideAnimateHud;

@end
