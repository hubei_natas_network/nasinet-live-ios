#                                钠斯直播系统IOS版

![](https://naisnet-1303209584.cos.ap-hongkong.myqcloud.com/1.png)


## 项目介绍：

​    钠斯直播系统是一款让用户通过手机观看直播，带货、打赏、分享短视频与生活动态，分享传播每一个直播精彩瞬间的流媒体直播系统。

​    系统由钠斯网络技术团队自主研发，可进行系统二次开发，全球任意地区均可搭建部署。钠斯直播系统包含了PC端，H5端，以及Android 和 IOS 移动端，实现了全端覆盖。

 

##  编译环境
Xcode 7＋


##  运行项目
1. 安装CocoaPods (关于CocoaPods的安装和使用，可参考[这个教程](http://code4app.com/article/cocoapods-install-usage))
2. 在终端下打开项目所在的目录，执行```pod install``` (若是首次使用CocoaPods，需先执行```pod setup```)
3. ```pod install```命令执行成功后，通过新生成的xcworkspace文件打开工程运行项目


##  目录简介

* AppDelegate：      存放AppDelegate和API定义
* Base：             基础view
* Define：           定义配置信息
* Model：            数据实体类
* Manager：          管理全局数据
* Module：           存放所有的view controller
* Resources：        存放除图片以外的资源文件
* Supporting Files： 图片资源
* Utils：            存放工具类


## 功能展示：

![](https://naisnet-1303209584.cos.ap-hongkong.myqcloud.com/2.png)

## 开源使用须知：

1. 允许用于个人学习、教学研究
2. 开源版不允许商业使用，如需商用请联系客服
3. 禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负



## 商务合作：

1. 如果你想使用功能更强大更完善的直播系统，请联系客服：18908605871（同微信）QQ：245792062

2. 如果您想基于直播系统进行定制开发，我们提供有偿定制服务支持！

3. 我们还有钠斯短视频，种草带货，社区，赛事系统等多款成熟商用级产品，另有自建流媒体，IM等产品，如有需要欢迎来联系我们！

4. 只有你想不到，没有我们做不到，其他合作模式不限，欢迎来撩！

5. 官网地址：http://www.nasinet.com

   

## 联系我们：

客服Q Q：245792062
联系客服：18908605871（同微信号）
客服微信：扫码可加微信  ![](https://naisnet-1303209584.cos.ap-hongkong.myqcloud.com/qrcode.jpg)



![](https://naisnet-1303209584.cos.ap-hongkong.myqcloud.com/3.jpg)



