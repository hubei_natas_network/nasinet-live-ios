#                                Nas Live System (iOS Version)

![](https://naisnet-1303209584.cos.ap-hongkong.myqcloud.com/1.png)


## Project Introduction:

Nas live broadcast system is a streaming media live broadcast system that allows users to watch live broadcasts through mobile phones, bring goods, give rewards, share short videos and life trends, and share and spread every wonderful moment of live broadcasts.

The system is independently developed by the Nass network technical team, which can carry out secondary development of the system, and can be deployed in any region of the world. Nas live broadcast system includes PC terminal, H5 terminal, as well as Android and IOS mobile terminals, achieving full coverage.
​    
 

##  Compilation environment
Xcode 7＋


##  Running items
1. Install CocoaPods (for the installation and use of CocoaPods, please refer to [this tutorial](http://code4app.com/article/cocoapods-install-usage))
2. Open the directory of the project under the terminal and execute```pod install``` (If you use cocoapods for the first time, you need to execute```pod setup```)
3. ```pod install```After the command is successfully executed, open the project running project through the newly generated xcworkspace file


##  Catalog introduction

* AppDelegate：      AppDelegate and API
* Base：             base view
* Define：           Define configuration information
* Model：            Data entity class
* Manager：          Manage global data
* Module：           Store all view controller
* Resources：        Store resource files other than pictures
* Supporting Files： poctures files
* Utils：            utils


## function display:

![](https://naisnet-1303209584.cos.ap-hongkong.myqcloud.com/2.png)

## Instructions for using open source:

1. Allowed for personal study, teaching research
2. The open source version is not allowed for commercial use, if you need commercial use, please contact customer service
3. It is prohibited to sell the code and resources of this project in any form, and the infringer shall be responsible for all consequences arising therefrom



## Business Cooperation:

1. If you want to use a more powerful and complete live broadcast system, please contact customer service: + (86) 18908605871 (same as WeChat) QQ: 245792062

2. If you want to customize development based on the live broadcast system, we provide paid customization service support!

3. We also have a variety of mature commercial-grade products such as Nass short video, grass planting, community, competition system, etc., as well as self-built streaming media, IM and other products, please feel free to contact us if you need it!

4. Only you can't think of it, we can't do it without us, other cooperation modes are not limited, welcome to tease!

5. Official website address: http://www.nasinet.com

   

## contact us:

Customer service QQ: 245792062
Contact customer service: 18908605871 (same as WeChat)
Customer Service WeChat: Scan the code to add WeChat ![](https://naisnet-1303209584.cos.ap-hongkong.myqcloud.com/qrcode.jpg)

![](https://naisnet-1303209584.cos.ap-hongkong.myqcloud.com/3.jpg)


